//pagination for queries with limit and offset (lists, stores ...)
import {useState} from 'react';

const usePaginationv2 = (limit = 10) => {
  const [currentPage, setCurrentPage] = useState(1);

  const goNextPage = (total) => {
    if (currentPage * limit <= total) {
      setCurrentPage(currentPage + 1);
    }
  };

  const goPrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  const resetPagination = () => {
    setCurrentPage(1);
  };

  return {
    currentPage,
    goPrevPage,
    goNextPage,
    resetPagination
  };
};

export default usePaginationv2;
