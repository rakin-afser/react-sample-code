export const getErrorByCode = (errorCode) => {
  switch (errorCode) {
    case 'UserNotFoundException':
      return {
        type: 'error',
        title: 'Invalid Username or Password',
        text: 'Please try again or click Forgot Username or Forgot Password link below.'
      };
    case 'NotAuthorizedException':
      return {
        type: 'error',
        title: 'Invalid Username or Password',
        text: 'Please try again or click Forgot Username or Forgot Password link below.'
      };
    case 'PasswordResetRequiredException':
      return {
        type: 'error',
        title: '',
        text: 'The user requires a new password.'
      };
  }
};
