import moment from 'moment';
import exampleData from 'containers/GuestCheckoutPage/assets/data/exampleData';

const {country} = exampleData;

export default function numberWithCommas(x) {
  return x?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export const abbreviateToNumber = (number) => {
  function intlFormat(num) {
    return new Intl.NumberFormat().format(Math.round(num * 10) / 10);
  }

  function makeFriendly(num) {
    if (num >= 1000000) return `${intlFormat(num / 1000000)}M`;
    if (num >= 1000) return `${intlFormat(num / 1000)}k`;
    return intlFormat(num);
  }

  return makeFriendly(number);
};

export const divideArrayToParts = (array, parts) => {
  return new Array(Math.ceil(array.length / parts)).fill().map((_) => array.splice(0, parts));
};

export const dateFns = {
  years: (minYear) => {
    const max = new Date().getFullYear();
    const min = max - minYear;
    const years = [];

    // eslint-disable-next-line no-plusplus
    for (let i = max; i >= min; i--) {
      years.push(i);
    }
    return years;
  },

  monthsName: () => {
    return moment.months();
  },

  daysPerMonth: (year, month) => {
    const daysQnt = moment(`${year} ${month}`, `YYYY MMM`).daysInMonth();

    return [...Array(daysQnt).keys()].map((i) => i + 1);
  }
};

export const findRecursive = (subMenuItems, value, propertyName, nestedFieldName) => {
  if (subMenuItems) {
    for (let i = 0; i < subMenuItems.length; i++) {
      if (subMenuItems[i][propertyName] === value) {
        return subMenuItems[i];
      }
      const found = findRecursive(subMenuItems[i][nestedFieldName], value, propertyName, nestedFieldName);
      if (found) return found;
    }
  }
};

export const truncateText = (text = '', maxLength = 20) => {
  if (text.length <= maxLength) return text;
  return `${text.slice(0, maxLength - 3)}...`;
};

export const getUrlWithoutLastParam = (url) => {
  const urlAsArray = url.split('/');
  return urlAsArray.splice(0, urlAsArray.length - 1).join('/');
};

export const toNumber = (string) => {
  return string?.replace(/^\D+/g, '');
};

export const userId = parseInt(localStorage.getItem('userId'));
export const token = localStorage.getItem('token');
export const gEmail = localStorage.getItem('gEmail');

export const prettifyPrice = (value) => (isNaN(parseInt(value)) ? (0).toFixed(2) : parseInt(value).toFixed(2));

export const getCountryFromCode = (code) => Object.values(country)?.find((item) => item.code === code)?.name;

export const isDeliveryRegion = (subCartsData) => {
  return subCartsData?.findIndex((item) => item?.shipping?.availableMethods?.length > 0) >= 0;
};

export function getCheckoutLink(subCartsData, user) {
  if (isDeliveryRegion(subCartsData)) {
    return '/checkout/review';
  }

  if (user?.email) {
    return '/checkout/details';
  }

  return '/checkout/start';
}

//
// Variation functions
//
// function getZeroAttrs(product) {
//   return product?.variations?.nodes?.reduce((acc, v) => {
//     const attr = v.attributes?.nodes.reduce(
//       (acc, attr) =>
//         attr?.attributeId === 0
//           ? [
//               ...acc,
//               {
//                 [attr?.name]: attr?.attributeId
//               }
//             ]
//           : acc,
//       []
//     );

//     if (attr.length) {
//       return [...acc, ...attr];
//     }
//     return acc;
//   }, []);
// }

// function variationPrice(product, selectedAttrs) {
//   let price = '';
//   let regularPrice = '';
//   let salePrice = '';

//   for (let i = 0; i < product?.variations?.nodes.length; i++) {
//     const element = product?.variations?.nodes[i];

//     const containsAll = element?.attributes?.nodes
//       ?.map((attr) => attr?.attributeId)
//       ?.every((id) => Object.values(selectedAttrs)?.includes(id));
//     // Lines for debugging
//     // console.log(element?.attributes?.nodes);
//     // console.log(containsAll);
//     // console.log(price);
//     if (containsAll) {
//       price = element.price;
//       if (element.salePrice) {
//         regularPrice = element.regularPrice;
//       }
//       salePrice = element.salePrice;
//       break;
//     }
//   }

//   return {price, regularPrice, salePrice};
// }

// Main variation function
// console.log(`getVariationPrice`, getVariationPrice(productData?.product, [436, 255]));
// export function variationPrice(product, selectedAttrs) {
//   let prices = variationPrice(product, selectedAttrs);
//   if (Object.values(prices)?.length) {
//     return prices;
//   }

//   const zeroAttrs = getZeroAttrs(product);

//   if (zeroAttrs?.length) {
//     for (const zeroAttr of zeroAttrs) {
//       prices = variationPrice(product, {...selectedAttrs, ...zeroAttr});
//       if (Object.values(prices)?.length) {
//         break;
//       }
//     }
//   }

//   return prices;
// }

// Filter function for useVariations hook
const sliceStart = 6; // length of variation prefix for attr id

export function filterVariations(variations, state) {
  if (Object.keys(state).length === 0) {
    return variations;
  }

  return Object.keys(state)?.reduce((accStateVars, currStateVar) => {
    return accStateVars?.reduce((accVars, currVar) => {
      const ids = variations
        ?.reduce((a, b) => {
          if (
            b?.attributes?.nodes?.findIndex(
              (attr) => attr?.id?.slice(sliceStart) === state?.[currStateVar]?.slice(sliceStart)
            ) >= 0
          ) {
            return [...a, ...b?.attributes?.nodes];
          }
          return a;
        }, [])
        ?.filter((a) => {
          return a?.id?.slice(sliceStart) !== state?.[currStateVar]?.slice(sliceStart);
        })
        ?.map((a) => a?.id?.slice(sliceStart));
      const filteredAttrsByName = currVar?.attributes?.nodes?.map((attr) =>
        currStateVar === attr?.name ? attr : {...attr, disabled: !ids?.includes(attr?.id?.slice(sliceStart))}
      );

      const withSelected = currVar?.attributes?.nodes?.findIndex(
        (attr) => attr?.id?.slice(sliceStart) === state?.[currStateVar]?.slice(sliceStart)
      );

      return [
        ...accVars,
        {
          attributes: {
            nodes: withSelected >= 0 ? currVar?.attributes?.nodes : filteredAttrsByName
          }
        }
      ];
    }, []);
  }, variations);
}

// Format function for useVariations hook
export function formatVariations(filteredVariations, variations) {
  const defaultVariations = variations?.reduce(
    (accVars, currVar) => ({
      ...accVars,
      ...currVar?.attributes?.nodes?.reduce((accAttrs, currAttr) => ({...accAttrs, [currAttr.name]: {}}), {})
    }),
    {}
  );

  return filteredVariations?.reduce((accVars, currVar) => {
    const filteredAttrs = currVar?.attributes?.nodes?.reduce((accAttrs, currAttr) => {
      const exists =
        accVars[currAttr.name]?.options?.findIndex(
          (option) => option.value?.slice(sliceStart) === currAttr.id?.slice(sliceStart)
        ) >= 0;

      return {
        ...accAttrs,
        [currAttr.name]: {
          placeholder: currAttr.label,
          options: exists
            ? accVars[currAttr.name]?.options || []
            : [
                ...(accVars[currAttr.name]?.options || []),
                {label: currAttr.value, value: currAttr.id, disabled: currAttr.disabled}
              ]
        }
      };
    }, {});

    return {...accVars, ...filteredAttrs};
  }, defaultVariations);
}

export function getVariationByAttrs(product, state) {
  let variation = null;

  for (let i = 0; i < product?.variations?.nodes.length; i++) {
    const element = product?.variations?.nodes[i];

    const containsAll = element?.attributes?.nodes
      ?.map((attr) => attr?.id)
      ?.every((id) =>
        Object.values(state)
          ?.map((a) => a?.slice(sliceStart))
          ?.includes(id?.slice(sliceStart))
      );
    if (containsAll) {
      variation = element;
    }
  }

  return variation;
}

export const renderCoinsByVariation = (coins, variation) => {
  if (variation && coins?.length > 1) {
    return coins.filter((el) => el.id === variation?.databaseId?.toString())[0]?.qty;
  }

  return coins && coins[0]?.qty;
};

//
// end of Variation functions
//

export const getDiscountPercent = (regularPrice, salePrice) => {
  let percent = null;

  const regularPriceRanges = regularPrice?.split(' - ');
  const salePriceRanges = salePrice?.split(' - ');

  if (regularPriceRanges?.length > 1 || salePriceRanges?.length > 1) {
    return null;
  }

  if (regularPrice && salePrice) {
    percent = (100 - (toNumber(salePrice?.replace(',', '')) / toNumber(regularPrice?.replace(',', ''))) * 100).toFixed(
      0
    );
  }

  return percent;
};

export function formatCartsData(dataCart) {
  return dataCart?.cart?.subcarts?.subcarts?.map((carts) => ({
    ...carts,
    items: carts?.items?.map((item) => {
      const foundedItem = dataCart?.cart?.contents?.nodes?.find(
        (el) => el.product.node.databaseId === parseInt(item.productId)
      );

      const foundedVariation = dataCart?.cart?.contents?.nodes?.find(
        (el) => el?.variation?.node?.databaseId === parseInt(item.variationId)
      );

      if (foundedVariation) {
        return {
          ...item,
          ...foundedVariation,
          ...foundedItem?.product?.node
        };
      }

      return {
        ...item,
        ...foundedItem,
        ...foundedItem?.product?.node
      };
    })
  }));
}

export function getYouTubeEmbed(url) {
  const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
  const match = url.match(regExp);
  if (match && match[2].length === 11) {
    return `https://www.youtube.com/embed/${match[2]}`;
  }

  return null;
}

export function stripTags(originalString) {
  return originalString?.replace(/(<([^>]+)>)/gi, '');
}

export const getLinkByUserByRole = (userRoles, databaseId) => {
  // fix incorrect redirect for seller
  // if (userRoles.some((el) => el.name === 'seller')) {
  //   return `/shop/${databaseId}/products`;
  // }

  return `/profile/${databaseId}/posts`;
};

export const getTagsFromString = (string) =>
  string
    ?.split(' ')
    ?.filter((item) => item.slice(0, 1) === '#')
    ?.map((item) => item.slice(1)) || [];

export const getTextFromString = (string) =>
  string
    ?.split(' ')
    ?.filter((item) => item.slice(0, 1) !== '#')
    .join(' ') || '';
export const getSelectedInterests = (interestData) => {
  let total = 0;
  if (interestData) {
    interestData.interests.nodes.map((el) => {
      if (el && el?.isFollowed === true) {
        total += 1;
      }

      return null;
    });
  }
  return total;
};
export const saveUserDataIntoLocalstorage = (data) => {};

export const getAttributesFilter = (filtersValue) => {
  const taxonomies = Object.keys(filtersValue)?.filter(
    (item) => !['categories', 'offers', 'price', 'priceValue', 'brands', 'productsWithShopcoins'].includes(item)
  ); // remove custom filters
  return taxonomies?.map((item) => ({
    taxonomy: `PA${item.replace(/[^A-Za-z]/g, '').toUpperCase()}`, // get taxonomy type
    terms: filtersValue?.[item]
  }));
};

export function getAddressComponents(place) {
  const {address_components: addressComponents} = place || {};

  let addrCountry;
  let addrDistrict;
  let addrCity;
  let addrRoad;
  let addrBlock;
  for (let i = 0; i < addressComponents?.length; i++) {
    for (let j = 0; j < addressComponents[i].types.length; j++) {
      switch (addressComponents[i].types[j]) {
        case 'country':
          addrCountry = addressComponents[i].long_name;
          break;
        case 'sublocality_level_1':
          addrDistrict = addressComponents[i].long_name;
          break;
        case 'locality':
          addrCity = addressComponents[i].long_name;
          break;
        case 'route':
          addrRoad = addressComponents[i].long_name.replace(/^\D+/g, '');
          break;
        case 'neighborhood':
          addrBlock = addressComponents[i].long_name.replace(/^\D+/g, '');
          break;
        default:
          break;
      }
    }
  }

  return {
    addrCountry: {value: addrCountry},
    addrDistrict: {value: addrDistrict},
    addrCity: {value: addrCity},
    addrRoad: {value: addrRoad},
    addrBlock: {value: addrBlock}
  };
}

export async function onShareProduct(p) {
  const {slug, ...rest} = p || {};
  try {
    if (navigator.share) {
      await navigator.share({
        ...rest,
        url: `${window.location.origin}/product/${slug}`
      });
    }
  } catch (err) {
    console.log(err);
  }
}
