export const validatePassword = (value) => {
  const isValid =
    /[A-Z]/.test(value) &&
    /[a-z]/.test(value) &&
    /[0-9]/.test(value) &&
    value.length >= 8 &&
    /[ !@#$%^~&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value);
  return {
    isValid,
    message: isValid
      ? ''
      : 'Your password MUST contain at least One Upper case, One Lowercase, One Numeral and One symbol “ @_-.'
  };
};

export const validateEmail = (value) => {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const isValid = re.test(String(value).toLowerCase());
  return {
    isValid,
    message: isValid ? '' : 'Please enter a valid Email'
  };
};
