// for queries with cursor pagination
// notice: invalid BE pagination for products query! (works correctly only with FE hacks)

import {useEffect, useState} from 'react';

const usePagination = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [currentCursor, setCurrentCursor] = useState('');
  const [cursorsHistory, setCursorsHistory] = useState(['']);

  useEffect(() => {
    setCurrentCursor([...cursorsHistory].pop());
  }, [cursorsHistory]);

  const goNextPage = (pageInfo) => {
    if (pageInfo?.hasNextPage && pageInfo?.endCursor) {
      const currentCursorsHistory = [...cursorsHistory];
      currentCursorsHistory.push(pageInfo?.endCursor);
      setCursorsHistory(currentCursorsHistory);
      setCurrentPage(currentPage + 1);
    }
  };

  const goPrevPage = (pageInfo) => {
    if (pageInfo?.hasPreviousPage && currentPage > 1) {
      const currentCursorsHistory = [...cursorsHistory];
      currentCursorsHistory.pop();
      setCursorsHistory(currentCursorsHistory);
      setCurrentPage(currentPage - 1);
    }
  };

  const resetPagination = () => {
    setCurrentCursor('');
    setCursorsHistory(['']);
    setCurrentPage(1);
  };

  return {
    currentPage,
    currentCursor,
    goPrevPage,
    goNextPage,
    resetPagination
  };
};

export default usePagination;
