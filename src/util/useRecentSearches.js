import {useEffect, useState} from 'react';
import {useLazyQuery, useMutation, gql} from '@apollo/client';
import _ from 'lodash';
import {userId} from 'util/heplers';

const GET_CUSTOMER_DATA = gql`
  query GetCustomerData($customerId: Int) {
    customer(customerId: $customerId) {
      username
      databaseId
      metaData(key: "recentSearches") {
        key
        value
      }
    }
  }
`;

const UPDATE_RECENT_SEARCHES = gql`
  mutation UpdateCustomer($value: String!) {
    updateRecentSearches(input: {recentSearches: $value}) {
      searches
    }
  }
`;

const useRecentSearches = () => {
  const [recentSearches, setRecentSearches] = useState([]);
  const [updateRecentSearches] = useMutation(UPDATE_RECENT_SEARCHES);
  const [getRecentSearches, {data: recentData, loading: recentLoading}] = useLazyQuery(GET_CUSTOMER_DATA, {
    variables: {customerId: userId},
    fetchPolicy: 'network-only'
  });

  useEffect(() => {
    if (userId) {
      getRecentSearches();
    }
  }, [getRecentSearches]);

  useEffect(() => {
    const searches = recentData?.customer?.metaData?.find((item) => item?.key === 'recentSearches');
    try {
      if (searches && userId) {
        setRecentSearches(JSON.parse(searches?.value));
      } else {
        setRecentSearches(JSON.parse(localStorage.getItem('recentSearches') || []));
      }
    } catch {}
  }, [recentLoading]);

  const clearRecentSearches = () => {
    setRecentSearches([]);
    try {
      if (userId) {
        updateRecentSearches({
          variables: {value: JSON.stringify([])}
        });
      } else {
        localStorage.setItem('recentSearches', JSON.stringify([]));
      }
    } catch {}
  };

  const saveSearch = (text, type, categorySlug, categoryName) => {
    const currentSearch = {text, type, categorySlug, categoryName};
    if (!isDuplicate(currentSearch) && !_.isEmpty(text)) {
      try {
        const currentRecentSearches = [...recentSearches, currentSearch];
        setRecentSearches(currentRecentSearches);
        if (userId) {
          updateRecentSearches({
            variables: {value: JSON.stringify(currentRecentSearches)}
          });
        } else {
          localStorage.setItem('recentSearches', JSON.stringify(currentRecentSearches));
        }
      } catch {}
    }
  };

  const isDuplicate = (searchObj) =>
    !!recentSearches?.find(
      (item) =>
        item.text === searchObj.text &&
        item.type === searchObj.type &&
        item.categorySlug === searchObj.categorySlug &&
        item.categoryName === searchObj.categoryName
    );

  return {
    clearRecentSearches,
    recentSearches,
    saveSearch
  };
};

export default useRecentSearches;
