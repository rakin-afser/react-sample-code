import {useEffect} from 'react';

const useInfiniteScroll = ({offset = 300, parentContainerClass, onLoad = () => {}}) => {
  useEffect(() => {
    const parent = document.querySelector(parentContainerClass) || window;
    parent.addEventListener('scroll', onScroll, {passive: true});
    return () => parent.removeEventListener('scroll', onScroll, {passive: true});
  });

  const onScroll = () => {
    if (parentContainerClass) {
      const parent = document.querySelector(parentContainerClass);
      if (parent?.scrollHeight - offset < parent?.scrollTop + document.body.clientHeight) {
        onLoad();
      }
    } else {
      const scrollHeight = Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.body.clientHeight,
        document.documentElement.clientHeight
      );
      if (scrollHeight - offset < window.pageYOffset + document.body.clientHeight) {
        onLoad();
      }
    }
  };
};

export default useInfiniteScroll;
