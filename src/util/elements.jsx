import React from 'react';
import Loader from 'components/Loader';

export function endElement(hasNextPage, loading, endRef) {
  if (loading) {
    return <Loader wrapperWidth="100%" wrapperHeight="auto" />;
  }

  if (!hasNextPage) {
    return null;
  }

  return <div ref={endRef} />;
}
