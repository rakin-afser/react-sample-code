const truncateParagraph = (paragraph = '', maxLength = 60) => {
  let text = paragraph
    .split('')
    .map((word, i) => (i < maxLength ? word : null))
    .join()
    .replace(/,/gi, '');
  return text + '...';
};

export default truncateParagraph;
