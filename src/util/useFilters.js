import {useEffect, useState} from 'react';
import _ from 'lodash';

const useFilters = (initialValue) => {
  const [filters, setFilters] = useState(initialValue);

  useEffect(() => {
    if (_.isEmpty(initialValue)) setFilters({});
  }, [initialValue]);

  const reloadFilters = (newValue) => {
    setFilters(newValue);
  };

  // values - for complex filter
  // Example: for price - itemId = 2 (for example "under $250") and values=[0,250])
  const changeFilters = (filter, itemId, values) => {
    if (!!filters?.[filter?.id] && !filter?.unic) {
      if (isItemSelected(filter?.id, itemId)) {
        // remove item from selected filters
        setFilters({
          ...filters,
          [filter?.id]: [...filters?.[filter?.id]?.filter((elId) => elId !== itemId)]
        });
      } else {
        // add item to exist array
        setFilters({
          ...filters,
          [filter?.id]: [...new Set([...filters?.[filter?.id], itemId])]
        });
      }
    } else {
      // add item
      if (filter?.id === 'price') {
        setFilters({...filters, [filter?.id]: [itemId], priceValue: values});
      } else if (filter?.id === 'sizeTypes') {
        setFilters({...filters, [filter?.id]: [itemId], sizes: []});
      } else if (filter?.id === 'categories') {
        // if category was changed clear all attribute filters too
        const currentAttributes = {};
        Object.keys(filters || {}).forEach((item) => {
          if (!['categories', 'offers', 'price', 'priceValue'].includes(item)) {
            // clear only attribute filters
            currentAttributes[item] = undefined;
          }
        });

        if (isItemSelected(filter?.id, itemId)) {
          setFilters({...filters, [filter?.id]: [], subCategory: [], ...currentAttributes});
        } else {
          setFilters({...filters, [filter?.id]: [itemId], subCategory: [], ...currentAttributes});
        }
      } else {
        setFilters({...filters, [filter?.id]: [itemId]});
      }
    }
  };

  const isItemSelected = (filterId, itemId) => {
    return !!filters?.[filterId]?.includes(itemId);
  };

  const clearFilter = (filterId) => {
    setFilters({...filters, [filterId]: []});
  };

  const clearFilterItem = (filterId, itemId) => {
    let currentFilters = {};
    if (filterId === 'categories') {
      // if category was changed clear all attribute filters too
      const currentAttributes = {};
      Object.keys(filters || {}).forEach((item) => {
        if (!['categories', 'offers', 'price', 'priceValue'].includes(item)) {
          // clear only attribute filters
          currentAttributes[item] = undefined;
        }
      });
      currentFilters = {
        ...filters,
        [filterId]: [...filters?.[filterId]?.filter((elId) => elId !== itemId)],
        ...currentAttributes
      };
    } else {
      currentFilters = {
        ...filters,
        [filterId]: [...filters?.[filterId]?.filter((elId) => elId !== itemId)]
      };
    }

    setFilters(currentFilters);
    return currentFilters;
  };

  const clearAll = () => {
    setFilters({});
  };

  // console.log('filters', filters);

  return {
    filters,
    reloadFilters,
    clearAll,
    clearFilter,
    clearFilterItem,
    changeFilters,
    isItemSelected
  };
};

export default useFilters;
