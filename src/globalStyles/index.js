import styled from 'styled-components/macro';
import {Button} from 'antd';
import {primaryColor, grayTextColor, grayBackroundColor, transparentTextColor} from 'constants/colors';

export const FlexContainer = styled.div`
  display: flex;
  width: ${({width}) => width || 'auto'};
  justify-content: ${({justifyContent}) => justifyContent || 'space-between'};
  align-items: ${({alignItems}) => alignItems || 'center'};
  flex-direction: ${({flexDirection}) => flexDirection || 'row'};
  flex-wrap: ${({flexWrap}) => flexWrap || 'nowrap'};
`;

export const ContentWrapper = styled.div`
  ${({banner}) =>
    banner &&
    `
    display: flex;
    align-items: center;
    justify-content: space-between;
  `}
  margin: 0 auto;
  margin-top: ${({marginTop}) => marginTop || 0}px;
  width: 100%;
  max-width: 1170px;
  position: relative;

  .ant-dropdown {
    width: 100%;
  }

  .menu-item {
    font-size: 16px;
    color: #000;
    font-weight: 500;
    cursor: pointer;
    transition: all 0.3s ease-in-out;
    letter-spacing: 0;
    padding: 40px 25px;
    position: relative;
    display: block;

    &:hover {
      color: #ed484f;
    }
  }
`;

export const FollowButton = styled(Button)`
  display: flex;
  align-items: center;
  justify-content: center;
  background: transparent !important;
  padding: 1px 16px 0 !important;
  color: ${primaryColor} !important;
  font-size: 14px !important;
  border: 1px solid ${primaryColor} !important;
  border-radius: 24px !important;
  height: 24px !important;
  width: 104px !important;
  color: ${(props) => (props.isfollowing === true ? grayTextColor : primaryColor)} !important;
  border-color: ${(props) => (props.isfollowing === true ? grayBackroundColor : primaryColor)} !important;
  transition: ease 0.4s;
`;

export const FollowersCount = styled.div`
  font-size: 12px;
  color: ${transparentTextColor};
`;
