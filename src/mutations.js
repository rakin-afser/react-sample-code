import {gql} from '@apollo/client';
import {
  CartFragment,
  CommentDesktopFragment,
  CommentFragment,
  FlashPostFragment,
  testSampleSubcartsFragment,
  PostFragment,
  ShippingAddressFragment,
  UpdatedCartItemFragment
} from 'fragments';

export const TRIGGER_LIKE = gql`
  mutation TriggerLike($input: AddUpdateLikedProductInput!) {
    addUpdateLikedProduct(input: $input) {
      isLiked
      id
      totalLikes
    }
  }
`;

export const ADD_UPDATE_LIKED_PRODUCT = gql`
  mutation AddUpdateLikedProduct($input: AddUpdateLikedProductInput!) {
    addUpdateLikedProduct(input: $input) {
      product {
        id
        databaseId
        isLiked
        totalLikes
      }
    }
  }
`;

export const ADD_UPDATE_LIKED_COMMENT = gql`
  mutation AddUpdateLikedComment($input: AddUpdateLikedCommentInput!) {
    addUpdateLikedComment(input: $input) {
      comment {
        id
        databaseId
        isLiked
        totalLikes
      }
    }
  }
`;

export const SendOTP = gql`
  mutation SendOTP($input: SendOTPInput!) {
    sendOTP(input: $input) {
      message
      code
    }
  }
`;

export const ValidateOTP = gql`
  mutation ValidateOTP($input: ValidateOTPInput!) {
    validateOTP(input: $input) {
      message
      code
    }
  }
`;

export const VALIDATE_USERNAME = gql`
  mutation ValidateUsername($input: ValidateUsernameInput!) {
    validateUsername(input: $input) {
      message
      code
    }
  }
`;

export const Login = gql`
  mutation Login($input: LoginInput!) {
    login(input: $input) {
      authToken
      wooSessionToken
      user {
        id
        databaseId
        capabilities
      }
    }
  }
`;

export const UpdateItemQuantities = gql`
  ${CartFragment}
  ${UpdatedCartItemFragment}
  mutation UpdateItemQuantities($input: UpdateItemQuantitiesInput!) {
    updateItemQuantities(input: $input) {
      cart {
        ...CartFragment
      }
      updated {
        ...UpdatedCartItemFragment
      }
    }
  }
`;

export const REMOVE_ITEMS_FROM_CART = gql`
  ${CartFragment}
  mutation RemoveItemsFromCart($input: RemoveItemsFromCartInput!) {
    removeItemsFromCart(input: $input) {
      cart {
        ...CartFragment
      }
    }
  }
`;

export const CREATE_COMMENT_DESKTOP = gql`
  ${CommentDesktopFragment}
  mutation CreateComment($input: CreateCommentInput!) {
    createComment(input: $input) {
      success
      comment {
        ...CommentDesktopFragment
      }
    }
  }
`;

export const CREATE_COMMENT = gql`
  ${CommentFragment}
  mutation CreateComment($input: CreateCommentInput!) {
    createComment(input: $input) {
      success
      comment {
        ...CommentFragment
      }
    }
  }
`;

export const ADD_SHIPPING_ADDRESS = gql`
  ${ShippingAddressFragment}
  mutation AddShippingAddress($input: AddShippingAddressInput!) {
    addShippingAddress(input: $input) {
      shippingAddresses {
        ...ShippingAddressFragment
      }
      shippingAddressesSuccess
    }
  }
`;

export const REMOVE_SHIPPING_ADDRESS = gql`
  ${ShippingAddressFragment}
  mutation RemoveShippingAddress($input: RemoveShippingAddressInput!) {
    removeShippingAddress(input: $input) {
      shippingAddresses {
        ...ShippingAddressFragment
      }
      shippingAddressesSuccess
    }
  }
`;

export const EDIT_SHIPPING_ADDRESS = gql`
  ${ShippingAddressFragment}
  mutation EditShippingAddress($input: EditShippingAddressInput!) {
    editShippingAddress(input: $input) {
      shippingAddresses {
        ...ShippingAddressFragment
      }
      shippingAddressesSuccess
    }
  }
`;

export const CALCULATE_SUBCARTS = gql`
  ${CartFragment}
  ${testSampleSubcartsFragment}
  ${ShippingAddressFragment}
  mutation CalculateSubcarts($input: CalculateSubcartsInput!) {
    calculateSubcarts(input: $input) {
      cart {
        ...CartFragment
        subcarts {
          ...testSampleSubcartsFragment
        }
      }
      shippingAddresses {
        ...ShippingAddressFragment
      }
    }
  }
`;

export const SET_SUBCART_SHIPPING_METHOD = gql`
  ${CartFragment}
  ${testSampleSubcartsFragment}
  mutation SetSubcartShippingMethod($input: SetSubcartShippingMethodInput!) {
    setSubcartShippingMethod(input: $input) {
      calculateSuccess
      cart {
        ...CartFragment
        subcarts {
          ...testSampleSubcartsFragment
        }
      }
    }
  }
`;

export const testSample_ORDER_CREATE = gql`
  mutation testSampleOrderCreate($input: testSampleOrderCreateInput!) {
    testSampleOrderCreate(input: $input) {
      orderCreateId
      transactionUrl
      errors {
        code
        description
      }
    }
  }
`;

export const testSample_VALIDATE_TAPPAY_AUTH_TOKEN = gql`
  mutation testSampleValidateTapPayAuthToken($input: testSampleValidateTapPayAuthTokenInput!) {
    testSampleValidateTapPayAuthToken(input: $input) {
      success
      status
    }
  }
`;

export const VALIDATE_EMAIL = gql`
  mutation ValidateEmail($input: ValidateEmailInput!) {
    validateEmail(input: $input) {
      code
    }
  }
`;
export const Subscribe = gql`
  mutation MyMutation($input: AddSubscriberInput!) {
    addSubscriber(input: $input) {
      message
    }
  }
`;

export const UpdatedUser = gql`
  mutation UpdatedUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      clientMutationId
      user {
        databaseId
        nickname
        name
        capabilities
        description
        username
        email
        id
        userId
        gender
        firstName
        lastName
        education
        url
        company
        job_title
        country
        noti_publish_posts
        noti_post_gets_like
        noti_post_gets_comment
        noti_someone_reacts_to_comment
        noti_list_followed
        noti_list_gets_a_like
        noti_follows_me
        noti_send_message
        noti_reward_earns
        noti_reward_coins_change
        noti_received_message
        noti_joins_by_my_link
        noti_upcoming_birthdays_of_my_friends
        avatar {
          scheme
          default
          foundAvatar
          forceDefault
          extraAttr
          rating
          url
        }
      }
    }
  }
`;
export const REGISTER_SOCIAL_CUSTOMER = gql`
  mutation RegisterSocialUser($input: RegisterNewCustomerInput!) {
    registerNewCustomer(input: $input) {
      message
      user_id
    }
  }
`;

export const ADD_TO_CART = gql`
  mutation AddToCart($input: AddToCartInput!) {
    addToCart(input: $input) {
      cart {
        contents {
          nodes {
            key
            quantity
            total
            product {
              node {
                id
                name
                slug
              }
            }
          }
        }
        subcarts {
          subcarts {
            shipping {
              chosenMethodId
              availableMethods {
                cost
                id
                label
                taxes
              }
            }
          }
        }
      }
    }
  }
`;

export const ADD_RECENTLY_VIEWED = gql`
  mutation AddRecentlyViewed($input: AddRecentlyViewedInput!) {
    addRecentlyViewed(input: $input) {
      message
    }
  }
`;

export const FOLLOW_STORE = gql`
  mutation FollowStore($storeId: Int!, $userId: Int!) {
    followUnfollowStore(input: {id: $storeId, user_id: $userId}) {
      clientMutationId
      status
    }
  }
`;

export const FOLLOW_UNFOLLOW_STORE = gql`
  mutation FollowUnfollowStore($input: FollowUnfollowStoreInput!) {
    followUnfollowStore(input: $input) {
      store {
        id
        followed
      }
    }
  }
`;

export const FOLLOW_UNFOLLOW_USER = gql`
  mutation FollowUnfollowUser($input: FollowUnfollowUserInput!) {
    followUnfollowUser(input: $input) {
      user {
        id
        isFollowed
      }
    }
  }
`;

export const FOLLOW_UNFOLLOW_CATEGORY = gql`
  mutation FollowUnfollowCategory($input: FollowUnfollowCategoryInput!) {
    followUnfollowCategory(input: $input) {
      category {
        id
        isFollowed
      }
    }
  }
`;

export const FOLLOW_UNFOLLOW_LIST = gql`
  mutation FollowUnfollowList($input: FollowUnfollowListInput!) {
    followUnfollowList(input: $input) {
      list {
        id
        isFollowed
      }
    }
  }
`;

export const CREATE_POST = gql`
  ${PostFragment}
  mutation CreatePost($input: CreatePostInput!) {
    createPost(input: $input) {
      post {
        ...PostFragment
      }
    }
  }
`;

export const CREATE_FLASH_POST = gql`
  ${FlashPostFragment}
  mutation CreatePost($input: CreatePostInput!) {
    createPost(input: $input) {
      post {
        ...FlashPostFragment
      }
    }
  }
`;

export const DELETE_POST = gql`
  mutation DeletePost($input: UpdatePostInput!) {
    updatePost(input: $input) {
      post {
        id
      }
    }
  }
`;

export const ADD_UPDATE_LIKED_POST = gql`
  mutation AddUpdateLikedPost($input: AddUpdateLikedPostInput!) {
    addUpdateLikedPost(input: $input) {
      post {
        id
        isLiked
        totalLikes
      }
    }
  }
`;

export const FOLLOW_INTEREST = gql`
  mutation FollowInterest($interestId: Int!, $userId: Int) {
    followUnfollowInterest(input: {id: $interestId, user_id: $userId}) {
      interest {
        id
        isFollowed
      }
    }
  }
`;

export const FOLLOW_UNFOLLOW_INTEREST = gql`
  mutation FollowUnfollowInterest($input: FollowUnfollowInterestInput!) {
    followUnfollowInterest(input: $input) {
      interest {
        id
        isFollowed
      }
    }
  }
`;

export const FROM_CART_TO_SAVE_FOR_LATER = gql`
  mutation FromCartToSaveForLater($productId: ID!, $clientMutationId: String, $variationId: ID) {
    sflMoveItemFromCart(
      input: {productId: $productId, clientMutationId: $clientMutationId, variationId: $variationId}
    ) {
      clientMutationId
      success
    }
  }
`;

export const FROM_SAVE_FOR_LATER_TO_CART = gql`
  mutation FromSaveForLaterToCart($productId: ID!, $clientMutationId: String, $variationId: ID) {
    sflMoveItemToCart(input: {productId: $productId, clientMutationId: $clientMutationId, variationId: $variationId}) {
      clientMutationId
      success
    }
  }
`;

export const REMOVE_FROM_SAVE_FOR_LATER = gql`
  mutation RemoveFromSaveForLater($productId: ID!, $clientMutationId: String, $variationId: ID) {
    sflRemoveItem(input: {productId: $productId, clientMutationId: $clientMutationId, variationId: $variationId}) {
      clientMutationId
      success
    }
  }
`;

export const ADD_REPORT = gql`
  mutation AddReport($input: AddReportInput!) {
    addReport(input: $input) {
      report {
        id
      }
    }
  }
`;

export const ADD_SUBSCRIBER = gql`
  mutation AddSubscriber($input: AddSubscriberInput!) {
    addSubscriber(input: $input) {
      message
    }
  }
`;

export const APPLY_MIDCOINS_DISCOUNT = gql`
  mutation ApplyMidcoinsDiscount($input: ApplyMidcoinsDiscountInput!) {
    applyMidcoinsDiscount(input: $input) {
      cart {
        appliedMidCoins {
          discountAmount
          qty
        }
        total
      }
    }
  }
`;

export const REMOVE_MIDCOINS_DISCOUNT = gql`
  mutation RemoveMidcoinsDiscount {
    removeMidcoinsDiscount(input: {}) {
      cart {
        appliedMidCoins {
          discountAmount
          qty
        }
        total
      }
    }
  }
`;

export const APPLY_COUPON = gql`
  mutation ApplyCoupon($input: ApplyCouponInput!) {
    applyCoupon(input: $input) {
      cart {
        appliedCoupons {
          code
          discountAmount
        }
        total
      }
    }
  }
`;

export const REMOVE_COUPONS = gql`
  mutation RemoveCoupon($input: RemoveCouponsInput!) {
    removeCoupons(input: $input) {
      cart {
        appliedCoupons {
          code
          discountAmount
        }
        total
      }
    }
  }
`;

export const testSample_GET_SAVED_CARD_TOKEN = gql`
  mutation testSampleGetSavedCardToken($input: testSampleGetSavedCardTokenInput!) {
    testSampleGetSavedCardToken(input: $input) {
      token
      success
    }
  }
`;
