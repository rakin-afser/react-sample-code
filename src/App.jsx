import React, {Suspense, lazy} from 'react';
import {ApolloProvider} from '@apollo/client';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import {ThemeProvider} from 'styled-components';
import {CookiesProvider} from 'react-cookie';
import apolloClient from './apolloClient';

import useGlobal from 'store';
import Loader from 'components/Loader';
import AuthProvider from 'containers/AuthProvider';
import TalkJsSession from 'components/Messenger/components/TalkJs/TalkJsSession';
import pusherJs from 'services/pusher';

import 'swiper/swiper.min.css';
import 'swiper/components/navigation/navigation.min.css';
import 'swiper/components/pagination/pagination.min.css';
import 'swiper/components/effect-fade/effect-fade.min.css';

const Page404 = lazy(() => import('containers/Page404'));
const HomePage = lazy(() => import('containers/HomePage'));
const ProductPage = lazy(() => import('containers/ProductPage'));
const UIkit = lazy(() => import('components/UI-kit'));
const SearchResult = lazy(() => import('containers/SearchResult'));
const MyProfile = lazy(() => import('containers/MyProfile'));
const AccountSettings = lazy(() => import('containers/AccountSettings'));
const CartPage = lazy(() => import('containers/CartPage'));
const ShopPage = lazy(() => import('containers/ShopPage'));
const UserOnboardingPage = lazy(() => import('containers/UserOnboardingPage'));
const CheckoutPage = lazy(() => import('containers/GuestCheckoutPage'));
const AboutPage = lazy(() => import('containers/AboutPage'));
const Feed = lazy(() => import('containers/Feed'));
const BuyerLanding = lazy(() => import('containers/BuyerLanding'));
const DealsPage = lazy(() => import('containers/DealsPage'));
const SellerLanding = lazy(() => import('containers/SellerLanding'));
const Categories = lazy(() => import('containers/Categories'));
const AuthContainer = lazy(() => import('containers/AuthContainer'));
const UserPage = lazy(() => import('containers/UserPage'));
const PrivacyPolicy = lazy(() => import('containers/PrivacyPolicy'));
const TermsConditions = lazy(() => import('containers/TermsConditions'));
const CookiePolicy = lazy(() => import('containers/CookiePolicy'));
const Post = lazy(() => import('containers/Post'));
const New = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Posts/containers/New'));

pusherJs.init();

function App() {
  const [globalState] = useGlobal();
  const {isArabic} = globalState;

  const theme = {
    isArabic
  };

  return (
    <CookiesProvider>
      <ApolloProvider client={apolloClient}>
        <AuthProvider>
          <ThemeProvider theme={theme}>
            <TalkJsSession>
              <Router>
                <Suspense fallback={<Loader />}>
                  <Switch>
                    {/* General Routes */}
                    <Route exact path="/" component={HomePage} />
                    <Route exact path="/ui-kit" component={UIkit} />
                    <Route exact path="/product/:productSlug" component={ProductPage} />
                    <Route exact path="/post/new" component={New} />
                    <Route exact path="/post/:postId" component={Post} />
                    <Route
                      exact
                      path="/categories/:categorySlug?/:subCategorySlug1?/:subCategorySlug2?"
                      component={Categories}
                    />
                    <Route exact path="/search/:page?/:param?" component={SearchResult} />
                    <Route
                      exact
                      path="/profile/:page(lists|posts|products|activity|messages|notifications|settings|edit|orders|rewards|promote|account-settings|dashboard|mylikes)/:slug?"
                      component={MyProfile}
                    />
                    <Route
                      exact
                      path="/profile/:userParam/:page(guest|lists|posts|products|posts|activity|messages|notifications|settings|edit|orders|rewards|promote|account-settings|dashboard|mylikes)/:slug?"
                      component={MyProfile}
                    />
                    <Route
                      exact
                      path="/influencer/profile/:userParam/:page(guest|lists|posts|products|posts|activity|messages|notifications|settings|edit|orders|rewards|promote|account-settings|dashboard)/:slug?"
                      component={MyProfile}
                    />
                    <Route exact path="/cart" component={CartPage} />
                    <Route exact path="/shop/:shopUrl/:page(products|posts|feedback|about)/" component={ShopPage} />
                    <Route
                      exact
                      path="/shop/:shopUrl/:page(categories)/:categoryName?/:subCategoryName?/:subCategoryName2?"
                      component={ShopPage}
                    />
                    <Route exact path="/shop/:shopUrl" component={ShopPage} />
                    <Route exact path="/user/:userName/lists/:slug?" component={UserPage} />
                    <Route path="/user-onboarding" component={UserOnboardingPage} />
                    <Route
                      exact
                      path="/checkout/:step(start|verify|details|address|review|payment|payment-handler|failed|success)"
                      component={CheckoutPage}
                    />
                    <Route exact path="/account-settings/:page?" component={AccountSettings} />

                    <Route exact path="/feed" component={Feed} />
                    <Route exact path="/buyer-landing" component={BuyerLanding} />
                    <Route exact path="/seller-landing" component={SellerLanding} />
                    {/* Auth Routes */}
                    <Route path="/auth">
                      <AuthContainer />
                    </Route>
                    {/* About Us */}
                    <Route exact path="/about/:page" component={AboutPage} />
                    {/* Privacy Policy */}
                    <Route exact path="/privacy-policy" component={PrivacyPolicy} />
                    <Route exact path="/terms-conditions" component={TermsConditions} />
                    <Route exact path="/cookie-policy" component={CookiePolicy} />
                    <Route
                      exact
                      path="/:page(deals|most-liked|featured|fashion|electronics|recently-viewed|popular-categories)"
                      component={DealsPage}
                    />
                    {/* 404 */}
                    <Route exact path="/404" component={Page404} />
                    {/* <Route
                      component={() => {
                        return <Redirect to="/404" />;
                      }}
                    /> */}
                    <Route path="*"
                        component={() => {
                          return window.location.href = `${window.location.origin}/404`        
                        }}/>
                  </Switch>
                </Suspense>
              </Router>
            </TalkJsSession>
          </ThemeProvider>
        </AuthProvider>
      </ApolloProvider>
    </CookiesProvider>
  );
}

export default App;
