import React from 'react';

const IconPreloader = ({
  className, width, height, style, fill,
}) => (
  <svg
    className={`icon icon--preloader${className ? ` ${className}` : ''}`}
    style={style}
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    viewBox="0 0 100 100"
  >
    <g transform="translate(76,50)">
      <g transform="rotate(0)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="1">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="-0.875s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="-0.875s"
          />
        </circle>
      </g>
    </g>
    <g transform="translate(68.38477631085024,68.38477631085024)">
      <g transform="rotate(45)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="0.875">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="-0.75s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="-0.75s"
          />
        </circle>
      </g>
    </g>
    <g transform="translate(50,76)">
      <g transform="rotate(90)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="0.75">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="-0.625s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="-0.625s"
          />
        </circle>
      </g>
    </g>
    <g transform="translate(31.615223689149765,68.38477631085024)">
      <g transform="rotate(135)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="0.625">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="-0.5s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="-0.5s"
          />
        </circle>
      </g>
    </g>
    <g transform="translate(24,50)">
      <g transform="rotate(180)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="0.5">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="-0.375s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="-0.375s"
          />
        </circle>
      </g>
    </g>
    <g transform="translate(31.61522368914976,31.615223689149765)">
      <g transform="rotate(225)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="0.375">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="-0.25s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="-0.25s"
          />
        </circle>
      </g>
    </g>
    <g transform="translate(49.99999999999999,24)">
      <g transform="rotate(270)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="0.25">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="-0.125s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="-0.125s"
          />
        </circle>
      </g>
    </g>
    <g transform="translate(68.38477631085024,31.61522368914976)">
      <g transform="rotate(315)">
        <circle cx="0" cy="0" r="5" fill="#ed484f" fillOpacity="0.125">
          <animateTransform
            attributeName="transform"
            type="scale"
            begin="0s"
            values="1.34 1.34;1 1"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
          />
          <animate
            attributeName="fill-opacity"
            keyTimes="0;1"
            dur="1s"
            repeatCount="indefinite"
            values="1;0"
            begin="0s"
          />
        </circle>
      </g>
    </g>
  </svg>
);

IconPreloader.defaultProps = {
  width: 180,
  height: 180,
  fill: '#000105',
};

export default IconPreloader;
