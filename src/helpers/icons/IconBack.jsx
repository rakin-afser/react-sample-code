import React from 'react';

const IconBack = ({
  className, width, height, style, fill, stroke
}) => (
  <svg
    className={`icon icon--back${className ? ` ${className}` : ''}`}
    style={style}
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    viewBox="0 0 28 28"
  >
    <g clipPath="url(#clipBack)">
      <path d="M19.8337 14L8.16699 14" stroke={stroke} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" fill="none"/>
      <path d="M14.0003 19.8333L8.16699 14L14.0003 8.16667" stroke={stroke} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" fill="none"/>
    </g>
    <defs>
      <clipPath id="clipBack">
        <rect y="14" width="19.799" height="19.799" transform="rotate(-45 0 14)" fill={fill}/>
      </clipPath>
    </defs>
  </svg>
);

IconBack.defaultProps = {
  width: 28,
  height: 28,
  stroke: '#999999',
  fill: '#fff',
};

export default IconBack;
