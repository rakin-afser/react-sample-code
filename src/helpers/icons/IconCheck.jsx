import React from 'react';

const IconCheck = ({
  className, width, height, style, fill,
}) => (
  <svg
    className={`icon icon--check${className ? ` ${className}` : ''}`}
    style={style}
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    viewBox="0 0 18 13"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M16.9815 0.851982C17.3395 1.20996 17.3395 1.79036 16.9815 2.14834L6.89818 12.2317C6.5402 12.5897 5.9598 12.5897 5.60182 12.2317L1.01849 7.64834C0.660505 7.29036 0.660505 6.70996 1.01849 6.35198C1.37647 5.994 1.95687 5.994 2.31485 6.35198L6.25 10.2871L15.6852 0.851982C16.0431 0.494001 16.6235 0.494001 16.9815 0.851982Z"
      fill={fill}
    />
  </svg>
);

IconCheck.defaultProps = {
  width: 16.5,
  height: 12,
  fill: 'white',
};

export default IconCheck;
