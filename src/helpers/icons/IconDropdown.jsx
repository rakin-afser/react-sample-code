import React from 'react';

const IconDropdown = ({
  className, width, height, style, fill,
}) => (
  <svg
    className={`icon icon--dropdown${className ? ` ${className}` : ''}`}
    style={style}
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    viewBox="0 0 12 9"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0.251051 0.898047C0.585786 0.535938 1.1285 0.535938 1.46323 0.898047L6 5.80582L10.5368 0.898047C10.8715 0.535938 11.4142 0.535938 11.7489 0.898047C12.0837 1.26016 12.0837 1.84725 11.7489 2.20936L6.60609 7.7728C6.27136 8.1349 5.72864 8.1349 5.39391 7.7728L0.251051 2.20936C-0.0836838 1.84725 -0.0836838 1.26016 0.251051 0.898047Z"
      fill={fill}
    />
  </svg>
);

IconDropdown.defaultProps = {
  width: 12,
  height: 9,
  fill: '#7A7A7A',
};

export default IconDropdown;
