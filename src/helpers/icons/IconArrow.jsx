import React from 'react';

const IconArrow = ({
  className, width, height, style, fill,
}) => (
  <svg
    className={`icon icon--arrow${className ? ` ${className}` : ''}`}
    style={style}
    xmlns="http://www.w3.org/2000/svg"
    width={width}
    height={height}
    viewBox="0 0 8 13"
  >
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M0.756282 12.2489C0.414573 11.9142 0.414573 11.3715 0.756282 11.0368L5.38756 6.5L0.756282 1.96323C0.414573 1.6285 0.414573 1.08579 0.756282 0.751051C1.09799 0.416317 1.65201 0.416317 1.99372 0.751051L7.24372 5.89391C7.58543 6.22864 7.58543 6.77136 7.24372 7.10609L1.99372 12.2489C1.65201 12.5837 1.09799 12.5837 0.756282 12.2489Z"
      fill={fill}
    />
  </svg>
);

IconArrow.defaultProps = {
  width: 8,
  height: 13,
  fill: 'white',
};

export default IconArrow;
