import {ApolloClient, InMemoryCache, ApolloLink, HttpLink} from '@apollo/client';
import {cartItemsVar, currencyVar, userVar} from 'hooks/reactiveVars';
import {setContext} from '@apollo/client/link/context';
// import { onError } from '@apollo/client/link/error';

// import { ApolloClient } from 'apollo-client';
// import { onError } from 'apollo-link-error';
// import { withClientState } from 'apollo-link-state';
// import { HttpLink } from 'apollo-link-http';
// import { InMemoryCache } from 'apollo-cache-inmemory';
// import { ApolloLink } from 'apollo-link';
// import { RetryLink } from 'apollo-link-retry';

// const cache = new InMemoryCache({
//   cacheRedirects: {
//     Query: {
//       movie: (_, { id }, { getCacheKey }) => getCacheKey({ __typename: 'Movie', id })
//     }
//   }
// });

const testSampleLink = new HttpLink({
  uri: `${process.env.REACT_APP_testSample_URI}`
  // credentials: 'include'
});

/**
 * Middleware operation
 * If we have a session token in localStorage, add it to the GraphQL request as a Session header.
 */
export const middleware = new ApolloLink((operation, forward) => {
  /**
   * If session data exist in local storage, set value as session header.
   */
  const localSession = localStorage.getItem('woo-session');
  const session = localSession ? localSession.replace('Session ', '') : null;

  if (session) {
    operation.setContext(({headers = {}}) => ({
      headers: {
        'woocommerce-session': `Session ${session}`
      }
    }));
  }

  return forward(operation);
});

/**
 * Afterware operation
 * This catches the incoming session token and stores it in localStorage, for future GraphQL requests.
 */
export const afterware = new ApolloLink((operation, forward) => {
  return forward(operation).map((response) => {
    //
    // Check for session header and update session in local storage accordingly.
    //
    const context = operation.getContext();
    const {
      response: {headers}
    } = context;
    const session = headers.get('woocommerce-session');
    if (session) {
      if (localStorage.getItem('woo-session') !== session) {
        localStorage.setItem('woo-session', headers.get('woocommerce-session'));
      }
    }

    return response;
  });
});

const authLink = setContext((_, {headers}) => {
  // get the authentication token from local storage if it exists
  const token = localStorage.getItem('token');
  // return the headers to the context so httpLink can read them
  if (token) {
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    };
  }
  return {
    headers
  };
});

export const client = new ApolloClient({
  link: ApolloLink.from([
    // onError(({ graphQLErrors, networkError }) => {
    //   if (graphQLErrors) {
    //     console.error(graphQLErrors);
    //   }
    //   if (networkError) {
    //     console.error(networkError);
    //   }
    // }),
    // withClientState({
    //   defaults: {
    //     isConnected: true
    //   },
    //   resolvers: {
    //     Mutation: {
    //       updateNetworkStatus: (_, { isConnected }, { cache: cacheObj }) => {
    //         cacheObj.writeData({ data: { isConnected } });
    //         return null;
    //       }
    //     }
    //   },
    //   cache
    // }),
    middleware.concat(afterware.concat(authLink.concat(testSampleLink)))
  ]),
  cache: new InMemoryCache({
    typePolicies: {
      Avatar: {
        keyFields: ['url']
      },
      LineItem: {
        keyFields: ['databaseId']
      },
      // CartItem: {
      //   keyFields: ['key']
      // },
      Query: {
        fields: {
          cart: {
            merge(existing = {}, incoming = {}, {field}) {
              return {...existing, ...incoming};
            }
          },
          // posts: {
          //   keyArgs: ['first', 'after'],
          //   merge(existing = {}, incoming, {readField, args, fieldName, field, storeFieldName}) {
          //     const oldNodes = existing?.nodes || [];
          //     const newNodes = incoming?.nodes || [];
          //     // make sure that all posts are unique
          //     const nodes = [...oldNodes, ...newNodes]?.filter(
          //       (v, i, a) => a.findIndex((f) => readField('id', f) === readField('id', v)) === i
          //     );
          //     return {...incoming, nodes};
          //   }
          // },
          products: {
            keyArgs: ['first', 'last', 'where'],
            merge(existing = {}, incoming, {readField}) {
              const oldNodes = existing?.nodes || [];
              const newNodes = incoming?.nodes || [];
              // make sure that all posts are unique
              const nodes = [...oldNodes, ...newNodes]?.filter(
                (v, i, a) => a.findIndex((f) => readField('id', f) === readField('id', v)) === i
              );
              return {...existing, ...incoming, nodes};
            },
            read(existing) {
              return existing;
            }
          },
          users: {
            keyArgs: ['first', 'last', 'where'],
            merge(existing = {}, incoming, {readField}) {
              const oldNodes = existing?.nodes || [];
              const newNodes = incoming?.nodes || [];
              // make sure that all posts are unique
              const nodes = [...oldNodes, ...newNodes]?.filter(
                (v, i, a) => a.findIndex((f) => readField('id', f) === readField('id', v)) === i
              );
              return {...existing, ...incoming, nodes};
            },
            read(existing) {
              return existing;
            }
          },
          stores: {
            keyArgs: ['first', 'last', 'where'],
            merge(existing = {}, incoming, {readField}) {
              const oldNodes = existing?.nodes || [];
              const newNodes = incoming?.nodes || [];
              // make sure that all posts are unique
              const nodes = [...oldNodes, ...newNodes]?.filter(
                (v, i, a) => a.findIndex((f) => readField('id', f) === readField('id', v)) === i
              );
              return {...existing, ...incoming, nodes};
            },
            read(existing) {
              return existing;
            }
          },
          lists: {
            keyArgs: ['first', 'last', 'where'],
            merge(existing = {}, incoming, {readField}) {
              const oldNodes = existing?.nodes || [];
              const newNodes = incoming?.nodes || [];
              // make sure that all posts are unique
              const nodes = [...oldNodes, ...newNodes]?.filter(
                (v, i, a) => a.findIndex((f) => readField('id', f) === readField('id', v)) === i
              );
              return {...existing, ...incoming, nodes};
            },
            read(existing) {
              return existing;
            }
          },
          testSampleBalanceData: {
            keyArgs: ['first', 'last', 'where'],
            merge(existing = {}, incoming, {readField}) {
              const oldNodes = existing?.nodes || [];
              const newNodes = incoming?.nodes || [];
              // make sure that all posts are unique
              const nodes = [...oldNodes, ...newNodes]?.filter(
                (v, i, a) => a.findIndex((f) => readField('id', f) === readField('id', v)) === i
              );
              return {...existing, ...incoming, nodes};
            },
            read(existing) {
              return existing;
            }
          },
          userData: {
            read() {
              return userVar();
            }
          },
          cartItems: {
            read() {
              return cartItemsVar();
            }
          },
          currency: {
            read() {
              return currencyVar();
            }
          }
        }
      }
    }
  })
});

export default client;
