import {gql} from '@apollo/client';

export const MediumMediaItemFragment = gql`
  fragment MediumMediaItemFragment on MediaItem {
    id
    sourceUrl(size: MEDIUM)
    altText
  }
`;

export const UpdatedCartItemFragment = gql`
  fragment UpdatedCartItemFragment on CartItem {
    key
    product {
      node {
        id
        databaseId
        name
        slug
        ... on SimpleProduct {
          regularPrice
          salePrice
        }
        ... on VariableProduct {
          regularPrice
          salePrice
        }
        image {
          id
          sourceUrl(size: LARGE)
          altText
        }
        databaseId
        seller {
          id
          name
          gravatar
          rating
        }
        productCategories {
          nodes {
            id
            slug
          }
        }
      }
    }
    midCoins
    shopCoins
    quantity
    variation {
      attributes {
        attributeId
        id
        label
        name
        value
      }
    }
  }
`;

export const CartFragment = gql`
  ${MediumMediaItemFragment}
  fragment CartFragment on Cart {
    total
    totalQuantity
    appliedShopCoins {
      discountAmount
    }
    appliedMidCoins {
      discountAmount
      qty
    }
    appliedCoupons {
      code
      discountAmount
    }
    contents {
      nodes {
        total
        key
        quantity
        variation {
          node {
            id
            databaseId
            regularPrice
            salePrice
            image {
              ...MediumMediaItemFragment
            }
          }
          attributes {
            id
            label
            value
          }
        }
        product {
          node {
            id
            databaseId
            name
            slug
            ... on SimpleProduct {
              regularPrice
              salePrice
            }
            ... on VariableProduct {
              regularPrice
              salePrice
            }
            image {
              id
              sourceUrl(size: LARGE)
              altText
            }
            databaseId
            seller {
              id
              name
              gravatar
              rating
            }
            productCategories {
              nodes {
                id
                slug
              }
            }
          }
        }
        shopCoins
        midCoins
      }
    }
    sflItems {
      product64Id
      productId
      qty
      variation64Id
      variationId
    }
    sflProducts {
      nodes {
        id
        databaseId
        # midCoins {
        #   id
        #   qty
        # }
        # shopCoins {
        #   id
        #   qty
        # }
        name
        slug
        ... on SimpleProduct {
          regularPrice
          salePrice
        }
        ... on VariableProduct {
          regularPrice
          salePrice
        }
        image {
          id
          sourceUrl(size: LARGE)
          altText
        }
        databaseId
        seller {
          id
          name
          gravatar
          rating
        }
        productCategories {
          nodes {
            id
            slug
          }
        }
      }
    }
  }
`;

export const StoreFragment = gql`
  fragment StoreFragment on Store {
    id
    name
    gravatar
    rating
    totalFollowers
    address {
      country
    }
  }
`;

export const MethodsFragment = gql`
  fragment SubcartsSubcartShippingMethodFragment on SubcartsSubcartShippingMethod {
    cost
    id
    label
    taxes
  }
`;

export const testSampleSubcartsFragment = gql`
  fragment testSampleSubcartsFragment on testSampleSubcarts {
    subcarts {
      items {
        currencySymbol
        includeTax
        productId
        qty
        subtotal
        variationId
      }
      shipping {
        chosenMethodId
        availableMethods {
          cost
          id
          label
          taxes
        }
      }
      store {
        id
        name
        gravatar
        rating
        totalFollowers
        address {
          country
        }
      }
      totals {
        itemsTotal
        shipping
        shopCoins
        total
      }
    }
    totals {
      itemsTotal
      save
      shipping
      total
      totalTaxes {
        amount
        id
        label
      }
    }
  }
`;

export const ProductToGlobalProductAttributeConnectionFragment = gql`
  fragment ProductToGlobalProductAttributeConnectionFragment on ProductToGlobalProductAttributeConnection {
    nodes {
      id
      name
      slug
      variation
      position
      scope
      attributeId
      visible
      terms {
        nodes {
          id
          name
          slug
          databaseId
        }
      }
    }
  }
`;

export const ShippingAddressFragment = gql`
  fragment ShippingAddressFragment on ShippingAddress {
    addDirections
    address1
    addressNickname
    apartment
    block
    city
    country
    district
    firstName
    floor
    id
    lastName
    map
    phone
    postCode
    road
    searchRoad
    type
    build
    selected
    primary
  }
`;

export const CommentDesktopFragment = gql`
  fragment CommentDesktopFragment on Comment {
    id
    databaseId
    content
    date
    totalLikes
    isLiked
    author {
      node {
        ... on User {
          id
          email
          avatar {
            url
          }
          username
          name
        }
      }
    }
    commentedOn {
      node {
        id
        databaseId
      }
    }
    parent {
      node {
        id
        databaseId
      }
    }
  }
`;

export const CommentFragment = gql`
  fragment CommentFragment on Comment {
    id
    databaseId
    content
    date
    totalLikes
    isLiked
    author {
      node {
        ... on User {
          id
          email
          avatar {
            url
          }
          username
          name
        }
      }
    }
    commentedOn {
      node {
        id
        databaseId
      }
    }
    parent {
      node {
        id
        databaseId
      }
    }
    replies {
      nodes {
        id
        databaseId
        content
        date
        totalLikes
        isLiked
        author {
          node {
            ... on User {
              id
              email
              avatar {
                url
              }
              username
              name
            }
          }
        }
        commentedOn {
          node {
            id
            databaseId
          }
        }
      }
    }
  }
`;

export const PostFragment = gql`
  fragment PostFragment on Post {
    id
    isFeedbackPost
    rating
    databaseId
    ratio
    link
    video
    slug
    isLiked
    totalLikes
    testSampleType
    galleryImages {
      nodes {
        id
        altText
        title
        description
        mediaDetails {
          height
          width
        }
        mediaItemUrl
        mimeType
        poster {
          id
          mediaItemUrl
          sourceUrl(size: THUMBNAIL)
          sourceUrlLarge: sourceUrl(size: LARGE)
          altText
        }
      }
    }
    geo {
      country_name
      city
    }
    title
    content
    date
    commentCount
    author {
      node {
        id
        avatar {
          url
        }
        roles {
          nodes {
            name
            id
          }
        }
        isFollowed
        totalFollowers
        name
        username
        databaseId
        slug
      }
    }
    tags {
      nodes {
        name
        id
        slug
      }
    }
    featuredImage {
      node {
        id
        sourceUrl(size: LARGE)
        altText
        mediaItemUrl
      }
    }
    relatedProducts {
      nodes {
        id
        name
        databaseId
        slug
        productBrands {
          nodes {
            name
            id
          }
        }
        image {
          id
          sourceUrl(size: LARGE)
          altText
        }
        galleryImages {
          nodes {
            id
            altText
            title
            description
            mediaItemUrl
            mimeType
          }
        }
        ... on SimpleProduct {
          id
          databaseId
          name
          price
          salePrice
          regularPrice
          shippingType
          slug
          shortDescription
          description
          isLiked
          totalLikes
          onSale
          product_video_url
          productCategories {
            nodes {
              slug
              id
              name
              parentDatabaseId
            }
          }
          seller {
            name
            id
            gravatar
            followed
          }
          image {
            id
            sourceUrl(size: LARGE)
            altText
          }
          galleryImages {
            nodes {
              id
              altText
              title
              description
              mediaItemUrl
              mimeType
            }
          }
        }
        ... on VariableProduct {
          id
          databaseId
          name
          price
          salePrice
          regularPrice
          shippingType
          slug
          isLiked
          totalLikes
          product_video_url
          variations {
            nodes {
              id
              regularPrice
              salePrice
              image {
                id
                sourceUrl(size: LARGE)
                altText
              }
            }
          }
          productCategories {
            nodes {
              id
              slug
              name
              parentDatabaseId
            }
          }
          seller {
            name
            id
            gravatar
            followed
          }
          image {
            id
            sourceUrl(size: LARGE)
            altText
          }
          galleryImages {
            nodes {
              id
              altText
              title
              description
              mediaItemUrl
              mimeType
            }
          }
        }
      }
    }
  }
`;

export const FlashPostFragment = gql`
  fragment FlashPostFragment on Post {
    id
    title
    slug
    databaseId
    testSampleType
    tags {
      edges {
        node {
          id
          name
        }
      }
    }
    galleryImages {
      nodes {
        id
        mediaType
        mediaItemUrl
        poster {
          id
          mediaItemUrl
          sourceUrl(size: THUMBNAIL)
          sourceUrlLarge: sourceUrl(size: LARGE)
          altText
        }
      }
    }
    author {
      node {
        id
        databaseId
        name
        isFollowed
        username
        avatar {
          url
        }
      }
    }
    relatedProducts {
      nodes {
        id
        slug
        image {
          id
          sourceUrl(size: THUMBNAIL)
          altText
        }
      }
    }
    commentCount
    isLiked
    totalLikes
  }
`;

export const ReviewsFragment = gql`
  fragment ReviewsFragment on Comment {
    id
    content
    rating
    date
    generalOpinion
    totalLikes
    galleryImages {
      nodes {
        id
        databaseId
        mediaItemUrl
        mimeType
        poster {
          mediaItemUrl
        }
      }
    }
    author {
      node {
        id
        databaseId
        name
        url
        ... on User {
          id
          email
          profile_picture
          mediaItems {
            nodes {
              id
              date
              mediaItemUrl
              sourceUrl
            }
          }
          username
          nickname
          name
          roles {
            nodes {
              name
            }
          }
          avatar {
            url
          }
        }
      }
    }
  }
`;
