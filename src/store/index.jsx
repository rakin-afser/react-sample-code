import React from 'react';
import useGlobalHook from 'use-global-hook';
import i18n from 'i18n';

import * as actions from '../actions';

const initialState = {
  isArabic: i18n.language === 'Arabic',
  actionsPopup: false,
  productFiltersPopup: false,
  headerBackButtonLink: '/',
  isModalOpened: false,
  actionsCartProduct: false,
  user: null,
  testUser: null, // need to delete
  influencer: false,
  quickView: null,
  leaveFeedback: {open: false, data: null},
  orderDetails: {
    open: false,
    data: null
  },
  actionAlert: {
    open: false,
    data: null
  },
  reportPopup: {
    active: false,
    subjectId: null,
    subjectSource: ''
  },
  authPopup: false,
  optionsPopup: {
    active: false,
    buttons: []
  },
  followersModal: false,
  isRequireAuth: false,
  showVariationsPopup: {active: false, productId: null},
  showSizeGuide: false,
  showProductAdded: false,
  isMuted: true,
  isAutoplay: true,
  returnRequest: false,
  sellerContact: false,
  messenger: {
    visible: false,
    data: null
  },
  signUpInfo: null,
  playInCardVideo: false
};

const useGlobal = useGlobalHook(React, initialState, actions);

export default useGlobal;
