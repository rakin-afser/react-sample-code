import {gql, useMutation} from '@apollo/client';
import {CommentFragment, CommentDesktopFragment} from 'fragments';
import moment from 'moment';
import {
  ADD_SUBSCRIBER,
  ADD_TO_CART,
  ADD_UPDATE_LIKED_COMMENT,
  ADD_UPDATE_LIKED_POST,
  ADD_UPDATE_LIKED_PRODUCT,
  APPLY_COUPON,
  APPLY_MIDCOINS_DISCOUNT,
  CREATE_COMMENT,
  CREATE_COMMENT_DESKTOP,
  CREATE_FLASH_POST,
  CREATE_POST,
  DELETE_POST,
  FOLLOW_UNFOLLOW_CATEGORY,
  FOLLOW_UNFOLLOW_INTEREST,
  FOLLOW_UNFOLLOW_LIST,
  FOLLOW_UNFOLLOW_STORE,
  FOLLOW_UNFOLLOW_USER,
  testSample_GET_SAVED_CARD_TOKEN,
  REGISTER_SOCIAL_CUSTOMER,
  REMOVE_COUPONS,
  REMOVE_MIDCOINS_DISCOUNT,
  VALIDATE_USERNAME
} from 'mutations';
import {v1} from 'uuid';
import {useUser} from './reactiveVars';
import parse from 'html-react-parser';
import {message} from 'antd';
import {FEED_POSTS, FEED_POSTS_MOBILE, POSTS, TRENDS} from 'queries';
import {useHistory} from 'react-router';
import {useWindowSize} from '@reach/window-size';

export const useAddToCartMutation = (options) => {
  return useMutation(ADD_TO_CART, {
    ...options,
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart(existing) {
            const {totalQuantity} = existing || {};
            return {...existing, totalQuantity: +totalQuantity + 1, ...data.addToCart.cart};
          }
        }
      });
    }
  });
};

export const useAddUpdateLikedProductMutation = (options) => {
  const {product, ...restOptions} = options || {};
  return useMutation(ADD_UPDATE_LIKED_PRODUCT, {
    ...restOptions,
    optimisticResponse: () => {
      const {id, __typename, isLiked, totalLikes} = product || {};
      return {
        addUpdateLikedProduct: {
          __typename: 'AddUpdateLikedProductPayload',
          product: product
            ? {
                __typename,
                id,
                isLiked: !isLiked,
                totalLikes: isLiked ? totalLikes - 1 : totalLikes + 1
              }
            : null
        }
      };
    },
    update(cache, {data}) {
      cache.writeQuery({
        id: product ? cache.identify(data?.addUpdateLikedProduct?.product) : null,
        data: data?.addUpdateLikedProduct?.product,
        query: gql`
          query Product {
            isLiked
            totalLikes
          }
        `
      });
    }
  });
};

export const useAddUpdateLikedCommentMutation = (options) => {
  const {comment, ...restOptions} = options || {};
  return useMutation(ADD_UPDATE_LIKED_COMMENT, {
    ...restOptions,
    optimisticResponse: () => {
      const {isLiked, totalLikes} = comment || {};
      return {
        addUpdateLikedComment: {
          comment: {
            ...comment,
            isLiked: !isLiked,
            totalLikes: isLiked ? totalLikes - 1 : totalLikes + 1
          },
          __typename: 'AddUpdateLikedCommentPayload'
        }
      };
    },
    update(cache, {data}) {
      cache.writeQuery({
        id: cache.identify(data?.addUpdateLikedComment?.comment),
        data: data?.addUpdateLikedComment?.product,
        query: gql`
          query Comment {
            isLiked
            totalLikes
          }
        `
      });
    }
  });
};

export const useAddUpdateLikedPostMutation = (options) => {
  const {post, ...restOptions} = options || {};
  return useMutation(ADD_UPDATE_LIKED_POST, {
    ...restOptions,
    optimisticResponse: () => {
      const {id, __typename, isLiked, totalLikes} = post || {};
      return {
        addUpdateLikedPost: {
          __typename: 'AddUpdateLikedPostPayload',
          post: post
            ? {
                __typename,
                id,
                isLiked: !isLiked,
                totalLikes: isLiked ? totalLikes - 1 : totalLikes + 1
              }
            : null
        }
      };
    },
    update(cache, {data}) {
      cache.writeQuery({
        id: post ? cache.identify(data?.addUpdateLikedPost?.post) : null,
        data: data?.addUpdateLikedPost?.post,
        query: gql`
          query Post {
            isLiked
            totalLikes
          }
        `
      });
    }
  });
};

export const useCreateCommentDesktopMutation = (options) => {
  const [user] = useUser();

  const {postId, commentId, ...restOptions} = options;

  return useMutation(CREATE_COMMENT_DESKTOP, {
    ...restOptions,
    onError(error) {
      if (error) {
        message.error(parse(error?.message || ''));
      }
    },
    optimisticResponse({input}) {
      const {commentOn, content, parent} = input || {};
      return {
        createComment: {
          success: true,
          comment: {
            id: v1(),
            databaseId: 123,
            content: `<p>${content}</p>\n`,
            date: moment(new Date()).format('DD MMM YYYY'),
            totalLikes: 0,
            isLiked: null,
            author: {
              node: {
                __typename: 'User',
                email: user?.email,
                id: user?.id,
                name: user?.name,
                avatar: {
                  __typename: 'Avatar',
                  url: user?.avatar?.url
                }
              },
              __typename: 'CommentToCommenterConnectionEdge'
            },
            commentedOn: {
              node: {
                id: v1(),
                databaseId: commentOn,
                __typename: 'Post'
              },
              __typename: 'CommentToContentNodeConnectionEdge'
            },
            parent: parent
              ? {
                  __typename: 'CommentToParentCommentConnectionEdge',
                  node: {
                    id: commentId,
                    databaseId: parent,
                    __typename: 'Comment'
                  }
                }
              : null,
            __typename: 'Comment'
          },
          __typename: 'CreateCommentPayload'
        }
      };
    },
    update(cache, {data: dataComment}) {
      if (dataComment?.createComment?.comment?.parent) {
        cache.modify({
          id: cache.identify(dataComment?.createComment?.comment?.parent?.node),
          fields: {
            replies(existing = {}) {
              const newComment = cache.writeFragment({
                id: cache.identify(dataComment?.createComment?.comment),
                data: dataComment?.createComment?.comment,
                fragment: CommentDesktopFragment
              });
              const {nodes} = existing;

              return {
                ...existing,
                nodes: [newComment, ...nodes]
              };
            }
          }
        });
        return;
      }

      cache.modify({
        id: cache.identify({__typename: 'Post', id: postId}),
        fields: {
          comments(existing = {}) {
            const newComment = cache.writeFragment({
              id: cache.identify(dataComment?.createComment?.comment),
              data: dataComment?.createComment?.comment,
              fragment: CommentFragment
            });
            const {nodes} = existing;

            return {
              ...existing,
              nodes: [newComment, ...nodes]
            };
          }
        }
      });
    }
  });
};

export const useCreateCommentMutation = (options) => {
  const [user] = useUser();

  const {postId, commentId, ...restOptions} = options;

  return useMutation(CREATE_COMMENT, {
    ...restOptions,
    onError(error) {
      if (error) {
        message.error(parse(error?.message || ''));
      }
    },
    optimisticResponse({input}) {
      const {commentOn, content, parent} = input || {};
      return {
        createComment: {
          success: true,
          comment: {
            id: v1(),
            databaseId: 123,
            content: `<p>${content}</p>\n`,
            date: moment(new Date()).format('DD MMM YYYY'),
            totalLikes: 0,
            isLiked: null,
            author: {
              node: {
                __typename: 'User',
                email: user?.email,
                id: user?.id,
                name: user?.name,
                avatar: {
                  __typename: 'Avatar',
                  url: user?.avatar?.url
                }
              },
              __typename: 'CommentToCommenterConnectionEdge'
            },
            commentedOn: {
              node: {
                id: v1(),
                databaseId: commentOn,
                __typename: 'Post'
              },
              __typename: 'CommentToContentNodeConnectionEdge'
            },
            parent: parent
              ? {
                  __typename: 'CommentToParentCommentConnectionEdge',
                  node: {
                    id: commentId,
                    databaseId: parent,
                    __typename: 'Comment'
                  }
                }
              : null,
            __typename: 'Comment'
          },
          __typename: 'CreateCommentPayload'
        }
      };
    },
    update(cache, {data: dataComment}) {
      if (dataComment?.createComment?.comment?.parent) {
        cache.modify({
          id: cache.identify(dataComment?.createComment?.comment?.parent?.node),
          fields: {
            replies(existing = {}) {
              const newComment = cache.writeFragment({
                id: cache.identify(dataComment?.createComment?.comment),
                data: dataComment?.createComment?.comment,
                fragment: CommentFragment
              });
              const {nodes} = existing;

              return {
                ...existing,
                nodes: [newComment, ...nodes]
              };
            }
          }
        });
        return;
      }

      cache.modify({
        id: cache.identify({__typename: 'Post', id: postId}),
        fields: {
          comments(existing = {}) {
            const newComment = cache.writeFragment({
              id: cache.identify(dataComment?.createComment?.comment),
              data: dataComment?.createComment?.comment,
              fragment: CommentFragment
            });
            const {nodes} = existing;

            return {
              ...existing,
              nodes: [newComment, ...nodes]
            };
          }
        }
      });
    }
  });
};

export function useCreatePostMutation(options) {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  const {push} = useHistory();
  return useMutation(CREATE_POST, {
    ...options,
    update(cache, {data: dataUpdate}) {
      const variables = {
        first: 7,
        where: {
          postGeo: 'LOCAL',
          orderby: [{field: 'DATE', order: 'DESC'}]
        }
      };
      const query = isMobile ? FEED_POSTS_MOBILE : FEED_POSTS;

      const {posts} = cache.readQuery({query, variables}) || {};
      const {nodes} = posts || {nodes: []};

      cache.writeQuery({
        query,
        variables,
        data: {
          posts: {
            ...posts,
            nodes: [dataUpdate?.createPost?.post, ...nodes]
          }
        }
      });
    },
    onCompleted(dataCreatePost) {
      const {databaseId} = dataCreatePost?.createPost?.post || {};
      if (databaseId) {
        push(`/post/${databaseId}`);
      }
    }
  });
}

export function useCreateFlashPostMutation(options) {
  return useMutation(CREATE_FLASH_POST, {
    ...options,
    update(cache, {data: dataUpdate}) {
      const variables = {first: 10, where: {testSampleType: 'flash'}};
      const query = TRENDS;

      const {posts} = cache.readQuery({query, variables}) || {};
      const {nodes} = posts || {nodes: []};

      cache.writeQuery({
        query,
        variables,
        data: {
          posts: {
            ...posts,
            nodes: [dataUpdate?.createPost?.post, ...nodes]
          }
        }
      });
    }
  });
}

export function useDeletePostMutation(options) {
  return useMutation(DELETE_POST, {
    ...options,
    update(cache, {data: dataDelete}) {
      cache.modify({
        fields: {
          posts(existing = {}, {readField}) {
            return {
              ...existing,
              nodes: existing?.nodes?.filter((postRef) => dataDelete.updatePost.post.id !== readField('id', postRef))
            };
          }
        }
      });
    }
  });
}

export function useValidateUsernameMutation(options) {
  return useMutation(VALIDATE_USERNAME, options);
}

export function useRegisterCustomerMutation(options) {
  return useMutation(REGISTER_SOCIAL_CUSTOMER, options);
}

export function useFollowUnFollowStoreMutation(options) {
  const {followed, ...rest} = options || {};
  return useMutation(FOLLOW_UNFOLLOW_STORE, {
    update(cache, {data}) {
      cache.modify({
        id: cache.identify(data.followUnfollowStore.store),
        fields: {
          followed() {
            return data.followUnfollowStore.store.followed;
          },
          totalFollowers(currTotalFollowers) {
            if (data?.followUnfollowStore?.store?.followed) {
              return currTotalFollowers + 1;
            }
            return currTotalFollowers - 1;
          }
        }
      });
    },
    optimisticResponse(args) {
      const {id} = args.input || {};
      return {
        followUnfollowStore: {
          store: {
            id,
            followed: !followed,
            __typename: 'Store'
          },
          __typename: 'FollowUnfollowStorePayload'
        }
      };
    },
    ...rest
  });
}

export function useFollowUnfollowInterestMutation(options) {
  const {id, isFollowed} = options || {};
  return useMutation(FOLLOW_UNFOLLOW_INTEREST, {
    ...options,
    update(cache, {data}) {
      cache.modify({
        id: cache.identify(data.followUnfollowInterest.interest),
        fields: {
          isFollowed() {
            return data.followUnfollowInterest.interest.isFollowed;
          }
        }
      });
    },
    optimisticResponse() {
      return {
        followUnfollowInterest: {
          interest: {
            id,
            isFollowed: !isFollowed,
            __typename: 'Interest'
          },
          __typename: 'FollowUnfollowInterestPayload'
        }
      };
    }
  });
}

export function useFollowUnfollowUserMutation(options) {
  const {id, isFollowed, ...rest} = options || {};
  return useMutation(FOLLOW_UNFOLLOW_USER, {
    ...rest,
    update(cache, {data}) {
      cache.modify({
        id: cache.identify(data.followUnfollowUser.user),
        fields: {
          isFollowed() {
            return data.followUnfollowUser.user.isFollowed;
          },
          totalFollowers(currTotalFollowers) {
            if (data?.followUnfollowUser?.user?.isFollowed) {
              return currTotalFollowers + 1;
            }
            return currTotalFollowers - 1;
          }
        }
      });
    },
    optimisticResponse() {
      return {
        followUnfollowUser: {
          user: {
            id,
            isFollowed: !isFollowed,
            __typename: 'User'
          },
          __typename: 'FollowUnfollowUserPayload'
        }
      };
    },
    onError(error) {
      message.error(error.message);
    }
  });
}

export function useFollowUnfollowCategoryMutation(options) {
  const {id, isFollowed, ...rest} = options || {};
  return useMutation(FOLLOW_UNFOLLOW_CATEGORY, {
    ...rest,
    update(cache, {data}) {
      cache.modify({
        id: cache.identify(data.followUnfollowCategory.category),
        fields: {
          isFollowed() {
            return data.followUnfollowCategory.category.isFollowed;
          }
        }
      });
    },
    optimisticResponse: {
      followUnfollowCategory: {
        category: {
          id,
          isFollowed: !isFollowed,
          __typename: 'ProductCategory'
        },
        __typename: 'FollowUnfollowCategoryPayload'
      }
    }
  });
}

export function useFollowUnfollowListMutation(options) {
  const {id, isFollowed, ...rest} = options || {};
  return useMutation(FOLLOW_UNFOLLOW_LIST, {
    update(cache, {data}) {
      cache.modify({
        id: cache.identify(data.followUnfollowList.list),
        fields: {
          isFollowed() {
            return data.followUnfollowList.list.isFollowed;
          },
          totalFollowers(currTotalFollowers) {
            if (data?.followUnfollowList?.list?.isFollowed) {
              return currTotalFollowers + 1;
            }
            return currTotalFollowers - 1;
          }
        }
      });
    },
    optimisticResponse: {
      followUnfollowList: {
        list: {
          id,
          isFollowed: !isFollowed,
          __typename: 'List'
        },
        __typename: 'FollowUnfollowListPayload'
      }
    },
    onError(error) {
      message.error(error.message);
    },
    ...rest
  });
}

export function useAddSubscriberMutation() {
  return useMutation(ADD_SUBSCRIBER);
}

export function useApplyMidcoinsDiscountMutation(options) {
  const [user] = useUser();
  const {qty, ...rest} = options || {};
  return useMutation(APPLY_MIDCOINS_DISCOUNT, {
    onError(error) {
      message.error(error?.message);
    },
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart(existing) {
            const {appliedMidCoins, total} = data?.applyMidcoinsDiscount?.cart || {};
            return {
              ...existing,
              total,
              appliedMidCoins
            };
          }
        }
      });
      cache.modify({
        id: cache.identify(user),
        fields: {
          availableMidcoins(existing) {
            let availableMidcoins = existing;
            if (existing >= parseFloat(qty || 0)) {
              availableMidcoins = (existing - qty)?.toFixed(2);
            }
            return availableMidcoins;
          }
        }
      });
    },
    ...rest
  });
}

export function useRemoveMidcoinsDiscountMutation(options) {
  const {qty, ...rest} = options || {};
  const [user] = useUser();
  return useMutation(REMOVE_MIDCOINS_DISCOUNT, {
    onError(error) {
      message.error(error?.message);
    },
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart(existing) {
            const {appliedMidCoins, total} = data?.removeMidcoinsDiscount?.cart || {};
            return {
              ...existing,
              total,
              appliedMidCoins
            };
          }
        }
      });
      cache.modify({
        id: cache.identify(user),
        fields: {
          availableMidcoins(existing) {
            let availableMidcoins = existing;
            if (existing && qty) {
              availableMidcoins = parseFloat(existing) + qty;
              availableMidcoins = availableMidcoins?.toFixed(2);
            }
            return availableMidcoins;
          }
        }
      });
    },
    ...rest
  });
}

export function useApplyCouponMutation(options) {
  return useMutation(APPLY_COUPON, {
    onError(error) {
      message.error(error?.message);
    },
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart(existing) {
            const {appliedCoupons, total} = data?.applyCoupon?.cart || {};
            return {
              ...existing,
              total,
              appliedCoupons
            };
          }
        }
      });
    },
    ...options
  });
}

export function useRemoveCouponsMutation(options) {
  return useMutation(REMOVE_COUPONS, {
    onError(error) {
      message.error(error?.message);
    },
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart(existing) {
            const {appliedCoupons, total} = data?.removeCoupons?.cart || {};
            return {
              ...existing,
              total,
              appliedCoupons
            };
          }
        }
      });
    },
    ...options
  });
}

export function usetestSampleGetSavedCardTokenMutation(options) {
  return useMutation(testSample_GET_SAVED_CARD_TOKEN, options);
}
