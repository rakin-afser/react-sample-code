import {useLazyQuery, useQuery} from '@apollo/client';
import {STORES_SEARCH} from 'containers/SearchResult/api/queries';
import {
  GET_LISTS,
  APPLIED_COUPONS,
  APPLIED_MIDCOINS,
  COMMENTS_OF_POST,
  COMMENTS_OF_POST_DESKTOP,
  FEED_POSTS,
  GET_COINS,
  GET_CURRENCIES,
  POSTS_SEARCH,
  PRODUCTS_SEARCH,
  RELATED_USERS,
  REPLY,
  USERS_SEARCH,
  PRODUCTS,
  testSample_CHECKOUTDOTCOM_PAYMENT,
  testSample_CHECKOUT,
  testSample_BALANCE,
  WALLET_BALANCE,
  AFFILIATE_LINK,
  POSTS_SEARCH_INPUT,
  SEARCH_INPUT,
  SEARCH_INPUT_PRODUCTS
} from 'queries';
import {useCallback, useEffect, useRef} from 'react';
import {useUser} from './reactiveVars';

export function useCommentsOfPostQuery(options) {
  const {contentRef, nextFirst, ...restOptions} = options;

  const endRef = useRef(null);
  const {data, loading, fetchMore} = useQuery(COMMENTS_OF_POST, {
    ...restOptions,
    notifyOnNetworkStatusChange: true
  });
  const {endCursor, hasNextPage} = data?.post?.comments?.pageInfo || {};

  const isEnd = useCallback(() => {
    if (!endRef) {
      return null;
    }
    const rect = endRef?.current?.getBoundingClientRect();
    return rect?.top - window.innerHeight < 0;
  }, [endRef]);

  useEffect(() => {
    const onScroll = () => {
      if (!hasNextPage || !isEnd() || loading) return;

      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const prevNodes = prev?.post?.comments?.nodes || [];
          const newNodes = fetchMoreResult?.post?.comments?.nodes || [];
          const nodes = [...prevNodes, ...newNodes];

          return {
            ...prev,
            post: {
              ...prev?.post,
              comments: {
                ...prev?.post?.comments,
                nodes,
                pageInfo: fetchMoreResult?.post?.comments?.pageInfo
              }
            }
          };
        },
        variables: {
          first: nextFirst || 5,
          after: endCursor
        }
      });
    };
    const ref = contentRef?.current;

    if (ref) {
      ref.addEventListener('scroll', onScroll);
      return () => ref.removeEventListener('scroll', onScroll);
    }
    return undefined;
  }, [hasNextPage, isEnd, endCursor, fetchMore, loading, contentRef, nextFirst]);

  return [endRef, {data, loading}];
}

export function useCommentsOfDesktopPostQuery(options) {
  const {contentRef, nextFirst, ...restOptions} = options;

  const {data, loading, fetchMore} = useQuery(COMMENTS_OF_POST_DESKTOP, {
    ...restOptions,
    notifyOnNetworkStatusChange: true
  });
  const {endCursor, hasNextPage} = data?.post?.comments?.pageInfo || {};

  const onUpdate = (values) => {
    const {scrollTop, scrollHeight, clientHeight} = values;
    const isEnd = (scrollTop + 100) / (scrollHeight - clientHeight) > 1;

    if (!hasNextPage || !isEnd || loading) return;

    fetchMore({
      updateQuery(prev, {fetchMoreResult}) {
        if (!fetchMoreResult) return prev;
        const prevNodes = prev?.post?.comments?.nodes || [];
        const newNodes = fetchMoreResult?.post?.comments?.nodes || [];
        const nodes = [...prevNodes, ...newNodes];

        return {
          ...prev,
          post: {
            ...prev?.post,
            comments: {
              ...prev?.post?.comments,
              nodes,
              pageInfo: fetchMoreResult?.post?.comments?.pageInfo
            }
          }
        };
      },
      variables: {
        first: nextFirst || 5,
        after: endCursor
      }
    });
  };

  return [onUpdate, {data, loading}];
}

export function useReplyQuery(options) {
  const {contentRef, nextFirst, ...restOptions} = options;

  const endRef = useRef(null);
  const {data, loading, fetchMore} = useQuery(REPLY, {
    ...restOptions,
    notifyOnNetworkStatusChange: true
  });
  const {endCursor, hasNextPage} = data?.comment?.replies?.pageInfo || {};

  const isEnd = useCallback(() => {
    if (!endRef) {
      return null;
    }

    const rect = endRef?.current?.getBoundingClientRect();
    return rect?.top - window.innerHeight === 0;
  }, [endRef]);

  useEffect(() => {
    const onScroll = () => {
      if (!hasNextPage || !isEnd() || loading) return;

      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const prevNodes = prev?.comment?.replies?.nodes || [];
          const newNodes = fetchMoreResult?.comment?.replies?.nodes || [];
          const nodes = [...prevNodes, ...newNodes];

          return {
            ...prev,
            comment: {
              ...prev?.comment,
              replies: {
                ...prev?.comment?.replies,
                nodes,
                pageInfo: fetchMoreResult?.comment?.replies?.pageInfo
              }
            }
          };
        },
        variables: {
          first: nextFirst || 5,
          after: endCursor
        }
      });
    };

    const ref = contentRef?.current;

    if (ref) {
      ref.addEventListener('scroll', onScroll);
      return () => ref.removeEventListener('scroll', onScroll);
    }
    return undefined;
  }, [hasNextPage, isEnd, endCursor, fetchMore, loading, contentRef, nextFirst]);

  return [endRef, {data, loading}];
}

export function useRelatedUsersLazyQuery(options) {
  const [user] = useUser();
  return useLazyQuery(RELATED_USERS, {
    ...options,
    variables: {
      id: user?.databaseId
    }
  });
}

export function useGetCoinsLazyQuery(options) {
  return useLazyQuery(GET_COINS, {...options});
}

export function useAppliedMidcoinsQuery(options) {
  return useQuery(APPLIED_MIDCOINS, options);
}

export function useAppliedCouponsQuery(options) {
  return useQuery(APPLIED_COUPONS, options);
}

export function useProductsSearchQuery(options) {
  return useQuery(PRODUCTS_SEARCH, options);
}

export function useProductsSearchLazyQuery(options) {
  return useLazyQuery(PRODUCTS_SEARCH, options);
}

export function useUsersSearchQuery(options) {
  return useQuery(USERS_SEARCH, options);
}

export function useUsersSearchLazyQuery(options) {
  return useLazyQuery(USERS_SEARCH, options);
}

export function useFeedPostsQuery(options) {
  return useQuery(FEED_POSTS, options);
}

export function usePostsSearchQuery(options) {
  return useQuery(POSTS_SEARCH, options);
}

export function usePostsSearchLazyQuery(options) {
  return useLazyQuery(POSTS_SEARCH, options);
}

export function useStoresSearchQuery(options) {
  return useQuery(STORES_SEARCH, options);
}

export function useStoresSearchLazyQuery(options) {
  return useLazyQuery(STORES_SEARCH, options);
}

export function useProductsQuery(options) {
  return useQuery(PRODUCTS, options);
}

export function useListsQuery(options) {
  return useQuery(GET_LISTS, options);
}

export function useListsLazyQuery(options) {
  return useLazyQuery(GET_LISTS, options);
}

export function usePostsSearchInputQuery(options) {
  return useQuery(POSTS_SEARCH_INPUT, options);
}

export function usetestSampleCheckoutdotcomPaymentLazyQuery(options) {
  return useLazyQuery(testSample_CHECKOUTDOTCOM_PAYMENT, options);
}

export function usetestSampleCheckoutQuery(options) {
  return useQuery(testSample_CHECKOUT, options);
}

export function usetestSampleBalanceQuery(options) {
  return useQuery(testSample_BALANCE, options);
}

export function useWalletBalanceQuery(options) {
  return useQuery(WALLET_BALANCE, options);
}

export function useAffiliateLinkQuery(options) {
  return useQuery(AFFILIATE_LINK, options);
}

export function useSearchInputQuery(options) {
  return useQuery(SEARCH_INPUT, options);
}

export function useSearchInputProductsQuery(options) {
  return useQuery(SEARCH_INPUT_PRODUCTS, options);
}
