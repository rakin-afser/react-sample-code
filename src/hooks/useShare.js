import qs from 'qs';

const useShare = () => {
  const getQueryString = (type, data) => qs.stringify({modal: {type, data}}, {addQueryPrefix: true});

  const share = async ({title = 'testSample', text = 'Share', type, modalType, customURL, data}) => {
    let shareURL;

    switch (type) {
      case 'product':
        shareURL = window.location.origin + getQueryString(modalType || 'product', data);
        break;
      case 'post': {
        shareURL = window.location.origin + getQueryString(modalType || 'postView', data);
        break;
      }
      default:
        shareURL = window.location.href;
        break;
    }

    const shareData = {
      title,
      text,
      url: customURL || shareURL
    };

    // console.log(shareData);

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return {share};
};

export default useShare;
