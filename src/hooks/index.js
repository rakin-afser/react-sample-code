import {useEffect, useMemo, useRef, useState} from 'react';
import {filterVariations, formatVariations} from 'util/heplers';

export function useVariations(product) {
  const initialState = useMemo(
    () =>
      product?.variations?.nodes?.[0]?.attributes?.nodes?.reduce(
        (accAttrs, currAttr) => ({...accAttrs, [currAttr?.name]: currAttr?.id}),
        {}
      ),

    [product]
  );

  // useEffect(() => {
  //   if (Object.keys(initialState || {})?.length > 0) {
  //     setState(initialState);
  //   }
  // }, [initialState]);

  const [state, setState] = useState(initialState || {});
  const filteredVars = filterVariations(product?.variations?.nodes, state);
  const formatedVariations = formatVariations(filteredVars, product?.variations?.nodes);
  return [formatedVariations, state, setState];
}

export function useOnScreen(options) {
  const {called, doAction, ...rest} = options || {};

  const ref = useRef();

  useEffect(() => {
    const defaultOptions = {
      root: null,
      rootMargin: '0px',
      threshold: 0.5
    };
    const observer = new IntersectionObserver(
      (entries) => {
        const {isIntersecting} = entries?.[0] || {};
        if (isIntersecting && !called && doAction) {
          doAction();
        }
      },
      {...defaultOptions, ...rest}
    );
    const {current} = ref;
    if (current) observer.observe(current);

    return () => {
      if (current) observer.unobserve(current);
    };
  }, [called, doAction, rest]);

  return ref;
}

export function useOnScreenPost(options) {
  const {onVisible, onHidden, ...rest} = options || {};

  const ref = useRef();

  useEffect(() => {
    const defaultOptions = {
      root: null,
      rootMargin: '0px',
      threshold: [0.16667, 0.33333]
    };
    const observer = new IntersectionObserver(
      (entries) => {
        const {isIntersecting} = entries?.[0] || {};
        if (isIntersecting && onVisible) {
          onVisible();
        }
        if (!isIntersecting && onHidden) {
          onHidden();
        }
      },
      {...defaultOptions, ...rest}
    );
    const {current} = ref;
    if (current) observer.observe(current);

    return () => {
      if (current) observer.unobserve(current);
    };
  }, [onVisible, onHidden, rest]);

  return ref;
}

function useDebounce(value, delay) {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(
    () => {
      // Update debounced value after delay
      const handler = setTimeout(() => {
        setDebouncedValue(value);
      }, delay);
      // Cancel the timeout if value changes (also on delay change or unmount)
      // This is how we prevent debounced value from updating if value is changed ...
      // .. within the delay period. Timeout gets cleared and restarted.
      return () => {
        clearTimeout(handler);
      };
    },
    [value, delay] // Only re-call effect if value or delay changes
  );
  return debouncedValue;
}

export function useStateDebounce(value, delay = 500) {
  const [state, setState] = useState(value);
  const stateDebounced = useDebounce(state, delay);
  return [state, stateDebounced, setState];
}
