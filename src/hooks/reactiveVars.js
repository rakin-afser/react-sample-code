import {makeVar, useReactiveVar} from '@apollo/client';

export const cartItemsVar = makeVar([]);
export function useCartItems() {
  const cartItems = useReactiveVar(cartItemsVar);
  return [cartItems, cartItemsVar];
}

export const currencyVar = makeVar('BD');
export function useCurrency() {
  const currency = useReactiveVar(currencyVar);
  return [currency, currencyVar];
}

export const userVar = makeVar(null);
export function useUser() {
  const user = useReactiveVar(userVar);
  const isSeller = user?.capabilities?.includes('seller');
  return [user ? {...user, isSeller} : user, userVar];
}
