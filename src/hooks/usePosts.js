import React, {useState, useEffect} from 'react';
import _ from 'lodash';
import {useLazyQuery} from '@apollo/client';

import {POSTS} from 'queries';
import usePagination from 'util/usePagination';

const usePosts = ({triggerClearSavedData, shouldUpdateSavedData = true, first = 12, variables = {}, fetchPolicy}) => {
  const [savedData, setSavedData] = useState([]);
  const [totalResults, setTotalResults] = useState(0);
  const {currentPage, currentCursor, goNextPage, goPrevPage, resetPagination} = usePagination();
  const [getPosts, {data, loading, error}] = useLazyQuery(POSTS, {
    variables: {first, after: currentCursor, ...variables},
    fetchPolicy
  });

  const pageInfo = data?.posts?.pageInfo;

  useEffect(() => {
    getPosts();
  }, [currentPage]);

  useEffect(() => {
    if (currentPage !== 1) {
      setSavedData([]);
      resetPagination();
      getPosts();
    }
  }, [triggerClearSavedData]);

  useEffect(() => {
    if (currentPage === 1 && pageInfo?.total && !loading) {
      // save total results (because of incorrect BE)
      setTotalResults(pageInfo?.total);
    }
    if (!_.isEmpty(data?.posts?.nodes) && !loading && shouldUpdateSavedData) {
      if (currentPage === 1) {
        setSavedData([...data?.posts?.nodes]);
      } else {
        setSavedData([...savedData, ...data?.posts?.nodes]);
      }
    }
  }, [data, loading]);

  return {
    currentData: data?.posts?.nodes || [],
    allData: savedData || [], // for infinite scroll
    loading,
    error,
    postsPagination: {
      totalResults,
      currentPage,
      totalPages: Math.ceil(totalResults / first),
      prevPage: () => {
        if (!loading) {
          goPrevPage(pageInfo);
        }
      },
      nextPage: () => {
        if (!loading) {
          goNextPage(pageInfo);
        }
      },
      resetPagination
    }
  };
};

export default usePosts;
