import React from 'react';
import _ from 'lodash';
import {useQuery} from '@apollo/client';

import {GET_REWARDS} from 'containers/MyProfile/api/queries';
import usePaginationv2 from 'util/usePaginationv2';
import {useUser} from 'hooks/reactiveVars';

const useRewards = (props = {}) => {
  const {limit = 10, variables} = props;
  const [user] = useUser();
  const {currentPage, goPrevPage, goNextPage, resetPagination} = usePaginationv2(limit);
  const {data, loading, error} = useQuery(GET_REWARDS, {
    variables: {
      userId: user?.databaseId,
      limit,
      offset: currentPage - 1,
      ...variables
    }
  });
  const totalResults = data?.testSampleRewardData?.total;

  return {
    currentData: data?.testSampleRewardData?.nodes,
    loading,
    error,
    rewardsPagination: {
      totalResults,
      currentPage,
      totalPages: Math.ceil(totalResults / limit),
      prevPage: () => {
        if (!loading) {
          goPrevPage();
        }
      },
      nextPage: () => {
        if (!loading) {
          goNextPage(totalResults);
        }
      },
      resetPagination
    }
  };
};

export default useRewards;
