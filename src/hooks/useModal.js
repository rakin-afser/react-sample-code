import qs from 'qs';
import {useLocation, useHistory} from 'react-router-dom';

const useModal = () => {
  const {search, pathname} = useLocation();
  const history = useHistory();

  const getModalData = () => {
    const {modal} = qs.parse(search, {ignoreQueryPrefix: true}) || {};
    return modal;
  };

  const openModal = (type, data) => {
    const searchObj = qs.parse(search, {ignoreQueryPrefix: true}) || {};
    const queryString = qs.stringify({...searchObj, modal: {type, data}}, {addQueryPrefix: true});
    history.push(pathname + queryString);
  };

  const closeModal = () => {
    if (!getModalData()) return;
    const searchObj = qs.parse(search, {ignoreQueryPrefix: true}) || {};
    const queryString = qs.stringify({...searchObj, modal: undefined}, {addQueryPrefix: true});
    setTimeout(() => {
      history.push(pathname + queryString);
    }, 500);
  };

  return {openModal, closeModal};
};

export default useModal;
