import styled from 'styled-components';
import {mainFont, secondaryFont} from 'constants/fonts';

export const AboutContainer = styled.div`
  padding-top: 42px;
  width: 100%;
`;

export const AboutTitle = styled.h3`
  font-family: ${mainFont};
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin-bottom: 33px;
  margin-top: 0;
`;

export const UserBar = styled.div`
  display: flex;
  padding: 25px 0 23px;
  border-top: 1px solid #e4e4e4;
  border-bottom: 1px solid #e4e4e4;
`;

export const UserImg = styled.img`
  border-radius: 50%;
  width: 44px;
  height: 44px;
`;

export const UserInfo = styled.div`
  width: 100%;
  max-width: 735px;
`;

export const Name = styled.h5`
  font-weight: 700;
  font-size: 16px;
  margin-bottom: 7px;
`;

export const Location = styled.span`
  display: inline-flex;
  font-family: ${secondaryFont};
  font-size: 12px;

  svg {
    margin-right: 7px;
  }
`;
export const Rating = styled.span`
  display: flex;
  align-items: center;
  font-family: ${secondaryFont};
  font-size: 12px;
`;

export const RatingScore = styled.span`
  color: #7a7a7a;
  font-weight: 700;
`;

export const Stars = styled.div`
  display: inline-flex;
  margin-right: 8px;
  margin-left: 9px;
`;

export const FollowBlock = styled.div`
  display: flex;
`;

export const FollowItem = styled.div`
  display: flex;
  font-family: ${secondaryFont};
  align-items: center;

  & + & {
    margin-left: 26px;
  }
`;

export const FollowQuantity = styled.span`
  font-size: 18px;
  font-weight: 600;
  color: #000;
`;

export const FollowText = styled.span`
  margin-left: 4px;
`;

export const FollowBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  max-width: 99px;
  width: 100%;
  height: 28px;
  background: transparent;
  border: 1px solid #ed484f;
  border-radius: 24px;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #ed484f;
  cursor: pointer;

  svg {
    margin-right: 7px;
  }
`;

export const Text = styled.div`
  div {
    text-align: left;
    padding: 0 0;
    max-width: 600px;
    line-height: 2;

    button {
      font-size: 12px;
    }
  }
`;

export const Tag = styled.div`
  & + & {
    margin-left: 6px;
  }
`;

export const Tags = styled.div`
  display: flex;
`;

export const Socials = styled.div`
  display: inline-flex;
  margin-left: 26px;
`;

export const SocialLink = styled.a`
  display: inline-flex;
  color: #bec0c3;
  transition: color 0.3s ease-in;

  &:hover {
    color: #ed494f;
  }

  & + & {
    margin-left: 7px;
  }
`;

export const AvatarPic = styled.img`
  width: 44px;
  height: 44px;
  border-radius: 50%;
  object-fit: cover;
  background: #f8f8f8;
  border: 1px solid #ddd;
  margin-right: 16px;
`;
