import React from 'react';
import {
  AboutContainer,
  AboutTitle,
  Name,
  UserInfo,
  UserBar,
  Rating,
  Location,
  FollowBtn,
  Stars,
  RatingScore,
  SocialLink,
  Socials,
  Text,
  AvatarPic
} from './styled';
import Plus from 'assets/Plus';
import Marker from 'assets/Marker';
import Star from 'assets/Star';
import {ReactComponent as Facebook} from './img/facebook.svg';
import {ReactComponent as Instagram} from './img/instagram.svg';
import {ReactComponent as Twitter} from './img/twitter.svg';
import {ReactComponent as Linked} from './img/linked.svg';
import ExpandedText from 'components/ExpandedText';
import userPlaceholder from 'images/placeholders/user.jpg';

const About = ({data}) => {
  const {
    vendorBiography,
    id,
    name,
    banner,
    followed,
    gravatar,
    rating,
    totalFollowers,
    storePpp,
    storeTnc,
    enableTnc,
    address,
    social
  } = data?.store || {};

  const socialLinks = [
    {link: social?.fb, icon: Facebook},
    {link: social?.instagram, icon: Instagram},
    {link: social?.twitter, icon: Twitter},
    {link: social?.linkedin, icon: Linked}
  ];

  const tags = [{value: '#Cream'}, {value: '#Canvas'}, {value: '#Big'}, {value: '#tags'}];
  return (
    <AboutContainer>
      <AboutTitle>About Seller</AboutTitle>
      <UserBar>
        <UserInfo>
          <div style={{display: 'flex', alignItems: 'center', marginBottom: '15px'}}>
            {/* <ImageBlock> */}
            <AvatarPic src={gravatar || userPlaceholder} />
            {/* </ImageBlock> */}
            <div style={{display: 'inline-block'}}>
              <Name>{name}</Name>
              <Rating>
                <RatingScore>4,9</RatingScore>
                <Stars>
                  {[1, 2, 3, 4, 5].map((_, i) => (
                    <Star key={i} width={12} height={12} fill="#FFC131" />
                  ))}
                </Stars>
                (602)
              </Rating>
            </div>
          </div>

          <Location>
            <Marker />
            {address?.city}, {address?.countryName}
          </Location>
          <Socials>
            {socialLinks.map((social) => (
              <SocialLink key={social.link} href={social.link}>
                <social.icon />
              </SocialLink>
            ))}
          </Socials>
        </UserInfo>
        <FollowBtn>
          <Plus />
          Follow
        </FollowBtn>
      </UserBar>
      <Text>
        <ExpandedText text={vendorBiography} divider={151} />
      </Text>
    </AboutContainer>
  );
};

export default About;
