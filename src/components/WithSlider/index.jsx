import React, {useEffect, useRef} from 'react';
import Slider from 'react-slick';
import {any, bool, number, string, oneOfType} from 'prop-types';
import {ContentWrapper} from 'globalStyles';
import arrow from '../../images/arrow.png';
import {ButtonText, CircleWrapper, Header, SeeMoreButton, SliderWrapper, Thumb, Title} from './styled';
import Arrow from '../../assets/Arrow';
import {useHistory} from 'react-router';

const WithSlider = React.forwardRef(
  (
    {
      withHeader = true,
      children,
      title,
      titleStyles,
      withSeeMore,
      marginTop,
      padding,
      to,
      hideViewAll,
      loading,
      seeMoreNumber,
      type = '',
      ...rest
    },
    ref
  ) => {
    const settings = {
      ...rest
    };
    const {push} = useHistory();

    let firstClientX;
    let clientX;

    const preventTouch = (e) => {
      const minValue = 5; // threshold

      clientX = e.touches[0].clientX - firstClientX;

      // Vertical scrolling does not work when you start swiping horizontally.
      if (Math.abs(clientX) > minValue) {
        e.preventDefault();
        e.returnValue = false;

        return false;
      }
    };

    const touchStart = (e) => {
      firstClientX = e.touches[0].clientX;
    };

    const containerRef = useRef();

    useEffect(() => {
      if (containerRef.current) {
        containerRef.current.addEventListener('touchstart', touchStart);
        containerRef.current.addEventListener('touchmove', preventTouch, {
          passive: false
        });
      }

      return () => {
        if (containerRef.current) {
          containerRef.current.removeEventListener('touchstart', touchStart);
          containerRef.current.removeEventListener('touchmove', preventTouch, {
            passive: false
          });
        }
      };
    });

    return (
      <ContentWrapper marginTop={marginTop}>
        {withHeader && (
          <Header>
            <Title titleStyles={{...titleStyles}}>{title}</Title>
            {hideViewAll ? null : (
              <Thumb to={to || '/'}>
                View all {<b>{seeMoreNumber || ''}</b>}{' '}
                <img style={{transform: 'rotateY(180deg)'}} width={8} height={12} src={arrow} alt="arrow" />
              </Thumb>
            )}
          </Header>
        )}
        <SliderWrapper className={type} padding={padding} ref={containerRef}>
          {/* eslint-disable-next-line react/jsx-props-no-spreading */}
          <Slider ref={ref} {...settings}>
            {children}
            {!loading && withSeeMore && (
              <SeeMoreButton onClick={() => push(to || '/')}>
                <CircleWrapper>
                  <Arrow />
                </CircleWrapper>
                <ButtonText>See More</ButtonText>
              </SeeMoreButton>
            )}
          </Slider>
        </SliderWrapper>
      </ContentWrapper>
    );
  }
);

WithSlider.propTypes = {
  title: string,
  children: any,
  marginTop: number,
  withSeeMore: bool,
  padding: oneOfType([string, number]),
  withHeader: bool,
  seeMoreNumber: number
};

WithSlider.defaultProps = {
  marginTop: 50,
  withSeeMore: false
  // seeMoreNumber: 30
};

export default WithSlider;
