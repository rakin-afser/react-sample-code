import styled from 'styled-components/macro';
import {FlexContainer} from 'globalStyles';
import {primaryColor, mainBlackColor, grayBackroundColor} from 'constants/colors';
import media from 'constants/media';
import {Link} from 'react-router-dom';

export const Header = styled(FlexContainer)`
  border-bottom: 2px ${primaryColor} solid;
  padding-bottom: 13px;

  @media (max-width: ${media.mobileMax}) {
    border-bottom: 0;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    padding: 0 16px 21px;
  }
`;

export const Title = styled.div`
  font-size: 24px;
  letter-spacing: 0.5px;
  color: ${mainBlackColor};
  font-weight: bold;

  @media (max-width: ${media.mobileMax}) {
    font-style: normal;
    font-weight: 700;
    font-size: 18px;
    line-height: 21px;
    color: #343434;
  }

  ${({titleStyles}) => titleStyles && {...titleStyles}}
`;

export const Thumb = styled(Link)`
  font-size: 14px;
  cursor: pointer;
  position: relative;
  top: 5px;
  color: inherit;

  img {
    margin-left: 10px;
    position: relative;
    top: -2px;
  }
  @media (max-width: ${media.mobileMax}) {
    top: 0;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    text-align: right;
    color: #000000;

    img {
      margin-left: 20px;
      max-width: 7px;
      max-height: 11px;
      top: 0;
    }
  }
`;

export const SliderWrapper = styled.div`
  padding: ${({padding}) => (padding ? padding : '32px 0')};
  width: 100%;
  margin-bottom: 0;

  &.shops {
    padding-left: 0;
    padding-right: 0;

    .slick {
      &-slide {
        margin-left: 12px;
        margin-right: 12px;
      }

      &-list {
        max-width: 1080px;
        padding: 0;
      }
    }
  }

  .slick {
    &-slider {
      margin-bottom: -25px;
    }

    &-list {
      padding: 0 0 25px;
      max-width: 1056px;
      margin: 0 auto;
    }

    &-slide {
      width: auto !important;
      margin-left: 18px;
      margin-right: 18px;
    }

    &-prev {
      left: 10px;
    }

    &-next {
      right: 20px;
    }

    &-dots {
      bottom: -35px !important;

      li {
        &.slick-active button {
          &:after {
            background: #656565;
            width: 8px;
            height: 8px;
          }
        }
        button {
          position: relative;
          padding: 0;

          &:before {
            display: none;
          }

          &:after {
            content: '';
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 6px;
            height: 6px;
            background: ${grayBackroundColor};
            border-radius: 50%;
            transition: all 0.3s ease;
          }
        }
      }
    }
  }

  @media (max-width: ${media.mobileMax}) {
    padding: 0;
  }
`;

export const SeeMoreButton = styled(FlexContainer)`
  cursor: pointer;
  width: 100%;
  flex-direction: column;
  display: flex !important;
  justify-content: center;
`;

export const CircleWrapper = styled(FlexContainer)`
  justify-content: center;
  width: 45px;
  height: 45px;
  border-radius: 50%;
  border: 2px ${primaryColor} solid;
  margin-bottom: 10px;
`;

export const ButtonText = styled.div`
  font-size: 16px;
  font-weight: bold;
  color: ${mainBlackColor};
`;
