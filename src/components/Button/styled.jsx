import styled from 'styled-components';

export const Wrapper = styled.button`
  outline: none;
  cursor: pointer;
  padding: 0 15px;
  height: 40px;
  background: #ED484F;
  border-radius: 24px;
  border: 1px solid #ED484F;
  transition: all .3s ease;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  color: #FFFFFF; 
  min-width: 120px;
  
  ${({styleType}) => styleType === 'blackBorder' && `
    background: transparent;
    border-color: #000;
    color: #000;
  `}
`;
