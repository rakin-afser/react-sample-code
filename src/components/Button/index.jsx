import React from 'react';
import { objectOf, oneOfType, object, string, number, node } from 'prop-types';

import { Wrapper } from './styled';

const Button = ({ children, ...props }) => {
  return <Wrapper {...props}>
    {children}
  </Wrapper>
};

Button.defaultProps = {
  children: null,
  props: null
};

Button.propTypes = {
  children: oneOfType([object, string, number, node]),
  props: objectOf(object)
};

export default Button;