import React, {useState} from 'react';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import {Wrapper, Title} from './styled';

const Tooltip = ({title, position, children}) => {
  const [visible, setVisible] = useState(false);

  return (
    <Wrapper
      onMouseEnter={() => setVisible(true)}
      onMouseLeave={() => {
        setVisible(false);
      }}
    >
      <Title position={position}>{title}</Title>
      {children}
    </Wrapper>
  );
};

Tooltip.propTypes = {
  title: PropTypes.string.isRequired,
  position: PropTypes.oneOf(['top', 'right', 'bottom', 'left'])
};

export default Tooltip;
