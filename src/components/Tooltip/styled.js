import styled from 'styled-components/macro';
import {mainFont} from 'constants/fonts';

export const Wrapper = styled.span``;

export const Title = styled.span`
  padding: 10px 31px 7px 16px;
  position: absolute;
  font-family: ${mainFont};
  font-size: 12px;
  line-height: 132%;
  color: #000000;
  background-color: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 3px;
  z-index: 10;
  transition: ease .4s;
  opacity: 1;

  &:hover {
    opacity: 1;
  }

  ${({position}) =>
    position &&
    `
    ${position}: 34px;
    
    left: -100%;
  `}
}

;
`;
