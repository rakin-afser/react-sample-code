import {Button} from 'antd';
import {secondaryFont} from 'constants/fonts';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

import {primaryColor} from 'constants/colors';

export const Avatar = styled.img`
  max-width: 100%;
  border-radius: 50%;
  margin-bottom: 5px;
  width: 25px;
  height: 25px;
  object-fit: cover;
`;

export const Container = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 100;
  padding: 8px 9px calc(4px + env(safe-area-inset-bottom));
  background-color: #fff;
  box-shadow: 0px -0.5px 0px rgba(0, 0, 0, 0.1), 0px -2px 12px rgba(0, 0, 0, 0.07);
  display: flex;
  justify-content: space-between;
  border-radius: 15px 15px 0px 0px;

  > * {
    flex-grow: 1;
    flex-basis: 0%;
  }
`;

export const NavLink = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  color: #666;
  font-size: 10px;
  font-family: ${secondaryFont};
  transition: color 0.3s ease-in;

  svg {
    color: #999;
    margin-bottom: 5px;
  }

  .title {
    margin-top: auto;
  }

  &:hover {
    color: #666;
  }

  ${(p) =>
    p.current &&
    `
    color: ${primaryColor};

    svg {
      color: ${primaryColor};

      path {
        ${p.notStroke ? `` : `stroke: ${primaryColor};`}
      }

      ellipse{
        fill: ${primaryColor};
      }
    }

    &:hover {
      color: ${primaryColor};
    }
  `}
`;

export const AddPost = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  font-family: ${secondaryFont};
  font-size: 10px;
  text-align: center;
  color: #ed484f;
`;

export const AddButton = styled(Button)`
  &&& {
    background-color: #ed484f;
    height: 50px;
    width: 50px;
    border: 5px solid #fff;
    display: flex;
    justify-content: center;
    align-items: center;
    padding: 0;
    margin-top: -29px;
    margin-bottom: 7px;
  }
`;

AddButton.defaultProps = {
  shape: 'round'
};

export const MessagesIconWrapper = styled.div`
  position: relative;
`;

export const MessagesIconBadge = styled.div`
  position: absolute;
  right: -5px;
  top: -3px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  color: #fff;
  font-size: 10px;
  width: 12px;
  height: 12px;
  background-color: ${primaryColor};
`;
