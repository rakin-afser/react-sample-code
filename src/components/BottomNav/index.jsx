import React, {useRef, useState} from 'react';
import {useHistory, useLocation} from 'react-router';
import {v1} from 'uuid';

import {AddButton, AddPost, Container, NavLink, Avatar, MessagesIconWrapper, MessagesIconBadge} from './styled';
import {ReactComponent as Home} from 'images/svg/home.svg';
import {ReactComponent as Category} from 'images/svg/category.svg';
import {ReactComponent as FeedNew} from 'images/svg/feed-new.svg';
import {ReactComponent as Cart} from 'images/svg/cart.svg';
import {ReactComponent as Union} from 'images/svg/union.svg';
import {ReactComponent as Profile} from 'images/svg/profile.svg';
import {ReactComponent as Messages} from 'images/svg/messages.svg';
import {ReactComponent as ShopNew} from 'images/svg/shop-new.svg';
import useGlobal from 'store';
import AuthRequest from 'components/AuthRequest';
import {useUser} from 'hooks/reactiveVars';
import audio from 'audio/click.wav';

const getCurrentPage = (path) => {
  switch (path) {
    case '/':
      return 'home';
    case '/categories':
      return 'categories';
    case '/feed':
      return 'feed';
    case '/profile/messages':
    case '/profile/notifications':
      return 'messages';
    case '/profile/lists':
    case '/profile/posts':
    case '/profile':
      return 'profile';
    default:
      return '';
  }
};

// disable some pages for mobile (for now)
const hiddenPages = ['profile', 'messages'];

const BottomNavigation = () => {
  const [, {setIsRequireAuth}] = useGlobal();
  const location = useLocation();
  const currentPage = getCurrentPage(location.pathname);
  const [user] = useUser();
  const [unreadMessages, setUnreadMessages] = useState(null);
  const audioRef = useRef();

  if (window.talkJsSession) {
    // console.log(window.talkJsSession.unreads.K.length);

    window.talkJsSession.unreads.on('change', (unreadConversations) => {
      const unreadCount = unreadConversations.length;
      setUnreadMessages(unreadCount);
    });

    window.talkJsSession.on('message', (unreadConversations) => {
      if (!unreadConversations.isByMe) {
        audioRef.current.play();
      }
    });
  }

  const renderMessagesIcon = () => {
    return (
      <MessagesIconWrapper>
        <Messages />
        {unreadMessages ? <MessagesIconBadge>{unreadMessages}</MessagesIconBadge> : null}
      </MessagesIconWrapper>
    );
  };

  const {push} = useHistory();
  const pages = [
    {
      title: 'Feed',
      icon: <FeedNew />,
      to: '/feed',
      id: 'feed'
    },
    {
      title: 'Marketplace',
      icon: <ShopNew width={22} height={22} />,
      to: '/',
      id: 'home'
    },
    {
      title: 'Cart',
      icon: <Cart width={30} height={28} />,
      to: '/cart',
      id: 'cart'
    },
    {
      title: 'Messages',
      icon: renderMessagesIcon(),
      to: '/profile/messages',
      id: 'messages'
    },
    {
      title: user?.email && user.firstName ? user.firstName : 'My Profile',
      icon: user?.email && user.avatar && user.avatar.url ? <Avatar src={user.avatar.url} /> : <Profile />,
      to: '/profile/posts',
      id: 'profile'
    }
  ];

  const onAddButton = user?.email ? (e) => e.currentTarget.previousSibling.click() : () => setIsRequireAuth(true);
  const href = ({id, to}) => (hiddenPages.includes(id) && !user?.email ? '#' : to);
  const onLink = (id) => (hiddenPages.includes(id) && !user?.email ? setIsRequireAuth(true) : null);

  async function onChange(e) {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      try {
        const medias = await Promise.all(
          files.map((file) => {
            return new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.addEventListener('load', (ev) => {
                resolve({
                  id: v1(),
                  file,
                  result: ev.target.result
                });
              });
              reader.addEventListener('error', reject);
              reader.readAsDataURL(file);
            });
          })
        );
        push('/post/new', {medias});
      } catch (error) {
        console.log(`error`, error);
      }
    }
  }

  return (
    <Container>
      <audio ref={audioRef} style={{display: 'none'}}>
        <source src={audio} type="audio/wav" />
      </audio>
      {pages.map((page) =>
        currentPage === 'feed' && page.id === 'cart' ? (
          <AddPost key={page.id}>
            <input onChange={onChange} type="file" style={{display: 'none'}} multiple accept="image/*, video/*" />
            <AddButton onClick={onAddButton}>
              <Union />
            </AddButton>
            Add Post
          </AddPost>
        ) : (
          <NavLink
            highlighted={page.isHighlighted && currentPage !== 'feed' ? 'true' : undefined}
            current={currentPage === page.id ? 'true' : undefined}
            notStroke={page.id === 'home'}
            key={page.id}
            to={() => href(page)}
            onClick={() => onLink(page.id)}
          >
            {page.icon}
            <span className="title">{page.title}</span>
          </NavLink>
        )
      )}
      <AuthRequest />
    </Container>
  );
};

export default BottomNavigation;
