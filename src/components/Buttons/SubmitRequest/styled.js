import styled from 'styled-components';
import {Button} from 'antd';

const StyledButton = styled(Button)`
  &&& {
    ${({customStyles}) => customStyles || null}
  }
  height: 28px;
  width: 140px;
  background: #ed484f;
  border-radius: 24px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #ffffff;
`;

export default StyledButton;
