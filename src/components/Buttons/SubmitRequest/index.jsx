import React from 'react';

import StyledButton from './styled';

const SubmitRequest = ({children, ...props}) => {
  return (
    <StyledButton type="danger" shape="round" {...props}>
      {children || 'Submit Request'}
    </StyledButton>
  );
};

export default SubmitRequest;
