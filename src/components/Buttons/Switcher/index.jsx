import React from 'react';

import {Containter} from 'components/Buttons/Switcher/styled';

const Switcher = ({isActive, onClick = () => {}}) => {
  return (
    <Containter isActive={isActive} onClick={onClick}>
      <div />
    </Containter>
  );
};

export default Switcher;
