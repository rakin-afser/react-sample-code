import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const Containter = styled.div`
  background: #c3c3c3;
  border-radius: 100px;
  width: 36px;
  height: 20px;
  transition: background 0.4s;
  position: relative;
  cursor: pointer;

  & div {
    position: absolute;
    left: 2px;
    top: 2px;
    background: #ffffff;
    border-radius: 50%;
    width: 16px;
    height: 16px;
    transition: all 0.4s;
  }

  ${({isActive}) =>
    isActive &&
    `
    background: ${primaryColor};
    & div {
      left: 18px;
    }
  `}
`;
