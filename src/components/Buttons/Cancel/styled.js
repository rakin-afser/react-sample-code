import styled from 'styled-components';
import {Button} from 'antd';

const StyledButton = styled(Button)`
  &&& {
    ${({customStyles}) => customStyles || null}
  }
  && {
    color: #000000;
    font-family: Helvetica;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
  }
  height: 28px;
  width: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export default StyledButton;
