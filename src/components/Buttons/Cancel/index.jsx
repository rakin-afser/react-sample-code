import React from 'react';

import StyledButton from './styled';

const Cancel = ({children, ...props}) => {
  return (
    <StyledButton type="link" {...props}>
      {children || 'Cancel'}
    </StyledButton>
  );
};

export default Cancel;
