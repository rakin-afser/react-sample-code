import styled, {css} from 'styled-components';
import media from '../../constants/media';
import {mainBlackColor as black, mainWhiteColor as white, primaryColor as primary} from 'constants/colors';
import {Button} from 'antd';

const Parent = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  background: transparent;
  border: none;
  outline: none;
  text-decoration: none;
  font-family: 'Helvetica Neue', sans-serif;
  cursor: pointer;
  transition: all 0.3s ease;
`;

export const AddToCart = styled(Parent)`
  width: 160px;
  height: 36px;
  border: 1px solid #ed484f;
  color: #ed484f;
  border-radius: 24px;
`;

export const Buy = styled(AddToCart)`
  background: #ed484f;
  border-radius: 24px;
  color: #fff;
`;

export const SeeMore = styled(Parent)`
  position: relative;
  width: 47px;
  height: 47px;
  border-radius: 50%;
  border: 3px solid #ed484f;
  font-family: Helvetica, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  text-align: center;
  color: #000000;
  margin: ${({withText}) => (withText ? '0 0 24px 0' : '0')};

  &::before {
    content: ${({withText}) => (withText ? `'See more'` : null)};
    position: absolute;
    top: calc(100% + 10px);
    left: 50%;
    transform: translateX(-50%);
    white-space: nowrap;
  }
`;

export const Follow = styled(Parent)`
  width: 99px;
  height: 29px;
  font-size: 14px;
  line-height: 140%;
  color: ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border: 1px solid ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border-radius: 24px;
  padding-top: 2px;
  transition: 0.3s;

  svg {
    margin-right: ${({active}) => (active ? 12 : 8)}px;
  }

  svg,
  svg path {
    transition: 0.3s;
  }

  @media (max-width: ${media.mobileMax}) {
    height: 25px;
    width: 83px;
    font-size: 12px;
    line-height: 13px;
  }

  @media (min-width: ${media.mobileMax}) {
    ${({active}) =>
      !active &&
      css`
        &:hover {
          color: #fff;
          background: #ed484f;

          svg path,
          svg {
            fill: #fff !important;
          }
        }
      `}
  }
`;

export const Following = styled(Follow)`
  color: #545454;
  border: 1px solid #c3c3c3;
`;

export const SendMessage = styled(Parent)`
  width: 99px;
  height: 29px;
  font-size: 14px;
  line-height: 140%;
  color: #666666;
  border: 1px solid #8f8f8f;
  border-radius: 24px;
  padding-top: 2px;

  & svg {
    margin-right: 8px;
  }

  @media (max-width: ${media.mobileMax}) {
    height: 25px;
    width: 98px;
    font-size: 12px;
    line-height: 13px;
  }
`;

export const LoadMore = styled(Parent)`
  width: 140px;
  height: 36px;
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  padding-top: 4px;
  color: #ed484f;
  border: 1px solid #ed484f;
  border-radius: 24px;
  justify-content: center;
  flex-direction: row-reverse;

  & svg {
    margin-left: 18px;
    margin-bottom: 2px;
  }
`;

export const Settings = styled(Parent)`
  min-width: 100px;
  height: 30px;
  font-size: 14px;
  line-height: 140%;
  color: #666666;
  border: 1px solid #8f8f8f;
  border-radius: 24px;

  & svg {
    margin-right: 8px;
  }

  &:hover {
    background: #8f8f8f;
    color: #ffffff;

    & svg path {
      fill: #ffffff;
    }
  }
`;

export const GoToStore = styled(Parent)`
  min-width: 130px;
  font-size: 14px;
  color: #545454;
  border: 1px solid #c3c3c3;
  border-radius: 24px;
  padding: 2px 5px;

  &:hover {
    background: #c3c3c3;
    color: #fff;
  }
`;

export const Cancel = styled.button`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: ${black};
  background: ${white};
  padding: 8px 39px;

  &:hover {
    text-decoration: underline;
  }
`;

export const Report = styled.button`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: ${white};
  background: ${primary};
  border-radius: 24px;
  padding: 8px 39px;
  min-width: 120px;
`;

export const OK = styled(Report)``;

export const AddNew = styled(Button)`
  &&& {
    width: 100%;
    border: 1px solid #efefef;
    background: #ffffff;
    height: 72px;
    color: #000;
    font-weight: 500;

    i {
      margin-right: 21px;
      line-height: 1;
    }
  }
`;
