import React from 'react';
import {
  AddToCart,
  Buy,
  SeeMore,
  Follow,
  Following,
  SendMessage,
  LoadMore,
  Settings,
  GoToStore,
  Cancel,
  Report,
  OK,
  AddNew
} from './styled';
import Icons from '../Icon';

export const components = {
  addToCart: {
    component: AddToCart,
    text: 'Add to cart'
  },
  buy: {
    component: Buy,
    text: 'Buy now'
  },
  seeMore: {
    component: SeeMore,
    icon: 'arrow'
  },
  follow: {
    component: Follow,
    text: 'Follow',
    icon: 'plus'
  },
  following: {
    component: Following,
    text: 'Following'
  },
  sendMessage: {
    component: SendMessage,
    text: 'Message',
    icon: 'sendMessage'
  },
  loadMore: {
    component: LoadMore,
    text: 'Load More',
    icon: 'plus'
  },
  goToStore: {
    component: GoToStore,
    text: 'Go to Store'
  },
  settings: {
    component: Settings,
    text: 'Settings',
    icon: 'settings'
  },
  cancel: {
    component: Cancel,
    text: 'Cancel'
  },
  report: {
    component: Report,
    text: 'Report'
  },
  ok: {
    component: OK,
    text: 'OK'
  },
  addNew: {
    component: AddNew,
    text: 'Add New',
    icon: 'plus',
    iconProps: {
      width: 14,
      height: 14
    }
  }
};

const Button = ({type, props, children, title = '', ...otherProps}) => {
  const Component = components[type].component;
  const text = title || components[type].text || null;
  const icon = components[type].icon;
  const iconProps = components[type].iconProps || null;

  return (
    <Component {...props} {...otherProps}>
      {props && props.icon ? props.icon : icon ? <Icons {...iconProps} type={icon} /> : null}
      {text}
      {children && children}
    </Component>
  );
};

export default Button;
