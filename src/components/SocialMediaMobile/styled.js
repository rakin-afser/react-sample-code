import styled from 'styled-components';

export const Container = styled.div`
  padding: 16px 0 18px 16px;
`;

export const Title = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  margin-bottom: 20px;
  color: #000;
`;

export const Icons = styled.div`
  display: flex;

  svg,
  img {
    margin-right: 20px;
  }
`;
