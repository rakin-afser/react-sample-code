import React from 'react';
import {Container, Title, Icons} from './styled';
import Facebook from '../../assets/Facebook';
import Twitter from '../../assets/Twitter';
import Instagram from '../../assets/Instagram';
import ins from 'images/instagram.png';

const SocialMediaMobile = () => {
  return (
    <Container>
      <Title>Social Media</Title>
      <Icons>
        <Facebook fill="#3B5998" width={20} height={20} />
        <Twitter fill="#55ACEE" width={20} height={20} />
        <img src={ins} alt="ins" />
      </Icons>
    </Container>
  );
};

export default SocialMediaMobile;
