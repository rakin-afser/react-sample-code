import React from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';

import Tools from 'components/Tools';
import Instagram from 'assets/Instagram';
import Twitter from 'assets/Twitter';
import Facebook from 'assets/Facebook';
import useGlobal from 'store';
import FooterBlock from './components/FooterBlock';

import americanExpress from './img/americanExpress.png';
import masterCard from './img/masterCard.svg';
import paypal from './img/paypal.svg';
import visa from './img/visa.svg';

import {
  Wrapper,
  Container,
  Copyright,
  CopyrightLinks,
  CopyrightText,
  FooterArea,
  FooterSocials,
  FooterPay
} from './styled';

const Footer = ({isMobile, isFooterShort, hideCopyright}) => {
  const [globalState] = useGlobal();
  const {isArabic} = globalState;
  const {t} = useTranslation();

  return (
    <Wrapper className="footer" isFooterShort={isFooterShort}>
      <FooterArea isFooterShort={isFooterShort} dir={isArabic ? 'rtl' : null}>
        <FooterBlock
          title={t('footer.navigation.sell')}
          links={[
            {url: '/', text: t('footer.navigation.startAShop')},
            {url: '/', text: t('footer.navigation.howToSell')},
            {url: 'http://seller.testSample.com/seller-landing/#pricing', text: t('footer.navigation.pricingPlans')}
          ]}
        />

        <FooterBlock
          title={t('footer.navigation.shop')}
          links={[
            {url: '/about/how-to-buy', text: t('footer.navigation.howToBuy')},
            {url: '/about/payments', text: t('footer.navigation.payments')},
            {url: '/about/returns', text: t('footer.navigation.returns')}
          ]}
        />

        <FooterBlock
          title={t('footer.navigation.shareAndEarn')}
          links={[
            {url: '/about/rate-&-earn', text: t('footer.navigation.rateAndEarn')},
            {url: '/about/save-to-lists', text: t('footer.navigation.saveToLists')},
            {url: '/about/influencer-rewards', text: t('footer.navigation.influencerReward')}
          ]}
        />

        <FooterBlock
          title={t('footer.navigation.about')}
          links={[
            {url: '/about/about-us', text: t('footer.navigation.aboutUs')},
            {url: '/about/contact-us', text: t('footer.navigation.contactUs')},
            {url: '/about/faqs', text: t('footer.navigation.faq')}
          ]}
          width="12.5%"
        />

        <FooterBlock width="32.6%" staticBlock>
          <Tools />

          <FooterSocials>
            <a rel="noreferrer" href="https://www.instagram.com/testSamplemarketplace/" target="_blank">
              <Instagram />
            </a>
            <a rel="noreferrer" href="https://twitter.com/testSample_me" target="_blank">
              <Twitter />
            </a>
            <a rel="noreferrer" href="https://www.facebook.com/testSamplemarketplace/" target="_blank">
              <Facebook />
            </a>
          </FooterSocials>

          {!isMobile && (
            <FooterPay>
              <img src={masterCard} alt="Mastercard" />
              <img src={visa} alt="Visa" />
              <img src={paypal} alt="Paypal" />
              <img style={{width: 67}} src={americanExpress} alt="American Express" />
            </FooterPay>
          )}
        </FooterBlock>
      </FooterArea>
      {!hideCopyright && (
        <Copyright>
          <Container style={{padding: isMobile ? 0 : 15}}>
            <CopyrightLinks>
              <li>
                <a href="/privacy-policy">Privacy Policy</a>
              </li>
              <li>
                <a href="/terms-conditions">Terms of use</a>
              </li>
              <li>
                <a href="/about/faqs">Help</a>
              </li>
            </CopyrightLinks>

            <CopyrightText>{t('footer.copyright')}</CopyrightText>
          </Container>
        </Copyright>
      )}
    </Wrapper>
  );
};

Footer.defaultProps = {
  isMobile: false,
  isFooterShort: false,
  hideCopyright: false
};

Footer.propTypes = {
  isMobile: PropTypes.bool,
  isFooterShort: PropTypes.bool,
  hideCopyright: PropTypes.bool
};

export default Footer;
