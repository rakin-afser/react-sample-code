import styled from 'styled-components/macro';
import {grayTextColor, transparentTextColor, primaryColor} from 'constants/colors';
import media from 'constants/media';

import {ContentWrapper} from 'globalStyles';
import {mainFont} from 'constants/fonts';

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  box-shadow: inset 0 1px 0 rgba(0, 0, 0, 0.16);
  margin-top: 86px;
  background-color: #fefefe;
  color: #000;

  @media (max-width: ${media.mobileMax}) {
    margin-top: 24px;
  }

  ${({isFooterShort}) => (isFooterShort ? 'margin-top: 56px;' : null)}
`;

export const FooterArea = styled(ContentWrapper)`
  width: 100%;
  max-width: 1440px;
  display: flex;
  flex-wrap: wrap;
  padding: 62px 118px 41px 132px;

  @media (max-width: ${media.mobileMax}) {
    padding: 18px 32px 0;
  }
  ${({isFooterShort}) => (isFooterShort ? 'display: none' : null)};
`;

export const FooterSocials = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-bottom: 31px;
  margin-left: auto;

  a {
    margin-left: 50px;

    &:first-child {
      margin-left: auto;
    }
  }

  svg {
    cursor: pointer;
    transition: all 0.3s ease;
    fill: ${transparentTextColor};

    &:hover {
      fill: ${primaryColor};
    }
  }

  @media (max-width: ${media.mobileMax}) {
    order: 1;
    width: 100%;
    margin: 6px auto 26px;
    max-width: 300px;

    a {
      margin: 0 auto;
    }

    svg {
      fill: #000;
    }
  }
`;

export const FooterPay = styled.div`
  position: relative;
  right: -10px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-left: 11px;

  img {
    margin-left: 28px;

    &:first-child {
      margin-left: auto;
      margin-top: 3px;
    }
  }
`;

export const Copyright = styled.div`
  background-color: #fafafa;
  color: #000;
  box-shadow: inset 0px 1px 0px rgba(0, 0, 0, 0.16);
  padding: 10px 0 8px;

  @media (max-width: ${media.mobileMax}) {
    padding: 24px 0 0;
    box-shadow: none;
    background: transparent;
  }
`;

export const CopyrightLinks = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  display: inline-flex;
  align-items: center;

  li {
    font-family: ${mainFont};
    margin-right: 25px;
  }

  a {
    color: inherit;
  }

  @media (max-width: ${media.mobileMax}) {
    padding: 0 24px 24px;
    text-align: center;
    width: 100%;
    max-width: 400px;
    margin: 0 auto;

    li {
      font-size: 14px;
      line-height: 17px;
      color: #fff;
      margin: 0 auto;

      a {
        font-size: inherit;
        line-height: inherit;
        color: #000;
      }
    }
  }
`;

export const CopyrightText = styled.span`
  margin-left: auto;
  font-family: ${mainFont};
  font-size: 14px;
  line-height: 14px;
  text-align: right;
  color: #545454;

  @media (max-width: ${media.mobileMax}) {
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    color: #666;
    margin: 0 auto;
    padding: 13px 20px;
    width: 100%;
    text-align: center;
    background-color: #efefef;
  }
`;

export const Container = styled(ContentWrapper)`
  padding-left: 15px;
  max-width: 1205px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;
