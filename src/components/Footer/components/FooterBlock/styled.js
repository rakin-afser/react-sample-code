import styled from 'styled-components/macro';
import media from 'constants/media';

export const Wrapper = styled.div`
  width: ${({width}) => width || '14.9%'};

  &:nth-of-type(3) {
    width: 17%;
  }

  &:nth-of-type(5) {
    margin-left: ${({isArabic}) => (isArabic ? '0' : 'auto')};
    margin-right: ${({isArabic}) => (isArabic ? 'auto' : '0')};
  }

  @media (max-width: ${media.mobileMax}) {
    display: flex;
    flex-wrap: wrap;
    width: 100%;
    margin-bottom: 16px;

    &:nth-of-type(3) {
      width: 100%;
    }

    &:not(:first-child) {
      border-top: 1px solid #efefef;
      padding-top: 20px;
    }

    ${({staticBlock}) => {
      if (staticBlock) {
        return `
          &:not(:first-child) {
            border-top: 0;
          }
        `;
      }
    }}
  }
`;

export const Title = styled.p`
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 1;
  letter-spacing: -0.384px;
  color: #000;
  margin-top: 18px;
  margin-bottom: 32px;
  display: block;
  width: 100%;
  position: relative;

  svg {
    path {
      fill: #fff;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 14px;
    line-height: 16px;
    letter-spacing: 0px;
    margin-bottom: 0;
    cursor: pointer;
  }
`;

export const Links = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;

  @media (max-width: ${media.mobileMax}) {
    transition: all 0.3s ease;
    overflow: hidden;
    position: relative;
    max-height: 200px;

    ${({active}) =>
      active
        ? `
        max-height: 400px;
        padding-top: 12px;
      `
        : `
        max-height: 0;
        padding-top: 0;
      `}
  }
`;

export const CustomLink = styled.a`
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 140%;
  letter-spacing: -0.256px;
  color: #000;
  display: block;
  width: 100%;

  &:not(:last-child) {
    margin-bottom: 22px;
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 14px;
    line-height: 17px;
    color: #333;

    &:not(:last-child) {
      margin-bottom: 8px;
    }
  }
`;

export const IconArrowFooter = styled.span`
  position: absolute;
  top: 50%;
  right: 5px;
  height: 7px;
  transition: all 0.3s ease;
  transform: translate(0, -50%) rotate(180deg);
  display: none;

  @media (max-width: ${media.mobileMax}) {
    display: initial;
  }

  svg {
    path {
      fill: #333;
    }
  }

  ${({active}) =>
    active
      ? `
      transform: translate(0, -50%) rotate(0deg);
    `
      : `
      transform: translate(0, -50%) rotate(180deg);
    `}
`;
