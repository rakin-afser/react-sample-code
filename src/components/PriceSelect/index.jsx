import React, {useState, useEffect} from 'react';
import {Slider} from 'antd';

import {PriceTitle, PriceSlider, PriceContainer, PriceLow, PriceHigh} from './styled';

const PriceSelect = ({
  title = '',
  min = 0,
  max = 1000,
  range = true,
  tipFormatter = null,
  currentPrice = [10, 1000],
  onChange = () => {},
  currencyType = '$',
  disabled
}) => {
  const [[lowPrice, highPrice], setPrice] = useState([currentPrice[0] || min, currentPrice[1] || max]);

  const handleChangeSlider = (values) => {
    setPrice(values);
  };

  useEffect(() => {
    // setPrice(currentPrice);
  }, [currentPrice]);

  return (
    <div>
      {title && <PriceTitle>Price</PriceTitle>}
      <PriceSlider>
        <Slider
          onAfterChange={() => onChange([lowPrice, highPrice])}
          min={min}
          max={max}
          range={range}
          disabled={disabled}
          tipFormatter={tipFormatter}
          value={[lowPrice, highPrice]}
          onChange={handleChangeSlider}
        />
        <PriceContainer>
          <PriceLow>
            {currencyType}
            {lowPrice}
          </PriceLow>
          <PriceHigh>
            {currencyType}
            {highPrice}
          </PriceHigh>
        </PriceContainer>
      </PriceSlider>
    </div>
  );
};

export default PriceSelect;
