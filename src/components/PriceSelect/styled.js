import styled from 'styled-components';

export const PriceTitle = styled.h3`
  font-family: Helvetica Neue;
  margin-top: 23px;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 140%;
  color: #000000;
`;

export const PriceSlider = styled.div`
  .ant-slider {
    // margin: 24px 7px 7px -8px;
  }

  & .ant-slider-rail {
    background: #e4e4e4;
    height: 10px;
    border-radius: 10px;
  }
  & .ant-slider-track {
    height: 10px;
    background-color: #f6a5a9;
  }
  & .ant-slider-handle {
    width: 20px;
    height: 20px;
    border: solid 2px #ed484f;
    border-radius: 4px;
  }
  & .ant-slider:hover .ant-slider-track {
    background-color: #f6a5a9;
  }
  & .ant-slider:hover .ant-slider-handle:not(.ant-tooltip-open) {
    border-color: #ed484f;
  }
  & .ant-slider-handle:focus {
    border-color: #ed484f;
    box-shadow: none;
  }
`;

export const PriceContainer = styled.div`
  display: flex;
  justify-content: space-between;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #000000;
  padding: 15px 10px 0;
`;

export const PriceLow = styled.div``;

export const PriceHigh = styled.div``;
