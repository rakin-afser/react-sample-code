import React from 'react';

import {Avatar, Follow, Number, Wrapper, TextWrapper} from './styled';
import useGlobal from 'store';
import userPlaceholder from 'images/placeholders/user.jpg';
import avatar1 from 'images/products/card1.jpg';
import avatar2 from 'images/products/product2.jpg';

const ListFollowers = ({followers = []}) => {
  const [, setGlobalState] = useGlobal();

  const followersTemp = [
    {name: 'Kylie Jenner', avatar: avatar1},
    {name: 'Scarlett Robertson', avatar: avatar2},
    {name: '123'},
    {name: '345'}
  ];

  return (
    <Wrapper>
      <Avatar src={followersTemp?.[0]?.avatar || userPlaceholder} />
      <Avatar last src={followersTemp?.[1]?.avatar || userPlaceholder} />
      <TextWrapper>
        <Follow>{followersTemp?.[0]?.name}</Follow>
        {followersTemp?.length - 1 > 0 && (
          <>
            <Follow thin>and</Follow>
            <Number onClick={() => {}} style={{fontSize: 12}}>
              {followersTemp?.length - 1} more
            </Number>
          </>
        )}
        <Follow thin>follow this list</Follow>
      </TextWrapper>
    </Wrapper>
  );
};

export default ListFollowers;
