import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  justify-content: flex-start;
  flex-grow: 1;
`;

export const Avatar = styled.img`
  width: 32px;
  height: 32px;
  background: #fff;
  padding: 2px;
  margin-left: -8px;
  flex-shrink: 0;
  border-radius: 50%;
  overflow: hidden;
  margin-right: ${({last}) => (last ? '8' : '0')}px;

  &:first-child {
    margin-left: 0;
  }
`;

export const Follow = styled.div`
  display: inline-block;
  font-family: Helvetica Neue, sans-serif;
  font-style: normal;
  font-weight: ${({thin}) => (thin ? '300' : '500')};
  font-size: 12px;
  line-height: 120%;
  color: ${({thin}) => (thin ? '#545454' : '#000')};
  letter-spacing: 0.06em;
  margin-right: 2px;
`;

export const Number = styled.a`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #287bdd;
  margin-right: 5px;
`;

export const TextWrapper = styled.div``;
