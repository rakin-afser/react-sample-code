import React, {useState} from 'react';
import {useWindowSize} from '@reach/window-size';
import PropTypes, {string, number, bool, func} from 'prop-types';
import AddToList from 'components/AddToList';
import Actions from 'components/Actions';

import Heart from 'assets/Heart';
import FilledHeart from 'assets/FilledHeart';
import Bookmark from 'assets/Bookmark';
import FilledBookmark from 'assets/FilledBookmark';
import {
  CardFooter,
  ImageContainer,
  Image,
  Card,
  Likes,
  BookmarkIconsWrapper,
  Title,
  Shipping,
  Price,
  Tools,
  Info,
  OldPrice,
  Sale,
  Red,
  SaleBold
} from './styled';

const CardProductShop = ({
  productId,
  title,
  imgSrc,
  isLiked,
  isWished,
  newPrice,
  oldPrice,
  inline,
  onLikeClick,
  onClick
}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [showAddToList, setShowAddToList] = useState(false);

  const onWishClick = () => setShowAddToList(true);

  let sale = null;

  if (oldPrice && newPrice)
    sale = (100 - (newPrice?.replace(/^\D+/g, '') / oldPrice?.replace(/^\D+/g, '')) * 100).toFixed(0);

  return (
    <div>
      <Card inline={inline}>
        <ImageContainer inline={inline} onClick={onClick}>
          <Image src={imgSrc} alt="product" inline={inline} />
          {sale && !inline ? (
            <Sale>
              -<SaleBold>{sale}</SaleBold>%
            </Sale>
          ) : null}

          {!inline && isMobile && (
            <Likes>
              <Heart isLiked={isLiked} />
            </Likes>
          )}
        </ImageContainer>
        <CardFooter inline={inline}>
          <Info inline={inline}>
            <Price inline={inline} sale={sale}>
              <Red sale={sale}>{newPrice}</Red>
              {oldPrice && <OldPrice>{oldPrice}</OldPrice>}
              <Shipping inline={inline}>Free Shipping</Shipping>
            </Price>
          </Info>
          <Title inline={inline.toString()} to="/product">
            {title}
          </Title>

          {isMobile ? (
            <>
              {inline && (
                <Likes
                  inline={inline}
                  onClick={() => {
                    console.log(Actions);
                  }}
                >
                  <Heart isLiked={isLiked} />
                </Likes>
              )}
              <Actions inline={inline} />
            </>
          ) : (
            <Tools inline={inline}>
              <Likes isLiked={isLiked} onClick={onLikeClick}>
                <FilledHeart width={16.67} height={15} />
                <Heart isLiked={isLiked} width={16.67} height={15} />
              </Likes>

              <BookmarkIconsWrapper onClick={onWishClick} isWished={isWished}>
                <Bookmark isWished={isWished} />
                <FilledBookmark />
              </BookmarkIconsWrapper>

              {!isMobile && (
                <AddToList productId={productId} showAddToList={showAddToList} setShowAddToList={setShowAddToList} />
              )}
            </Tools>
          )}
        </CardFooter>
      </Card>
    </div>
  );
};

CardProductShop.propTypes = {
  title: string.isRequired,
  imgSrc: string.isRequired,
  isLiked: bool.isRequired,
  isWished: bool.isRequired,
  newPrice: number,
  oldPrice: number,
  inline: bool,
  onLikeClick: func,
  onClick: func,
  productId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired
};

CardProductShop.defaultProps = {
  newPrice: null,
  oldPrice: null,
  inline: false,
  onClick: () => {}
};

export default CardProductShop;
