import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {mainWhiteColor, mainBlackColor, transparentTextColor, secondaryTextColor, primaryColor} from 'constants/colors';
import {FlexContainer} from 'globalStyles';
import media from 'constants/media';
import search from './img/search.svg';

export const Card = styled(FlexContainer)`
  position: relative;
  flex-direction: column;
  width: 262px;
  height: 369px;
  background: ${mainWhiteColor};
  cursor: pointer;
  box-shadow: 0 2px 9px rgba(0, 0, 0, 0.09);
  border-radius: 4px;
  background: #ffffff;
  mix-blend-mode: normal;

  @media (max-width: ${media.mobileMax}) {
    min-width: 159px;
    width: 159px;
    height: auto;
    margin: 0 4px;
    box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
    border-radius: 4px;

    svg {
      max-width: 12px;
      max-height: 14px;
    }

    ${({inline}) => {
      if (inline) {
        return `
          width: 100%;
          height: auto;
          background: #FFFFFF;
          border: 1px solid #EEEEEE;
          box-sizing: border-box;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.01);
          border-radius: 2px;
          padding: 10px;
          display: flex;
          flex-wrap: wrap;
          flex-direction: initial;
          margin: 0;
        `;
      }
    }}
  }
`;

export const Info = styled(FlexContainer)`
  width: 100%;

  @media (max-width: ${media.mobileMax}) {
    flex-wrap: wrap;

    ${({inline}) => {
      if (inline) {
        return `
            order: 2;
            flex-wrap: wrap;
          `;
      }
    }}
  }
`;

export const Tools = styled(FlexContainer)`
  align-items: center;
  width: 100%;

  ${({inline}) => {
    if (inline) {
      return `
          order: 3;
          margin-top: 18px;
          display: none;
        `;
    }
  }}
`;

export const ImageContainer = styled.div`
  position: relative;

  &::after {
    content: '';
    background-color: rgba(0, 0, 0, 0.44);
    background-image: url(${search});
    background-position: center center;
    background-repeat: no-repeat;
    position: absolute;
    width: 100%;
    min-width: 80px;
    height: 100%;
    min-height: 80px;
    z-index: 99;
    top: 0;
    left: 0;
    opacity: 0;
    visibility: hidden;
    transition: opacity 0.3s ease-in, visibility 0.3s ease-in;
  }

  &:hover::after {
    opacity: 1;
    visibility: visible;
  }

  ${({inline}) => {
    if (inline) {
      return `
      position: static;
    `;
    }
  }}
`;

export const Image = styled.img`
  width: 100%;
  height: 265px;
  object-fit: cover;

  @media (max-width: ${media.mobileMax}) {
    height: 174px;
    object-fit: cover;
    border-radius: 4px 4px 0 0;
    width: 100%;

    ${({inline}) => {
      if (inline) {
        return `
            width: 100px;
            height: 100px;
            border-radius: 1px;
            margin: 0;
          `;
      }
    }}
  }
`;

export const CardFooter = styled(FlexContainer)`
  width: 100%;
  height: 128px;
  flex-direction: column;
  letter-spacing: -0.6px;
  padding: 6px 15px 22px 15px;

  @media (max-width: ${media.mobileMax}) {
    padding: 6px 7px 8px;
    position: relative;
    height: auto;

    ${({inline}) => {
      if (inline) {
        return `
            width: calc(100% - 100px);
            padding-left: 10px;
            margin: 0 0 auto;
            padding: 0 0 0 10px;
          `;
      }
    }}
  }
`;

export const Price = styled.div`
  font-size: 16px;
  color: #8f8f8f;
  width: 100%;
  @media (max-width: ${media.mobileMax}) {
    font-size: 14px;
    font-weight: 500;
    line-height: 12px;
    color: ${({sale}) => (sale ? primaryColor : '#000')};
    line-height: 1;
    margin-top: 2px;

    small {
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 140%;
      color: #8f8f8f;
      letter-spacing: 0;
    }

    ${({inline}) => {
      if (inline) {
        return `
            order: 2;
            font-size: 14px;
          `;
      }
    }}
  }
`;

export const OldPrice = styled.span`
  color: #a7a7a7;
  margin-left: 6px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  text-decoration-line: line-through;
  text-transform: uppercase;

  & small {
    font-size: 12px;
  }

  @media (max-width: ${media.mobileMax}) {
    font-weight: normal;
    font-size: 12px;
    line-height: 10px;
    text-decoration-line: line-through;
    color: #999999;
    margin-left: 10px;

    small {
      font-weight: normal;
      font-size: 14px;
    }

    ${({inline}) => {
      if (inline) {
        return `
          font-size: 12px;
          margin-left: 10px;
        `;
      }
    }}
  }
`;

export const Shipping = styled.span`
  font-size: 12px;
  color: ${transparentTextColor};
  display: inline-block;
  float: right;
  margin-top: 4px;

  @media (max-width: ${media.mobileMax}) {
    font-size: 10px;
    line-height: 1;
    color: #999999;
    margin-right: -1px;
    letter-spacing: -0.1px;
    margin-top: 6px;
    width: 100%;
    letter-spacing: -0.3px;

    ${({inline}) => {
      if (inline) {
        return `
            order: 1;
            font-style: normal;
            font-weight: normal;
            font-size: 12px;
            line-height: 1.4;
            color: #8F8F8F;
            margin: 1px 0 10px;
            width: 100%;
          `;
      }
    }}
  }
`;

export const Title = styled(Link)`
  width: 100%;
  font-size: 14px;
  margin: 0 0 10px;
  color: ${mainBlackColor};
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;

  @media (max-width: ${media.mobileMax}) {
    font-size: 10px;
    color: #000;
    margin: 9px 0 0;
    font-weight: normal;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-family: 'SF Pro Display';
    line-height: 14px;
    letter-spacing: 0;

    ${({inline}) => {
      if (inline) {
        return `
            order: 1;
            margin: 0 0 2px;
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;
            color: #000000;
          `;
      }
    }}
  }
`;

export const Likes = styled(FlexContainer)`
  font-size: 14px;

  svg.iconHeart {
    display: ${({isLiked}) => (isLiked ? 'none' : 'block')};
  }

  svg.iconFilledHeart {
    display: ${({isLiked}) => (isLiked ? 'block' : 'none')};
  }

  &:hover {
    svg.iconHeart {
      display: none;
    }

    svg.iconFilledHeart {
      display: block;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    position: absolute;
    bottom: 8px;
    right: 8px;
    width: 24px;
    height: 24px;
    background: rgba(255, 255, 255, 0.76);
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;

    svg {
      max-width: 12px;
      max-height: 11px;
    }

    ${({inline}) => {
      if (inline) {
        return `
          position: static;
          order: 5;
          margin: 14px auto 0 0;
        `;
      }
    }}
  }
`;

export const BookmarkIconsWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  height: 20px;

  svg.iconBookmark {
    display: ${({isWished}) => (isWished ? 'none' : 'block')};
  }

  svg.iconFilledBookmark {
    display: ${({isWished}) => (isWished ? 'block' : 'none')};
    margin-right: -4px;
  }

  &:hover {
    svg.iconBookmark {
      display: none;
    }

    svg.iconFilledBookmark {
      display: block;
    }
  }
`;

export const LikesCount = styled.span`
  color: ${secondaryTextColor};
  margin-left: 6px;

  @media (max-width: ${media.mobileMax}) {
    margin-left: 4px;
  }
`;

export const SaleBold = styled.span`
  font-weight: bold;
`;

export const Sale = styled.div`
  position: absolute;
  right: 4px;
  top: 4px;
  background: #f0646a;
  width: 35px;
  height: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 2px;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #ffffff;

  @media (max-width: ${media.mobileMax}) {
    width: 27px;
    height: 27px;
    top: 5px;
    right: 5px;
    background: #f0646a;
    border-radius: 2px;
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    text-align: center;
    color: #ffffff;
    line-height: 27px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const Actions = styled.div`
  position: absolute;
  right: 9px;
  bottom: 11px;
  display: flex;
  cursor: pointer;

  span {
    width: 4px;
    height: 4px;
    border-radius: 50%;
    background: #7c7e82;
    margin-left: 3px;
  }

  ${({inline}) => {
    if (inline) {
      return `
        bottom: inherit;
        right: 0;
        top: 10px;
      `;
    }
  }}
`;

export const Red = styled.span`
  color: #000;
  ${({sale}) => {
    if (sale) {
      return `
        color: #ED484F;
      `;
    }
  }}
`;
