import React from 'react';
import {string} from 'prop-types';

// import {featuredProducts} from 'constants/staticData';

import WithScroll from '../WithScroll';
import CardNewArrival from 'components/CardNewArrival';

import {Wrapper, SliderContainer} from './styled';

const ProductSlider = ({title, seeMoreText, products}) => {
  return (
    <Wrapper>
      <SliderContainer>
        <WithScroll
          marginTop={0}
          title={title}
          withSeeMore
          seeMoreText={seeMoreText}
          seeMoreCounter={null}
          height={275}
        >
          {products?.map((item) => (
            <CardNewArrival
              alwaysImage
              key={Object.prototype.hasOwnProperty.call(item, 'node') ? item?.node?.id : item?.id}
              node={Object.prototype.hasOwnProperty.call(item, 'node') ? item.node : item}
            />
          ))}
        </WithScroll>
      </SliderContainer>
    </Wrapper>
  );
};

ProductSlider.defaultProps = {
  title: 'More From This Seller',
  seeMoreText: 'See More'
};

ProductSlider.propTypes = {
  title: string,
  seeMoreText: string
};

export default ProductSlider;
