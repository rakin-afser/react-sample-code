import React, {useState} from 'react';
import {object} from 'prop-types';
import {useHistory} from 'react-router-dom';
import useGlobal from 'store';

import Icon from 'components/Icon';
import Title from '../Title';
import Popup from './popup';

import {
  Wrapper,
  Seller,
  SellerThumb,
  SellerContent,
  SellerLabel,
  SellerName,
  SellerButtons,
  SellerButton,
  SellerFollowers,
  SellerImages,
  SellerFollowersCount,
  Rating,
  Reviews
} from './styled';
import {useUser} from 'hooks/reactiveVars';

const AboutSeller = ({seller}) => {
  const [showPopup, setShowPopup] = useState(false);
  const history = useHistory();
  const [user] = useUser();
  const isSeller = user?.capabilities?.includes('seller');
  const [, setGlobalStore] = useGlobal();

  const goShopPage = () => {
    history.push(`/shop/${seller?.storeUrl}/products`);
  };

  const dataForMessenger = {
    databaseId: seller?.id,
    username: seller?.name,
    photoUrl: seller?.gravatar
  };

  const onShowMessenger = () => {
    setGlobalStore.setMessenger({
      data: isSeller ? null : dataForMessenger
    });
    history.push('/profile/messages');
  };

  return (
    <Wrapper>
      <Title title="About Seller" style={{marginBottom: 24}} />

      <Seller>
        <SellerThumb onClick={goShopPage} src={seller?.gravatar} />
        <SellerContent>
          <SellerLabel>Seller</SellerLabel>
          {seller?.name && <SellerName onClick={goShopPage}>{seller?.name}</SellerName>}

          {seller?.rating && (
            <Rating>
              <Icon type="star" svgStyle={{width: 11, height: 11, fill: seller?.rating >= 1 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 11, height: 11, fill: seller?.rating >= 2 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 11, height: 11, fill: seller?.rating >= 3 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 11, height: 11, fill: seller?.rating >= 4 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 11, height: 11, fill: seller?.rating >= 5 ? '#FFC131' : '#ccc'}} />

              {seller?.reviews && <Reviews>({seller?.reviews})</Reviews>}
            </Rating>
          )}

          <SellerButtons>
            <SellerButton>
              <Icon type="plus" />
              <span>Follow</span>
            </SellerButton>

            <SellerButton onClick={onShowMessenger} type="primary">
              <span>Chat with seller</span>
            </SellerButton>
          </SellerButtons>
        </SellerContent>
      </Seller>

      {seller?.followers && seller?.followers?.length && (
        <SellerFollowers>
          <SellerImages>
            {seller?.followers?.map(
              (follower, key) => key <= 1 && <img src={follower.avatar} key={key} alt={follower.name} />
            )}
          </SellerImages>
          <SellerFollowersCount>
            {seller?.followers[0].name} and{' '}
            <a href="#" onClick={() => setShowPopup(true)}>
              {seller?.followers?.length - 1} more
            </a>
            <span>started following this Seller</span>
          </SellerFollowersCount>
        </SellerFollowers>
      )}

      <Popup showPopup={showPopup} setShowPopup={setShowPopup} />
    </Wrapper>
  );
};

AboutSeller.defaultProps = {
  seller: {}
};

AboutSeller.propTypes = {
  seller: object
};

export default AboutSeller;
