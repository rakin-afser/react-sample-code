import React from 'react';

import Icon from 'components/Icon';
import {Wrapper} from './styled';

const BackLink = ({...props}) => {
  return (
    <Wrapper {...props}>
      <Icon type="arrowBack" />
    </Wrapper>
  );
};

BackLink.defaultProps = {};

BackLink.propTypes = {};

export default BackLink;
