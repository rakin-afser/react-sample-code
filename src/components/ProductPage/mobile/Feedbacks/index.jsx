import React, {useState} from 'react';
import {bool} from 'prop-types';
import _ from 'lodash';
import parse from 'html-react-parser';
import moment from 'moment';

import Icon from 'components/Icon';
import PhotosPopup from './photosPopup';
import CustomerFeedback from './customerFeedback';
import avatar1 from 'images/avatar1.png';
import avatar2 from 'images/avatar2.png';
import useGlobal from 'store';
import {
  Wrapper,
  Heading,
  ViewMore,
  RatingWrapper,
  Evaluation,
  EvaluationValue,
  EvaluationRating,
  Rating,
  RatingList,
  Row,
  Star,
  Line,
  Percent,
  Avatars,
  Photos,
  PhotosTitle,
  PhotosAvatar,
  Gallery,
  GalleryCol,
  GalleryItem,
  FeedbackItem,
  FeedbackItemHeader,
  FeedbackItemImg,
  FeedbackItemName,
  FeedbackItemRating,
  FeedbackItemText,
  FeedbackItemFooter,
  FeedbackGallery,
  FeedbackIcon,
  SeeMoreButton
} from './styled';
import Heart from 'assets/Heart';
import MessageIcon from 'assets/MessageIcon';
import userPlaceholder from 'images/placeholders/user.jpg';
import {setRating} from 'components/helpers';

const Feedbacks = ({data = {}, hideFooter, onClickSeeMore = () => {}, showSeeMoreBtn, disablePopup}) => {
  const [globalState, setGlobalState] = useGlobal();
  const [showCustomerFeedback, setShowCustomerFeedback] = useState(false);
  const [showPhotosPopup, setShowPhotosPopup] = useState(false);

  const reviews = data?.reviews?.nodes || [];

  const postsWithPhotos =
    reviews.filter(
      (item) => !_.isEmpty(item?.galleryImages?.nodes?.filter((i) => i?.mimeType?.split('/')?.[0] === 'image'))
    ) || [];

  const displayOnePhotoPerPost = postsWithPhotos?.length > 7;

  const openFeedback = ()=>{
    setGlobalState.setFeedback({isOpen: true, data});
  }

  return (
    <Wrapper>
      <Heading>
        <span>Customer Feedback</span>
        <ViewMore onClick={onClickSeeMore}>View All</ViewMore>
      </Heading>

      <RatingWrapper>
        <Evaluation>
          <EvaluationValue>
            4,7<span>/5</span>
          </EvaluationValue>

          <EvaluationRating>
            <Rating>
              <Icon type="star" svgStyle={{width: 12, height: 12, fill: 4.7 >= 1 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 12, height: 12, fill: 4.7 >= 2 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 12, height: 12, fill: 4.7 >= 3 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 12, height: 12, fill: 4.7 >= 4 ? '#FFC131' : '#ccc'}} />
              <Icon type="star" svgStyle={{width: 12, height: 12, fill: 4.7 >= 5 ? '#FFC131' : '#ccc'}} />
            </Rating>

            <span>{reviews?.length} feedbacks</span>
          </EvaluationRating>
        </Evaluation>

        <RatingList>
          <Row>
            <Star>
              5<Icon type="star" svgStyle={{width: 8, height: 8, fill: '#999'}} />
            </Star>
            <Line progress={66} background="#208C4E" />
            <Percent>66%</Percent>
          </Row>

          <Row>
            <Star>
              4<Icon type="star" svgStyle={{width: 8, height: 8, fill: '#999'}} />
            </Star>
            <Line progress={35} background="#2ECC71" />
            <Percent>35%</Percent>
          </Row>

          <Row>
            <Star>
              3<Icon type="star" svgStyle={{width: 8, height: 8, fill: '#999'}} />
            </Star>
            <Line progress={20} background="#FFC131" />
            <Percent>20%</Percent>
          </Row>

          <Row>
            <Star>
              2<Icon type="star" svgStyle={{width: 8, height: 8, fill: '#999'}} />
            </Star>
            <Line progress={12} background="#FF7F0B" />
            <Percent>12%</Percent>
          </Row>

          <Row>
            <Star>
              1<Icon type="star" svgStyle={{width: 8, height: 8, fill: '#999'}} />
            </Star>
            <Line progress={5} background="#E4171F" />
            <Percent>5%</Percent>
          </Row>
        </RatingList>
      </RatingWrapper>

      <Photos>
        <PhotosTitle>
          <span>Photos from Feedback</span>
        </PhotosTitle>

        <Gallery>
          {postsWithPhotos.map((item, key) => {
            const images = item?.galleryImages?.nodes.filter((i) => i?.mimeType?.split('/')?.[0] === 'image');
            const gallery = displayOnePhotoPerPost ? [images?.[0]] : images;

            return gallery?.map((image) => (
              <GalleryItem image={image?.mediaItemUrl} onClick={openFeedback} />
            ));
          })}
        </Gallery>

        <Avatars>
          <PhotosAvatar image={avatar1} />
          <PhotosAvatar image={avatar2} />
          <PhotosAvatar image={avatar2} last more={7} />
        </Avatars>
      </Photos>

      {reviews.map((el, key) => {
        const {
          author,
          content,
          rating = 0,
          generalOpinion,
          galleryImages,
          date,
          totalComments,
          totalLikes
        } = el;
        const media = galleryImages?.nodes?.filter((i) => i?.mimeType?.split('/')?.[0] === 'image');
        return (
          <FeedbackItem>
            <FeedbackItemHeader>
              <FeedbackItemImg src={author?.node?.profile_picture || userPlaceholder} />
              <FeedbackItemName>
                {author?.node?.name}
                <span>{moment(parse(date)).fromNow()}</span>
              </FeedbackItemName>
            </FeedbackItemHeader>
            <FeedbackItemRating>
              {setRating(rating)}
              <span>{generalOpinion}</span>
            </FeedbackItemRating>

            <FeedbackItemText>{parse(content)}</FeedbackItemText>

            <FeedbackGallery>
              <Gallery gutter={8}>
                {media?.map((item) => (
                  <GalleryItem gutter={8} image={item?.mediaItemUrl} onClick={openFeedback} />
                ))}
              </Gallery>
            </FeedbackGallery>

            {!hideFooter && (
              <FeedbackItemFooter>
                <FeedbackIcon>
                  <Heart />
                  {totalLikes}
                </FeedbackIcon>
                <FeedbackIcon>
                  <MessageIcon color="#ea2c34" />
                  {totalComments}
                </FeedbackIcon>
              </FeedbackItemFooter>
            )}
          </FeedbackItem>
        );
      })}

      <PhotosPopup showPhotosPopup={showPhotosPopup && !disablePopup} setShowPhotosPopup={setShowPhotosPopup} />

      <CustomerFeedback
        showCustomerFeedback={showCustomerFeedback && !disablePopup}
        setShowCustomerFeedback={setShowCustomerFeedback}
      />
    </Wrapper>
  );
};

Feedbacks.defaultProps = {
  hideFooter: false,
  showSeeMoreBtn: false,
  disablePopup: false
};

Feedbacks.propTypes = {
  hideFooter: bool,
  showSeeMoreBtn: bool,
  disablePopup: bool
};

export default Feedbacks;
