import styled from 'styled-components';

export const PopupWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 100;
  background: #fff;
  transition: all 0.3s ease;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  max-width: 100vw;

  opacity: ${({active}) => (active ? 1 : 0)};
  transform: translateY(${({active}) => (active ? '0px' : '-50px')});
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
`;

export const IconWrapper = styled.div`
  position: absolute;
  top: 10px;
  right: 3px;
  z-index: 110;
  cursor: pointer;
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 70px;
  border-bottom: 1px solid #efefef;
`;

export const Title = styled.h3`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #000000;
  margin: auto;
`;

export const Content = styled.div`

`;

export const Tabs = styled.div`
  display: flex;
`;

export const Tab = styled.div`
  width: 100%;
  cursor: pointer;
  height: 40px;
  font-weight: ${({active}) => active ? '700' : 'normal'};
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${({active}) => active ? '#000' : '#999'};
  border-bottom: 2px solid ${({active}) => active ? '#ED484F' : '#EFEFEF'};
  transition: all .3s ease;
`;

export const SubTab = styled(Tab)`
  border-bottom: 2px solid ${({active}) => active ? '#000' : '#EFEFEF'};
`;

export const TabContent = styled.div`
  width: 100%;
  height: ${({mini}) => mini ? 'calc(100% - 84px)' : 'calc(100% - 104px)'};
  margin-top: 24px;
`;

export const TableContainer = styled.div`
  display: flex;
  height: 100%;
  
  .ScrollbarsCustom-Content {
    display: flex;
  }
  
  .ScrollbarsCustom-Scroller {
    max-width: 100vw;
  }
  
  .TrackX {
    left: 0 !important;
    right: 0 !important;
    width: 100% !important;
    height: 4px !important;
    background: #EFEFEF !important;
  }
  
  .ScrollbarsCustom-ThumbX {
    background: #666666 !important;
    border-radius: 4px !important;
  }
  
  .TrackY {
    top: 0 !important;
    bottom: 0 !important;
    height: 100% !important;
    width: 4px !important;
    background: #EFEFEF !important;
  }
  
  .ScrollbarsCustom-ThumbY {
    background: #666666 !important;
    border-radius: 4px !important;
  }
`;

export const Table = styled.table`
  width: 100%;
  text-align: center;
  margin-top: 10px;
  
  thead {
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;    
    color: #000000;
    border-bottom: 1px solid #C4C4C4;
    
    tr {
      &:first-child {
        th {
          font-style: normal;
          font-weight: 500;
          font-size: 16px;
          line-height: 20px;    
          color: #000000;
          padding: 16px 14px 6px;
          min-width: 40px;
          width: auto;
        }
      }
      
      &:nth-child(2) {
        th {
          padding-bottom: 16px;
          font-weight: 500;
          font-size: 12px;
          line-height: 15px;
          text-align: center;
          color: #666666;
        }
      }
    }
  }
  
  tbody {
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    text-align: center;    
    color: #666666;
    
    tr {
      border-bottom: 1px solid #C4C4C4;
      
      td {
        padding: 16px 14px;
      }
    }
  }
  
  .name {
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
  }
`;