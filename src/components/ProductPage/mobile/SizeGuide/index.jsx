import React, { useEffect, useState } from 'react';
import Div100vh from 'react-div-100vh';
import { string, bool, func } from 'prop-types';

import Icon from "components/Icon";
import {
  Women,
  Men,
  Kids
} from './tabs'

import {
  Content,
  Header,
  IconWrapper,
  PopupWrapper,
  Title,
  Tabs,
  Tab
} from './styled';

const SizeGuide = ({ title, showSizeGuide, setShowSizeGuide }) => {
  const [tab, setTab] = useState(0);
  const tabs = ['Women', 'Men', 'Kids'];

  const renderTabs = () => {
    switch (tab) {
      case 2:
        return <Kids/>;
      case 1:
        return <Men/>;
      case 0:
      default:
        return <Women/>;
    }
  };

  useEffect(() => {
    if(showSizeGuide) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [showSizeGuide]);

  return <PopupWrapper active={showSizeGuide}>
    <Header>
      <Title>{title}</Title>

      <IconWrapper onClick={() => setShowSizeGuide(false)}>
        <Icon
          type="close"
          svgStyle={{ width: 24, height: 24, color: '#1A1A1A' }}
        />
      </IconWrapper>
    </Header>

    <Content>
      <Div100vh
        style={{
          height: '100vh',
          maxHeight: 'calc(100rvh - 70px)',
          overflowX: 'hidden',
          overflowY: 'auto'
        }}
      >
        <Tabs>
          {
            tabs.map((name, index) =>
              <Tab
                key={index}
                active={index === tab}
                onClick={() => setTab(index)}
              >
                {name}
              </Tab>
            )
          }
        </Tabs>
        {renderTabs()}
      </Div100vh>
    </Content>
  </PopupWrapper>
};

SizeGuide.defaultProps = {
  title: 'Size guide',
  showSizeGuide: false,
  setShowSizeGuide: () => {}
};

SizeGuide.propTypes = {
  title: string,
  showSizeGuide: bool,
  setShowSizeGuide: func
};

export default SizeGuide;