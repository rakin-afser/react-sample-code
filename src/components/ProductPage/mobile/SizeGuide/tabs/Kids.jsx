import React, { useState, useEffect } from 'react';
import Scrollbar from 'react-scrollbars-custom';
import {  } from 'prop-types';

import {
  SubTab,
  TabContent,
  Tabs,
  Table,
  TableContainer
} from '../styled';

const Kids = ({  }) => {
  const [tab, setTab] = useState(0);
  const [subTab, setSubTab] = useState(0);

  const tabs = ['Clothes', 'Shoes'];
  const clothesTabs = ['Babies', 'Children', 'Teen Boys', 'Teen Girls'];
  const shoesTabs = ['Infant', 'Toddler', 'Junior'];

  useEffect(() => {
    setSubTab(0);
  }, [tab]);

  const renderTabs = () => {
    switch (tab) {
      case 1:
        return shoes();
      case 0:
      default:
        return clothes();
    }
  };

  const renderClothesTabs = () => {
    switch (subTab) {
      case 3:
        return teenGirls();
      case 2:
        return teenBoys();
      case 1:
        return children();
      case 0:
      default:
        return babies();
    }
  };

  const renderShoesTabs = () => {
    switch (subTab) {
      case 2:
        return junior();
      case 1:
        return toddler();
      case 0:
      default:
        return infant();
    }
  };

  const clothes = () => {
    return <TabContent mini>
      <Tabs>
        {
          clothesTabs.map((name, index) =>
            <SubTab
              key={index}
              active={index === subTab}
              onClick={() => setSubTab(index)}
            >
              {name}
            </SubTab>
          )
        }
      </Tabs>
      {renderClothesTabs()}
    </TabContent>
  };

  const shoes = () => {
    return <TabContent mini>
      <Tabs>
        {
          shoesTabs.map((name, index) =>
            <SubTab
              key={index}
              active={index === subTab}
              onClick={() => setSubTab(index)}
            >
              {name}
            </SubTab>
          )
        }
      </Tabs>
      {renderShoesTabs()}
    </TabContent>
  };

  const infant = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>EU</th>
              <th>UK</th>
              <th>UK</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>16</td>
              <td>0.5</td>
              <td>0.5</td>
            </tr>

            <tr>
              <td>16.5</td>
              <td>1</td>
              <td>0.5</td>
            </tr>

            <tr>
              <td>17</td>
              <td>1.5</td>
              <td>1</td>
            </tr>

            <tr>
              <td>17.5</td>
              <td>2</td>
              <td>1</td>
            </tr>

            <tr>
              <td>18</td>
              <td>2.5</td>
              <td>1.5</td>
            </tr>

            <tr>
              <td>18.5</td>
              <td>3</td>
              <td>2</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const toddler = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>EU</th>
              <th>UK</th>
              <th>UK</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>19</td>
              <td>3.5</td>
              <td>2.5</td>
            </tr>

            <tr>
              <td>19.5</td>
              <td>4</td>
              <td>3</td>
            </tr>

            <tr>
              <td>20</td>
              <td>4.5</td>
              <td>3.5</td>
            </tr>

            <tr>
              <td>20.5</td>
              <td>5</td>
              <td>4</td>
            </tr>

            <tr>
              <td>21</td>
              <td>5.5</td>
              <td>4.5</td>
            </tr>

            <tr>
              <td>22</td>
              <td>6</td>
              <td>5</td>
            </tr>

            <tr>
              <td>22.5</td>
              <td>6.5</td>
              <td>5.5</td>
            </tr>

            <tr>
              <td>23</td>
              <td>7</td>
              <td>6</td>
            </tr>

            <tr>
              <td>23.5</td>
              <td>7.5</td>
              <td>6.5</td>
            </tr>

            <tr>
              <td>24</td>
              <td>8</td>
              <td>7</td>
            </tr>

            <tr>
              <td>25</td>
              <td>8.5</td>
              <td>7.5</td>
            </tr>

            <tr>
              <td>25.5</td>
              <td>9</td>
              <td>8</td>
            </tr>

            <tr>
              <td>26</td>
              <td>9.5</td>
              <td>8.5</td>
            </tr>

            <tr>
              <td>27</td>
              <td>10</td>
              <td>9</td>
            </tr>

            <tr>
              <td>27.5</td>
              <td>10.5</td>
              <td>9.5</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const junior = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>EU</th>
              <th>US</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>27,5</td>
              <td>9.5</td>
            </tr>

            <tr>
              <td>28</td>
              <td>10</td>
            </tr>

            <tr>
              <td>29</td>
              <td>10.5</td>
            </tr>

            <tr>
              <td>30</td>
              <td>11</td>
            </tr>

            <tr>
              <td>30.5</td>
              <td>11.5</td>
            </tr>

            <tr>
              <td>31</td>
              <td>12</td>
            </tr>

            <tr>
              <td>31.5</td>
              <td>12.5</td>
            </tr>

            <tr>
              <td>32</td>
              <td>13</td>
            </tr>

            <tr>
              <td>33</td>
              <td>13.5</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const babies = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>Age</th>
              <th/>
              <th>Chest</th>
              <th>Waist</th>
              <th>Hips</th>
            </tr>

            <tr>
              <th/>
              <th/>
              <th>Inches</th>
              <th>Inches</th>
              <th>Inches</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td style={{ minWidth: 150 }}><span className="name">Tiny Babe</span></td>
              <td/>
              <td style={{ minWidth: 110 }}>16-17</td>
              <td style={{ minWidth: 110 }}>16-17</td>
              <td style={{ minWidth: 110 }}>16-17</td>
            </tr>

            <tr>
              <td><span className="name">Newborn</span></td>
              <td/>
              <td>17-17.7</td>
              <td>17-17.7</td>
              <td>17-17.7</td>
            </tr>

            <tr>
              <td><span className="name">0-3m</span></td>
              <td/>
              <td>17.7-18.5</td>
              <td>17.7-18.5</td>
              <td>17.7-18.5</td>
            </tr>

            <tr>
              <td><span className="name">3-6m</span></td>
              <td/>
              <td>18.5-19.3</td>
              <td>18-19</td>
              <td>18.5-19.3</td>
            </tr>

            <tr>
              <td><span className="name">6-9m</span></td>
              <td/>
              <td>19.3-20</td>
              <td>18.5-19.3</td>
              <td>19.3-20</td>
            </tr>

            <tr>
              <td><span className="name">9-12m</span></td>
              <td/>
              <td>20-21</td>
              <td>19-19.7</td>
              <td>20-21</td>
            </tr>

            <tr>
              <td><span className="name">12-18m</span></td>
              <td/>
              <td>20.5-21.3</td>
              <td>20.5-21.3</td>
              <td>21-22</td>
            </tr>

            <tr>
              <td><span className="name">18-24m</span></td>
              <td/>
              <td>21-21.7</td>
              <td>19.7-20.5</td>
              <td>21-22</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const children = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
          <tr>
            <th>Age</th>
            <th/>
            <th>Chest</th>
            <th>Waist</th>
            <th>Hips</th>
          </tr>

          <tr>
            <th/>
            <th/>
            <th>Inches</th>
            <th>Inches</th>
            <th>Inches</th>
          </tr>
          </thead>

          <tbody>
            <tr>
              <td style={{ minWidth: 150 }}><spam className="name">2-3y</spam></td>
              <td/>
              <td style={{ minWidth: 110 }}>21.3-22</td>
              <td style={{ minWidth: 110 }}>20-21</td>
              <td style={{ minWidth: 110 }}>21.7-22.8</td>
            </tr>

            <tr>
              <td><spam className="name">3-4y</spam></td>
              <td/>
              <td>21.7-22.4</td>
              <td>20.5-21.3</td>
              <td>22.5-23.6</td>
            </tr>

            <tr>
              <td><spam className="name">4-5y</spam></td>
              <td/>
              <td>22-22.8</td>
              <td>21-21.7</td>
              <td>23.2-24.5</td>
            </tr>

            <tr>
              <td><spam className="name">5-6y</spam></td>
              <td/>
              <td>22.4-23.2</td>
              <td>21.3-22</td>
              <td>24-25.2</td>
            </tr>

            <tr>
              <td><spam className="name">6-7y</spam></td>
              <td/>
              <td>22.8-24.4</td>
              <td>21.7-22.8</td>
              <td>24.8-26.4</td>
            </tr>

            <tr>
              <td><spam className="name">7-8y</spam></td>
              <td/>
              <td>24-25.6</td>
              <td>22.5-23.2</td>
              <td>26-27.6</td>
            </tr>

            <tr>
              <td><spam className="name">8-9y</spam></td>
              <td/>
              <td>25.2-26.8</td>
              <td>22.8-24</td>
              <td>27.2-28.7</td>
            </tr>

            <tr>
              <td><spam className="name">9-10y</spam></td>
              <td/>
              <td>26.4-28</td>
              <td>23.6-24.5</td>
              <td>28.3-30</td>
            </tr>

            <tr>
              <td><spam className="name">10-11y</spam></td>
              <td/>
              <td>27.6-29</td>
              <td>24-25.2</td>
              <td>29.5-31</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const teenBoys = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
          <tr>
            <th>Age</th>
            <th/>
            <th>Chest</th>
            <th>Waist</th>
            <th>Hips</th>
          </tr>

          <tr>
            <th/>
            <th/>
            <th>Inches</th>
            <th>Inches</th>
            <th>Inches</th>
          </tr>
          </thead>

          <tbody>
            <tr>
              <td style={{ minWidth: 150 }}><spam className="name">11-12y</spam></td>
              <td/>
              <td style={{ minWidth: 110 }}>28.7-30.3</td>
              <td style={{ minWidth: 110 }}>24.8-25.6</td>
              <td style={{ minWidth: 110 }}>31-32.7</td>
            </tr>

            <tr>
              <td><spam className="name">12-13y</spam></td>
              <td/>
              <td>30-31.5</td>
              <td>25.2-26.4</td>
              <td>32.3-34.3</td>
            </tr>

            <tr>
              <td><spam className="name">13-14y</spam></td>
              <td/>
              <td>31-32.7</td>
              <td>26-26.8</td>
              <td>33.9-35.4</td>
            </tr>

            <tr>
              <td><spam className="name">14-15y</spam></td>
              <td/>
              <td>32.3-34</td>
              <td>26.4-27.6</td>
              <td>35-37</td>
            </tr>

            <tr>
              <td><spam className="name">15-16y</spam></td>
              <td/>
              <td>33.5-35</td>
              <td>27.2-28</td>
              <td>36.6-38.2</td>
            </tr>

            <tr>
              <td><spam className="name">From 16y</spam></td>
              <td/>
              <td>34.6-36.2</td>
              <td>27.6-28.7</td>
              <td>37.8-39.8</td>
            </tr>

            <tr>
              <td><spam className="name">From 17y</spam></td>
              <td/>
              <td>35.8-37.4</td>
              <td>28.3-29.5</td>
              <td>39.4-41.3</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const teenGirls = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
          <tr>
            <th>Age</th>
            <th/>
            <th>Chest</th>
            <th>Waist</th>
            <th>Hips</th>
          </tr>

          <tr>
            <th/>
            <th/>
            <th>Inches</th>
            <th>Inches</th>
            <th>Inches</th>
          </tr>
          </thead>

          <tbody>
            <tr>
              <td style={{ minWidth: 150 }}><spam className="name">11-12y</spam></td>
              <td/>
              <td style={{ minWidth: 110 }}>28.7-30.3</td>
              <td style={{ minWidth: 110 }}>24.8-25.6</td>
              <td style={{ minWidth: 110 }}>30.7-32.3</td>
            </tr>

            <tr>
              <td><spam className="name">12-13y</spam></td>
              <td/>
              <td>30-31.5</td>
              <td>26-27.2</td>
              <td>31.9-33.5</td>
            </tr>

            <tr>
              <td><spam className="name">13-14y</spam></td>
              <td/>
              <td>31-32.7</td>
              <td>26.8-28</td>
              <td>33.1-34.6</td>
            </tr>

            <tr>
              <td><spam className="name">14-15y</spam></td>
              <td/>
              <td>32.3-34</td>
              <td>27.6-28.7</td>
              <td>34.3-35.8</td>
            </tr>

            <tr>
              <td><spam className="name">15-16y</spam></td>
              <td/>
              <td>33.5-35</td>
              <td>29.1-30.7</td>
              <td>35.4-37</td>
            </tr>

            <tr>
              <td><spam className="name">From 16y</spam></td>
              <td/>
              <td>34.6-36.2</td>
              <td>29.1-30.7</td>
              <td>36.6-38.2</td>
            </tr>

            <tr>
              <td><spam className="name">From 17y</spam></td>
              <td/>
              <td>35.8-37.4</td>
              <td>30.3-31.9</td>
              <td>37.8-39.4</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  return <TabContent mini>
    <Tabs>
      {
        tabs.map((name, index) =>
          <SubTab
            key={index}
            active={index === tab}
            onClick={() => setTab(index)}
          >
            {name}
          </SubTab>
        )
      }
    </Tabs>
    {renderTabs()}
  </TabContent>
};

Kids.defaultProps = {

};

Kids.propTypes = {

};

export default Kids;