import React, { useState } from 'react';
import Scrollbar from 'react-scrollbars-custom';
import {  } from 'prop-types';

import {
  SubTab,
  TabContent,
  Tabs,
  Table,
  TableContainer
} from '../styled';

const Men = ({  }) => {
  const [tab, setTab] = useState(0);
  const tabs = ['Tops', 'Trouses', 'Jackets', 'Shorts', 'Shoes'];

  const renderTabs = () => {
    switch (tab) {
      case 4:
        return shoes();
      case 3:
        return shorts();
      case 2:
        return jackets();
      case 1:
        return trouses();
      case 0:
      default:
        return tops();
    }
  };

  const tops = () => {
    return <TableContainer>
      <Scrollbar
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>Size</th>
              <th colSpan={2}>Chest</th>
            </tr>
            <tr>
              <th/>
              <th>Inches</th>
              <th>CM</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td><span className="name">XXS</span></td>
              <td>32-34</td>
              <td>81-86</td>
            </tr>

            <tr>
              <td><span className="name">XS</span></td>
              <td>34-36</td>
              <td>86-91</td>
            </tr>

            <tr>
              <td><span className="name">S</span></td>
              <td>36-38</td>
              <td>91-96</td>
            </tr>

            <tr>
              <td><span className="name">M</span></td>
              <td>40-42</td>
              <td>96-101</td>
            </tr>

            <tr>
              <td><span className="name">L</span></td>
              <td>42-44</td>
              <td>101-106</td>
            </tr>

            <tr>
              <td><span className="name">XL</span></td>
              <td>46-48</td>
              <td>111-116</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const jackets = () => {
    return <TableContainer>
      <Scrollbar
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>Size</th>
              <th colSpan={2}>Chest</th>
            </tr>
            <tr>
              <th/>
              <th>Inches</th>
              <th>CM</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td><span className="name">XXS</span></td>
              <td>32-34</td>
              <td>81-86</td>
            </tr>

            <tr>
              <td><span className="name">XS</span></td>
              <td>34-36</td>
              <td>86-91</td>
            </tr>

            <tr>
              <td><span className="name">S</span></td>
              <td>36-38</td>
              <td>91-96</td>
            </tr>

            <tr>
              <td><span className="name">M</span></td>
              <td>40-42</td>
              <td>96-101</td>
            </tr>

            <tr>
              <td><span className="name">L</span></td>
              <td>42-44</td>
              <td>101-106</td>
            </tr>

            <tr>
              <td><span className="name">XL</span></td>
              <td>46-48</td>
              <td>111-116</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const trouses = () => {
    return <TableContainer>
      <Scrollbar
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>Size</th>
              <th colSpan={2}>Waist</th>
            </tr>
            <tr>
              <th/>
              <th>Inches</th>
              <th>CM</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td><span className="name">26”</span></td>
              <td>26</td>
              <td>66</td>
            </tr>

            <tr>
              <td><span className="name">28”</span></td>
              <td>28</td>
              <td>77</td>
            </tr>

            <tr>
              <td><span className="name">30”</span></td>
              <td>30</td>
              <td>77.5</td>
            </tr>

            <tr>
              <td><span className="name">31”</span></td>
              <td>31</td>
              <td>78.5</td>
            </tr>

            <tr>
              <td><span className="name">32”</span></td>
              <td>32</td>
              <td>81</td>
            </tr>

            <tr>
              <td><span className="name">33”</span></td>
              <td>33</td>
              <td>82.5</td>
            </tr>

            <tr>
              <td><span className="name">34”</span></td>
              <td>34</td>
              <td>86</td>
            </tr>

            <tr>
              <td><span className="name">36”</span></td>
              <td>36</td>
              <td>91</td>
            </tr>

            <tr>
              <td><span className="name">38”</span></td>
              <td>38</td>
              <td>96</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const shorts = () => {
    return <TableContainer>
      <Scrollbar
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>Size</th>
              <th colSpan={2}>Waist</th>
            </tr>
            <tr>
              <th/>
              <th>Inches</th>
              <th>CM</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td><span className="name">XXS</span></td>
              <td>26-28</td>
              <td>66-71</td>
            </tr>

            <tr>
              <td><span className="name">XS</span></td>
              <td>28-30</td>
              <td>71-76</td>
            </tr>

            <tr>
              <td><span className="name">S</span></td>
              <td>30-32</td>
              <td>76-81</td>
            </tr>

            <tr>
              <td><span className="name">M</span></td>
              <td>32-34</td>
              <td>81-86</td>
            </tr>

            <tr>
              <td><span className="name">L</span></td>
              <td>36-38</td>
              <td>91-96</td>
            </tr>

            <tr>
              <td><span className="name">XL</span></td>
              <td>38-40</td>
              <td>96-101</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const shoes = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>EU</th>
              <th>UK</th>
              <th>UK</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>39</td>
              <td>7</td>
              <td>5</td>
            </tr>

            <tr>
              <td>39.5</td>
              <td>7.5</td>
              <td>5.5</td>
            </tr>

            <tr>
              <td>40</td>
              <td>8</td>
              <td>6</td>
            </tr>

            <tr>
              <td>40.5</td>
              <td>8.5</td>
              <td>8.5</td>
            </tr>

            <tr>
              <td>41</td>
              <td>9</td>
              <td>7</td>
            </tr>

            <tr>
              <td>41.5</td>
              <td>9.5</td>
              <td>7.5</td>
            </tr>

            <tr>
              <td>42</td>
              <td>10</td>
              <td>8</td>
            </tr>

            <tr>
              <td>42.5</td>
              <td>10.5</td>
              <td>8.5</td>
            </tr>

            <tr>
              <td>43</td>
              <td>11</td>
              <td>9</td>
            </tr>

            <tr>
              <td>43.5</td>
              <td>11.5</td>
              <td>9.5</td>
            </tr>

            <tr>
              <td>44</td>
              <td>12</td>
              <td>10</td>
            </tr>

            <tr>
              <td>44.5</td>
              <td>12.5</td>
              <td>10.5</td>
            </tr>

            <tr>
              <td>45</td>
              <td>13</td>
              <td>11.5</td>
            </tr>

            <tr>
              <td>45.5</td>
              <td>13.5</td>
              <td>12</td>
            </tr>

            <tr>
              <td>46</td>
              <td>14</td>
              <td>12.5</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  return <TabContent>
    <Tabs>
      {
        tabs.map((name, index) =>
          <SubTab
            key={index}
            active={index === tab}
            onClick={() => setTab(index)}
          >
            {name}
          </SubTab>
        )
      }
    </Tabs>
    {renderTabs()}
  </TabContent>
};

Men.defaultProps = {

};

Men.propTypes = {

};

export default Men;