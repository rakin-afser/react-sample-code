import React from 'react';
import {number, func, bool, object} from 'prop-types';

import Icon from 'components/Icon';
import Popup from 'components/MobileCommentsPopup';
import {Wrapper, Counter, CounterLabel, Left, Right} from './styled';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import {onShareProduct} from 'util/heplers';

const Counters = ({
  style,
  likes,
  isLiked,
  comments,
  share,
  bookmark,
  showCommentsPopup,
  setShowCommentsPopup,
  triggerLike,
  item = {}
}) => {
  const [globalState, globalActions] = useGlobal();
  const [, setGlobalState] = useGlobal();
  const [user] = useUser();
  const {databaseId, seller, slug, name} = item;

  const formatNumbers = (num) => {
    return num.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
  };

  const onClick = () => {
    if (user?.databaseId) {
      setGlobalState.setActionsPopup(true, {productId: databaseId, seller, slug, name});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  const onLike = () => {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    if (triggerLike) triggerLike();
  };

  const onComment = () => {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    setShowCommentsPopup(true);
  };

  return (
    <Wrapper style={style}>
      <Left>
        <Counter onClick={onLike}>
          <Icon type={isLiked ? 'liked' : 'like'} />
          {likes ? <CounterLabel type={isLiked ? 'liked' : 'like'}>{formatNumbers(likes)}</CounterLabel> : null}
        </Counter>
        <Counter onClick={onComment}>
          <Icon type="message" />
          {comments ? <CounterLabel>{formatNumbers(comments)}</CounterLabel> : null}
        </Counter>
        {share && (
          <Counter
            onClick={() =>
              onShareProduct({
                title: name,
                text: 'I would like to recommend this product at testSample',
                slug
              })
            }
          >
            <Icon type="share" color="currentColor" />
          </Counter>
        )}
      </Left>
      <Right>
        {bookmark && (
          <Counter onClick={onClick}>
            {globalState.actionsPopup ? (
              <Icon width={23} height={27} type="filledBookmarkIcon" />
            ) : (
              <Icon type="bookmarkIcon" />
            )}
          </Counter>
        )}
      </Right>
      {/* <Popup showCommentsPopup={showCommentsPopup} setShowCommentsPopup={setShowCommentsPopup} /> */}
    </Wrapper>
  );
};

Counters.defaultProps = {
  style: {},
  likes: null,
  comments: null,
  share: null,
  bookmark: null,
  showCommentsPopup: false,
  setShowCommentsPopup: () => {}
};

Counters.propTypes = {
  style: object,
  likes: number,
  comments: number,
  share: number,
  bookmark: number,
  showCommentsPopup: bool,
  setShowCommentsPopup: func
};

export default Counters;
