import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 14px 16px;
  border-bottom: 1px solid #e4e4e4;
  justify-content: space-between;
`;

export const Left = styled.div`
  display: flex;
  align-items: center;
`;

export const Right = styled.div`
  display: flex;

  & > div {
    margin-left: 34px;
  }
  & > div:first-child {
    margin-left: 0;
  }
`;

export const Counter = styled.div`
  display: inline-flex;
  align-items: center;
  overflow: hidden;
  background: #f5f5f5;
  border: 0.5px solid #cccccc;
  border-radius: 22px;
  padding: 0 10px;
  display: flex;
  height: 26px;
  align-items: center;
  color: #999999;

  &:not(:first-child) {
    margin-left: 10px;
  }

  svg {
    max-width: 15px;
    max-height: 15px;
    fill: #999999;
    color: #999999;
  }
`;

export const CounterLabel = styled.span`
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  color: #999999;
  padding-left: 5.5px;
  line-height: 140%;

  ${({type}) => (type === 'liked' ? 'color: #ED484F;' : '')}
`;
