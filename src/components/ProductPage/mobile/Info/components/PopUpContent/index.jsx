import React, {useEffect, useRef} from 'react';
import Scrollbar from 'react-scrollbars-custom';
import {
  Wrapper,
  FormGroup,
  FormTitle,
  FormValues,
  FormValue,
  ColorImage,
  Color,
  ColorList,
  ColorListWrapper,
  StyledSlider
} from './styled';
import Slider from 'react-slick';
import Icon from 'components/Icon';

const PopUpContent = ({variation, variations, vars, varState, setVarState}) => {
  const sliderRef = useRef(null);
  const {slickGoTo} = sliderRef?.current || {};

  useEffect(() => {
    if (variation && slickGoTo && variations?.nodes?.length > 0) {
      const slideIndex = variations?.nodes?.findIndex(({id}) => id === variation?.id);
      slickGoTo(slideIndex);
    }
  }, [variation, slickGoTo, variations]);

  function onClick(value, label) {
    setVarState({...varState, [label]: value});
  }

  function renderPopUpContent() {
    return Object.keys(vars || {})?.map((varName) => {
      const {placeholder, options} = vars[varName];
      switch (placeholder) {
        case 'Color':
          return (
            <FormGroup key={varName}>
              <FormTitle>{placeholder}</FormTitle>
              <ColorListWrapper>
                <ColorList>
                  {options?.map((option) => (
                    <Color
                      selected={Object.values(varState)?.includes(option?.value) ? 1 : 0}
                      onClick={() => onClick(option?.value, varName)}
                      key={option?.value}
                      color={option?.label}
                      disabled={option?.disabled}
                    >
                      <Icon type="checkbox" color="#000" />
                    </Color>
                  ))}
                </ColorList>
              </ColorListWrapper>
            </FormGroup>
          );

        default:
          return (
            <FormGroup key={varName}>
              <FormTitle>{placeholder}</FormTitle>
              <FormValues>
                <Scrollbar
                  disableTracksWidthCompensation
                  style={{height: 40}}
                  trackXProps={{
                    renderer: (props) => {
                      const {elementRef, ...restProps} = props;
                      return <span {...restProps} ref={elementRef} className="TrackX" />;
                    }
                  }}
                >
                  {options?.map((option) => (
                    <FormValue
                      active={Object.values(varState)?.includes(option?.value) ? 1 : 0}
                      onClick={() => onClick(option?.value, varName)}
                      key={option?.value}
                      disabled={option?.disabled}
                    >
                      {option?.label}
                    </FormValue>
                  ))}
                </Scrollbar>
              </FormValues>
            </FormGroup>
          );
      }
    });
  }

  const withImages = variations?.nodes?.every((v) => v.image);

  return (
    <Wrapper>
      {withImages ? (
        <StyledSlider fade ref={sliderRef} infinite={false} touchMove={false}>
          {variations?.nodes?.map((v) => {
            const {id, sourceUrl, altText} = v?.image || {};
            return <ColorImage key={id} src={sourceUrl} alt={altText} />;
          })}
        </StyledSlider>
      ) : null}
      {renderPopUpContent()}
    </Wrapper>
  );
};

export default PopUpContent;
