import {primaryColor} from 'constants/colors';
import Slider from 'react-slick';
import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  max-width: 100%;
`;

export const FormGroup = styled.div`
  margin-bottom: 32px;
`;

export const StyledSlider = styled(Slider)`
  margin-bottom: 40px;
`;

export const FormTitle = styled.h3`
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 14px;
  color: #666;
  margin-bottom: 16px;

  span {
    font-weight: 500;
    font-size: 14px;
    color: #ed484f;
    margin-left: auto;
    color: ${primaryColor};
  }
`;

export const FormValues = styled.div`
  display: flex;
  margin: 0 -16px;

  .ScrollbarsCustom-Content {
    display: flex;
    padding: 0 16px !important;

    &:after {
      content: '';
      min-width: 16px;
      height: 1px;
      display: inline-flex;
    }
  }

  .TrackX {
    display: none;
  }
`;

export const FormValue = styled.div`
  height: 40px;
  border: 1px solid ${({active}) => (active ? '#000000' : '#999999')};
  padding: 0 16px;
  line-height: 38px;
  border-radius: 40px;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${({active}) => (active ? '#000000' : '#999999')};
  box-shadow: ${({active}) => (active ? 'inset 0 0 0 1px #000' : 'inset 0 0 0 1px transparent')};
  white-space: nowrap;
  cursor: pointer;
  opacity: ${({disabled}) => (disabled ? 0.2 : 1)};
  transition: border 0.1s ease-in, opacity 0.1s ease-in, box-shadow 0.3s ease-in;

  &:not(:last-child) {
    margin-right: 12px;
  }
`;

export const ColorImage = styled.img`
  height: 100%;
  max-height: 285px;
  width: 100%;
  object-fit: contain;
`;

export const ColorListWrapper = styled.div`
  overflow: auto;
`;

export const ColorList = styled.div`
  display: flex;
  margin-bottom: 16px;
`;

export const Color = styled.div`
  border: 1px solid ${({selected}) => (selected ? '#cccccc' : 'transparent')};
  border-radius: 12px;
  position: relative;
  min-width: 55px;
  height: 50px;
  transition: border-color 0.1s ease-in, opacity 0.1s ease-in;
  opacity: ${({disabled}) => (disabled ? 0.2 : 1)};
  position: relative;

  & + & {
    margin-left: 12px;
  }

  &::before {
    content: '';
    border-radius: inherit;
    position: absolute;
    top: 6px;
    left: 6px;
    right: 6px;
    bottom: 6px;
    background-color: ${({color}) => color || 'transparent'};
  }

  > i {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    opacity: ${({selected}) => (selected ? 1 : 0)};
    transition: opacity 0.3s ease-in;
  }
`;
