import React from 'react';
import {Wrapper, Image, Attributes, Attribute, Label, Value, ImagePlaceholder, ColorAttribute} from './styled';
import Icon from 'components/Icon';

const Variation = ({variations, variation, setShowVariationsPopup}) => {
  const {image, attributes} = variation || {};
  const {sourceUrl, altText} = image || {};
  const labels = variations?.nodes?.[0]?.attributes?.nodes?.map((a) => a?.label);

  if (!variation && !variations) {
    return null;
  }

  function renderAttributes() {
    return attributes?.nodes?.map(({label, value, attributeId}) =>
      label === 'Color' ? (
        <ColorAttribute key={attributeId} color={value} />
      ) : (
        <Attribute key={attributeId}>
          <Label>{label}</Label>
          <Value>{value}</Value>
        </Attribute>
      )
    );
  }

  return (
    <Wrapper onClick={() => setShowVariationsPopup(true)}>
      {sourceUrl ? <Image src={sourceUrl} alt={altText} /> : <ImagePlaceholder />}
      <Attributes>
        {attributes?.nodes?.length > 0
          ? renderAttributes()
          : labels?.map((label) => (
              <Attribute key={label}>
                <Label>{label}</Label>
                <Value>-</Value>
              </Attribute>
            ))}
      </Attributes>
      <Icon type="chevronRight" fill="#000" />
    </Wrapper>
  );
};

export default Variation;
