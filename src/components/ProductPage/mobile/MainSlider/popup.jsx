import React, {useEffect, useRef} from 'react';
import {array, number, bool, func} from 'prop-types';
import Scrollbar from 'react-scrollbars-custom';
import Slider from 'react-slick';

import Icon from 'components/Icon';

import {PopupWrapper, IconWrapper, SliderWrapper, Slide, SlideImage, SliderNav, SlideNav} from './styled';

const Popup = ({slides, index, setIndex, showPopup, setShowPopup}) => {
  const sliderPopup = useRef(null);
  useEffect(() => {
    if (showPopup) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [showPopup]);

  const settings = {
    dots: false,
    arrows: false,
    infinite: false,
    touchThreshold: 15,
    speed: 200
  };

  // // Fix slider vertical scroll on swipe
  // let firstClientX;
  // let clientX;

  // const preventTouch = (e) => {
  //   clientX = e.touches[0].clientX - firstClientX;

  //   const horizontalScroll = Math.abs(clientX) > settings.touchThreshold;
  //   if (horizontalScroll && e.cancelable) {
  //     e.preventDefault();
  //   }
  // };

  // const touchStart = (e) => {
  //   firstClientX = e.touches[0].clientX;
  // };

  // useEffect(() => {
  //   if (sliderPopup.current) {
  //     sliderPopup.current.addEventListener('touchstart', touchStart);
  //     sliderPopup.current.addEventListener('touchmove', preventTouch, {
  //       passive: false
  //     });
  //   }

  //   return () => {
  //     if (sliderPopup.current) {
  //       sliderPopup.current.removeEventListener('touchstart', touchStart);
  //       sliderPopup.current.removeEventListener('touchmove', preventTouch, {
  //         passive: false
  //       });
  //     }
  //   };
  // });
  // // end of Fix slider vertical scroll on swipe

  return (
    <PopupWrapper active={showPopup}>
      <IconWrapper onClick={() => setShowPopup(false)}>
        <Icon type="close" svgStyle={{width: 24, height: 24, color: '#1A1A1A'}} />
      </IconWrapper>

      <SliderWrapper ref={sliderPopup}>
        <Slider {...settings}>
          {slides &&
            slides.length &&
            slides.map((slide, key) => (
              <Slide key={key}>
                <SlideImage src={slide} />
              </Slide>
            ))}
        </Slider>
      </SliderWrapper>

      <SliderNav>
        <Scrollbar
          disableTracksWidthCompensation
          style={{height: 62}}
          trackXProps={{
            renderer: (props) => {
              const {elementRef, ...restProps} = props;
              return <span {...restProps} ref={elementRef} className="TrackX" />;
            }
          }}
        >
          {slides &&
            slides.length &&
            slides.map((slide, key) => (
              <SlideNav
                key={key}
                active={key === index}
                image={slide}
                onClick={() => {
                  setIndex(key);
                  sliderPopup.current.slickGoTo(key);
                }}
              />
            ))}
        </Scrollbar>
      </SliderNav>
    </PopupWrapper>
  );
};

Popup.defaultProps = {
  slides: [],
  index: 0,
  showPopup: false,
  setShowPopup: () => {}
};

Popup.propTypes = {
  slides: array,
  index: number,
  showPopup: bool,
  setShowPopup: func
};

export default Popup;
