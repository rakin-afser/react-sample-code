import React, {useCallback, useEffect, useRef, useState} from 'react';
import Popup from './popup';

import {
  Wrapper,
  SlideImage,
  RightTopComponent,
  SlideVideo,
  StyledVideo,
  IconPlay,
  ExternalVideo,
  RatioHolder,
  MediaHolder
} from './styled';
// import VideoPlayer from 'components/VideoPlayer';
import ReactPlayer from 'react-player';
import Player from 'components/Player';
import SwiperCore, {Pagination} from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
import LazyLoad from 'react-lazyload';

SwiperCore.use([Pagination]);

const MinSlider = ({
  isActive,
  slides,
  offset,
  fluidVideos,
  rightTopComponent,
  isStandStill,
  autoPlay,
  skipViewPort,
  hidePlay,
  playOnScroll,
  ratio
}) => {
  const [showPopup, setShowPopup] = useState(false);
  const [index, setIndex] = useState(0);
  const [currentSlide, setCurrentSlide] = useState(0);
  const slider = useRef(null);
  const videoRefs = useRef([]);
  const [width, height] = ratio?.split(':') || [];

  const isInViewport = useCallback(
    (el) => {
      if (skipViewPort) return true;

      if (!el) {
        return null;
      }
      const rect = el?.getBoundingClientRect();
      return rect?.bottom <= window?.innerHeight;
    },
    [skipViewPort]
  );

  const settings = {
    slidesPerView: 1,
    loop: false,
    pagination: {
      clickable: true
    },
    onSlideChange: async (slide) => {
      if (isStandStill) {
        return null;
      }
      const {realIndex, previousIndex} = slide;
      setCurrentSlide(realIndex);

      if (isInViewport(slider?.current)) {
        if (videoRefs.current[previousIndex]) {
          const video = videoRefs?.current?.[previousIndex]?.player?.player || videoRefs.current[previousIndex];
          const {pause} = video;
          pause.call(video);
        }
        if (videoRefs.current[realIndex]) {
          const video = videoRefs?.current?.[realIndex]?.player?.player || videoRefs.current[realIndex];
          const {play} = video;
          try {
            await play.call(video);
          } catch (error) {
            console.error('Video play error', error);
          }
        }
      }
      return null;
    }
  };

  function renderMedia(slide, key) {
    switch (slide?.mimeType?.split('/')?.[0]) {
      case 'image':
        return <SlideImage key={key} src={slide?.sourceUrl || slide?.mediaItemUrl} />;
      case 'video':
        return (
          <SlideVideo key={key}>
            <Player
              playOnScroll={playOnScroll}
              hidePlay={hidePlay}
              poster={slide?.poster?.sourceUrl}
              autoPlay={key === currentSlide ? autoPlay : false}
              src={slide?.mediaItemUrl}
              ref={(el) => {
                videoRefs.current[key] = el;
              }}
            />
          </SlideVideo>
        );
      case 'default-video':
        return (
          <>
            <IconPlay type="play" width="30" height="30" />
            <StyledVideo controls={false} muted src={slide?.mediaItemUrl} />{' '}
          </>
        );
      case 'external':
        return (
          <ExternalVideo key={key}>
            <ReactPlayer
              playsinline
              muted
              ref={(el) => {
                videoRefs.current[key] = el;
              }}
              className="react-player"
              url={slide?.mediaItemUrl}
              width="100%"
              height="100%"
            />
          </ExternalVideo>
        );

      default:
        return null;
    }
  }

  function renderSlider() {
    if (!slides?.length) return null;

    if (slides?.length === 1) {
      return slides?.map((slide, key) => (
        <div key={slide?.id}>
          <RatioHolder ratio={height / width} />
          <MediaHolder>
            {rightTopComponent ? <RightTopComponent>{rightTopComponent}</RightTopComponent> : null}
            <LazyLoad>{renderMedia(slide, key)}</LazyLoad>
          </MediaHolder>
        </div>
      ));
    }

    return (
      <Swiper {...settings}>
        {slides?.map((slide, key) => (
          <SwiperSlide key={slide?.id} dataIndex={key}>
            <RatioHolder ratio={height / width} />
            <MediaHolder>
              {rightTopComponent ? <RightTopComponent>{rightTopComponent}</RightTopComponent> : null}
              <LazyLoad>{renderMedia(slide, key)}</LazyLoad>
            </MediaHolder>
          </SwiperSlide>
        ))}
      </Swiper>
    );
  }

  return (
    <Wrapper pb={slides?.length > 1 ? 1 : 0} ref={slider}>
      {renderSlider()}
      <Popup slides={slides} index={index} setIndex={setIndex} showPopup={showPopup} setShowPopup={setShowPopup} />
    </Wrapper>
  );
};

MinSlider.defaultProps = {
  slides: [],
  rightTopComponent: null,
  offset: 0
};

// MinSlider.propTypes = {
//   slides: array,
//   rightTopComponent: node
// };

export default MinSlider;
