import styled, {css} from 'styled-components';
import {primaryColor} from 'constants/colors';
import media from '../../../../constants/media';
import Icon from 'components/Icon';

export const Wrapper = styled.div`
  position: relative;

  .swiper-container {
    ${({pb}) =>
      pb
        ? css`
            padding-bottom: 26px;
          `
        : null}

    &-horizontal > .swiper-pagination-bullets {
      font-size: 0;
      line-height: 1;
      bottom: 0;
      margin: 8px 0 16px 0;

      @media (max-width: ${media.mobileMax}) {
        margin: 8px 0 12px 0;
      }

      .swiper-pagination-bullet {
        @media (max-width: ${media.mobileMax}) {
          margin: 0 3px;
        }
      }
    }

    .swiper-pagination-bullet {
      transition: background-color 0.3s ease;
      background-color: #e4e4e4;
      opacity: 1;

      @media (max-width: ${media.mobileMax}) {
        width: 6px;
        height: 6px;
      }

      &-active {
        background-color: ${primaryColor};
      }
    }
  }
`;

export const SliderWrapper = styled.div`
  position: relative;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  z-index: 101;
  max-width: 100%;
  margin: auto 0;
`;

export const Slide = styled.div`
  position: relative;
`;

export const SlideImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const SlideVideo = styled.div`
  width: 100%;
  height: 100%;

  & .video-js {
    max-width: 100%;
  }

  > div {
    width: 100%;
    height: 100%;
  }

  video {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`;

export const StyledVideo = styled.video`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const ExternalVideo = styled.div`
  position: relative;
  height: 100vh;
  max-height: 444px;

  .react-player {
    position: absolute;
    top: 0;
    left: 0;
  }
`;

export const PopupWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1200;
  background: #fff;
  transition: all 0.3s ease;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  max-width: 100vw;

  opacity: ${({active}) => (active ? 1 : 0)};
  transform: translateY(${({active}) => (active ? '0px' : '-50px')});
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
`;

export const IconWrapper = styled.div`
  position: absolute;
  top: 10px;
  right: 3px;
  z-index: 110;
  cursor: pointer;
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SliderNav = styled.div`
  position: absolute;
  bottom: 34px;
  left: 12px;
  right: 12px;
  z-index: 102;

  .TrackX {
    display: none !important;
  }

  .ScrollbarsCustom-Content {
    display: flex;
    justify-content: center;
  }
`;

export const SlideNav = styled.div`
  min-width: 62px;
  width: 62px;
  height: 62px;
  border: 1px solid #cccccc;
  border-radius: 2px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  background-image: url(${({image}) => image});
  position: relative;
  margin: 0 4px;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: linear-gradient(0deg, rgba(255, 255, 255, 0.7), rgba(255, 255, 255, 0.7));
    transition: all 0.3s ease;
    opacity: 0;
  }

  ${({active}) => {
    if (!active) {
      return `
        &:before {
          opacity: 1;
        }
      `;
    }
  }}
`;

export const RightTopComponent = styled.div`
  position: absolute;
  right: 16px;
  top: 16px;
`;

export const IconPlay = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 1;
  width: 68px;
  height: 68px;
  border-radius: 50%;
  background-color: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.3);

  svg {
    margin-left: 3px;
  }
`;

export const RatioHolder = styled.div`
  padding-bottom: ${({ratio}) => (ratio ? ratio * 100 : 100)}%;
`;

export const MediaHolder = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  .lazyload-wrapper {
    height: 100%;
  }
`;
