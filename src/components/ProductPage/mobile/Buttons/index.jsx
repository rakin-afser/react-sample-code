import React, {useState} from 'react';
import {func, object} from 'prop-types';
import {useHistory} from 'react-router-dom';
import {Wrapper, Button, Error} from './styled';
import {ADD_TO_CART} from 'mutations';
import {useMutation} from '@apollo/client';
import {useUser} from 'hooks/reactiveVars';
import {getCheckoutLink} from 'util/heplers';
import parse from 'html-react-parser';

const Buttons = ({id, postId, listId, style, setShowMessage, disabled, variation, setShowVariationsPopup}) => {
  const [user] = useUser();
  const [addedToCart, setAddedToCart] = useState(false);
  let purchaseSource = postId ? {source_type: 'FEED_POST', id: +postId} : {};
  purchaseSource = listId ? {source_type: 'LIST', id: +listId} : purchaseSource;
  const [addToCart, {loading, error}] = useMutation(ADD_TO_CART, {
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart() {
            return data.addToCart.cart;
          }
        }
      });
    },
    onCompleted() {
      setTimeout(() => {
        setAddedToCart(true);
        if (setShowMessage) setShowMessage(true);
      }, 1000);
    }
  });
  const [buyNow, {loading: loadingBuy, error: errorBuy}] = useMutation(ADD_TO_CART, {
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart() {
            return data.addToCart.cart;
          }
        }
      });
    },
    onCompleted(data) {
      history.push(getCheckoutLink(data?.addToCart?.cart?.subcarts?.subcarts, user));
    }
  });

  const history = useHistory();
  function onAddToCart() {
    if (variation && setShowVariationsPopup) {
      setShowVariationsPopup(true);
      return;
    }

    if (!addedToCart) {
      addToCart({
        variables: {
          input: {
            productId: id,
            quantity: 1,
            variationId: variation?.databaseId,
            purchase_source: purchaseSource
          }
        }
      });
    }

    if (addedToCart) {
      history.push('/cart');
    }
  }

  function onBuyNow() {
    if (variation && setShowVariationsPopup) {
      setShowVariationsPopup(true);
      return;
    }
    buyNow({
      variables: {
        input: {
          productId: id,
          quantity: 1,
          variationId: variation?.databaseId,
          purchase_source: purchaseSource
        }
      }
    });
  }

  function renderError() {
    if (!error && !errorBuy) {
      return null;
    }

    if (error) return <Error>{parse(error?.message || '')}</Error>;

    if (errorBuy) return <Error>{parse(errorBuy?.message || '')}</Error>;

    return null;
  }

  return (
    <>
      {renderError()}
      <Wrapper style={style}>
        <Button kind="pr-bare" disabled={disabled} onClick={onAddToCart} loading={loading}>
          {addedToCart ? 'Go to Cart' : 'Add to Cart'}
        </Button>
        <Button kind="primary" disabled={disabled} onClick={onBuyNow} type="primary" loading={loadingBuy}>
          Buy Now
        </Button>
      </Wrapper>
    </>
  );
};

Buttons.defaultProps = {
  style: {},
  setShowMessage: () => {}
};

Buttons.propTypes = {
  style: object,
  setShowMessage: func
};

export default Buttons;
