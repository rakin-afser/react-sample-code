import styled from 'styled-components';
import {primaryColor} from 'constants/colors';
import Btn from 'components/Btn';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin: 40px -5px 0;
`;

export const Button = styled(Btn)`
  margin: 0 5px;
  width: 100%;
`;

export const PopupWrapper = styled.div`
  position: fixed;
  top: ${({active}) => (active ? '0px' : '-64px')};
  left: 0;
  right: 0;
  height: 64px;
  background: #65c97a;
  display: flex;
  align-items: center;
  padding: 13px 26px;
  transition: all 0.3s ease;
`;

export const Error = styled.p`
  color: ${primaryColor};
  margin-top: 10px;
  margin-bottom: 10px;
`;
