import styled from 'styled-components/macro';

export const CounterWrapper = styled.div`
  position: absolute;
  top: ${({top}) => top};
  right: ${({right}) => right};
  bottom: ${({bottom}) => bottom};
  left: ${({left}) => left};
  background: #ffffff;
  mix-blend-mode: normal;
  opacity: 0.9;
  box-shadow: 1.15385px 1.15385px 2.30769px rgba(0, 0, 0, 0.16);
  border-radius: 10.5px;
  padding: 4px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 9;
  line-height: 1;

  & svg {
    margin-right: ${({withoutCount}) => (withoutCount ? '0' : '4')}px;
  }

  & span {
    margin-top: 2px;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    letter-spacing: -0.016em;
    color: #000000;
  }
`;
