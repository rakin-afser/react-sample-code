import React from 'react';

import {CounterWrapper} from './styled';
import Icon from 'components/Icon';

const ProductsCounter = ({
  count,
  top = '16px',
  right = 'unset',
  bottom = 'unset',
  left = '11px',
  withoutCount = false
}) => {
  if (!count) return null;
  return (
    <CounterWrapper withoutCount={withoutCount} top={top} right={right} bottom={bottom} left={left}>
      <Icon type="bag" />
      {!withoutCount && <span>{count}</span>}
    </CounterWrapper>
  );
};

export default ProductsCounter;
