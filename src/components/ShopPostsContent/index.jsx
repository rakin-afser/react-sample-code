import React from 'react';
import data from './data';
import Post from 'components/Post';
import WithScroll from 'components/WithScroll';
import CardNewArrival from 'components/CardNewArrival';
import {SliderContainer} from './styled';

const ShopPostsContent = ({isUserLogeIn}) => {
  return (
    <>
      {data.map((item) => {
        if (item.type === 'post') {
          return <Post key={item.id} data={item} isUserLogeIn={isUserLogeIn} />;
        }
        if (item.type === 'slider') {
          return (
            <SliderContainer key={item.id}>
              <WithScroll
                seeMoreCounter={item.seeMoreCounter}
                marginTop={20}
                title={item.title}
                withSeeMore
                height={255}
              >
                {item.products.map((product, index) => (
                  <CardNewArrival key={index} {...product} />
                ))}
              </WithScroll>
            </SliderContainer>
          );
        }
      })}
    </>
  );
};

export default ShopPostsContent;
