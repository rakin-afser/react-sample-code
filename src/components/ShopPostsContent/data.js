import video1 from 'videos/videoplayback.mp4';
import video2 from 'videos/videoplayback_2.mp4';
import storeAvatar from 'images/avatars/chanel-avatar.png';
import post1 from 'images/posts/post1.jpg';
import post2 from 'images/posts/post2.jpg';
import post3 from 'images/posts/post3.jpg';
import post4 from 'images/posts/post4.jpg';
import product from 'images/products/product24.jpg';
import {v1} from 'uuid';
import {featuredProducts} from 'constants/staticData';

export default [
  {
    type: 'post',
    id: v1(),
    store: {
      name: 'Chanel Beauty',
      avatar: storeAvatar
    },
    date: '10m ago',
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [post1, post1, post1],
    productsCount: 1,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 252,
    comments: 23
  },
  {
    type: 'post',
    id: v1(),
    store: {
      name: 'Chanel Beauty',
      avatar: storeAvatar
    },
    date: '2m ago',
    videos: [video1, video2],
    productsCount: 3,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Chanel Lipstick',
        image: product,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Chanel Lipstick',
        image: product,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 343,
    comments: 65
  },
  {
    type: 'post',
    id: v1(),
    store: {
      name: 'Chanel Beauty',
      avatar: storeAvatar
    },
    description: {
      title: 'New colors - new life!',
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other. '
    },
    date: '5m ago',
    tags: ['#Cream', '#canvas', '#Big', '#tags'],
    posters: [post2, post2, post2],
    products: [
      {
        name: 'Chanel Lipstick',
        image: product,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 635,
    comments: 324
  },
  {
    type: 'slider',
    id: v1(),
    title: 'Featured Products',
    seeMoreCounter: '',
    products: [...featuredProducts]
  },
  {
    type: 'post',
    id: v1(),
    store: {
      name: 'Chanel Beauty',
      avatar: storeAvatar
    },
    date: '2m ago',
    posters: [post3, post3, post3],
    productsCount: 1,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product,
        delivery: 'Free Shipping',
        sale: null,
        price: 490
      }
    ],
    likes: 3102,
    comments: 5
  },
  {
    type: 'slider',
    id: v1(),
    title: 'New Arrivals',
    seeMoreCounter: '',
    products: [...featuredProducts]
  },
  {
    type: 'post',
    id: v1(),
    store: {
      name: 'Chanel Beauty',
      avatar: storeAvatar
    },
    date: '2m ago',
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [post4, post4, post4],
    productsCount: 1,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product,
        delivery: 'Free Shipping',
        sale: null,
        price: 490
      }
    ],
    likes: 3102,
    comments: 5
  },
  {
    type: 'slider',
    id: v1(),
    title: 'Popular Products',
    products: [...featuredProducts]
  }
];
