import React from 'react';
import PropTypes from 'prop-types';
import {Modal} from 'antd';

import Icon from 'components/Icon';
import {CloseContainer, TitleWrapper, Title, Content, ButtonsBlock, CancelButton, NextButton} from './styled';

const ActionAlert = ({content, headerTitle, okBtnName, cancelBtnName, open, onClose, onSubmit}) => {
  const onCancel = () => {
    onClose(false);
  };

  const onModalSubmit = () => {
    onSubmit();
    onClose(false);
  };

  return (
    <Modal
      wrapClassName="leave-feedback-modal"
      visible={open}
      onCancel={onCancel}
      width={425}
      footer={null}
      destroyOnClose
      bodyStyle={{
        padding: '13px 0 16px',
        minHeight: '124px'
      }}
      closeIcon={
        <CloseContainer
          style={{
            display: 'none',
            top: '12px',
            position: 'relative',
            right: '-21px'
          }}
        >
          <Icon color="#000" type="close" />
        </CloseContainer>
      }
    >
      {headerTitle && (
        <TitleWrapper>
          <Title>{headerTitle}</Title>
        </TitleWrapper>
      )}

      <Content>{content}</Content>

      <ButtonsBlock>
        <CancelButton onClick={onCancel}>{cancelBtnName}</CancelButton>
        <NextButton onClick={onModalSubmit}>
          <span>{okBtnName}</span>
        </NextButton>
      </ButtonsBlock>
    </Modal>
  );
};

ActionAlert.defaultProps = {
  headerTitle: null,
  content: null
};

ActionAlert.propTypes = {
  headerTitle: PropTypes.string,
  content: PropTypes.node,
  okBtnName: PropTypes.string.isRequired,
  cancelBtnName: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired
};

export default ActionAlert;
