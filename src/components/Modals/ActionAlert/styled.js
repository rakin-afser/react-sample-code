import styled from 'styled-components/macro';

export const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Title = styled.h3`
  padding: 6px 0 25px 25px;
  margin-bottom: 0;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const Content = styled.div`
  padding: 13px 37px 0;
  font-size: 14px;
  letter-spacing: -0.016em;
  line-height: 140%;
  color: #666666;
`;

export const ButtonsBlock = styled.div`
  margin-top: 50px;
  padding-right: 12px;
  background-color: white;
  text-align: right;
  right: 24px;
  left: 24px;
  bottom: 24px;
`;

export const NextButton = styled.button`
  padding-left: 16px;
  padding-right: 16px;
  background: #ed484f;
  font-weight: 500;
  font-size: 12px;
  color: #fff;
  height: 28px;
  border: 1px solid #ed484f;
  border-radius: 24px;
  cursor: pointer;
  transition: ease 0.6s;

  i {
    vertical-align: middle;
    margin-left: 5px;

    svg,
    svg path {
      fill: ${({isNotAvailable}) => (isNotAvailable ? '#ccc' : '#000')};
    }
  }

  &:hover {
    color: #000;
    border-color: #000;
    background-color: #fff;
  }
`;

export const CloseContainer = styled.div`
  padding-right: 13px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 28px;
  height: 28px;
  transition: ease 0.6s;

  svg {
    transition: ease 0.6s;
  }

  &:hover {
    background: #f2f2f2;
    border-radius: 7px;

    svg,
    svg path {
      fill: #000;
    }
  }
`;

export const CancelButton = styled.button`
  font-family: 'Helvetica', sans-serif;
  font-weight: normal;
  font-size: 12px;
  height: 23px;
  color: #656565;
  border: none;
  background: none;
  margin-right: ${({isBack}) => (isBack ? '13px' : '32px')};
  cursor: pointer;
  transition: ease 0.6s;

  &:hover {
    color: #ed484f;
  }
`;
