import styled from 'styled-components';
import {grayTextColor} from 'constants/colors';
import {mainFont, secondaryFont} from 'components/Header/constants';

export const Wrapper = styled.div`
  padding: 3px 0 0;
  display: flex;
`;

export const Col = styled.div`
  font-family: ${secondaryFont};
  font-size: 12px;
  color: ${grayTextColor};

  &:first-child {
    margin-right: 42px;
  }

  span {
    font-weight: 500;
    color: #000;
  }
`;

export const Title = styled.div`
  margin-bottom: 5px;
  font-family: ${mainFont};
  font-weight: 500;
  font-size: 14px;
  color: #000000;
`;

export const Name = styled.div`
  margin-bottom: 3px;
  font-family: ${mainFont};
  font-weight: bold;
  font-size: 12px;
  color: ${grayTextColor};
`;

export const Txt = styled.div`
  margin-bottom: 3px;

  &:last-child {
    margin-bottom: 0;
  }
`;
