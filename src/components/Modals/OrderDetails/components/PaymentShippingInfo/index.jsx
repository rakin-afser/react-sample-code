import React from 'react';
import PropTypes from 'prop-types';

import {Wrapper, Col, Title, Name, Txt} from './styled';

const PaymentShippingInfo = ({payment, shipping}) => {
  return (
    <Wrapper>
      <Col>
        <Title>Shipping Address</Title>
        <Name>{shipping.name}</Name>
        <Txt>{shipping.address}</Txt>
        <Txt>{shipping.address2}</Txt>
        <Txt>{shipping.address3}</Txt>
      </Col>
      <Col>
        <Title>Payment Method</Title>
        <Txt>
          {payment.cardType}
          <span> {payment.cardOwner}</span>,
        </Txt>
        <Txt>
          <span>Expires: {payment.expires}</span>
        </Txt>
        <Txt>{payment.received}</Txt>
        <Txt>{payment.paypal}</Txt>
      </Col>
    </Wrapper>
  );
};

PaymentShippingInfo.propTypes = {
  payment: PropTypes.shape().isRequired,
  shipping: PropTypes.shape().isRequired
};

export default PaymentShippingInfo;
