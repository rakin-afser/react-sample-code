import React from 'react';
import ClampLines from 'react-clamp-lines';
import parse from 'html-react-parser';

import {Wrapper, Image, Title, PriceRow, NewPrice, OldPrice} from './styled';

const Card = ({img, title, newPrice, currencySymbol}) => {
  return (
    <Wrapper>
      <Image src={img} alt={title} />
      <Title>
        <ClampLines text={title} lines={1} buttons={false} />
      </Title>
      <PriceRow>
        <span>{currencySymbol && parse(currencySymbol.toString())}</span>
        <NewPrice>{newPrice}</NewPrice>
        <OldPrice>
          {currencySymbol && parse(currencySymbol.toString())} <span>'need old price'</span>
        </OldPrice>
      </PriceRow>
    </Wrapper>
  );
};

export default Card;
