import styled from 'styled-components/macro';
import {secondaryFont} from 'components/Header/constants';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  margin-left: auto;
  margin-right: auto;
  padding: 8px 0 9px;
  width: 120px;
  height: 200px;
  background: #ffffff;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.13);
  border-radius: 2px;
`;

export const Image = styled.img`
  padding-left: 15px;
  padding-right: 15px;
  margin-bottom: 5px;
  border-radius: 4px;
  overflow: hidden;
  object-fit: cover;
`;

export const Title = styled.h3`
  padding-left: 7px;
  padding-right: 5px;
  margin-bottom: 3px;
  font-family: ${secondaryFont}, sans-serif;
  font-size: 12px;
  color: #000000;
`;

export const PriceRow = styled.div`
  padding-left: 7px;
  padding-right: 5px;
  display: flex;

  span {
    font-weight: 500;
    font-size: 10px;
    letter-spacing: 0.06em;
    color: #999999;
  }
`;

export const NewPrice = styled.div`
  margin-right: 7px;
  font-family: ${secondaryFont}, sans-serif;
  font-size: 12px;
  line-height: 14px;
  color: ${primaryColor};
`;

export const OldPrice = styled.div`
  margin-top: 1px;
  text-transform: uppercase;
  font-family: ${secondaryFont};
  font-size: 8px;
  color: #a7a7a7;
  font-weight: normal;

  span {
    margin-left: 1px;
    font-family: ${secondaryFont};
    font-size: 8px;
    color: #a7a7a7;
    font-weight: normal;
  }
`;
