import styled from 'styled-components/macro';
import WithSlider from 'components/WithSlider';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div``;

export const WithSliderStyled = styled(WithSlider)`
  & .slick-next,
  & .slick-prev {
    top: calc(50% - 11px);

    &::before {
      width: 12px !important;
      height: 14px !important;
    }
  }

  & .slick-next {
    right: -24px;
  }

  & .slick-prev {
    left: -16px;
  }

  & .slick-slide {
    padding-top: 3px;

    img {
      width: 100%;
      height: 120px;
      object-fit: cover;
    }
  }

  & .slick-dots {
    bottom: 7px !important;

    li {
      width: 9px;

      button {
        &::after {
          width: 4px;
          height: 4px;
        }
      }

      &.slick-active {
        button {
          &::after {
            width: 4px;
            height: 4px;
            background: ${primaryColor};
          }
        }
      }
    }
  }
`;
