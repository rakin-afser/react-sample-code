import React from 'react';
import PropTypes from 'prop-types';
import Card from 'components/Modals/OrderDetails/components/Card';
import {Wrapper, WithSliderStyled} from './styled';

const Slider = ({items}) => {
  return (
    <Wrapper>
      <WithSliderStyled
        slidesToShow={items.length < 3 ? items.length : 3}
        withHeader={false}
        marginTop={0}
        padding="21px 0 7px 0"
        slidesToScroll={items.length < 3 ? items.length : 3}
      >
        {items.map((data, idx) => (
          <Card
            key={idx}
            img={data?.product?.image?.sourceUrl}
            title={data?.product?.name}
            newPrice={data?.total}
            currencySymbol={data?.product?.currencySymbol}
          />
        ))}
      </WithSliderStyled>
    </Wrapper>
  );
};

Slider.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape()).isRequired
};

export default Slider;
