import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import {Wrapper, Left, Right, Date, Seller, Price, Txt} from './styled';

const OrderInfo = ({data}) => {
  return (
    <Wrapper>
      <Left>
        <Txt>Order Placed:</Txt>
        <Txt>Sold by:</Txt>
        <Txt>Total:</Txt>
        <Txt>GrandTotal:</Txt>
      </Left>
      <Right>
        <Date>{moment(data?.date).format('D MMM YYYY')}</Date>
        <Seller>{data.storeName}</Seller>
        <Price>{data?.total}</Price>
        <Price>{data?.total}</Price>
      </Right>
    </Wrapper>
  );
};

OrderInfo.propTypes = {
  data: PropTypes.shape().isRequired
};

export default OrderInfo;
