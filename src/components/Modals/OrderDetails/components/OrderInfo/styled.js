import styled from 'styled-components';
import {blue, grayTextColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding-top: 11px;
  display: flex;
  justify-content: space-between;
`;

export const Left = styled.div`
  font-size: 14px;
  color: ${grayTextColor};
`;

export const Right = styled.div`
  text-align: right;
  font-size: 14px;
  color: ${grayTextColor};
`;

export const Date = styled.div`
  margin-bottom: 9px;
  color: #000;
`;

export const Seller = styled.div`
  margin-bottom: 9px;
  font-weight: bold;
  font-size: 14px;
  color: ${blue};
`;

export const Price = styled.div`
  margin-bottom: 9px;
  font-weight: 500;
  font-size: 16px;
  color: #000;

  sub {
    margin-right: 4px;
    bottom: 1px;
    font-size: 10px;
    font-weight: 500;
  }
`;

export const Txt = styled.div`
  margin-bottom: 11px;
`;
