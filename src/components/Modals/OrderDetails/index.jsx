import React from 'react';
import {Modal} from 'antd';
import PropTypes from 'prop-types';
import useGlobal from 'store';

import {CloseContainer, ModalBody} from 'components/CreatePost/styled';
import Slider from 'components/Modals/OrderDetails/components/Slider';
import PaymentShippingInfo from 'components/Modals/OrderDetails/components/PaymentShippingInfo';
import OrderInfo from 'components/Modals/OrderDetails/components/OrderInfo';
import Icon from 'components/Icon';
import exampleData from './exampleData';
import {TitleWrapper, Title, Hr} from './styled';

const OrderDetails = ({orderDetailsModal, setOrderDetailsModal}) => {
  const [globalStore] = useGlobal();
  const {payment} = exampleData;

  let orderInfo;
  let items;
  let shippingInfo;

  if (globalStore?.orderDetails?.data) {
    const {date, total, stores, lineItems, shipping} = globalStore?.orderDetails?.data;

    orderInfo = {
      date,
      total,
      storeName: stores?.nodes?.[0]?.name
    };

    shippingInfo = {
      name: `${shipping?.firstName || ''} ${shipping?.lastName || ''}`,
      address: `${shipping?.postcode || ''} ${shipping?.address1 || ''}`,
      address2: `${shipping?.address2 || ''}`,
      address3: `${shipping?.city || ''} ${shipping?.country || ''}`
    };

    items = lineItems?.nodes;
  }

  const onCancel = () => {
    setOrderDetailsModal(false);
  };

  return (
    <Modal
      wrapClassName="leave-feedback-modal"
      visible={orderDetailsModal}
      onCancel={onCancel}
      width={425}
      footer={null}
      destroyOnClose
      bodyStyle={{
        padding: '13px 0 16px',
        minHeight: '543px'
      }}
      closeIcon={
        <CloseContainer
          style={{
            top: '12px',
            position: 'relative',
            right: '-21px'
          }}
        >
          <Icon color="#000" type="close" />
        </CloseContainer>
      }
    >
      <TitleWrapper>
        <Title>Order Details</Title>
      </TitleWrapper>

      <ModalBody withPadding>
        <Slider items={items} />
        <Hr />
        <PaymentShippingInfo payment={payment} shipping={shippingInfo} />
        <Hr />
        <OrderInfo data={orderInfo} />
      </ModalBody>
    </Modal>
  );
};

OrderDetails.propTypes = {
  orderDetailsModal: PropTypes.bool.isRequired,
  setOrderDetailsModal: PropTypes.func.isRequired
};

export default OrderDetails;
