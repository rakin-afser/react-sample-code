import styled from 'styled-components';
import {secondaryFont} from 'components/Header/constants';
import {headerShadowColor} from 'constants/colors';

export const TitleWrapper = styled.div`
  padding-bottom: 14px;
  display: flex;
  align-items: center;
  border-bottom: 1px solid #e4e4e4;
`;

export const Title = styled.h3`
  padding-left: 25px;
  margin-bottom: 0;
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 18px;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const Hr = styled.hr`
  margin-bottom: 14px;
  margin-left: -24px;
  margin-right: -24px;
  height: 1px;
  background-color: ${headerShadowColor};
  border: none;
`;
