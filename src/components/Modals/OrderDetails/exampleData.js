import img1 from './img/Img1.jpg';
import img2 from './img/Img2.jpg';
import img3 from './img/Img3.jpg';

const exampleData = {
  items: [
    {id: 1, img: img1, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 2, img: img2, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 3, img: img3, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 4, img: img1, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 5, img: img2, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 6, img: img3, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 7, img: img1, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 8, img: img2, title: 'New Stunna High', newPrice: 198, price: 84.0},
    {id: 9, img: img3, title: 'New Stunna High', newPrice: 198, price: 84.0}
  ],

  shipping: {
    name: 'testUser Thomas',
    address: '1821  15th Avenue',
    address2: 'Grand Island 8 - 178',
    address3: 'Berlin, Germany'
  },

  payment: {
    cardType: 'Visa',
    cardOwner: 'Danila Puoytrbrayn',
    expires: '02/2020',
    received: 'Received Cash on delivery',
    paypal: 'Paypal Transaction number'
  },

  order: {
    orderPlaced: 'Mon, 24.09.19',
    soldBy: 'Best Seller',
    total: '132.000',
    grandTotal: '132.000'
  }
};

export default exampleData;
