import styled from 'styled-components';
import {headerShadowColor, midCoinsColor} from 'constants/colors';

export const ModalTitleStyled = styled.div`
  font-family: 'SF Pro Display', sans-serif;
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 0.019em;

  span {
    color: ${midCoinsColor};
  }
`;

export const ProgressBarWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 1px;
  height: 5px;
  background-color: #efefef;
  width: calc(100% - 2px);
  transition: ease 0.6s;
`;

export const ProgressBar = styled(ProgressBarWrapper)`
  left: 0;
  background-color: #ed484f;
  width: ${({step}) => `${step * 50}%`};
`;

export const Hr = styled.hr`
  margin-bottom: 14px;
  margin-left: -24px;
  margin-right: -24px;
  height: 1px;
  background-color: ${headerShadowColor};
  border: none;
`;
