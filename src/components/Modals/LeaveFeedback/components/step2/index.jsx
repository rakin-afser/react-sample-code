import React from 'react';
import {Input} from 'antd';

import FadeOnMount from 'components/Transitions';
import {Title, Row, Txt, TextAreaWrapper} from './styled';
import AttachBlock from 'components/CreatePost/components/AttachBlock';

const Step2 = ({onChange, uploads, setUploads}) => {
  const addImages = (value) => {
    setUploads((oldArray) => [...oldArray, value]);
  };

  return (
    <FadeOnMount>
      <Title>Comments</Title>
      <TextAreaWrapper>
        <Input.TextArea onChange={onChange} maxLength={400} autoSize={{minRows: 5, maxRows: 5}} />
      </TextAreaWrapper>
      <Txt>You can add up to 5 photos or videos. This feedback will be shared in feed.</Txt>
      <Row>
        <AttachBlock uploads={uploads} setUploads={setUploads} addImages={addImages} noYoutubeLink small noPreview />
      </Row>
    </FadeOnMount>
  );
};

export default Step2;
