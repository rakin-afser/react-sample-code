import styled from 'styled-components';

export const Title = styled.div`
  margin-top: 15px;
  margin-bottom: 8px;
  font-weight: bold;
  font-size: 14px;
  color: #000;
`;

export const Txt = styled.div`
  margin-bottom: 20px;
  font-family: 'SF Pro Display', sans-serif;
  font-size: 12px;
  line-height: 132%;
  color: #000105;
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
`;

export const TextAreaWrapper = styled.div`
  margin-bottom: 16px;
`;
