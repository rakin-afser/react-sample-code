import React from 'react';
import useGlobal from 'store';
import FadeOnMount from 'components/Transitions';
import PropTypes from 'prop-types';
import {Wrapper, TitleWrapper, Title, Txt, Button} from './styled';
import {ReactComponent as CheckIcon} from './img/checkIcon.svg';
import {ReactComponent as RightIcon} from './img/rightArrow.svg';
import {ReactComponent as StorageIcon} from './img/storageIcon.svg';
import Loader from 'components/Loader';

const Step3 = ({setCurrentStep, loading, midcoins, shopcoins}) => {
  const [, globalActions] = useGlobal();

  const onModalCancel = () => {
    globalActions.setLeaveFeedback(false);
    setCurrentStep(0);
  };

  return (
    <FadeOnMount>
      <Wrapper>
        {loading ? (
          <Loader wrapperHeight="168px" wrapperWidth="100%" />
        ) : (
          <>
            <TitleWrapper>
              <CheckIcon />
              <Title>Thank you for your feedback!</Title>
            </TitleWrapper>
            <Txt>
              You have just unlocked {midcoins ? `${midcoins} Mid-Coins` : ''} {midcoins && shopcoins ? ' and ' : ''}
              {midcoins ? `${shopcoins} Shop-Coins` : ''}
              <StorageIcon />
            </Txt>
            <Button onClick={onModalCancel}>
              Continue Shopping <RightIcon />
            </Button>
          </>
        )}
      </Wrapper>
    </FadeOnMount>
  );
};

Step3.propTypes = {
  setCurrentStep: PropTypes.func.isRequired
};

export default Step3;
