import styled from 'styled-components';
import {mainBlackColor, midCoinsColor, primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding: 11px;
  text-align: center;
`;

export const TitleWrapper = styled.div`
  margin-bottom: 18px;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    margin-right: 6px;
    position: relative;
    left: -14px;
  }
`;

export const Title = styled.div`
  font-size: 16px;
  color: ${mainBlackColor};
`;

export const Txt = styled.p`
  margin-bottom: 19px;
  font-size: 14px;
  color: ${midCoinsColor};

  svg {
    position: relative;
    top: 2px;
  }
`;

export const Button = styled.button`
  margin-left: auto;
  margin-right: auto;
  display: flex;
  align-items: center;
  padding: 3px 26px;
  font-size: 14px;
  color: ${primaryColor};
  border: 1px solid ${primaryColor};
  border-radius: 24px;
  transition: ease 0.6s;

  svg {
    position: relative;
    right: -12px;

    path {
      transition: ease 0.6s;
    }
  }

  &:hover {
    background-color: ${primaryColor};
    color: #fff;

    svg {
      path {
        fill: #fff;
      }
    }
  }
`;
