import styled from 'styled-components';
import {headerShadowColor} from 'constants/colors';

export const Top = styled.div`
  padding: 15px 14px 4px 15px;
`;

export const Hr = styled.hr`
  margin-left: -24px;
  margin-right: -24px;
  height: 1px;
  background-color: ${headerShadowColor};
  border: none;
`;
