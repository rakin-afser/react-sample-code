import React from 'react';
import Rate from 'components/Rate';
import ProductInfoShort from 'components/ProductInfoShort';
import exampleData from 'components/Modals/exampleData';
import FadeOnMount from 'components/Transitions';
import {Hr, Top} from 'components/Modals/LeaveFeedback/components/step1/styled';

const Step1 = ({data, onRatingChange}) => {
  return (
    <FadeOnMount>
      <Top>
        <ProductInfoShort data={data} />
      </Top>
      <Hr />
      <Rate
        data={{
          item1: 5,
          item2: 5,
          item3: 5
        }}
        onChange={onRatingChange}
      />
    </FadeOnMount>
  );
};

export default Step1;
