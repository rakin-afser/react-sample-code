import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  ArrowBack,
  ButtonsBlock,
  CancelButton,
  CloseContainer,
  Header,
  LeftBlock,
  ModalBody,
  NextButton,
  SkipButton,
  Title
} from 'components/CreatePost/styled';
import Icon from 'components/Icon';
import {useMutation, gql} from '@apollo/client';
import {Modal} from 'antd';
import arrowBack from 'components/CreatePost/images/arrow_left.png';
import {ModalTitleStyled, ProgressBar, ProgressBarWrapper, Hr} from 'components/Modals/LeaveFeedback/styled';
import Step1 from 'components/Modals/LeaveFeedback/components/step1';
import Step2 from 'components/Modals/LeaveFeedback/components/step2';
import Step3 from 'components/Modals/LeaveFeedback/components/step3';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import axiosClient from 'axiosClient';
import axios from 'axios';
import {useCreatePostMutation} from 'hooks/mutations';
import {WRITE_REVIEW} from 'containers/OrdersPage/api/queries';

//* Leave Feedback & Unlock MidCoins *//
const LeaveFeedback = ({displaying}) => {
  const [currentStep, setCurrentStep] = useState(0);
  const [globalState, globalActions] = useGlobal();
  const [ratingValue, setRatingValue] = useState(0);
  const [uploads, setUploads] = useState([]);
  const [generalOpinion, setGeneralOpinion] = useState('Positive');
  const [inputValue, setInputValue] = useState('');
  const {data} = globalState.leaveFeedback;
  const [user] = useUser();
  const userId = user?.databaseId.toString();
  const [addReviewLoading, setAddReviewLoading] = useState(false);

  const [writeReview, {data: writeReviewData, loading, error}] = useMutation(WRITE_REVIEW, {
    onCompleted() {
      setAddReviewLoading(false);
    },
    onError() {
      // TODO: display error
      setAddReviewLoading(false);
    }
  });
  const [post] = useCreatePostMutation();

  const onCancel = () => {
    globalActions.setLeaveFeedback(false);
  };

  const onWriteReview = async () => {
    const filesToUpload = uploads?.map((upload) => {
      const formData = new FormData();
      formData.append('file', upload?.file);
      return axiosClient.post('/', formData);
    });
    try {
      setAddReviewLoading(true);
      const res = await axios.all(filesToUpload);
      const galleryId = res?.map((item) => item.data.id);

      writeReview({
        variables: {
          input: {
            rating: ratingValue,
            author: userId,
            content: inputValue,
            general_opinion: generalOpinion,
            commentOn: data?.databaseId,
            attachmentIds: galleryId,
            store_id: +data?.storeId,
            order_id: +data?.orderId
          }
        }
        // update(cache, {data}) {
        //   cache.modify({
        //     fields: {
        //       products() {
        //         return data;
        //       }
        //     }
        //   });
        // }
      });

      post({
        variables: {
          input: {
            status: 'PUBLISH',
            title: '',
            content: inputValue,
            isFeedbackPost: true,
            rating: ratingValue,
            relatedProducts: [data?.databaseId],
            galleryId
          }
        }
      });
    } catch (err) {
      setAddReviewLoading(false);
      console.log(`error`, err);
    }
  };

  const onChange = (e) => {
    setInputValue(e.target.value);
  };

  const onSubmit = () => {
    setCurrentStep(currentStep + 1);

    if (currentStep === 1) {
      onWriteReview();
    }
  };

  const onRatingChange = (rating) => {
    setRatingValue(rating?.total || 0);
    setGeneralOpinion(rating?.generalOpinion || 'Positive');
  };

  const showScreen = [
    {component: <Step1 onRatingChange={onRatingChange} data={data} />},
    {component: <Step2 uploads={uploads} setUploads={setUploads} onChange={onChange} />},
    {
      component: (
        <Step3
          loading={addReviewLoading}
          midcoins={data?.midcoins}
          shopcoins={data?.shopcoins}
          setCurrentStep={setCurrentStep}
        />
      )
    }
  ];

  const header = (
    <Header style={{paddingLeft: 12, paddingTop: 3}}>
      <ProgressBarWrapper>
        <ProgressBar step={currentStep + 1} isFinalStep={currentStep === 2} />
      </ProgressBarWrapper>

      <LeftBlock>
        {currentStep ? (
          <ArrowBack>
            <img
              style={{marginRight: 24}}
              src={arrowBack}
              alt="arrow"
              onClick={() => setCurrentStep(currentStep - 1)}
            />
          </ArrowBack>
        ) : null}
        <Title>
          <ModalTitleStyled style={{paddingLeft: currentStep === 0 && '11px'}}>
            Leave Feedback & Unlock <span>MidCoins</span>
          </ModalTitleStyled>
        </Title>
      </LeftBlock>
      {currentStep === 2 ? (
        <SkipButton onClick={onSubmit}>
          <span>Skip</span>
          <Icon type="arrow" width="8px" height="10px" color="#ED484F" />
        </SkipButton>
      ) : null}
    </Header>
  );

  const buttons = (
    <ButtonsBlock style={{borderTop: 'none'}}>
      <Hr />
      <CancelButton onClick={onCancel}>Cancel</CancelButton>
      <NextButton disabled={currentStep === 1 && !inputValue} onClick={onSubmit}>
        <span>{currentStep === 0 ? 'Next' : 'Submit'}</span>{' '}
        {currentStep === 0 ? <Icon type="arrow" width="8px" height="10px" color="#ccc" /> : null}
      </NextButton>
    </ButtonsBlock>
  );
  //comment

  return (
    <Modal
      wrapClassName="leave-feedback-modal"
      visible={displaying}
      onCancel={onCancel}
      width={425}
      footer={null}
      destroyOnClose
      bodyStyle={{
        padding: '13px 0 16px',
        minHeight: currentStep !== 2 ? '451px' : '168px'
      }}
      closeIcon={
        <CloseContainer
          style={{
            top: '12px',
            position: 'relative',
            right: '-21px',
            display: currentStep === 2 ? 'none' : 'flex'
          }}
        >
          <Icon color="#000" type="close" />
        </CloseContainer>
      }
    >
      {currentStep !== 2 ? header : null}
      <ModalBody withPadding>
        {showScreen[currentStep].component}
        {currentStep !== 2 ? buttons : null}
      </ModalBody>
    </Modal>
  );
};

LeaveFeedback.propTypes = {
  displaying: PropTypes.bool.isRequired
};

export default LeaveFeedback;
