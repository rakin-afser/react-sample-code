import image from './img/pic.jpg';

const exampleData = {
  img: image,
  title: 'Nike white black and gold Air Max Dia ...',
  size: 'UA 34',
  color: 'white',
  quantity: 1,
  price: '132.000',
  rate: {
    item1: 1,
    item2: 3,
    item3: 4
  }
};

export default exampleData;
