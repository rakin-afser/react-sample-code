import React, {useEffect} from 'react';
import {useLazyQuery} from '@apollo/client';

import useGlobal from 'store';
import BackPage from 'components/Modals/mobile/BackPage';
import Post from 'components/Post';
import {GET_REVIEWS} from 'queries';
import Loader from 'components/Loader';

const Feedback = () => {
  const [globalState, setGlobalState] = useGlobal();
  const [getReviews, {loading, data}] = useLazyQuery(GET_REVIEWS);

  useEffect(() => {
    if (globalState.feedback?.isOpen && globalState.feedback?.data?.databaseId) {
      getReviews({variables: {contentId: globalState.feedback?.data?.databaseId}});
    }
  }, [globalState.feedback]);

  return (
    <BackPage
      iconLeft
      title="Feedback"
      active={globalState.feedback?.isOpen}
      setActive={setGlobalState.setFeedback}
      contentStyle={{padding: '0'}}
    >
      {loading && <Loader wrapperWidth="100%" />}
      {!loading &&
        data?.comments?.nodes.map((item) => <Post key={item.id} data={{...item, isFeedbackPost: true}} isUserLogeIn />)}
    </BackPage>
  );
};

export default Feedback;
