import {secondaryFont} from 'components/Header/constants';
import styled from 'styled-components';
import Icon from 'components/Icon';

export const Container = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  background-color: #fff;
  z-index: 1000;
  max-height: 100vh;
  overflow: auto;

  &.view-enter {
    opacity: 0;
    transform: translateY(-100%);
  }
  &.view-enter-active {
    opacity: 1;
    transform: translateY(0);
    transition: opacity 300ms, transform 300ms;
  }
  &.view-exit {
    opacity: 1;
  }
  &.view-exit-active {
    opacity: 0;
    transform: translateY(-100%);
    transition: opacity 300ms, transform 300ms;
  }
`;

export const Wrapper = styled.div`
  padding: 24px 16px 0;
`;

export const Back = styled.button`
  position: absolute;
  top: 24px;
  left: 16px;
  background: #ffffff;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  width: 42px;
  height: 42px;
  border-radius: 50%;
  z-index: 1000;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const SubTitle = styled.h5`
  font-size: 12px;
  line-height: 15px;
  margin-bottom: 8px;
  color: #000;
  text-align: center;
`;

export const List = styled.ul`
  display: flex;
`;

export const SliderControls = styled.div`
  display: flex;
  align-items: flex-end;
`;

export const PostItems = styled.div`
  width: 64px;
  height: 64px;
  margin-right: 24px;
  position: relative;

  &::after {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: -6px;
    border-bottom: 2px solid #ed484f;
  }

  img {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
`;

export const Item = styled.li`
  height: 72px;
  width: 72px;

  & + & {
    margin-left: 8px;
  }

  img {
    object-fit: cover;
    width: 100%;
    height: 100%;
    border-radius: 2px;
    border: 1px solid #efefef;
  }
`;

export const AuthorHeader = styled.div`
  display: flex;
  align-items: center;
  margin-top: 44px;
  position: relative;
`;

export const AuthorMeta = styled.div``;

export const AuthorTitle = styled.h4`
  font-family: ${secondaryFont};
  font-size: 16px;
  line-height: 1;
  margin-bottom: 0;
  color: #000;
`;

export const AuthorImage = styled.img`
  width: 50px;
  height: 50px;
  border: 1px solid #f0f0f0;
  object-fit: cover;
  border-radius: 50%;
  margin-right: 8px;
`;

export const AuthorTime = styled.time`
  color: #7a7a7a;
  font-size: 12px;
`;

export const Dots = styled(Icon)`
  position: absolute;
  top: 10px;
  right: 0;
`;

export const MainDesc = styled.p`
  font-size: 16px;
  line-height: 1.5;
  margin-top: 6px;
  margin-bottom: 0;
  color: #000;
  font-weight: 500;
`;

export const Desc = styled.p`
  line-height: 1.4;
  margin-bottom: 5px;
  color: #000;
`;

// export TagList =
