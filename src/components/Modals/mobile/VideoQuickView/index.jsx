import React from 'react';
import {
  Container,
  Back,
  SubTitle,
  Wrapper,
  List,
  Item,
  PostItems,
  SliderControls,
  AuthorHeader,
  AuthorMeta,
  AuthorTitle,
  AuthorTime,
  AuthorImage,
  Dots,
  MainDesc,
  Desc
} from './styled';
import VideoSlider from 'components/VideoSlider';
import Tags from 'components/Tags';
import {list, videos} from './data';
import {CSSTransition} from 'react-transition-group';
import Icon from 'components/Icon';
import {createPortal} from 'react-dom';
import post from 'images/products/product1.jpg';

const VideoQuickView = ({active, setActive}) => {
  return createPortal(
    <CSSTransition in={active} classNames="view" timeout={300} unmountOnExit>
      <Container>
        <Back onClick={() => setActive(false)}>
          <Icon type="arrowBack" color="#000" />
        </Back>
        <VideoSlider slides={videos} />
        <Wrapper>
          <SubTitle>Products in this post:</SubTitle>
          <SliderControls>
            <PostItems>
              <img src={post} alt="alt" />
            </PostItems>
            <List>
              {list.map((item) => (
                <Item key={item}>
                  <img src={item} alt="alt" />
                </Item>
              ))}
            </List>
          </SliderControls>
          <AuthorHeader>
            <AuthorImage src={post} alt="alt" />
            <AuthorMeta>
              <AuthorTitle>Chanel</AuthorTitle>
              <AuthorTime>2m ago</AuthorTime>
            </AuthorMeta>
            <Dots type="dots" />
          </AuthorHeader>
          <MainDesc>Cream canvas Big Sad Wolf Tote Bag</MainDesc>
          <Desc>
            Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to
            the other.
          </Desc>
          <Tags />
        </Wrapper>
      </Container>
    </CSSTransition>,
    document.getElementById('modal')
  );
};

export default VideoQuickView;
