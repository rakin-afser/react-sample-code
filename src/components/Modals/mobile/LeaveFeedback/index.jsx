import useGlobal from 'store';
import BackPage from '../BackPage';
import React, {useState} from 'react';
import Main from './components/Main';
import Result from './components/Result';
import {SwitchTransition, CSSTransition} from 'react-transition-group';

const LeaveFeedback = () => {
  const [globalState, globalActions] = useGlobal();
  const [share, setShare] = useState(false);

  return (
    <BackPage
      iconLeft={!share}
      iconRight={share}
      title="Leave Feedback"
      active={globalState.leaveFeedback.open}
      setActive={globalActions.setLeaveFeedback}
    >
      <SwitchTransition mode="out-in">
        <CSSTransition
          key={!share}
          classNames="fade"
          addEndListener={(node, done) => {
            node.addEventListener('transitionend', done, false);
          }}
        >
          {!share ? <Main setShare={setShare} /> : <Result setShare={setShare} />}
        </CSSTransition>
      </SwitchTransition>
    </BackPage>
  );
};

export default LeaveFeedback;
