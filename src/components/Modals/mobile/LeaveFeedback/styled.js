import {Button} from 'antd';
import {primaryColor} from 'constants/colors';
import styled from 'styled-components/macro';

export const Container = styled.div`
  &.fade-enter {
    opacity: 0;
    transform: translateX(100%);
  }
  &.fade-enter-active {
    opacity: 1;
    transform: translateX(0%);
  }
  &.fade-exit {
    opacity: 1;
    transform: translateX(0%);
  }
  &.fade-exit-active {
    opacity: 0;
    transform: translateX(-100%);
  }
  &.fade-enter-active,
  &.fade-exit-active {
    transition: opacity 300ms ease-in-out, transform 300ms ease-in-out;
  }
`;

export const ResultWrap = styled.div`
  padding-top: 94px;
  text-align: center;
  padding-bottom: 20px;
`;

export const ResultText = styled.p`
  font-size: 16px;
  font-weight: 500;
  margin-top: 63px;
  margin-bottom: 16px;
`;

export const ColoredText = styled.p`
  color: #398287;
  margin-bottom: 48px;

  i {
    margin-left: 4px;
    vertical-align: text-bottom;
  }
`;

export const Hr = styled.hr`
  border: 4px solid #fafafa;
  margin: 0 -16px;
  margin-bottom: ${({mb}) => mb && `${mb}px`};
`;

export const Seller = styled.p`
  color: #000;
  font-weight: 500;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-top: 1px solid #efefef;
  margin-bottom: 0;

  a {
    display: inline-flex;
    align-items: center;
    justify-content: space-between;
    color: #666;
    width: 100%;
    max-width: 226px;
    padding-top: 18px;
    padding-bottom: 20px;
  }
`;

export const Wrap = styled.div`
  padding: 16px 0;

  & + & {
    border-top: 1px solid #efefef;
  }
`;

export const StyledButton = styled(Button)`
  &&& {
    border: 1px solid ${({selected}) => (selected ? '#999' : '#efefef')};
    border-radius: 8px;
    height: 40px;
    width: 100%;
    color: #666666;
    background-color: ${({selected}) => (selected ? '#fafafa' : 'transparent')};
    font-weight: ${({selected}) => (selected ? '500' : '400')};
  }

  i {
    line-height: 1;
    vertical-align: middle;
  }

  svg {
    margin-right: 8px;
  }
`;

export const Title = styled.p`
  color: #000;
  font-weight: 500;
`;

export const Score = styled(Title)`
  padding-top: 14px;
  margin-bottom: 16px;
  display: flex;
  justify-content: space-between;
  border-top: 1px solid #efefef;
`;

export const Rating = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  line-height: 1;
  padding-top: 15px;
  padding-bottom: 15px;
  border-top: 1px solid #efefef;
  font-weight: 500;

  .ant-rate {
    color: #ffc131;

    &-star > div:hover {
      transform: none;
    }

    &-star-second {
      color: #ccc;
    }

    &-star:not(:last-child) {
      margin-right: 4px;
    }
  }
`;

export const Footer = styled.div`
  padding: 16px;
  margin: 32px -16px 0;
  box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.03);
  display: flex;
  justify-content: space-between;
`;

export const StyledBtn = styled(Button)`
  &&& {
    height: 40px;
    border-radius: 100px;
    font-weight: 500;
  }
`;

export const Cancel = styled(StyledBtn)`
  &&& {
    border: none;
    color: #000;
    max-width: 120px;
    width: 100%;
  }
`;

export const Share = styled(StyledBtn)`
  &&& {
    color: #fff;
    border-color: ${primaryColor};
    background-color: ${primaryColor};
    max-width: 120px;
    width: 100%;
  }
`;

export const Continue = styled(StyledBtn)`
  &&& {
    color: #fff;
    border-color: ${primaryColor};
    background-color: ${primaryColor};
    padding-left: 29px;
    padding-right: 13px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    line-height: 1;

    i {
      margin-top: 2px;
      margin-left: 15px;
      line-height: 1;
    }
  }
`;

export const Label = styled.div`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #000;
  margin-bottom: 6px;
  display: block;

  span {
    color: ${primaryColor};
    margin-left: 3px;
  }
`;

export const TextAreaWrapper = styled.div`
  margin: 0 0 16px 0;
`;
