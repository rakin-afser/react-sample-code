import React from 'react';
import {Container, ResultWrap, ResultText, ColoredText, Continue} from '../styled';
import Icon from 'components/Icon';
import {useHistory} from 'react-router';
import useGlobal from 'store';

const Result = ({setShare}) => {
  const [, globalActions] = useGlobal();

  function onClick() {
    setTimeout(() => setShare(false), 1000);
    globalActions.setLeaveFeedback(false);
  }

  return (
    <Container>
      <ResultWrap>
        <Icon type="checkCircleFilled" />
        <ResultText>Thank You for your feedback!</ResultText>
        <ColoredText>
          You have unlocked 23 MidCoins <Icon type="coins" />
        </ColoredText>
        <Continue onClick={onClick}>
          Continue Shopping
          <Icon widht="7" height="12" type="chevronRight" fill="currentColor" />
        </Continue>
      </ResultWrap>
    </Container>
  );
};

export default Result;
