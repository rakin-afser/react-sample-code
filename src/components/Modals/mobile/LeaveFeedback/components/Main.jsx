import React, {useState} from 'react';
import {Row, Col, Rate, Input} from 'antd';
import {useHistory, Link} from 'react-router-dom';
import {useMutation} from '@apollo/client';

import ProdDetail from 'components/ProdDetail/mobile';
import Icon from 'components/Icon';
import {
  Container,
  Wrap,
  Hr,
  TextAreaWrapper,
  Label,
  Seller,
  Title,
  Rating,
  Score,
  Footer,
  Cancel,
  Share,
  StyledButton
} from '../styled';
import AttachBlock from 'components/CreatePost/components/AttachBlock';
import {WRITE_REVIEW} from 'containers/OrdersPage/api/queries';
import {useCreatePostMutation} from 'hooks/mutations';
import {useUser} from 'hooks/reactiveVars';
import axiosClient from 'axiosClient';
import useGlobal from 'store';
import axios from 'axios';
import Loader from 'components/Loader';

const ratings = [
  {title: 'Positive', selected: true},
  {title: 'Neutral', selected: false},
  {title: 'Negative', selected: false}
];

const Main = ({setShare}) => {
  const history = useHistory();
  const [globalState, globalActions] = useGlobal();
  const [generalOpinion, setGeneralOpinion] = useState('Positive');
  const [uploads, setUploads] = useState([]);
  const [accuracy, setAccuracy] = useState(5);
  const [communication, setCommunication] = useState(5);
  const [delivery, setDelivery] = useState(5);
  const [inputValue, setInputValue] = useState('');
  const [addReviewLoading, setAddReviewLoading] = useState(false);
  const [user] = useUser();
  const userId = user?.databaseId.toString();
  const {data} = globalState.leaveFeedback;

  const store = data?.product?.seller;

  const [post] = useCreatePostMutation();
  const [writeReview, {data: writeReviewData, loading, error}] = useMutation(WRITE_REVIEW, {
    onCompleted() {
      setAddReviewLoading(false);
      setShare(true);
    },
    onError() {
      // TODO: display error
      setAddReviewLoading(false);
      setShare(true);
    }
  });

  const getTotalScore = () => {
    const total = Math.round((accuracy + communication + delivery) / 3);
    return total;
  };

  const onGoToStore = () => {
    globalActions.setLeaveFeedback({open: false, data: null});
    history.push(`/shop/${store?.id}/products`);
  };

  const addImages = (value) => {
    setUploads((oldArray) => [...oldArray, value]);
  };

  const onWriteReview = async () => {
    const filesToUpload = uploads?.map((upload) => {
      const formData = new FormData();
      formData.append('file', upload?.file);
      return axiosClient.post('/', formData);
    });
    try {
      setAddReviewLoading(true);
      const res = await axios.all(filesToUpload);
      const galleryId = res?.map((item) => item.data.id);

      writeReview({
        variables: {
          input: {
            rating: getTotalScore(),
            author: userId,
            content: inputValue,
            general_opinion: generalOpinion,
            commentOn: data?.product?.databaseId,
            attachmentIds: galleryId,
            order_id: data?.orderId,
            store_id: +data?.product?.seller?.id
          }
        },
        update(cache, {data}) {
          cache.modify({
            fields: {
              products() {
                return data;
              }
            }
          });
        }
      });

      post({
        variables: {
          input: {
            status: 'PUBLISH',
            title: '',
            content: inputValue,
            isFeedbackPost: true,
            rating: getTotalScore(),
            relatedProducts: [data?.product?.databaseId],
            galleryId
          }
        }
      });
    } catch (err) {
      setAddReviewLoading(false);
      setShare(true);
      console.log(`error`, err);
    }
  };

  return (
    <Container>
      {addReviewLoading ? (
        <Loader wrapperWidth="100%" />
      ) : (
        <>
          <ProdDetail data={globalState?.leaveFeedback?.data} />
          <Seller>
            Seller:
            <Link onClick={onGoToStore}>
              {store?.name} ({store?.storesCount})
              <Icon type="chevronRight" fill="currentColor" />
            </Link>
          </Seller>
          <Hr />
          <Wrap>
            <Title>Overall Rating</Title>
            <Row gutter={8}>
              {ratings.map((item) => (
                <Col key={item.title} span={8}>
                  <StyledButton onClick={() => setGeneralOpinion(item?.title)} selected={generalOpinion === item.title}>
                    {generalOpinion === item.title && <Icon color="#2ECC71" type="checkCircle" />}
                    {item.title}
                  </StyledButton>
                </Col>
              ))}
            </Row>
          </Wrap>
          <Rating>
            Accurate Description
            <Rate
              value={accuracy}
              onChange={(e) => setAccuracy(e || 1)}
              character={<Icon fill="currentColor" width={20} height={20} type="star" />}
            />
          </Rating>
          <Rating>
            Seller’s communication
            <Rate
              value={communication}
              onChange={(e) => setCommunication(e || 1)}
              character={<Icon fill="currentColor" width={20} height={20} type="star" />}
            />
          </Rating>
          <Rating>
            Delivery Time
            <Rate
              value={delivery}
              onChange={(e) => setDelivery(e || 1)}
              character={<Icon fill="currentColor" width={20} height={20} type="star" />}
            />
          </Rating>
          <Score>
            You Score <span>{getTotalScore()}</span>
          </Score>
          <Hr mb={24} />
          <TextAreaWrapper>
            <Label>Your Feedback</Label>
            <Input.TextArea
              label="Your Feedback"
              placeholder="Write your review"
              value={inputValue}
              onChange={(e) => setInputValue(e.target.value)}
              maxLength={400}
              autoSize={{minRows: 5, maxRows: 5}}
            />
          </TextAreaWrapper>
          <div>You can add up to 5 photos or videos. This feedback will be shared in feed.</div>
          <AttachBlock uploads={uploads} setUploads={setUploads} addImages={addImages} noYoutubeLink small noPreview />
          <Footer>
            <Cancel onClick={() => globalActions.setLeaveFeedback(false)}>Cancel</Cancel>
            <Share onClick={onWriteReview}>Share</Share>
          </Footer>
        </>
      )}
    </Container>
  );
};

export default Main;
