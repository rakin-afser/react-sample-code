import React, {useEffect, useState} from 'react';
import Div100vh from 'react-div-100vh';
import {string, bool, func} from 'prop-types';

import Icon from 'components/Icon';
import {Women, Men, Kids} from './tabs';

import {Content, Header, IconWrapper, PopupWrapper, Title, Tabs, Tab} from './styled';
import useGlobal from 'store';
import {createPortal} from 'react-dom';

const SizeGuide = ({title}) => {
  const [globalState, globalActions] = useGlobal();
  const [tab, setTab] = useState(0);
  const tabs = ['Women', 'Men', 'Kids'];

  const renderTabs = () => {
    switch (tab) {
      case 2:
        return <Kids />;
      case 1:
        return <Men />;
      case 0:
      default:
        return <Women />;
    }
  };

  useEffect(() => {
    if (globalState.showSizeGuide) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [globalState.showSizeGuide]);

  return createPortal(
    <PopupWrapper active={globalState.showSizeGuide}>
      <Header>
        <Title>{title}</Title>

        <IconWrapper onClick={() => globalActions.setShowSizeGuide(false)}>
          <Icon type="close" svgStyle={{width: 24, height: 24, color: '#1A1A1A'}} />
        </IconWrapper>
      </Header>

      <Content>
        <Div100vh
          style={{
            height: '100vh',
            maxHeight: 'calc(100rvh - 70px)',
            overflowX: 'hidden',
            overflowY: 'auto'
          }}
        >
          <Tabs>
            {tabs.map((name, index) => (
              <Tab key={index} active={index === tab} onClick={() => setTab(index)}>
                {name}
              </Tab>
            ))}
          </Tabs>
          {renderTabs()}
        </Div100vh>
      </Content>
    </PopupWrapper>,
    document.getElementById('modal')
  );
};

SizeGuide.defaultProps = {
  title: 'Size guide'
};

SizeGuide.propTypes = {
  title: string
};

export default SizeGuide;
