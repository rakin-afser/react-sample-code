import React, { useState } from 'react';
import Scrollbar from 'react-scrollbars-custom';
import {  } from 'prop-types';

import {
  SubTab,
  TabContent,
  Tabs,
  Table,
  TableContainer
} from '../styled';

const Women = ({  }) => {
  const [tab, setTab] = useState(0);
  const tabs = ['Clothes', 'Shoes'];

  const renderTabs = () => {
    switch (tab) {
      case 1:
        return shoes();
      case 0:
      default:
        return clothes();
    }
  };

  const clothes = () => {
    return <TableContainer>
      <Scrollbar
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>Size</th>
              <th>UK</th>
              <th colSpan={2}>Bust</th>
              <th colSpan={2}>Waist</th>
            </tr>
            <tr>
              <th/>
              <th/>
              <th>Inches</th>
              <th>CM</th>
              <th>Inches</th>
              <th>CM</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td><span className="name">XS</span></td>
              <td>6</td>
              <td>31</td>
              <td>78.5</td>
              <td>23.75</td>
              <td>86</td>
            </tr>

            <tr>
              <td><span className="name">S</span></td>
              <td>8</td>
              <td>32</td>
              <td>81</td>
              <td>24.75</td>
              <td>88.5</td>
            </tr>

            <tr>
              <td><span className="name">M</span></td>
              <td>10</td>
              <td>34</td>
              <td>86</td>
              <td>26.75</td>
              <td>93.5</td>
            </tr>

            <tr>
              <td><span className="name">L</span></td>
              <td>12</td>
              <td>36</td>
              <td>91</td>
              <td>28.75</td>
              <td>98.5</td>
            </tr>

            <tr>
              <td><span className="name">XL</span></td>
              <td>14</td>
              <td>38</td>
              <td>99</td>
              <td>30.75</td>
              <td>103.5</td>
            </tr>

            <tr>
              <td><span className="name">XXL</span></td>
              <td>16</td>
              <td>40</td>
              <td>101</td>
              <td>32.75</td>
              <td>108.5</td>
            </tr>

            <tr>
              <td><span className="name">XXXL</span></td>
              <td>18</td>
              <td>42</td>
              <td>106</td>
              <td>34.75</td>
              <td>113.5</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  const shoes = () => {
    return <TableContainer>
      <Scrollbar
        disableTracksWidthCompensation
        trackXProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackX"/>;
          }
        }}
        trackYProps={{
          renderer: (props) => {
            const {elementRef, ...restProps} = props;
            return <span {...restProps} ref={elementRef} className="TrackY"/>;
          }
        }}
      >
        <Table>
          <thead>
            <tr>
              <th>EU</th>
              <th>UK</th>
              <th>UK</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>35</td>
              <td>5</td>
              <td>2.5</td>
            </tr>

            <tr>
              <td>35.5</td>
              <td>5.5</td>
              <td>2.5</td>
            </tr>

            <tr>
              <td>36</td>
              <td>6</td>
              <td>3.5</td>
            </tr>

            <tr>
              <td>37</td>
              <td>6.5</td>
              <td>4</td>
            </tr>

            <tr>
              <td>37.5</td>
              <td>7</td>
              <td>4.5</td>
            </tr>

            <tr>
              <td>38</td>
              <td>7.5</td>
              <td>5</td>
            </tr>

            <tr>
              <td>39</td>
              <td>8</td>
              <td>5.5</td>
            </tr>

            <tr>
              <td>39.5</td>
              <td>8.5</td>
              <td>6</td>
            </tr>

            <tr>
              <td>40</td>
              <td>9</td>
              <td>6.5</td>
            </tr>

            <tr>
              <td>40.5</td>
              <td>9.5</td>
              <td>7</td>
            </tr>

            <tr>
              <td>41</td>
              <td>10</td>
              <td>7.5</td>
            </tr>

            <tr>
              <td>42</td>
              <td>10.5</td>
              <td>8</td>
            </tr>
          </tbody>
        </Table>
      </Scrollbar>
    </TableContainer>
  };

  return <TabContent>
    <Tabs>
      {
        tabs.map((name, index) =>
          <SubTab
            key={index}
            active={index === tab}
            onClick={() => setTab(index)}
          >
            {name}
          </SubTab>
        )
      }
    </Tabs>
    {renderTabs()}
  </TabContent>
};

Women.defaultProps = {

};

Women.propTypes = {

};

export default Women;