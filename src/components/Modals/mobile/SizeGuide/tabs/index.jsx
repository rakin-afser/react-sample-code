import Women from './Women';
import Men from './Men';
import Kids from './Kids';

export {
  Women,
  Men,
  Kids
};