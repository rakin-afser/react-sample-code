import React, {useEffect} from 'react';

import Icon from 'components/Icon';

import {PopupWrapper, IconWrapper} from './styled';
import useGlobal from 'store';

const Popup = () => {
  const [globalState, globalActions] = useGlobal();

  useEffect(() => {
    if (globalState.showProductAdded) {
      setTimeout(() => {
        globalActions.setShowProductAdded(false);
      }, 3000);
    }
  }, [globalState.showProductAdded]);

  return (
    <PopupWrapper active={globalState.showProductAdded}>
      <IconWrapper>
        <Icon type="checkbox" />
      </IconWrapper>
      <span>Product Added to Cart</span>
    </PopupWrapper>
  );
};

Popup.defaultProps = {};

Popup.propTypes = {};

export default Popup;
