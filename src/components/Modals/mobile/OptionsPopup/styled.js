import {Button} from 'antd';
import styled from 'styled-components';

export const PopupOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0, 0, 0, 0.5);
`;

export const PopupContent = styled.div`
  margin-top: auto;
  padding: 10px 10px;
  width: 100%;
  z-index: 2;
  transition: bottom 0.3s ease;
  position: absolute;
  left: 0;
  right: 0;
  bottom: -100%;
`;

export const Popup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100vw;
  z-index: 10000;
  display: flex;
  flex-wrap: wrap;

  &.enter-active ${PopupContent} {
    bottom: 0;
    transition: bottom 300ms;
  }

  &.enter-done ${PopupContent} {
    bottom: 0;
  }

  &.exit ${PopupContent} {
    bottom: 0;
  }

  &.exit-active ${PopupContent} {
    bottom: -100%;
    transition: bottom 300ms;
  }

  &.enter ${PopupOverlay} {
    opacity: 0;
  }

  &.enter-active ${PopupOverlay} {
    opacity: 1;
    transition: opacity 300ms;
  }

  &.exit ${PopupOverlay} {
    opacity: 1;
  }

  &.exit-active ${PopupOverlay} {
    opacity: 0;
    transition: opacity 300ms;
  }
`;

export const BtnsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 8px;
`;

export const Btn = styled(Button)`
  &&& {
    font-family: 'SF Pro Display', sans-serif;
    border-radius: 0;
    border: none;
    border-top: 1px solid #dadade;
    height: 56px;
    font-size: 20px;
    color: ${({warn}) => (warn ? '#FF212A' : '#007aff')};
    letter-spacing: 0.38px;
    background-color: rgba(248, 248, 248, 0.96);

    &:first-child {
      border-radius: 13px 13px 0 0;
    }
    &:last-child {
      border-radius: 0 0 13px 13px;
    }
  }
`;

Btn.defaultProps = {
  block: true
};

export const Cancel = styled(Button)`
  &&& {
    font-family: 'SF Pro Display', sans-serif;
    font-weight: 600;
    border-radius: 13px;
    font-size: 20px;
    height: 57px;
    color: #007aff;
    letter-spacing: 0.38px;
  }
`;

Cancel.defaultProps = {
  block: true
};
