import {Popup, PopupOverlay, PopupContent, Cancel, BtnsWrapper, Btn} from './styled';
import React from 'react';
import Div100vh from 'react-div-100vh';
import {createPortal} from 'react-dom';
import useGlobal from 'store';
import {CSSTransition} from 'react-transition-group';

const OptionsPopup = () => {
  const [globalState, globalActions] = useGlobal();
  const {active, buttons} = globalState.optionsPopup;

  return createPortal(
    <CSSTransition in={active} timeout={300} unmountOnExit>
      <Popup>
        <Div100vh
          style={{
            height: '100vh',
            width: '100%',
            maxHeight: 'calc(100rvh)',
            display: 'flex'
          }}
        >
          <PopupOverlay onClick={() => globalActions.setOptionsPopup({active: false})} />
          <PopupContent>
            <BtnsWrapper>
              {buttons.map((btn) =>
                btn ? (
                  <Btn warn={btn.warn ? 1 : 0} onClick={btn.onClick} key={btn.title}>
                    {btn.title}
                  </Btn>
                ) : null
              )}
            </BtnsWrapper>
            <Cancel onClick={() => globalActions.setOptionsPopup({active: false})}>Cancel</Cancel>
          </PopupContent>
        </Div100vh>
      </Popup>
    </CSSTransition>,
    document.getElementById('modal')
  );
};

export default OptionsPopup;
