import React, {useState, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {useLazyQuery} from '@apollo/client';
import htmlParse from 'html-react-parser';
import moment from 'moment';

import {
  StyledModal,
  ModalContent,
  SliderWrapMobile,
  Block,
  Image,
  Previews,
  PreviewItem,
  PostInfoWrapper,
  PreviewText,
  PostTitle,
  PostContent
} from 'components/Modals/mobile/PostViewMobile/styled';
import BackButton from 'components/BackButton';
import Loader from 'components/Loader';
import {PRODUCT_QUICKVIEW} from 'queries';
import {useUser} from 'hooks/reactiveVars';
import useGlobal from 'store';
import Comments from 'components/Comments';
import MainSlider from 'components/ProductPage/mobile/MainSlider';
import ShopInfo from 'components/ShopInfo/desktop/Small';
import productPlaceholder from 'images/placeholders/product.jpg';
import userPlaceholder from 'images/placeholders/user.jpg';
import {ReactComponent as IconPlay} from 'components/QuickView/desktop/components/VideoPlayer/img/iconPlay.svg';
import Tags from 'components/Tags';
import Info from 'components/QuickView/mobile/Info';
import {useVariations} from 'hooks';
import {getVariationByAttrs, renderCoinsByVariation, stripTags} from 'util/heplers';
import Actions from 'components/Modals/mobile/PostViewMobile/components/Actions';
import useShare from 'hooks/useShare';
import {useAddUpdateLikedPostMutation} from 'hooks/mutations';
import SizeGuide from 'components/ProductPage/mobile/SizeGuide';
import AuthorInfo from 'components/Modals/mobile/PostViewMobile/components/AuthorInfo';
import {v1} from 'uuid';

const PostViewMobile = ({isOpen = false}) => {
  const [user] = useUser();
  const [globalState, setGlobalState] = useGlobal();
  const history = useHistory();
  const [media, setMedia] = useState([]);
  const [currentSlide, setCurrentSlide] = useState({});
  const [showSizeGuide, setShowSizeGuide] = useState(false);
  const [showVariationsPopup, setShowVariationsPopup] = useState(false);
  const [showMessage, setShowMessage] = useState(false);
  const [showCommentsPopup, setShowCommentsPopup] = useState(false);
  const data = globalState.quickView?.postData;
  const [triggerLike] = useAddUpdateLikedPostMutation({post: data});
  const {
    id,
    databaseId,
    author,
    comments,
    title,
    content = '',
    slug,
    isLiked,
    galleryImages = {},
    totalLikes,
    video,
    relatedProducts,
    initialProductSlug
  } = globalState.quickView?.postData || {};
  const avatar = author?.node?.avatar?.url;
  const userName = author?.node?.name;
  const tags = data?.tags?.nodes?.map((tag) => `#${tag.name}`);
  const commentCount = data?.commentCount;

  const [getProduct, {data: productData, loading: productLoading}] = useLazyQuery(PRODUCT_QUICKVIEW);

  const date = moment(data?.date).format('DD MMM YYYY');

  useEffect(() => {
    if (currentSlide?.type === 'product' && currentSlide?.slug) {
      getProduct({variables: {id: currentSlide?.slug}});
    }
  }, [currentSlide]);

  useEffect(() => {
    const gallery =
      galleryImages?.nodes?.map((item) => ({
        ...item
      })) || [];
    const products =
      relatedProducts?.nodes?.map((item) => ({
        id: item?.slug,
        type: 'product',
        preview: item?.image?.sourceUrl,
        children: [{mimeType: 'image', mediaItemUrl: item?.image?.sourceUrl}],
        slug: item?.slug,
        databaseId: item?.databaseId
      })) || [];
    const postMedia = [
      {
        id: 'post-media',
        type: gallery?.[0]?.mimeType || 'post-media',
        preview: gallery?.[0]?.poster?.mediaItemUrl || gallery?.[0]?.mediaItemUrl || productPlaceholder,
        children: [...gallery]
      }
    ];

    const currentMedia = [...postMedia, ...products];
    setMedia(currentMedia);
    setCurrentSlide(currentMedia?.[0] || {});
  }, [galleryImages?.nodes, relatedProducts?.nodes, video]);

  useEffect(() => {
    if (initialProductSlug && currentSlide?.slug !== initialProductSlug) {
      setCurrentSlide(media.find((i) => i?.slug === initialProductSlug) || media?.[0]);
    }
  }, [initialProductSlug, media]);

  const onClose = () => {
    setGlobalState.setQuickView(null);
  };

  function onLike() {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    triggerLike({variables: {input: {id: databaseId}}});
  }

  function onComment() {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    setShowCommentsPopup(true);
  }

  const onShare = async () => {
    const shareData = {
      title: `testSample - ${title}`,
      text: stripTags(content),
      url: `${window.location.origin}/post/${databaseId}`
    };

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const goToProductPage = () => {
    onClose();
    const params = databaseId ? `?postId=${databaseId}` : '';
    history.push(`/product/${productData?.product?.slug}${params}`);
  };

  const getProductSliders = () => {
    const {image, galleryImages, product_video_url: productVideoUrl} = productData?.product || {};

    const mainImage = image ? [image] : [];
    const productVideo = productVideoUrl ? [{id: v1(), mimeType: 'video', mediaItemUrl: productVideoUrl}] : [];
    const gallery = galleryImages?.nodes || [];

    return [...mainImage, ...productVideo, ...gallery];
  };

  const {
    databaseId: databaseProductId,
    name,
    isLiked: isProductLiked,
    totalLikes: totalProductLikes,
    reviewCount,
    commentCount: commentProductCount,
    midCoins,
    averageRating,
    variations,
    productCategories
  } = productData?.product || {};

  const [vars, varState, setVarState] = useVariations(productData?.product);
  const variation = getVariationByAttrs(productData?.product, varState);

  const {price, regularPrice, salePrice} = variation || productData?.product || {};

  return (
    <StyledModal
      visible={isOpen}
      onCancel={onClose}
      width="100%"
      style={{
        width: '100%',
        margin: 0,
        maxWidth: '100%',
        top: 0,
        padding: 0
      }}
      footer={null}
      destroyOnClose
    >
      <BackButton onClick={onClose} />
      <ModalContent>
        <SliderWrapMobile>
          {productLoading ? (
            <Loader wrapperHeight="470px" wrapperWidth="100%" />
          ) : (
            <MainSlider slides={currentSlide?.type === 'product' ? getProductSliders() : currentSlide?.children} />
          )}
        </SliderWrapMobile>
        <PreviewText>Products in this post:</PreviewText>

        <Previews>
          {media?.map((item, index) => (
            <PreviewItem
              onClick={() => setCurrentSlide(item)}
              isPostMedia={item?.id === 'post-media'}
              isActive={currentSlide?.id === item?.id}
              isVideo={item.type?.split('/')?.[0] === 'video'}
              key={index}
            >
              <Image src={item.preview} />
              {item.type?.split('/')?.[0] === 'video' && <IconPlay />}
            </PreviewItem>
          ))}
        </Previews>

        <div style={{marginBottom: 20}}>
          <Actions
            onCommentsClick={onComment}
            onLikeClick={onLike}
            onShareClick={onShare}
            isLiked={currentSlide?.type === 'product' ? isProductLiked : isLiked}
            totalLikes={currentSlide?.type === 'product' ? totalProductLikes : totalLikes}
            totalComments={currentSlide?.type === 'product' ? commentProductCount : commentCount}
          />
        </div>

        {currentSlide?.type === 'product' ? (
          <>
            <Info
              postId={databaseId}
              databaseId={databaseProductId}
              goToProductPage={goToProductPage}
              name={name}
              rating={averageRating}
              reviews={reviewCount}
              price={price}
              regularPrice={regularPrice}
              salePrice={salePrice}
              coins={renderCoinsByVariation(midCoins, variation)}
              showVariationsPopup={showVariationsPopup}
              setShowVariationsPopup={setShowVariationsPopup}
              showMessage={showMessage}
              setShowMessage={setShowMessage}
              setShowSizeGuide={setShowSizeGuide}
              variations={variations}
              variation={variation}
              productCategories={productCategories}
              vars={vars}
              varState={varState}
              setVarState={setVarState}
            />
            <SizeGuide showSizeGuide={showSizeGuide} setShowSizeGuide={setShowSizeGuide} />
          </>
        ) : (
          <>
            <Block>
              <AuthorInfo src={avatar || userPlaceholder} authorName={userName} date={date} />
            </Block>

            <PostInfoWrapper>
              <PostTitle>{title}</PostTitle>
              <PostContent>{content ? htmlParse(content) : ''}</PostContent>
              <Tags
                onClick={onClose}
                items={
                  currentSlide?.type === 'product' && !productLoading
                    ? productData?.product?.productTags?.nodes
                    : tags?.nodes
                }
              />
            </PostInfoWrapper>
          </>
        )}
        <Comments
          author={author?.node?.name}
          postId={id}
          commentOn={databaseId}
          data={comments?.nodes}
          isUserLogeIn={!!user?.databaseId}
          isModal={false}
          showCommentsPopup={showCommentsPopup}
          setShowCommentsPopup={setShowCommentsPopup}
        />
      </ModalContent>
    </StyledModal>
  );
};

export default PostViewMobile;
