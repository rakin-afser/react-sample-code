import styled from 'styled-components/macro';

export const Row = styled.div`
  display: flex;
  align-items: center;
`;

export const Name = styled.span`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 140%;
  color: #000000;
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const AuthorInfoWrapper = styled.div`
  padding: 0 0 0 8px;
`;

export const DateTime = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  display: flex;
  align-items: center;
  color: #8f8f8f;
`;

export const AuthorImage = styled.img`
  border: 1px solid #f0f0f0;
  height: 50px;
  width: 50px;
  border-radius: 50%;
`;
