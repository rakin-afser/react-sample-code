import React from 'react';

import {Row, Name, Wrapper, AuthorInfoWrapper, DateTime, AuthorImage} from './styled.js';
import Icon from 'components/Icon';

const AuthorInfo = ({authorName, src, date, onDotsClick}) => {
  return (
    <Wrapper>
      <Row>
        <AuthorImage src={src} />
        <AuthorInfoWrapper>
          <Name>{authorName}</Name>
          <DateTime>{date}</DateTime>
        </AuthorInfoWrapper>
      </Row>
      <Row>
        <Icon type="dots" onClick={onDotsClick} />
      </Row>
    </Wrapper>
  );
};

export default AuthorInfo;
