import React from 'react';

import {ActionsWrap, Item, Likes, LikeWrapper} from './styled.js';
import Icon from 'components/Icon';
import CommentsIcon from 'assets/Comments';
import Share from 'assets/Share';

const Actions = ({
  onCommentsClick,
  onLikeClick,
  isLiked,
  totalLikes,
  totalComments,
  onShareClick
}) => {
  return (
    <ActionsWrap>
      <Item onClick={onLikeClick}>
        <LikeWrapper>
          <Icon type={isLiked ? 'liked' : 'like'} color={isLiked ? '#ED484F' : '#8F8F8F'} width="20" height="18" />
          {totalLikes ? <Likes>{totalLikes}</Likes> : null}
        </LikeWrapper>
      </Item>
      <Item onClick={onCommentsClick}>
        <CommentsIcon fill="#8F8F8F" />
        {totalComments ? <Likes>{totalComments}</Likes> : null}
      </Item>
      <Item onClick={onShareClick}>
        <Share fill="#999"/>
      </Item>
    </ActionsWrap>
  );
};

Actions.defaultProps = {
  onCommentsClick: () => {},
  onLikeClick: () => {},
  onShareClick: () => {}
};

export default Actions;
