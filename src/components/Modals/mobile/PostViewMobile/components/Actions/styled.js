import styled from 'styled-components/macro';
import media from 'constants/media';
import {mainFont} from 'constants/fonts';

export const ActionsWrap = styled.div`
  display: flex;
  width: 100%;
  padding: 15px 16px 15px;
  position: relative;
  border-top: 1px solid #e4e4e4;
  border-bottom: 1px solid #e4e4e4;
`;

export const Likes = styled.span`
  margin-top: 0;
  display: inline-block;
  font-family: ${mainFont};
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #8f8f8f;
  align-self: center;
  margin-left: 6px;
  margin-right: 23px;
`;

export const Item = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  height: 26px;
  background: #f5f5f5;
  border: 0.5px solid #cccccc;
  box-sizing: border-box;
  border-radius: 22px;
  padding: 0 10px;
  cursor: pointer;
  margin-left: ${({marginAuto}) => (marginAuto ? 'auto' : 'unset')};

  i,
  svg {
    max-width: 15px;
    max-height: 15px;
  }

  &:not(:last-child) {
    margin-right: 10px;
  }

  span {
    font-size: 10px;
    line-height: 1;
    color: #999999;
    margin-right: 0;
    line-height: 13px;
  }
`;

export const LikeWrapper = styled.div`
  display: flex;
  align-items: center;
`;
