import styled, {css} from 'styled-components/macro';
import {Modal} from 'antd';
import {primaryColor} from 'constants/colors';

export const StyledModal = styled(Modal)`
  & .ant-modal-content {
    border-radius: 0;
    background: #fff;
  }

  & .ant-modal-body {
    display: flex;
    flex-direction: column;
    padding: 0;
    height: 100%;
    margin: 0;
    min-height: 100vh;
  }

  & .ant-modal-close {
    display: none;
  }
`;

export const SliderWrapMobile = styled.div`
  position: relative;

  & .video-js {
    width: 100%;
  }
`;

export const ModalContent = styled.div`
  overflow-y: auto;
  flex-grow: 1;
  position: relative;
`;

export const Block = styled.div`
  margin: 0 16px;
`;

export const PreviewItem = styled.div`
  position: relative;
  cursor: pointer;
  width: 72px;
  height: 72px;
  margin-right: 8px;
  flex-direction: row;
  display: flex;
  flex-shrink: 0;
  border: 1px solid #efefef;
  border-radius: 2px;

  & svg {
    position: absolute;
    width: 8px;
    height: 8px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    path {
      fill: #ffffff;
    }
  }

  ${({isVideo}) =>
    isVideo &&
    css`
      &::before {
        content: '';
        width: 19px;
        height: 19px;
        position: absolute;
        background: #000000;
        opacity: 0.4;
        border: 0.5px solid #000000;
        box-sizing: border-box;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.19);
        border-radius: 50%;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
    `}

  ${({isActive}) =>
    isActive &&
    css`
      &::after {
        content: '';
        width: 100%;
        height: 2px;
        position: absolute;
        background: ${primaryColor};
        bottom: -1px;
      }
    `}

    ${({isPostMedia}) =>
      isPostMedia &&
      css`
        width: 64px;
        height: 64px;
        margin-right: 24px;
      `}
`;

export const Previews = styled.div`
  margin: 2px 0 10px 16px;
  max-width: 100%;
  overflow-x: auto;
  display: flex;
  align-items: center;
  transition: margin 0.5s;
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  flex-shrink: 0;
`;

export const PostInfoWrapper = styled.div`
  padding: 16px;
`;

export const PreviewText = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  margin: 20px 0 8px;
  width: 100%;
  text-align: center;
  color: #000000;
`;

export const PostTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: #000000;
  margin-bottom: 2px;
`;

export const PostContent = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #000000;
  opacity: 0.8;
`;
