import styled from 'styled-components';
import {blue} from 'constants/colors';

export const Wrapper = styled.div`
  padding: 16px 0 24px;

  ${({bt}) => bt && 'border-top: 1px solid #efefef'};

  & + & {
    padding-top: 24px;
    position: relative;

    &::before {
      content: '';
      background-color: #fafafa;
      height: 8px;
      position: absolute;
      top: 0;
      left: -16px;
      right: -16px;
    }
  }

  &:last-child {
    font-size: 16px;
    > div + div {
      margin-top: 24px;
    }
  }
`;

export const Title = styled.div`
  margin-bottom: 24px;
  font-weight: 500;
  font-size: 18px;
  color: #000000;
`;

export const Name = styled.div`
  margin-bottom: 3px;
  font-weight: 500;
  font-size: 16px;
  color: #000;
`;

export const Txt = styled.div`
  margin-bottom: 3px;

  &:last-child {
    margin-bottom: 0;
  }

  span {
    font-size: 14px;
    font-weight: 500;
    color: #000;
  }
`;

export const Seller = styled.span`
  &&& {
    font-size: 16px;
    color: ${blue};
  }
`;
