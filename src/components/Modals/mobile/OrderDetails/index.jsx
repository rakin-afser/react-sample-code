import React from 'react';
import BackPage from 'components/Modals/mobile/BackPage';
import useGlobal from 'store';
import {useHistory} from 'react-router-dom';
import {Wrapper, Title, Name, Txt, Seller} from './styled';
import ProdDetail from 'components/ProdDetail/mobile';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import moment from 'moment';

const OrderDetails = () => {
  const history = useHistory();
  const [globalState, globalActions] = useGlobal();

  const shipping = globalState?.orderDetails?.data?.shipping;
  const total = globalState?.orderDetails?.data?.total;
  const date = globalState?.orderDetails?.data?.date;
  const products = globalState?.orderDetails?.data?.lineItems?.nodes;
  const seller = globalState?.orderDetails?.data?.stores?.nodes[0];

  const onGoToSeller = () => {
    history.push(`/shop/${seller?.id}/products`);
    globalActions.setOrderDetails({open: false, data: null});
  };

  return (
    <BackPage
      iconRight
      active={globalState.orderDetails.open}
      setActive={globalActions.setOrderDetails}
      title="Order Details"
    >
      {products?.map((item, idx) => (
        <ProdDetail key={idx} data={item} />
      ))}

      <Wrapper bt>
        <Title>Shipping Address</Title>
        <Name>
          {shipping?.firstName} {shipping?.lastName}
        </Name>
        <Txt>
          {shipping?.postcode} {shipping?.address1}
        </Txt>
        <Txt>
          {shipping?.address2} {shipping?.state}
        </Txt>
        <Txt>
          {shipping?.city} {getCountryFromCode(shipping?.country)}
        </Txt>
      </Wrapper>
      <Wrapper>
        <Title>Payment Method</Title>
        <Txt>
          Visa
          <span> Danila Puoytrbrayn,</span> <span>Expires: 02/2020</span>
        </Txt>
        <Txt>Received Cash on delivery</Txt>
        <Txt>Paypal Transaction number</Txt>
      </Wrapper>
      <Wrapper>
        <Txt>
          Total: <span>{total}</span>
        </Txt>
        <Txt>
          GrandTotal: <span>{total}</span>
        </Txt>
        <Txt>
          Sold by: <Seller onClick={onGoToSeller}>{seller?.name}</Seller>
        </Txt>
        <Txt>
          Order Placed: <span>{moment(date).format('ddd, MMM, d, YYYY')}</span>
        </Txt>
      </Wrapper>
    </BackPage>
  );
};

export default OrderDetails;
