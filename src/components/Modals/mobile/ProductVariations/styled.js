import styled from 'styled-components/macro';

export const PopupOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0, 0, 0, 0.4);
`;

export const PopupContent = styled.div`
  position: absolute;
  left: 10px;
  right: 10px;
  bottom: -100%;
  border-radius: 12px;
  z-index: 101;
  background: #fff;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  max-width: 100vw;
  padding: 17px;
`;

export const Popup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100vw;
  z-index: 10000;
  display: flex;
  flex-wrap: wrap;

  &.enter-active ${PopupContent} {
    bottom: 10px;
    transition: bottom 300ms;
  }

  &.enter-done ${PopupContent} {
    bottom: 10px;
  }

  &.exit ${PopupContent} {
    bottom: 10px;
  }

  &.exit-active ${PopupContent} {
    bottom: -100%;
    transition: bottom 300ms;
  }

  &.enter ${PopupOverlay} {
    opacity: 0;
  }

  &.enter-active ${PopupOverlay} {
    opacity: 1;
    transition: opacity 300ms;
  }

  &.exit ${PopupOverlay} {
    opacity: 1;
  }

  &.exit-active ${PopupOverlay} {
    opacity: 0;
    transition: opacity 300ms;
  }
`;

export const IconWrapper = styled.div`
  position: absolute;
  top: -3px;
  right: 0;
  z-index: 110;
  cursor: pointer;
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
