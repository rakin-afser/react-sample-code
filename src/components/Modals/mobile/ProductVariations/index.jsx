import React, {useEffect} from 'react';

import Icon from 'components/Icon';
import Buttons from './components/Buttons';

import {Popup, PopupOverlay, PopupContent, IconWrapper} from './styled';
import {createPortal} from 'react-dom';
import useGlobal from 'store';
import {useLazyQuery} from '@apollo/client';
import {FEED_VARIATIONS_POPUP} from 'queries';
import {getVariationByAttrs} from 'util/heplers';
import {useVariations} from 'hooks';
import Loader from 'components/Loader';
import PopUpContent from 'components/QuickView/mobile/Info/components/PopUpContent';
import {CSSTransition} from 'react-transition-group';

const ProductVariations = () => {
  const [globalState, globalActions] = useGlobal();
  const {active, productId} = globalState?.showVariationsPopup || {};
  const [get, {data, loading}] = useLazyQuery(FEED_VARIATIONS_POPUP, {variables: {id: productId}});
  const [vars, varState, setVarState] = useVariations(data?.product);
  const variation = getVariationByAttrs(data?.product, varState);
  const {databaseId, variations, name, coins} = data?.product || {};

  useEffect(() => {
    if (productId && !data && !loading) get();
  }, [productId, get, data, loading]);

  function renderPopup() {
    if (loading) {
      return <Loader wrapperWidth="100%" wrapperHeight="auto" />;
    }

    return (
      <>
        <PopUpContent
          productId={productId}
          data={data}
          vars={vars}
          name={name}
          coins={coins}
          variations={variations}
          varState={varState}
          setVarState={setVarState}
          variation={variation}
        />
        <Buttons
          databaseId={databaseId}
          variation={variation}
          disabled={variations && !variation}
          style={{marginTop: 0}}
        />
      </>
    );
  }

  return createPortal(
    <CSSTransition in={active} timeout={300} unmountOnExit>
      <Popup>
        <PopupOverlay onClick={() => globalActions.setShowVariationsPopup({active: false, productId})} />
        <PopupContent>
          <IconWrapper onClick={() => globalActions.setShowVariationsPopup({active: false, productId})}>
            <Icon type="close" svgStyle={{width: 24, height: 24, color: '#1A1A1A'}} />
          </IconWrapper>
          {renderPopup()}
        </PopupContent>
      </Popup>
    </CSSTransition>,
    document.getElementById('modal')
  );
};

export default ProductVariations;
