import React, {useState} from 'react';
import {object} from 'prop-types';
import {useHistory} from 'react-router-dom';

import {IconPreloader} from 'helpers/icons';

import {Wrapper, Button} from './styled';
import useGlobal from 'store';
import {useMutation} from '@apollo/client';
import {ADD_TO_CART} from 'mutations';
import {getCheckoutLink} from 'util/heplers';
import {useUser} from 'hooks/reactiveVars';

const Buttons = ({style, variation, databaseId}) => {
  const [user] = useUser();
  const [globalState, globalActions] = useGlobal();
  const {purchaseSource} = globalState?.showVariationsPopup || {};
  const [addedToCart, setAddedToCart] = useState(false);

  const [addToCart, {loading}] = useMutation(ADD_TO_CART, {
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart() {
            return data.addToCart.cart;
          }
        }
      });
    },
    onCompleted() {
      setAddedToCart(true);
    }
  });

  const [buyNow] = useMutation(ADD_TO_CART, {
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart() {
            return data.addToCart.cart;
          }
        }
      });
    },
    onCompleted(data) {
      globalActions.setShowVariationsPopup({active: false});
      history.push(getCheckoutLink(data?.addToCart?.cart?.subcarts?.subcarts, user));
    }
  });

  const history = useHistory();

  function onAdd() {
    if (!addedToCart) {
      globalActions.setShowProductAdded(true);
      addToCart({
        variables: {
          input: {
            productId: databaseId,
            quantity: 1,
            variationId: variation?.databaseId,
            purchase_source: purchaseSource
          }
        }
      });
    } else {
      globalActions.setShowVariationsPopup({active: false});
      history.push('/cart');
    }
  }

  return (
    <Wrapper style={style}>
      <Button
        style={{
          position: 'relative'
        }}
        onClick={() => onAdd()}
      >
        {loading ? (
          <IconPreloader
            width={16}
            height={16}
            style={{
              position: 'absolute',
              top: '50%',
              left: '50%',
              width: 20,
              height: 20,
              transform: 'translate(-50%, -50%)'
            }}
          />
        ) : addedToCart ? (
          'Go to Cart'
        ) : (
          'Add to Cart'
        )}
      </Button>
      <Button
        type="primary"
        onClick={() =>
          buyNow({
            variables: {
              input: {
                productId: databaseId,
                quantity: 1,
                variationId: variation?.databaseId,
                purchase_source: purchaseSource
              }
            }
          })
        }
      >
        Buy Now
      </Button>
    </Wrapper>
  );
};

Buttons.defaultProps = {
  style: {}
};

Buttons.propTypes = {
  style: object
};

export default Buttons;
