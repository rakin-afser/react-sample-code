import styled, {css} from 'styled-components/macro';

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: stretch;
  padding: 15vh 0 16px;
  background-color: rgba(0, 0, 0, 0.45);
  z-index: 1001;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  cursor: default;
  overflow: hidden;
  opacity: 0;
  transition: 0.5s;

  &.mobileModal-enter-active {
    opacity: 1;
  }

  &.mobileModal-enter-done {
    opacity: 1;
  }

  ${({rightAlignModal}) =>
    rightAlignModal &&
    css`
      justify-content: flex-end;
      padding: 0;
    `}
`;

export const Modal = styled.div`
  background: #ffffff;
  display: flex;
  flex-direction: column;

  ${({styles}) =>
    !!styles &&
    css`
      border-radius: ${styles.borderRadius || '8px'};
      width: ${styles.width || 'calc(100% - 20px)'};
    `}

  ${({theme: {isArabic}}) =>
    isArabic &&
    css`
      direction: rtl;
    `}

  ${({rightAlignModal}) =>
    rightAlignModal &&
    css`
      border-radius: 18px 0 0 18px;
      width: calc(100% - 52px);
      max-width: 88%;
    `}
`;
