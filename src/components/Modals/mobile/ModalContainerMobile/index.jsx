import React, {useRef} from 'react';
import {createPortal} from 'react-dom';
import {CSSTransition} from 'react-transition-group';

import useOutsideClick from 'util/useOutsideClick';
import {Wrapper, Modal} from 'components/Modals/mobile/ModalContainerMobile/styled';

const ModalContainer = ({isOpen = false, onClose = () => {}, children, styles = {}, rightAlignModal}) => {
  const modalRef = useRef();

  useOutsideClick(modalRef, () => {
    if (isOpen) {
      //useOutsideClick working incorrect with ant dropdowns
      //and if clicked element remove immediately. Need to fix
      onClose();
    }
  });

  return createPortal(
    <CSSTransition in={isOpen} timeout={500} classNames="mobileModal" unmountOnExit>
      <Wrapper rightAlignModal={rightAlignModal}>
        <Modal ref={modalRef} styles={styles} rightAlignModal={rightAlignModal}>
          {children}
        </Modal>
      </Wrapper>
    </CSSTransition>,
    document.getElementById('modal')
  );
};

export default ModalContainer;
