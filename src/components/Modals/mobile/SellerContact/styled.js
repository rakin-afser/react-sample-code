import {Badge, Button} from 'antd';
import {primaryColor} from 'constants/colors';
import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';
import Icon from 'components/Icon';

export const Avatar = styled.img`
  border-radius: 50%;
  border: 1px solid #efefef;
  width: 58px;
  height: 58px;
`;

export const Info = styled.div`
  padding-top: 8px;
  padding-left: 16px;
  padding-right: 24px;
`;

export const Name = styled.p`
  margin-bottom: 4px;
  font-size: 18px;
  line-height: 1;
  font-weight: 500;
  color: #000;
`;

export const Status = styled.span`
  color: #999;
`;

export const Seller = styled.div`
  padding-top: 16px;
  display: flex;
  position: relative;
`;

export const StyledIcon = styled(Icon)`
  position: absolute;
  top: 24px;
  right: 0;
`;

export const StatusBadge = styled(Badge)`
  .ant-badge-dot {
    border: 2px solid #ffffff;
    background: ${({status}) => (status === 'online' ? '#2ecc71' : '#C3C3C3')};
    width: 14px;
    height: 14px;
  }
`;

export const Wrap = styled.div`
  padding-top: 16px;
  padding-bottom: 8px;
  position: relative;
  margin-bottom: 16px;

  input {
    color: #1f6bc4;
  }

  &::after {
    content: '';
    background-color: #fafafa;
    height: 8px;
    position: absolute;
    right: -16px;
    left: -16px;
    bottom: 0;
  }
`;

export const Footer = styled.div`
  padding: 16px;
  margin: 32px -16px 0;
  box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.03);
  display: flex;
  justify-content: space-between;
`;

export const StyledBtn = styled(Button)`
  &&& {
    height: 40px;
    border-radius: 100px;
    font-weight: 500;
  }
`;

export const Cancel = styled(StyledBtn)`
  &&& {
    border: none;
    color: #000;
    max-width: 120px;
    width: 100%;
    margin-left: 19px;
  }
`;

export const Submit = styled(StyledBtn)`
  &&& {
    color: #fff;
    border-color: ${primaryColor};
    background-color: ${primaryColor};
    max-width: 186px;
    width: 100%;
  }
`;

export const Text = styled.p`
  font-size: 12px;
  font-family: ${secondaryFont};
  line-height: 1.333;
  margin-bottom: 16px;

  a {
    color: ${primaryColor};
    text-decoration: underline;
  }
`;
