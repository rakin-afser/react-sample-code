import AreaField from 'components/AreaField';
import CustomUpload from 'components/CustomUpload';
import Input from 'components/Input';
import React from 'react';
import {Link} from 'react-router-dom';
import useGlobal from 'store';
import BackPage from '../BackPage';
import {
  Wrap,
  StatusBadge,
  Footer,
  Cancel,
  Submit,
  Info,
  Text,
  Seller,
  Avatar,
  Name,
  Status,
  StyledIcon
} from './styled';
import avatar from 'images/avatar3.png';

const SellerContact = () => {
  const [globalState, globalActions] = useGlobal();

  return (
    <BackPage
      iconRight
      title="Contact Seller"
      active={globalState.sellerContact}
      setActive={globalActions.setSellerContact}
    >
      <Seller>
        <StatusBadge status="online" size="large" offset={[-9, 49]}>
          <Avatar src={avatar} alt="avatar" />
        </StatusBadge>
        <Info>
          <Name>Adidas Original</Name>
          <Status>online</Status>
        </Info>
        <StyledIcon type="dots" width={24} height={24} />
      </Seller>
      <Wrap>
        <Input label="Order Number" value="#5327352254" readOnly />
        <AreaField size="large" counter label="Message to Seller" />
      </Wrap>
      <div>
        <Text>
          You can add up to 10 photos to share wih your seller. After you start a return, thes photos can’t be removed.
          <Link to="/">Learn more</Link>
        </Text>
        <CustomUpload />
      </div>
      <Footer>
        <Cancel onClick={() => globalActions.setSellerContact(false)}>Cancel</Cancel>
        <Submit onClick={() => globalActions.setSellerContact(false)}>Submit Request</Submit>
      </Footer>
    </BackPage>
  );
};

export default SellerContact;
