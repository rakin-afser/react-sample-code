import {Button} from 'antd';
import {secondaryFont} from 'components/Header/constants';
import {primaryColor} from 'constants/colors';
import styled from 'styled-components';

export const Wrap = styled.div`
  padding-top: 16px;

  &:first-child {
    padding-bottom: 0;
  }

  & + & {
    padding-top: 24px;
    padding-bottom: 8px;
    position: relative;

    &::after,
    &::before {
      content: '';
      position: absolute;
      height: 8px;
      background-color: #fafafa;
      left: -16px;
      right: -16px;
    }

    &::before {
      top: 0;
    }

    &::after {
      bottom: 0;
    }
  }

  label {
    color: #000;
  }
`;

export const Text = styled.p`
  font-size: 12px;
  font-family: ${secondaryFont};
  line-height: 1.333;
  margin-bottom: 16px;

  a {
    color: ${primaryColor};
    text-decoration: underline;
  }
`;

export const Title = styled.h5`
  font-weight: 700;
  font-size: 16px;
  margin-bottom: 16px;
`;

export const Seller = styled.span`
  color: #1f6bc4;
  font-weight: 700;
`;

export const Summary = styled.div`
  color: #545454;
  margin-bottom: 4px;

  p {
    margin-bottom: 12px;
  }
`;

export const MainText = styled.span`
  font-weight: 500;
  color: #000;
`;

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Footer = styled.div`
  padding: 16px;
  margin: 0 -16px;
  box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.03);
  display: flex;
  justify-content: space-between;
`;

export const StyledBtn = styled(Button)`
  &&& {
    height: 40px;
    border-radius: 100px;
    font-weight: 500;
  }
`;

export const Cancel = styled(StyledBtn)`
  &&& {
    border: none;
    color: #000;
    max-width: 120px;
    width: 100%;
    margin-left: 19px;
  }
`;

export const Submit = styled(StyledBtn)`
  &&& {
    color: #fff;
    border-color: ${primaryColor};
    background-color: ${primaryColor};
    max-width: 186px;
    width: 100%;
  }
`;
