import AreaField from 'components/AreaField';
import SelectField from 'components/SelectField';
import React from 'react';
import {Link} from 'react-router-dom';
import useGlobal from 'store';
import BackPage from '../BackPage';
import {reasons} from './exampleData';
import {Wrap, Text, Title, Seller, Summary, MainText, Row, Footer, Cancel, Submit} from './styled';
import CustomUpload from 'components/CustomUpload';
import ProdDetail from 'components/ProdDetail/mobile';

const ReturnRequest = () => {
  const [globalState, globalActions] = useGlobal();

  return (
    <BackPage
      iconRight
      active={globalState.returnRequest.open}
      setActive={globalActions.setReturnRequest}
      title="Request a Return"
    >
      <Wrap>
        <SelectField
          defaultValue={reasons[0].value}
          options={reasons}
          required
          label="Why do you want to return this item?"
        />
        <AreaField required counter size="large" label="Add Details" />
      </Wrap>
      <Wrap>
        <Text>
          You can add up to 10 photos to share wih your seller. After you start a return, thes photos can’t be removed.
          <Link to="/">Learn more</Link>
        </Text>
        <CustomUpload />
        <Title>Refund summary:</Title>
        <Summary>
          <p>
            Sold by: <Seller>Best Seller</Seller>
          </p>
          <Row>
            <p>
              Order id: <MainText>09876YO</MainText>
            </p>
            <p>
              Items: <MainText>1</MainText>
            </p>
          </Row>
        </Summary>
      </Wrap>
      <ProdDetail data={globalState?.returnRequest?.data} />
      <Footer>
        <Cancel onClick={() => globalActions.setReturnRequest(false)}>Cancel</Cancel>
        <Submit onClick={() => globalActions.setReturnRequest(false)}>Submit Request</Submit>
      </Footer>
    </BackPage>
  );
};

export default ReturnRequest;
