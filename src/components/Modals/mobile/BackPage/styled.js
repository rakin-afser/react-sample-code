import styled from 'styled-components/macro';

export const PopupBg = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1004;
  background: rgba(0, 0, 0, 0.55);
  transition: all 0.3s ease;

  opacity: ${({active}) => (active ? 1 : 0)};
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
`;

export const PopupWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: #fff;
  display: flex;
  flex-direction: column;
  max-width: 100vw;
  z-index: 1005;
  overflow: hidden;

  ${({type}) =>
    type === 'comments' || type === 'replies'
      ? `
        max-height: 75vh;
        margin: auto 10px 10px;
        border-radius: 12px;
      `
      : ''}
`;

export const IconLeft = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  z-index: 110;
  cursor: pointer;
  width: 60px;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const IconRight = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  z-index: 110;
  cursor: pointer;
  width: 60px;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 60px;
  border-bottom: 1px solid #d8d8d8;
`;

export const Title = styled.h3`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #000000;
  margin: auto;
  display: flex;
  align-items: center;

  ${({type}) => (type === 'comments' ? 'margin: auto auto auto 22px;' : '')}

  ${({type}) => (type === 'replies' ? 'margin: auto auto auto 62px;' : '')}

  .count {
    width: 29px;
    height: 28px;
    border-radius: 50%;
    background: rgba(237, 73, 79, 0.79);
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 28px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #ffffff;
    margin-left: 9px;
  }
`;

export const Content = styled.div`
  overflow-y: auto;
  padding: 0 16px;
`;
