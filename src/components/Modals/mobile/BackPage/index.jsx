import React, {forwardRef} from 'react';
import Icon from 'components/Icon';
import {PopupWrapper, PopupBg, Header, Title, IconLeft, IconRight, Content} from './styled';
import {createPortal} from 'react-dom';
import {Transition} from 'react-transition-group';

const BackPage = forwardRef(
  ({children, title = 'Title', active, contentStyle, setActive, onClose, iconLeft = true, iconRight = true, type = ''}, ref) => {
    const duration = 300;
    const transitionStyles = {
      entering: {
        opacity: 1,
        transform: 'translateY(0)',
        transition: `opacity ${duration}ms ease-in-out, transform ${duration}ms ease-in-out`
      },
      exiting: {
        opacity: 0,
        transform: 'translateY(-50px)',
        transition: `opacity ${duration}ms ease-in-out, transform ${duration}ms ease-in-out`
      }
    };

    function onEnter(node) {
      node.style.setProperty('opacity', '0');
      node.style.setProperty('transform', 'translateY(-50px)');
      return node.offsetHeight;
    }

    function onEntered() {
      document.body.classList.add('overflow-hidden');
    }

    function onExited() {
      document.body.classList.remove('overflow-hidden');
    }

    return createPortal(
      <>
        {
          type === 'comments' || type === 'replies' 
          ? <PopupBg active={active} onClick={() => onClose(false)}/> 
          : null
        }
        <Transition
          in={active}
          timeout={duration}
          unmountOnExit
          onEnter={onEnter}
          onEntered={onEntered}
          onExited={onExited}
        >
          {(state) => (
            <>
              <PopupWrapper style={transitionStyles?.[state]} type={type}>
                <Header type={type}>
                  {iconLeft && type !== 'comments' && (
                    <IconLeft onClick={() => setActive(false)}>
                      <Icon type="arrowBack" svgStyle={{stroke: '#1A1A1A'}} />
                    </IconLeft>
                  )}
                  <Title type={type} dangerouslySetInnerHTML={{__html: title}}/>
                  {iconRight && (
                    <IconRight onClick={() => onClose(false)}>
                      <Icon type="crossThin" fill="#1A1A1A"/>
                    </IconRight>
                  )}
                </Header>
                <Content style={contentStyle} ref={ref}>
                  {children}
                </Content>
              </PopupWrapper>
            </>
          )}
        </Transition>
      </>,
      document.getElementById('modal')
    );
  }
);

export default BackPage;
