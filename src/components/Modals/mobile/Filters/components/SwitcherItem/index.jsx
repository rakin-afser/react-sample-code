import React from 'react';

import {FilterItem, FilterItemWrapper} from 'components/Modals/mobile/Filters/styled';
import Switcher from 'components/Buttons/Switcher';

const SwitcherItem = ({title, onClick, isActive, index, bold}) => (
  <FilterItemWrapper key={index} onClick={onClick}>
    <FilterItem bold={bold}>
      {title}
      <Switcher isActive={isActive} />
    </FilterItem>
  </FilterItemWrapper>
);

export default SwitcherItem;
