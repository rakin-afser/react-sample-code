import React from 'react';
import _ from 'lodash';

import {FilterItem, FilterItemWrapper} from 'components/Modals/mobile/Filters/styled';
import SelectedFilters from 'components/Modals/mobile/Filters/components/SelectedFilters';
import Checkmark from 'assets/Checkmark';
import Arrow from 'assets/Arrow';

const SimpleItem = ({
  filtersValue,
  title,
  count,
  onClick,
  isActive,
  selected,
  index,
  bold,
  isExpands,
  selectedChildren,
  type,
  children
}) => (
  <FilterItemWrapper key={index} selected={selected} isActive={isActive} onClick={onClick}>
    <FilterItem bold={bold}>
      {children}
      {title}&nbsp;<span>{!!count && `/ ${count}`}</span>
      {isExpands && <Arrow color="black" width={8} />}
      {selected && <Checkmark width={20} />}
    </FilterItem>
    {!_.isEmpty(selectedChildren) && (
      <SelectedFilters filtersValue={filtersValue} type={type} items={selectedChildren} />
    )}
  </FilterItemWrapper>
);

export default SimpleItem;
