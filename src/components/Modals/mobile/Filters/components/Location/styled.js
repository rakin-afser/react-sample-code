import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  padding: 0 27px;

  & > div {
    margin-bottom: 21px;

    .ant-select-selection__placeholder {
      color: #898989;
      line-height: 1.5;
    }

    .ant-select-selection__rendered {
      padding: 10px 17px;
      height: 40px;
    }

    .ant-select-selection-selected-value {
      line-height: 1.5;
    }

    .ant-select-arrow {
      right: 16px;
    }

    & label {
      font-family: Helvetica Neue;
      font-size: 14px;
      color: #000000;
      margin-bottom: 8px;
    }
  }
`;
