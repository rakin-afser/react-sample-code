import React, {useState} from 'react';
import {Select} from 'antd';

import {Wrapper} from 'components/Modals/mobile/Filters/components/Location/styled';
import SelectField from 'components/SelectField';
import {country} from 'constants/staticData';

const {Option} = Select;

const Location = ({value = {}, onSearch = () => {}, onChange = () => {}}) => {
  return (
    <Wrapper>
      <SelectField
        label="Select Country"
        value={value?.country}
        placeholder="Your Country"
        onSearch={onSearch}
        onChange={(value) => onChange('country', value)}
      >
        {Object.keys(country).map((key) => (
          <Option value={key} key={key}>
            {key}
          </Option>
        ))}
      </SelectField>
      <SelectField
        label="Select City"
        value={value?.city}
        placeholder="Your City"
        onSearch={onSearch}
        onChange={(value) => onChange('city', value)}
      >
        {Object.keys(country).map((key) => (
          <Option value={key} key={key}>
            {key}
          </Option>
        ))}
      </SelectField>
    </Wrapper>
  );
};

export default Location;
