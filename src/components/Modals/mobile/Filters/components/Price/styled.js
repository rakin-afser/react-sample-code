import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding: 0 27px;

  & .ant-slider-rail {
    background: #efefef;
    border-radius: 29px;
    height: 3px;
  }

  & .ant-slider-track {
    background: #efefef;
  }

  & .ant-slider:hover .ant-slider-track {
    background: #efefef;
  }

  & .ant-slider-handle {
    margin-top: -7px;
    width: 18px;
    height: 18px;
    background: #666666;
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.04);
    border-radius: 2px;
    border: none;
    transition: background 0.5s;

    &::before {
      position: absolute;
      top: 4px;
      left: 4px;
      content: '';
      border-radius: 50%;
      width: 10px;
      height: 10px;
      background: #fff;
    }

    &::after {
      position: absolute;
      bottom: -20px;
      content: '';
      border-radius: 50%;
      width: 10px;
      height: 10px;
      background: #fff;
    }
  }

  & .ant-slider-handle:focus {
    box-shadow: none;
  }

  ${({isActive}) =>
    isActive &&
    `
    & .ant-slider-handle {
      background: ${primaryColor};
    }
  `}
`;

export const PriceContainer = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 16px 10px 0;
`;

export const Text = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #666666;
`;
