import React, {useState} from 'react';
import {Slider} from 'antd';

import {Wrapper, PriceContainer, Text} from 'components/Modals/mobile/Filters/components/Price/styled';

const Price = ({value = [], onChange = () => {}, min = 10, max = 11000, type = '$'}) => {
  const [[lowPrice, highPrice], setPrice] = useState([value[0] || min, value[1] || max]);

  const handleChangeSlider = (values) => {
    setPrice(values);
  };

  return (
    <Wrapper isActive={value?.[0] > min || value?.[1] < max}>
      <Slider
        min={min}
        max={max}
        range={true}
        defaultValue={[min, max]}
        value={[lowPrice, highPrice]}
        onAfterChange={onChange}
        onChange={handleChangeSlider}
      />
      <PriceContainer>
        <Text>
          {type}
          {value?.[0] || min}
        </Text>
        <Text>
          {type}
          {value?.[1] || max}
        </Text>
      </PriceContainer>
    </Wrapper>
  );
};

export default Price;
