import React from 'react';
import _ from 'lodash';

import {SelectedFiltersWrapper} from 'components/Modals/mobile/Filters/styled';
import getAttribute from 'components/Modals/mobile/Filters/utils/getAttribute';

const SelectedFilters = ({filtersValue, items, type}) => {
  const separator = type === 'price' ? ' - ' : ' | ';

  const {data} = getAttribute(filtersValue) || {};
  const currentAttribute = data.find((i) => i?.slug === type) || {};

  if (type === 'price')
    return (
      <SelectedFiltersWrapper>
        {_.values(items)?.map((item, index) => `${index ? separator : ''}${item}`)}
      </SelectedFiltersWrapper>
    );

  return (
    <SelectedFiltersWrapper>
      {_.isEmpty(currentAttribute?.options)
        ? _.values(items)?.map((item, index) => `${index ? separator : ''}${_.capitalize(item.replace(/-/g, ' '))}`)
        : currentAttribute?.options
            ?.filter((i) => filtersValue?.[type].includes(i.slug))
            ?.map((el, index) => `${index ? separator : ''}${el?.name}`)}
    </SelectedFiltersWrapper>
  );
};

export default SelectedFilters;
