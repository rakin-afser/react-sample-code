import React, {useState, useEffect} from 'react';
import _ from 'lodash';
import {useLocation, useHistory} from 'react-router-dom';
import qs from 'qs';
import {useQuery} from '@apollo/client';

import useGlobal from 'store';
import ArrowBack from 'assets/ArrowBack';
import Loader from 'components/Loader';
import {
  ModalHeader,
  Title,
  ResetFilter,
  BackButton,
  ModalContent,
  ModalFooter,
  SaveButton,
  SearchWrapper,
  Label,
  Cancel
} from 'components/Modals/mobile/Filters/styled';
import useFilters from 'util/useFilters';
import {StyledModal} from 'components/Modals/mobile/Filters/ProductFilters/styled';
import SearchField from 'components/SearchField/Mobile';
import SimpleItem from 'components/Modals/mobile/Filters/components/SimpleItem';
import {getCurrentAlfabet} from 'components/Modals/mobile/Filters/utils/helpers';
import {GET_TAGS} from 'components/CreatePost/api/queries';
import {storesSortByOptions, postsSortByOptions} from 'constants/sorting';

const StoresFilters = ({type}) => {
  const history = useHistory();
  const {pathname, search} = useLocation();
  const [attributeSearch, setAttributeSearch] = useState('');
  const [globalState, setGlobalState] = useGlobal();
  const [isOpen, setIsOpen] = useState(false);
  const {search: searchValue, filters: filtersValue} = qs.parse(search.replace('?', '')) || {};
  const {filters, changeFilters, isItemSelected, clearFilter, clearAll, reloadFilters} = useFilters(filtersValue);
  const [currentTab, setCurrentTab] = useState(null);
  const {data: tagsData, loading: tagsLoading, error: tagsError} = useQuery(GET_TAGS, {
    variables: {search: attributeSearch, first: 100}
  }); // todo: add pagination

  const simpleData = {
    hashtags: {
      id: 'hashtags',
      loading: tagsLoading,
      name: 'Hashtags',
      children: tagsData?.tags?.nodes?.map((item) => ({name: item?.name, slug: item?.slug})) || []
    },
    postType: {
      id: 'postType',
      name: 'Post Type',
      unic: true,
      children: [
        {slug: 'common', name: 'Common Posts'},
        {slug: 'feedback', name: 'Feedback Posts'}
      ]
    },
    sortBy: {
      id: 'sortBy',
      name: 'Sort By',
      unic: true,
      children:
        type === 'posts'
          ? postsSortByOptions.map((item) => ({slug: item.value, name: item.name}))
          : storesSortByOptions.map((item) => ({slug: item.value, name: item.name}))
    }
  };

  useEffect(() => {
    setIsOpen(globalState.filterModal);
  }, [globalState.filterModal]);

  useEffect(() => {
    setAttributeSearch('');
  }, [currentTab]);

  useEffect(() => {
    reloadFilters(filtersValue);
  }, [search]);

  const backButtonClick = () => {
    setCurrentTab(null);
  };

  const footerLinkClick = (type) => {
    switch (type) {
      case 'cancel':
        onClose();
        break;
      case 'sizeGuide':
      default:
        setGlobalState.setShowSizeGuide(true);
    }
  };

  const onClose = () => {
    setGlobalState.setFilterModal(false, {type: 'products'});
  };

  const onSubmit = () => {
    const queryString = qs.stringify({filters, search: searchValue}); // save search
    history.push(`${pathname}?${queryString}`);
    setGlobalState.setFilterModal(false);
  };

  const getTabContent = () => {
    if (simpleData?.[currentTab]?.loading) return <Loader wrapperWidth="100%" wrapperHeight="100px" dotsSize="10px" />;
    switch (currentTab) {
      case 'hashtags':
        return getCurrentAlfabet(simpleData?.[currentTab]?.children)?.map((item) => (
          <>
            <Label>{item}</Label>
            {simpleData?.[currentTab]?.children
              ?.filter((i) => i.name[0].toLowerCase() === item.toLowerCase())
              ?.filter((item) => item?.name.toLowerCase().includes(attributeSearch.toLowerCase()))
              .map((subItem, index) => (
                <SimpleItem
                  filtersValue={filters}
                  type={simpleData?.[currentTab]?.id}
                  key={index}
                  title={subItem?.name}
                  selected={isItemSelected(simpleData?.[currentTab].id, subItem.slug)}
                  isActive={isItemSelected(simpleData?.[currentTab].id, subItem.slug)}
                  onClick={() => changeFilters(simpleData?.[currentTab], subItem.slug)}
                />
              ))}
          </>
        ));
      default:
        return simpleData?.[currentTab]?.children
          ?.filter((item) => item?.name.toLowerCase().includes(attributeSearch.toLowerCase()))
          ?.map((item, index) => (
            <SimpleItem
              filtersValue={filters}
              type={simpleData?.[currentTab]?.id}
              key={index}
              title={item?.name}
              selected={isItemSelected(simpleData?.[currentTab].id, item.slug)}
              isActive={isItemSelected(simpleData?.[currentTab].id, item.slug)}
              onClick={() => changeFilters(simpleData?.[currentTab], item.slug)}
            />
          ));
    }
  };

  return (
    <StyledModal
      visible={isOpen}
      onCancel={onClose}
      style={{
        top: 0,
        right: 0,
        display: 'flex',
        padding: 0,
        height: '100vh',
        margin: '0 0 0 auto',
        justifyContent: 'flex-end',
        bottom: 0
      }}
      footer={null}
      destroyOnClose
    >
      <ModalHeader>
        {!!currentTab && (
          <BackButton onClick={backButtonClick}>
            <ArrowBack stroke="#000" />
          </BackButton>
        )}
        <Title>{simpleData?.[currentTab]?.name || 'Sort & Filter'}</Title>
        {!_.isEmpty(filters?.[simpleData?.[currentTab]?.id]) &&
          !['location', 'price', ''].includes(simpleData?.[currentTab]?.id) && (
            <ResetFilter onClick={() => clearFilter(simpleData?.[currentTab]?.id)}>
              Reset Filter ({_.size(filters?.[simpleData?.[currentTab]?.id])})
            </ResetFilter>
          )}
        {!_.isEmpty(filters) && !currentTab && <ResetFilter onClick={clearAll}>Reset All</ResetFilter>}
        {!_.isEmpty(filters?.[simpleData?.[currentTab]?.id]) && ['location', 'price'].includes(currentTab) && (
          <ResetFilter onClick={() => clearFilter(simpleData?.[currentTab]?.id)}>
            Reset {simpleData?.[currentTab]?.name}
          </ResetFilter>
        )}
      </ModalHeader>
      <ModalContent>
        {['hashtags'].includes(currentTab) && (
          <SearchWrapper>
            <SearchField
              onReset={() => setAttributeSearch('')}
              onChange={(e) => setAttributeSearch(e.target.value)}
              searchValue={attributeSearch}
              placeholder={`Search for ${simpleData?.[currentTab]?.name}`}
            />
          </SearchWrapper>
        )}
        {currentTab ? (
          getTabContent()
        ) : (
          <>
            {/* Post Type */}
            {type === 'posts' && (
              <SimpleItem
                isExpands
                filtersValue={filters}
                type={simpleData?.postType?.id}
                title={simpleData?.postType?.name}
                bold
                onClick={() => setCurrentTab('postType')}
                isActive={!_.isEmpty(filters?.[simpleData?.postType?.id])}
                selectedChildren={filters?.[simpleData?.postType?.id]}
              />
            )}
            {/* Hashtags */}
            <SimpleItem
              isExpands
              filtersValue={filters}
              type={simpleData?.hashtags?.id}
              title={simpleData?.hashtags?.name}
              bold
              onClick={() => setCurrentTab('hashtags')}
              isActive={!_.isEmpty(filters?.[simpleData?.hashtags?.id])}
              selectedChildren={filters?.[simpleData?.hashtags?.id]}
            />
            {/* Sort By */}
            <SimpleItem
              isExpands
              filtersValue={filters}
              type={simpleData?.sortBy?.id}
              title={simpleData?.sortBy?.name}
              bold
              onClick={() => setCurrentTab('sortBy')}
              isActive={!_.isEmpty(filters?.[simpleData?.sortBy?.id])}
              selectedChildren={filters?.[simpleData?.sortBy?.id]}
            />
          </>
        )}
      </ModalContent>
      <ModalFooter>
        <Cancel onClick={() => footerLinkClick('cancel')}>Cancel</Cancel>
        <SaveButton onClick={onSubmit}>Save & Close</SaveButton>
      </ModalFooter>
    </StyledModal>
  );
};

export default StoresFilters;
