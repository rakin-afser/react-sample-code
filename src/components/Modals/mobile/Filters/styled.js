import styled from 'styled-components/macro';

export const ModalHeader = styled.div`
  display: flex;
  padding: 25px 24px 20px 24px;
  position: relative;
  justify-content: center;
  line-height: normal;
`;

export const Title = styled.div`
  font-family: Helvetica Neue;
  font-weight: 500;
  font-size: 18px;
  color: #000000;
  max-width: 53%;
  text-align: center;
`;

export const BackButton = styled.div`
  position: absolute;
  transform: translateY(-2px);
  left: 20px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
      transform: scale(-1,1) translateY(-2px);
      `}
`;

export const ResetFilter = styled.div`
  font-family: Helvetica Neue;
  font-size: 11px;
  color: #000000;
  position: absolute;
  right: 24px;
  line-height: 2.5;
`;

export const FilterItem = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: ${({bold}) => (bold ? '500' : '400')};
  font-size: 16px;
  color: #000000;
  line-height: normal;
  display: flex;
  align-items: center;
  justify-content: space-between;

  & span {
    display: inline-block;
    flex-grow: 1;
    font-family: Helvetica Neue;
    font-weight: normal;
    font-size: 11px;
    color: #999999;
  }
`;

export const FilterItemWrapper = styled.div`
  padding: 16px 25px 16px 21px;
  border-top: 2px solid #fafafa;
  display: flex;
  flex-direction: column;
  transition: background-color 0.3s;

  ${({selected}) =>
    selected &&
    `
    background-color:#f7f7f7;
    & svg path{
      fill: #000;
      height="auto"
    }
  `}

  ${({isActive}) =>
    isActive &&
    `
    background-color:#f7f7f7;
  `}
`;

export const ModalContent = styled.div`
  overflow-y: auto;
  flex-grow: 1;
`;

export const ModalFooter = styled.div`
  display: flex;
  padding: 5px 24px 23px;
  justify-content: flex-end;
  align-items: center;
`;

export const SaveButton = styled.button`
  background: #ffffff;
  border: 1px solid #666;
  border-radius: 24px;
  color: #666;
  line-height: normal;
  padding: 6px 12px 7px 12px;
  font-family: Helvetica Neue;
  font-weight: 500;
  font-size: 14px;
`;

export const SelectedFiltersWrapper = styled.div`
  font-family: Helvetica Neue;
  font-size: 11px;
  color: #666;
  padding-top: 10px;
`;

export const SearchWrapper = styled.div`
  margin: 0 22px 14px;
`;

export const Label = styled.div`
  background: #f1f7fd;
  padding: 9px 16px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #000000;
`;

export const FooterLink = styled.div`
  flex-grow: 1;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #000;
`;

export const Cancel = styled.div`
  flex-grow: 1;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #999;
  padding: 0 12px;
`;

export const BackItem = styled.div`
  display: flex;
  align-items: center;
  & svg {
    transform: rotate(180deg);
    margin-right: 15px;
    margin-bottom: 3px;
  }
`;
