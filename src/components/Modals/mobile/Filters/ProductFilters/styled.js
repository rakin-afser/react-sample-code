import styled, {css} from 'styled-components/macro';
import {Modal} from 'antd';

export const StyledModal = styled(Modal)`
  & .ant-modal-content {
    background: #fff;
    border-radius: 18px 0px 0px 18px;
    width: calc(100% - 52px);
    max-width: 88%;
    margin: 0;
    overflow: hidden;
  }

  & .ant-modal-body {
    display: flex;
    flex-direction: column;
    padding: 0;
    height: 100%;
    margin: 0;
  }

  & .ant-modal-close {
    display: none;
  }
`;
