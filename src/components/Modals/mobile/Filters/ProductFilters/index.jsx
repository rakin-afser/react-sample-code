import React, {useState, useEffect} from 'react';
import _ from 'lodash';
import tinycolor from 'tinycolor2';
import {useLocation, useHistory} from 'react-router-dom';
import qs from 'qs';

import useGlobal from 'store';
import ArrowBack from 'assets/ArrowBack';
import Loader from 'components/Loader';
import Select from 'components/SelectTrue';
import {
  ModalHeader,
  Title,
  ResetFilter,
  BackButton,
  ModalContent,
  ModalFooter,
  SaveButton,
  SearchWrapper,
  Label,
  FooterLink,
  Cancel,
  BackItem
} from 'components/Modals/mobile/Filters/styled';
import useFilters from 'util/useFilters';
import {StyledModal} from 'components/Modals/mobile/Filters/ProductFilters/styled';
import SearchField from 'components/SearchField/Mobile';
import Price from 'components/Modals/mobile/Filters/components/Price';
import getFiltersData from 'components/Modals/mobile/Filters/utils/getFiltersData';
import Icon from 'components/Icon';
import {Circle} from 'components/Modals/Filters/components/ColorsBlock/styled';
import SimpleItem from 'components/Modals/mobile/Filters/components/SimpleItem';
import SwitcherItem from 'components/Modals/mobile/Filters/components/SwitcherItem';
import {getCurrentAlfabet} from 'components/Modals/mobile/Filters/utils/helpers';

const getFilter = (item, filters, setCurrentTab, isItemSelected, changeFilters) => {
  const filterId = item?.slug;

  switch (item?.slug) {
    default: {
      return (
        <SimpleItem
          isExpands
          filtersValue={filters}
          type={filterId}
          title={item?.name}
          bold
          onClick={() => setCurrentTab(item?.slug)}
          isActive={!_.isEmpty(filters?.[filterId])}
          selectedChildren={filters?.[filterId]}
        />
      );
    }
  }
};

const ProductFilters = () => {
  const history = useHistory();
  const {pathname, search} = useLocation();
  const [attributeSearch, setAttributeSearch] = useState('');
  const [globalState, setGlobalState] = useGlobal();
  const [isOpen, setIsOpen] = useState(false);
  const [currentFilter, setCurrentFilter] = useState({});
  const {search: searchValue, filters: filtersValue} = qs.parse(search.replace('?', '')) || {};
  const {filters, changeFilters, isItemSelected, clearFilter, clearAll, reloadFilters} = useFilters(filtersValue);
  const [currentTab, setCurrentTab] = useState(null);
  const {data: simpleData, attributes = [], loading, filtersData} = getFiltersData(filters);
  const {categoriesData} = filtersData;

  useEffect(() => {
    setIsOpen(globalState.filterModal);
  }, [globalState.filterModal]);

  useEffect(() => {
    setAttributeSearch('');
  }, [currentTab]);

  useEffect(() => {
    reloadFilters(filtersValue);
  }, [search]);

  const backButtonClick = () => {
    setCurrentTab(null);
  };

  const footerLinkClick = (type) => {
    switch (type) {
      case 'cancel':
        onClose();
        break;
      case 'sizeGuide':
      default:
        setGlobalState.setShowSizeGuide(true);
    }
  };

  const onClose = () => {
    setGlobalState.setFilterModal(false, {type: 'products'});
  };

  const priceChange = (value) => {
    if (value?.[0] === simpleData?.price?.min && value?.[1] === simpleData?.price?.max) {
      clearFilter('priceValue');
    } else {
      changeFilters({id: 'price', unic: true}, 'custom', value);
    }
  };

  const getCurrentPrice = () => {
    const priceValue = filters?.priceValue || [];
    return [Number(priceValue?.[0]), Number(priceValue?.[1])];
  };

  const onSubmit = () => {
    const queryString = qs.stringify({filters, search: searchValue}); // save search
    history.push(`${pathname}?${queryString}`);
    setGlobalState.setFilterModal(false);
  };

  const getTabContent = () => {
    if (simpleData?.[currentTab]?.loading) return <Loader wrapperWidth="100%" wrapperHeight="100px" dotsSize="10px" />;
    switch (currentTab) {
      case 'categories':
        return (
          <>
            {!_.isEmpty(filters?.categories) && (
              <>
                <SimpleItem
                  parent
                  filtersValue={filters}
                  title={
                    <BackItem>
                      <Icon type="arrow" color="#000" height={14} width={12} />
                      Back to {categoriesData?.productCategories?.nodes?.[0]?.parent?.node?.name || 'All Categories'}
                    </BackItem>
                  }
                  onClick={() =>
                    changeFilters(
                      simpleData?.categories,
                      categoriesData?.productCategories?.nodes?.[0]?.parent?.node?.slug ||
                        categoriesData?.productCategories?.nodes?.[0]?.slug
                    )
                  }
                />
              </>
            )}
            {simpleData?.[currentTab]?.children?.map((item, index) => (
              <SimpleItem
                filtersValue={filters}
                type={simpleData?.[currentTab]?.id}
                key={index}
                title={item?.name}
                selected={isItemSelected(simpleData?.[currentTab].id, item.slug)}
                isActive={isItemSelected(simpleData?.[currentTab].id, item.slug)}
                onClick={() => changeFilters(simpleData?.[currentTab], item.slug)}
              />
            ))}
          </>
        );
      case 'price':
        return (
          <Price
            currencyType={simpleData?.price.type}
            value={getCurrentPrice()}
            min={simpleData?.price.min}
            max={simpleData?.price.max}
            onChange={priceChange}
          />
        );
      case 'color':
        return simpleData?.[currentTab]?.children
          ?.filter((item) => item?.name.toLowerCase().includes(attributeSearch.toLowerCase()))
          ?.map((item, index) => {
            const color = tinycolor(item?.slug);

            return (
              <SimpleItem
                filtersValue={filters}
                type={simpleData?.[currentTab]?.id}
                key={index}
                title={item?.name}
                selected={isItemSelected(simpleData?.[currentTab].id, item.slug)}
                isActive={isItemSelected(simpleData?.[currentTab].id, item.slug)}
                onClick={() => changeFilters(simpleData?.[currentTab], item.slug)}
              >
                <Circle
                  style={{margin: '0 8px 4px 0'}}
                  border={color.isValid() ? color.toName() === 'white' : false}
                  color={color.isValid() ? color.toHexString() : '#000'}
                />
              </SimpleItem>
            );
          });
      case 'brands':
      case 'material':
        return getCurrentAlfabet(simpleData?.[currentTab]?.children)?.map((item) => (
          <>
            <Label>{item}</Label>
            {simpleData?.[currentTab]?.children
              ?.filter((i) => i.name[0].toLowerCase() === item.toLowerCase())
              ?.filter((item) => item?.name.toLowerCase().includes(attributeSearch.toLowerCase()))
              .map((subItem, index) => (
                <SimpleItem
                  filtersValue={filters}
                  type={simpleData?.[currentTab]?.id}
                  key={index}
                  title={subItem?.name}
                  selected={isItemSelected(simpleData?.[currentTab].id, subItem.slug)}
                  isActive={isItemSelected(simpleData?.[currentTab].id, subItem.slug)}
                  onClick={() => changeFilters(simpleData?.[currentTab], subItem.slug)}
                />
              ))}
          </>
        ));
      case 'shopLocation':
        return (
          <>
            {simpleData?.[currentTab]?.children?.map((item, index) => (
              <SimpleItem
                filtersValue={filters}
                type={simpleData?.[currentTab]?.id}
                key={index}
                title={item?.name}
                selected={isItemSelected(simpleData?.[currentTab].id, item.id)}
                isActive={isItemSelected(simpleData?.[currentTab].id, item.id)}
                onClick={() => changeFilters(simpleData?.[currentTab], item.id)}
              />
            ))}
            <div style={{margin: '16px 21px'}}>
              <Select
                showSearch
                allowClear
                name="customLocation"
                onChange={(itemId) => changeFilters(simpleData?.shopLocationCustom, itemId)}
                value={filters?.[simpleData?.shopLocationCustom.id]}
                options={simpleData?.shopLocationCustom?.children}
                labelKey="name"
                valueKey="id"
                disabled={!isItemSelected(simpleData?.shopLocation.id, 'custom')}
              />
            </div>
          </>
        );
      default:
        return simpleData?.[currentTab]?.children
          ?.filter((item) => item?.name.toLowerCase().includes(attributeSearch.toLowerCase()))
          ?.map((item, index) => (
            <SimpleItem
              filtersValue={filters}
              type={simpleData?.[currentTab]?.id}
              key={index}
              title={item?.name}
              selected={isItemSelected(simpleData?.[currentTab].id, item.slug)}
              isActive={isItemSelected(simpleData?.[currentTab].id, item.slug)}
              onClick={() => changeFilters(simpleData?.[currentTab], item.slug)}
            />
          ));
    }
  };

  return (
    <StyledModal
      visible={isOpen}
      onCancel={onClose}
      style={{
        top: 0,
        right: 0,
        display: 'flex',
        padding: 0,
        height: '100vh',
        margin: '0 0 0 auto',
        justifyContent: 'flex-end',
        bottom: 0
      }}
      footer={null}
      destroyOnClose
    >
      <ModalHeader>
        {!!currentTab && (
          <BackButton onClick={backButtonClick}>
            <ArrowBack stroke="#000" />
          </BackButton>
        )}
        <Title>{simpleData?.[currentTab]?.name || 'Sort & Filter'}</Title>
        {!_.isEmpty(filters?.[simpleData?.[currentTab]?.id]) &&
          !['location', 'price', ''].includes(simpleData?.[currentTab]?.id) && (
            <ResetFilter onClick={() => clearFilter(simpleData?.[currentTab]?.id)}>
              Reset Filter ({_.size(filters?.[simpleData?.[currentTab]?.id])})
            </ResetFilter>
          )}
        {!_.isEmpty(filters) && !currentTab && <ResetFilter onClick={clearAll}>Reset All</ResetFilter>}
        {!_.isEmpty(filters?.[simpleData?.[currentTab]?.id]) && ['location', 'price'].includes(currentTab) && (
          <ResetFilter onClick={() => clearFilter(simpleData?.[currentTab]?.id)}>
            Reset {simpleData?.[currentTab]?.name}
          </ResetFilter>
        )}
      </ModalHeader>
      <ModalContent>
        {['color', 'brands', 'material'].includes(currentTab) && (
          <SearchWrapper>
            <SearchField
              onReset={() => setAttributeSearch('')}
              onChange={(e) => setAttributeSearch(e.target.value)}
              searchValue={attributeSearch}
              placeholder={`Search for ${simpleData?.[currentTab]?.name}`}
            />
          </SearchWrapper>
        )}
        {/* {getFilterItems()} */}
        {currentTab ? (
          getTabContent()
        ) : (
          <>
            {/* Categories */}
            <SimpleItem
              filtersValue={filters}
              type={simpleData?.categories?.id}
              isExpands
              title={simpleData?.categories?.name}
              count={null}
              bold
              onClick={() => setCurrentTab('categories')}
              isActive={!_.isEmpty(filters?.[simpleData?.categories?.id])}
              selectedChildren={filters?.[simpleData?.categories?.id]}
            />
            {Object.keys(attributes)?.map((item) =>
              getFilter(attributes?.[item], filters, setCurrentTab, isItemSelected, changeFilters)
            )}

            {/* Brands */}
            <SimpleItem
              isExpands
              filtersValue={filters}
              type={simpleData?.brands?.id}
              title={simpleData?.brands?.name}
              bold
              onClick={() => setCurrentTab('brands')}
              isActive={!_.isEmpty(filters?.[simpleData?.brands?.id])}
              selectedChildren={filters?.[simpleData?.brands?.id]}
            />
            {/* Price */}
            <SimpleItem
              isExpands
              filtersValue={filters}
              type={simpleData?.price?.id}
              title={simpleData?.price?.name}
              bold
              onClick={() => setCurrentTab('price')}
              isActive={!_.isEmpty(filters?.priceValue)}
              selectedChildren={filters?.priceValue}
            />
            {/* Location (disable for now) */}

            {/* <SimpleItem
              isExpands
              type={simpleData?.shopLocation?.id}
              title={simpleData?.shopLocation?.name}
              bold
              onClick={() => setCurrentTab('shopLocation')}
              isActive={!_.isEmpty(filters?.[simpleData?.shopLocation?.id])}
              selectedChildren={filters?.[simpleData?.shopLocation?.id]}
            /> */}
            {/* Offers */}
            {simpleData?.offers?.children?.map((item, index) => (
              <SwitcherItem
                key={index}
                bold
                title={item?.name}
                isActive={isItemSelected(simpleData?.offers.id, item.id)}
                onClick={() => changeFilters(simpleData?.offers, item.id)}
              />
            ))}
            <SwitcherItem
              bold
              title="Products with Shopcoins"
              isActive={isItemSelected('productsWithShopcoins', 'true')}
              onClick={() =>
                changeFilters(
                  {id: 'productsWithShopcoins', unic: true},
                  String(!isItemSelected('productsWithShopcoins', 'true'))
                )
              }
            />
            {/* Sort by */}
          </>
        )}
      </ModalContent>
      <ModalFooter>
        {_.isEmpty(currentFilter.footerLink) ? (
          <Cancel onClick={() => footerLinkClick('cancel')}>Cancel</Cancel>
        ) : (
          <FooterLink onClick={() => footerLinkClick(currentFilter.footerLink?.type)}>
            {currentFilter.footerLink?.name}
          </FooterLink>
        )}
        <SaveButton onClick={onSubmit}>Save & Close</SaveButton>
      </ModalFooter>
    </StyledModal>
  );
};

export default ProductFilters;
