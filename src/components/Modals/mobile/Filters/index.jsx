import React, {useState, useEffect} from 'react';
import _ from 'lodash';

import useGlobal from 'store';
import ModalContainer from 'components/Modals/mobile/ModalContainerMobile';
import ArrowBack from 'assets/ArrowBack';
import Arrow from 'assets/Arrow';
import {
  ModalHeader,
  Title,
  ResetFilter,
  BackButton,
  FilterItem,
  ModalContent,
  ModalFooter,
  SaveButton,
  FilterItemWrapper,
  SelectedFiltersWrapper,
  SearchWrapper,
  Label,
  FooterLink,
  Cancel
} from 'components/Modals/mobile/Filters/styled';
import Checkmark from 'assets/Checkmark';
import Switcher from 'components/Buttons/Switcher';
import SearchField from 'components/SearchField/Mobile';
import Location from 'components/Modals/mobile/Filters/components/Location';
import Price from 'components/Modals/mobile/Filters/components/Price';
import ProductFilters from 'components/Modals/mobile/Filters/ProductFilters';
import StoresFilters from 'components/Modals/mobile/Filters/StoresFilters';

const SelectedFilters = ({items, parent}) => {
  const separator = parent?.id === 'price' ? ' - ' : ' | ';
  return (
    <SelectedFiltersWrapper>
      {_.values(items)?.map(
        (item, index) => `${index ? separator : ''}${parent.children.find((el) => el.id === item)?.name || item}`
      )}
    </SelectedFiltersWrapper>
  );
};

const FiltersModal = ({isOpen, onClose, data = {}, type}) => {
  const [searchValue, setSearchValue] = useState('');
  const [globalState, setGlobalState] = useGlobal();
  const [currentFilter, setCurrentFilter] = useState(data);
  const [filterHistory, setFilterHistory] = useState([]);
  const [selectedFilters, setSelectedFilters] = useState({}); // <filter id>: [<variant id>] // <filter id>: {<field id>: <variant id>}

  useEffect(() => {
    let timeout;
    timeout = setTimeout(
      () => {
        setSearchValue('');
        setCurrentFilter(data);
        setFilterHistory([]);
        setSelectedFilters({});
      },
      isOpen ? 0 : 500
    );

    return () => clearTimeout(timeout);
  }, [isOpen]);

  const setStateDelayed = (setState, newState) => {
    // useOutsideClick working incorrect if clicked element remove immediately. Need to fix
    setTimeout(() => setState(newState));
  };

  const filterItemClick = (item) => {
    if (item?.children?.length) {
      setFilterHistory([...filterHistory, currentFilter]);
      setStateDelayed(setCurrentFilter, item);
    } else {
      // unic - only one value can be selected
      if (!!selectedFilters[currentFilter.id] && !currentFilter.unic) {
        if (isItemSelected(item.id)) {
          // remove item from selected filters
          setSelectedFilters({
            ...selectedFilters,
            [currentFilter.id]: [...selectedFilters[currentFilter.id].filter((itemId) => itemId !== item.id)]
          });
        } else {
          // add item to exist array
          setSelectedFilters({
            ...selectedFilters,
            [currentFilter.id]: [...new Set([...selectedFilters[currentFilter.id], item.id])]
          });
        }
      } else {
        // add item
        setSelectedFilters({...selectedFilters, [currentFilter.id]: [item.id]});
      }
    }
  };

  const isItemSelected = (itemId) => {
    return !!selectedFilters[currentFilter.id]?.includes(itemId);
  };

  const resetFilter = (resetAll = false) => {
    return setStateDelayed(setSelectedFilters, resetAll ? {} : {...selectedFilters, [currentFilter.id]: null});
  };

  const backButtonClick = () => {
    setStateDelayed(setCurrentFilter, [...filterHistory]?.pop() || data);
    setFilterHistory([...filterHistory].splice(0, filterHistory.length - 1) || []);
  };

  const locationChange = (fieldId, value) => {
    setSelectedFilters({
      ...selectedFilters,
      [currentFilter.id]: {...selectedFilters[currentFilter.id], [fieldId]: value}
    });
  };

  const priceChange = (value) => {
    if (value?.[0] === currentFilter.min && value?.[1] === currentFilter.max) {
      resetFilter();
    } else {
      setSelectedFilters({
        ...selectedFilters,
        [currentFilter.id]: value
      });
    }
  };

  const footerLinkClick = (type) => {
    switch (type) {
      case 'cancel':
        onClose();
        break;
      case 'sizeGuide':
      default:
        setGlobalState.setShowSizeGuide(true);
    }
  };

  const FilterItemComponent = (item) => (
    <FilterItemWrapper
      key={item.id}
      selected={!item.withSwitcher && isItemSelected(item.id)}
      isActive={!_.isEmpty(selectedFilters[item.id])}
      onClick={() => filterItemClick(item)}
    >
      <FilterItem bold={currentFilter.id === 'main'}>
        {item.name}&nbsp;<span>{!!item.count && `/ ${item.count}`}</span>
        {!!item?.children?.length && <Arrow color="black" width={8} />}
        {isItemSelected(item.id) && !item.withSwitcher && <Checkmark width={20} />}
        {item.withSwitcher && <Switcher isActive={isItemSelected(item.id)} />}
      </FilterItem>
      {!_.isEmpty(selectedFilters[item.id]) && <SelectedFilters parent={item} items={selectedFilters[item.id]} />}
    </FilterItemWrapper>
  );

  const getFilterItems = () => {
    switch (currentFilter.id) {
      case 'price':
        return <Price onChange={priceChange} value={selectedFilters[currentFilter.id]} {...currentFilter} />;
      case 'location':
        return <Location onChange={locationChange} value={selectedFilters[currentFilter.id]} />;
      default:
        return currentFilter.alphabetical
          ? getCurrentAlfabet(currentFilter.children)?.map((item) => (
              <>
                <Label>{item}</Label>
                {currentFilter.children
                  ?.filter((i) => i.name[0].toLowerCase() === item.toLowerCase())
                  .map((subItem) => FilterItemComponent(subItem))}
              </>
            ))
          : currentFilter.grouped
          ? currentFilter.groups?.map((item) => (
              <>
                <Label>{item.name}</Label>
                {currentFilter.children
                  ?.filter((i) => i.groupId === item.id)
                  .map((subItem) => FilterItemComponent(subItem))}
              </>
            ))
          : currentFilter?.children?.map((item) => FilterItemComponent(item));
    }
  };

  const getCurrentAlfabet = (items = []) => {
    // items = [{name:<item name>}]
    return [
      ...new Set(
        [...items]
          ?.sort((a, b) => {
            if (a.name < b.name) {
              return -1;
            }
            if (a.name > b.name) {
              return 1;
            }
            return 0;
          })
          .map((item) => item?.name[0])
      )
    ];
  };

  if (type === 'products') return <ProductFilters />;
  if (['stores','posts'].includes(type)) return <StoresFilters type={type} />;

  return (
    <ModalContainer
      styles={{borderRadius: '18px'}}
      isOpen={isOpen}
      onClose={globalState.showSizeGuide ? () => {} : onClose}
      rightAlignModal
    >
      <ModalHeader>
        {currentFilter.id !== 'main' && (
          <BackButton onClick={backButtonClick}>
            <ArrowBack stroke="#000" />
          </BackButton>
        )}
        <Title>{currentFilter.name}</Title>
        {!_.isEmpty(selectedFilters[currentFilter.id]) && !['location', 'price', 'main'].includes(currentFilter.id) && (
          <ResetFilter onClick={resetFilter}>Reset Filter ({_.size(selectedFilters[currentFilter.id])})</ResetFilter>
        )}
        {!_.isEmpty(selectedFilters) && currentFilter.id === 'main' && (
          <ResetFilter onClick={() => resetFilter(true)}>Reset All</ResetFilter>
        )}
        {!_.isEmpty(selectedFilters[currentFilter.id]) && ['location', 'price'].includes(currentFilter.id) && (
          <ResetFilter onClick={resetFilter}>Reset {currentFilter.name}</ResetFilter>
        )}
      </ModalHeader>
      <ModalContent>
        {currentFilter.withSearch && (
          <SearchWrapper>
            <SearchField
              onReset={() => setStateDelayed(setSearchValue, '')}
              onChange={(e) => setSearchValue(e.target.value)}
              searchValue={searchValue}
              placeholder={`Search for ${currentFilter.name}`}
            />
          </SearchWrapper>
        )}
        {getFilterItems()}
      </ModalContent>
      <ModalFooter>
        {_.isEmpty(currentFilter.footerLink) ? (
          <Cancel onClick={() => footerLinkClick('cancel')}>Cancel</Cancel>
        ) : (
          <FooterLink onClick={() => footerLinkClick(currentFilter.footerLink?.type)}>
            {currentFilter.footerLink?.name}
          </FooterLink>
        )}
        <SaveButton onClick={onClose}>Save & Close</SaveButton>
      </ModalFooter>
    </ModalContainer>
  );
};

export default FiltersModal;
