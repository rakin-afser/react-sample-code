export const getCurrentAlfabet = (items = []) => {
  // items = [{name:<item name>}]
  return [
    ...new Set(
      [...items]
        ?.sort((a, b) => {
          if (a.name < b.name) {
            return -1;
          }
          if (a.name > b.name) {
            return 1;
          }
          return 0;
        })
        .map((item) => item?.name[0].toUpperCase())
    )
  ];
};
