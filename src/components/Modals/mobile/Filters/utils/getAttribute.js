import React from 'react';
import _ from 'lodash';
import {useQuery} from '@apollo/client';

import {BRANDS, CATEGORIES, GET_ATTRIBUTES_BY_CATEGORY} from 'components/Modals/Filters/api/queries';

const useAttributes = (currentFilters) => {
  const {data: brandsData, loading: brandsLoading} = useQuery(BRANDS, {
    variables: {slug: currentFilters?.brands}
  });
  const {data: categoriesData, loading: categoriesLoading} = useQuery(CATEGORIES, {
    variables: {slug: currentFilters?.categories || []}
  });
  const {data: attributesData, loading: attributesLoading, error: attributesError} = useQuery(
    GET_ATTRIBUTES_BY_CATEGORY,
    {
      variables: {
        categories: currentFilters?.categories || []
      },
      fetchPolicy: 'cache-and-network'
    }
  );

  const attributes = attributesData?.getAttributes || [];

  return {
    data: [
      {slug: 'brands', options: brandsData?.productBrands?.nodes},
      {slug: 'categories', options: categoriesData?.productCategories?.nodes},
      ...attributes
    ],
    loading: brandsLoading || attributesLoading || categoriesLoading
  };
};

export default useAttributes;
