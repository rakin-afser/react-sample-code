import React, {useEffect} from 'react';
import {useLazyQuery} from '@apollo/client';
import _ from 'lodash';

import {BRANDS, CATEGORIES, GET_ATTRIBUTES_BY_CATEGORY} from 'components/Modals/Filters/api/queries';
import {
  offers,
  price,
  shopLocation,
  shopLocationCustom,
  sizeTypes,
  sizes as defaultSizes
} from 'components/Modals/Filters/api/constants';

const useFiltersData = (currentFilters, getData = true) => {
  const [getBrands, {data: brandsData, loading: brandsLoading, error: brandsError}] = useLazyQuery(BRANDS);
  const [getCategories, {data: categoriesData, loading: categoriesLoading, error: categoriesError}] = useLazyQuery(
    CATEGORIES,
    {
      variables: {
        parentId: _.isEmpty(currentFilters?.categories) ? 0 : undefined,
        slug: currentFilters?.categories || [],
        hideEmpty: _.isEmpty(currentFilters?.categories)
      },
      fetchPolicy: 'cache-and-network'
    }
  );
  const [getAttributes, {data: attributesData, loading: attributesLoading, error: attributesError}] = useLazyQuery(
    GET_ATTRIBUTES_BY_CATEGORY,
    {
      variables: {
        categories: currentFilters?.categories || []
      },
      fetchPolicy: 'cache-and-network'
    }
  );

  useEffect(() => {
    if (getData) {
      getBrands();
      getCategories();
      getAttributes();
    }
  }, [getData]);

  const subCategories = categoriesData?.productCategories?.nodes?.[0]?.children?.nodes;

  const categories = {
    unic: true,
    loading: categoriesLoading,
    id: 'categories',
    name: 'Categories',
    children:
      (!_.isEmpty(currentFilters?.categories) && !_.isEmpty(subCategories)
        ? subCategories
        : categoriesData?.productCategories?.nodes) || []
  };

  const brands = {
    id: 'brands',
    loading: brandsLoading,
    name: 'Brands',
    children: brandsData?.productBrands?.nodes || []
  };

  const attributes = {};

  (attributesData?.getAttributes || []).forEach((item) => {
    attributes[item?.slug] = {id: item?.slug, name: item?.name, children: item?.options};
  });

  return {
    attributes: _.isEmpty(currentFilters?.categories) ? [] : attributesData?.getAttributes || [],
    data: {
      categories,
      parentCategory: categoriesData?.productCategories?.nodes?.[0] || {},
      brands,
      price,
      shopLocation,
      shopLocationCustom,
      offers,
      sizeTypes,
      defaultSizes,
      ...attributes
    },
    loading: [brandsLoading, categoriesLoading].reduce((acc, quantity) => acc || quantity, false),
    filtersLoading: {
      brandsLoading,
      categoriesLoading
    },
    filtersData: {categoriesData} // for non-typical filters
  };
};
export default useFiltersData;
