import {Popup, PopupOverlay, PopupContent} from './styled';
import React from 'react';
import Div100vh from 'react-div-100vh';
import {createPortal} from 'react-dom';

const BottomPopup = ({children, active, setActive}) => {
  return createPortal(
    <Popup active={active}>
      <Div100vh
        style={{
          height: '100vh',
          width: '100%',
          maxHeight: 'calc(100rvh)',
          display: 'flex'
        }}
      >
        <PopupOverlay active={active} onClick={() => setActive(false)}></PopupOverlay>
        <PopupContent active={active}>{children}</PopupContent>
      </Div100vh>
    </Popup>,
    document.getElementById('modal')
  );
};

export default BottomPopup;
