import {Button} from 'antd';
import styled from 'styled-components';

export const Popup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100vw;
  z-index: 10000;
  opacity: ${({active}) => (active ? 1 : 0)};
  pointer-events: none;
  transition: all 0.3s ease;
  display: flex;
  flex-wrap: wrap;
`;

export const PopupOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0, 0, 0, 0.5);
  opacity: 0;
  pointer-events: none;
  transition: all 0.3s ease;

  ${({active}) =>
    active &&
    `
    opacity: 1;
    pointer-events: all;
  `}
`;

export const PopupContent = styled.div`
  margin-top: auto;
  padding: 10px 10px;
  width: 100%;
  position: relative;
  z-index: 2;
  transition: all 0.3s ease;
  position: absolute;
  left: 0;
  right: 0;
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
  bottom: ${({active}) => (active ? '0' : '-100%')};
`;
