import {Button} from 'antd';
import {mainFont} from 'constants/fonts';
import styled from 'styled-components';

export const Popup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100vw;
  z-index: 10000;
  opacity: ${({active}) => (active ? 1 : 0)};
  pointer-events: none;
  transition: all 0.3s ease;
  display: flex;
  flex-wrap: wrap;
`;

export const PopupOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0, 0, 0, 0.5);
  opacity: 0;
  pointer-events: none;
  transition: all 0.3s ease;

  ${({active}) =>
    active &&
    `
    opacity: 1;
    pointer-events: all;
  `}
`;

export const BtnsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 8px;
`;

export const PopupContent = styled.div`
  margin-top: auto;
  padding: 10px 10px;
  width: 100%;
  position: relative;
  z-index: 2;
  transition: all 0.3s ease;
  position: absolute;
  left: 0;
  right: 0;
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
  bottom: ${({active}) => (active ? '0' : '-100%')};
`;

export const FakeBtn = styled.div`
  font-family: ${mainFont};
  border-radius: 0;
  border: none;
  border-top: 1px solid #dadade;
  height: 63px;
  font-size: 16px;
  color: #000;
  letter-spacing: 0.38px;
  background-color: #fff;
  text-align: left;
  padding-left: 32px;
  display: flex;
  align-items: center;

  i:first-child {
    margin-right: 11px;
    line-height: 1;
    display: inline-block;
  }

  &:first-child {
    border-radius: 13px 13px 0 0;
  }
  &:last-child {
    border-radius: 0 0 13px 13px;
  }
`;

export const Connect = styled(Button)`
  &&& {
    color: #999;
    border: 1px solid currentColor;
    height: 28px;
    border-radius: 30px;
    padding: 0 18px;
    margin-left: auto;
    margin-right: 34px;
  }
`;

export const Btn = styled(Button)`
  &&& {
    font-family: ${mainFont};
    border-radius: 0;
    border: none;
    border-top: 1px solid #dadade;
    height: 63px;
    font-size: 16px;
    color: #000;
    letter-spacing: 0.38px;
    background-color: #fff;
    text-align: left;
    padding-left: 32px;
    display: flex;
    align-items: center;

    i:first-child {
      margin-right: 11px;
      line-height: 1;
    }

    i:last-child {
      line-height: 1;
      display: inline-block;
      margin-right: 19px;
      margin-left: auto;
      font-size: 0;
    }

    &:first-child {
      border-radius: 13px 13px 0 0;
    }
    &:last-child {
      border-radius: 0 0 13px 13px;
    }
  }
`;

Btn.defaultProps = {
  block: true
};

export const Cancel = styled(Button)`
  &&& {
    font-family: 'SF Pro Display', sans-serif;
    font-weight: 600;
    border-radius: 13px;
    font-size: 20px;
    height: 57px;
    color: #007aff;
    letter-spacing: 0.38px;
  }
`;

Cancel.defaultProps = {
  block: true
};
