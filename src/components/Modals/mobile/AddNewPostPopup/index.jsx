import {Popup, PopupOverlay, PopupContent, Cancel, BtnsWrapper, Btn, FakeBtn, Connect} from './styled';
import React, {useState} from 'react';
import Div100vh from 'react-div-100vh';
import {createPortal} from 'react-dom';
import Icon from 'components/Icon';
import {v1} from 'uuid';

const AddNewPostPopup = ({active, setActive, setUploads, uploads}) => {
  const [instagram, setInstagram] = useState(false);
  const [facebook, setFacebook] = useState(false);

  function renderInstagram() {
    if (instagram) {
      return (
        <Btn>
          <Icon type="smallInstagram" color="currentColor" />
          Instagram
          <Icon type="chevronRight" fill="currentColor" />
        </Btn>
      );
    }

    return (
      <FakeBtn>
        <Icon type="smallInstagram" color="currentColor" />
        Instagram
        <Connect onClick={() => setInstagram(true)}>Connect</Connect>
      </FakeBtn>
    );
  }

  function renderFacebook() {
    if (facebook) {
      return (
        <Btn>
          <Icon type="smallFacebook" color="currentColor" />
          Facebook
          <Icon type="chevronRight" fill="currentColor" />
        </Btn>
      );
    }
    return (
      <FakeBtn>
        <Icon type="smallFacebook" color="currentColor" />
        Facebook
        <Connect onClick={() => setFacebook(true)}>Connect</Connect>
      </FakeBtn>
    );
  }

  async function onUpload(e) {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      try {
        const images = await Promise.all(
          files.map((file) => {
            return new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.addEventListener('load', (ev) => {
                resolve({
                  id: v1(),
                  file,
                  result: ev.target.result
                });
              });
              reader.addEventListener('error', reject);
              reader.readAsDataURL(file);
            });
          })
        );
        setUploads([...uploads, ...images]);
        setActive(false);
      } catch (error) {
        console.log(`error`, error);
      }
    }
  }

  return createPortal(
    <Popup active={active}>
      <Div100vh
        style={{
          height: '100vh',
          width: '100%',
          maxHeight: 'calc(100rvh)',
          display: 'flex'
        }}
      >
        <PopupOverlay active={active} onClick={() => setActive(false)} />
        <PopupContent active={active}>
          <BtnsWrapper>
            {renderInstagram()}
            {renderFacebook()}
            <input
              onChange={onUpload}
              type="file"
              style={{display: 'none'}}
              accept="image/*, video/*"
              capture="filesystem"
            />
            <Btn onClick={(e) => e.currentTarget.previousSibling.click()}>
              <Icon type="smallCamera" color="currentColor" />
              Camera
            </Btn>
            <input onChange={onUpload} type="file" style={{display: 'none'}} multiple accept="image/*, video/*" />
            <Btn onClick={(e) => e.currentTarget.previousSibling.click()}>
              <Icon type="smallClip" color="currentColor" />
              Attach Images / Video
            </Btn>
          </BtnsWrapper>
          <Cancel onClick={() => setActive(false)}>Cancel</Cancel>
        </PopupContent>
      </Div100vh>
    </Popup>,
    document.getElementById('modal')
  );
};

export default AddNewPostPopup;
