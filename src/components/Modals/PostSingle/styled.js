import styled from 'styled-components/macro';
import {Modal} from 'antd';

export const StyledModal = styled(Modal)`
  width: 100% !important;

  & .ant-modal-content {
    background: #fff;
    border-radius: 4px;
    width: 100%;
    margin: 0;
  }

  & .ant-modal-body {
    display: flex;
    flex-direction: column;
    padding: 0;
    height: 100%;
    margin: 0;
  }

  & .ant-modal-close {
    display: none;
  }
`;

export const CloseWrapper = styled.div`
  cursor: pointer;
  position: absolute;
  top: 12px;
  right: -30px;

  & svg {
    width: 16px;
    height: 16px;

    path {
      fill: #fff;
    }
  }
`;

export const ModalContent = styled.div`
  overflow-y: auto;
  flex-grow: 1;
  max-height: 100%;
  overflow: -moz-scrollbars-none;
  overflow-y: auto;

  &::-webkit-scrollbar {
    display: none;
  }
`;

export const PostContainer = styled.div`
  margin: 0;
`;
