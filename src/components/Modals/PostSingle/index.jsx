import React, {useState} from 'react';
import {useQuery} from '@apollo/client';

import {StyledModal, CloseWrapper, ModalContent, PostContainer} from 'components/Modals/PostSingle/styled';
import {ReactComponent as CloseIcon} from 'images/svg/modal-close.svg';
import Loader from 'components/Loader';
import useModal from 'hooks/useModal';
import Post from 'components/Post';
import {POST} from 'queries';
import Scrollbar from 'components/Scrollbar';

const PostSingle = ({data}) => {
  const {closeModal} = useModal();
  const [isOpen, setIsOpen] = useState(true);
  const {data: postData, loading, error} = useQuery(POST, {
    variables: {postId: data?.slug}
  });

  const {
    author,
    date,
    title,
    tags,
    content,
    comments,
    commentCount,
    relatedProducts,
    galleryImages,
    databaseId,
    isLiked,
    totalLikes,
    video,
    featuredImage
  } = postData?.post || {};

  const onClose = () => {
    setIsOpen(false);
    closeModal();
  };

  return (
    <StyledModal
      visible={isOpen}
      onCancel={onClose}
      style={{
        display: 'flex',
        padding: 0,
        height: '88vh',
        bottom: 0,
        width: '520px !important',
        top: '6vh',
        maxWidth: '526px',
        margin: 'auto auto 10px'
      }}
      footer={null}
      destroyOnClose
    >
      <CloseWrapper>
        <CloseIcon onClick={onClose} />
      </CloseWrapper>
      <div style={{overflow: 'hidden', borderRadius: '4px'}}>
        <ModalContent>
          {loading && <Loader wrapperHeight="200px" wrapperWidth="100%" />}
          {!loading && (
            <PostContainer>
              <Post onPostClick={() => {}} data={postData?.post} isUserLogeIn />
            </PostContainer>
          )}
        </ModalContent>
      </div>
    </StyledModal>
  );
};

export default PostSingle;
