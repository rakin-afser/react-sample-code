import {gql} from '@apollo/client';

export const COLORS = gql`
  query Colors($slug: [String]) {
    paColors(first: 100, where: {slug: $slug, hideEmpty: true}) {
      nodes {
        slug
        name
        databaseId
      }
    }
  }
`;

export const BRANDS = gql`
  query Brands($slug: [String]) {
    productBrands(first: 100, where: {slug: $slug, hideEmpty: true}) {
      nodes {
        name
        slug
        databaseId
      }
    }
  }
`;

export const SIZES = gql`
  query Sizes($slug: [String]) {
    paSizes(first: 100, where: {slug: $slug, hideEmpty: true}) {
      nodes {
        name
        slug
        databaseId
      }
    }
  }
`;

export const MATERIALS = gql`
  query Materials($slug: [String]) {
    paMaterials(first: 100, where: {slug: $slug, hideEmpty: true}) {
      nodes {
        name
        slug
        databaseId
      }
    }
  }
`;

export const CATEGORIES = gql`
  query Categories($parentId: Int, $slug: [String], $hideEmpty: Boolean) {
    productCategories(where: {parent: $parentId, slug: $slug, hideEmpty: $hideEmpty}, first: 100) {
      nodes {
        name
        slug
        children {
          nodes {
            name
            slug
          }
        }
        parent {
          node {
            name
            slug
          }
        }
      }
    }
  }
`;

export const GET_ATTRIBUTES_BY_CATEGORY = gql`
  query getAttributesByCategory($categories: [String]) {
    getAttributes(categoryIn: $categories) {
      id
      name
      slug
      options {
        id
        name
        slug
      }
    }
  }
`;
