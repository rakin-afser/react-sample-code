export const offers = {
  id: 'offers',
  name: 'Offers',
  children: [
    {id: 'free', name: 'FREE delivery/shipping'},
    {id: 'sale', name: 'On Discount/Sale'}
  ]
};

export const clothingMaterial = {
  id: 'clothingMaterial',
  name: 'Clothing Material',
  children: [
    {id: 'cotton', name: 'Cotton'},
    {id: 'chiffon', name: 'Chiffon'},
    {id: 'denim', name: 'Denim'},
    {id: 'silk', name: 'Silk'},
    {id: 'nylon', name: 'Nylon'}
  ]
};

export const price = {
  name: 'Price',
  id: 'price',
  unic: true,
  min: 10,
  max: 1000,
  type: '$',
  defaultId: 'any',
  children: [
    {id: 'any', name: 'Any price', values: [10, 1000]},
    {id: 'custom', name: 'Custom'}
  ]
};

export const shopLocation = {
  id: 'shopLocation',
  unic: true,
  name: 'Shop Location',
  defaultId: 'any',
  children: [
    {
      name: 'Anywhere',
      id: 'any'
    },
    {
      name: 'Bahrain',
      id: 'bahrain'
    },
    {
      name: 'Saudi Arabia',
      id: 'saudiArabia'
    },
    {
      name: 'Custom',
      id: 'custom'
    }
  ]
};

export const shopLocationCustom = {
  id: 'shipToCustom',
  unic: true,
  children: [
    {
      name: 'USA',
      id: 'usa'
    },
    {
      name: 'England',
      id: 'england'
    },
    {
      name: 'Spain',
      id: 'spain'
    }
  ]
};

export const sizeTypes = {
  id: 'sizeTypes',
  unic: true,
  defaultId: 'int',
  children: [
    {
      name: 'INT',
      id: 'int'
    },
    {
      name: 'UK',
      id: 'uk'
    },
    {
      name: 'US',
      id: 'us'
    }
    // {
    //   name: 'Others',
    //   id: 'others'
    // }
  ]
};

export const sizes = {
  int: [
    {name: 'XS', slug: 'xs'},
    {name: 'S', slug: 's'},
    {name: 'M', slug: 'm'},
    {name: 'L', slug: 'l'},
    {name: 'XL', slug: 'xl'},
    {name: 'XXL', slug: 'xxl'}
  ],
  uk: [
    {name: '0', slug: '0'},
    {name: '2', slug: '2'},
    {name: '4', slug: '4'},
    {name: '6', slug: '6'},
    {name: '8', slug: '8'},
    {name: '10', slug: '10'},
    {name: '12', slug: '12'},
    {name: '14', slug: '14'},
    {name: '16', slug: '16'},
    {name: '18', slug: '18'},
    {name: '20', slug: '20'},
    {name: '22', slug: '22'},
    {name: '24', slug: '24'}
  ],
  us: [
    {name: '4', slug: '4'},
    {name: '6', slug: '6'},
    {name: '8', slug: '8'},
    {name: '10', slug: '10'},
    {name: '12', slug: '12'},
    {name: '14', slug: '14'},
    {name: '16', slug: '16'},
    {name: '18', slug: '18'},
    {name: '20', slug: '20'},
    {name: '22', slug: '22'},
    {name: '24', slug: '24'},
    {name: '26', slug: '26'},
    {name: '28', slug: '28'}
  ]
};
