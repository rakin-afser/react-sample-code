import React, {useState, useEffect} from 'react';
import _ from 'lodash';
import qs from 'qs';
import {useLocation, useHistory} from 'react-router-dom';

import useGlobal from 'store';
import Switcher from 'components/Buttons/Switcher';
import Icon from 'components/Icon';
import Input from 'components/Input';
import Loader from 'components/Loader';
import PriceSelect from 'components/PriceSelect';
import Select from 'components/SelectTrue';
import {
  StyledModal,
  Wrapper,
  CloseContainer,
  Title,
  FilterBlock,
  BlockTitle,
  FiltersHeader,
  FiltersContent,
  FiltersFooter,
  BlockItem,
  CategoryItem,
  ApplyButton,
  BackButton,
  ItemName,
  SizeItem,
  SizesWrapper
} from 'components/Modals/Filters/styled';
import useFilters from 'util/useFilters';
import ExpandedBlock from 'components/Modals/Filters/components/BrandsBlock';
import ColorsBlock from 'components/Modals/Filters/components/ColorsBlock';
import getFiltersData from 'components/Modals/mobile/Filters/utils/getFiltersData';

const getFilter = (item, isItemSelected, changeFilters) => {
  const filterId = item?.slug;

  switch (item?.slug) {
    case 'color': {
      return (
        <ColorsBlock
          title={item?.name}
          filterId={filterId}
          children={item?.options || []}
          isItemSelected={isItemSelected}
          changeFilters={changeFilters}
        />
      );
    }
    case 'size': {
      return (
        <FilterBlock>
          <BlockTitle>{item?.name}</BlockTitle>
          {/* <Select
            name="sizeTypes"
            onChange={(itemId) => changeFilters(sizeTypes, itemId)}
            value={filters?.[sizeTypes.id] || [sizeTypes.defaultId]}
            options={sizeTypes?.children}
            labelKey="name"
            valueKey="id"
          /> */}
          <SizesWrapper>
            {item?.options?.map((el, index) => (
              <SizeItem
                key={index}
                onClick={() => changeFilters({id: filterId}, el?.slug)}
                selected={isItemSelected(filterId, el?.slug)}
              >
                {el?.name}
              </SizeItem>
            ))}
          </SizesWrapper>
        </FilterBlock>
      );
    }
    default: {
      return (
        <ExpandedBlock
          title={item?.name}
          filterId={filterId}
          children={item?.options || []}
          isItemSelected={isItemSelected}
          changeFilters={changeFilters}
        />
      );
    }
  }
};

const Filters = () => {
  const history = useHistory();
  const {pathname, search} = useLocation();
  const [globalState, setGlobalState] = useGlobal();
  const {search: searchValue, filters: filtersValue} = qs.parse(search.replace('?', '')) || {};
  const {filters, changeFilters, isItemSelected, reloadFilters} = useFilters(filtersValue);
  const {data, attributes, filtersLoading, filtersData = {}} = getFiltersData(filters, !!globalState.filterModal);
  const getCurrentPrice = () => {
    const priceValue = filters?.priceValue || [];
    return [Number(priceValue?.[0]), Number(priceValue?.[1])];
  };

  const {categories, parentCategory, brands, price, shopLocation, shopLocationCustom, offers} = data || {};
  const {categoriesLoading} = filtersLoading || {};
  const {sizesData, categoriesData} = filtersData;

  useEffect(() => {
    reloadFilters(filtersValue);
  }, [search]);

  const onSubmit = () => {
    const queryString = qs.stringify({filters, search: searchValue}); // save search
    history.push(`${pathname}?${queryString}`);
    setGlobalState.setFilterModal(false);
  };

  return (
    <StyledModal
      visible={globalState.filterModal}
      onCancel={() => setGlobalState.setFilterModal(false)}
      style={{
        top: 130,
        display: 'flex',
        padding: 0,
        height: 'calc(100vh - 130px)',
        marginLeft: 4,
        bottom: 0
      }}
      footer={null}
      destroyOnClose
      bodyStyle={{
        padding: 0,
        height: '100%'
      }}
      closeIcon={
        <CloseContainer
          style={{
            top: 2,
            position: 'relative',
            right: 0
          }}
        >
          <Icon color="#999" type="close" width={24} height={24} />
        </CloseContainer>
      }
    >
      <Wrapper>
        <FiltersHeader>
          <Title>Filters</Title>
        </FiltersHeader>
        <FiltersContent>
          <FilterBlock style={{margin: '0 0 32px'}}>
            <BlockTitle style={{margin: '0 24px 12px'}}>{categories?.name}</BlockTitle>
            {categoriesLoading ? (
              <Loader wrapperWidth="100%" wrapperHeight="50px" dotsSize="10px" />
            ) : (
              <>
                {!_.isEmpty(filters?.categories) && (
                  <>
                    <CategoryItem
                      parent
                      onClick={() =>
                        changeFilters(
                          categories,
                          categoriesData?.productCategories?.nodes?.[0]?.parent?.node?.slug ||
                            categoriesData?.productCategories?.nodes?.[0]?.slug
                        )
                      }
                    >
                      <Icon type="arrow" color="#000" height={14} width={12} />
                      Back to {categoriesData?.productCategories?.nodes?.[0]?.parent?.node?.name || 'All Categories'}
                    </CategoryItem>
                  </>
                )}
                {categories?.children?.map((item) => (
                  <CategoryItem
                    selected={isItemSelected(categories.id, item?.slug)}
                    onClick={() => changeFilters(categories, item?.slug)}
                    key={item?.slug}
                  >
                    {item?.name}
                  </CategoryItem>
                ))}
              </>
            )}
          </FilterBlock>

          <ExpandedBlock
            title={brands?.name}
            loading={brands?.loading}
            filterId={brands?.id}
            isItemSelected={isItemSelected}
            changeFilters={changeFilters}
          >
            {brands?.children || []}
          </ExpandedBlock>

          {attributes?.map((item) => getFilter(item, isItemSelected, changeFilters))}

          <FilterBlock>
            <BlockTitle>Rewards</BlockTitle>
            <BlockItem>
              <ItemName
                style={{alignItems: 'stretch'}}
                onClick={() =>
                  changeFilters(
                    {id: 'productsWithShopcoins', unic: true},
                    String(!isItemSelected('productsWithShopcoins', 'true'))
                  )
                }
              >
                <span style={{marginRight: 12}}>Products with Shopcoins</span>
                <Switcher isActive={isItemSelected('productsWithShopcoins', 'true')} />
              </ItemName>
            </BlockItem>
          </FilterBlock>

          <FilterBlock>
            <BlockTitle>Price</BlockTitle>
            {price?.children?.slice(0, 5)?.map((item) => (
              <BlockItem key={item.id}>
                <Input
                  style={{marginBottom: 0}}
                  onChange={() => changeFilters(price, item.id, item?.values)}
                  type="radio"
                  name="price"
                  id={`${price?.id}-${item.id}`}
                  value={item.name}
                  checked={_.isEmpty(filters?.price) ? price?.defaultId === item.id : isItemSelected(price.id, item.id)}
                />
                <ItemName style={{marginLeft: 12}}>
                  <span>{item.name}</span>
                </ItemName>
              </BlockItem>
            ))}
            <PriceSelect
              currencyType={price?.type}
              currentPrice={getCurrentPrice()}
              min={price?.min}
              max={price?.max}
              disabled={!isItemSelected(price?.id, 'custom')}
              onChange={(value) => changeFilters({id: 'price', unic: true}, 'custom', value)}
            />
          </FilterBlock>

          {/* Location disable for now */}

          {/* <FilterBlock>
            <BlockTitle>Shop location</BlockTitle>
            {shopLocation?.children?.map((item) => (
              <BlockItem key={item.id}>
                <Input
                  style={{marginBottom: 0}}
                  onChange={() => changeFilters(shopLocation, item.id)}
                  type="radio"
                  name="location"
                  id={`${shopLocation.id}-${item.id}`}
                  value={item.id}
                  checked={
                    _.isEmpty(filters?.shopLocation)
                      ? shopLocation.defaultId === item.id
                      : isItemSelected(shopLocation.id, item.id)
                  }
                />
                <ItemName style={{marginLeft: 12}}>
                  <span>{item.name}</span>
                </ItemName>
              </BlockItem>
            ))}
            <Select
              showSearch
              allowClear
              name="customLocation"
              onChange={(itemId) => changeFilters(shopLocationCustom, itemId)}
              value={filters?.[shopLocationCustom.id]}
              options={shopLocationCustom?.children}
              labelKey="name"
              valueKey="id"
              disabled={!isItemSelected(shopLocation.id, 'custom')}
            />
          </FilterBlock> */}

          <FilterBlock>
            <BlockTitle>Offers</BlockTitle>
            {offers?.children?.map((item) => (
              <BlockItem key={item.id}>
                <Input
                  style={{marginBottom: 0}}
                  onChange={() => changeFilters(offers, item.id)}
                  type="checkbox"
                  id={`${offers.id}-${item.id}`}
                  checked={isItemSelected(offers.id, item.id)}
                  name={offers.id}
                />
                <ItemName style={{marginLeft: 12}}>
                  <span>{item.name}</span>
                </ItemName>
              </BlockItem>
            ))}
          </FilterBlock>
        </FiltersContent>
        <FiltersFooter>
          <BackButton onClick={() => setGlobalState.setFilterModal(false)}>Back</BackButton>
          <ApplyButton onClick={onSubmit}>Apply</ApplyButton>
        </FiltersFooter>
      </Wrapper>
    </StyledModal>
  );
};

export default Filters;
