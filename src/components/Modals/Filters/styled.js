import styled, {css} from 'styled-components/macro';
import {Modal} from 'antd';
import {primaryColor} from 'constants/colors';

export const StyledModal = styled(Modal)`
  & .ant-modal-content {
    border-radius: 8px;
    margin-bottom: 4px;
    overflow: hidden;
  }
`;

export const Wrapper = styled.div`
  background-color: #fff;
  width: 312px;
  display: flex;
  flex-direction: column;
  height: 100%;
`;

export const FiltersHeader = styled.div`
  padding: 16px 24px 0 24px;
`;

export const FiltersContent = styled.div`
  flex-grow: 1;
  overflow-y: auto;
  padding: 22px 0 0 0;

  &::-webkit-scrollbar {
    display: none;
  }
  -ms-overflow-style: none;
  scrollbar-width: none;
`;

export const FiltersFooter = styled.div`
  padding: 16px 28px;
  border-top: 1px solid #e4e4e4;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const CloseContainer = styled.div`
  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const Title = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  color: #a7a7a7;
`;

export const FilterBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 24px 40px;
`;

export const BlockTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #000;
  margin-bottom: 20px;
`;

export const BlockItem = styled.div`
  margin-bottom: 10px;
  &:last-child {
    margin-bottom: 0;
  }
  display: flex;
  align-items: center;
`;

export const ShowMoreButton = styled.div`
  margin: 2px 0 12px;
  display: flex;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  color: #000000;
  cursor: pointer;

  & svg {
    transition: 0.5s transform;
    ${({expanded}) =>
      expanded &&
      css`
        transform: rotate(180deg);
      `}
  }
`;

export const CategoryItem = styled(BlockItem)`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #464646;
  cursor: pointer;
  transition: background 0.4s;
  margin: 0;
  padding: 8px 24px;

  &:hover {
    background: #f5f5f5;
  }

  ${({parent}) =>
    parent &&
    css`
      &:hover {
        background: #fff;
      }
      & svg {
        transform: rotate(180deg);
        margin-right: 12px;
        margin-bottom: 3px;
      }
    `};

  ${({selected}) =>
    selected &&
    css`
      color: #000;
      background: #f5f5f5;
    `};

  ${({subItem, selected}) =>
    subItem &&
    css`
      padding: 5px 24px 5px 40px;
      background: white;
      text-decoration: ${selected ? 'underline' : 'none'};
      &:hover {
        background: white;
        color: #000;
      }
    `};
`;

export const BackButton = styled.div`
  padding: 8px 42px;
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #999;
  cursor: pointer;
`;

export const ApplyButton = styled(BackButton)`
  background: #ed484f;
  border-radius: 24px;
  flex-shrink: 0;
  color: #ffffff;
`;

export const ItemName = styled.div`
  display: flex;
  align-items: baseline;
  white-space: nowrap;
  overflow: hidden;
  max-width: 100%;
  width: 100%;

  & span {
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const ExpandedBlock = styled.div`
  height: 0;
  overflow: hidden;
  transition: 1s all;
  opacity: 0;

  ${({expanded}) =>
    expanded &&
    css`
      opacity: 1;
      height: fit-content;
    `};
`;

export const SizesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: -10px;
`;

export const SizeItem = styled.div`
  padding: 8px 13px;
  border: 1px solid #cccccc;
  border-radius: 22px;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  color: #666666;
  margin: 10px 5px 0 0;
  cursor: pointer;
  transition: 0.4s all;

  ${({selected}) =>
    selected &&
    css`
      background: #efefef;
      border: 1px solid #efefef;
      color: #000;
    `};
`;
