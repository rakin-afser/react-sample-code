import React, {useState} from 'react';
import tinycolor from 'tinycolor2';
import _ from 'lodash';

import Icon from 'components/Icon';
import Input from 'components/Input';
import Loader from 'components/Loader';
import {
  FilterBlock,
  BlockTitle,
  BlockItem,
  ItemName,
  ShowMoreButton,
  ExpandedBlock
} from 'components/Modals/Filters/styled';
import {Circle} from 'components/Modals/Filters/components/ColorsBlock/styled';

const ColorsBlock = ({
  displayedItems = 5,
  title,
  filterId,
  children = [],
  loading,
  isItemSelected = () => {},
  changeFilters = () => {}
}) => {
  const [expanded, setExpanded] = useState(false);

  if (!loading && _.isEmpty(children)) return null;

  return (
    <FilterBlock>
      <BlockTitle>{title}</BlockTitle>
      {loading ? (
        <Loader wrapperWidth="100%" wrapperHeight="50px" dotsSize="10px" />
      ) : (
        children?.slice(0, displayedItems)?.map((item) => {
          const color = tinycolor(item.name);
          return (
            <BlockItem key={item.slug}>
              <Input
                style={{marginBottom: 0}}
                onChange={() => changeFilters({id: filterId}, item.slug)}
                type="checkbox"
                id={`${filterId}-${item.slug}`}
                checked={isItemSelected(filterId, item.slug)}
                name={filterId}
              />
              <ItemName style={{marginLeft: 12}}>
                <Circle
                  border={color.isValid() ? color.toName() === 'white' : false}
                  color={color.isValid() ? color.toHexString() : '#000'}
                />
                <span style={{marginLeft: 8, textTransform: 'capitalize'}}>
                  {(color.isValid() && color.toName()) || item.name}
                </span>
              </ItemName>
            </BlockItem>
          );
        })
      )}
      {children?.length > displayedItems && (
        <ShowMoreButton expanded={expanded} onClick={() => setExpanded(!expanded)}>
          {expanded ? 'Show Less' : 'Show More'}
          <Icon type="arrowDown" color="#000" />
        </ShowMoreButton>
      )}
      <ExpandedBlock expanded={expanded}>
        {children?.slice(displayedItems)?.map((item) => {
          var color = tinycolor(item.name);
          return (
            <BlockItem key={item.slug}>
              <Input
                style={{marginBottom: 0}}
                onChange={() => changeFilters({id: filterId}, item.slug)}
                type="checkbox"
                id={`${filterId}-${item.slug}`}
                checked={isItemSelected(filterId, item.slug)}
                name={filterId}
              />
              <ItemName style={{marginLeft: 12}}>
                <Circle
                  border={color.isValid() ? color.toName() === 'white' : false}
                  color={color.isValid() ? color.toHexString() : '#000'}
                />
                <span style={{marginLeft: 8, textTransform: 'capitalize'}}>
                  {(color.isValid() && color.toName()) || item.name}
                </span>
              </ItemName>
            </BlockItem>
          );
        })}
      </ExpandedBlock>
    </FilterBlock>
  );
};

export default ColorsBlock;
