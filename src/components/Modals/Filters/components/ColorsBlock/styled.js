import styled from 'styled-components/macro';

export const Circle = styled.div`
  background: ${({color}) => (color ? color : '#000')};
  border-radius: 50%;
  flex-shrink: 0;
  width: 12px;
  height: 12px;
  margin-top: 2px;
  border: ${({border}) => (border ? '1px solid #c4c4c4' : 'none')};
`;
