import styled from 'styled-components/macro';
import {Modal, Row, Select} from 'antd';
import Input from 'components/Input';
import {headerShadowColor, menuTitleColor, primaryColor, secondaryTextColor} from 'constants/colors';

export const ModalStyled = styled(Modal)`
  & .ant-modal-content {
    box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
    border-radius: 8px;
    overflow: hidden;
  }

  .ant-modal-header {
    padding: 16px 24px 8px;
    border-bottom: none;
  }

  .ant-modal-title {
    font-size: 20px;
    color: #000;
    letter-spacing: 0.019em;
  }

  .ant-modal-body {
    padding: 0 24px 0 !important;
  }
`;

export const InputStyled = styled(Input)`
  margin-bottom: 10px;

  input {
    padding-left: 15px;

    &[type='textarea'] {
      height: 61px;
    }
  }
`;

export const Hr = styled.hr`
  margin-left: -24px;
  margin-right: -24px;
  border: none;
  height: 1px;
  background-color: ${menuTitleColor};
`;

export const ChecksWrapper = styled(Row)`
  margin-bottom: 8px;
  padding: 10px 0;

  & .ant-radio-wrapper {
    span + span {
      position: relative;
      top: 1px;
    }
  }
`;

export const Span = styled.span`
  margin-right: 18px;
`;

export const DateWrapper = styled.div`
  margin-top: 5px;
  margin-bottom: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const SelectStyled = styled(Select)`
  & .ant-select-selection {
    margin-right: 7px;

    &.ant-select-selection--single {
      border-radius: 0;
      height: 40px;
    }
  }

  & .ant-select-selection__rendered {
    margin-left: 15px;
    line-height: 41px;
  }

  & .ant-select-arrow {
    margin-top: -2px;
    transition: ease 0.3s;
  }

  &.ant-select-open {
    & .ant-select-arrow {
      transform: rotate(180deg);
    }
  }
`;

export const ButtonsWrapper = styled.div`
  padding-top: 9px;
  padding-bottom: 15px;
  display: flex;
  justify-content: flex-end;
  align-items: center;

  button {
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 14px;
    font-family: 'Helvetica Neue', sans-serif;
    color: #333333;
    line-height: 1.1;
    border: none;
    transition: ease 0.6s;
    box-shadow: none;
    
    &:hover {
      color ${primaryColor};
    }

    &:nth-of-type(2) {
      margin-left: 36px;
      padding: 0 34px;
      border-radius: 24px;
      background-color: ${primaryColor};
      color: #fff;
      
      &:hover {
        background-color: ${headerShadowColor};
        color: ${primaryColor};
      }
    }
  }
`;

export const EditAvatar = styled.i`
  position: absolute;
  bottom: 6px;
  right: 11px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 33px;
  height: 33px;
  background: #c3c3c3;
  border-radius: 50%;
  border: 3px solid #ffffff;
  transition: ease 0.3s;

  &:hover {
    cursor: pointer;
    background: ${secondaryTextColor};
  }

  ${(p) =>
    p.isMobile &&
    `
    bottom: 1px;
    right: 4px;
    background: ${primaryColor};
    width: 22px;
    height: 22px;
    border: 2px solid #ffffff;

    & svg {
      width: 9px;
    }
  `}
`;
