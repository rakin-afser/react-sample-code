import React, {useState, useRef} from 'react';
import axiosClient from 'axiosClient';
import {Row, Col, Button, Select, Switch, Radio} from 'antd';
import PropTypes from 'prop-types';
import {useMutation, gql} from '@apollo/client';
import avatarPlaceholder from 'images/avatarPlaceholder.png';
import UserAvatar from 'containers/MyProfile/components/SIdebar/Components/Avatar';
import {dateFns, userId} from 'util/heplers';
import Icon from 'components/Icon';
import {
  ModalStyled,
  InputStyled,
  Hr,
  ChecksWrapper,
  Span,
  DateWrapper,
  SelectStyled,
  ButtonsWrapper,
  EditAvatar
} from './styled';
import {ReactComponent as SelectIcon} from './img/selectIcon.svg';

const UPDATE_USER_DATA = gql`
  mutation UpdateUserData(
    $userId: ID!
    $description: String
    $lastName: String
    $firstName: String
    $profilePicture: String
    $country: String
    $jobTitle: String
    $dateOfBirth: String
    $gender: String
    $education: String
    $company: String
    $showDateOfBirth: Boolean
  ) {
    updateUser(
      input: {
        id: $userId
        description: $description
        lastName: $lastName
        firstName: $firstName
        profile_picture: $profilePicture
        country: $country
        job_title: $jobTitle
        date_of_birth: $dateOfBirth
        gender: $gender
        education: $education
        company: $company
        show_date_of_birth: $showDateOfBirth
      }
    ) {
      clientMutationId
    }
  }
`;

const EditProfile = ({edit, setEdit, data}) => {
  const [updateUser, {data: userData, loading: userLoading, error: userError}] = useMutation(UPDATE_USER_DATA, {
    variables: {userId}
  });

  const [formData, setFormData] = useState({
    avatar: data?.user?.profile_picture || avatarPlaceholder,
    avatarToUpload: null,
    country: data.user?.country,
    fName: data?.user?.firstName,
    lName: data?.user?.lastName,
    userName: data?.user?.email,
    gender: data?.user?.gender,
    dateOfBirth: data?.user?.date_of_birth,
    showDate: data?.user?.show_date_of_birth,
    aboutMe: data?.user?.description,
    jobTitle: data?.user?.job_title,
    company: data?.user?.company,
    education: data?.user?.education
  });

  const [date, setDate] = useState({
    year: 2021,
    month: 'January',
    day: 1
  });

  const refInput = useRef(null);

  const {Option} = Select;
  const {years, monthsName, daysPerMonth} = dateFns;

  const yearsList = years(61);
  const monthsList = monthsName();

  const onFileUpload = () => {
    refInput.current.click();
  };

  const onChangeFile = (e) => {
    const reader = new FileReader();
    const file = e.target.files[0];

    const formDataImage = new FormData();
    formDataImage.append('file', e.target.files[0]);

    reader.onloadend = () => {
      setFormData({...formData, avatar: reader.result, avatarToUpload: formDataImage});
    };

    reader.readAsDataURL(file);
  };

  const onChange = (e) => {
    setFormData({...formData, [e.target.name]: e.target.value});
  };

  const onDateChange = (name, value) => {
    setDate({...date, [name]: value});
    setFormData({...formData, dateOfBirth: `${date.month} ${date.day}, ${date.year}`});
  };

  const onSwitch = (checked) => {
    setFormData({...formData, showDate: checked});
  };

  const updateUserFunc = (response) => {
    updateUser({
      variables: {
        description: formData.aboutMe,
        firstName: formData.fName,
        lastName: formData.lName,
        userName: formData.userName,
        profilePicture: response?.data?.source_url || null,
        country: formData.country,
        jobTitle: formData.jobTitle,
        dateOfBirth: `${date.day} ${date.month} ${date.year}`,
        showDateOfBirth: Boolean(formData.showDate),
        gender: formData.gender,
        company: formData.company,
        education: formData.education
      },
      update(cache, {usrData}) {
        cache.modify({
          fields: {
            user() {
              return usrData;
            }
          }
        });
      }
    });
  };

  const onFormSubmit = () => {
    if (formData.avatarToUpload) {
      axiosClient({
        method: 'post',
        headers: {
          'Content-Disposition': `filename=${formData.avatarToUpload.name}`
        },
        data: formData.avatarToUpload
      })
        .then((response) => {
          if (response) {
            updateUserFunc(response);
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.log(error);
        });
    } else {
      updateUserFunc();
    }

    setEdit(false);
  };

  return (
    <ModalStyled
      title="Edit Profile"
      centered
      width="459px"
      bodyStyle={{padding: '12px 35px 8px 42px', top: 0}}
      visible={edit}
      onCancel={() => setEdit(false)}
      footer={null}
    >
      <form onSubmit={onFormSubmit}>
        <Row>
          <Col span={8} style={{paddingTop: 18}}>
            <UserAvatar setEdit={onFileUpload} img={formData.avatar} small />
            <EditAvatar onClick={() => refInput.current.click()}>
              <Icon type="pencil" />
              <input ref={refInput} type="file" onChange={onChangeFile} style={{display: 'none'}} />
            </EditAvatar>
          </Col>
          <Col span={16} style={{paddingLeft: 11}}>
            <InputStyled
              name="fName"
              onChange={onChange}
              placeholder="Enter your first name"
              label="First Name"
              value={formData.fName}
            />
            <InputStyled
              name="lName"
              onChange={onChange}
              placeholder="Enter your last name"
              label="Last Name"
              value={formData.lName}
            />
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <InputStyled
              name="userName"
              onChange={onChange}
              placeholder="Enter your user name"
              label="Username"
              value={formData.userName}
              disabled
            />
          </Col>
        </Row>
        <Hr />
        <ChecksWrapper>
          <Radio.Group name="gender" onChange={onChange} value={formData.gender}>
            <Span>I am:</Span>
            <Radio value="male">Male</Radio>
            <Radio value="female">Female</Radio>
            <Radio value="others">Others</Radio>
          </Radio.Group>
        </ChecksWrapper>
        <Row>Date of birth</Row>
        <DateWrapper>
          <SelectStyled
            suffixIcon={<SelectIcon />}
            value={date.month}
            onChange={(value) => onDateChange('month', value)}
            name="month"
          >
            {monthsList.map((el, idx) => (
              <Option value={el} key={idx}>
                {el}
              </Option>
            ))}
          </SelectStyled>
          <SelectStyled
            suffixIcon={<SelectIcon />}
            value={date.day}
            onChange={(value) => onDateChange('day', value)}
            name="day"
          >
            {daysPerMonth(date.year, date.month).map((el) => (
              <Option value={el} key={el}>
                {el}
              </Option>
            ))}
          </SelectStyled>
          <SelectStyled
            style={{marginRight: 6}}
            suffixIcon={<SelectIcon />}
            value={date.year}
            onChange={(value) => onDateChange('year', value)}
            name="year"
          >
            {yearsList.map((el) => (
              <Option value={el} key={el}>
                {el}
              </Option>
            ))}
          </SelectStyled>
          <Switch checked={Boolean(formData?.showDate)} onChange={onSwitch} />
          <span style={{marginLeft: 8}}>Show</span>
        </DateWrapper>
        <Row>
          <InputStyled value={formData.aboutMe} onChange={onChange} type="textarea" label="About me" name="aboutMe" />
        </Row>
        <Row>
          <Col>
            <InputStyled
              onChange={onChange}
              placeholder="Enter your user Job Title"
              label="Job Title"
              name="jobTitle"
              value={formData.jobTitle}
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <InputStyled
              onChange={onChange}
              placeholder="Enter your company name"
              label="Company"
              value={formData.company}
              name="company"
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <InputStyled
              onChange={onChange}
              placeholder="Enter your education"
              label="Education"
              name="education"
              value={formData.education}
            />
          </Col>
        </Row>
        <Hr />
        <ButtonsWrapper>
          <Button onClick={() => setEdit(false)}>Cancel</Button>
          <Button type="submit" onClick={onFormSubmit}>
            Save
          </Button>
        </ButtonsWrapper>
      </form>
    </ModalStyled>
  );
};

EditProfile.propTypes = {
  setEdit: PropTypes.func.isRequired,
  edit: PropTypes.bool.isRequired
};

export default EditProfile;
