import React, {useRef} from 'react';
import {createPortal} from 'react-dom';
import {CSSTransition} from 'react-transition-group';

import useOutsideClick from 'util/useOutsideClick';
import {ReactComponent as CloseIcon} from 'images/svg/modal-close.svg';
import {Wrapper, Modal, ModalHeader} from 'components/Modals/ModalContainer/styled';

const ModalContainer = ({isOpen, onClose, title, children, hideTitle = false, closeIconStyles={color: '#000'}}) => {
  const modalRef = useRef();

  useOutsideClick(modalRef, () => {
    if (isOpen) {
      onClose();
    }
  });

  return createPortal(
    <CSSTransition in={isOpen} timeout={500} classNames="desktopModal" unmountOnExit>
      <Wrapper>
        <Modal ref={modalRef}>
          <ModalHeader hideTitle={hideTitle}>
            {!hideTitle && title}
            <CloseIcon onClick={onClose} style={closeIconStyles} />
          </ModalHeader>
          {children}
        </Modal>
      </Wrapper>
    </CSSTransition>,
    document.getElementById('modal')
  );
};

export default ModalContainer;
