import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-start;
  padding-top: 81px;
  background-color: rgba(0, 0, 0, 0.45);
  z-index: 1000;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  cursor: default;
  overflow: hidden;
  opacity: 0;
  transition: 0.5s;

  &.desktopModal-enter-active {
    opacity: 1;
  }

  &.desktopModal-enter-done {
    opacity: 1;
  }
`;

export const Modal = styled.div`
  background: #ffffff;
  display: flex;
  flex-direction: column;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  width: 425px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const ModalHeader = styled.div`
  display: flex;
  padding: 16px 24px;
  border-bottom: 1px solid #e4e4e4;
  position: relative;

  & svg {
    cursor: pointer;
    position: absolute;
    top: 10px;
    right: 10px;
    width: 12px;
    height: 12px;
  }

  ${({hideTitle}) =>
    hideTitle &&
    `
    border-bottom: none;
    padding: 0;
  `}

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    & svg {
      right: unset;
      left: 10px;
    }
  `}
`;
