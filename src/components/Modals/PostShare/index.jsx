import React from 'react';
import PropTypes from 'prop-types';

import {
  Item
} from './styled';

import ModalContainer from 'components/Modals/ModalContainer';

const PostShare = ({title, cancel, isOpen, onClose, buttons}) => {
  const closeModal = () => {
    onClose();
  };

  return (
    <ModalContainer
      onClose={closeModal}
      isOpen={isOpen}
      hideTitle
      closeIconStyles={{
        width: 16,
        height: 16,
        top: 6,
        right: '-28px',
        color: '#fff'
      }}
    >
      {title ? <Item style={{cursor: 'default'}} dangerouslySetInnerHTML={{__html: title}}/> : null}
      {
        buttons && buttons.length 
        ? 
          buttons.map((button, index) => {
            return (
              <Item 
                key={`action_${index}`} 
                style={button.style}
                onClick={button.onClick ? button.onClick : () => {}}
                dangerouslySetInnerHTML={{__html: button.title}}
              />
            );
          })
        : null
      }
      {cancel ? <Item onClick={onClose} dangerouslySetInnerHTML={{__html: cancel}}/> : null}
    </ModalContainer>
  );
};

PostShare.defaultProps = {
  title: 'Share to...',
  cancel: 'Cancel',
  isOpen: false,
  buttons: [],
  onClose: () => {}
};

PostShare.propTypes = {
  title: PropTypes.string,
  cancel: PropTypes.string,
  isOpen: PropTypes.bool,
  buttons: PropTypes.arrayOf(PropTypes.object),
  onClose: PropTypes.func
};

export default PostShare;
