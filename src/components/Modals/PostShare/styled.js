import styled from 'styled-components/macro';

export const Item = styled.div`
  padding: 17px 24px;
  cursor: pointer;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #000000;
  display: flex;
  align-items: center;

  &:not(:last-child) {
    border-bottom: 1px solid #efefef;
  }

  .icon {
    width: 24px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 12px;
  }
`;
