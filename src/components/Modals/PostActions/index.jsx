import React from 'react';
import PropTypes from 'prop-types';

import {
  Item
} from './styled';

import ModalContainer from 'components/Modals/ModalContainer';

const PostActions = ({isOpen, onClose, buttons}) => {
  const closeModal = () => {
    onClose();
  };

  return (
    <ModalContainer
      onClose={closeModal}
      isOpen={isOpen}
      hideTitle
      closeIconStyles={{
        width: 16,
        height: 16,
        top: 6,
        right: '-28px',
        color: '#fff'
      }}
    >
      {
        buttons && buttons.length 
        ? 
          buttons.map((button, index) => {
            return (
              <Item 
                key={`action_${index}`} 
                style={button.style}
                onClick={button.onClick ? button.onClick : () => {}}
                dangerouslySetInnerHTML={{__html: button.title}}
              />
            );
          })
        : null
      }
    </ModalContainer>
  );
};

PostActions.defaultProps = {
  isOpen: false,
  buttons: [],
  onClose: () => {}
};

PostActions.propTypes = {
  isOpen: PropTypes.bool,
  buttons: PropTypes.arrayOf(PropTypes.object),
  onClose: PropTypes.func
};

export default PostActions;
