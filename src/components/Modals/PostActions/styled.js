import styled from 'styled-components/macro';

export const Item = styled.div`
  padding: 17px 30px;
  cursor: pointer;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #000000;

  &:not(:last-child) {
    border-bottom: 1px solid #efefef;
  }
`;
