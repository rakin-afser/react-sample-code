import React, {useState} from 'react';
import _ from 'lodash';

import {
  OtherProductsWrapper,
  ProductContainer,
  ProductImage,
  ProductInfo,
  Text,
  LikeButton,
  MoreButton
} from 'components/Modals/PostView/components/OtherProducts/styled';
import {Image, Title} from 'components/Modals/PostView/styled';
import Icon from 'components/Icon';
import productPlaceholder from 'images/placeholders/product.jpg';
import IconHeart from 'assets/Heart';
import IconFilledHeart from 'assets/FilledHeart';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';
import AddToList from 'components/AddToList';

const ProductCard = ({product = {}, onClick = () => {}}) => {
  const {databaseId, name, image, isLiked, slug, salePrice, regularPrice, price, shippingType} = product;
  const [showAddToList, setShowAddToList] = useState(false);
  const [triggerLike] = useAddUpdateLikedProductMutation({
    variables: {input: {id: databaseId}},
    product
  });

  const onWishClick = () => setShowAddToList(true);

  return (
    <ProductContainer>
      <ProductImage>
        <Image onClick={() => onClick(slug)} src={image?.sourceUrl || productPlaceholder} />
      </ProductImage>
      <ProductInfo>
        <Text style={{color: '#000', flexGrow: '1'}} onClick={() => onClick(slug)}>
          {name?.length > 60 ? `${name.slice(0, 60)}...` : name}
        </Text>
        {shippingType && <Text style={{fontSize: 12, color: '#8F8F8F', flexGrow: '1'}}>{shippingType}</Text>}
        <Text style={{flexGrow: '2'}}>
          {!!salePrice && <span style={{fontWeight: 500, color: '#000'}}>{salePrice}&nbsp;</span>}&nbsp;
          {salePrice ? <span style={{textDecoration: 'line-through'}}>{regularPrice}</span> : <span>{price}</span>}
        </Text>
        <LikeButton isLiked={isLiked} onClick={triggerLike}>
          <IconHeart width={14} height={11} isLiked={false} />
          <IconFilledHeart width={14} height={11} />
        </LikeButton>
      </ProductInfo>
      <MoreButton onClick={onWishClick}>
        <Icon isWished={false} type="bookmark" width={17} height={15} color="#7C7E82" />
      </MoreButton>
      <AddToList
        wrapperStyle={{bottom: '8px', right: '8px'}}
        showAddToList={showAddToList}
        setShowAddToList={setShowAddToList}
        productId={databaseId}
      />
    </ProductContainer>
  );
};

const OtherProducts = ({productsData, stickyTitle, displayedProducts = 2, onClick, wrapperStyle}) => {
  return (
    !_.isEmpty(productsData) && (
      <OtherProductsWrapper style={wrapperStyle}>
        <Title sticky={stickyTitle}>Other Products</Title>
        {productsData?.slice(0, displayedProducts).map((item, index) => (
          <ProductCard onClick={onClick} key={index} product={item?.node} />
        ))}
      </OtherProductsWrapper>
    )
  );
};

export default OtherProducts;
