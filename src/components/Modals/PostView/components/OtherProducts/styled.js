import styled, {css} from 'styled-components/macro';

export const OtherProductsWrapper = styled.div`
  margin: 30px 10px 0 0;
`;

export const ProductContainer = styled.div`
  display: flex;
  border: 1px solid #eeeeee;
  padding: 13px 11px;
  justify-content: flex-start;
  align-items: stretch;
  margin-bottom: -1px;
  position: relative;
`;

export const ProductImage = styled.div`
  width: 120px;
  height: 120px;
  cursor: pointer;

  & img {
    width: 120px;
    height: 120px;
  }
`;

export const ProductInfo = styled.div`
  margin-left: 15px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
`;

export const Text = styled.div`
  font-family: Helvetica Neue;
  font-size: 14px;
  color: #999999;
  cursor: pointer;
`;

export const LikeButton = styled.button`
  background: #ffffff;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  border-radius: 50%;
  width: 28px;
  height: 28px;
  justify-content: center;
  align-items: center;
  display: flex;
  transition: all 0.5s;

  ${({isLiked}) =>
    isLiked
      ? css`
          svg.iconHeart {
            display: none;
          }

          svg.iconFilledHeart {
            display: block;
          }
        `
      : css`
          svg.iconHeart {
            display: block;
          }

          svg.iconFilledHeart {
            display: none;
          }
        `}
`;

export const MoreButton = styled.div`
  position: absolute;
  right: 12px;
  bottom: 0px;
  cursor: pointer;
  right: 12px;
  bottom: 7px;
`;
