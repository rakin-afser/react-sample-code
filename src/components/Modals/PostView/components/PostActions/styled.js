import styled from 'styled-components/macro';

export const PostActionsWrapper = styled.div`
  display: flex;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #8f8f8f;
  flex-grow: 1;
  justify-content: flex-start;

  & > div {
    margin-right: 0;
    height: 26px;
    background: #f5f5f5;
    border: 0.5px solid #cccccc;
    box-sizing: border-box;
    border-radius: 22px;
    padding: 0 10px;
    cursor: pointer;
    display: flex;
    align-items: center;

    i,
    svg {
      max-width: 15px;
      max-height: 15px;
    }

    span {
      font-size: 10px;
      line-height: 1;
      color: #999999;
      margin: 1px 0 0 5.5px;
      position: relative;
      top: -1px;
      line-height: 13px;
    }

    &:not(:last-child) {
      margin-right: 10px;
    }
  }

  & svg {
    cursor: pointer;
  }
`;
