import React from 'react';
import SwiperCore, {Navigation, Pagination} from 'swiper';
import {SwiperSlide} from 'swiper/react';
import VideoPlayer from 'components/VideoPlayer';

import Icon from 'components/Icon';
import {
  SliderWrapper,
  SliderContent,
  ImageSlide,
  VideoSlide,
  FullscreenButton
} from 'components/Modals/PostView/components/Slider/styled';
import productPlaceholder from 'images/placeholders/product.jpg';

// Swiper modules
SwiperCore.use([Navigation, Pagination]);

const Slider = ({data = [], onChange = () => {}, swiper, setFullScreen, setSwiper = () => {}, slideStyle}) => {
  return (
    <SliderWrapper>
      <SliderContent
        slidesPerView={1}
        spaceBetween={20}
        activeIndex={1}
        loop
        onSlideChange={(slide) => onChange(slide?.realIndex)}
        pagination={{
          clickable: true,
          el: '.swiper-pagination'
        }}
        navigation={{
          nextEl: '.custom-swiper-button-next',
          prevEl: '.custom-swiper-button-prev'
        }}
        onSwiper={(swiper) => setSwiper(swiper)}
      >
        {data?.map((item, index) =>
          item?.type === 'video' && !item?.source ? null : (
            <SwiperSlide key={index} style={slideStyle}>
              {item?.type === 'video' && (
                <VideoSlide>
                  <FullscreenButton onClick={setFullScreen}>
                    <Icon type="expand" color="#fff" />
                  </FullscreenButton>
                  <VideoPlayer src={item?.source} />
                </VideoSlide>
              )}
              {['image', 'product'].includes(item?.type) && <ImageSlide src={item?.source || productPlaceholder} />}
            </SwiperSlide>
          )
        )}
      </SliderContent>
      <div className="swiper-pagination" />
      <div className="custom-swiper-button-prev">
        <Icon type="arrow" width={11} height={18} color="#fff" />
      </div>
      <div className="custom-swiper-button-next">
        <Icon type="arrow" width={11} height={18} color="#fff" />
      </div>
    </SliderWrapper>
  );
};

export default Slider;
