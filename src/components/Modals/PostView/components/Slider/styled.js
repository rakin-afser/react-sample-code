import {primaryColor} from 'constants/colors';
import styled, {css} from 'styled-components/macro';
import {Swiper} from 'swiper/react';

export const SliderContent = styled(Swiper)`
  height: 100%;
`;

export const ImageSlide = styled.img`
  width: 100%;
  height: 100%;
  object-fit: contain;
`;

export const VideoSlide = styled.div`
  width: 100%;
  height: 100%;

  & > div {
    width: 100%;
    height: 100%;
  }

  & .video-js {
    width: 100% !important;
    height: 100% !important;
  }
`;

export const SliderWrapper = styled.div`
  width: ${({small}) => (small ? 'calc(100% - 54px)' : '100%')};
  height: ${({small}) => (small ? 452 : 598)}px;
  padding-bottom: ${({small}) => (small ? 0 : '30px')};
  margin-right: ${({small}) => (small ? '54px' : 0)};
  position: relative;

  .swiper-container {
    &-horizontal > .swiper-pagination-bullets {
      font-size: 0;
      line-height: 1;
      bottom: 0;
      margin: 8px 0 16px 0;
    }

    .swiper-pagination-bullet {
      transition: background-color 0.3s ease;
      background-color: #e4e4e4;
      opacity: 1;

      &-active {
        background-color: ${primaryColor};
      }
    }
  }

  .swiper-button-disabled {
    opacity: 0.3;
  }

  .custom-swiper-button-next,
  .custom-swiper-button-prev {
    background: #00000030;
    border-radius: 3px;
    width: 37px;
    height: 37px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    z-index: 5;
    transition: background-color 0.3s ease, opacity 0.3s ease;

    &:not(.swiper-button-disabled) {
      cursor: pointer;

      &:hover {
        background: #00000060;
      }
    }
  }

  .custom-swiper-button-prev {
    left: 8px;
    svg {
      margin: 9px 11px;
      transform: scale(-1, 1);
    }
  }

  .custom-swiper-button-next {
    right: 8px;
    svg {
      margin: 9px 14px;
    }
  }
`;
