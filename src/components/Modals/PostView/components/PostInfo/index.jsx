import React from 'react';

import {Wrapper, Title, Description} from './styled';
import Scrollbar from 'react-scrollbars-custom';
import ExpandedText from 'components/ExpandedText';

const PostInfo = ({title, description}) => {
  return (
    <Wrapper>
      <Title>{title?.length > 60 ? `${title.slice(0, 60)}...` : title}</Title>
      {/* <Scrollbar disableTracksWidthCompensation style={{height: 280}}> */}
      <Description>
        <ExpandedText
          isExpanded={false}
          margin={'0'}
          moreText="More"
          lessText="Less"
          text={description}
          divider={113}
        />
      </Description>
      {/* </Scrollbar> */}
    </Wrapper>
  );
};

export default PostInfo;
