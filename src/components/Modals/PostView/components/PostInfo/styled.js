import styled from 'styled-components/macro';

export const Wrapper = styled.div``;

export const Title = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  letter-spacing: 0.019em;
  color: #000000;
  margin: 11px 0 17px;
`;

export const Description = styled.div`
  padding: 0 20px 0 0;

  & > div {
    text-align: left;
  }
`;
