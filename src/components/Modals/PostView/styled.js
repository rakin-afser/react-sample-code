import styled, {css} from 'styled-components/macro';
import {primaryColor} from 'constants/colors';
import {Modal} from 'antd';

export const Wrapper = styled.div`
  border-radius: 8px;
  padding: 21px 22px 44px;
  position: relative;
`;

export const CloseWrapper = styled.div`
  cursor: pointer;
  position: absolute;
  top: 12px;
  right: -30px;

  & svg {
    width: 16px;
    height: 16px;

    path {
      fill: #fff;
    }
  }
`;

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  padding: ${({padding}) => padding || '0'};
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  width: ${({width}) => width || '50%'};
  flex-shrink: 0;
  padding: ${({padding}) => padding || '0'};
`;

export const MediaWrapper = styled.div`
  flex-shrink: 0;
  display: flex;

  ${({fullScreen}) =>
    fullScreen &&
    css`
      width: 100%;
      height: 568px;

      & > div {
        height: 568px;
      }
    `}
`;

export const SliderWrapper = styled.div`
  flex-grow: 1;
  max-width: 100%;
  height: 452px;

  ${({fullScreen}) =>
    fullScreen &&
    css`
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
    `}
`;

export const Preview = styled.div`
  width: 40px;
  margin: 0 43px 0 5px;
  flex-shrink: 0;
  user-select: none;

  ${({fullScreen}) =>
    fullScreen &&
    css`
      display: none;
    `}
`;

export const UserInfoContainer = styled.div`
  margin: 0 0 30px;
`;

export const CommentsWrapper = styled.div`
  width: 100%;
  height: 262px;
  max-width: 488px;
  display: flex;
  overflow-x: hidden;
  -ms-overflow-style: none;
  overflow: -moz-scrollbars-none;
  &::-webkit-scrollbar {
    display: none;
  }
`;

export const PreviewWrapper = styled.div`
  position: relative;
  background: #ddd;
  cursor: pointer;
  width: 40px;
  height: 40px;
  margin-bottom: 5px;
  transition: border-bottom 0.4s;
  border-bottom: 2px solid transparent;

  & svg {
    position: absolute;
    width: 8px;
    height: 8px;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    path {
      fill: #ffffff;
    }
  }

  ${({isVideo}) =>
    isVideo &&
    css`
      &::before {
        content: '';
        width: 19px;
        height: 19px;
        position: absolute;
        background: #000000;
        opacity: 0.4;
        border: 0.5px solid #000000;
        box-sizing: border-box;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.19);
        border-radius: 50%;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
    `}

  ${({isActive}) =>
    isActive &&
    css`
      border-bottom: 2px solid ${primaryColor};
    `}
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  flex-shrink: 0;
`;

export const ShopInfoContainer = styled.div`
  padding: 13px 0 17px;
  overflow: hidden;
`;

export const Title = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.019em;
  color: #000000;
  padding: 11px 0;

  ${({sticky}) =>
    sticky &&
    css`
      position: sticky;
      background-color: #fff;
      z-index: 10;
      top: 0;
    `}
`;

export const PostInfoWrapper = styled.div`
  max-width: 100%;
  padding-left: 80px;
  margin-bottom: 24px;
  flex-grow: 1;

  ${({withoutMargin}) =>
    withoutMargin &&
    css`
      margin: 0;
    `}

  ${({fullScreen}) =>
    fullScreen &&
    css`
      display: none;
    `}
`;

export const Description = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  letter-spacing: 0.3px;
  color: #464646;
  padding-right: 20px;
`;

export const SeeComments = styled(Title)`
  margin: 30px 0;
  width: 100%;
  transition: 0.5s margin;
  display: flex;
  align-items: center;

  & span {
    flex-grow: 1;
    font-weight: normal;
    margin: 0 4px;
  }

  ${({openComments}) =>
    openComments &&
    css`
      margin: 15px 0 30px;
    `}
`;

export const ShowMoreButton = styled.div`
  display: flex;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #8f8f8f;
  cursor: pointer;

  & svg {
    transition: 0.5s transform;
    margin-left: 5px;
    ${({expanded}) =>
      expanded &&
      css`
        transform: rotate(180deg);
      `}
  }
`;

export const StyledModal = styled(Modal)`
  &&& {
    top: 0;
    padding-top: 24px;
  }

  .ant-modal-content {
    border-radius: 8px;
    overflow: hidden;
  }
`;
