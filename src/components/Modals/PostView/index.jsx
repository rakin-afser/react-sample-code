import React from 'react';
import SinglePost from 'containers/SinglePost';
import {StyledModal} from './styled';
import {useLocation} from 'react-router';

const PostView = (props) => {
  const {visible, setVisible, postId} = props || {};
  const {pathname} = useLocation();

  const onClose = () => {
    setVisible(false);
  };

  return (
    <StyledModal
      afterClose={() => {
        window.history.pushState(null, null, pathname);
      }}
      centered
      destroyOnClose
      closable={false}
      width={1010}
      bodyStyle={{
        padding: '0'
      }}
      visible={visible}
      onOk={onClose}
      onCancel={onClose}
      footer={null}
      closeIcon={null}
    >
      <SinglePost postId={postId} />
    </StyledModal>
  );
};

export default PostView;
