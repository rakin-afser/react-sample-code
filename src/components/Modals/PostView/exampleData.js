import image1 from 'images/posts/post1.jpg';
import image2 from 'images/posts/post2.jpg';
import image3 from 'images/posts/post3.jpg';
import image4 from 'images/posts/post4.jpg';
import video1 from 'videos/videoplayback.mp4';
import video2 from 'videos/videoplayback_2.mp4';
import image5 from 'images/products/product14.jpg';
import image6 from 'images/products/product18.jpg';
import image7 from 'images/products/product19.jpg';
import image8 from 'images/products/product20.jpg';

const exampleData = {
  media: [
    {id: 'video-group1', type: 'video', preview: image5, children: [video1, video2]},
    {id: 'images-group1', type: 'image', children: [image1, image2, image3, image4]},
    {id: 'images-group2', type: 'image', children: [image6, image1, image4]},
    {id: 'images-group3', type: 'image', children: [image7, image2]},
    {id: 'images-group4', type: 'image', children: [image8, image1, image3]}
  ],
  title: '2018 Floral Dresses Vestido De Festa Vestido De Festa',
  description: `For Rihanna, creating the perfect soft matte base 
  is the most important part of any look Trendy overview for last 
  season, tenden collaboration, FW The magazine covers international,
   national and local fashion and beauty trends and news. Lorem ipsum 
   dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
   incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
   veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
   ex ea commodo consequat.For Rihanna, creating the perfect soft matte base 
   is the most important part of any look Trendy overview for last 
   season, tenden collaboration, FW The magazine covers international,
    national and local fashion and beauty trends and news. Lorem ipsum 
    dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
    incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
    veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
    ex ea commodo consequat.For Rihanna, creating the perfect soft matte base 
    is the most important part of any look Trendy overview for last 
    season, tenden collaboration, FW The magazine covers international,
     national and local fashion and beauty trends and news. Lorem ipsum 
     dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
     incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
     veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
     ex ea commodo consequat.For Rihanna, creating the perfect soft matte base 
     is the most important part of any look Trendy overview for last 
     season, tenden collaboration, FW The magazine covers international,
      national and local fashion and beauty trends and news. Lorem ipsum 
      dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
      incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
      veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip 
      ex ea commodo consequat.`
};

export default exampleData;
