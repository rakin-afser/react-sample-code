import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Modal} from 'antd';
import {ButtonsBlock, CloseContainer, ModalBody, NextButton} from 'components/CreatePost/styled';
import Icon from 'components/Icon';
import ImageUploadingHolder from 'components/ImageUploadingHolder';
import ProductInfoShort from 'components/ProductInfoShort';
import exampleData from 'components/Modals/exampleData';
import InfoPanel from 'components/Modals/RequestAReturn/components/InfoPanel';
import Form from 'components/Modals/RequestAReturn/components/Form';
import {
  Title,
  TitleWrapper,
  Hr,
  TxtSummary,
  ImagesWrapper,
  CancelButton,
  ProductInfoWrapper,
  InfoPanelWrapper
} from './styled';

const RequestAReturn = ({reqReturnModal, setReqReturnModal}) => {
  const [images, setImages] = useState([]);

  const addImage = (value) => {
    setImages((oldArray) => [...oldArray, value]);
  };

  const removeImage = (id) => {
    setImages((oldArray) => oldArray.filter((el) => el.id !== id));
  };

  const onCancel = () => {
    setReqReturnModal(false);
  };

  return (
    <div>
      <Modal
        wrapClassName="leave-feedback-modal"
        visible={reqReturnModal}
        onCancel={onCancel}
        width={425}
        footer={null}
        destroyOnClose
        bodyStyle={{
          padding: '13px 0 16px',
          minHeight: '650px'
        }}
        closeIcon={
          <CloseContainer
            style={{
              top: '12px',
              position: 'relative',
              right: '-21px'
            }}
          >
            <Icon color="#000" type="close" />
          </CloseContainer>
        }
      >
        <TitleWrapper>
          <Title>Request a Return</Title>
        </TitleWrapper>

        <ModalBody withPadding>
          <Form />

          <ImagesWrapper>
            <ImageUploadingHolder addImage={addImage} deleteImage={removeImage} images={images} />
          </ImagesWrapper>
          <TxtSummary>Refund summary:</TxtSummary>

          <InfoPanelWrapper>
            <InfoPanel soldBy="Best Seller" itemsCount={1} orderId="09876YO" />
          </InfoPanelWrapper>

          <ProductInfoWrapper>
            <ProductInfoShort data={exampleData} />
          </ProductInfoWrapper>

          <ButtonsBlock style={{borderTop: 'none', paddingTop: 0}}>
            <Hr />
            <CancelButton onClick={onCancel}>Cancel</CancelButton>
            <NextButton onClick={() => {}}>
              <span>Submit</span>
            </NextButton>
          </ButtonsBlock>
        </ModalBody>
      </Modal>
    </div>
  );
};

RequestAReturn.propTypes = {
  reqReturnModal: PropTypes.bool.isRequired,
  setReqReturnModal: PropTypes.func.isRequired
};

export default RequestAReturn;
