import React from 'react';
import Select from 'components/SelectTrue';
import {Input} from 'antd';
import {
  Txt1,
  SelectWrapper,
  Txt2,
  Txt3,
  TextAreaWrapper
} from 'components/Modals/RequestAReturn/components/Form/styled';

const options = [
  {
    id: 1,
    title: 'Defective Product'
  },
  {
    id: 2,
    title: 'Some Title 2'
  },
  {
    id: 3,
    title: 'Some Title 3'
  }
];

const Form = () => {
  return (
    <>
      <Txt1>Why do you want to return this item? *</Txt1>
      <SelectWrapper>
        <Select value="Defective Product" onChange={() => {}} options={options} valueKey="id" labelKey="title" />
      </SelectWrapper>
      <Txt2>Add Details *</Txt2>
      <TextAreaWrapper>
        <Input.TextArea maxLength={400} autoSize={{minRows: 4, maxRows: 4}} />
      </TextAreaWrapper>
      <Txt3>You can add up to 5 photos or videos.</Txt3>
    </>
  );
};

Form.propTypes = {};

export default Form;
