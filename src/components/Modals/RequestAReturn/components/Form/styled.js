import styled from 'styled-components';
import {mainBlackColor, secondaryTextColor} from 'constants/colors';

export const Txt1 = styled.div`
  margin-bottom: 6px;
  font-size: 14px;
  color: ${secondaryTextColor};
`;

export const SelectWrapper = styled.div`
  margin-bottom: 15px;
`;

export const Txt2 = styled.div`
  margin-bottom: 8px;
  font-size: 14px;
  color: ${mainBlackColor};
`;

export const Txt3 = styled.div`
  margin-bottom: 8px;
  font-family: 'SF Pro Display', sans-serif;
  font-size: 12px;
  color: #000105;
`;

export const TextAreaWrapper = styled.div`
  margin-bottom: 15px;
`;
