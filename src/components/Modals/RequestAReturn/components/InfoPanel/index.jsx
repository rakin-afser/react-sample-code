import React from 'react';
import PropTypes from 'prop-types';
import {Wrapper, Item} from './styled';

const InfoPanel = ({soldBy, orderId, itemsCount}) => {
  return (
    <Wrapper>
      <Item>
        Sold by: <span>{soldBy}</span>
      </Item>
      <Item>
        Order id: <span>{orderId}</span>
      </Item>
      <Item>
        <span>{itemsCount} item</span>
      </Item>
    </Wrapper>
  );
};

InfoPanel.propTypes = {
  soldBy: PropTypes.string.isRequired,
  orderId: PropTypes.string.isRequired,
  itemsCount: PropTypes.number.isRequired
};

export default InfoPanel;
