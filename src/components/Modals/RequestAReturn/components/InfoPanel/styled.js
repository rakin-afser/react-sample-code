import styled from 'styled-components';
import {blue} from 'constants/colors';

export const Wrapper = styled.div`
  padding: 19px 24px;
  display: flex;
  align-items: center;
  background-color: #f6f6f6;
`;

export const Item = styled.div`
  font-size: 14px;
  color: #545454;

  &:first-child {
    margin-right: 26px;

    span {
      margin-left: 4px;
      font-weight: bold;
      color: ${blue};
    }
  }

  &:nth-child(2) {
    margin-right: auto;

    span {
      margin-left: 4px;
      font-weight: 500;
      color: #000;
    }
  }

  &:nth-child(3) {
    span {
      font-weight: bold;
      color: #000105;
    }
  }
`;
