import styled from 'styled-components';
import {headerShadowColor} from 'constants/colors';

export const TitleWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Title = styled.h3`
  padding: 6px 0 25px 25px;
  margin-bottom: 0;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const TxtSummary = styled.div`
  margin-bottom: 6px;
  font-size: 14px;
  font-weight: 500;
  color: #000;
`;

export const ImagesWrapper = styled.div`
  margin-bottom: 14px;
`;

export const CancelButton = styled.button`
  font-weight: normal;
  font-size: 14px;
  height: 23px;
  color: #000000;
  border: none;
  background: none;
  margin-right: ${({isBack}) => (isBack ? '13px' : '32px')};
  cursor: pointer;
  transition: ease 0.6s;

  &:hover {
    color: #ed484f;
  }
`;

export const Hr = styled.hr`
  margin-bottom: 14px;
  margin-left: -24px;
  margin-right: -24px;
  height: 1px;
  background-color: ${headerShadowColor};
  border: none;
`;

export const InfoPanelWrapper = styled.div`
  border-bottom: 1px solid #e4e4e4;
  margin-left: -24px;
  margin-right: -24px;
  background-color: #f6f6f6;
`;

export const ProductInfoWrapper = styled.div`
  padding: 17px 14px;
`;
