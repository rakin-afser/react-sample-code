import styled from 'styled-components/macro';
import {mainBlackColor as black, grayTextColor as gray700} from 'constants/colors';

export const Title = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
  text-transform: capitalize;
`;

export const Text = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #000;

  ${({gray}) =>
    gray &&
    `
    color: #464646;
  `}
`;

export const Note = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 160%;
  color: #656565;
  margin: 0;
  padding: 0;
`;

export const SubmittedBlock = styled.div`
  max-width: 330px;
  margin: 80px auto 64px auto;
  text-align: center;
`;

export const SubmittedTitle = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 22px;
  color: ${black};
  margin-bottom: 12px;
`;

export const SubmittedText = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  letter-spacing: -0.016em;
  color: ${gray700};
  margin-bottom: 0;
`;

export const ModalBlock = styled.div`
  padding: ${({checkbox}) => (checkbox ? `30px 24px` : `16px 24px 0 24px`)};
  position: relative;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    &::after {
      right: unset;
      left: 40px;
    }
  `}
`;

export const CheckboxLabel = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-size: 14px;
  line-height: 140%;
  color: #a7a7a7;

  & span {
    color: #000;
    font-weight: 500;
  }
`;

export const ModalFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 16px 24px;
  border-top: 1px solid #e4e4e4;
  margin-top: 16px;
`;

export const Cancel = styled.div`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  transition: color 0.3s;

  &:hover {
    cursor: pointer;
    color: #464646;
  }
`;

export const Submit = styled.div`
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 24px;
  padding: 6px 34px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 1;
  margin-left: 24px;
  color: #000;
  min-height: 28px;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    transition: 0.4s;
    cursor: pointer;
    color: #fff;
    ${({loading}) => (loading ? '' : 'border: 1px solid #ed484f;')}
    ${({loading}) => (loading ? '' : 'background-color: #ed484f;')}
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-right: 24px;
    margin-left: 0;
  `}
`;
