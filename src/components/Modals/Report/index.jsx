import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useTranslation} from 'react-i18next';

import {
  Title,
  Text,
  Note,
  SubmittedBlock,
  SubmittedTitle,
  SubmittedText,
  ModalBlock,
  ModalFooter,
  Cancel,
  Submit
} from './styled';

import Icon from 'components/Icon';
import Textarea from 'components/Textarea';
import ModalContainer from 'components/Modals/ModalContainer';
import Checkbox from 'components/Checkbox';

const reportCategories = [
  {id: 'violence', text: 'Violence'},
  {id: 'nudity', text: 'Nudity'},
  {id: 'spam', text: 'Spam'},
  {id: 'other', text: 'Other'}
];

const ReportModal = ({isOpen, onSubmit, onClose, type = 'post'}) => {
  const {t} = useTranslation();
  const [selectedCategory, setSelectedCategory] = useState('violence');
  const [reportText, setReportText] = useState('');
  const [isSubmitted, setIsSubmitted] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleReportTextChange = (event) => {
    const text = event.target.value;
    setReportText(text);
  };

  const submitReport = async () => {
    if (isSubmitted) {
      closeModal();
    } else {
      setLoading(true);

      const result = await onSubmit({category: selectedCategory, text: selectedCategory === 'other' ? reportText : ''});

      if (result) {
        setIsSubmitted(true);
      }

      setLoading(false);
    }
  };

  const closeModal = () => {
    setTimeout(() => setIsSubmitted(false), 1000);
    setSelectedCategory('violence');
    setReportText('');
    onClose();
  };

  return (
    <ModalContainer
      title={<Title>{`${t('reportModal.report')} ${t(`reportModal.${type}`)}`}</Title>}
      onClose={closeModal}
      isOpen={isOpen}
      hideTitle={isSubmitted}
    >
      {isSubmitted ? (
        <SubmittedBlock>
          <SubmittedTitle>{t('reportModal.thankYou')}</SubmittedTitle>
          <SubmittedText>{t('reportModal.reportSent')}</SubmittedText>
        </SubmittedBlock>
      ) : (
        <>
          <ModalBlock>
            <Text>{`${t('reportModal.chooseReason')} ${t(`reportModal.${type}`)}`}</Text>
          </ModalBlock>
          {reportCategories.map((item) => (
            <ModalBlock key={item.id}>
              <Checkbox
                key={`report-${item.id}`}
                onChange={() => setSelectedCategory(item.id)}
                checked={item.id === selectedCategory}
              >
                <Text gray={item.id !== selectedCategory}>{item.text}</Text>
              </Checkbox>
            </ModalBlock>
          ))}
          {selectedCategory === 'other' && (
            <ModalBlock>
              <Textarea
                placeholder={t('reportModal.addText')}
                height={88}
                value={reportText}
                onChange={handleReportTextChange}
              />
            </ModalBlock>
          )}
          <ModalBlock>
            <Note>{t('reportModal.note1')}</Note>
            <Note>{t('reportModal.note2')}</Note>
          </ModalBlock>
        </>
      )}
      <ModalFooter>
        {!isSubmitted && <Cancel onClick={closeModal}>{t('general.cancel')}</Cancel>}
        <Submit onClick={submitReport} loading={loading}>
          {
            loading
              ? <Icon type="loader" svgStyle={{width: 24, height: 5, fill: '#ED484F'}} />
              : isSubmitted ? t('general.ok') : t('reportModal.report')
          }
        </Submit>
      </ModalFooter>
    </ModalContainer>
  );
};

ReportModal.propTypes = {
  isOpen: PropTypes.bool,
  onSubmit: PropTypes.func,
  onClose: PropTypes.func,
  type: PropTypes.string
};

export default ReportModal;
