import {Progress} from 'antd';
import styled from 'styled-components';

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`;

export const VideoProgress = styled(Progress)`
  position: absolute !important;
  bottom: 0;
  left: 0;
  right: 0;
  font-size: 0 !important;

  .ant-progress-inner {
    background-color: rgba(44, 42, 42, 0.2);
    border-radius: 0;
  }

  .ant-progress-bg {
    border-radius: 0 !important;
  }
`;

VideoProgress.defaultProps = {
  strokeColor: '#ED484F',
  strokeWidth: 4,
  showInfo: false,
  strokeLinecap: 'square'
};

export const Play = styled.button`
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 10;
  transform: translate(-50%, -50%);
  width: 64px;
  height: 64px;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  background-color: #fff;
  border-radius: 50%;
  overflow: hidden;

  /* svg {
    margin-left: 2px;
    margin-top: 4px;
  } */
`;

export const MuteButton = styled.button`
  position: absolute;
  bottom: 36px;
  right: 13px;
  background-color: rgba(27, 27, 27, 0.51);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.04);
  display: inline-flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  padding: 0;
  line-height: 1;
`;

export const Remaining = styled.p`
  position: absolute;
  bottom: 12px;
  right: 12px;
  height: auto;
  font-size: 12px;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  margin-bottom: 0;
  color: #fff;
`;

export const Poster = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
`;

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1;
`;
