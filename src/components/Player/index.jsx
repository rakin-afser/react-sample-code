import React, {forwardRef, useState} from 'react';
import ReactPlayer from 'react-player';
import {VideoProgress, Wrapper, Play, MuteButton, Remaining, Poster, Overlay} from './styled';
import Icon from 'components/Icon';
import moment from 'moment';

const Player = forwardRef((props, ref) => {
  const {src, hidePlay, autoPlay, poster, playOnScroll} = props;
  const [progress, setProgress] = useState(0);
  const [muted, setMuted] = useState(true);
  const [playing, setPlaying] = useState(false);
  const [isPlayed, setIsPlayed] = useState(false);
  const [playedTime, setPlayedTime] = useState(0);
  const [duration, setDuration] = useState(0);

  function onProgress(e) {
    const {played, playedSeconds} = e || {};
    setPlayedTime(playedSeconds);
    setProgress(played * 100);
  }

  function onEnded() {
    setPlaying(false);
    setIsPlayed(false);
  }

  const onPlayHandler = () => {
    if (playOnScroll) return;
    setIsPlayed(true);
    setPlaying(true);
  };

  const onPauseHandler = () => {
    setIsPlayed(false);
    setPlaying(false);
  };

  return (
    <Wrapper>
      <Overlay onClick={() => setMuted(!muted)} />
      {isPlayed || playOnScroll ? null : <Poster style={{backgroundImage: `url(${poster})`}} />}
      <ReactPlayer
        loop
        playsinline
        onDuration={(d) => setDuration(d)}
        onEnded={onEnded}
        progressInterval={250}
        onPlay={onPlayHandler}
        onPause={onPauseHandler}
        playing={autoPlay || playing}
        muted={muted}
        ref={ref}
        onProgress={onProgress}
        height="100%"
        width="100%"
        controls={false}
        url={src}
      />
      {hidePlay || playing || isPlayed ? null : (
        <Play onClick={() => setPlaying(true)}>
          <Icon type="play" width={23} height={23} />
        </Play>
      )}
      <MuteButton>
        <Icon type={muted ? 'muted' : 'unmuted'} />
      </MuteButton>
      <Remaining>{moment.utc(Math.max(0, duration - playedTime) * 1000).format('mm:ss')}</Remaining>
      <VideoProgress percent={progress} />
    </Wrapper>
  );
});

export default Player;
