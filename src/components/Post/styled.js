import styled, {css} from 'styled-components/macro';
import media from 'constants/media';
import {blue, primaryColor} from 'constants/colors';
import {mainFont} from 'constants/fonts';

export const PostWrap = styled.div`
  order: ${({order}) => order || null};
  width: 100%;
  background: #ffffff;
  border: 1px solid #ececec;
  box-sizing: border-box;
  border-radius: 4px;
  margin-bottom: 24px;

  ${({forFeed}) =>
    forFeed &&
    `
    border: none;
  `};

  .slick-slider .slick-prev {
    left: 10px;
    z-index: 999;
  }

  .slick-slider .slick-next {
    right: 27px;
  }

  .slick-slider .slick-prev:before,
  .slick-slider .slick-next:before {
    background-position: center;
    background-color: rgba(0, 0, 0, 0.3);
    width: 37px !important;
    height: 37px !important;
    background-size: 10.79px 18.5px;
    mix-blend-mode: lighten;
    background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iMTkiIHZpZXdCb3g9IjAgMCAxMiAxOSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xMC43Mjk5IDE4LjM2M0MxMS4yNTY3IDE3Ljg0NjkgMTEuMjU2NyAxNy4wMTAyIDEwLjcyOTkgMTYuNDk0MkwzLjU5MDAxIDkuNUwxMC43Mjk5IDIuNTA1ODJDMTEuMjU2NyAxLjk4OTc3IDExLjI1NjcgMS4xNTMwOSAxMC43Mjk5IDAuNjM3MDM3QzEwLjIwMzEgMC4xMjA5ODkgOS4zNDg5OSAwLjEyMDk4OSA4LjgyMjE5IDAuNjM3MDM3TDAuNzI4NDMzIDguNTY1NjFDMC4yMDE2MzIgOS4wODE2NiAwLjIwMTYzMiA5LjkxODM0IDAuNzI4NDMzIDEwLjQzNDRMOC44MjIxOCAxOC4zNjNDOS4zNDg5OCAxOC44NzkgMTAuMjAzMSAxOC44NzkgMTAuNzI5OSAxOC4zNjNaIiBmaWxsPSIjRkFGQUZBIi8+Cjwvc3ZnPgo=);
    background-repeat: no-repeat;
  }

  .slick-slider .slick-dots {
    bottom: -24px !important;

    & .slick-active button:before {
      color: #ea2c34;
    }

    & button:before {
      font-size: 7px;
      color: #e4e4e4;
      opacity: 1 !important;
    }
  }

  .slick-dots li {
    margin: 0 -2px;
  }
`;

export const PostWrapMobile = styled.div`
  order: ${({order}) => order || null};
  width: 100%;
  background: #ffffff;
  box-sizing: border-box;
  border-radius: 4px;

  & + & {
    border-top: 4px solid #f7f8fa;
  }

  &&& {
    .vjs-play-control.vjs-ended,
    .vjs-big-play-button {
      display: none;
    }
  }

  &:only-of-type {
    border-bottom: none;
    margin-bottom: 0;
  }

  .slick-slider .slick-prev {
    left: 10px;
    z-index: 999;
  }

  .slick-slider .slick-next {
    right: 27px;
  }

  .slick-slider .slick-prev:before,
  .slick-slider .slick-next:before {
    background-position: center;
    background-color: rgba(0, 0, 0, 0.3);
    width: 37px !important;
    height: 37px !important;
    background-size: 10.79px 18.5px;
    mix-blend-mode: lighten;
    background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iMTkiIHZpZXdCb3g9IjAgMCAxMiAxOSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xMC43Mjk5IDE4LjM2M0MxMS4yNTY3IDE3Ljg0NjkgMTEuMjU2NyAxNy4wMTAyIDEwLjcyOTkgMTYuNDk0MkwzLjU5MDAxIDkuNUwxMC43Mjk5IDIuNTA1ODJDMTEuMjU2NyAxLjk4OTc3IDExLjI1NjcgMS4xNTMwOSAxMC43Mjk5IDAuNjM3MDM3QzEwLjIwMzEgMC4xMjA5ODkgOS4zNDg5OSAwLjEyMDk4OSA4LjgyMjE5IDAuNjM3MDM3TDAuNzI4NDMzIDguNTY1NjFDMC4yMDE2MzIgOS4wODE2NiAwLjIwMTYzMiA5LjkxODM0IDAuNzI4NDMzIDEwLjQzNDRMOC44MjIxOCAxOC4zNjNDOS4zNDg5OCAxOC44NzkgMTAuMjAzMSAxOC44NzkgMTAuNzI5OSAxOC4zNjNaIiBmaWxsPSIjRkFGQUZBIi8+Cjwvc3ZnPgo=);
    background-repeat: no-repeat;
  }

  .slick-slider .slick-dots {
    /* bottom: -24px !important; */
    & .slick-active button:before {
      color: #ea2c34;
    }

    & button:before {
      font-size: 10px;
      color: #e4e4e4;
      opacity: 1 !important;
    }
  }

  .slick-dots li {
    margin: 0 3px;
    padding: 0;
  }
`;

export const PostContainer = styled.div`
  padding: 16px 46px 4px 50px;
  position: relative;
  z-index: 1;
  // overflow: hidden;
  background-color: #fff;
  border-bottom: 0.5px solid rgba(21, 21, 21, 0.1);

  ${({forFeed}) =>
    forFeed &&
    `
    padding: 0;
  `};

  @media (max-width: ${media.mobileMax}) {
    border-bottom: 0;
    padding: 16px 46px 18px 50px;
  }
`;

export const PostContainerMobile = styled.div`
  padding: 16px 16px 0;
`;

export const PostHeader = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const StoreImage = styled.img`
  border: 1px solid #f0f0f0;
  object-fit: cover;
  width: 48px;
  height: 48px;
  border-radius: 50px;
  transition: ease 0.4s;
  cursor: pointer;
  box-shadow: 0 0 0 rgba(000, 000, 000, 0);

  &:hover {
    box-shadow: 0 0 4px rgba(000, 000, 000, 0.5);
  }
`;

export const StoreImageMobile = styled.img`
  border: 1px solid #f0f0f0;
  width: 50px;
  height: 50px;
  border-radius: 50px;
  flex-shrink: 0;
`;

export const Block = styled.div`
  margin-left: 9px;
  display: flex;
  width: 100%;
  flex-wrap: wrap;
`;

export const StoreData = styled.div`
  display: flex;
  flex: 1;
`;

export const MoreButtonArea = styled.div`
  position: relative;
  right: 0;
  padding: 0 7px;
  height: 16px;
  cursor: pointer;
  background: #f4f4f4;
  border-radius: 20px;
  transition: ease 0.4s;

  &:hover {
    transform: scale(1.12);
    background-color: #eaeaea;
  }

  img {
    display: block;
    margin: 5px 0 0;
  }
`;

export const Actions = styled.div`
  position: relative;
  display: flex;
  justify-content: flex-end;
  align-items: center;

  button {
    margin: 0 16px 0 0;
  }
`;

export const Influencer = styled.span`
  font-size: 12px;
  font-family: 'Helvetica Neue', sans-serif;
  color: #1b5dab;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  background-color: #e5f1ff;
  border-radius: 24px;
  padding: 4px 8px;
  margin-top: 2px;
  line-height: 1;
  margin-bottom: 5px;
`;

export const InflCode = styled.span`
  font-weight: 500;
  color: #4a90e2;
  display: block;
  margin-bottom: 2px;

  span {
    font-weight: 400;
    color: #666666;
  }
`;

export const DateTime = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  display: flex;
  align-items: center;
  color: #8f8f8f;
`;

export const CountryName = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  display: flex;
  align-items: center;
  color: #8f8f8f;
  position: relative;
  display: inline-flex;
  align-items: center;

  &:before {
    content: '|';
    display: inline-flex;
    margin: auto 10px;

    @media (max-width: 767px) {
      display: none;
      width: 100%;
    }
  }

  @media (max-width: 767px) {
    margin-left: 8px;
  }
`;

export const StoreName = styled.h5`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 140%;
  color: #3a3a3a;
  margin-bottom: 1px;
  transition: ease 0.4s;
  cursor: pointer;
  width: 100%;

  &:hover {
    color: ${blue};
  }
`;

export const MoreButton = styled.img``;

export const SliderWrap = styled.div`
  position: relative;
`;

export const SingleImage = styled.img`
  width: 100%;
  object-fit: contain;
`;

export const SliderWrapMobile = styled.div`
  position: relative;
  margin: 0 -16px;
`;

export const ClickableOverlay = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 14px;
  left: 0;
  z-index: 1;
`;

export const ProductCounter = styled.div`
  position: absolute;
  width: 55px;
  height: 22px;
  left: 16px;
  top: 16px;
  background: #ffffff;
  mix-blend-mode: normal;
  opacity: 0.9;
  box-shadow: 1.42857px 1.42857px 2.85714px rgba(0, 0, 0, 0.16);
  border-radius: 12.1429px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 11.4286px;
  line-height: 140%;
  display: flex;
  align-items: center;
  letter-spacing: -0.016em;
  color: #7a7a7a;
  justify-content: center;
  padding-top: 3px;
  z-index: 9;
  transition: ease 0.4s;
  cursor: pointer;
  border: 1px solid transparent;
`;

export const ProductCounterMobile = styled.div`
  position: absolute;
  width: 56px;
  height: 24px;
  top: 16px;
  right: 16px;
  background: #ffffff;
  mix-blend-mode: normal;
  opacity: 0.9;
  box-shadow: 1.42857px 1.42857px 2.85714px rgba(0, 0, 0, 0.16);
  border-radius: 12.1429px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 11.4286px;
  line-height: 140%;
  display: flex;
  align-items: center;
  letter-spacing: -0.016em;
  color: #7a7a7a;
  justify-content: center;
  z-index: 9;
`;

export const ProductCounterMobileVideo = styled(ProductCounterMobile)`
  top: 40px;
  right: 5px;
`;

export const BagsImage = styled.img`
  margin-right: 8px;
  width: 15.68px;
  height: 14px;
  margin-bottom: 3px;
`;

export const BagsImageMobile = styled.img`
  margin-right: 10px;
  width: 20px;
  height: 22px;
`;

export const VideoWrapper = styled.div`
  height: 100%;
  width: 100%;

  > div {
    height: 100%;
    width: 100%;
  }
  video {
    object-fit: cover;
  }
`;

export const Poster = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Slide = styled.div`
  position: relative;
`;

export const ActionsWrap = styled.div`
  display: flex;
  width: 100%;
  padding-top: 15px;
  padding-bottom: 15px;
  position: relative;

  @media (max-width: ${media.mobileMax}) {
    padding-top: 15px;
    padding-bottom: 15px;

    &::before {
      content: '';
      position: absolute;
      left: -16px;
      right: -16px;
    }

    &::before {
      border-top: 0.5px solid #d8d8d8;
      top: 0px;
    }
  }
`;

export const Item = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  margin-left: ${({marginAuto}) => (marginAuto ? 'auto' : 'unset')};

  height: 26px;
  background: #f5f5f5;
  border: 0.5px solid #cccccc;
  box-sizing: border-box;
  border-radius: 22px;
  padding: 0 11px 0 10px;
  cursor: pointer;

  i,
  svg {
    max-width: 15px;
    max-height: 15px;
  }

  &:not(:last-child) {
    margin-right: 10px;
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }

  span {
    font-size: 10px;
    line-height: 1;
    color: #999999;
    margin-right: 0;
    line-height: 13px;
  }
`;

export const RatioHolder = styled.div`
  padding-bottom: ${({ratio}) => (ratio ? ratio * 100 : 100)}%;
`;

export const MediaHolder = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

export const Right = styled.div`
  margin-left: auto;
`;

export const Likes = styled.span`
  margin-top: 0;
  display: inline-block;
  font-family: ${mainFont};
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #8f8f8f;
  align-self: center;
  margin-left: 6px;

  @media (max-width: 767px) {
    font-size: 10px;
    position: relative;
    top: -1px;
    line-height: 1;
    color: #999999;
    margin-right: 0;
  }
`;

export const CommentsCount = styled(Likes)`
  font-family: Helvetica, sans-serif;
  margin-left: 0;
  color: ${({showComments}) => (showComments ? primaryColor : '#8f8f8f')};
  margin-right: 0;
  display: flex;
  align-items: center;
  cursor: pointer;
  transition: ease 0.4s;

  span {
    margin-left: 6px;
  }

  svg {
    path {
      transition: ease 0.4s;
      fill: ${({showComments}) => (showComments ? primaryColor : '#8f8f8f')};
    }
  }

  &:hover {
    color: ${primaryColor};

    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }

  @media (max-width: 767px) {
    transition: ease 0.3s;
    font-size: 10px;
    position: relative;
    top: -1px;
    line-height: 1;
    color: #999999;
    margin-left: 6px;
  }
`;

export const BookmarkWrap = styled.div`
  display: flex;
  align-items: center;
  margin-right: 0;
  max-height: 20px;
  overflow: hidden;

  svg.iconBookmark {
    display: ${({isWished}) => (isWished ? 'none' : 'block')};
  }

  svg.iconFilledBookmark {
    display: ${({isWished}) => (isWished ? 'block' : 'none')};
    margin-right: -4px;
  }

  & > svg {
    cursor: pointer;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    & > svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const Ads = styled.a`
  color: #4a90e2;
  font-size: 12px;
  display: inline-block;
  margin-top: 6px;
  margin-bottom: 5px;
`;

export const ProductDescription = styled.div`
  width: 100%;
  margin-top: 16px;
  margin-bottom: 10px;
`;

export const ProductDescriptionMobile = styled.div`
  width: 100%;
  margin-top: 2px;
  margin-bottom: 10px;
`;

export const DescriptionTitle = styled.h4`
  margin-bottom: 4px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 140%;
  color: #3a3a3a;
`;

export const DescriptionText = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  mix-blend-mode: normal;
  opacity: 0.8;

  p {
    margin-bottom: 0;
  }
`;

export const PostWithLink = styled.div``;

export const ExternalLink = styled.a`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #1b5dab;
  mix-blend-mode: normal;
  opacity: 0.8;
`;

export const LinkHeader = styled.p`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  color: #000000;
`;

export const LinkPreviewWrap = styled.div`
  width: 100%;
  max-width: 478px;
  margin-top: 11px;
`;

export const LinkPreview = styled.img`
  width: 100%;
`;

export const Benefit = styled.span`
  background: #f2f2f2;
  border-radius: 24px;
  padding: 6px 23px;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #656565;
  cursor: pointer;
  transition: background 0.3s;

  &:hover {
    background: #e2e2e2;
  }

  &:first-child {
    margin-right: 7px;
  }

  &:nth-child(2) {
    margin-right: 31px;
  }
`;

export const RatingBlock = styled.div`
  border: 1px solid #313e4f;
  border-radius: 70px;
  padding: 3px 14px;
  margin-left: 57px;
  display: flex;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #313e4f;
  width: fit-content;

  span {
    margin-right: 9px;
  }

  svg {
    margin: 0 1px;
  }
`;

export const ShareWrapper = styled.div`
  position: absolute;
  right: 0;
  //top: -238px;
  bottom: 35px;
  z-index: 200;
  width: 384px;
`;

export const LikeWrapper = styled.div`
  display: flex;
  align-items: center;

  ${({isLiked}) => (isLiked ? `span { color: #ED494F; }` : '')}
`;

export const OptionsWrapper = styled.div`
  z-index: 10;

  &.enter {
    transition: ease 0.4s;
    opacity: 0;
  }

  &.enter-active {
    opacity: 1;
  }

  &.exit {
    transition: ease 0.4s;
    opacity: 1;
  }

  &.exit-active {
    opacity: 0;
  }
`;

export const IconShareWrapper = styled.div`
  position: relative;
  top: 1px;
  cursor: pointer;

  svg {
    path {
      transition: ease 0.3s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const CommentsWrapper = styled.div`
  position: relative;
  z-index: 0;
  display: flex;
  overflow: hidden;

  &.enter {
    opacity: 0;
  }

  &.enter-active {
    opacity: 1;
    margin-top: 0;
    transition: opacity ease 300ms, margin-top 300ms;
  }

  &.exit-active {
    opacity: 1;
  }

  &.exit {
    opacity: 0;
    transition: opacity ease 300ms, margin-top 300ms;
  }
`;
