import storeAvatar1 from './img/avatar1.png';
import poster1 from './img/poster1.png';
import poster2 from './img/poster2.png';
import poster3 from './img/poster3.png';
import product1 from './img/product1.png';
import product2 from './img/product2.png';
import product3 from './img/product3.png';
import product4 from './img/product4.png';
import product5 from './img/product5.png';
import linkPreview from './img/preview.png';

export default [
  {
    id: 1,
    store: {
      name: 'Kylie Skin',
      avatar: storeAvatar1
    },
    date: '2d ago',
    followed: false,
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster1, poster1, poster1, poster1, poster1, poster1],
    productsCount: 3,
    products: [
      {
        name: 'Red Lipstick',
        brand: 'Chanel',
        image: product1,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Lux Foundation',
        brand: 'Chanel',
        image: product2,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Red Sweater',
        brand: 'Chanel',
        image: product3,
        price: 526,
        delivery: 'Free Shipping',
        sale: -40
      }
    ],
    likes: 1342,
    comments: 546
  },
  {
    id: 2,
    store: {
      name: 'Kylie Skin',
      avatar: storeAvatar1
    },
    date: '5w ago',
    description: {
      title: 'Cream canvas Big Sad Wolf Tote Bag',
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other.'
    },
    followed: false,
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster2, poster2, poster2, poster2, poster2, poster2],
    productsCount: 3,
    products: [
      {
        name: 'Beats Headphones Solo 3',
        brand: 'Beats',
        image: null,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 3102,
    comments: 5
  },
  {
    id: 3,
    store: {
      name: 'Lykie Skin',
      avatar: storeAvatar1
    },
    link: {
      url: 'https://www.youtube.com/watch?v=VAMpNDrsgAg',
      header: 'Hey! Check this lovely tunes out:',
      preview: {
        image: linkPreview
      }
    },
    date: '14 hours ago',
    followed: false,
    likes: 342,
    comments: 546
  },
  {
    id: 4,
    store: {
      name: 'Kylie Skin',
      avatar: storeAvatar1
    },
    date: '5w ago',
    followed: true,
    tags: ['#KENZO', '#Kenzocollection', '#shoes', '#leather'],
    posters: [poster1, poster1, poster1, poster1, poster1, poster1],
    productsCount: 5,
    products: [
      {
        name: 'Red Lipstick',
        brand: 'Chanel',
        image: product1,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Lux Foundation',
        brand: 'Chanel',
        image: product2,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Red Sweater',
        brand: 'Chanel',
        image: product3,
        price: 526,
        delivery: 'Free Shipping',
        sale: -40
      },
      {
        name: 'Red Lipstick',
        brand: 'Chanel',
        image: product1,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Lux Foundation',
        brand: 'Chanel',
        image: product2,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 653,
    comments: 546
  },
  {
    id: 5,
    store: {
      name: 'Kylie Skin',
      avatar: storeAvatar1
    },
    date: '15w ago',
    description: {
      title: 'Cream canvas Big Sad Wolf Tote Bag',
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other.'
    },
    followed: false,
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster3, poster3, poster3, poster3, poster3, poster3],
    productsCount: 2,
    products: [
      {
        name: 'Chanel Lipstick',
        brand: 'Chanel',
        image: product4,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Claret Coat',
        brand: 'Zara',
        image: product5,
        price: 526,
        delivery: 'Free Shipping',
        sale: 40
      }
    ],
    likes: 653,
    comments: 546
  }
];
