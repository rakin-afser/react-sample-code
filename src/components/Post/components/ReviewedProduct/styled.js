import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  margin: 36px auto 38px;
  padding: 5px 16px 4px 0;
  max-width: 442px;
  background: #ffffff;
  border: 1px solid #eeeeee;
  box-sizing: border-box;
  border-radius: 2px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const InfoBlock = styled.div`
  display: flex;
  align-items: center;
`;
export const ProductImage = styled.img`
  margin-left: 3px;
  margin-right: 16px;
  width: 64px;
  height: 64px;
  border-radius: 2px;
`;
export const Name = styled.h3`
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  margin: 0;
`;
export const Description = styled.p`
  font-size: 12px;
  line-height: 132%;
  color: #666666;
  margin-bottom: 6px;
`;

export const ViewButton = styled.button`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  line-height: 1;
  color: #000;
  background: transparent;
  width: 99px;
  height: 28px;
  cursor: pointer;
  border: 1px solid #000000;
  border-radius: 24px;
  transition: ease 0.4s;

  span {
    position: relative;
    top: 2px;
  }

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;
