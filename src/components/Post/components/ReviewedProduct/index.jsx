import React from 'react';
import StarRating from 'components/StarRating';
import {Wrapper, InfoBlock, ProductImage, Name, Description, ViewButton} from './styled';

const ReviewedProduct = ({products = []}) => (
  <div>
    {products.map((el, i) => (
      <Wrapper key={i}>
        <InfoBlock>
          <ProductImage src={el.img} alt={el.title} />
          <div>
            <Name>{el.title}</Name>
            <Description>{el.description}</Description>
            <StarRating stars={el.rating} />
          </div>
        </InfoBlock>
        <ViewButton>
          <span>View</span>
        </ViewButton>
      </Wrapper>
    ))}
  </div>
);

export default ReviewedProduct;
