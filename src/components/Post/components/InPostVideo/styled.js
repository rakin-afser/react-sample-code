import styled from 'styled-components/macro';
import {secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const Overlay = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.25));
  transition: ease 0.4s;
  opacity: ${({videoEnded}) => (videoEnded ? '1' : '0')};
`;

export const PlayBtn = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  align-items: center;
  justify-content: center;
  width: 68px;
  height: 68px;
  background: #fafafa;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  border-radius: 50%;
  cursor: pointer;
  opacity: 0;
  transition: ease 0.4s;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    box-shadow: 0 2px 7px rgba(0, 0, 0, 0.3);

    svg {
      path {
        fill: #464646;
      }
    }
  }
`;

export const RePlayBtn = styled(PlayBtn)`
  background: rgba(27, 27, 27, 0.51);
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.04);

  &:hover {
    box-shadow: 0 2px 7px rgba(0, 0, 0, 0.3);
    background: rgba(27, 27, 27, 0.8);

    svg {
      path {
        fill: none;
      }
    }
  }
`;

export const MuteButton = styled.div`
  position: absolute;
  left: 8px;
  top: 8px;
  cursor: pointer;
  opacity: 0;
  transition: ease 0.4s;
  z-index: 10;
`;

export const Wrapper = styled.div`
  position: relative;
  width: ${({width}) => width};
  height: ${({height}) => height};

  video {
    width: inherit;
    height: inherit;
    object-fit: cover;

    &:focus {
      outline: none;
    }
  }

  &:hover ${PlayBtn} {
    opacity: ${({embedded}) => (embedded ? '0' : '1')};
  }

  &:hover ${MuteButton} {
    opacity: 1;
  }

  progress[value] {
    position: absolute;
    left: 0;
    bottom: 0;
    appearance: none;
    border: none;
    background: rgba(44, 42, 42, 0.2);
    height: 4px;
    display: inline;
    order: 1;
    width: 100%;
  }

  progress[value]::-webkit-progress-bar {
    background: rgba(44, 42, 42, 0.2);
  }

  progress[value]::-webkit-progress-value {
    background-color: ${primaryColor};
    position: relative;
    transition: width 1s linear;
  }
`;

export const ProgressNum = styled.div`
  padding: 2px 4px;
  position: absolute;
  bottom: 6px;
  right: 4px;
  background: rgba(139, 139, 139, 0.3);
  backdrop-filter: blur(8px);
  border-radius: 11px;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  line-height: 12px;
  color: #ffffff;
`;
