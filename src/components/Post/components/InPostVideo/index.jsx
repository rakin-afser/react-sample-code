import React, {useState, useRef, useEffect} from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';
import {PlayBtn, MuteButton, Wrapper, ProgressNum, RePlayBtn, Overlay} from './styled';
import {ReactComponent as IconPlay} from './img/iconPlay.svg';
import {ReactComponent as IconPause} from './img/iconPause.svg';
import {ReactComponent as IconMuted} from './img/iconMuted.svg';
import {ReactComponent as IconUnMuted} from './img/iconUnMuted.svg';
import {ReactComponent as IconReplay} from './img/iconReplay.svg';

const InPostVideo = ({src, onStopTrigger, embedded, width = '479px', height = '479px'}) => {
  const videoRef = useRef(null);
  const progressRef = useRef(null);
  const [play, setPlay] = useState(false);
  const [muted, setMuted] = useState(true);
  const [progress, setProgress] = useState(0);
  const [duration, setDuration] = useState(null);
  const [onEnded, setOnended] = useState(false);

  const onPlayToggle = () => {
    setPlay((prev) => !prev);
  };

  const onMuteToggle = () => {
    setMuted((prev) => !prev);
  };

  const onReplayVideo = () => {
    setOnended(false);
    videoRef.current.seekTo(0);
    setPlay(true);
  };

  const onProgressBar = (e) => {
    // const position = (e.nativeEvent.offsetX / progressRef.current.current.offsetWidth) * duration;
    // videoRef.current.seekTo(position);
  };

  useEffect(() => {
    setPlay(false);
  }, [onStopTrigger]);

  useEffect(() => {
    if (progress >= duration) {
      setOnended(true);
    } else {
      setOnended(false);
    }
  }, [progress, duration]);

  return (
    <Wrapper embedded={embedded} width={width} height={height}>
      <ReactPlayer
        controls={embedded}
        muted={muted}
        onProgress={(e) => setProgress(Math.floor(e.playedSeconds))}
        ref={videoRef}
        playing={play}
        url={src}
        width="100%"
        height="100%"
        onDuration={(e) => {
          setDuration(Math.floor(e));
        }}
      />
      {!embedded && <Overlay videoEnded={onEnded} />}
      <MuteButton onClick={onMuteToggle}>{muted ? <IconMuted /> : <IconUnMuted />}</MuteButton>
      {!embedded && (
        <progress
          onClick={onProgressBar}
          ref={progressRef}
          max="100"
          value={((progress / duration) * 100).toString()}
        />
      )}
      {!onEnded && <PlayBtn onClick={onPlayToggle}>{!play ? <IconPlay /> : <IconPause />}</PlayBtn>}
      {!embedded && onEnded && (
        <>
          <RePlayBtn onClick={onReplayVideo}>
            <IconReplay />
          </RePlayBtn>
        </>
      )}
      <ProgressNum>
        {progress}/{duration}
      </ProgressNum>
    </Wrapper>
  );
};

InPostVideo.defaultProps = {
  onStopTrigger: 0,
  embedded: false
};

InPostVideo.propTypes = {
  src: PropTypes.node.isRequired,
  onStopTrigger: PropTypes.number,
  embedded: PropTypes.bool
};

export default InPostVideo;
