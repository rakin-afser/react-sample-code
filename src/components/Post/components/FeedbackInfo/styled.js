import styled from 'styled-components';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding: 14px 51px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #e4e4e4;
`;

export const LikesBlock = styled.div`
  img:nth-child(2) {
    margin: 0 8px 0 -5px;
  }
`;

export const Info = styled.span`
  font-weight: normal;
  font-size: 13px;
  line-height: 16px;
  color: #8f8f8f;
`;

export const LikedBy = styled(Info)`
  color: #000000;
  transition: ease 0.4s;
`;

export const Button = styled.button`
  transition: ease 0.4s;
  cursor: pointer;

  &:hover ${LikedBy} {
    color: ${primaryColor};
    text-decoration: underline;
  }
`;
