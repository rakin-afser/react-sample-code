import React from 'react';
import PropTypes from 'prop-types';
import {Wrapper, LikesBlock, Info, LikedBy, Button} from './styled';

const FeedbackInfo = ({liked, showFollowers}) => (
  <Wrapper>
    <LikesBlock>
      <img src={liked[0].avatar} alt={liked[0].username} />
      <img src={liked[1].avatar} alt={liked[1].username} />
      <Info>
        <LikedBy>{liked[0].username}</LikedBy> and
        <Button type="button" onClick={showFollowers}>
          <LikedBy>{liked.length - 1} more</LikedBy>
        </Button>
        like this post
      </Info>
    </LikesBlock>
  </Wrapper>
);

FeedbackInfo.defaultProps = {
  liked: [],
  showFollowers: () => {}
};

FeedbackInfo.propTypes = {
  liked: PropTypes.arrayOf(PropTypes.any),
  showFollowers: PropTypes.func
};

export default FeedbackInfo;
