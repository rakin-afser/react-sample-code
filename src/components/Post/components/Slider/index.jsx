import React, {useCallback, useRef, useState} from 'react';
import SwiperCore, {Pagination, Navigation} from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
import {Wrapper, SlideImage, SlideVideo, RatioHolder, MediaHolder, Single} from './styled';
import Player from 'components/Player';
import Icon from 'components/Icon';
import LazyLoad from 'react-lazyload';

SwiperCore.use([Pagination, Navigation]);

const Slider = ({slides, autoPlay, ratio}) => {
  const [width, height] = ratio?.split(':') || [];
  const [currentSlide, setCurrentSlide] = useState(0);
  const slider = useRef(null);
  const videoRefs = useRef([]);

  const isInViewport = useCallback((el) => {
    if (!el) {
      return null;
    }
    const rect = el?.getBoundingClientRect();
    return rect?.bottom <= window?.innerHeight;
  }, []);

  const settings = {
    slidesPerView: 1,
    loop: false,
    pagination: {
      clickable: true
    },
    navigation: {
      nextEl: '.custom-swiper-button-next',
      prevEl: '.custom-swiper-button-prev'
    },
    onSlideChange: async (slide) => {
      const {realIndex, previousIndex} = slide;
      setCurrentSlide(realIndex);

      if (isInViewport(slider?.current)) {
        if (videoRefs.current[previousIndex]) {
          const video = videoRefs?.current?.[previousIndex]?.player?.player || videoRefs.current[previousIndex];
          const {pause} = video;
          pause.call(video);
        }
        if (videoRefs.current[realIndex]) {
          const video = videoRefs?.current?.[realIndex]?.player?.player || videoRefs.current[realIndex];
          const {play} = video;
          try {
            await play.call(video);
          } catch (error) {
            console.error('Video play error', error);
          }
        }
      }
      return null;
    }
  };

  function renderMedia(slide, key) {
    switch (slide?.mimeType?.split('/')?.[0]) {
      case 'image':
        return <SlideImage src={slide?.sourceUrl || slide?.mediaItemUrl} />;
      case 'video':
        return (
          <SlideVideo>
            <Player
              playOnScroll
              hidePlay
              poster={slide?.poster?.sourceUrl}
              autoPlay={key === currentSlide ? autoPlay : false}
              src={slide?.mediaItemUrl}
              ref={(el) => {
                videoRefs.current[key] = el;
              }}
            />
          </SlideVideo>
        );

      default:
        return null;
    }
  }

  function renderControls() {
    if (!slides || slides?.length <= 1) return null;

    return (
      <>
        <div className="custom-swiper-button-prev">
          <Icon type="arrow" width={11} height={18} color="#fff" />
        </div>
        <div className="custom-swiper-button-next">
          <Icon type="arrow" width={11} height={18} color="#fff" />
        </div>
      </>
    );
  }

  function renderSlider() {
    if (slides?.length > 1)
      return (
        <Swiper {...settings}>
          <div className="swiper-wrapper">
            {slides?.map((slide, key) => (
              <SwiperSlide key={slide?.id} dataIndex={key}>
                <RatioHolder ratio={height / width} />
                <MediaHolder>{renderMedia(slide, key)}</MediaHolder>
              </SwiperSlide>
            ))}
          </div>
          {renderControls()}
        </Swiper>
      );

    if (slides?.length === 1)
      return slides?.map((slide, key) => (
        <Single key={slide?.id}>
          <RatioHolder ratio={height / width} />
          <MediaHolder>{renderMedia(slide, key)}</MediaHolder>
        </Single>
      ));
    return null;
  }

  return (
    <Wrapper pb={slides?.length > 1 ? 1 : 0}>
      <LazyLoad>{renderSlider()}</LazyLoad>
    </Wrapper>
  );
};

export default Slider;
