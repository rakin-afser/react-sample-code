import {primaryColor} from 'constants/colors';
import styled, {css} from 'styled-components';

export const Wrapper = styled.div`
  position: relative;

  .swiper-container {
    ${({pb}) =>
      pb
        ? css`
            padding-bottom: 32px;
          `
        : null}

    &-horizontal > .swiper-pagination-bullets {
      font-size: 0;
      line-height: 1;
      bottom: 0;
      margin: 8px 0 16px 0;
    }

    .swiper-pagination-bullet {
      transition: background-color 0.3s ease;
      background-color: #e4e4e4;
      opacity: 1;

      &-active {
        background-color: ${primaryColor};
      }
    }
  }

  .swiper-button-disabled {
    opacity: 0.3;
  }

  .custom-swiper-button-next,
  .custom-swiper-button-prev {
    background: #00000030;
    border-radius: 3px;
    width: 37px;
    height: 37px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    z-index: 5;
    transition: background-color 0.3s ease, opacity 0.3s ease;

    &:not(.swiper-button-disabled) {
      cursor: pointer;

      &:hover {
        background: #00000060;
      }
    }
  }

  .custom-swiper-button-prev {
    left: 8px;
    svg {
      margin: 9px 11px;
      transform: scale(-1, 1);
    }
  }

  .custom-swiper-button-next {
    right: 8px;
    svg {
      margin: 9px 14px;
    }
  }
`;

export const SlideImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const SlideVideo = styled.div`
  width: 100%;
  height: 100%;

  > div {
    width: 100%;
    height: 100%;
  }

  video {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }
`;

export const RatioHolder = styled.div`
  padding-bottom: ${({ratio}) => (ratio ? ratio * 100 : 100)}%;
`;

export const MediaHolder = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  .lazyload-wrapper {
    height: 100%;
  }
`;

export const Single = styled.div`
  position: relative;
`;
