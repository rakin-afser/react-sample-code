import styled from 'styled-components';
import {blue} from 'constants/colors';

export const OptionWrapper = styled.div`
  position: absolute;
  right: 33px;
  border: 1px solid #efefef;
  box-sizing: border-box;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.09);
  border-radius: 8px;
  width: 90px;
  background-color: #ffffff;
`;

export const OptionButton = styled.div`
  font-weight: normal;
  font-size: 12px;
  text-align: center;
  padding: 6px 28px;
  cursor: pointer;
  ${({top}) => (top ? 'border-bottom: 1px solid #F8F8F8' : null)}
  transform-style: ease .4s;

  &:hover {
    color: ${blue};
  }
`;
