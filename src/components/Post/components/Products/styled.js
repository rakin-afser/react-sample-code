import styled from 'styled-components/macro';
import media from 'constants/media';
import {blue, primaryColor} from 'constants/colors';

export const Product = styled.div`
  background: #fff;
  border: 1px solid #eee !important;
  box-sizing: border-box;
  border-radius: 2px;
  padding: 5px 29px 5px 5px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  margin: 0 0 10px;

  &:last-child {
    margin-bottom: 0;
  }

  ${({isAlone}) => (isAlone ? 'border: none;' : null)};
`;

export const ProductMobile = styled.div`
  background: #fff;
  box-sizing: border-box;
  padding: 4px 18px 4px 4px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 100%;
  margin-bottom: 11px;
  position: relative;

  & + &::before {
    content: '';
    position: absolute;
    top: -5px;
    left: -16px;
    right: -16px;
    border-top: 1px solid #eeeeee;
  }

  &.item-enter {
    opacity: 0;
    margin-top: -${({height}) => height}px;
  }

  &.item-enter-active {
    opacity: 1;
    margin-top: 0;
    transition: opacity 300ms, margin-top 300ms;
  }

  &.item-exit {
    opacity: 1;
  }

  &.item-exit-active {
    opacity: 0;
    margin-top: -${({height}) => height}px;
    transition: opacity 300ms, margin-top 300ms;
  }
`;

export const ProductImage = styled.img`
  height: 64px;
  width: 64px;
  object-fit: cover;
  margin-right: 21px;
  cursor: pointer;
  transition: ease 0.4s;
  box-shadow: 0 0 0 rgba(000, 000, 000, 0);

  &:hover {
    box-shadow: 0 0 4px rgba(000, 000, 000, 0.5);
  }
`;

export const ProductImageMobile = styled.img`
  height: 54px;
  width: 54px;
  margin-right: 21px;
  object-fit: cover;
  object-position: top center;
`;

export const Description = styled.div`
  padding-top: 2px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  height: 100%;
  max-width: calc(100% - 75px - 110px);
`;

export const ProductName = styled.h6`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  display: flex;
  align-items: center;
  color: #000000;
  margin-bottom: 0;
  transition: ease 0.4s;
  cursor: pointer;

  &:hover {
    color: ${blue};
  }
`;

export const ProductNameMobile = styled.h6`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  display: flex;
  align-items: center;
  color: #000000;
  margin-bottom: 0;
  width: 100%;

  span {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

export const ProductBrand = styled.span`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  display: flex;
  align-items: center;
  color: #666666;
  margin-top: 6px;
`;

export const ProductDelivery = styled.span`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  display: flex;
  align-items: center;
  color: #666666;
  margin-top: 7px;
`;

const Parent = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  background: transparent;
  border: none;
  outline: none;
  text-decoration: none;
  font-family: 'Helvetica Neue', sans-serif;
  cursor: pointer;
  padding-bottom: 0;
`;

export const Cart = styled.div`
  display: flex;
  align-items: center;
  margin-right: 6px;
  margin-top: -2px;

  svg path {
    transition: ease 0.4s;
  }

  ${({isAlone}) =>
    isAlone &&
    `
    svg path {
      fill: ${primaryColor};
    }
  `}
`;

export const SellButton = styled(Parent)`
  margin-left: auto;
  min-width: 99px;
  width: auto;
  height: 28px;
  line-height: 1;
  padding: 2px 5px 0;
  border: 1px solid #666666;
  border-radius: 24px;
  color: #666666;
  ${({isAlone}) => (isAlone ? 'color: #ED484F; border: 1px solid #ED484F; & img {align-self: center;}' : null)};
  transition: ease 0.4s;

  i {
    margin-bottom: 2px;
    margin-right: 6px;
  }

  &:hover {
    background-color: ${primaryColor};
    color: #fff;
    border-color: ${primaryColor};

    ${Cart} {
      svg {
        path {
          fill: #fff;
        }
      }
    }
  }
`;

export const ViewButton = styled(Parent)`
  border: 0.5px solid #cccccc;
  margin-left: auto;
  border-radius: 24px;
  padding: 5px 36px;
  font-weight: 500;
`;

export const Sale = styled.div`
  align-self: flex-start;
  width: 35px;
  height: 18px;
  border-radius: 4px;
  border-top-left-radius: 0;
  background: #f0646a;
  font-size: 12px;
  line-height: 15px;
  display: flex;
  align-items: baseline;
  justify-content: center;
  color: white;
  margin-top: 5px;
  margin-left: 11px;
`;

export const ProductsBlock = styled.div`
  @media (min-width: 768px) {
    margin: 30px 0 ${({quantity}) => (quantity <= 2 ? '17px' : '0')};
  }
  @media (max-width: 767px) {
    padding-top: 26px;
  }
`;

export const ProductsExpanded = styled.div`
  max-height: ${({expanded}) => (expanded > 2 ? `1000px` : `209px`)};
  display: flex;
  flex-direction: column;
  align-items: center;
  transition: max-height ease 1s;

  @media (min-width: ${media.desktopMini}) {
    margin-bottom: ${({expanded}) => (expanded > 2 ? `22px` : `0`)};
  }
`;

export const ExpandProductsButton = styled.button`
  margin-top: 15px;
  outline: 0;
  box-sizing: border-box;
  position: relative;
  cursor: pointer;
  background: #ffffff;
  border: 1px solid #999999;
  border-radius: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
  width: 123px;
  height: 30px;
  padding: 0 6px 0 12px;
  transition: ease 0.4s;

  > span {
    margin: 0 4px;

    @media (min-width: ${media.desktopMini}) {
      color: inherit;
    }
  }

  & svg {
    margin-left: 3px;

    path {
      transition: ease 0.4s;
    }
  }

  &::before {
    position: absolute;
    z-index: 99;
    content: '';
    height: 1px;
    width: 227.5px;
    background: #cccccc;
    left: -227.5px;
  }

  &::after {
    position: absolute;
    z-index: 99;
    content: '';
    height: 1px;
    width: 225.5px;
    background: #cccccc;
    left: 100%;
  }

  &:hover {
    background-color: ${primaryColor};
    color: #fff;
    border: 1px solid ${primaryColor};

    svg {
      path {
        fill: #fff;
      }
    }
  }

  @media (max-width: 900px) {
    &::before,
    &::after {
      display: none;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    margin-top: 7px;
  }
`;

export const ExpandProductsButtonMobile = styled.button`
  outline: 0;
  box-sizing: border-box;
  border: 0;
  position: relative;
  cursor: pointer;
  background: #ffffff;
  border: 1px solid #999999;
  border-radius: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
  width: 123px;
  height: 30px;
  padding: 0 6px 0 12px;

  & span {
    margin: 0 4px;
  }

  & svg {
    margin-left: 3px;
  }

  &::before {
    position: absolute;
    z-index: 99;
    content: '';
    height: 1px;
    width: 227.5px;
    background: #cccccc;
    left: -227.5px;
  }

  &::after {
    position: absolute;
    z-index: 99;
    content: '';
    height: 1px;
    width: 225.5px;
    background: #cccccc;
    left: 100%;
  }

  @media (max-width: 900px) {
    &::before,
    &::after {
      display: none;
    }
  }
`;

export const Divider = styled.div`
  margin-top: 7px;
  margin-bottom: 8px;
  position: relative;
  width: 100%;
  display: flex;
  justify-content: center;

  &::before {
    content: '';
    display: block;
    position: absolute;
    left: -16px;
    right: -16px;
    top: 0;
    bottom: 0;
    margin-top: auto;
    margin-bottom: auto;
    background-color: #999999;
    height: 1px;
    z-index: 0;
  }
`;
