import React, {useState} from 'react';
import ClampLines from 'react-clamp-lines';
import ArrowDown from 'assets/ArrowDown';
import FadeOnMount from 'components/Transitions';
import PostView from 'components/Modals/PostView';
import {
  Product,
  ProductImage,
  Description,
  ProductName,
  ProductBrand,
  ProductDelivery,
  Sale,
  SellButton,
  Cart,
  ProductsExpanded,
  ExpandProductsButton,
  ProductsBlock
} from '../styled.js';
import {ReactComponent as IconCart} from '../img/cart.svg';
import {getDiscountPercent} from 'util/heplers';
import {useAddToCartMutation} from 'hooks/mutations.js';
import Btn from 'components/Btn/index.jsx';

const ProductItem = ({isAlone, product, onProductClick, postId}) => {
  const {variations, databaseId} = product || {};
  const {image, regularPrice} = variations?.nodes?.[0] || product || {};
  const {databaseId: variationId} = variations?.nodes?.[0] || {};
  const {sourceUrl, altText} = image || {};
  const [visible, setVisible] = useState(false);
  const [addToCart, {loading}] = useAddToCartMutation();

  function onClick() {
    onProductClick();
    setVisible(true);
  }

  return (
    <>
      <Product style={{marginBottom: 8}} isAlone={isAlone}>
        {product?.image ? <ProductImage onClick={onClick} src={sourceUrl} alt={altText} /> : null}
        <Description>
          {product?.name ? (
            <ProductName onClick={onClick}>
              <ClampLines buttons={false} text={product?.name} lines={1} />
            </ProductName>
          ) : null}
          {product?.productBrands?.nodes[0]?.name ? (
            <ProductBrand>Brand: {product?.productBrands?.nodes[0]?.name}</ProductBrand>
          ) : null}
          {product?.shippingType ? <ProductDelivery>{product?.shippingType}</ProductDelivery> : null}
        </Description>
        {product?.onSale ? (
          <Sale>
            -<b>{getDiscountPercent(product.regularPrice, product.salePrice)}</b>%
          </Sale>
        ) : null}
        <Btn
          kind={isAlone ? 'sell' : 'multisell'}
          ml="auto"
          loading={loading}
          onClick={() =>
            addToCart({
              variables: {
                input: {productId: databaseId, variationId, purchase_source: {source_type: 'FEED_POST', id: postId}}
              }
            })
          }
        >
          {regularPrice}
        </Btn>
      </Product>
      <PostView visible={visible} setVisible={setVisible} postId={postId} productId={databaseId} />
    </>
  );
};

const Products = (props) => {
  const {products, ...rest} = props || {};
  const [shownProduct, setShownProduct] = useState(2);

  const getProducts = (count = null) => {
    const isAlone = products.length === 1;
    if (count !== null) {
      return products.map((product, i) => {
        return i < count ? (
          <FadeOnMount key={product?.id}>
            <ProductItem key={product?.id} isAlone={isAlone} product={product} {...rest} />
          </FadeOnMount>
        ) : null;
      });
    }
    return products.map((product) => {
      return <ProductItem key={product?.id} isAlone={isAlone} product={product} {...rest} />;
    });
  };

  const getMoreProducts = (e) => {
    e.currentTarget.style.display = 'none';
    setShownProduct(products.length);
  };

  return (
    <ProductsBlock quantity={products.length}>
      {products.length > 2 ? (
        <ProductsExpanded expanded={shownProduct}>
          {getProducts(shownProduct)}
          <ExpandProductsButton type="button" onClick={(e) => getMoreProducts(e)}>
            See <span>{products.length - 2}</span> More <ArrowDown color="#999999" />
          </ExpandProductsButton>
        </ProductsExpanded>
      ) : products.length > 0 ? (
        getProducts()
      ) : null}
    </ProductsBlock>
  );
};

export default Products;
