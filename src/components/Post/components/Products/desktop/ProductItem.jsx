import React, {useState} from 'react';
import ClampLines from 'react-clamp-lines';
import PostView from 'components/Modals/PostView';
import {
  Product,
  ProductImage,
  Description,
  ProductName,
  ProductBrand,
  ProductDelivery,
  Sale,
  SellButton,
  Cart
} from '../styled.js';
import {ReactComponent as IconCart} from '../img/cart.svg';
import {getDiscountPercent} from 'util/heplers';

const ProductItem = ({isAlone, product, onProductClick, postId}) => {
  const {variations, databaseId} = product || {};
  const {image, regularPrice} = variations?.nodes?.[0] || product || {};
  const {sourceUrl, altText} = image || {};
  const [visible, setVisible] = useState(false);

  function onClick() {
    onProductClick();
    setVisible(true);
  }

  return (
    <>
      <Product style={{marginBottom: 8}} isAlone={isAlone}>
        {product?.image ? <ProductImage onClick={onClick} src={sourceUrl} alt={altText} /> : null}
        <Description>
          {product?.name ? (
            <ProductName onClick={onClick}>
              <ClampLines buttons={false} text={product?.name} lines={1} />
            </ProductName>
          ) : null}
          {product?.productBrands?.nodes[0]?.name ? (
            <ProductBrand>Brand: {product?.productBrands?.nodes[0]?.name}</ProductBrand>
          ) : null}
          {product?.shippingType ? <ProductDelivery>{product?.shippingType}</ProductDelivery> : null}
        </Description>
        {product?.onSale ? (
          <Sale>
            -<b>{getDiscountPercent(product.regularPrice, product.salePrice)}</b>%
          </Sale>
        ) : null}

        <SellButton isAlone={isAlone}>
          <Cart isAlone={isAlone}>
            <IconCart />
          </Cart>
          {regularPrice}
        </SellButton>
      </Product>
      <PostView visible={visible} setVisible={setVisible} postId={postId} productId={databaseId} />
    </>
  );
};

export default ProductItem;
