import {useWindowSize} from '@reach/window-size';
import React from 'react';
import PostDesktop from './PostDesktop';
import PostMobile from './PostMobile';

const Post = (props) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return isMobile ? <PostMobile {...props} /> : <PostDesktop {...props} />;
};

export default Post;
