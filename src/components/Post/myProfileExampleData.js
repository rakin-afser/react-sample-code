import React from 'react';

import video2 from 'videos/videoplayback_2.mp4';
import video1 from 'videos/videoplayback.mp4';
import kathryn_mccoy from './img/kathryn_mccoy.png';
import ginger from './img/ginger.png';
import poster1 from './img/poster1.png';
import poster3 from './img/poster3.png';
import product1 from './img/product1.png';
import product2 from './img/product2.png';
import product3 from './img/product3.png';
import product4 from './img/product4.png';
import product5 from './img/product5.png';
import linkPreview from './img/preview.png';
import user1 from './img/user1.png';
import user2 from './img/user2.png';

import Button from 'components/Buttons';

export default [
  {
    id: 7,
    store: {
      name: 'kylie_brown',
      avatar: kathryn_mccoy
    },
    date: '2m ago',
    description: {
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other. '
    },
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster1, poster3, poster1, poster1, poster1, poster1],
    videos: [video1, video2],
    embeddedVideos: ['https://youtu.be/a3ICNMQW7Ok'],
    productsCount: 3,
    products: [],
    followBtn: <Button type="follow" />,
    likes: 653,
    comments: 546
  },
  {
    id: 8,
    store: {
      name: 'kylie_brown',
      avatar: kathryn_mccoy
    },
    date: '2m ago',
    description: {
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other. '
    },
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster1, poster1, poster1, poster1, poster1, poster1],
    productsCount: 3,
    products: [
      {
        name: 'Chanel Lipstick',
        brand: 'Chanel',
        image: product4,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    followBtn: <Button type="follow" />,
    likes: 653,
    comments: 546
  },
  {
    id: 9,
    store: {
      name: 'kylie_brown',
      avatar: kathryn_mccoy
    },
    date: '2m ago',
    description: {
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other. '
    },
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster1, poster1, poster1, poster1, poster1, poster1],
    productsCount: 3,
    products: [
      {
        name: 'Chanel Lipstick',
        brand: 'Chanel',
        image: product4,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Claret Coat',
        brand: 'Zara',
        image: product5,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Chanel Lipstick',
        brand: 'Chanel',
        image: product4,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Claret Coat',
        brand: 'Zara',
        image: product5,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    followBtn: <Button type="follow" />,
    likes: 653,
    comments: 546
  },
  {
    id: 10,
    store: {
      name: 'kylie_brown',
      avatar: kathryn_mccoy
    },
    date: '2m ago',
    description: {
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other. '
    },
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster1, poster1, poster1, poster1, poster1, poster1],
    productsCount: 3,
    products: [
      {
        name: 'Chanel Lipstick',
        brand: 'Chanel',
        image: product4,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Claret Coat',
        brand: 'Zara',
        image: product5,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    followBtn: <Button type="follow" />,
    likes: 653,
    comments: 546
  },
  {
    id: 6,
    store: {
      name: 'kathryn_mccoy',
      avatar: kathryn_mccoy
    },
    date: 'Nov 10 at 3:21 PM',
    description: {
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other.'
    },
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [ginger, poster3],
    likes: 653,
    stars: 5,
    feedbackBenefit: {
      helpful: 54,
      notHelpful: 6
    },
    userReaction: [
      {username: 'arianagrande', avatar: user1},
      {username: 'arianagrande', avatar: user2},
      {username: 'arianagrande', avatar: user1},
      {username: 'arianagrande', avatar: user2},
      {username: 'arianagrande', avatar: user1},
      {username: 'arianagrande', avatar: user2},
      {username: 'arianagrande', avatar: user1},
      {username: 'arianagrande', avatar: user2}
    ],
    productsFeedback: [
      {
        img: product4,
        title: 'CK Crop Top',
        description: 'High qualuty, smooth texture',
        rating: 5
      }
    ]
  },
  {
    id: 1,
    store: {
      name: 'kathryn_mccoy',
      avatar: kathryn_mccoy
    },
    date: '2d ago',
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster1, poster1, poster1, poster1, poster1, poster1],
    productsCount: 3,
    products: [
      {
        name: 'Red Lipstick',
        brand: 'Chanel',
        image: product1,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Lux Foundation',
        brand: 'Chanel',
        image: product2,
        price: 526,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Red Sweater',
        brand: 'Chanel',
        image: product3,
        price: 526,
        delivery: 'Free Shipping',
        sale: -40
      }
    ],
    likes: 1342,
    comments: 546
  },
  {
    id: 3,
    store: {
      name: 'kathryn_mccoy',
      avatar: kathryn_mccoy
    },
    link: {
      url: 'https://www.youtube.com/watch?v=VAMpNDrsgAg',
      header: 'Hey! Check this lovely tunes out:',
      preview: {
        image: linkPreview
      }
    },
    date: '14 hours ago',
    likes: 342,
    comments: 546
  }
];
