import React, {useEffect, useState, useRef} from 'react';
import moment from 'moment';
import {useHistory} from 'react-router-dom';

import CommentsIcon from 'assets/Comments';
import Share from 'assets/Share';
import Bookmark from 'assets/Bookmark';
import BookmarkFilled from 'assets/BookmarkFilled';
import Button from 'components/Buttons';
import Comments from 'components/Comments';
import AddToList from 'components/AddToList';
import VideoQuickView from 'components/Modals/mobile/VideoQuickView';
import MainSlider from 'components/ProductPage/mobile/MainSlider';
import StarRating from 'components/StarRating';
import PostRatingMobile from 'components/PostMobile/components/PostRatingMobile';
import VideoSlider from 'components/VideoSlider';
import {useUser} from 'hooks/reactiveVars';
import useGlobal from 'store';
import FeedbackInfo from './components/FeedbackInfo';
import Options from './components/Options';
import Products from './components/Products/mobile';
import ReviewedProduct from './components/ReviewedProduct';
import bags from './img/bags.svg';
import moreIcon from './img/More_Button.svg';
import ProductsCounter from 'components/ProductsCounter';
import {
  Actions,
  ActionsWrap,
  Ads,
  BagsImageMobile,
  Benefit,
  Block,
  BookmarkWrap,
  CommentsCount,
  DateTime,
  DescriptionText,
  DescriptionTitle,
  ExternalLink,
  InflCode,
  Influencer,
  Item,
  Likes,
  LinkHeader,
  LinkPreview,
  LinkPreviewWrap,
  MoreButton,
  MoreButtonArea,
  PostContainerMobile,
  PostHeader,
  PostWithLink,
  PostWrapMobile,
  ProductCounterMobileVideo,
  ProductDescriptionMobile,
  SliderWrapMobile,
  ClickableOverlay,
  StoreData,
  StoreImageMobile,
  StoreName,
  SingleImage,
  LikeWrapper,
  CountryName
} from './styled.js';
import Icon from 'components/Icon';
import {useAddUpdateLikedPostMutation, useDeletePostMutation, useFollowUnfollowUserMutation} from 'hooks/mutations';
import {getLinkByUserByRole, stripTags} from 'util/heplers';
import useShare from 'hooks/useShare';
import Btn from 'components/Btn';
import {useOnScreenPost} from 'hooks';
import Tags from 'components/Tags';

const Post = ({
  data,
  order,
  showActions = true,
  showMoreButton = true,
  isStandStill,
  postContainerStyle,
  active,
  visible,
  setVisible
}) => {
  const [showCommentsPopup, setShowCommentsPopup] = useState(false);
  const [globalState, setGlobalState] = useGlobal();
  const [user] = useUser();
  const [addedToList, setAddedToList] = useState(false);
  const history = useHistory();
  const currentPost = useRef(null);
  const [stopped, setStopped] = useState(false);
  const ref = useOnScreenPost({
    onVisible: () => {
      if (stopped) return;
      if (visible?.includes(order)) return;
      if (setVisible) setVisible((s) => [...s, order]);
    },
    onHidden: () => {
      if (setVisible) setVisible((s) => (s?.includes(order) ? s?.filter((a) => order !== a) : s));
      if (stopped) setStopped(false);
    }
  });

  const {
    id,
    databaseId,
    author,
    comments,
    title,
    content,
    slug,
    isLiked,
    totalLikes,
    video,
    isFeedbackPost,
    seller,
    geo,
    tags,
    rating = 0,
    ratio
  } = data || {};

  const {id: authorId, databaseId: authorDatabaseId, isFollowed} = author.node || {};
  const isBelongs = authorId === user?.id;

  const [deletePost] = useDeletePostMutation({variables: {input: {id, status: 'TRASH'}}});
  const [triggerLike] = useAddUpdateLikedPostMutation({post: data});
  const [followUnfollowUser] = useFollowUnfollowUserMutation({
    id: authorId,
    isFollowed
  });

  const avatar = author?.node?.avatar?.url;
  const userName = author?.node?.username;
  const relatedProducts = data?.relatedProducts?.nodes;
  const date = moment(data?.date).format('DD MMM YYYY');
  const commentCount = data?.commentCount;
  const userRoles = author?.node?.roles?.nodes;
  const gallery = video
    ? [{mediaItemUrl: video, mimeType: 'external'}, ...(data?.galleryImages?.nodes || [])]
    : data?.galleryImages?.nodes;
  // const featuredImage = data?.featuredImage?.node?.sourceUrl;
  // const featuredImageAltTxt = data?.featuredImage?.node?.altText;

  const openQuickView = () => {
    // pause the video if quick view opened
    setStopped(true);
    setVisible([]);
    setGlobalState.setQuickView({quickViewType: 'postView', postData: data});
  };

  const addToList = (value = false) => {
    setAddedToList(value);
  };

  function onLike() {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    triggerLike({variables: {input: {id: databaseId}}});
  }

  function onComment() {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    setShowCommentsPopup(true);
  }

  const buttons = [
    isBelongs && {
      title: 'Edit'
    },
    isBelongs && {
      title: 'Delete',
      onClick: () => {
        deletePost();
        setGlobalState.setOptionsPopup({active: false});
      }
    },
    {
      title: 'Report',
      onClick: () => {
        setGlobalState.setOptionsPopup({active: false});
        setGlobalState.setReportPopup({active: true, subjectId: databaseId, subjectSource: 'post_report'});
      }
    },
    {
      title: isFollowed ? 'Unfollow' : 'Follow',
      onClick: () => {
        followUnfollowUser({variables: {input: {id: authorDatabaseId}}});
        setGlobalState.setOptionsPopup({active: false});
      }
    }
  ];

  function onMore() {
    if (user) {
      setGlobalState.setOptionsPopup({active: true, buttons});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  }

  const onSaveToList = () => {
    if (user?.databaseId) {
      setGlobalState.setActionsPopup(true, {productId: databaseId, seller, slug, title, callback: addToList});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  const onShare = async () => {
    const shareData = {
      title: `testSample - ${title}`,
      text: stripTags(content),
      url: `${window.location.origin}/post/${databaseId}`
    };

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <PostWrapMobile order={order} ref={currentPost}>
      {/* {data.userReaction && <FeedbackInfo liked={data.userReaction} />} */}
      <PostContainerMobile style={postContainerStyle}>
        <PostHeader>
          <StoreData>
            {avatar ? (
              <StoreImageMobile
                src={avatar}
                alt={userName}
                onClick={() => history.push(getLinkByUserByRole(userRoles, author?.node?.databaseId))}
              />
            ) : null}
            <Block>
              {userName ? <StoreName>{userName}</StoreName> : null}
              {/* {data.influencer && (
                <Influencer>
                  <StarThin />
                  <span style={{marginLeft: 4}}>Influencer</span>
                </Influencer>
              )} */}
              {date ? <DateTime>{date}</DateTime> : null}
              {geo && geo.city ? (
                <CountryName>{`${geo.city}${geo.country_name ? `, ${geo.country_name}` : ''}`}</CountryName>
              ) : null}
            </Block>
          </StoreData>
          <Actions>
            {isBelongs ? null : (
              <Btn
                kind={isFollowed ? 'following-md' : 'follow-md-iconless'}
                onClick={() =>
                  followUnfollowUser({
                    variables: {input: {id: authorDatabaseId}}
                  })
                }
              />
            )}
            {showMoreButton ? (
              <MoreButtonArea>
                <MoreButton onClick={onMore} src={moreIcon} alt="moreSettings " />
              </MoreButtonArea>
            ) : null}
          </Actions>
        </PostHeader>
        {isFeedbackPost ? <PostRatingMobile initialState={{likes: 0, dislikes: 0}} rating={Number(rating)} /> : null}
        {/* {data.ads && <Ads href={data.ads.href}>{data.ads.title}</Ads>} */}

        <ProductDescriptionMobile>
          <DescriptionTitle>{title}</DescriptionTitle>
          {content ? <DescriptionText dangerouslySetInnerHTML={{__html: content}} /> : null}
          <Tags items={tags?.nodes} />
        </ProductDescriptionMobile>
        {/* {data?.posters ? (
          <SliderWrapMobile>
            {data?.products ? (
              <ProductCounterMobile>
                <BagsImageMobile src={bags} alt="bags" /> {data?.products?.length}
              </ProductCounterMobile>
            ) : null}
            <MainSlider slides={data?.posters} />
          </SliderWrapMobile>
        ) : null} */}
        {gallery?.length ? (
          <SliderWrapMobile ref={ref}>
            <ProductsCounter count={relatedProducts?.length} />
            <MainSlider
              autoPlay={active}
              hidePlay
              offset={50}
              fluidVideos
              slides={gallery}
              isStandStill={isStandStill}
              playOnScroll
              ratio={ratio}
            />
          </SliderWrapMobile>
        ) : null}

        {/* {data?.videos && (
          <SliderWrapMobile>
            <ClickableOverlay onClick={() => setVideoView(true)} />
            <VideoQuickView active={videoView} setActive={setVideoView} />
            {/* {data.products ? (
              <ProductCounterMobileVideo>
                <BagsImageMobile src={bags} alt="bags" /> {data.products.length}
              </ProductCounterMobileVideo>
            ) : null}
            <VideoSlider slides={data.videos} />
          </SliderWrapMobile>
        )} */}
        {/* {data.link ? (
          <PostWithLink>
            {data.link.header !== undefined ? <LinkHeader>{data.link.header}</LinkHeader> : null}
            {data.link.url ? <ExternalLink href={data.link.url}>{data.link.url}</ExternalLink> : null}
            {data.link.preview ? (
              <LinkPreviewWrap>
                <LinkPreview src={data.link.preview.image} alt="link preview info" />
              </LinkPreviewWrap>
            ) : null}
          </PostWithLink>
        ) : null} */}
        {/* {data.inflCode && (
          <InflCode>
            Influencer’s Code: <span>{data.inflCode}</span>
          </InflCode>
        )} */}
        {relatedProducts?.length ? (
          <Products openQuickView={openQuickView} products={relatedProducts} postId={databaseId} />
        ) : null}
        {/* {data.productsFeedback ? <ReviewedProduct products={data.productsFeedback} /> : null} */}
        {showActions ? (
          <ActionsWrap>
            <Item>
              <LikeWrapper onClick={onLike} isLiked={isLiked}>
                <Icon
                  type={isLiked ? 'liked' : 'like'}
                  color={isLiked ? '#ED484F' : '#999999'}
                  width="20"
                  height="18"
                />
                {totalLikes ? <Likes>{totalLikes}</Likes> : null}
              </LikeWrapper>
              {/* {data.likes ? (
                  <Likes>
                    {data.likes < 1000
                      ? isLiked
                        ? data.likes + 1
                        : data.likes
                      : data.likes < 1000000
                      ? changeLikesFormat(data.likes)
                      : changeLikesFormat(data.likes, 2)}
                  </Likes>
                ) : null} */}
            </Item>
            <Item onClick={onComment}>
              <CommentsIcon fill="#999999" />
              {commentCount ? <CommentsCount>{commentCount}</CommentsCount> : null}
            </Item>

            <Item>
              <Share fill="#999999" onClick={onShare} />
            </Item>

            {relatedProducts?.length ? (
              <Item
                marginAuto
                onClick={() => {
                  onSaveToList();
                }}
              >
                {addedToList ? <BookmarkFilled fill="#999999" /> : <Bookmark fill="#999999" />}
              </Item>
            ) : null}
          </ActionsWrap>
        ) : null}
      </PostContainerMobile>
      <Comments
        postId={id}
        commentOn={databaseId}
        data={comments?.nodes}
        isModal={false}
        showCommentsPopup={showCommentsPopup}
        setShowCommentsPopup={setShowCommentsPopup}
        commentCount={commentCount}
      />
    </PostWrapMobile>
  );
};

export default Post;
