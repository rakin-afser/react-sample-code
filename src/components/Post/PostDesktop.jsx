import React, {useState, useRef, useCallback, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import moment from 'moment';
import parse from 'html-react-parser';
import PropTypes from 'prop-types';
import {CSSTransition} from 'react-transition-group';
import {message} from 'antd';
import avatarPlaceholder from 'images/avatarPlaceholder.png';
import postData from 'components/Post/examplePostData.js';
import myProfileExampleData from 'components/Post/myProfileExampleData';

import {ADD_REPORT} from 'mutations';
import {useMutation} from '@apollo/client';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import LikesComponent from 'components/Likes';
import Button from 'components/Buttons';
import ShareBlock from 'components/ShareBlock';
import Comments from 'components/Comments';
import StarRating from 'components/StarRating';
import Bookmark from 'assets/Bookmark';
import {mainFont} from 'constants/fonts';
import AddToList from 'components/AddToList';
import InPostVideo from 'components/Post/components/InPostVideo';
import {ReactComponent as IconComments} from './img/iconComments.svg';
import moreIcon from './img/More_Button.svg';
import bags from './img/bags.svg';
import Products from './components/Products/desktop';
import Options from './components/Options';
import FeedbackInfo from './components/FeedbackInfo';
import Slider from './components/Slider';
import ReviewedProduct from './components/ReviewedProduct';
import {ReactComponent as IconShare} from './img/iconShare.svg';
import {
  PostWrap,
  PostHeader,
  StoreImage,
  Block,
  DateTime,
  CountryName,
  StoreName,
  StoreData,
  Actions,
  MoreButton,
  VideoWrapper,
  Poster,
  ProductCounter,
  ActionsWrap,
  Item,
  CommentsCount,
  BookmarkWrap,
  LinkPreviewWrap,
  ProductDescription,
  DescriptionTitle,
  DescriptionText,
  ExternalLink,
  SliderWrap,
  SingleImage,
  BagsImage,
  PostWithLink,
  LinkHeader,
  LinkPreview,
  PostContainer,
  Benefit,
  RatingBlock,
  MoreButtonArea,
  ShareWrapper,
  OptionsWrapper,
  IconShareWrapper,
  CommentsWrapper,
  RatioHolder,
  MediaHolder,
  Right,
  Slide
} from './styled.js';
import VideoPlayer from 'components/VideoPlayer';
import {useAddUpdateLikedPostMutation, useDeletePostMutation, useFollowUnfollowUserMutation} from 'hooks/mutations';
import Btn from 'components/Btn';
import Tags from 'components/Tags';
import facebook from 'images/svg/facebook.svg';
import twitter from 'images/svg/twitter.svg';
import email from 'images/svg/mail.svg';
import link from 'images/svg/link.svg';
import {stripTags} from 'util/heplers';
import {useOnScreenPost} from 'hooks';

const PostDesktop = ({data, order, showReportPopup, forFeed, active, visible, setVisible}) => {
  const [triggerLike] = useAddUpdateLikedPostMutation({post: data});
  const history = useHistory();
  const [, setGlobalState] = useGlobal();
  const [user] = useUser();
  const [postEditing, setPostEditing] = useState(false);
  const [sharing, setSharing] = useState(false);
  const [showAddToList, setShowAddToList] = useState(false);
  const [showComments, setShowComments] = useState(false);
  const [currentSlide, setCurrentSlide] = useState(0);
  // const [pausedByUser, setPausedByUser] = useState(false);
  const textAreaLink = useRef(null);
  const [stopped, setStopped] = useState(false);
  const ref = useOnScreenPost({
    onVisible: () => {
      if (stopped) return;
      if (visible?.includes(order)) return;
      if (setVisible) setVisible((s) => [...s, order]);
    },
    onHidden: () => {
      if (setVisible) setVisible((s) => (s?.includes(order) ? s?.filter((a) => order !== a) : s));
      if (stopped) setStopped(false);
    }
  });
  const [addReport] = useMutation(ADD_REPORT, {
    onError() {
      message.error('Please try to sign in or check the form data.');
    }
  });

  const {id, databaseId: postID, isLiked, title, content, totalLikes, isFeedbackPost, rating = 0, author, geo, ratio} =
    data || {};
  const {id: authorId, databaseId, isFollowed, nicename} = author?.node || {};

  const [deletePost] = useDeletePostMutation({variables: {input: {id, status: 'TRASH'}}});
  const [followUnfollowUser] = useFollowUnfollowUserMutation({id: authorId, isFollowed});

  const onPostDelete = () => {
    deletePost();
    // setPostEditing(false);
    // setGlobalState.setActionAlert({data: data.id, open: true});
  };

  const onSubmitReport = async (value) => {
    const response = await addReport({
      variables: {input: {reason: value.category, description: value.text, subjectId: postID}}
    });

    return response;
  };

  const copyLink = () => {
    textAreaLink.current.select();
    document.execCommand('copy');
  };

  const onOpenActions = () => {
    const buttons = [
      {
        title: 'Report',
        style: {
          color: '#EA2C34'
        },
        onClick: () => {
          setGlobalState.setPostActions(false);
          setGlobalState.setReportModal(true, {onSubmit: onSubmitReport});
        }
      },
      {
        title: 'Copy Link',
        onClick: () => {
          copyLink();
          setGlobalState.setPostActions(false);
        }
      },
      {
        title: 'Share to...',
        onClick: () => {
          setGlobalState.setPostShare(true, {
            buttons: [
              {
                title: `<span class="icon"><img src="${facebook}" /></span> <span>Share to Facebook</span>`,
                onClick: () => {
                  setGlobalState.setPostShare(false);
                }
              },
              {
                title: `<span class="icon"><img src="${twitter}" /></span> <span>Share to Twitter</span>`,
                onClick: () => {
                  setGlobalState.setPostShare(false);
                }
              },
              {
                title: `<span class="icon"><img src="${email}" /></span> <span>Share via email</span>`,
                onClick: () => {
                  setGlobalState.setPostShare(false);
                }
              },
              {
                title: `<span class="icon"><img src="${link}" /></span> <span>Copy Link</span>`,
                onClick: () => {
                  copyLink();
                  setGlobalState.setPostShare(false);
                }
              }
            ]
          });
          setGlobalState.setPostActions(false);
        }
      },
      // {
      //   title: 'Turn on Post Notifivations',
      //   onClick: () => {
      //     setGlobalState.setPostActions(false);
      //   }
      // },
      // {
      //   title: 'Mute',
      //   onClick: () => {
      //     setGlobalState.setPostActions(false);
      //   }
      // },
      {
        title: isFollowed ? 'Unfollow' : 'Follow',
        onClick: () => {
          followUnfollowUser({
            variables: {input: {id: databaseId}}
          });
          setGlobalState.setPostActions(false);
        }
      }
    ];

    setGlobalState.setPostActions(true, {buttons});
    // setGlobalState.setReportModal(true, {onSubmit: onSubmitReport});
  };

  const avatar = data?.author?.node?.avatar?.url;
  const userName = data?.author?.node?.name;
  const userRoles = data?.author?.node?.roles?.nodes;
  const relatedProducts = data?.relatedProducts?.nodes;
  const date = moment(data?.date).format('DD MMM YYYY');

  const getLinkByUserByRole = (roles) => {
    if (roles.some((el) => el.name === 'seller')) {
      return `/shop/${nicename}/products`;
    }

    return `/profile/${databaseId}/posts`;
  };

  const onClick = () => {
    // pause the video if quick view opened
    setStopped(true);
    setVisible([]);
  };

  function onEnter(node) {
    node.style.setProperty('margin-top', `-${node.clientHeight}px`);
  }

  function onEntering(node) {
    node.style.setProperty('margin-top', '');
  }

  function onEntered(node) {
    node.style.setProperty('margin-top', '');
  }

  function onExiting(node) {
    node.style.setProperty('margin-top', `-${node.clientHeight}px`);
  }

  const onLike = () => {
    if (!user?.databaseId) {
      history.push('/auth/sign-in');
      return;
    }

    triggerLike({variables: {input: {id: data?.databaseId}}});
  };

  const onComment = () => {
    if (!user?.databaseId) {
      history.push('/auth/sign-in');
      return;
    }

    setShowComments((prev) => !prev);
  };

  const onSaveToList = () => {
    if (!user?.databaseId) {
      history.push('/auth/sign-in');
      return;
    }

    setShowAddToList(true);
  };

  const onShare = async () => {
    const shareData = {
      title: `testSample - ${title}`,
      text: stripTags(content),
      url: `${window.location.origin}/post/${postID}`
    };

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <PostWrap forFeed={forFeed}>
      {/* {postData.userReaction && ( */}
      {/*   <FeedbackInfo showFollowers={() => setGlobalState.setFollowersModal(true)} liked={postData.userReaction} /> */}
      {/* )} */}

      <PostContainer forFeed={forFeed}>
        <textarea
          value={`${window.location.origin}/post/${postID}`}
          ref={textAreaLink}
          style={{position: 'absolute', opacity: 0, pointerEvents: 'none'}}
        />
        <PostHeader>
          <StoreData>
            <StoreImage
              onClick={() => history.push(`${getLinkByUserByRole(userRoles)}`)}
              src={avatar || avatarPlaceholder}
              alt={userName}
            />
            <Block>
              {userName ? (
                <StoreName onClick={() => history.push(`${getLinkByUserByRole(userRoles)}`)}>{userName}</StoreName>
              ) : null}
              {date ? <DateTime>{date}</DateTime> : null}
              {geo && geo.city ? (
                <CountryName>{`${geo.city}${geo.country_name ? `, ${geo.country_name}` : ''}`}</CountryName>
              ) : null}
            </Block>
          </StoreData>

          {user?.email ? (
            <Actions>
              {user?.databaseId === databaseId ? null : (
                <Btn
                  kind={isFollowed ? 'following-md' : 'follow-md'}
                  onClick={() =>
                    followUnfollowUser({
                      variables: {input: {id: databaseId}}
                    })
                  }
                />
              )}
              {/* {postData.followBtn} */}
              {/* {postData.hasOwnProperty('followed') && */}
              {/*   (postData.followed ? <Button type="following" /> : <Button type="follow" />)} */}
              <CSSTransition in={postEditing} timeout={100} unmountOnExit>
                <OptionsWrapper>
                  {user?.id === authorId ? (
                    <Options editAction={() => setPostEditing(false)} deleteAction={onPostDelete} />
                  ) : null}
                </OptionsWrapper>
              </CSSTransition>
              <MoreButtonArea
                onClick={() => {
                  if (user?.id !== authorId) {
                    onOpenActions();
                  }
                }}
              >
                <MoreButton onClick={() => setPostEditing(!postEditing)} src={moreIcon} alt="moreSettings " />
              </MoreButtonArea>
            </Actions>
          ) : null}
        </PostHeader>

        {isFeedbackPost ? (
          <RatingBlock>
            <span>Feedback</span>
            <StarRating stars={Number(rating)} starWidth="8px" starHeight="8px" />{' '}
          </RatingBlock>
        ) : null}

        <ProductDescription>
          <DescriptionTitle>{data?.title ? data?.title : null}</DescriptionTitle>
          <DescriptionText>{data?.content ? parse(data?.content) : null}</DescriptionText>
          <Tags items={data?.tags?.nodes} />
        </ProductDescription>

        <div>
          {data?.galleryImages?.nodes?.length ? (
            <SliderWrap ref={ref}>
              {relatedProducts?.length > 1 ? (
                <ProductCounter>
                  <BagsImage src={bags} alt="bags" /> {relatedProducts?.length}
                </ProductCounter>
              ) : null}
              <Slider autoPlay={active} slides={data?.galleryImages?.nodes} ratio={ratio} />
            </SliderWrap>
          ) : null}
        </div>

        {/*  {myProfileExampleData[6].link ? ( // todo embedding from socials needs to connect  */}
        {/*    <PostWithLink>  */}
        {/*      {myProfileExampleData[6].link.header !== undefined ? (  */}
        {/*        <LinkHeader>{myProfileExampleData[6].link.header}</LinkHeader>  */}
        {/*      ) : null}  */}
        {/*      {myProfileExampleData[6].link.url ? (  */}
        {/*        <ExternalLink href={myProfileExampleData[6].link.url}>{myProfileExampleData[6].link.url}</ExternalLink>  */}
        {/*      ) : null}  */}
        {/*      {myProfileExampleData[6].link.preview ? (  */}
        {/*        <LinkPreviewWrap>  */}
        {/*          <LinkPreview src={myProfileExampleData[6].link.preview.image} alt="link preview info" />  */}
        {/*        </LinkPreviewWrap>  */}
        {/*      ) : null}  */}
        {/*    </PostWithLink>  */}
        {/*  ) : null}  */}

        {relatedProducts && relatedProducts.length > 0 ? (
          <div style={{marginBottom: 3}}>
            <Products postId={postID} products={relatedProducts} onProductClick={onClick} />
          </div>
        ) : null}
        {myProfileExampleData[5].productsFeedback ? (
          <div style={{marginBottom: 4}}>
            <ReviewedProduct products={myProfileExampleData[5].productsFeedback} />
          </div>
        ) : null}

        <ActionsWrap>
          <Item onClick={onLike}>
            <LikesComponent
              width={20}
              height={20}
              isLiked={isLiked}
              likesCount={totalLikes}
              showCount
              style={{fontSize: 14, color: '#8F8F8F', fontFamily: mainFont}}
            />
          </Item>
          <Item showComments={showComments} onClick={onComment}>
            <CommentsCount showComments={showComments}>
              <IconComments />
              {data?.commentCount ? <span>{data?.commentCount}</span> : null}
            </CommentsCount>
          </Item>
          <Item onClick={onShare}>
            <IconShareWrapper>
              <IconShare />
            </IconShareWrapper>
          </Item>
          <Right>
            {sharing && (
              <ShareWrapper>
                <ShareBlock onClose={() => setSharing(false)} title="Share Products" />
              </ShareWrapper>
            )}
            {/*  {myProfileExampleData[4].feedbackBenefit ? ( // todo need to be done  */}
            {/*    <>  */}
            {/*      <Benefit>Helpful ({myProfileExampleData[4].feedbackBenefit.helpful})</Benefit>  */}
            {/*      <Benefit>Not helpful ({myProfileExampleData[4].feedbackBenefit.notHelpful})</Benefit>  */}
            {/*    </>  */}
            {/*  ) : (  */}
            {/*    <BookmarkWrap>  */}
            {/*      <Bookmark  */}
            {/*        onClick={() => {  */}
            {/*          setShowAddToList(true);  */}
            {/*        }}  */}
            {/*        fill="#8f8f8f"  */}
            {/*        isWished={false}  */}
            {/*        width={16}  */}
            {/*        height={20}  */}
            {/*      />  */}
            {/*      <AddToList  */}
            {/*        productId={data?.databaseId}  */}
            {/*        showAddToList={showAddToList}  */}
            {/*        setShowAddToList={setShowAddToList}  */}
            {/*      />  */}
            {/*    </BookmarkWrap>  */}
            {/*  )}  */}
            {isFeedbackPost ? ( // todo need to be done (add handlers & correct counts)
              <>
                <Benefit>Helpful (0)</Benefit>
                <Benefit>Not helpful (0)</Benefit>
              </>
            ) : (
              <Item onClick={onSaveToList}>
                <BookmarkWrap>
                  <Bookmark fill="#8f8f8f" isWished={false} width={16} height={20} />
                  <AddToList
                    productId={data?.databaseId}
                    showAddToList={showAddToList}
                    setShowAddToList={setShowAddToList}
                  />
                </BookmarkWrap>
              </Item>
            )}
          </Right>
        </ActionsWrap>
      </PostContainer>

      <CSSTransition
        onEnter={onEnter}
        onEntering={onEntering}
        onExiting={onExiting}
        onEntered={onEntered}
        in={showComments}
        timeout={300}
        unmountOnExit
      >
        <CommentsWrapper>
          <Comments
            postId={id}
            data={data?.comments?.nodes}
            commentOn={data?.databaseId}
            showReportPopup={showReportPopup}
            isModal={false}
          />
        </CommentsWrapper>
      </CSSTransition>
    </PostWrap>
  );
};

PostDesktop.defaultProps = {
  data: [],
  order: 0,
  showReportPopup: () => {}
};

PostDesktop.propTypes = {
  data: PropTypes.shape(),
  order: PropTypes.number,
  showReportPopup: PropTypes.func
};

export default PostDesktop;
