import video1 from 'videos/videoplayback.mp4';
import video2 from 'videos/videoplayback_2.mp4';
import storeAvatar1 from './img/avatar1.png';
import poster1 from './img/poster1.png';
import poster2 from './img/poster2.png';
import poster3 from './img/poster3.png';
import product1 from './img/product1.png';
import product4 from './img/product4.png';

export default [
  {
    id: 1,
    store: {
      name: 'Chanel Beauty',
      avatar: storeAvatar1
    },
    date: '10m ago',
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster1, poster1, poster1, poster1, poster1, poster1],
    productsCount: 1,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product1,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 252,
    comments: 23
  },
  {
    id: 2,
    store: {
      name: 'Kylie Skin',
      avatar: storeAvatar1
    },
    date: '2m ago',
    videos: [video1, video2],
    productsCount: 3,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product1,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Chanel Lipstick',
        image: product1,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      },
      {
        name: 'Chanel Lipstick',
        image: product1,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 343,
    comments: 65
  },
  {
    id: 3,
    store: {
      name: 'Beats by Dr. Dre',
      avatar: storeAvatar1
    },
    ads: {
      title: 'This post is sponsored and contain ad or promotion',
      href: 'https://www.google.com'
    },
    description: {
      title: 'BOBBI BROWN/ 2019/ WINTER',
      text:
        'That new collection is just coming out, baes. Feature large placement print to one side and a small slogan print to the other. '
    },
    date: '5m ago',
    tags: ['#Cream', '#canvas', '#Big', '#tags'],
    posters: [poster2],
    likes: 635,
    comments: 324
  },
  {
    id: 4,
    store: {
      name: 'billieeilish',
      avatar: storeAvatar1
    },
    influencer: true,
    inflCode: 'HAPPY 2020',
    description: {
      title: 'New colors - new life!',
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other.'
    },
    date: '2m ago',
    tags: ['#Cream', '#Canvas', '#Big', '#tags'],
    posters: [poster2, poster2, poster2],
    productsCount: 1,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product1,
        delivery: 'Free Shipping',
        sale: null,
        price: 490
      }
    ],
    likes: 3102,
    comments: 5
  },
  {
    id: 5,
    store: {
      name: 'sammi.maria',
      avatar: storeAvatar1
    },
    date: '19m ago',
    posters: [poster2, poster2, poster2],
    productsCount: 1,
    products: [
      {
        name: 'Black Sweater',
        image: product1,
        delivery: 'Free Shipping',
        sale: null,
        view: true
      }
    ],
    likes: 3102,
    comments: 5
  },
  {
    id: 6,
    store: {
      name: 'gigihadid',
      avatar: storeAvatar1
    },
    stars: 5,
    date: '15w ago',
    description: {
      title: 'I just love it <3 <3',
      text:
        'New collection is coming in spring 2020 and we created so many things which are really awesome and new, i love it so much and i recommend it!'
    },
    tags: ['#topproducts', '#electronics', '#headphones'],
    posters: [poster3, poster3, poster3, poster3, poster3, poster3],
    productsCount: 1,
    products: [
      {
        name: 'Beats Solo Pro',
        image: product4,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 653,
    comments: 546
  }
];
