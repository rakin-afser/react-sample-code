import React from 'react';
import {Card, Header, Avatar, HeaderInfo, Title, Status, Image, Post} from './styled';

const EmptyNews = ({...props}) => (
  <Card {...props}>
    <Header>
      <Avatar />
      <HeaderInfo>
        <Title />
        <Status />
      </HeaderInfo>
    </Header>
    <Image />
    <Post />
  </Card>
);

export default EmptyNews;
