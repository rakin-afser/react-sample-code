import styled, {keyframes} from 'styled-components';

const loading = keyframes`
  to {
    background-position: calc(100% + 100px) 50%;
  }
`;

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  width: 280px;
  padding: 12px 14px;
  margin: 10px auto;
  background-color: #eee;
  position: relative;

  &:after {
    display: block;
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(90deg, transparent 0, #eee, #f5f5f5, #eee, transparent 100%);
    background-size: 100px 100%;
    background-position: -100px 50%;
    background-repeat: no-repeat;
    z-index: 1;
    top: 0;
    left: 0;
    animation: ${loading} 1.5s infinite;
  }
`;

export const Header = styled.div`
  display: flex;
`;

export const Avatar = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background-color: white;
`;

export const HeaderInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 15px;
`;

export const Title = styled.div`
  width: 120px;
  height: 14px;
  margin-bottom: 10px;
  background-color: white;
`;
export const Status = styled.div`
  width: 150px;
  height: 10px;
  margin-bottom: 10px;
  background-color: white;
`;
export const Image = styled.div`
  width: 70px;
  height: 70px;
  margin: 20px 0;
  background-color: white;
  border-radius: 5px;
`;

export const Post = styled.div`
  width: 100%;
  height: 30px;
  background-color: white;
`;
