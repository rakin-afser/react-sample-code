import React from 'react';
import Grid from 'components/Grid';
import Product from 'components/Cards/Product';
import exampleData from './exampleData';

const Result = () => (
  <Grid sb wrap margin="41px 0 0 0">
    {exampleData.map((item, i) => (
      <Product key={i} index={i} content={item} margin="0 0 48px 0" />
    ))}
  </Grid>
);

export default Result;
