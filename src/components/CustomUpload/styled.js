import styled from 'styled-components';
import {Upload} from 'antd';

export const StyledUpload = styled(Upload)`
  &&& {
    .ant-upload {
      border: none;
      background-color: transparent;
      height: auto;

      &-btn {
        border: 1px solid #efefef;
        color: #000000;
        padding: 18px 5px;
        max-width: 257px;
        margin: 0 auto 25px;
        border-radius: 4px;
        font-weight: 500;
      }

      &-drag-container {
        display: flex;
        justify-content: center;
        align-items: center;
        line-height: 1.2;
      }

      svg {
        margin-right: 13px;
      }
    }

    .ant-upload-list-picture-card {
      display: flex;
      flex-wrap: wrap;
      margin: 0 -2.5px;

      .ant-upload-list-item {
        padding: 0;
        border-radius: 2px;
        border: none;
        float: none;
        width: calc(20% - 5px);
        padding-bottom: calc(20% - 5px);
        height: auto;
        margin: 4px 2.5px;
      }

      .ant-upload-list-item-actions {
        opacity: 1;
        width: 16px;
        height: 16px;
        background-color: #fafafa;
        opacity: 0.65;
        border-radius: 50%;
        transform: rotate(45deg);
        top: 4px;
        right: 4px;
        left: auto;

        &::before,
        &::after {
          content: '';
          position: absolute;
          height: 1px;
          border-radius: 1px;
          width: 6px;
          background-color: #000;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          margin: auto;
        }

        &::after {
          transform: rotate(90deg);
        }

        i {
          position: absolute;
          width: 100%;
          height: 100%;
          z-index: 100;
        }

        svg {
          display: none;
        }
      }

      .ant-upload-list-item-info {
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;

        &::before {
          opacity: 0;
        }
      }
    }
  }
`;
