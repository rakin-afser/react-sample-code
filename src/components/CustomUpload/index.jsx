import React from 'react';
import Icon from 'components/Icon';
import {StyledUpload} from './styled';

const CustomUpload = () => {
  return (
    <StyledUpload
      showUploadList={{showPreviewIcon: false, showRemoveIcon: true, showDownloadIcon: false}}
      type="drag"
      multiple
      listType="picture-card"
    >
      <Icon type="plus" width={14} height={14} />
      Add Images or Videos
    </StyledUpload>
  );
};

export default CustomUpload;
