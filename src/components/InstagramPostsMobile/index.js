import React, {useMemo} from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import {useWindowSize} from '@reach/window-size';
import {
  InstagramWrap,
  InstagramTitle,
  InstagramPost,
  InstagramPostsBlock,
  Checkout,
  CheckoutText,
  CheckoutButton
} from './styled';
import {divideArrayToParts} from '../../util/heplers';
import Instagram from '../../assets/Instagram';
import FormMessage from '../FormMessage';

const INSTAGRAM_SLIDER_SETTINGS = {
  slidesToShow: 1,
  arrows: false,
  slidesToScroll: 1,
  dots: true,
  swipeToSlide: true,
  infinite: true,
  mobileFirst: true,
  sliderPerRow: 2
};

const InstagramPostsMobile = ({posts}) => {
  const {width} = useWindowSize();

  const getInstagramPosts = (instagramPosts) => {
    const groupedPosts = divideArrayToParts(instagramPosts, 6);
    return groupedPosts.map((postsArray, index) => (
      <InstagramPostsBlock key={index} width={width}>
        {postsArray.map((post, index) => (
          <InstagramPost key={index} src={post.image} width={parseInt(width / 3.05) - 15.5} />
        ))}
      </InstagramPostsBlock>
    ));
  };

  const memoizedPosts = useMemo(() => getInstagramPosts(posts), [posts]);
  return (
    <InstagramWrap>
      <InstagramTitle>Instagram Posts</InstagramTitle>
      <Slider {...INSTAGRAM_SLIDER_SETTINGS}>{memoizedPosts}</Slider>
      <Checkout>
        <CheckoutText>Checkout my instagram photos</CheckoutText>
        <CheckoutButton>
          <Instagram width={20} height={20} fill="#fff" /> <span>Connect</span>
        </CheckoutButton>
      </Checkout>
    </InstagramWrap>
  );
};

InstagramPostsMobile.defaultProps = {
  posts: []
};

InstagramPostsMobile.propTypes = {
  posts: PropTypes.array
};

export default InstagramPostsMobile;
