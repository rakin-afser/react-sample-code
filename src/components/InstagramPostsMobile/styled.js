import styled from 'styled-components';

export const InstagramWrap = styled.div`
  width: 100%;
  max-width: 676px;
  padding-left: 16px;
  padding-bottom: 21px;

  .slick-slider {
    width: 1px;
    min-width: 100%;
    width: 100%;
  }

  .slick-slider .slick-prev {
    right: 114px;
    left: auto;
    top: -45px;
  }
  .slick-slider .slick-next {
    right: 18px;
    top: -45px;
  }
  .slick-slider .slick-dots {
    text-align: right;
    bottom: auto;
    top: -41px;
    right: 21px;
  }
  .slick-slider .slick-dots li {
    width: 8px;
    height: 8px;
  }
  .slick-slider .slick-dots li button:before {
    color: #e4e4e4;
    opacity: 1;
    font-size: 8px;
  }
  .slick-slider .slick-dots li.slick-active button:before {
    color: #ea2c34;
  }
  .slick-slider .slick-list {
    z-index: 1;
  }
`;

export const InstagramTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  margin-bottom: 16px;
  color: #000;
`;

export const InstagramPostsBlock = styled.div`
  display: flex !important;
  flex-wrap: wrap;
  max-width: 767px;
  width: ${({width}) => width - 16 + 'px !important'};
`;

export const InstagramPost = styled.div`
  border-radius: 4px;
  width: ${({width}) => width + 'px !important'};
  height: ${({width}) => width + 'px !important'};

  margin: 0 10px 8px 0;
  background-image: ${({src}) => 'url(' + src + ')'};
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

export const Checkout = styled.div`
  font-size: 12px;
  line-height: 14px;
  font-family: 'Helvetica Neue', sans-serif;
  margin-top: 6px;
  padding-right: 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const CheckoutText = styled.div`
  color: #999;
`;

export const CheckoutButton = styled.button`
  width: 98px;
  height: 36px;
  background-color: #ed484f;
  border-radius: 4px;
  color: #fff;
  display: flex;
  align-items: center;

  & svg {
    margin: 0 7px;
  }
`;
