import styled from 'styled-components';
import {primaryColor} from 'constants/colors';

export const Reply = styled.div`
  position: absolute;
  z-index: 90;
  bottom: ${({active}) => (active ? '65px' : '0px')};
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #999999;
  height: 29px;
  padding: 0 16px;
  border-top: 1px solid #efefef;
  background: #fff;
  transition: all 0.3s ease;

  span {
    color: #000;
  }
`;

export const ReplyIcon = styled.div`
  margin-left: auto;
  margin-right: -3px;
`;

export const Comment = styled.div`
  padding: 21px 25px 6px 16px;
  display: flex;
  width: 100%;
  flex-wrap: wrap;

  &:not(:last-child) {
    border-bottom: 1px solid #efefef;
  }
`;

export const CommentAvatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  object-fit: cover;
  margin-top: -2px;
`;

export const CommentContent = styled.div`
  width: calc(100% - 40px);
  padding: 0 0 0 10px;
`;

export const CommentName = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  margin: 0 0 8px 0;
  display: flex;
  align-items: center;

  span {
    margin-left: auto;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    color: #4f4f4f;
  }
`;

export const ShowAll = styled.p`
  font-weight: 400;
  line-height: 19.6px;
  color: #666;
  margin-top: 22.5px;
  margin-bottom: 6px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  & svg {
    margin-left: 10px;
  }
`;

export const CommentMessage = styled.p`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 19px;
  color: #000000;
  margin: 0;
`;

export const CommentTools = styled.div`
  display: flex;
  align-items: center;
  margin-top: 6px;
`;

export const CommentLikes = styled.div`
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 13px;
  color: #000000;

  svg {
    fill: #333333;
  }

  span {
    margin-left: 4px;
  }
`;

export const CommentReplies = styled.span`
  font-family: 'SF Pro Display', sans-serif;
  font-size: 12px;
  line-height: 14px;
  color: #999;
  margin-left: 9px;
`;

export const CommentReply = styled.span`
  font-family: 'SF Pro Display', sans-serif;
  margin-left: 17px;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 14px;
  color: #666;
`;

export const Report = styled.span`
  display: inline-block;
  margin-left: 18px;
  color: #999;
  transition: color 0.3s ease-in;

  &:hover {
    color: #ed484f;
  }
`;
