import React, {useState} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  Comment,
  CommentAvatar,
  CommentContent,
  CommentLikes,
  CommentMessage,
  CommentName,
  CommentReply,
  CommentReplies,
  ShowAll,
  CommentTools,
  Report
} from './styled';
import avatar from '../../images/avatar.png';
import Icon from '../Icon';
import ArrowRight from '../../assets/ArrowRight';
import {ReactComponent as ReportIcon} from 'images/svg/report.svg';
import useGlobal from 'store';

const CommentMobile = ({name, time, message, replies, likes, showAllCommentsBtn, onClickShowAll}) => {
  const [reply, setReply] = useState(null);
  const [, globalActions] = useGlobal();
  const [isCommentLiked, setIsCommentLiked] = useState(false);

  function onReport() {
    globalActions.setReportPopup({active: true});
  }

  return (
    <Comment>
      <CommentAvatar src={avatar} />
      <CommentContent>
        <CommentName>
          {name}
          <span>{time}</span>
        </CommentName>
        <CommentMessage>{message}</CommentMessage>

        <CommentTools>
          <CommentLikes onClick={() => setIsCommentLiked(!isCommentLiked)}>
            <Icon type={isCommentLiked ? 'liked' : 'like'} svgStyle={{width: 13, height: 13, color: '#666'}} />
            <span>{isCommentLiked ? likes + 1 : likes}</span>
          </CommentLikes>
          <CommentReplies>{replies} Replies</CommentReplies>
          <CommentReply onClick={() => setReply('Leonardo Dicaprio')}>Reply</CommentReply>
          <Report onClick={onReport}>
            <ReportIcon />
          </Report>
        </CommentTools>
      </CommentContent>
      {showAllCommentsBtn && (
        <ShowAll onClick={onClickShowAll}>
          Show all comments
          <ArrowRight />
        </ShowAll>
      )}
    </Comment>
  );
};

CommentMobile.defaultProps = {
  name: PropTypes.string,
  time: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.instanceOf(moment), PropTypes.string]),
  message: PropTypes.string,
  replies: PropTypes.number,
  likes: PropTypes.number,
  showAllCommentsBtn: PropTypes.bool,
  onClickShowAll: PropTypes.func
};

export default CommentMobile;
