import React from 'react';
import Star from 'assets/ProductPage/Star';
import {Wrap} from './styled';

const StarRating = ({stars = 4, generalStars = 5, color = '#FFC131', starWidth, starHeight}) => {
  return (
    <Wrap className="star-rating">
      {[...Array(generalStars)].map((_, index) => (
        <Star
          width={starWidth}
          height={starHeight}
          key={index}
          color={(stars === 0 ? 5 : stars) > index ? color : '#efefef'}
        />
      ))}
    </Wrap>
  );
};

export default StarRating;
