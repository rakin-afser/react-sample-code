import styled from 'styled-components';

export const Wrap = styled.div`
  align-items: center;
  display: flex;

  & svg {
    &:not(:first-child) {
      margin-left: 5px;
    }
  }
`;
