import styled from 'styled-components/macro';
import {Select} from 'antd';
import {Link} from 'react-router-dom';
import Icon from 'components/Icon';
import Btn from 'components/Btn';
import {primaryColor} from 'constants/colors';

export const Card = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  border-bottom: ${({last}) => (last ? 'none' : '1px solid #E4E4E4')};
  padding: 16px 0;
  margin: 0 24px;
`;

export const Preview = styled.img`
  display: block;
  width: 130px;
  height: 130px;
  object-fit: cover;
  margin: 0 16px 0 0;
`;

export const Price = styled.span`
  font-family: Helvetica Neue, sans-serif;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #000000;
`;

export const OldPrice = styled(Price)`
  font-size: 12px;
  color: #a7a7a7;
  margin: 0 12px 0 4px;
  text-decoration: line-through;
`;

export const Sale = styled.span`
  font-family: Helvetica Neue, sans-serif;
  font-weight: 700;
  font-size: 14px;
  line-height: 140%;
  color: #ed494f;
`;

export const Name = styled(Link)`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  max-width: ${({saveForLaterCard}) => (saveForLaterCard ? '300px' : '420px')};
  margin: 12px 0 16px 0;
  color: inherit;
`;

export const StyledSelect = styled(Select)`
  &&& {
    margin: 0;

    & .ant-select-selection--single {
      width: 100px;
      height: 24px;
      border: none;
      border-left: ${({borderNone}) => borderNone || '1px solid #e4e4e4'};
      border-radius: 0;
      display: flex;
      align-items: center;
    }

    & .ant-select-arrow-icon {
      margin-top: 3px;
    }

    & .ant-select-selection__rendered {
      font-family: Helvetica Neue, sans-serif;
      font-size: 14px;
      line-height: 140%;
      color: #000;
    }
  }
`;

export const Size = styled(StyledSelect)``;

export const Color = styled(StyledSelect)`
  margin-right: 12px;
`;

export const AttributeColor = styled.div`
  background-color: ${({color}) => (color ? color : 'transparent')};
  width: 35px;
  height: 24px;
  border-radius: 8px;
  border: 1px solid #e4e4e4;
  margin-left: 10px;
  margin-right: 10px;
  position: relative;

  &::before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: -10px;
    width: 1px;
    background-color: #e4e4e4;
  }
`;

export const Attribute = styled.div`
  border-left: 1px solid #e4e4e4;
  padding-left: 10px;
  padding-right: 10px;
  height: 24px;
  line-height: 24px;
`;

export const Count = styled(StyledSelect)``;

export const MidCoins = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: SF Pro Display, sans-serif;
  font-size: 12px;
  line-height: 132%;
  color: #398287;
  margin: 0 16px 0 0;

  & svg {
    margin: 0 0 4px 4px;
  }
`;

export const ShopCoins = styled(MidCoins)``;

export const Actions = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Remove = styled(Icon)`
  justify-content: flex-end;
  cursor: pointer;
  transition: ease 0.4s;
  margin-bottom: auto;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const BtnAddToLater = styled.button`
  margin-top: auto;
  transition: ease 0.4s;
  display: flex;
  align-items: center;
  justify-content: center;

  i {
    margin-right: 0;

    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    i {
      svg {
        path {
          fill: ${primaryColor};
        }
      }
    }
  }
`;

export const BtnAddToCart = styled(Btn)`
  padding: 0 14px;
  font-size: 14px;
  height: 24px;
  margin-top: auto;
  margin-right: 23px;
`;
