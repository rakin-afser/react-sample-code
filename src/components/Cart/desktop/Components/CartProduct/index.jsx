import React from 'react';

import Grid from 'components/Grid';
import Icon from 'components/Icon';

import {
  Card,
  Preview,
  Price,
  OldPrice,
  Sale,
  Name,
  Attribute,
  AttributeColor,
  Count,
  MidCoins,
  ShopCoins,
  Actions,
  Remove,
  BtnAddToLater,
  BtnAddToCart
} from './styled';
import {ReactComponent as Down} from 'images/svg/Down.svg';
import {Link} from 'react-router-dom';
import {Select} from 'antd';
import {UpdateItemQuantities, REMOVE_ITEMS_FROM_CART, FROM_CART_TO_SAVE_FOR_LATER} from 'mutations';
import {useMutation} from '@apollo/client';
import {getDiscountPercent} from 'util/heplers';

const {Option} = Select;

function CartProduct({product, isLast, saveForLaterCard, onRemoveFromSaveForLater, onFromSaveForLaterToCart}) {
  const {key, name, midCoins, slug, quantity, total, shopCoins, variation, databaseId, variationId} = product;
  const {image, salePrice, regularPrice} = variation?.node || product || {};
  const {attributes} = variation || {};

  const [updateQuantity] = useMutation(UpdateItemQuantities);
  const [removeItem] = useMutation(REMOVE_ITEMS_FROM_CART);
  const [fromCartToSaveForLater] = useMutation(FROM_CART_TO_SAVE_FOR_LATER);

  function onQuantity(value) {
    updateQuantity({
      variables: {input: {items: [{key, quantity: value}]}},
      // optimisticResponse: {
      //   updateItemQuantities: {
      //     updated: {
      //       key,
      //       quantity: value
      //     }
      //   }
      // },
      update(cache, {data}) {
        cache.modify({
          fields: {
            cart: () => data?.updateItemQuantities?.cart
            // cart: (existing) => {
            //   const {updated} = data?.updateItemQuantities || {};
            //   const nodes = existing?.contents?.nodes?.map((node) =>
            //     node?.key === updated?.key ? {...node, ...updated} : node
            //   );
            //   return {
            //     ...existing,
            //     ...data?.updateItemQuantities?.cart,
            //     contents: {
            //       ...existing?.contents,
            //       nodes,
            //       ...data?.updateItemQuantities?.cart?.contents
            //     }
            //   };
            // }
          }
        });
      }
    });
  }

  function onRemove() {
    removeItem({
      variables: {input: {keys: [key]}},

      update(cache, {data}) {
        cache.modify({
          fields: {
            cart: () => data?.removeItemsFromCart?.cart
          }
        });
      }
    });
  }

  function onFromCartToSaveForLater(productId, variationId) {
    fromCartToSaveForLater({
      variables: {productId, variationId},

      update(cache, {data}) {
        cache.modify({
          fields: {
            // cart: () => data?.sflMoveItemFromCart.cart
            cart() {
              return data;
            }
          }
        });
      }
    });
  }

  return (
    <Card sb last={isLast}>
      <Grid>
        <Link to={`/product/${slug}`}>
          <Preview src={image?.sourceUrl} alt={image?.altText} />
        </Link>
        <Grid column>
          <Grid aife customStyles="align-items: baseline !important;">
            <Price>{total}</Price>
            {getDiscountPercent(regularPrice, salePrice) ? (
              <>
                {/* <Currency marginLeft={8}>BD{` `}</Currency> */}
                <OldPrice>{regularPrice}</OldPrice>
                <Sale>{getDiscountPercent(regularPrice, salePrice)}%</Sale>
              </>
            ) : null}
          </Grid>
          <Name saveForLaterCard={saveForLaterCard} to={`/product/${slug}`}>
            {name}
          </Name>
          <Grid aic margin="0 0 16px 0">
            {attributes?.map(({id, value, label}) =>
              label === 'Color' ? <AttributeColor key={id} color={value} /> : <Attribute key={id}>{value}</Attribute>
            )}
            <Count value={quantity} suffixIcon={<Down />} onChange={onQuantity}>
              {[1, 2, 3, 4, 5].map((v) => (
                <Option key={v} value={v}>
                  {v}
                </Option>
              ))}
            </Count>
          </Grid>
          <Grid aic>
            <MidCoins>
              Mid Coins: +{midCoins}
              <Icon type="coinsProfile" />
            </MidCoins>
            <ShopCoins>
              Shop* Coins: +{shopCoins}
              <Icon type="coinsProfile" />
            </ShopCoins>
          </Grid>
        </Grid>
      </Grid>
      <Actions>
        <Remove
          onClick={saveForLaterCard ? () => onRemoveFromSaveForLater(databaseId, variationId) : onRemove}
          type="close"
          color="#545454"
        />

        {saveForLaterCard ? (
          <BtnAddToCart onClick={() => onFromSaveForLaterToCart(databaseId, variationId)} kind="primary-small">
            Add to cart
          </BtnAddToCart>
        ) : (
          <BtnAddToLater onClick={() => onFromCartToSaveForLater(databaseId, variationId)}>
            <Icon type="bookmark" />
          </BtnAddToLater>
        )}
      </Actions>
    </Card>
  );
}

export default CartProduct;
