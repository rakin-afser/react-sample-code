import styled from 'styled-components';

export const StickyWrapper = styled.div`
  width: 300px;
  margin: 0 0 0 40px;
  position: sticky;
  top: 0;
  margin-bottom: 60px;
`;
export const Container = styled.div`
  border: 1px solid #e4e4e4;
  border-radius: 3px;
  padding: 16px 16px 13px;
  background: #fff;
`;

export const Heading = styled.span`
  display: block;
  font-family: Helvetica Neue, sans-serif;
  font-weight: bold;
  font-style: normal;
  font-size: 18px;
  line-height: 22px;
  color: #000;
  margin: 0 0 28px 0;
`;

export const Title = styled.span`
  font-family: Helvetica Neue, sans-serif;
  font-size: ${({bold}) => (bold ? '16' : '14')}px;
  font-weight: ${({bold}) => (bold ? 'bold' : 'normal')};
  line-height: ${({bold}) => (bold ? '20px' : '140%')};
  color: #000;
`;

export const Total = styled.div`
  margin: 25px 0 0 0;
  padding: 12px 0 0 0;
  border-top: 1px solid #e4e4e4;
`;

export const Count = styled(Title)`
  display: inline-flex;
  align-items: baseline;
  ${(props) => {
    if (props.red) return 'color: #ED494F';
    if (props.green) return 'color: #398287';
    return null;
  }};
  ${({bold, small}) => {
    if (bold && small)
      return `font-size: 12px;
              line-height: 15px;
              font-weight: bold`;
    if (bold)
      return `font-size: 16px;
              line-height: 20px;
              margin-left: 4px;
              font-weight: bold`;
    if (small)
      return `font-size: 10px;
              line-height: 12px;`;

    return `font-size: 14px;
            line-height: 140%;
            margin-left: 4px;`;
  }};
  text-transform: uppercase;
`;

export const CoinsCounter = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: SF Pro Display, sans-serif;
  font-size: 12px;
  color: #398287;

  & svg {
    margin-left: 4px;
  }
`;

export const Confirm = styled.button`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 268px;
  height: 46px;
  text-align: center;
  font-family: Helvetica Neue, sans-serif;
  font-weight: 500;
  font-size: 18px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: #ffffff;
  background: #ed484f;
  border: none;
  border-radius: 24px;
  margin: 12px 0 25px;
  cursor: pointer;

  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }

  svg {
    position: absolute;
    right: 30px;
  }
`;

export const Notice = styled.span`
  display: flex;
  justify-content: center;
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: ${({green}) => (green ? '#26A95E' : '#000')};
  font-weight: ${({bold}) => (bold ? '700' : '400')};
  margin: ${({bold}) => (bold ? '0 0 0 4px' : '0')};
`;

export const Accept = styled.div`
  display: block;
  border-bottom: 1px solid #e4e4e4;
  padding-bottom: 8px;
  font-family: SF Pro Display, sans-serif;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #000;
  margin: 32px 0 0;
`;

export const Message = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  text-align: center;
  color: #000000;
  position: absolute;
  bottom: -54px;
`;
