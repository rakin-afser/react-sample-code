import React, {useRef, useEffect, useState} from 'react';

import Grid from 'components/Grid';
import Icon from 'components/Icon';
import {ReactComponent as MasterCard} from 'images/svg/MasterCard.svg';
import {ReactComponent as AmericanExpress} from 'images/svg/AmericanExpress.svg';
// import {ReactComponent as Paypall} from 'images/svg/PaypallIcon.svg';
import {ReactComponent as Visa} from 'images/svg/VisaIcon.svg';
import {ReactComponent as Right} from 'images/svg/Right.svg';
import {
  StickyWrapper,
  Container,
  Title,
  Count,
  Heading,
  CoinsCounter,
  Total,
  Confirm,
  Notice,
  Accept,
  Message
} from './styled';
import {useHistory} from 'react-router';
import {useUser} from 'hooks/reactiveVars';
import {getCheckoutLink} from 'util/heplers';

function Summary({bahrainiDinar, sellerRef, dataCart}) {
  const [user] = useUser();
  const cartRef = useRef(null);
  const history = useHistory();

  const {appliedShopCoins, appliedMidCoins, appliedCoupons, totalQuantity} = dataCart?.cart || {};
  const hasRedenptions = appliedShopCoins || appliedMidCoins || appliedCoupons;
  const {totalTaxes, shipping, itemsTotal} = dataCart?.cart?.subcarts?.totals || {};

  function onPlaceOrder() {
    history.push(getCheckoutLink(dataCart?.cart?.subcarts?.subcarts, user));
  }

  return (
    <StickyWrapper>
      <Container ref={cartRef}>
        <Heading>Cart Summary</Heading>
        <Grid sb aic margin="0 0 24px 0">
          <Title>Items Total ({totalQuantity})</Title>
          <Count small>
            <Count>{itemsTotal}</Count>
          </Count>
        </Grid>
        <Grid sb aic margin="0 0 24px 0">
          <Title>Estimated Shipping </Title>
          <Count small>
            <Count>{shipping}</Count>
          </Count>
        </Grid>
        {totalTaxes?.map(({id, label, amount}) => (
          <Grid sb aic margin="0 0 24px 0" key={id}>
            <Title>{label}</Title>
            <Count small>
              {/* BD */}
              <Count>{amount}</Count>
            </Count>
          </Grid>
        ))}

        {hasRedenptions ? <Title>Redemptions:</Title> : null}
        {appliedCoupons?.length
          ? appliedCoupons?.map((c) => (
              <Grid key={c?.code} sb aic margin="8px 0 0 0">
                <CoinsCounter>Coupon {c?.code}</CoinsCounter>
                <Count small>
                  <Count>{c?.discountAmount}</Count>
                </Count>
              </Grid>
            ))
          : null}
        {appliedMidCoins ? (
          <Grid sb aic margin="8px 0 0 0">
            <CoinsCounter>
              Mid Coins: - {appliedMidCoins?.qty}
              <Icon type="coinsProfile" />
            </CoinsCounter>
            <Count small>
              <Count>{appliedMidCoins?.discountAmount}</Count>
            </Count>
          </Grid>
        ) : null}
        {appliedShopCoins ? (
          <Grid sb aic margin="8px 0 0 0">
            <CoinsCounter>
              Shop*Coins: - {appliedShopCoins?.qty}
              <Icon type="coinsProfile" />
            </CoinsCounter>
            <Count small>
              <Count>{appliedShopCoins?.discountAmount}</Count>
            </Count>
          </Grid>
        ) : null}

        <Total>
          <Grid sb aic margin="0 0 5px">
            <Title bold>Total To Pay</Title>
            <Count total small bold>
              <Count bold>{dataCart?.cart.total}</Count>
            </Count>
          </Grid>
          {/* <Notice>
            You{' '}
            <Notice green bold>
              {' '}
              SAVE{' '}
            </Notice>{' '}
            <Notice bold>40% </Notice>&#160;on this order
          </Notice> */}
          <Confirm disabled={!+totalQuantity} onClick={onPlaceOrder}>
            Place Order <Right />
          </Confirm>
        </Total>
        <Accept>We Accept</Accept>
        <Grid aic sb>
          <MasterCard style={{marginLeft: 10}} />
          <Visa />
          {/* <Paypall /> */}
          <AmericanExpress />
        </Grid>
        <Message>You can choose Shipping & Discount options during Checkout</Message>
      </Container>
    </StickyWrapper>
  );
}

export default Summary;
