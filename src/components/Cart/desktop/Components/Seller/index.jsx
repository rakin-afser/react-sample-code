import React from 'react';
import {
  Container,
  Header,
  Logo,
  Name,
  Rating,
  Status,
  Counter,
  Buy,
  Footer,
  Total,
  TotalItem,
  Coins,
  AvailableCount,
  AvailableDate
} from './styled';
import Grid from 'components/Grid';
import {setRating} from 'components/helpers';
import CartProduct from '../CartProduct';
import {useMutation} from '@apollo/client';
import {REMOVE_ITEMS_FROM_CART} from 'mutations';
import userPlaceholder from 'images/placeholders/user.jpg';

function Seller({seller = {}, bahrainiDinar, keys}) {
  const [removeItem] = useMutation(REMOVE_ITEMS_FROM_CART);

  const {gravatar, name, rating, totalFollowers} = seller?.store || {};
  const {itemsTotal, shipping, shopCoins, midCoins, total} = seller?.totals || {};
  const sellerKeys = seller?.items?.map((el) => el.key);
  const keysToRemove = keys?.filter((key) => !sellerKeys?.includes(key));
  function onBuyFromSeller() {
    if (keysToRemove?.length) {
      removeItem({
        variables: {input: {keys: keysToRemove}},

        update(cache, {data}) {
          cache.modify({
            fields: {
              cart: () => data?.removeItemsFromCart?.cart
            }
          });
        }
      });
    }
  }

  return (
    <Container>
      <Header>
        <Grid sb aic padding="24px 24px 16px 24px">
          <Grid aic>
            <Logo src={gravatar || userPlaceholder} />
            <Grid column>
              <Name>{name}</Name>
              <Rating>
                <Status>{rating}</Status>
                {setRating(rating)}
                <Counter>({totalFollowers})</Counter>
              </Rating>
            </Grid>
          </Grid>
          <Buy onClick={onBuyFromSeller}>Buy from this Seller</Buy>
        </Grid>
      </Header>
      {seller?.items?.map((item, i) => (
        <CartProduct key={item.key} isLast={i} product={item} bahrainiDinar={bahrainiDinar} />
      ))}
      <Footer>
        <Grid sb>
          <Coins>
            {/* {shopCoins ? (
              <>
                <AvailableCount>{shopCoins} Shop*Coins</AvailableCount>
                <AvailableDate>Available (now-Nov. 20)</AvailableDate>
              </>
            ) : null} */}
          </Coins>
          <Total>
            <TotalItem>Estimated Shipping: {shipping}</TotalItem>
            <TotalItem>Items Total: {itemsTotal}</TotalItem>
            {shopCoins ? <TotalItem green>Shop*Coins: - {shopCoins}</TotalItem> : null}
            {midCoins ? <TotalItem green>Mid Coins: - {midCoins}</TotalItem> : null}
            <TotalItem bold>
              {name} Total: {total}
            </TotalItem>
          </Total>
        </Grid>
      </Footer>
    </Container>
  );
}

export default Seller;
