import styled from 'styled-components';
import {mainFont} from 'constants/fonts';

export const Wrapper = styled.div`
  max-width: 680px;
  width: 100%;
`;

export const Title = styled.h3`
  font-family: ${mainFont};
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  letter-spacing: 0.013em;
  color: #343434;
`;
