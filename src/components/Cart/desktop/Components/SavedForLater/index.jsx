import React from 'react';
import {useMutation} from '@apollo/client';
import CartProduct from 'components/Cart/desktop/Components/CartProduct';
import {FROM_SAVE_FOR_LATER_TO_CART, REMOVE_FROM_SAVE_FOR_LATER} from 'mutations';
import WithSlider from 'components/WithSlider';
import {Title, Wrapper} from './styled';
import ProductCard from 'components/Cards/Product';

const SavedForLater = ({data, bahrainiDinar}) => {
  const [fromSaveForLaterToCart] = useMutation(FROM_SAVE_FOR_LATER_TO_CART);
  const [removeFromSaveForLater] = useMutation(REMOVE_FROM_SAVE_FOR_LATER);

  const onRemoveFromSaveForLater = (productId, variationId) => {
    removeFromSaveForLater({
      variables: {productId, variationId},

      update(cache, {data}) {
        cache.modify({
          fields: {
            // cart: () => data.sflMoveItemToCart.cart
            cart() {
              return data;
            }
          }
        });
      }
    });
  };

  const onFromSaveForLaterToCart = (productId, variationId) => {
    fromSaveForLaterToCart({
      variables: {productId, variationId},

      update(cache, {data}) {
        cache.modify({
          fields: {
            // cart: () => data.sflMoveItemToCart.cart
            cart() {
              return data;
            }
          }
        });
      }
    });
  };

  const products = data?.sflProducts?.nodes;
  const items = data?.sflItems;

  const dataToPass = products.reduce((acc, elem) => {
    const id = items.filter((el) => parseInt(el.productId) === elem.databaseId);
    return acc.concat({...elem, variationId: parseInt(id[0].variationId)});
  }, []);

  if (products?.length < 3) {
    return (
      <Wrapper>
        <Title>Saved for later ({products?.length || 0})</Title>

        {dataToPass &&
          dataToPass?.map((item, i) => (
            <CartProduct
              saveForLaterCard
              onRemoveFromSaveForLater={onRemoveFromSaveForLater}
              onFromSaveForLaterToCart={onFromSaveForLaterToCart}
              key={i}
              isLast={i}
              product={item}
              bahrainiDinar={bahrainiDinar}
            />
          ))}
      </Wrapper>
    );
  }

  const sliderTitleStyles = {
    'font-weight': '500',
    'font-size': '18px',
    'letter-spacing': '0.013em',
    color: '#343434'
  };

  return (
    <Wrapper>
      <WithSlider
        padding="32px 0"
        marginTop={0}
        title={`Saved for later (${products?.length || 0})`}
        titleStyles={sliderTitleStyles}
        slidesToScroll={3}
        infinite={false}
        slidesToShow={3}
        dots
        withHeader
        hideViewAll
      >
        {dataToPass.map((item, i) => (
          <ProductCard
            saveForLaterCard
            onRemoveFromSaveForLater={onRemoveFromSaveForLater}
            onFromSaveForLaterToCart={onFromSaveForLaterToCart}
            hideLens
            hideStoreBlock
            key={i}
            inQuickView
            index={i}
            maxWidth="250px"
            content={item}
          />
        ))}
      </WithSlider>
    </Wrapper>
  );
};

export default SavedForLater;
