import {Link} from 'react-router-dom';
import styled from 'styled-components/macro';

export const Continue = styled.div`
  display: flex;
  align-items: center;
  max-width: 1020px;
  font-size: 14px;
  line-height: 140%;
  color: #000;
  margin: 26px auto 0;
  padding: 0;

  & svg {
    width: 8px;
    height: 12px;
    margin-right: 16px;
    transform: rotate(180deg);
  }
`;

export const ContinueLink = styled(Link)`
  color: inherit;
`;

export const CartWrapper = styled.div`
  max-width: 678px;
  width: 100%;
  margin: 24px auto 36px auto;

  & .slick-list {
    padding: 12px 0;
  }
`;
