import React, {useEffect, useRef} from 'react';

// import {newArrivals} from 'constants/staticData';

import Grid from 'components/Grid';
import ListHeading from 'components/ListHeading';
import Icon from 'components/Icon';
// import CardNewArrival from 'components/CardNewArrival';
// import ProductCard from 'components/Cards/Product';
// import WithSlider from 'components/WithSlider';
import Seller from './Components/Seller';
import Summary from './Components/Summary';
import {Continue, ContinueLink} from './styled';
import {useQuery} from '@apollo/client';
import {CART} from 'queries';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';
import {formatCartsData} from 'util/heplers';
import SavedForLater from 'components/Cart/desktop/Components/SavedForLater';

function Index() {
  const sellerRef = useRef();
  const bahrainiDinar = (value) => Number(value).toFixed(2);

  const {data: dataCart, loading} = useQuery(CART);
  // const [getSimilar, {data: dataSimilar}] = useLazyQuery(PRODUCTS_BY_CATEGORY);

  // useEffect(() => {
  // const exclude = dataCart?.cart?.contents?.nodes?.map((el) => el.product.node.databaseId);
  // const categoryIn = dataCart?.cart?.contents?.nodes?.reduce((acc, item) => {
  //   return [...acc, ...item?.product?.node?.productCategories?.nodes?.map((el) => el.slug)];
  // }, []);

  // if (categoryIn?.length > 0) {
  //   getSimilar({variables: {categoryIn, exclude: exclude}});
  // }
  // }, [dataCart]);

  const keys = dataCart?.cart?.contents?.nodes?.map((el) => el.key);
  const cartsData = formatCartsData(dataCart);

  return (
    <Grid pageContainer padding="16px 0 0 0">
      <ListHeading
        heading="My Cart"
        customStyles="padding-bottom:16px;"
        customStylesHeading="font-size: 28px !important; line-height: 33px !important;"
      />
      <Continue>
        <ContinueLink to="/categories">
          <Icon type="arrow" color="#000" />
          Continue Shopping
        </ContinueLink>
      </Continue>
      <Grid customStyles={{maxWidth: 1020}} margin="24px auto 55px">
        <Grid column width="100%">
          {loading ? (
            <SkeletonTheme style={{marginTop: 30}}>
              <Skeleton style={{marginBottom: 30, marginLeft: 20}} width={600} height={100} />
              <Skeleton style={{marginBottom: 30, marginLeft: 20}} width={600} height={100} />
              <Skeleton style={{marginBottom: 30, marginLeft: 20}} width={600} height={100} />
            </SkeletonTheme>
          ) : (
            <div ref={sellerRef}>
              {cartsData?.map((item) => (
                <Seller key={item?.store?.id} seller={item} bahrainiDinar={bahrainiDinar} keys={keys} />
              ))}
            </div>
          )}
          {/* <CartWrapper>
            <WithSlider
              hideViewAll
              marginTop={31}
              title="Items You May Like"
              slidesToScroll={2}
              infinite={false}
              slidesToShow={2}
              withSeeMore
            >
              {dataSimilar?.products?.edges?.map((arrival, index) => (
                <ProductCard key={index} index={index} maxWidth="250px" content={arrival?.node} />
              ))}
            </WithSlider>
          </CartWrapper> */}
        </Grid>
        <Summary bahrainiDinar={bahrainiDinar} sellerRef={sellerRef} dataCart={dataCart} />
      </Grid>
      {/* {dataCart?.cart?.sflProducts?.nodes?.length ? (
        <Grid customStyles={{maxWidth: 1020}} margin="24px auto 55px">
          <SavedForLater bahrainiDinar={bahrainiDinar} data={dataCart?.cart} />
        </Grid>
      ) : null} */}
    </Grid>
  );
}

export default Index;
