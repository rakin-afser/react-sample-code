import styled from 'styled-components/macro';

export const Wrapper = styled.div``;

export const Total = styled.div`
  padding: 24px 16px 16px;
`;

export const TotalMessage = styled.span`
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: #999999;
  display: block;
  max-width: 290px;
  margin: 0 auto 8px;
`;

export const TotalArea = styled.div`
  border: 1px solid #e4e4e4;
  border-radius: 3px;
  padding: 26px 16px;
`;

export const TotalTitle = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  margin: 0 0 22px;
`;

export const TotalItem = styled.div`
  display: flex;
  align-items: center;
  margin: 0 0 24px;

  font-size: 14px;
  line-height: 140%;
  color: #000000;

  span {
    margin-left: auto;

    small {
      font-size: 10px;
      line-height: 140%;
      padding-right: 4px;
    }
  }
`;

export const TotalItemCoins = styled.span`
  padding-left: 4px;
  font-size: 14px;

  svg {
    stroke: #398287;
    margin: 0 0 0 10px;
    position: relative;
    top: 2px;
  }
`;

export const TotalToPay = styled.div`
  border-top: 1px solid #e4e4e4;
  padding-top: 12px;
`;

export const TotalToPayPrice = styled.span`
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  text-align: right;
  color: #000000;
  margin-left: auto;
  display: inline-flex;
  align-items: center;

  small {
    font-size: 80%;
    padding-right: 4px;
    line-height: 1;
  }

  span {
    font-size: 12px;
    color: #7a7a7a;
    padding-left: 8px;
  }
`;

export const TotalToPayText = styled.div`
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #000;
  text-align: center;

  span {
    color: #26a95e;
    font-weight: 700;
    text-transform: uppercase;
  }

  strong {
    font-weight: 700;
    color: #000;
  }
`;

export const TotalToPayButton = styled.button`
  height: 46px;
  background: #ed484f;
  border-radius: 33px;
  font-weight: 500;
  font-size: 18px;
  line-height: 132%;
  width: 100%;
  border: 0;
  outline: none;
  margin-top: 16px;

  display: flex;
  align-items: center;
  text-align: center;
  justify-content: center;
  letter-spacing: -0.024em;
  color: #ffffff;

  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

export const TotalToPayButtonSticky = styled(TotalToPayButton)`
  height: 42px;
`;

export const TotalAccept = styled.div`
  margin-top: 24px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;

  span {
    width: 100%;
    font-weight: 500;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    padding-bottom: 16px;
  }

  img {
    margin: 0 18px 0 0;
  }
`;

export const TotalAcceptShipping = styled.div`
  width: 100%;
  border-top: 1px solid #e4e4e4;
  padding-top: 16px;
  margin-top: 22px;
  font-style: normal;
  font-weight: 300;
  font-size: 14px;
  line-height: 17px;
  color: #999999;
`;

export const TotalToPaySticky = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 16px;
  background: #fff;
  z-index: 100;
  box-shadow: inset 0px 1px 6px rgba(90, 90, 90, 0.13);
  transition: all 0.3s ease;
  transform: translateY(${({show}) => (show ? '0' : '100%')});
`;

export const TotalToPayPriceSticky = styled.div`
  margin-left: auto;
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  text-align: right;
  color: #666666;
  display: inline-flex;
  align-items: center;

  small {
    font-size: 12px;
    line-height: 1;
    padding-right: 4px;
    padding-top: 2px;
  }
`;

export const TotalToPayOldPriceSticky = styled.div`
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  text-align: right;
  color: #7a7a7a;
  display: inline-flex;
  align-items: center;
  padding-left: 6px;

  small {
    font-size: 10px;
    line-height: 1;
    padding-top: 1px;
  }
`;

export const TotalToPayTitleSticky = styled.div`
  width: 100%;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 20px;
  color: #666666;
  padding: 0 16px;
  display: flex;
  align-items: center;
`;
