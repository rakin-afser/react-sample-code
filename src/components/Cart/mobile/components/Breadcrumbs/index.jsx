import React from 'react';
import {string} from 'prop-types';
import Icon from 'components/Icon';
import {Wrapper, Title, BackLink} from './styled';

const Breadcrumbs = ({title, backURL}) => {
  return (
    <Wrapper>
      <BackLink to={backURL}>
        <Icon type="arrowBack" color="#000" width={29} height={29} />
      </BackLink>
      <Title>{title}</Title>
    </Wrapper>
  );
};

Breadcrumbs.defaultProps = {
  title: 'My Cart',
  backURL: '/'
};

Breadcrumbs.propTypes = {
  title: string,
  backURL: string
};

export default Breadcrumbs;
