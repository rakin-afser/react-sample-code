import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const Wrapper = styled.div`
  min-height: 58px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.span`
  display: block;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  text-align: center;
  color: #000000;
`;

export const BackLink = styled(Link)`
  position: absolute;
  top: 50%;
  left: 10px;
  transform: translateY(-50%);
  max-height: 24px;
`;
