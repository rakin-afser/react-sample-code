import styled from 'styled-components';

export const Wrapper = styled.div`
	
`;

export const Popup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100vw;
  z-index: 10000;
  opacity: ${({ show }) => show ? 1 : 0};
  pointer-events: ${({ show }) => show ? 'all' : 'none'};
  transition: all .3s ease;
  display: flex;
  flex-wrap: wrap;
  
`;

export const PopupOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0,0,0,.5);
  opacity: ${({ show }) => show ? 1 : 0};
  pointer-events: ${({ show }) => show ? 'all' : 'none'};
  transition: all .3s ease;
`;

export const PopupContent = styled.div`
  margin: auto 10px 10px 10px;
  background: #FFFFFF;
  border-radius: 12px;
  padding: 0;
  width: calc(100% - 20px);
  position: relative;
  z-index: 2;
  transition: all .3s ease;
  position: absolute;
  left: 0;
  right: 0;
  bottom: ${({ show }) => show ? '0' : '-100%'};
`;

export const Item = styled.div`
  display: flex;
  align-items: center;
  min-height: 24px;
  padding: 20px 32px;
  cursor: pointer;
  
  &:first-child {
    padding: 32px 32px 20px;
  }
  
  &:last-child {
    padding: 20px 32px 32px;
  }
  
  &:not(:last-child) {
    border-bottom: 1px solid rgba(239, 239, 239, .5);
  }
`;

export const ItemLabel = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  display: flex;
  align-items: center;
  
  svg {
    stroke: #666666;
    margin-left: 12px;
  }
`;

export const ItemRight = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const AddedToWishlist = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #666666;
  display: flex;
  align-items: center;
  
  span {
    opacity: 0.8;
  }
`;

export const AddedToWishlistIcon = styled.div`
  width: 16px;
  height: 16px;
  border: 1px solid #2ECC71;
  border-radius: 50%;
  margin-right: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  
  svg {
    fill: #2ECC71;
    max-width: 7px;
    max-height: 6px;
  }
`;

export const IconBookmark = styled.span`
  display: flex;
  margin: -6px 0;
  
  svg {
    fill: #666666;
  }
`;

export const IconClose = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: -6px 0;
  
  svg {
    width: 26px;
    height: 26px;
    margin-right: -5px;
  }
`;