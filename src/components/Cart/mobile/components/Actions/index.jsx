import React, {useEffect, useState} from 'react';
import {bool, func} from 'prop-types';
import Div100vh from 'react-div-100vh';

import useGlobal from 'store';

import Icon from 'components/Icon';

import {
  Popup,
  PopupOverlay,
  PopupContent,
  Item,
  ItemLabel,
  ItemRight,
  AddedToWishlist,
  AddedToWishlistIcon,
  IconBookmark,
  IconClose
} from './styled';

const Actions = ({show, setShowActions, removed, setRemoved, dataRemove, loadingRemove, removeItem}) => {
  const [saveLoading, setSaveLoading] = useState(false);
  const [saved, setSaved] = useState(false);

  const addToSave = () => {
    setSaveLoading(true);

    setTimeout(() => {
      setSaveLoading(false);
      setSaved(true);

      setTimeout(() => {
        setShowActions(false);
      }, 2000);
    }, 500);
  };

  const renderSaved = () => {
    if (saveLoading) {
      return <Icon type="loader" svgStyle={{width: 24, height: 5, fill: '#ED484F'}} />;
    }

    if (saved) {
      return (
        <AddedToWishlist>
          <AddedToWishlistIcon>
            <Icon type="checkbox" />
          </AddedToWishlistIcon>
          <span>Item Saved</span>
        </AddedToWishlist>
      );
    }

    return (
      <IconBookmark>
        <Icon type="bookmarkIcon" />
      </IconBookmark>
    );
  };

  const renderRemoved = () => {
    if (loadingRemove) {
      return <Icon type="loader" svgStyle={{width: 24, height: 5, fill: '#ED484F'}} />;
    }

    if (dataRemove) {
      return (
        <AddedToWishlist>
          <AddedToWishlistIcon>
            <Icon type="checkbox" />
          </AddedToWishlistIcon>
          <span>Item Removed</span>
        </AddedToWishlist>
      );
    }

    return (
      <IconClose>
        <Icon type="close" svgStyle={{color: '#ED484F'}} />
      </IconClose>
    );
  };

  return (
    <Popup show={show}>
      <Div100vh
        style={{
          height: '100vh',
          width: '100%',
          maxHeight: 'calc(100rvh)',
          display: 'flex'
        }}
      >
        <PopupOverlay
          show={show}
          onClick={() => {
            setShowActions(false);
          }}
        />
        <PopupContent show={show}>
          <Item onClick={() => !saved && addToSave()}>
            <ItemLabel>
              <span>Save for Later</span>
            </ItemLabel>
            <ItemRight>{renderSaved()}</ItemRight>
          </Item>

          <Item onClick={!dataRemove ? () => removeItem() : null}>
            <ItemLabel>
              <span>Remove Item</span>
            </ItemLabel>
            <ItemRight>{renderRemoved()}</ItemRight>
          </Item>
        </PopupContent>
      </Div100vh>
    </Popup>
  );
};

Actions.defaultProps = {
  show: false,
  removed: false,
  setShowActions: () => {},
  setRemoved: () => {}
};

Actions.propTypes = {
  show: bool,
  removed: bool,
  setShowActions: func,
  setRemoved: func
};

export default Actions;
