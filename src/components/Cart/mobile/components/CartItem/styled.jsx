import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 16px;
`;

export const Seller = styled.div`
  display: flex;
  align-items: flex-start;
  padding-bottom: 16px;
  margin-bottom: 16px;
  border-bottom: 1px solid #E4E4E4;
`;

export const SellerThumb = styled.img`
  width: 44px;
  height: 44px;
  border: 1px solid #EFEFEF;
  border-radius: 50%;
  object-fit: cover;
`;

export const SellerInfo = styled.div`
  padding-left: 12px;
  display: flex;
  flex-wrap: wrap;
`;

export const SellerTitle = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  width: 100%;
`;

export const SellerRating = styled.div`
  display: flex;
  align-items: center;
  font-size: 12px;
  line-height: 132%;
  color: #7A7A7A;
  margin-top: 6px;
  
  strong {
    margin-right: 4px;
  }
  
  span {
    margin-left: 6px;
    letter-spacing: -0.4px;
  }
  
  i {
    margin: 0 1px;
  }
`;

export const SellerButton = styled.button`
  margin-left: auto;
  min-width: 106px;
  min-height: 28px;
  border: 1px solid #C3C3C3;
  box-sizing: border-box;
  border-radius: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  background: #fff;
  outline: none;
  cursor: pointer;
  padding: 0;
  
  font-weight: normal;
  font-size: 10px;
  line-height: 12px;
  color: #545454;
  letter-spacing: -0.2px;
  white-space: nowrap;
`;

export const Summary = styled.div`
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-align: right;
  color: #000000;
  letter-spacing: -0.6px;
  
  > span {
    width: 100%;
    display: block;
    
    &:not(:last-child) {
      margin-bottom: 8px;
    }
  }
`;

export const Price = styled.span`
  padding-left: 8px;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-align: right;
  color: #000000;
  letter-spacing: -0.5px;
  
  small {
    font-size: 12px;
    line-height: 120%;
    padding-right: 4px; 
  }
`;