import React from 'react';

import sellerThumb from 'images/sellerThumb.png';
import productSrc1 from 'images/products/card1.jpg';
import productSrc2 from 'images/products/card2.jpg';

import Icon from 'components/Icon';
import Divider from 'components/Divider';

import Product from '../Product';

import {
  Wrapper,
  Seller,
  SellerInfo,
  SellerThumb,
  SellerRating,
  SellerTitle,
  SellerButton,
  Summary,
  Price
} from './styled';
import {useMutation} from '@apollo/client';
import {REMOVE_ITEMS_FROM_CART} from 'mutations';

const CartItem = ({keys, store, items, totals}) => {
  const {name, gravatar, rating, totalFollowers} = store;
  const {itemsTotal, shipping, total, shopCoins} = totals;
  const sellerKeys = items?.map((el) => el.key);
  const keysToRemove = keys?.filter((key) => !sellerKeys?.includes(key));

  const [removeItems] = useMutation(REMOVE_ITEMS_FROM_CART, {
    variables: {input: {keys: keysToRemove}},
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart: () => data?.removeItemsFromCart?.cart
        }
      });
    }
  });

  return (
    <>
      <Wrapper>
        <Seller>
          <SellerThumb src={gravatar} alt={name} />
          <SellerInfo>
            <SellerTitle>{name}</SellerTitle>
            <SellerRating>
              <strong>{rating}</strong>
              <Icon type="star" svgStyle={{width: 14, height: 14, fill: '#FFC131'}} />
              <Icon type="star" svgStyle={{width: 14, height: 14, fill: '#FFC131'}} />
              <Icon type="star" svgStyle={{width: 14, height: 14, fill: '#FFC131'}} />
              <Icon type="star" svgStyle={{width: 14, height: 14, fill: '#FFC131'}} />
              <Icon type="star" svgStyle={{width: 14, height: 14, fill: '#FFC131'}} />
              {totalFollowers ? <span>({totalFollowers})</span> : null}
            </SellerRating>
          </SellerInfo>
          <SellerButton onClick={keysToRemove?.length ? removeItems : null}>Buy from this Seller</SellerButton>
        </Seller>
        {items.map((item) => (
          <Product key={item?.id} keyCart={item?.key} {...item} />
        ))}
        {/* <Product thumb={productSrc2} price={243.99} newPrice={132.98} /> */}
        <Summary>
          <span>
            Estimated Shipping:{' '}
            <Price>
              {/* <small>BD</small> */}
              {shipping}
            </Price>
          </span>
          <span>
            Items Total:{' '}
            <Price>
              {/* <small>BD</small> */}
              {itemsTotal}
            </Price>
          </span>
          {shopCoins ? (
            <span style={{color: '#398287'}}>
              Shop*Coins:{' '}
              <Price>
                <span style={{color: '#398287'}}>- {shopCoins}</span>
              </Price>
            </span>
          ) : null}
          <span>
            <strong>
              {name} Total:{' '}
              <Price>
                {/* <small><strong>BD</strong></small> */}
                <strong>{total}</strong>
              </Price>
            </strong>
          </span>
        </Summary>
      </Wrapper>
      <Divider style={{height: 8}} />
    </>
  );
};

CartItem.defaultProps = {};

CartItem.propTypes = {};

export default CartItem;
