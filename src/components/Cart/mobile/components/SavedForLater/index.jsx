import React from 'react';
import {CartItem} from 'components/Cart/mobile/components/index';

const SavedForLater = ({data}) => {
  return (
    <div>
      <h2>Saved for later (3)</h2>
      {data.map((item) => (
        <CartItem key={item.id} {...item} keys={[]} />
      ))}
    </div>
  );
};

export default SavedForLater;
