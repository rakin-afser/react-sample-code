import React, {Fragment, useState, useRef, useEffect} from 'react';
import {string, number} from 'prop-types';

import {Actions} from 'components/Cart/mobile/components';

import {
  Wrapper,
  ProductThumb,
  ProductInfo,
  ProductPrice,
  NewPrice,
  OldPrice,
  Sale,
  ProductTitle,
  ProductAttributes,
  ProductDivider,
  ProductCounters,
  Counter,
  CounterField,
  CounterButton,
  Dots,
  AttributeColor
} from './styled';
import {Link} from 'react-router-dom';
import {useMutation} from '@apollo/client';
import {REMOVE_ITEMS_FROM_CART, UpdateItemQuantities} from 'mutations';
import {getDiscountPercent, renderCoinsByVariation} from 'util/heplers';

const Product = (props) => {
  const {name, quantity, midCoins, shopCoins, product, keyCart, variation, variationId} = props;
  const {image, salePrice, regularPrice} = variation?.node || props || {};
  const {sourceUrl, altText} = image;
  const {slug} = product?.node || {};
  const {attributes} = variation || {};
  const variationToPass = {
    databaseId: variationId
  };

  const el = useRef(null);

  const [showActions, setShowActions] = useState(false);
  const [removed, setRemoved] = useState(false);

  const [updateQuantity] = useMutation(UpdateItemQuantities);

  const [removeItem, {data: dataRemove, loading: loadingRemove}] = useMutation(REMOVE_ITEMS_FROM_CART, {
    variables: {input: {keys: [keyCart]}},
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart() {
            return data?.removeItemsFromCart?.cart;
          }
        }
      });
    }
  });

  function onQuantity(value) {
    updateQuantity({
      variables: {input: {items: [{key: keyCart, quantity: value}]}},
      update(cache, {data}) {
        cache.modify({
          fields: {
            cart: () => data?.updateItemQuantities?.cart
          }
        });
      }
    });
  }

  useEffect(() => {
    if (showActions) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [showActions]);

  useEffect(() => {
    if (removed) {
      setShowActions(false);
    }
  }, [removed, el]);

  return (
    <>
      <Wrapper ref={el}>
        <Link to={`/product/${slug}`}>
          <ProductThumb src={sourceUrl} alt={altText} />
        </Link>
        <ProductInfo>
          <Dots
            onClick={() => {
              setShowActions(true);
            }}
          >
            <span />
            <span />
            <span />
          </Dots>
          <ProductPrice>
            <NewPrice>
              {/* <small>BD</small> */}
              {salePrice || regularPrice}
            </NewPrice>
            {salePrice ? <OldPrice>{regularPrice}</OldPrice> : null}
            {salePrice ? <Sale>-{getDiscountPercent(regularPrice, salePrice)}%</Sale> : null}
          </ProductPrice>

          <ProductTitle>
            <Link to={`/product/${slug}`}>{name}</Link>
          </ProductTitle>

          <ProductAttributes>
            {attributes?.map(({id, value, label}) => (
              <Fragment key={id}>
                {label === 'Color' ? <AttributeColor color={value} /> : <span>{value}</span>}
                <ProductDivider />
              </Fragment>
            ))}
            <Counter>
              <CounterButton onClick={() => onQuantity(quantity - 1)}>–</CounterButton>
              <CounterField value={quantity} type="tel" />
              <CounterButton onClick={() => onQuantity(quantity + 1)}>+</CounterButton>
            </Counter>
          </ProductAttributes>
        </ProductInfo>

        <ProductCounters>
          Mid Coins: <span>+{midCoins}</span>
          Shop Coins: <span>+{shopCoins}</span>
        </ProductCounters>
      </Wrapper>
      <Actions
        dataRemove={dataRemove}
        loadingRemove={loadingRemove}
        show={showActions}
        setShowActions={setShowActions}
        removed={removed}
        removeItem={removeItem}
      />
    </>
  );
};

Product.defaultProps = {
  thumb: null,
  price: null,
  newPrice: null
};

Product.propTypes = {
  thumb: string,
  price: number,
  newPrice: number
};

export default Product;
