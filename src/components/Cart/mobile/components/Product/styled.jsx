import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-bottom: 16px;
  margin-bottom: 16px;
  border-bottom: 1px solid #e4e4e4;
  display: flex;
  flex-wrap: wrap;
  transition: all 0.3s ease;
`;

export const ProductThumb = styled.img`
  width: 95px;
  height: 98px;
  border: 1px solid #efefef;
  border-radius: 2px;
  object-fit: cover;
`;

export const ProductInfo = styled.div`
  width: calc(100% - 95px);
  padding-left: 12px;
  position: relative;
`;

export const ProductPrice = styled.div`
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  text-transform: uppercase;
  color: #999999;
  padding-top: 5px;
`;

export const NewPrice = styled.span`
  font-weight: 500;
  font-size: 14px;
  color: #ed484f;
  letter-spacing: -1.1px;

  small {
    font-weight: 500;
    font-size: 12px;
    line-height: 15px;
    color: #999999;
    letter-spacing: 0.7px;
  }
`;

export const OldPrice = styled.span`
  text-decoration-line: line-through;
  margin-left: 10px;
  letter-spacing: -0.6px;
`;

export const Sale = styled.span`
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 140%;
  color: #ed494f;
  margin-left: 16px;
`;

export const ProductTitle = styled.div`
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #000000;
  margin: 15px 0 0;
  letter-spacing: -0.1px;

  a {
    color: inherit;
    display: block;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
  }
`;

export const ProductAttributes = styled.div`
  display: flex;
  align-items: center;
  font-size: 12px;
  line-height: 14px;
  color: #666666;
  margin: 16px 0 0;
  letter-spacing: -0.5px;

  span {
    max-width: 55px;
    border: 0;
    padding: 0;
    border-radius: 0;
    appearance: none;
    outline: none;
    cursor: pointer;

    white-space: nowrap;
    text-overflow: ellipsis;
    position: relative;
    overflow: hidden;
  }
`;

export const AttributeColor = styled.div`
  background-color: ${({color}) => (color ? color : 'transparent')};
  border: 1px solid #e4e4e4;
  border-radius: 5px;
  height: 20px;
  width: 25px;
`;

export const ProductDivider = styled.div`
  height: 19px;
  width: 1px;
  background: #e4e4e4;
  margin: 0 8px;
`;

export const ProductCounters = styled.div`
  width: 100%;
  margin-top: 12px;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #398287;
  letter-spacing: -0.6px;

  span {
    font-size: 14px;
    padding-right: 23px;
  }
`;

export const Counter = styled.div`
  display: inline-flex;
  align-items: center;
`;

export const CounterButton = styled.button`
  width: 30px;
  height: 30px;
  background: #efefef;
  border-radius: 2px;
  color: #000;
  border: 0;
  padding: 0 0 4px;
  line-height: 1;
  outline: none;
  font-size: 22px;
  transition: all 0.3s ease;
`;

export const CounterField = styled.input`
  width: 42px;
  height: 20px;
  border: 0;
  padding: 0;
  text-align: center;
  font-weight: 500;
  color: #666666;
  outline: none;
  font-size: 14px;
`;

export const Dots = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  top: 3px;
  right: -2px;
  width: 24px;
  height: 24px;

  span {
    width: 4px;
    height: 4px;
    background: #7c7e82;
    border-radius: 50%;
    margin: auto 1.5px;
  }
`;
