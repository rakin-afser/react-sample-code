import React, {useState, useRef, useEffect} from 'react';

import {Breadcrumbs, CartItem} from './components';

import Icon from 'components/Icon';
import Divider from 'components/Divider';

import ms from 'images/svg/ms.svg';
import visa from 'images/svg/visa.svg';
import paypall from 'images/svg/paypall.svg';
import cash from 'images/svg/cash.svg';

import {
  Wrapper,
  Total,
  TotalMessage,
  TotalArea,
  TotalTitle,
  TotalItem,
  TotalItemCoins,
  TotalToPay,
  TotalToPayPrice,
  TotalToPayText,
  TotalToPayButton,
  TotalAccept,
  TotalAcceptShipping,
  TotalToPaySticky,
  TotalToPayTitleSticky,
  TotalToPayPriceSticky,
  TotalToPayOldPriceSticky,
  TotalToPayButtonSticky
} from './styled';
import {useHistory} from 'react-router';
import {useQuery} from '@apollo/client';
import {CART} from 'queries';
import {formatCartsData, getCheckoutLink} from 'util/heplers';
import {useUser} from 'hooks/reactiveVars';
import SavedForLater from 'components/Cart/mobile/components/SavedForLater';

const CartPageMobile = ({}) => {
  const [showSticky, setShowSticky] = useState(true);
  const [user] = useUser();
  const total = useRef(null);
  const history = useHistory();
  const {data: dataCart} = useQuery(CART);
  const {shipping, shopCoins, itemsTotal} = dataCart?.cart?.subcarts?.totals || {};

  const keys = dataCart?.cart?.contents?.nodes?.map((el) => el.key);

  const cartsData = formatCartsData(dataCart);

  const {appliedShopCoins, appliedMidCoins, appliedCoupons, totalQuantity} = dataCart?.cart || {};
  const hasRedenptions = appliedShopCoins || appliedMidCoins || appliedCoupons;

  const handleScroll = () => {
    if (total && total.current && window.scrollY + window.innerHeight - total.current.offsetTop >= 0) {
      setShowSticky(false);
    } else {
      setShowSticky(true);
    }
  };

  useEffect(() => {
    if (total && total.current) {
      window.onscroll = handleScroll;
    } else {
      window.onscroll = null;
    }
  }, [total]);

  function onPlaceOrder() {
    history.push(getCheckoutLink(dataCart?.cart?.subcarts?.subcarts, user));
  }

  return (
    <Wrapper>
      <Breadcrumbs />
      <Divider style={{height: 8}} />
      {cartsData?.map((subcart) => (
        <CartItem key={subcart?.store?.id} {...subcart} keys={keys} />
      ))}

      {/*<SavedForLater data={cartsData} />*/}

      <Total>
        <TotalArea>
          <TotalTitle>Cart Summary</TotalTitle>
          <TotalItem>
            Items Total
            <span>
              {/* <small>BD</small> */}
              {itemsTotal}
            </span>
          </TotalItem>
          <TotalItem>
            Estimated Shipping
            <span>
              {/* <small>BD</small> */}
              {shipping}
            </span>
          </TotalItem>

          {dataCart?.cart?.subcarts?.totals?.totalTaxes?.map(({id, label, amount}) => (
            <TotalItem key={id}>
              {label}
              <span>
                {/* <small>BD</small> */}
                {amount}
              </span>
            </TotalItem>
          ))}
          {hasRedenptions ? <TotalItem>Redemptions:</TotalItem> : null}
          {appliedCoupons?.length
            ? appliedCoupons?.map((c) => (
                <TotalItem key={c?.code}>
                  <span
                    style={{
                      marginLeft: 0,
                      fontSize: 12,
                      color: '#398287'
                    }}
                  >
                    Coupon {c?.code}:
                  </span>
                  <span>-{c?.discountAmount}</span>
                </TotalItem>
              ))
            : null}
          {appliedMidCoins ? (
            <TotalItem>
              <span
                style={{
                  marginLeft: 0,
                  fontSize: 12,
                  color: '#398287'
                }}
              >
                Mid Coins:
                <TotalItemCoins>
                  - {appliedMidCoins?.discountAmount}
                  <Icon type="coins" />
                </TotalItemCoins>
              </span>
            </TotalItem>
          ) : null}
          {appliedShopCoins ? (
            <TotalItem>
              <span
                style={{
                  marginLeft: 0,
                  fontSize: 12,
                  color: '#398287'
                }}
              >
                Shop*Coins:
                <TotalItemCoins>
                  - {appliedShopCoins?.discountAmount}
                  <Icon type="coins" />
                </TotalItemCoins>
              </span>
            </TotalItem>
          ) : null}

          <TotalToPay ref={total}>
            <TotalTitle>
              Total To Pay
              <TotalToPayPrice>{dataCart?.cart?.total}</TotalToPayPrice>
            </TotalTitle>

            {/* <TotalToPayText>
              You{' '}
              <span>
                SAVE <strong>40%</strong>
              </span>{' '}
              on this order
            </TotalToPayText> */}

            <TotalToPayButton disabled={!+totalQuantity} onClick={onPlaceOrder}>
              Place Order
            </TotalToPayButton>
          </TotalToPay>

          <TotalAccept>
            <span>We Accept</span>

            <img src={ms} alt="" style={{width: 28}} />
            <img src={visa} alt="" style={{width: 55}} />
            <img src={cash} alt="" style={{width: 30}} />

            <TotalAcceptShipping>Select Shipping & Discount in the next step</TotalAcceptShipping>
          </TotalAccept>
        </TotalArea>
      </Total>

      <TotalToPaySticky show={showSticky}>
        <TotalToPayTitleSticky>
          <span>Total To Pay</span>
          <TotalToPayPriceSticky>
            {/* <small>BD</small> */}
            {dataCart?.cart?.total}
            {/* <TotalToPayOldPriceSticky>
              <small>BD</small>
              756.00
            </TotalToPayOldPriceSticky> */}
          </TotalToPayPriceSticky>
        </TotalToPayTitleSticky>

        <TotalToPayButtonSticky disabled={!+totalQuantity} onClick={onPlaceOrder}>
          Place Order
        </TotalToPayButtonSticky>
      </TotalToPaySticky>
    </Wrapper>
  );
};

CartPageMobile.defaultProps = {};

CartPageMobile.propTypes = {};

export default CartPageMobile;
