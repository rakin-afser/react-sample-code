import styled, {css} from 'styled-components';
import {primaryColor} from 'constants/colors';

export const LoaderWrapper = styled.div`
  width: ${({width}) => width};
  height: ${({height}) => height};
  flex-direction: column;
  display: flex;
  align-items: center;
  justify-content: center;
  ${({order}) =>
    order
      ? css`
          order: ${order};
        `
      : null}
`;

export const Dots = styled.div`
  position: relative;
  margin: 20px;

  & div {
    display: inline-block;
    opacity: 0;
    width: ${({size}) => size};
    height: ${({size}) => size};
    margin-right: ${({size}) => size};
    border-radius: 50%;
    background: ${primaryColor};
    animation: loading 1s infinite;
  }

  & div:nth-child(1) {
    animation-delay: 0.1s;
  }
  & div:nth-child(2) {
    animation-delay: 0.2s;
  }
  & div:nth-child(3) {
    animation-delay: 0.3s;
    margin-right: 0;
  }

  @keyframes loading {
    0% {
      opacity: 0;
    }
    50% {
      opacity: 1;
    }
    100% {
      opacity: 0;
    }
  }
`;
