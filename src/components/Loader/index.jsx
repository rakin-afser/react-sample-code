import React from 'react';

import {LoaderWrapper, Dots} from 'components/Loader/styled';

const Loader = ({wrapperWidth = '100vw', wrapperHeight = '100vh', dotsSize = '10px', isPartialHeight, order}) => (
  <LoaderWrapper order={order} width={wrapperWidth} height={isPartialHeight ? '100%' : wrapperHeight}>
    <Dots size={dotsSize}>
      <div />
      <div />
      <div />
    </Dots>
  </LoaderWrapper>
);

export default Loader;
