import styled from 'styled-components';
import media from '../../constants/media';

export const Wrapper = styled.div.attrs(({styles}) => (
  {style: {...styles}}
  ))`
  width: 100%;
  border-radius: 4px;
  display: flex;
  margin: 16px auto 0;
  background: ${({ type }) => (type === 'success' ? '#E2F9E8' : '#EE989B')};
  
  max-width: 327px;
  padding: 6px 2px 8px 8px;
  margin: 0px auto 0;

  .icon {
    margin: 2px;
    align-self: center;
    margin: 0
  }
  
  animation: fadein 1s;
  -moz-animation: fadein 1s; /* Firefox */
  -webkit-animation: fadein 1s; /* Safari and Chrome */
  -o-animation: fadein 1s; /* Opera */
  
  @keyframes fadein {
    from {
        opacity:0;
    }
    to {
        opacity:1;
    }
  }
  @-moz-keyframes fadein { /* Firefox */
      from {
          opacity:0;
      }
      to {
          opacity:1;
      }
  }
  @-webkit-keyframes fadein { /* Safari and Chrome */
      from {
          opacity:0;
      }
      to {
          opacity:1;
      }
  }
  @-o-keyframes fadein { /* Opera */
      from {
          opacity:0;
      }
      to {
          opacity: 1;
      }
  }
`;

export const IconBox = styled.div`
  width: 43px;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const Title = styled.div`
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: ${({type}) => type === 'error' ? '#fff' : '#000'}
`

export const Message = styled.div`
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #fff;
  padding-right: 20px;
  color: ${({type}) => type === 'error' ? '#fff' : '#000'}
  margin-top: 3px
`

export const Content = styled.div`
  width: calc(100% - 24px);
  color: ${({ type }) => (type === 'success' ? '#333333' : '#FFFFFF')};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 1.4;
  padding-left: 7px;

  span {
    display: block;
    font-weight: 500;
  }  
`;
