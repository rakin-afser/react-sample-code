import React from 'react';
import { oneOfType, object } from 'prop-types';

import { IconMessageCheck, IconMessageError } from 'helpers/icons';

import { Wrapper, IconBox, Content, Title, Message } from './styled';

const FormMessage = ({ message, styles }) => {
  if(message === null) return null;

  const {
    type,
    title,
    text,
    boldText
  } = message;

  return <Wrapper type={type} styles={styles}>
    <IconBox>
      {type === 'success' && <IconMessageCheck fill="#2ECC71"/>}
      {type === 'error' && <IconMessageError fill="#ED484F"/>}
    </IconBox>
    <Content type={type}>
      {title && <Title type={type}>{title}</Title>}
      {text && <Message type={type}>{text}{boldText && <b> {boldText}</b>}</Message>}
    </Content>
  </Wrapper>
};

FormMessage.defaultProps = {
  message: null
};

FormMessage.propTypes = {
  message: oneOfType([object])
};

export default FormMessage;
