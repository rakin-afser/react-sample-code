import React, {useState} from 'react';
import Avatar from 'components/Avatar';
import {useHistory} from 'react-router-dom';
import Icon from 'components/Icon';
import useGlobal from 'store';
import {
  EditButton,
  FollowBlock,
  FollowNumber,
  FollowText,
  Header,
  Name,
  UserInfo,
  Profession,
  Description,
  ButtonWrap,
  SettingsLink
} from './styled';
import {useTranslation} from 'react-i18next';
import Button from 'components/Buttons';
import Btn from 'components/Btn';
import {useFollowUnfollowUserMutation} from 'hooks/mutations';

const AccountHeader = ({user, onSwitchUser, isOwner}) => {
  const history = useHistory();
  const {t} = useTranslation();
  const [, setGlobalState] = useGlobal();
  const [descriptionExpanded, setDescriptionExpanded] = useState(false);
  const {id, databaseId, isFollowed} = user || {};
  const [followUnfollow] = useFollowUnfollowUserMutation({id, isFollowed});

  const onSendMessage = () => {
    setGlobalState.setMessenger({
      data: {
        databaseId: user.databaseId,
        username: user?.email,
        photoUrl: user.profile_picture
      }
    });
    history.push('/profile/messages');
  };

  function renderButtons() {
    if (isOwner) {
      return <EditButton to="/profile/edit">{t('myProfilePage.editProfile')}</EditButton>;
    }

    return (
      <ButtonWrap sb aic>
        <Btn
          kind={isFollowed ? 'following-md' : 'follow-md'}
          onClick={() => followUnfollow({variables: {input: {id: databaseId}}})}
        />
        <Button onClick={onSendMessage} type="sendMessage" />
      </ButtonWrap>
    );
  }

  return (
    <>
      <Header>
        <Avatar isMobile img={user?.avatar?.url} setEdit={() => console.log('photo edit')} />
        <UserInfo>
          <Name onClick={onSwitchUser || null}>
            <span>
              {user?.firstName} {user?.lastName}
            </span>
            {/* disabled at the moment */}
            {/* {isOwner ? <Icon type="chevronDown" /> : null} */}
          </Name>
          {isOwner ? (
            <SettingsLink to="/profile/settings">
              <Icon type="gear" />
            </SettingsLink>
          ) : null}
          <FollowBlock>
            <FollowNumber>{user?.totalFollowers || 0}</FollowNumber>
            <FollowText>{t('myProfilePage.followers')}</FollowText>
            <FollowNumber>{user?.totalFollowing || 0}</FollowNumber>
            <FollowText>{t('myProfilePage.following')}</FollowText>
          </FollowBlock>
          {renderButtons()}
        </UserInfo>
      </Header>
      <Profession>{user?.job_title}</Profession>
      <Description expanded={descriptionExpanded} onClick={() => setDescriptionExpanded(!descriptionExpanded)}>
        {descriptionExpanded
          ? user && user?.description
            ? user?.description
            : ''
          : `${user?.description?.slice(0, 100) || ''}`}
      </Description>
    </>
  );
};

export default AccountHeader;
