import {primaryColor} from 'constants/colors';
import {mainFont, secondaryFont} from 'constants/fonts';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

export const FlexBlock = styled.div`
  padding: 0 16px;
  display: flex;
`;

export const Header = styled(FlexBlock)`
  padding-top: 24px;
  display: flex;
`;

export const UserInfo = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: flex-start;
  flex-direction: column;
  position: relative;
`;

export const Name = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  color: #000;
  line-height: normal;
  display: flex;
  align-items: center;

  & span {
    margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 10px' : '0 10px 0 0')};
  }
`;

export const FollowText = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #666666;
  margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 18px' : '0 18px 0 0')};
`;

export const FollowBlock = styled.div`
  display: flex;
  margin: ${({theme: {isArabic}}) => (isArabic ? '14px 0 8px' : '11px 0 14px')};
`;

export const FollowNumber = styled(FollowText)`
  color: #000000;
  font-weight: 500;
  margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 8px' : '0 8px 0 0')};
`;

export const ButtonWrap = styled.div`
  display: flex;

  button + button {
    margin-left: 16px;
  }
`;

export const EditButton = styled(Link)`
  color: #666;
  font-size: 12px;
  border: 1px solid #666666;
  border-radius: 24px;
  padding: 5px 15px;
  line-height: 1;
`;

export const Description = styled.span`
  flex-shrink: 0;
  margin: 3px 0 16px;
  padding: 0 16px;
  font-family: ${mainFont};
  font-size: 14px;
  color: #000;
  display: block;
  overflow: hidden;
  line-height: 1.2;

  &::after {
    //content: 'see more';
    color: ${primaryColor};
    margin-left: 4px;
  }

  ${({expanded}) =>
    expanded &&
    `
      &::after {
        // content: 'less';
      }
    `}
`;

export const Profession = styled(FlexBlock)`
  margin-top: 11px;
  font-family: ${secondaryFont};
  font-weight: 500;
  font-size: 14px;
  color: #000;
`;

export const SettingsLink = styled(Link)`
  position: absolute;
  top: 0;
  right: 4px;
`;
