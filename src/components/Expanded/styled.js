import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  position: relative;
`;

export const TxtHidden = styled.div`
  max-height: ${({active}) => (active ? '1000px' : '0')};
  transition: max-height 0.8s;
  overflow: hidden;
`;

export const Button = styled.div`
  cursor: pointer;

  &:focus {
    outline: none;
    border: none;
  }
`;
