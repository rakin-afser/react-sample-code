import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Wrapper, TxtHidden, Button} from 'components/Expanded/styled';

const Expanded = ({contentVisible, children, hideBtn, showBtn, opened, stylesForHidden, ...props}) => {
  const [active, setActive] = useState(opened);

  const onStateToggle = () => {
    setActive((prev) => !prev);
  };

  return (
    <Wrapper {...props}>
      {contentVisible}
      <TxtHidden style={stylesForHidden} active={active}>
        {children}
      </TxtHidden>
      {active ? (
        <Button role="button" tabIndex={0} onKeyDown={onStateToggle} onClick={onStateToggle}>
          {hideBtn}
        </Button>
      ) : (
        <Button role="button" tabIndex={0} onKeyDown={onStateToggle} onClick={onStateToggle}>
          {showBtn}
        </Button>
      )}
    </Wrapper>
  );
};

Expanded.defaultProps = {
  contentVisible: null,
  opened: false,
  stylesForHidden: {}
};

Expanded.propTypes = {
  contentVisible: PropTypes.node,
  children: PropTypes.node.isRequired,
  hideBtn: PropTypes.node.isRequired,
  showBtn: PropTypes.node.isRequired,
  opened: PropTypes.bool,
  stylesForHidden: PropTypes.shape()
};

export default Expanded;
