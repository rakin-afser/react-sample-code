import React, {useState} from 'react';
import SearchInputDesktop from './SearchInputDesktop';
import SearchInputMobile from './SearchInputMobile';
import avatar from './img/avatar.jpg';
import avatar2 from './img/avatar2.jpg';
import avatar3 from './img/avatar3.jpg';
import avatar4 from './img/avatar4.jpg';
import {
  ResultCategory,
  CategoryHeading,
  CategoryHeader,
  ViewAll,
  CategoryItem,
  ItemPic,
  ItemName,
  ItemTitle,
  ItemRating,
  ItemFollow,
  ItemView,
  ItemFrom,
  ItemDescription
} from './styled';

const avatars = [avatar, avatar2, avatar3, avatar4];
const items = [1, 2];

function getItem(name) {
  switch (name) {
    case 'posts':
      return (
        <>
          <ItemPic src={avatar4} />
          <ItemDescription>
            <ItemName>Nike Seller</ItemName>
            <ItemTitle>Cream canvas Big Sad Wolf</ItemTitle>
          </ItemDescription>
          <ItemView>View</ItemView>
        </>
      );
    case 'stores':
      return (
        <>
          <ItemPic src={avatar3} />
          <ItemDescription>
            <ItemName>Reebok</ItemName>
            <ItemTitle>112k followers</ItemTitle>
          </ItemDescription>
          <ItemFollow>Follow</ItemFollow>
        </>
      );
    case 'products':
    default:
      return (
        <>
          <ItemPic src={avatar} />
          <ItemDescription>
            <ItemName>Reebok Crossfit</ItemName>
            <ItemRating>x x x x x (335)</ItemRating>
          </ItemDescription>
          <ItemFrom>From BD119</ItemFrom>
        </>
      );
  }
}

function getCategory(categories) {
  return categories.map((item) => (
    <ResultCategory>
      <CategoryHeader>
        <CategoryHeading>{item.text}</CategoryHeading>
        <ViewAll>View All</ViewAll>
      </CategoryHeader>
      {items.map(() => (
        <CategoryItem>{getItem(item.page)}</CategoryItem>
      ))}
    </ResultCategory>
  ));
}

const SearchInput = ({isMobile}) => {
  const [isShow, showResult] = useState(false);
  const [isFocus, showRecent] = useState(false);

  document.addEventListener('click', (ev) => {
    const t = ev.target;
    const close = t.classList.contains('close-search-popup');
    if (close) {
      showResult(false);
      showRecent(false);
    }
  });

  const closeHandler = () => {};

  return isMobile ? (
    <SearchInputMobile />
  ) : (
    <SearchInputDesktop
      getCategory={getCategory}
      closeHandler={closeHandler}
      isShow={isShow}
      open={showResult}
      isFocus={isFocus}
      hint={showRecent}
    />
  );
};

export default SearchInput;
