import React from 'react';
import PropTypes from 'prop-types';
import {Search, SearchContainer, SearchButtonMobile} from './styled';
import SearchIcon2 from 'assets/SearchIcon2';

const SearchInputMobile = ({onChange, value}) => {
  return (
    <SearchContainer>
      <Search value={value} placeholder="Search items" onChange={onChange} isMobile={true} />
      <SearchButtonMobile>
        <SearchIcon2 />
      </SearchButtonMobile>
    </SearchContainer>
  );
};

SearchInputMobile.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string
};

export default SearchInputMobile;
