import React from 'react';
import {
  Wrap, FeedbackInfo, FeedbackAvatar, FeedbackCity, FeedbackDate, FeedbackImage,
  ImgProduct, ImgAvatar
} from './styled.js';

const FeedbackCard = ({feedback}) => {
  return (
    <Wrap>
      <FeedbackInfo>
        <FeedbackAvatar>
          <ImgAvatar src={feedback.feedbackAvatar} alt="avatar"/>
        </FeedbackAvatar>
        <div>
          <FeedbackCity>Gordon Rivera</FeedbackCity>
          <FeedbackDate>July 28</FeedbackDate>
        </div>
      </FeedbackInfo>
      <FeedbackImage>
        <ImgProduct src={feedback.feedbackPic} alt="product"/>
      </FeedbackImage>
    </Wrap>
  )
}

export default FeedbackCard;
