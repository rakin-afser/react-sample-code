import styled from 'styled-components';

export const Wrap = styled.div`
  width: 188px !important;
  border: 1px solid #eeeeee;
  border-radius: 3px;
`;

export const FeedbackInfo = styled.div`
  display: flex;
  align-items: center;
  padding: 16px 16px 13px;
`;

export const FeedbackAvatar = styled.div`
  margin-right: 8px;
`;

export const FeedbackCity = styled.span`
  display: block;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const FeedbackDate = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
`;

export const FeedbackImage = styled.div`
  width: 100%;
`;

export const ImgAvatar = styled.img`
  width: 44px;
  border: 0.5px solid #cccccc;
  border-radius: 25px;
`;

export const ImgProduct = styled.img`
  width: 100%;
`;
