import styled, {css} from 'styled-components';
import {primaryColor} from 'constants/colors';

export const Wrap = styled.div`
  background: #ffffff;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin-left: 32px;
  margin-bottom: 19px;
  padding: 11px 0 0 0;
  display: flex;
`;

export const Tab = styled.div`
  display: block;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: #7a7a7a;
  padding: 9px 0;
  margin: 0 60px 0 24px;
  border-bottom: 2px solid #fff;
  line-height: normal;
  transition: all 0.4s;
  cursor: pointer;

  &:hover {
    color: #000;
  }

  ${({isActive}) =>
    isActive &&
    css`
      font-weight: 500;
      color: #000000;
      border-bottom: 2px solid ${primaryColor};
    `}
`;
