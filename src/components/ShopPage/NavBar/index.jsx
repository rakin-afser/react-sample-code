import React from 'react';
import {Wrap, Tab} from './styled.js';

const NavBar = ({tabs = [], currentTab, onChange = () => {}}) => {
  return (
    <Wrap>
      {tabs?.map((item) => (
        <Tab onClick={() => onChange(item?.slug)} isActive={currentTab === item?.slug}>
          {item?.name}
        </Tab>
      ))}
    </Wrap>
  );
};

export default NavBar;
