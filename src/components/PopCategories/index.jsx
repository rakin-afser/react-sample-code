import React from 'react'
import {
  CatItem,
  CatList,
  Header,
  Container,
  Title,
  Text,
} from './styled'
import Clothing from "images/categories/clothing.jpg";
import Shoes from "images/categories/shoes.jpg";
import Electronics from "images/categories/electronics.jpg";
import Beauty from "images/categories/beauty.jpg";

const categories = [
  {
    title: 'Clothing',
    followers: '21k',
    image: Clothing,
  },
  {
    title: 'Shoes',
    followers: '1M',
    image: Shoes,
  },
  {
    title: 'Electronics',
    followers: '21k',
    image: Electronics,
  },
  {
    title: 'Beauty',
    followers: '1M',
    image: Beauty,
  }
]


const PopCategories = () => {
  return (
    <Container>
      <Header>
        Popular Categories
      </Header>
      <CatList>
        {categories.map((cat) => (
          <CatItem key={cat.title} image={cat.image}>
            <Title>{cat.title}</Title>
            <Text>{cat.followers} followers</Text>
          </CatItem>
        ))}
      </CatList>
    </Container>
  )
}

export default PopCategories
