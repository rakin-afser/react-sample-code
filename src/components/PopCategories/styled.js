import styled from 'styled-components';

export const Header = styled.div`
  font-family: 'Helvetica Neue', sans-serif;
  text-align: center;
  padding: 16px 0;
  width: 100%;
  margin-bottom: 0;
  font-size: 18px;
  font-weight: 500;
`;

export const Title = styled.p`
  font-size: 22px;
  margin-bottom: 9px;
  color: #fff;
`;

export const Text = styled.p`
  font-size: 12px;
  margin-bottom: 0;
  color: #fff;
`;

export const Container = styled.div`
  width: 100%;
  margin-bottom: 15px;
`;

export const CatList = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const CatItem = styled.div`
  min-height: 96px;
  width: calc(50% - 3px);
  flex: 0 0 auto;
  margin: 1.5px;
  padding: 18px 20px;
  border-radius: 6px;
  position: relative;
  overflow: hidden;
  background-image: url(${({image}) => image});
  background-repeat: no-repeat;
  background-size: cover;

  > * {
    position: relative;
    z-index: 1;
  }

  &::before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    // background-image: linear-gradient(0deg, rgba(0, 0, 0, 0.43), rgba(0, 0, 0, 0.43));
    z-index: 0;
  }
`;
