import styled, {css} from 'styled-components/macro';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  primaryColor as primary,
  bookmarkFillColor as gray900,
  secondaryColor as secondary
} from 'constants/colors';

export const Item = styled.div`
  position: relative;
  border-radius: 4px;
  overflow: hidden;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  margin-bottom: 16px;
`;

export const Image = styled.img`
  width: 100%;
  height: ${({$isBigImage}) => ($isBigImage ? '190px' : '168px')};
  object-fit: cover;
  border-radius: 4px 4px 0px 0px;
`;

export const Body = styled.div`
  padding: 8px 8px 12px;
  position: relative;
  background: #fff;
`;

export const Price = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  margin-bottom: 5px;
  max-width: calc(100% - 30px);
  flex-wrap: wrap;
`;

export const CurrentPrice = styled.p`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  color: ${({isDiscount}) => (isDiscount ? primary : gray900)};
  margin-bottom: 0;
  line-height: 1;
  white-space: nowrap;
`;

export const CurrentPriceValue = styled.span`
  font-size: 14px;
  font-weight: 600;
  letter-spacing: -0.8px;
  white-space: nowrap;
`;

export const PriceWithoutDiscount = styled.p`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  color: ${gray900};
  text-decoration: line-through;
  line-height: 1;
  margin: 0 0px 1px 5px;
  white-space: nowrap;
`;

export const Name = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${black};
  max-width: calc(100% - 32px);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Label = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 300;
  font-size: 10px;
  line-height: 140%;
  margin-bottom: 6px;
  color: ${gray900};
`;

export const DiscountLabel = styled.div`
  background-color: ${secondary};
  color: ${white};
  position: absolute;
  top: 3px;
  right: 3px;
  border-radius: 2px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  padding: 6px 2px;
`;

export const ButtonMore = styled.button`
  transform: rotate(90deg);
  position: absolute;
  right: 12px;
  bottom: 8px;
  padding: 0;
`;

export const Likes = styled.div`
  background: transparent;
  position: absolute;
  right: 8px;
  top: -12px;
  text-align: center;
`;

export const ButtonLike = styled.button`
  border-radius: 50%;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.08);
  background: ${white};
  width: 24px;
  height: 24px;
  padding: 0;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const LikesNumber = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  color: ${black};
  margin-top: 3px;
`;

export const PlayBtn = styled.div`
  position: absolute;
  bottom: 8px;
  left: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 22px;
  height: 22px;
  z-index: 1;

  svg {
    width: 38px;
    height: 38px;
    max-width: none !important;
    max-height: none !important;
  }
`;

export const ImageWrapper = styled.div`
  position: relative;
`;
