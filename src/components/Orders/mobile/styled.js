import styled from 'styled-components';
import {ReactComponent as Dots} from 'images/svg/dots.svg';
import {Button} from 'antd';

export const Wrap = styled.div`
  background-color: #fafafa;
`;

export const Order = styled.div`
  background-color: transparent;
`;

export const Seller = styled.div`
  padding: 19px 16px 16px;
  background-color: transparent;
`;

export const Header = styled.div`
  position: relative;
  padding-left: ${({status}) => `${status.startsWith('buyed') ? 88 : 62}px`};
`;
export const Main = styled.div`
  padding-left: ${({status}) => `${status.startsWith('buyed') ? 88 : 0}px`};
  font-size: 12px;
  line-height: 1.25;
  font-weight: 500;
  color: #666;
`;

export const Image = styled.img`
  border-radius: ${({status}) => (status.startsWith('buyed') ? '4px' : '50%')};
  position: absolute;
  top: 0;
  left: 0;
  height: ${({status}) => `${status.startsWith('buyed') ? 65 : 50}px`};
  width: ${({status}) => `${status.startsWith('buyed') ? 65 : 50}px`};
  border: 1px solid #efefef;
`;

export const Title = styled.h5`
  max-width: 220px;
  font-size: 16px;
  line-height: 1.18;
  font-weight: 500;
  padding-bottom: ${({status}) => `${status.startsWith('buyed') ? 6 : 0}px`};
  white-space: nowrap;
  padding-right: ${({status}) => `${status.startsWith('buyed') ? 70 : 20}px`};
`;

export const OptionsBtn = styled(Dots)`
  position: absolute;
  top: 0;
  right: 0;
  z-index: 1;
  width: 24px;
  height: 24px;
`;

export const View = styled.span`
  color: #4a90e2;
  position: absolute;
  top: 0;
  right: 0;
  z-index: 1;
`;

export const Row = styled.p`
  display: flex;
  ${({between}) =>
    between &&
    `
    justify-content: space-between;
  `}
  margin-bottom: 0;
  margin-top: 14px;
`;

export const Bold = styled.span`
  color: #000;
  font-weight: 500;
`;

export const Coins = styled.p`
  display: flex;
  margin-top: 11px;
  margin-bottom: 0;
  color: ${({status}) => `${status === 'buyed' ? '#999999' : '#398287'}`};

  i {
    display: inline-block;
    vertical-align: text-top;
  }

  svg {
    stroke: currentColor;
  }
`;

export const Coin = styled.span`
  font-size: 12px;
  line-height: 1.25;

  span {
    font-weight: 500;
  }

  & + & {
    margin-left: 17px;
  }
`;

export const ProgressId = styled.span`
  color: #999;
  font-size: 12px;
  line-height: 1.16;
  margin-left: 8px;
  margin-top: 1px;
  align-self: center;
`;

const CustomButton = styled(Button)`
  &&& {
    border-color: currentColor;
    border-radius: 100px;
    font-size: 12px;
    height: 28px;
    width: 92px;
  }
`;
