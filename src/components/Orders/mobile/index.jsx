import React from 'react';
import {
  Wrap,
  Order,
  Seller,
  Header,
  Main,
  Image,
  Title,
  OptionsBtn,
  View,
  Bold,
  Row,
  Coins,
  Coin,
  ProgressId
} from './styled';
import OrderStatus from 'components/OrderStatus';
import useGlobal from 'store';
import moment from 'moment';
import ClampLines from 'react-clamp-lines';
import OrderCartItem from 'containers/MyProfile/MyProfileMobile/components/Orders/components/OrderCartItem';
import Error from 'components/Error';
import NoItems from 'components/NoItems/mobile';
import {ReactComponent as NoProducts} from 'images/svg/no-products.svg';
import avatarPlaceholder from 'images/avatarPlaceholder.png';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';

const Orders = ({data, loading, error}) => {
  const [, globalActions] = useGlobal();

  if (loading)
    return (
      <SkeletonTheme style={{marginTop: 30, width: '100%'}}>
        <Skeleton style={{marginBottom: 30, width: '100%'}} height={100} />
        <Skeleton style={{marginBottom: 30, width: '100%'}} height={100} />
        <Skeleton style={{marginBottom: 30, width: '100%'}} height={100} />
        <Skeleton style={{marginBottom: 30, width: '100%'}} height={100} />
      </SkeletonTheme>
    );

  function renderHeader(args) {
    const {status, seller, product, el, databaseId} = args || {};

    return (
      <Row style={{marginTop: 4}}>
        <OrderStatus status={status} />
        {databaseId && <ProgressId>{databaseId}</ProgressId>}
        <View onClick={() => globalActions.setOrderDetails({open: true, data: el})}>View Order</View>
      </Row>
    );
  }

  function renderMain({status, quantity, date, orderId, shippingLines, seller, total}) {
    if (!status.startsWith('CANCELLED')) {
      return (
        <>
          <Row between>
            <span>
              Items: <Bold>{quantity}</Bold>
            </span>
            <span>
              Order Date: <Bold>{moment(date).format('DD/MM/YY')}</Bold>
            </span>
            <span>
              Order ID: <Bold>{orderId}</Bold>
            </span>
          </Row>
          <Row between>
            <span>
              Shipping/Delivery: <Bold>{shippingLines?.nodes[0]?.shippingMethod?.title}</Bold>
            </span>
            <span>
              Total: <Bold>{total}</Bold>
            </span>
          </Row>
        </>
      );
    }
    return (
      <Row between>
        <Bold>{total}</Bold>
        <span>Ordered On {moment(date).format('MMM DD, YYYY')}</span>
      </Row>
    );
  }

  const renderNoItems = () => {
    return (
      <NoItems
        icon={<NoProducts />}
        title="No Orders Found"
        desc="You do not have any orders to display in this view"
      />
    );
  };

  if (error) {
    return <Error />;
  }

  if (!data?.orders?.nodes?.length) {
    return <Wrap style={{background: 'transparent'}}>{renderNoItems()}</Wrap>;
  }

  return (
    <Wrap>
      {data?.orders?.nodes
        .filter((el) => el?.stores?.nodes?.length === 1)
        .map((el) => {
          const {id, databaseId, date, total, status, stores, shippingLines, lineItems} = el;

          return (
            <Order key={id} status={status}>
              {stores?.nodes?.map(({product, gravatar, name, orderId, quantity, seller}) => (
                <Seller key={product?.id}>
                  <Header status={status}>
                    <Image status={status} src={gravatar || avatarPlaceholder} alt={name} />
                    <Title status={status}>
                      <ClampLines text={name} lines={1} buttons={false} />
                    </Title>
                    {renderHeader({status, seller, product, databaseId, el})}
                  </Header>
                  <Main status={status}>
                    {renderMain({
                      status,
                      date,
                      orderId: databaseId,
                      quantity: lineItems?.nodes?.length,
                      shippingLines,
                      seller,
                      total
                    })}
                  </Main>
                </Seller>
              ))}
              {lineItems?.nodes?.map((orderCartItem, index) => (
                <OrderCartItem status={status} key={index} cartItemInfo={orderCartItem} date={date} />
              ))}
            </Order>
          );
        })}
    </Wrap>
  );
};

export default Orders;
