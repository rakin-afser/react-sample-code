import React from 'react';
import PropTypes from 'prop-types';
import ClampLines from 'react-clamp-lines';
import parse from 'html-react-parser';
import {Wrapper, Row, Title, Txt, TxtBlack, Currency, PriceNum, PriceTxt, Image} from './styled';

const ProductInfoShort = ({data}) => {
  return (
    <Wrapper>
      <div>
        <Image src={data.image} alt={data.name} />
      </div>
      <div>
        <Title>
          <ClampLines text={data.name ? data.name : ''} lines={2} buttons={false} />
        </Title>
        <Row alignCenter mb={6}>
          {/*  <Row alignCenter mr={12}>*/}
          {/*    <Txt>Size:</Txt>*/}
          {/*    <TxtBlack>{data.size}</TxtBlack>*/}
          {/*  </Row>*/}
          {/*  <Row alignCenter mr={12}>*/}
          {/*    <Txt>Color:</Txt>*/}
          {/*    <TxtBlack>{data.color}</TxtBlack>*/}
          {/*  </Row>*/}
          {/*  <Row alignCenter mr={12}>*/}
          {/*    <Txt>Quantity:</Txt>*/}
          {/*    <TxtBlack>{data.quantity}</TxtBlack>*/}
          {/*  </Row>*/}
          {/*</Row>*/}
          {/*<Row>*/}
          <PriceTxt>Item price:</PriceTxt>
          <PriceNum>
            {parse(data.currencySymbol.toString())}
            {data.price}
          </PriceNum>
        </Row>
      </div>
    </Wrapper>
  );
};

ProductInfoShort.propTypes = {
  data: PropTypes.shape().isRequired
};

export default ProductInfoShort;
