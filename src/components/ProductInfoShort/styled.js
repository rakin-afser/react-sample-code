import styled from 'styled-components/macro';
import {bookmarkFillColor, mainBlackColor} from 'constants/colors';

export const Image = styled.img`
  margin-right: 12px;
  width: 75px;
  height: 69px;
  border: 1px solid #efefef;
  border-radius: 2px;
  overflow: hidden;
  object-fit: contain;
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Row = styled.div`
  margin-bottom: ${({mb}) => mb && `${mb}px`};
  margin-right: ${({mr}) => mr && `${mr}px`};
  display: flex;
  align-items: ${({alignCenter}) => alignCenter && 'center'};
  justify-content: ${({spaceBetween}) => spaceBetween && 'space-between'};
`;

export const Title = styled.h3`
  margin-bottom: 10px;
  font-size: 14px;
  color: ${mainBlackColor};
`;

export const Txt = styled.div`
  margin-right: 8px;
  font-size: 12px;
  color: ${bookmarkFillColor};
`;

export const TxtBlack = styled.div`
  font-size: 12px;
  color: ${mainBlackColor};
`;

export const PriceTxt = styled.div`
  margin-right: 4px;
  font-size: 14px;
  color: #404040;
`;

export const Currency = styled.div`
  margin-top: 3px;
  margin-right: 4px;
  font-size: 10px;
  font-weight: 500;
  text-transform: uppercase;
  color: ${mainBlackColor};
`;

export const PriceNum = styled.div`
  font-size: 16px;
  font-weight: 500;
  color: ${mainBlackColor};
`;
