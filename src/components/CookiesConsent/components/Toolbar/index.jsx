import React from 'react';
import PropTypes from 'prop-types';
import {useWindowSize} from '@reach/window-size';
import {
  Wrapper,
  Inner,
  InnerContent,
  Left,
  LinkButton,
  Right,
  RightMobileInner,
  Button,
  Button2,
  Button3
} from './styled';

const Toolbar = ({setActive, onAcceptAll, onRejectAll}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return (
    <Wrapper>
      <Inner>
        <InnerContent isMobile={isMobile}>
          <Left>
            {isMobile ? (
              <p>
                We use cookies
                <br /> View our <LinkButton onClick={() => setActive('policy')}>Cookie Policy.</LinkButton>
              </p>
            ) : (
              <p>
                We use cookies to provide website functionality, to analyze traffic on our testSample web-site, personalize
                content, serve target advertisements and to enable social media functionality. Our{' '}
                <LinkButton onClick={() => setActive('policy')}>Cookie Policy</LinkButton> provides more information and
                explains how to update your cookie settings. View our{' '}
                <LinkButton onClick={() => setActive('policy')}>Cookie Policy.</LinkButton>
              </p>
            )}
          </Left>
          <Right>
            {isMobile ? (
              <>
                <RightMobileInner>
                  <Button2 onClick={onRejectAll}>Reject</Button2>
                  <Button3 onClick={onAcceptAll}>Accept</Button3>
                </RightMobileInner>
                <Button onClick={() => setActive('settings')}>Cookie Settings</Button>
              </>
            ) : (
              <>
                <Button onClick={() => setActive('settings')}>Cookie Settings</Button>
                <Button2 onClick={onRejectAll}>Reject All</Button2>
                <Button3 onClick={onAcceptAll}>Accept All Cookies</Button3>
              </>
            )}
          </Right>
        </InnerContent>
      </Inner>
    </Wrapper>
  );
};

Toolbar.propTypes = {
  setActive: PropTypes.func.isRequired,
  onAcceptAll: PropTypes.func.isRequired,
  onRejectAll: PropTypes.isRequired
};

export default Toolbar;
