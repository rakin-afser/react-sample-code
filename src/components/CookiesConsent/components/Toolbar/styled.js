import styled from 'styled-components/macro';
import {mainFont} from 'constants/fonts';
import {blue, primaryColor} from 'constants/colors';
import media from 'constants/media';

export const Inner = styled.div`
  display: flex;
  align-items: center;
  margin: auto auto 0;
  width: 100%;
  height: 203px;
  border-radius: 12px 12px 0 0;
  transition: ease 1s;
  background-color: #fff;
`;

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  height: 100vh;
  width: 100%;
  transition: ease 1s;
  overflow: hidden;
  z-index: 10;

  &.enter {
    ${Inner} {
      transform: translateY(100%);
    }
  }

  &.enter-active {
    ${Inner} {
      transform: translateY(0);
    }
  }

  &.exit-active {
    ${Inner} {
      transform: translateY(100%);
    }
  }
`;

export const InnerContent = styled.div`
  padding: 0 20px;
  margin: 0 auto;
  display: ${({isMobile}) => (isMobile ? 'block' : 'flex')};
  align-items: center;
  justify-content: space-between;
  max-width: 1244px;

  ${({isMobile}) =>
    isMobile &&
    `
    width: 100%
  `};
`;

export const Left = styled.div`
  flex-basis: 856px;

  p {
    font-family: ${mainFont};
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #545454;
  }
`;

export const LinkButton = styled.button`
  ${Left};

  font-weight: 500;
  text-decoration: underline;
  color: inherit;
  transition: ease 0.4s;

  &:hover {
    color: ${blue};
  }
`;

export const Right = styled.div`
  flex-basis: 245px;
`;

export const RightMobileInner = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  display: flex;
`;

export const Button = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 46px;
  border: 1px solid #000000;
  border-radius: 200px;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #000000;
  transition: ease 0.4s;
  cursor: pointer;

  &:hover {
    color: ${primaryColor};
    border-color: ${primaryColor};
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 16px;
    height: 36px;
  }
`;

export const Button2 = styled(Button)`
  border: none;
`;

export const Button3 = styled(Button)`
  background-color: #000;
  color: #fff;

  &:hover {
    background-color: transparent;
  }
`;
