import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useCookies} from 'react-cookie';
import {useWindowSize} from '@reach/window-size';
import Icons from 'components/Icon';
import {
  Overlay,
  Wrapper,
  CloseBtn,
  Text,
  AllowAllBtn,
  Preferences,
  List,
  ListItem,
  Buttons,
  Button,
  Button2,
  StyledSwitch
} from './styled';

const Settings = ({setActive, onAcceptAll, onRejectAll}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [, setCookie] = useCookies();
  const [cookiesConfig, setCookiesConfig] = useState({
    essential: true,
    performance: true,
    advertising: true,
    analytics: true
  });

  const onChangePerformance = (checked) => {
    setCookiesConfig({...cookiesConfig, performance: checked});
  };

  const onChangeAdvertising = (checked) => {
    setCookiesConfig({...cookiesConfig, advertising: checked});
  };

  const onChangeAnalytics = (checked) => {
    setCookiesConfig({...cookiesConfig, analytics: checked});
  };

  const onConfirm = () => {
    setActive(null);

    setTimeout(() => {
      setCookie('cookiesAgreementEssential', cookiesConfig.essential, {path: '/'});

      if (!cookiesConfig.performance) {
        setCookie('cookiesAgreementPerformance', cookiesConfig.performance, {path: '/'});
      }

      if (!cookiesConfig.advertising) {
        setCookie('cookiesAgreementAdvertising', cookiesConfig.advertising, {path: '/'});
      }

      if (!cookiesConfig.analytics) {
        setCookie('cookiesAgreementAnalytics', cookiesConfig.analytics, {path: '/'});
      }
    }, 1000);
  };

  return (
    <Overlay>
      <Wrapper isMobile={isMobile}>
        <CloseBtn onClick={() => setActive('toolbar')}>
          <Icons type="close" color="#1A1A1A" width={32} height={32} />
        </CloseBtn>
        <h2>Cookie Settings</h2>
        <Text isMobile={isMobile}>
          testSample may request cookies to be set on your device. We use cookies to let us know when you visit our testSample
          web-site, to understand how you interact with us, to enrich and personalize your user experience, to enable
          social media functionality and to customize your relationship with testSample, including providing you with more
          relevant advertising. Click on the different category headings to find out more. You can also change your
          cookie preferences at any time. Note that blocking some types of cookies may impact your experienxe on our
          testSample web-site and the services we are able to offer.
        </Text>
        <AllowAllBtn isMobile={isMobile} onClick={onAcceptAll}>
          Allow All
        </AllowAllBtn>
        <Preferences>
          <h3>Manage Consent Preferences</h3>
          <List>
            <ListItem>
              Essential Website Cookies <span>Always Active</span>
            </ListItem>
            <ListItem>
              Performance and Functionality Cookies{' '}
              <StyledSwitch size={isMobile && 'small'} defaultChecked onChange={onChangePerformance} />
            </ListItem>
            <ListItem>
              Advertising (Targeting) Cookies{' '}
              <StyledSwitch size={isMobile && 'small'} defaultChecked onChange={onChangeAdvertising} />
            </ListItem>
            <ListItem>
              Analytics and Customization Cookies{' '}
              <StyledSwitch size={isMobile && 'small'} defaultChecked onChange={onChangeAnalytics} />
            </ListItem>
          </List>
        </Preferences>
        <Buttons isMobile={isMobile}>
          <Button isMobile={isMobile} onClick={onRejectAll}>
            Reject All
          </Button>
          <Button2 onClick={onConfirm}>Confirm</Button2>
        </Buttons>
      </Wrapper>
    </Overlay>
  );
};

Settings.propTypes = {
  setActive: PropTypes.func.isRequired,
  onAcceptAll: PropTypes.func.isRequired,
  onRejectAll: PropTypes.func.isRequired
};

export default Settings;
