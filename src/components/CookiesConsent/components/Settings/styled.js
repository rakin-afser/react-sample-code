import styled from 'styled-components/macro';
import {blue, primaryColor} from 'constants/colors';
import {Switch} from 'antd';
import media from 'constants/media';

export const Wrapper = styled.div`
  position: relative;
  padding: ${({isMobile}) => (isMobile ? '20px' : '81px 48px 50px 47px')};
  background: #ffffff;
  border-radius: 0 12px 12px 0;
  max-width: 510px;
  width: 100%;
  height: 100%;
  transition: ease 1s;

  h2 {
    margin-bottom: 28px;
    font-weight: 500;
    font-size: 24px;
    color: #000000;

    @media (max-width: ${media.mobileMax}) {
      margin-bottom: 18px;
    }
  }
`;

export const Overlay = styled.div`
  position: fixed;
  left: 0;
  top: 0;
  width: 100%;
  height: 100vh;
  z-index: 1000;
  transition: ease 1s;

  &.enter {
    ${Wrapper} {
      transform: translateX(-100%);
    }
  }

  &.enter-done {
    ${Wrapper} {
      transform: translateX(0);
    }
  }

  &.exit-active {
    ${Wrapper} {
      transform: translateX(-100%);
    }
  }
`;

export const CloseBtn = styled.button`
  padding: 4px;
  position: absolute;
  right: 22px;
  top: 22px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: ease 0.4s;
  border-radius: 20px;
  background-color: transparent;

  i {
    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    background-color: #333;

    i {
      svg {
        path {
          fill: #fff;
        }
      }
    }
  }

  @media (max-width: ${media.mobileMax}) {
    top: 16px;
  }
`;

export const Text = styled.p`
  margin-bottom: 23px;
  font-size: ${({isMobile}) => (isMobile ? '13px' : '14px')};
  line-height: 145%;
  color: #545454;
`;

export const AllowAllBtn = styled.button`
  padding: 0;
  position: relative;
  margin-bottom: ${({isMobile}) => (isMobile ? '27px' : '63px')};
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  transition: ease 0.4s;

  &::after {
    content: '';
    position: absolute;
    bottom: -6px;
    left: 0;
    width: 100%;
    height: 2px;
    background-color: #000;
    transition: ease 0.4s;
  }

  &:hover {
    color: ${blue};

    &::after {
      background-color: ${blue};
    }
  }
`;

export const Preferences = styled.div`
  margin-bottom: 71px;

  h3 {
    margin-bottom: 29px;
    font-weight: 500;
    font-size: 18px;
    color: #000000;
  }

  @media (max-width: ${media.mobileMax}) {
    margin-bottom: 10px;

    h3 {
      margin-bottom: 18px;
    }
  }
`;

export const List = styled.ul``;

export const ListItem = styled.li`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-top: 1px solid #efefef;
  padding: 17px 0;
  font-size: 14px;
  color: #545454;

  span {
    font-weight: 500;
    color: #ed484f;
  }

  &:last-child {
    border-bottom: 1px solid #efefef;
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 13px;
    padding: 12px 0;
  }
`;

export const StyledSwitch = styled(Switch)`
  &.ant-switch-checked {
    background-color: ${primaryColor};
  }
`;

export const Buttons = styled.div`
  display: flex;
  align-items: center;
  justify-content: ${({isMobile}) => (isMobile ? 'flex-start' : 'space-between')};
`;

export const Button = styled.button`
  min-width: 200px;
  height: 46px;
  border: 1px solid #666666;
  border-radius: 200px;
  font-size: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #545454;
  transition: ease 0.4s;

  &:hover {
    color: ${primaryColor};
    border-color: ${primaryColor};
  }

  &:first-child {
    ${({isMobile}) =>
      isMobile &&
      `
      margin-right: 10px;
    `}
  }

  @media (max-width: ${media.mobileMax}) {
    margin-top: 10px;
    height: 36px;
    font-size: 16px;
    min-width: 120px;
  }
`;

export const Button2 = styled(Button)`
  background-color: #000;
  color: #fff;

  &:hover {
    background-color: transparent;
  }
`;
