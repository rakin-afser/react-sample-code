import styled from 'styled-components';
import {Modal} from 'antd';

const StyledModal = styled(Modal)`
  top: 0 !important;

  ${({isMobile}) =>
    isMobile &&
    `
      position: absolute:
      top: auto !important;
      bottom: 0 !important;
      padding: 0 !important;
      height: calc(100% - 20px);
      overflow: hidden;
      
      
  `};

  & .ant-modal-content {
    border-radius: 12px;

    ${({isMobile}) =>
      isMobile &&
      `
      position: absolute !important;
      left: 0 !important;
      width: 100% !important;
      bottom: -20px !important;
  `};
  }

  & .ant-modal-body {
    padding: ${({isMobile}) => (isMobile ? '20px 4px 20px 10px !important' : '52px 14px 42px 70px !important')};
    height: ${({isMobile}) => (isMobile ? '100%' : 'calc(100vh - 200px)')};

    & .ScrollbarsCustom-Content {
      padding-right: ${({isMobile}) => (isMobile ? '30px !important' : '50px !important')};
    }

    & .ScrollbarsCustom-TrackY {
      ${({isMobile}) =>
        isMobile &&
        `
            top: 50px !important;
             height: calc(100% - 60px) !important;
          `};
    }

    h2 {
      text-align: center;
      margin-bottom: 27px;
    }

    h3 {
      margin-bottom: 23px;
    }

    p {
      margin-bottom: 18px;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 145%;
      color: #545454;

      &:last-child {
        font-weight: 500;
      }
    }
  }
`;

export default StyledModal;
