import React, {useState} from 'react';
import {useCookies} from 'react-cookie';
import {CSSTransition} from 'react-transition-group';
import Policy from 'components/CookiesConsent/components/Policy';
import Toolbar from 'components/CookiesConsent/components/Toolbar';
import Settings from 'components/CookiesConsent/components/Settings';
import {Overlay} from './styled';

const CookiesConsent = () => {
  const [cookies, setCookie] = useCookies();
  const [active, setActive] = useState('toolbar');

  const onAcceptAll = () => {
    setActive(null);

    setTimeout(() => {
      setCookie('cookiesAgreementEssential', true, {path: '/'});
    }, 1000);
  };

  const onRejectAll = () => {
    setActive(null);

    setTimeout(() => {
      setCookie('cookiesAgreementEssential', true, {path: '/'});
      setCookie('cookiesAgreementPerformance', false, {path: '/'});
      setCookie('cookiesAgreementAdvertising', false, {path: '/'});
      setCookie('cookiesAgreementAnalytics', false, {path: '/'});
    }, 1000);
  };

  if (!cookies.cookiesAgreementEssential) {
    return (
      <Overlay>
        <CSSTransition in={active === 'toolbar'} timeout={1000} unmountOnExit>
          <Toolbar onAcceptAll={onAcceptAll} onRejectAll={onRejectAll} setActive={setActive} />
        </CSSTransition>
        <CSSTransition in={active === 'settings'} timeout={1000} unmountOnExit>
          <Settings onAcceptAll={onAcceptAll} onRejectAll={onRejectAll} setActive={setActive} />
        </CSSTransition>
        <Policy setActive={setActive} visible={active === 'policy'} />
      </Overlay>
    );
  }

  return null;
};

CookiesConsent.propTypes = {};

export default CookiesConsent;
