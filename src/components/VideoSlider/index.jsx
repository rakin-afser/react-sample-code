import {mobileHeaderHeight} from 'components/Header/constants';
// import {ReactComponent as Mute} from 'images/svg/mute.svg';
// import {ReactComponent as Unmute} from 'images/svg/unmute.svg';
import {ReactComponent as PlayIcon} from 'images/svg/play-icon.svg';
import {array} from 'prop-types';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import Slider from 'react-slick';
import useGlobal from 'store';
import {CSSTransition} from 'react-transition-group';
import {
  Play,
  // MuteButton,
  Slide,
  SlideVideo,
  VideoProgress,
  Wrapper
} from './styled';

const VideoSlider = ({slides}) => {
  const slider = useRef(null);
  const slideRefs = useRef([]);
  const [percent, setPercent] = useState(0);
  const [globalState, globalActions] = useGlobal();

  const settings = {
    dots: true,
    arrows: false,
    infinite: false,
    touchThreshold: 15,
    beforeChange: (curr, next) => {
      if (isInViewport(slideRefs.current[0]) && globalState.isAutoplay) {
        slideRefs.current[curr].querySelector('video').pause();
        slideRefs.current[next].querySelector('video').play();
      }
    }
  };

  const isInViewport = useCallback(function(el) {
    const rect = el.getBoundingClientRect();
    return rect.top >= parseInt(mobileHeaderHeight) && rect.bottom <= window.innerHeight;
  }, []);

  useEffect(() => {
    function handleScroll() {
      if (isInViewport(slideRefs.current[0]) && globalState.isAutoplay) {
        slider.current.querySelector('.slick-current video').play();
      } else {
        slider.current.querySelector('.slick-current video').pause();
      }
    }

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [slideRefs, slider, globalState.isAutoplay]);

  function onTimeUpdate(e) {
    const currentTime = e.target.currentTime;
    const duration = e.target.duration;

    setPercent(Math.round((currentTime / duration) * 100));
  }

  function onClickPlay(e) {
    globalActions.setIsAutoplay(!globalState.isAutoplay);
    e.target
      .closest('div')
      .querySelector('video')
      .play();
  }

  return (
    <Wrapper ref={slider}>
      <Slider {...settings}>
        {slides &&
          slides.length &&
          slides.map((slide, key) => (
            <Slide key={key} ref={(el) => (slideRefs.current[key] = el)}>
              {/* <CSSTransition in={isMuted} timeout={300} classNames="mute" unmountOnExit>
                <MuteButton onClick={() => setIsMuted(false)}>
                  <Mute />
                </MuteButton>
              </CSSTransition>
              <CSSTransition in={!isMuted} timeout={300} classNames="mute" unmountOnExit>
                <MuteButton onClick={() => setIsMuted(true)}>
                  <Unmute />
                </MuteButton>
              </CSSTransition> */}
              <CSSTransition in={!globalState.isAutoplay} timeout={300} classNames="opacity" unmountOnExit>
                <Play onClick={onClickPlay}>
                  <PlayIcon />
                </Play>
              </CSSTransition>
              <SlideVideo
                playsInline={globalState.isAutoplay}
                controls
                onTimeUpdate={onTimeUpdate}
                muted={globalState.isMuted}
                loop
                src={slide}
              />
              <VideoProgress percent={percent} />
            </Slide>
          ))}
      </Slider>
    </Wrapper>
  );
};

VideoSlider.defaultProps = {
  slides: []
};

VideoSlider.propTypes = {
  slides: array
};

export default VideoSlider;
