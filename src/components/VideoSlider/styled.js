import styled from 'styled-components';
import {primaryColor} from 'constants/colors';
import {Button, Progress} from 'antd';

export const Wrapper = styled.div`
  position: relative;

  .slick {
    &-track {
      display: flex;

      &:before,
      &:after {
        display: none;
      }
    }

    &-slide {
      > div {
        display: flex;
      }
    }

    &-dots {
      position: static;
      display: flex !important;
      justify-content: center;
      margin: 8px 0 16px 0;

      li {
        width: 12px;
        height: 6px;
        border-radius: 50%;
        margin: 0;

        &.slick-active {
          button {
            background: ${primaryColor};
          }
        }

        button {
          padding: 0;
          margin: auto;
          width: 6px;
          height: 6px;
          border-radius: 50%;
          background: #e4e4e4;

          &:before {
            display: none;
          }
        }
      }
    }
  }

  .opacity-enter {
    opacity: 0;
  }
  .opacity-enter-active {
    opacity: 1;
    transition: opacity 300ms;
  }
  .opacity-exit {
    opacity: 1;
  }
  .opacity-exit-active {
    opacity: 0;
    transition: opacity 300ms;
  }
`;

export const Slide = styled.div`
  position: relative;
`;

export const SlideVideo = styled.video`
  width: 100vw;
  height: 100vh;
  max-height: calc(100vmin - 100px);
  object-fit: cover;
  min-height: 100%;
`;

export const PopupWrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 100;
  background: #fff;
  transition: all 0.3s ease;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  max-width: 100vw;

  opacity: ${({active}) => (active ? 1 : 0)};
  transform: translateY(${({active}) => (active ? '0px' : '-50px')});
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
`;

export const IconWrapper = styled.div`
  position: absolute;
  top: 10px;
  right: 3px;
  z-index: 110;
  cursor: pointer;
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SliderNav = styled.div`
  position: absolute;
  bottom: 34px;
  left: 12px;
  right: 12px;
  z-index: 102;

  .TrackX {
    display: none !important;
  }

  .ScrollbarsCustom-Content {
    display: flex;
    justify-content: center;
  }
`;

export const SlideNav = styled.div`
  min-width: 62px;
  width: 62px;
  height: 62px;
  border: 1px solid #cccccc;
  border-radius: 2px;
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  background-image: url(${({image}) => image});
  position: relative;
  margin: 0 4px;

  &:before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: linear-gradient(0deg, rgba(255, 255, 255, 0.7), rgba(255, 255, 255, 0.7));
    transition: all 0.3s ease;
    opacity: 0;
  }

  ${({active}) => {
    if (!active) {
      return `
        &:before {
          opacity: 1;
        }
      `;
    }
  }}
`;

export const MuteButton = styled(Button)`
  position: absolute !important;
  top: 40px;
  left: 5px;
  background-color: rgba(27, 27, 27, 0.51) !important;
  border: none !important;
  display: inline-flex !important;
  align-items: center;
  justify-content: center;
  z-index: 1;
  border-radius: 50%;
`;

export const VideoProgress = styled(Progress)`
  position: absolute !important;
  bottom: 0;
  left: 0;
  right: 0;
  font-size: 0 !important;

  .ant-progress-inner {
    background-color: rgba(44, 42, 42, 0.2);
    border-radius: 0;
  }

  .ant-progress-bg {
    border-radius: 0 !important;
  }
`;

VideoProgress.defaultProps = {
  strokeColor: '#ED484F',
  strokeWidth: 4,
  showInfo: false,
  strokeLinecap: 'square'
};

export const Play = styled(Button)`
  &&& {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 64px;
    height: 64px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    border-color: #fff;
    z-index: 10;
    overflow: hidden;

    svg {
      margin-left: 2px;
      margin-top: 4px;
    }
  }
`;
