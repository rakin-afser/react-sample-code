import {primaryColor} from 'constants/colors';
import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components/macro';
import media from 'constants/media';

export const Video = styled.video``;

export const VideoWrapper = styled.div`
  > div {
    width: 100%;
    height: 100%;
  }

  &&& {
    .vjs {
      &-poster {
        background-size: cover;
      }

      &-volume-panel {
        position: absolute;
        top: 13px;
        right: 17px;
        width: 30px !important;
        height: 30px;

        @media (max-width: ${media.mobileMax}) {
          top: 0;
          left: 0;
          bottom: 0;
          right: 0;
          width: auto !important;
          height: auto;
        }
      }

      &-mute-control {
        width: 100%;

        @media (max-width: ${media.mobileMax}) {
          height: 100%;
        }
      }

      &-remaining-time,
      &-volume-panel {
        @media (max-width: ${media.mobileMax}) {
          transition: opacity 0.3s ease;
          opacity: 1;
        }
      }

      &-mute-control .vjs-icon-placeholder {
        @media (max-width: ${media.mobileMax}) {
          width: 30px;
          height: 30px;
          position: absolute;
          top: auto;
          bottom: 34px;
          right: 11px;
          background: rgba(27, 27, 27, 0.51);
          border-radius: 50%;
        }
      }

      &-volume-control {
        display: none;
      }

      &-remaining-time {
        position: absolute;
        bottom: 0;
        right: 0;
        height: auto;
        font-size: 12px;
        font-family: ${secondaryFont};
        font-weight: 600;

        span:nth-child(2) {
          display: none;
        }
      }

      &-progress-control {
        height: auto;
        width: 100%;
        position: absolute;
        bottom: 0;
        left: 0;
      }

      &-progress-holder {
        margin: 0;
      }

      &-play-progress {
        background-color: ${primaryColor};

        &::before {
          display: none;
        }
      }

      &-load-progress {
        background-color: rgba(44, 42, 42, 0.2);
      }

      &-control-bar {
        height: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: transparent;
        display: block !important;
        opacity: 1 !important;
      }

      &-user-inactive {
        .vjs-play-control {
          opacity: 0;
        }

        .vjs-volume-panel {
          opacity: 0;
        }

        .vjs-remaining-time {
          opacity: 0;
        }
      }

      &-ended {
        .vjs-play-control {
          opacity: 1;
        }
      }

      &-play-control {
        height: auto;
        position: absolute;
        z-index: 1;
        font-size: 30px;
        opacity: 1;
        transition: opacity ease 0.3s;

        &:not(.vjs-ended) {
          @media (max-width: ${media.mobileMax}) {
            display: none;
          }
        }
      }

      &-play-control,
      &-big-play-button {
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        height: 68px;
        width: 68px;
        border-radius: 50%;
        background-color: #fff;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
        border: none;
        font-size: 30px;
        z-index: 1;

        .vjs-icon-placeholder {
          &::before {
            font-size: 1em;
            width: auto;
            height: auto;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            color: ${primaryColor};
          }
        }
      }
    }
  }
`;
