import React, {forwardRef, useEffect, useImperativeHandle, useRef} from 'react';
// import Hls from 'hls.js';
import videojs from 'video.js';
import {VideoWrapper, Video} from './styled';

export const VideoPlayer = forwardRef((props, ref) => {
  const {src, poster, autoPlay, onClick, onPlay, onPause, ...restProps} = props;
  const videoRef = useRef(null);

  useImperativeHandle(ref, () => ({
    play: async () => {
      if (videoRef.current.play !== undefined) {
        try {
          await videoRef.current.play();
        } catch (error) {
          console.log(`error`, error);
        }
      }
    },
    pause: () => {
      videoRef.current.pause();
    }
  }));

  useEffect(() => {
    const videoJSOptions = {
      controls: true,
      userActions: {hotkeys: true},
      preload: 'auto',
      inactivityTimeout: 1000,
      ...restProps
    };

    const player = videojs(videoRef.current, videoJSOptions, () => {
      player.src(src);
      player.controlBar.fullscreenToggle.dispose();
      if (player?.controlBar?.pictureInPictureToggle) {
        player.controlBar.pictureInPictureToggle.dispose();
      }
    });

    // player.controlBar.progressControl.dispose();
    // player.controlBar.progressControl.disable();
    // player.controlBar.fullscreenControl.dispose();
    // player.controlBar.remainingTimeDisplay.dispose();
    player.on('play', (e) => {
      onPlay(e);
    });

    player.on('click', (e) => {
      onClick(e);
    });

    player.on('pause', (e) => {
      onPause(e);
    });

    return () => {
      player.dispose();
    };
  }, [src]);

  return (
    <VideoWrapper className="vjs-custom-wrapper">
      <div data-vjs-player>
        <Video muted ref={videoRef} poster={poster} autoPlay={autoPlay} className="video-js" playsInline />
      </div>
    </VideoWrapper>
  );
});

VideoPlayer.defaultProps = {
  autoplay: false,
  loop: false,
  onPause: () => {},
  onPlay: () => {},
  onClick: () => {}
};

export default VideoPlayer;
