import React from 'react';
import {Scrollbars} from 'react-custom-scrollbars';

const Scrollbar = (props) => {
  const {children, ...rest} = props;
  return (
    <Scrollbars
      hideTracksWhenNotNeeded
      renderTrackVertical={({style, ...restProps}) => (
        <div
          style={{
            ...style,
            backgroundColor: 'rgba(0, 0, 0, 0.1)',
            width: 4,
            top: 15,
            bottom: 15,
            right: 0,
            borderRadius: 4
          }}
          {...restProps}
        />
      )}
      renderThumbVertical={({style, ...restProps}) => (
        <div
          style={{
            ...style,
            backgroundColor: 'rgba(0, 0, 0, 0.4)',
            width: '100%',
            borderRadius: 'inherit'
          }}
          {...restProps}
        />
      )}
      {...rest}
    >
      {children}
    </Scrollbars>
  );
};

export default Scrollbar;
