import styled from 'styled-components/macro';

export const WrapperDesktop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.28);
  padding: 100px 20px;

  .inner {
    width: 100%;
    max-width: 1040px;
    background: #ffffff;
    border-radius: 8px;
    margin: auto;
    display: flex;
    flex-wrap: wrap;
    position: relative;
  }

  .content {
    width: 100%;
    padding: 42px 60px;
  }

  .footer {
    margin-top: auto;
    border-top: 1px solid #ececec;
    padding: 32px;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    justify-content: center;
    width: 100%;

    .button {
      height: 48px;
      background: #ffffff;
      border: 1px solid #000000;
      border-radius: 24px;

      font-style: normal;
      font-weight: 600;
      font-size: 18px;
      line-height: 48px;
      display: inline-flex;
      align-items: center;
      text-align: center;
      justify-content: center;
      letter-spacing: 0.019em;
      color: #000000;
      margin: 0 15px;
      min-width: 229px;
    }

    .button--black {
      background: #000;
      color: #fff;
    }
  }

  .close {
    position: absolute;
    top: 12px;
    right: 12px;
    cursor: pointer;
    padding: 10px;
    display: flex;
    transition: all 0.3s ease;

    &:hover {
      opacity: 0.4;
    }
  }

  .title {
    font-family: 'Helvetica Neue';
    font-style: normal;
    font-weight: bold;
    font-size: 24px;
    line-height: 120%;
    text-align: center;
    letter-spacing: 0.013em;
    color: #000000;
    display: block;
    margin-bottom: 60px;
  }

  .list {
    display: flex;
    margin: 0 -10px;
  }

  .item {
    width: calc(25% - 20px);
    margin: 0 10px;
    background: #ffffff;
    box-shadow: 0px 10px 20px rgba(58, 60, 74, 0.1);
    border-radius: 30px;
    padding: 18px 10px 23px;

    &__thumb {
      min-height: 188px;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    &__title {
      margin-top: 18px;
      font-style: normal;
      font-weight: bold;
      font-size: 20px;
      line-height: 24px;
      color: #000000;
      text-align: center;
    }

    &__text {
      margin-top: 10px;
      font-style: normal;
      font-weight: normal;
      font-size: 12px;
      line-height: 140%;
      color: #050505;
      opacity: 0.6;
      text-align: center;
    }
  }
`;

export const WrapperMobile = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10000;
  background: #fff;
  padding: 84px 20px 0;

  @media (max-width: 370px) {
    padding: 54px 16px 16px;
  }

  .skip {
    position: absolute;
    top: 52px;
    right: 28px;
    font-family: 'Helvetica Neue';
    cursor: pointer;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: rgba(0, 0, 0, 0.65);
    padding: 0;

    @media (max-width: 370px) {
      top: 32px;
    }
  }

  .next {
    position: absolute;
    bottom: 22px;
    right: 28px;
    width: 58px;
    height: 58px;
    border: 1px solid #000000;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0;
    z-index: 10;

    @media (max-width: 370px) {
      bottom: 40px;
      right: 26px;
    }
  }

  .signin {
    position: absolute;
    bottom: 32px;
    left: 28px;
    width: 88px;
    height: 36px;
    border: 1px solid #000000;
    border-radius: 36px;
    font-style: normal;
    font-weight: 600;
    font-size: 13px;
    line-height: 16px;
    text-align: center;
    text-transform: uppercase;
    color: #000000;
    letter-spacing: 0.5px;
    z-index: 10;

    @media (max-width: 370px) {
      bottom: 49px;
      left: 16px;
    }
  }

  .back {
    position: absolute;
    bottom: 33px;
    left: 21px;
    padding: 0;
    width: 34px;
    height: 34px;
    display: flex;
    justify-content: center;
    align-items: center;
    transform: rotate(180deg);
    z-index: 10;

    @media (max-width: 370px) {
      left: 10px;
    }
  }

  .slick {
    &-dots {
      bottom: 48px !important;
      height: 5px;
      display: flex !important;
      justify-content: center;

      li {
        width: 5px;
        height: 5px;
        border-radius: 50%;
        background: #000000;
        opacity: 0.4;
        margin: 0 5px;
        font-size: 0;

        &.slick-active {
          opacity: 1;
        }

        button {
          display: none !important;
        }
      }
    }

    &-track {
      display: flex !important;
      height: 100% !important;
    }

    &-slide,
    &-list,
    &-slider {
      height: 100% !important;
    }
  }

  .item {
    display: flex !important;
    flex-wrap: wrap;

    &.last .item {
      &__title {
        margin-top: 25px;
      }
    }

    &__thumb {
      width: 100%;
      min-height: 37vh;

      img {
        width: auto;
        height: 37vh;
        margin: 0 auto;
        display: block;
      }

      @media (max-height: 600px) {
        min-height: 32vh;

        img {
          height: 32vh;
        }
      }

      @media (max-width: 370px) {
        min-height: 30vh;

        img {
          height: 30vh;
        }
      }
    }

    &__title {
      width: 100%;
      font-style: normal;
      font-weight: bold;
      font-size: 30px;
      line-height: 36px;
      color: #000000;
      margin-bottom: 17px;
      margin-top: 55px;
      text-align: center;

      @media (max-height: 600px) {
        font-size: 26px;
        margin-top: 35px;
        margin-bottom: 10px;
      }

      @media (max-width: 370px) {
        font-size: 24px;
        margin-top: 35px;
        margin-bottom: 10px;
      }
    }

    &__text {
      width: 100%;
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      line-height: 140%;
      color: #050505;
      opacity: 0.65;
      text-align: center;

      @media (max-height: 600px) {
        font-size: 14px;
      }

      @media (max-width: 370px) {
        font-size: 14px;
      }
    }

    &__buttons {
      margin-top: 10px;
      display: flex;
      flex-wrap: wrap;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      width: 100%;

      .button {
        height: 40px;
        background: #ffffff;
        border: 1px solid #000000;
        border-radius: 24px;

        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 40px;
        display: inline-flex;
        align-items: center;
        text-align: center;
        justify-content: center;
        letter-spacing: 0.019em;
        color: #000000;
        margin: 0;
        min-width: 255px;

        &:not(:first-child) {
          margin-top: 16px;
        }
      }

      .button--black {
        background: #000;
        color: #fff;
      }
    }
  }
`;

export const PaginationHolder = styled.div`
  display: flex;
  justify-content: space-around;
  position: absolute;
  bottom: 45px;
  left: 0;
  right: 0;
  max-width: 75px;
  margin: 0 auto;

  .swiper-pagination-bullet {
    width: 5px;
    height: 5px;
    transition: opacity 0.3s ease-in;

    &:not(.swiper-pagination-bullet-active) {
      opacity: 0.4;
    }

    &-active {
      background-color: #000;
    }
  }
`;
