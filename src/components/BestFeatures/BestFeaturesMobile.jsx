import React, {useState, useRef} from 'react';
import {Link, useHistory} from 'react-router-dom';
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {Pagination, EffectFade} from 'swiper';

import Icon from 'components/Icon';

import {WrapperMobile, PaginationHolder} from './styled';

import icon1 from './images/icon-1.svg';
import icon2 from './images/icon-2.svg';
import icon3 from './images/icon-3.svg';
import icon4 from './images/icon-4.svg';
import icon5 from './images/icon-5.svg';
import TransitionOpacity from 'containers/Animations/TransitionOpacity';

SwiperCore.use([EffectFade, Pagination]);

const BestFeaturesMobile = (props) => {
  const {setShow, ...rest} = props;
  const history = useHistory();
  const slider = useRef(null);
  const [currentIndex, setCurrentIndex] = useState(0);
  const slides = [
    {
      id: 1,
      image: icon1,
      title: 'Discover & Search',
      text: 'A social marketplace to search and discover your favorite Items, Stores, Posts & Lists'
    },
    {
      id: 2,
      image: icon2,
      title: 'Be a Creator & Earn',
      text: 'Create a video, add products, start earning when someone buys from your post'
    },
    {
      id: 3,
      image: icon3,
      title: 'Shop & Earn Reward',
      text: 'Earn it with every purchase and redeem it in your next eliigible purchase'
    },
    {
      id: 4,
      image: icon4,
      title: 'Follow & Explore',
      text: 'Influencers, brands, friends, stores'
    },
    {
      id: 5,
      image: icon5,
      title: 'Get Started',
      text: ''
    }
  ];

  const onNext = () => {
    if (slider && slider.current) {
      slider.current.slideNext();
    }
  };

  const onBack = () => {
    if (slider && slider.current) {
      slider.current.slidePrev();
    }
  };

  const onSignIn = () => {
    setShow(false);
    history.push('/auth/sign-in');
  };

  const onSkip = () => {
    setShow(false);
  };

  const settings = {
    onSwiper(swiper) {
      slider.current = swiper;
    },
    effect: 'fade',
    pagination: {
      clickable: true,
      el: '.custom-pagination'
    },
    fadeEffect: {crossFade: true},
    onTransitionEnd(swiper) {
      const {activeIndex} = swiper || {};
      setCurrentIndex(activeIndex);
    }
  };

  const lastSlide = currentIndex === slides.length - 1;

  return (
    <WrapperMobile {...rest}>
      <TransitionOpacity active={!lastSlide}>
        <button type="button" className="skip" onClick={onSkip}>
          Skip
        </button>
      </TransitionOpacity>
      <Swiper {...settings}>
        {slides?.map((slide, i) => {
          return (
            <SwiperSlide key={slide?.id}>
              <div className={`item ${i === slides.length - 1 ? ' last' : ''}`}>
                <div className="item__thumb">
                  <img src={slide.image} alt={slide.title} />
                </div>
                <div className="item__title">{slide.title}</div>
                {slide.text ? <div className="item__text">{slide.text}</div> : null}
                {i === slides.length - 1 ? (
                  <>
                    <div className="item__buttons">
                      <Link to="/auth/sign-in" className="button">
                        Sign in
                      </Link>
                      <Link to="/auth/sign-up" className="button button--black">
                        Create Account
                      </Link>
                    </div>
                  </>
                ) : null}
              </div>
            </SwiperSlide>
          );
        })}
      </Swiper>
      <PaginationHolder className="custom-pagination" />
      <TransitionOpacity active={lastSlide}>
        <button type="button" className="back" onClick={onBack}>
          <Icon type="arrow" width="7px" height="12px" color="#000" />
        </button>
      </TransitionOpacity>
      <TransitionOpacity active={!lastSlide}>
        <button type="button" className="signin" onClick={onSignIn}>
          Sign In
        </button>
      </TransitionOpacity>
      <TransitionOpacity active={!lastSlide}>
        <button type="button" className="next" onClick={onNext}>
          <Icon type="arrow" width="7px" height="12px" color="#000" />
        </button>
      </TransitionOpacity>
    </WrapperMobile>
  );
};

export default BestFeaturesMobile;
