import React, {useEffect, useState} from 'react';
import {useWindowSize} from '@reach/window-size';

import BestFeaturesDesktop from './BestFeaturesDesktop';
import BestFeaturesMobile from './BestFeaturesMobile';
import TransitionOpacity from 'containers/Animations/TransitionOpacity';

const BestFeatures = () => {
  const wasShown = localStorage.getItem('bestFeatures') === 'true';
  const [show, setShow] = useState(!wasShown);
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  useEffect(() => {
    if (show) {
      localStorage.setItem('bestFeatures', true);
    }
  }, [show]);

  if (isMobile) {
    return (
      <TransitionOpacity active={show}>
        <BestFeaturesMobile setShow={setShow} />
      </TransitionOpacity>
    );
  }

  return (
    <TransitionOpacity active={show}>
      <BestFeaturesDesktop setShow={setShow} />
    </TransitionOpacity>
  );
};

export default BestFeatures;
