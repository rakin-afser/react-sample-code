import React from 'react';
import {Link} from 'react-router-dom';

import Icon from 'components/Icon';

import {WrapperDesktop} from './styled';

import icon1 from './images/icon-1.svg';
import icon2 from './images/icon-2.svg';
import icon3 from './images/icon-3.svg';
import icon4 from './images/icon-4.svg';

const BestFeaturesDesktop = (props) => {
  const {setShow, ...rest} = props;
  return (
    <WrapperDesktop {...rest}>
      <div className="inner">
        <span className="close" onClick={() => setShow(false)}>
          <Icon type="close" color="#000" />
        </span>
        <div className="content">
          <span className="title">Best Features We Got</span>

          <div className="list">
            <div className="item">
              <div className="item__thumb">
                <img src={icon1} alt="Discover & Search" />
              </div>
              <div className="item__title">Discover & Search</div>
              <div className="item__text">
                A social marketplace to search and discover your favorite Items, Stores, Posts & Lists
              </div>
            </div>
            <div className="item">
              <div className="item__thumb">
                <img src={icon2} alt="Be a Creator & Earn" />
              </div>
              <div className="item__title">Be a Creator & Earn</div>
              <div className="item__text">
                Create a video, add products, start earning when someone buys from your post
              </div>
            </div>
            <div className="item">
              <div className="item__thumb">
                <img src={icon3} alt="Shop & Earn Reward" />
              </div>
              <div className="item__title">Shop & Earn Reward</div>
              <div className="item__text">
                Earn it with every purchase and redeem it in your next eliigible purchase
              </div>
            </div>
            <div className="item">
              <div className="item__thumb">
                <img src={icon4} alt="Follow & Explore" />
              </div>
              <div className="item__title">Follow & Explore</div>
              <div className="item__text">Influencers, brands, friends, stores</div>
            </div>
          </div>
        </div>
        <div className="footer">
          <Link to="/auth/sign-in" className="button">
            Sign in
          </Link>
          <Link to="/auth/sign-up" className="button button--black">
            Create Account
          </Link>
        </div>
      </div>
    </WrapperDesktop>
  );
};

export default BestFeaturesDesktop;
