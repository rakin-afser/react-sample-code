import {Link} from 'react-router-dom';
import styled, {css} from 'styled-components';

export const StoresContainer = styled.div`
  border-top: 4px solid #f7f8fa;
  max-width: 100%;
  padding-top: 12px;
  ${({order}) => css`
    order: ${order};
  `}
`;

export const Store = styled.div`
  min-width: 160px;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.06);
  border-radius: 4px;
  margin-bottom: 15px;

  & + & {
    margin-left: 8px;
  }
`;

export const StoreThumb = styled.img`
  width: 100%;
  height: 92px;
  object-fit: ${({placeholder}) => (placeholder ? '100% 100%' : 'cover')};
  border-radius: 4px;
`;

export const StoreContent = styled.div`
  width: 100%;
  padding: 10px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`;

export const StoreTitle = styled.span`
  display: block;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  margin: 0 0 6px;
  width: 100%;
`;

export const StoreInside = styled.div``;

export const StoreRating = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #999999;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  margin-bottom: 10px;

  span {
    padding-left: 8px;
  }
`;

export const StoreContainer = styled.div`
  display: flex;
  max-width: 100%;
  overflow: auto;
`;

export const StoreWrapper = styled.div`
  display: flex;
  padding: 0 16px;
`;

export const Header = styled.div`
  margin-bottom: 12px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 16px;
  width: 100%;
`;

export const Title = styled.p`
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 21px;
  color: #343434;
  margin: 0;
`;

export const Text = styled.p`
  margin-bottom: 0;
`;

export const View = styled.a`
  margin: 0 0 0 auto;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-align: right;
  color: #464646;
  display: flex;
  align-items: center;
  line-height: 1.2;

  svg {
    width: 9px;
    height: 14px;
    margin-left: 20px;
  }
`;

export const LinkThumb = styled(Link)`
  width: 100%;
`;
