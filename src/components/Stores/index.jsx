import React from 'react';
import Arrow from 'assets/Arrow';
import Store from './components/Store';
import {StoreContainer, StoreWrapper, Header, Title, View, Text, StoresContainer} from './styled';

const Stores = ({stores, order}) => {
  if (!stores) {
    return null;
  }

  return (
    <StoresContainer order={order}>
      <Header>
        <div>
          <Title>New Shops:</Title>
          <Text>Recomendations to you</Text>
        </div>
        <View>
          <span>View All</span>
          <Arrow color="#666666" />
        </View>
      </Header>
      <StoreContainer>
        <StoreWrapper>
          {stores?.nodes?.map((store) => (
            <Store store={store} key={store?.id} />
          ))}
        </StoreWrapper>
      </StoreContainer>
    </StoresContainer>
  );
};

export default Stores;
