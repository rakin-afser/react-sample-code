import React from 'react';

import Star from 'assets/Star';

import {Store, StoreThumb, StoreContent, StoreInside, StoreTitle, StoreRating, LinkThumb} from '../styled';
import placeholder from 'images/placeholder.png';
import {Link} from 'react-router-dom';
import Btn from 'components/Btn';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';

const SearchStore = ({store}) => {
  const {id, name, storeUrl, banner, followed, rating} = store || {};
  const [followStore, {loading: loadingfollowStore}] = useFollowUnFollowStoreMutation({
    followed
  });

  return (
    <Store>
      <LinkThumb to={`/shop/${storeUrl}/products`}>
        <StoreThumb src={banner || placeholder} placeholder={!banner ? 1 : 0} />
      </LinkThumb>
      <StoreContent>
        <StoreInside>
          <Link to={`/shop/${storeUrl}/products`}>
            <StoreTitle dangerouslySetInnerHTML={{__html: name}} />
          </Link>
          <StoreRating>
            {rating ? <span>{rating} Followers</span> : ''}
            <Star fill={rating >= 1 ? '#FFC131' : '#CCC'} />
            <Star fill={rating >= 2 ? '#FFC131' : '#CCC'} />
            <Star fill={rating >= 3 ? '#FFC131' : '#CCC'} />
            <Star fill={rating >= 4 ? '#FFC131' : '#CCC'} />
            <Star fill={rating >= 5 ? '#FFC131' : '#CCC'} />
          </StoreRating>
        </StoreInside>
        <Btn
          kind={followed ? 'following' : 'follow'}
          loading={loadingfollowStore}
          onClick={() =>
            followStore({
              variables: {input: {id: parseInt(id)}}
            })
          }
        />
      </StoreContent>
    </Store>
  );
};

export default SearchStore;
