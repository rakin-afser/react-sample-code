import styled from 'styled-components';
import {Select} from 'antd';

const SelectStyled = styled(Select)`
  display: inline-block;
  width: 100%;

  & .ant-select-selection {
    margin-right: 7px;

    &.ant-select-selection--single {
      margin-right: 0;
      border-radius: 2px;
      height: 40px;
      color: #000;
    }
  }

  & .ant-select-selection__rendered {
    margin-left: 15px;
    line-height: 41px;
  }

  & .ant-select-arrow {
    margin-top: -2px;
    transition: ease 0.3s;
  }

  &.ant-select-open {
    & .ant-select-arrow {
      transform: rotate(180deg);
    }
  }
`;

export default SelectStyled;
