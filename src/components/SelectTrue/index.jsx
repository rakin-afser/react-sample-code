import React from 'react';
import PropTypes from 'prop-types';
import SelectStyled from 'components/SelectTrue/styled';
import {ReactComponent as SelectIcon} from './img/selectIcon.svg';

const Select = ({value, onChange, options, valueKey, labelKey, name, ...props}) => {
  return (
    <SelectStyled suffixIcon={<SelectIcon />} value={value} onChange={onChange} name={name} {...props}>
      {options?.map((el, idx) => (
        <SelectStyled.Option value={el[valueKey]} key={idx}>
          {el[labelKey]}
        </SelectStyled.Option>
      ))}
    </SelectStyled>
  );
};

Select.defaultProps = {
  name: ''
};

Select.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  valueKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  labelKey: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.string
};

export default Select;
