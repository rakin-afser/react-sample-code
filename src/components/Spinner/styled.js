import styled from 'styled-components';

export const SpinnerArea = styled.div`
  display: flex;
  justify-content: space-around;
  width: 100%;
  padding-top: 42px;
  color: #c3c3c3;
  font-size: 16px;
`;
