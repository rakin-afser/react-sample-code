import React, {useRef, useState} from 'react';
import {GoogleMap, LoadScript, Marker, StandaloneSearchBox} from '@react-google-maps/api';
import {Input, Wrapper, InputWrapper, SearchIcon, LocBtn} from './styled';
import Icon from 'components/Icon';
import Geocode from 'react-geocode';
import {getAddressComponents} from 'util/heplers';

Geocode.setApiKey('AIzaSyBuNEl8COfr7Is6_LPjrI-SaTRcPssw98o');
Geocode.setLocationType('ROOFTOP');

const Map = (props) => {
  const [libraries] = useState(['places']);
  const {center, position, setPosition, setCenter, setFields} = props || {};
  const ref = useRef();
  async function onPlacesChanged() {
    const {location} = ref?.current?.getPlaces()?.[0]?.geometry || {};
    const place = ref?.current?.getPlaces()?.[0];

    const addrComponets = getAddressComponents(place);

    setFields(addrComponets);

    setCenter({lat: location?.lat(), lng: location?.lng()});
    setPosition({lat: location?.lat(), lng: location?.lng()});
  }

  function getCurrentPostition() {
    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    async function success(pos) {
      const {coords} = pos || {};
      setPosition({lat: coords.latitude, lng: coords.longitude});
      setCenter({lat: coords.latitude, lng: coords.longitude});

      const {results} = await Geocode.fromLatLng(coords.latitude, coords.longitude);
      const place = results?.[0] || {};
      const addrComponets = getAddressComponents(place);
      setFields(addrComponets);
    }

    function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    navigator.geolocation.getCurrentPosition(success, error, options);
  }

  async function onClickMap(e) {
    setPosition({lat: e.latLng.lat(), lng: e.latLng.lng()});
    const {results} = await Geocode.fromLatLng(e.latLng.lat(), e.latLng.lng());
    const place = results?.[0] || {};
    const addrComponets = getAddressComponents(place);
    setFields(addrComponets);
  }

  return (
    <LoadScript language="en" libraries={libraries} googleMapsApiKey="AIzaSyCHdkvgxa2gSIsZxu8Rjnulh_iiZh2QC5s">
      <StandaloneSearchBox
        onLoad={(r) => {
          ref.current = r;
        }}
        onPlacesChanged={onPlacesChanged}
      >
        <Wrapper>
          <InputWrapper>
            <Input type="text" placeholder="Search Road or Landmark" />
            <SearchIcon type="search" width={18} height={18} color="#A7A7A7" />
          </InputWrapper>
          <LocBtn type="circle" onClick={getCurrentPostition}>
            <Icon type="target" width={15} height={15} />
          </LocBtn>
        </Wrapper>
      </StandaloneSearchBox>
      <GoogleMap
        onClick={onClickMap}
        zoom={10}
        center={center}
        mapContainerStyle={{
          height: 220
        }}
        options={{
          fullscreenControl: false,
          zoomControl: false,
          rotateControl: false,
          streetViewControl: false,
          mapTypeControl: false
        }}
      >
        {position ? <Marker onClick={() => setPosition(null)} position={position} /> : null}
      </GoogleMap>
    </LoadScript>
  );
};

export default Map;
