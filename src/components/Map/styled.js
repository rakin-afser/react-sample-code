import styled from 'styled-components';
import Icon from 'components/Icon';
import {Button} from 'antd';

export const Wrapper = styled.div`
  display: flex;
  margin-top: 12px;
  margin-bottom: 12px;
`;

export const InputWrapper = styled.div`
  position: relative;
  width: 100%;
`;

export const Input = styled.input`
  &&& {
    width: 100%;
    height: 33px;
    padding: 4px 30px 4px 11px;
    outline: none;

    &::placeholder {
      color: #bfbfbf;
    }
  }
`;

export const SearchIcon = styled(Icon)`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: 10px;
`;

export const LocBtn = styled(Button)`
  &&& {
    min-width: 25px;
    width: 25px;
    height: 25px;
    margin: auto auto auto 8px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
  }
`;
