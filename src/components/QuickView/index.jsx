import React, {useEffect, useState} from 'react';
import {useWindowSize} from '@reach/window-size';

import useGlobal from 'store';
import QuickViewMobile from 'components/QuickView/mobile';
import QuickViewDesktop from 'components/QuickView/desktop';
import PostViewMobile from 'components/Modals/mobile/PostViewMobile';
import useModal from 'hooks/useModal';

const QuickView = () => {
  const [globalState] = useGlobal();
  const [show, setShow] = useState(true);
  const {quickView} = globalState;
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const {closeModal} = useModal();

  const onClose = () => {
    closeModal();
    setShow(false);
  };

  useEffect(() => {
    if (quickView) {
      setShow(true);
    }
  }, [quickView]);

  const renderByType = (type) => {
    if (type === 'product') {
      return (
        <QuickViewDesktop
          multiple={false}
          multipleSwitcher={false}
          isFeedbackWithImages={false}
          visibility={show}
          setVisibility={onClose}
          data={quickView}
          showRating
          showInfluencer={false}
          showFollowers
        />
      );
    }

    if (type === 'multipleProducts') {
      return (
        <QuickViewDesktop
          multiple
          multipleSwitcher
          isFeedbackWithImages={false}
          visibility={show}
          setVisibility={onClose}
          data={quickView}
          showRating
          showInfluencer={false}
          showFollowers
          postDisplaying
        />
      );
    }

    if (type === 'postPopup') {
      return (
        <QuickViewDesktop
          multipleSwitcher
          isFeedbackWithImages={false}
          visibility={show}
          setVisibility={onClose}
          data={quickView}
          showRating={false}
          showInfluencer
          showFollowers
          postDisplaying
        />
      );
    }

    if (type === 'pageFeedback') {
      return (
        <QuickViewDesktop
          multiple={false}
          multipleSwitcher={false}
          isFeedbackWithImages
          visibility={show}
          setVisibility={onClose}
          data={quickView}
          showRating={false}
          showInfluencer={false}
          showFollowers
        />
      );
    }

    // if (type === 'postView') {
    //   return <PostView isOpen={show} />;
    // }

    return (
      // videoAndImage type
      <QuickViewDesktop
        multiple={false}
        multipleSwitcher={false}
        isFeedbackWithImages={false}
        visibility={show}
        setVisibility={onClose}
        data={quickView}
        showRating={false}
        showInfluencer={false}
        showFollowers
      />
    );
  };

  const renderByTypeMobile = (type) => {
    switch (type) {
      case 'postView':
        return <PostViewMobile isOpen={show} />;
      default:
        return <QuickViewMobile />;
    }
  };

  // eslint-disable-next-line no-nested-ternary
  return (
    <>
      {quickView
        ? isMobile
          ? renderByTypeMobile(quickView.quickViewType)
          : renderByType(quickView.quickViewType)
        : null}
    </>
  );
};

QuickView.defaultProps = {};

QuickView.propTypes = {};

export default QuickView;
