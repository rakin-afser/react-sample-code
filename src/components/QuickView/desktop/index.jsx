import React, {useState} from 'react';
import {Modal, Row, Col} from 'antd';
import PropTypes from 'prop-types';

import OrderInfo from 'containers/ProductPage/Components/OrderInfo';
import MediaViewer from 'components/QuickView/desktop/components/MediaViewer';
import Header from 'components/QuickView/desktop/components/Header';
import Tags from 'components/Tags';
import {data as exampleData, products, post} from 'components/QuickView/desktop/exampleData';
import FeedbackWithImages from './components/FeedbackWithImages';
import userPlaceholder from 'images/placeholders/user.jpg';
import {ReactComponent as IconClose} from './imgs/iconClose.svg';
import {ReactComponent as IconArrow} from './imgs/iconArrow.svg';
import {ModalContainer, OutsideArrow, LeftWrapper, IconCloseStyled} from './styled';
import useGlobal from 'store';

const QuickViewDesktop = ({
  isFeedbackWithImages,
  multipleSwitcher,
  multiple,
  visibility,
  setVisibility,
  showRating,
  showInfluencer,
  showFollowers,
  postDisplaying
}) => {
  const [globalState, globalActions] = useGlobal();
  const {quickView} = globalState;
  const {online} = exampleData;
  const product = quickView?.data;

  const {name, gravatar, rating, totalFollowers} = product?.seller || {};
  const galleryImages = product?.galleryImages?.nodes ? product?.galleryImages?.nodes : [];
  const [mainImage, setMainImage] = useState();
  const images = product?.image && galleryImages ? [mainImage || product?.image, ...galleryImages] : null;
  const productVideo = product?.product_video_url;
  const [postSelection, setPostSelection] = useState(postDisplaying);
  const [inCart, setInCart] = useState([]);
  const [activeProduct, setActiveProduct] = useState(postSelection ? post : images);
  const [orderInfoForMultiple, setOrderInfoForMultiple] = useState(false);

  const leftColWidth = () => {
    if (orderInfoForMultiple) {
      return 12;
    }

    return 24;
  };

  function renderModalContent() {
    return (
      <>
        <ModalContainer switcher={multipleSwitcher}>
          <Row>
            <Col span={multiple ? leftColWidth() : 12}>
              {' '}
              <LeftWrapper multiple={multiple} withSwitcher={multipleSwitcher}>
                <Header
                  multipleSwitcher={multipleSwitcher}
                  followers={totalFollowers}
                  avatar={gravatar || userPlaceholder}
                  rating={rating}
                  name={name}
                  showRating={showRating}
                  showInfluencer={showInfluencer}
                  showFollowers={showFollowers}
                  online={online}
                  postData={post}
                  showRight={orderInfoForMultiple}
                  shopUrl={product?.seller?.storeUrl}
                  withRight={multiple}
                />

                <MediaViewer
                  multipleSwitcher={multipleSwitcher}
                  setPostSelection={setPostSelection}
                  multiple={multiple}
                  postSelection={postSelection}
                  products={products}
                  postData={post}
                  productVideo={productVideo}
                  activeProduct={activeProduct || images}
                  setActiveProduct={setActiveProduct}
                  setOrderInfoForMultiple={setOrderInfoForMultiple}
                  inCart={inCart}
                  orderInfoForMultiple={orderInfoForMultiple}
                />

                <Tags items={product?.productTags?.nodes} styles={{marginTop: '63px'}} />
              </LeftWrapper>
            </Col>
            {!multiple && (
              <Col span={multiple ? 11 : 12}>
                {isFeedbackWithImages ? (
                  <FeedbackWithImages />
                ) : (
                  <OrderInfo
                    setMainImage={setMainImage}
                    inCart={inCart}
                    multiple={multiple}
                    productData={{product}}
                    postData={post}
                    quickView
                    postDiplaying={postSelection}
                    isModal
                    isMulipleModal={multiple}
                    returnsInfoDisplaying={false}
                    paymentsInfoDisplaying={false}
                    setPostSelection={setPostSelection}
                    setInCart={setInCart}
                  />
                )}
              </Col>
            )}
            {multiple && orderInfoForMultiple && (
              <Col span={multiple ? 11 : 12}>
                <OrderInfo
                  inCart={inCart}
                  multiple={multiple}
                  productData={activeProduct}
                  postData={post}
                  quickView
                  postDiplaying={postSelection}
                  isModal
                  isMulipleModal={multiple}
                  returnsInfoDisplaying={false}
                  paymentsInfoDisplaying={false}
                  setPostSelection={setPostSelection}
                  setOrderInfoForMultiple={setOrderInfoForMultiple}
                  setInCart={setInCart}
                  orderInfoForMultiple={orderInfoForMultiple}
                />
              </Col>
            )}
          </Row>
        </ModalContainer>
        <OutsideArrow>
          <IconArrow />
        </OutsideArrow>
        <OutsideArrow right>
          <IconArrow />
        </OutsideArrow>
      </>
    );
  }

  return (
    <Modal
      centered
      wrapClassName="quick-view-modal"
      width={multiple || multipleSwitcher ? '1007px' : '862px'}
      bodyStyle={{
        padding: multipleSwitcher ? '27px 28px 24px 18px' : '27px 28px 24px 42px',
        top: 0,
        height: multiple && multipleSwitcher ? 637 : 'auto',
        minHeight: 530,
        display: 'flex',
        alignItems: 'center'
      }}
      afterClose={() => globalActions.setQuickView(null)}
      visible={visibility}
      onOk={setVisibility}
      onCancel={setVisibility}
      footer={null}
      closeIcon={
        <IconCloseStyled>
          <IconClose
            style={{
              width: 16,
              position: 'relative',
              height: 16,
              top: -8,
              right: -8
            }}
          />
        </IconCloseStyled>
      }
    >
      {renderModalContent()}
    </Modal>
  );
};

QuickViewDesktop.defaultProps = {
  isFeedbackWithImages: false,
  multipleSwitcher: false,
  multiple: false,
  visibility: false,
  setVisibility: (f) => f,
  showRating: false,
  showInfluencer: true,
  showFollowers: false,
  avatar: null,
  postDisplaying: false
};

QuickViewDesktop.propTypes = {
  isFeedbackWithImages: PropTypes.bool,
  multipleSwitcher: PropTypes.bool,
  multiple: PropTypes.bool,
  visibility: PropTypes.bool,
  setVisibility: PropTypes.func,
  showRating: PropTypes.bool,
  showInfluencer: PropTypes.bool,
  showFollowers: PropTypes.bool,
  avatar: PropTypes.node,
  postDisplaying: PropTypes.bool
};

export default QuickViewDesktop;
