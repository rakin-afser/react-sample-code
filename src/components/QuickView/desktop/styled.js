import styled from 'styled-components/macro';
import sliderBlackArrow from 'images/blackArrow.png';
import {primaryColor} from 'constants/colors';
import sliderArrow from './imgs/slider-arrow.png';

export const IconCloseStyled = styled.div`
  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    svg {
      path {
        fill: #000;
      }
    }
  }
`;

export const ModalContainer = styled.div`
  height: 100%;
  width: 100%;

  .ant-row,
  .ant-col {
    height: 100%;
  }

  .slick-slider,
  .slick-slide {
    width: 362px !important;
    height: 362px !important;

    .slick-dots li button:after {
      width: 8px;
      height: 8px;
    }

    .slick-dots li.slick-active button:after {
      background-color: ${primaryColor};
    }
  }

  .lower-slider.slick-slider,
  .lower-slider.slick-slider .slick-slide {
    width: 110px;
    height: auto !important;
  }

  .lower-slider {
    margin: ${({switcher}) => (switcher ? '21px auto 16px 66px' : '40px auto 16px 0')};
    max-width: 362px;

    .slick-list {
      padding-bottom: 0 !important;
    }

    .slick-prev:before,
    .slick-next:before {
      background: url(${sliderBlackArrow}) no-repeat;
    }

    .slick-arrow,
    .slick-prev:before,
    .slick-next:before {
      width: 5px !important;
      height: 8px !important;
    }

    .slick-prev {
      left: -9px !important;
    }

    .slick-next {
      right: -9px !important;
    }
  }

  .slick-prev,
  .slick-next {
    z-index: 20;
  }

  .slick-prev,
  .slick-next,
  .slick-prev:before,
  .slick-next:before {
    height: 37px !important;
    width: 37px !important;
  }

  .slick-prev:before,
  .slick-next:before {
    background: url(${sliderArrow});
  }

  .slick-prev {
    left: 12px;
  }

  .slick-next {
    right: 12px;
  }

  .slick-dots li.slick-active button:after {
    background-color: #ea2c34;
  }
`;

export const OutsideArrow = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  top: 50%;
  transform: translateY(-50%) !important;
  position: absolute;

  ${({right}) => (right ? {right: '-52px', svg: {transform: 'rotateY(180deg) !important'}} : {left: '-52px'})}
  &:focus {
    outline: none;
  }

  svg {
    transition: ease 0.4s;

    path {
      transition: inherit;
    }
  }

  &:hover {
    svg {
      path {
        fill: #000;
      }
    }
  }
`;

export const PreviewImage = styled.img`
  width: 60px !important;
  height: 60px !important;
  margin-right: 9px !important;
  margin-bottom: 10px;
  cursor: pointer;
`;

export const PreviewVideoBlock = styled.div`
  width: 60px;
  height: 60px;
  margin-right: 6px !important;
  cursor: pointer;
  position: relative;

  ${PreviewImage} {
    margin-right: 0 !important;
  }

  i {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const LeftWrapper = styled.div`
  max-width: ${({withSwitcher}) => (withSwitcher ? '430px' : '362px')};
  max-width: ${({multiple}) => multiple && '100%'};
  margin: ${({multiple}) => (multiple ? '0' : '0 auto 0 0')};
  position: relative;
  overflow: hidden;
`;
