import avatarImg from 'images/avatar3.png';
import nike1 from 'components/QuickView/desktop/pop_up_imgs/nike_1.png';
import nike2 from 'components/QuickView/desktop/pop_up_imgs/nike_2.png';
import nike3 from 'components/QuickView/desktop/pop_up_imgs/nike_3.png';
import offWhite1 from 'components/QuickView/desktop/pop_up_imgs/off_white_1.png';
import offWhite2 from 'components/QuickView/desktop/pop_up_imgs/off_white_2.png';
import offWhite3 from 'components/QuickView/desktop/pop_up_imgs/off_white_3.png';
import givenchy1 from 'components/QuickView/desktop/pop_up_imgs/givenchy_1.png';
import givenchy2 from 'components/QuickView/desktop/pop_up_imgs/givenchy_2.png';
import givenchy3 from 'components/QuickView/desktop/pop_up_imgs/givenchy_3.png';
import model from 'components/QuickView/desktop/imgs/model.png';
import secondImg from 'images/products/card2.jpg';
import firstImg from 'images/products/card1.jpg';
import video2 from 'videos/videoplayback_2.mp4';
import video1 from 'videos/videoplayback.mp4';

export const data = {
  name: 'Chanel',
  online: true,
  followers: '2k',
  rating: '3M',
  price: '499.99',
  avatar: avatarImg,
  multiplePoductVariants: [
    {id: 1, images: [nike1, nike2, nike3], title: '2018 Floral Dress...', price: '499.99'},
    {id: 2, images: [offWhite1, offWhite2, offWhite3], title: '2018 Floral Dress...', price: '499.99'},
    {id: 3, images: [givenchy1, givenchy2, givenchy3], title: '2018 Floral Dress...', price: '499.99'}
  ],
  poductVariants: [
    {id: 1, images: [model, secondImg, firstImg, secondImg], videos: [video2]},
    {id: 2, images: [secondImg, model, secondImg, firstImg], videos: [video1]},
    {id: 3, images: [firstImg, secondImg], videos: []}
  ]
};

export const products = [
  {
    id: 1,
    title: '2018 Floral Dresses Vestido De Festa Vestido De Festa',
    likes: '602',
    price: '499.99',
    oldPrice: '699.99',
    discount: '40%',
    midCoins: '19',
    brand: 'Zara',
    previewImg: model,
    parameters: [
      {
        id: 1,
        title: 'Color',
        options: [
          {id: 1, value: 'Pink', additionalClass: '-pink'},
          {id: 2, value: 'Yellow', additionalClass: '-yellow'},
          {id: 3, value: 'Red', additionalClass: '-red'},
          {id: 4, value: 'Grey', additionalClass: '-grey'},
          {id: 5, value: 'Black', additionalClass: '-black'},
          {id: 6, value: 'Orange', additionalClass: '-orange'},
          {id: 7, value: 'Green', additionalClass: '-green'}
        ]
      },
      {
        id: 2,
        title: 'Size',
        options: [
          {id: 1, value: 'UK XS, 42', additionalClass: ''},
          {id: 2, value: 'UK S, 36', additionalClass: ''},
          {id: 3, value: 'UK M, 40', additionalClass: ''},
          {id: 4, value: 'UK L, 44', additionalClass: ''},
          {id: 5, value: 'UK XL, 48', additionalClass: ''},
          {id: 6, value: 'UK XXL, 52', additionalClass: ''},
          {id: 7, value: 'UK XXXL, 58', additionalClass: ''}
        ]
      }
    ],
    images: [givenchy2, secondImg, givenchy2],
    videos: []
  },
  {
    id: 2,
    title: 'Floral Dresses Vestido De Festa Vestido De Festa',
    likes: '602',
    price: '499.99',
    oldPrice: '699.99',
    discount: '40%',
    midCoins: '19',
    brand: 'Zara',
    previewImg: nike1,
    parameters: [
      {
        id: 1,
        title: 'Color',
        options: [
          {id: 1, value: 'Pink', additionalClass: '-pink'},
          {id: 2, value: 'Yellow', additionalClass: '-yellow'},
          {id: 3, value: 'Red', additionalClass: '-red'},
          {id: 4, value: 'Grey', additionalClass: '-grey'},
          {id: 5, value: 'Black', additionalClass: '-black'},
          {id: 6, value: 'Orange', additionalClass: '-orange'},
          {id: 7, value: 'Green', additionalClass: '-green'}
        ]
      },
      {
        id: 2,
        title: 'Size',
        options: [
          {id: 1, value: 'UK XS, 42', additionalClass: ''},
          {id: 2, value: 'UK S, 36', additionalClass: ''},
          {id: 3, value: 'UK M, 40', additionalClass: ''},
          {id: 4, value: 'UK L, 44', additionalClass: ''},
          {id: 5, value: 'UK XL, 48', additionalClass: ''},
          {id: 6, value: 'UK XXL, 52', additionalClass: ''},
          {id: 7, value: 'UK XXXL, 58', additionalClass: ''}
        ]
      }
    ],
    images: [nike1, nike2, nike3],
    videos: []
  },
  {
    id: 3,
    title: 'Product 2 Dresses Vestido De Festa Vestido De Festa',
    likes: '602',
    price: '499.99',
    oldPrice: '699.99',
    discount: '40%',
    midCoins: '19',
    brand: 'Zara',
    previewImg: offWhite1,
    parameters: [
      {
        id: 1,
        title: 'Color',
        options: [
          {id: 1, value: 'Pink', additionalClass: '-pink'},
          {id: 2, value: 'Yellow', additionalClass: '-yellow'},
          {id: 3, value: 'Red', additionalClass: '-red'},
          {id: 4, value: 'Grey', additionalClass: '-grey'},
          {id: 5, value: 'Black', additionalClass: '-black'},
          {id: 6, value: 'Orange', additionalClass: '-orange'},
          {id: 7, value: 'Green', additionalClass: '-green'}
        ]
      },
      {
        id: 2,
        title: 'Size',
        options: [
          {id: 1, value: 'UK XS, 42', additionalClass: ''},
          {id: 2, value: 'UK S, 36', additionalClass: ''},
          {id: 3, value: 'UK M, 40', additionalClass: ''},
          {id: 4, value: 'UK L, 44', additionalClass: ''},
          {id: 5, value: 'UK XL, 48', additionalClass: ''},
          {id: 6, value: 'UK XXL, 52', additionalClass: ''},
          {id: 7, value: 'UK XXXL, 58', additionalClass: ''}
        ]
      }
    ],
    images: [offWhite1, offWhite2, offWhite3],
    videos: []
  },
  {
    id: 4,
    title: '2020 Floral Dresses Vestido De Festa Vestido De Festa',
    likes: '612',
    price: '1499.99',
    oldPrice: '2099.99',
    discount: '40%',
    midCoins: '19',
    brand: 'Zara',
    previewImg: nike1,
    parameters: [
      {
        id: 1,
        title: 'Color',
        options: [
          {id: 1, value: 'Pink', additionalClass: '-pink'},
          {id: 2, value: 'Yellow', additionalClass: '-yellow'},
          {id: 3, value: 'Red', additionalClass: '-red'},
          {id: 4, value: 'Grey', additionalClass: '-grey'},
          {id: 5, value: 'Black', additionalClass: '-black'},
          {id: 6, value: 'Orange', additionalClass: '-orange'},
          {id: 7, value: 'Green', additionalClass: '-green'}
        ]
      },
      {
        id: 2,
        title: 'Size',
        options: [
          {id: 1, value: 'UK XS, 42', additionalClass: ''},
          {id: 2, value: 'UK S, 36', additionalClass: ''},
          {id: 3, value: 'UK M, 40', additionalClass: ''},
          {id: 4, value: 'UK L, 44', additionalClass: ''},
          {id: 5, value: 'UK XL, 48', additionalClass: ''},
          {id: 6, value: 'UK XXL, 52', additionalClass: ''},
          {id: 7, value: 'UK XXXL, 58', additionalClass: ''}
        ]
      }
    ],
    images: [nike1, nike2, nike3],
    videos: []
  },
  {
    id: 5,
    title: 'Product 5 Floral Dresses Vestido De Festa Vestido De Festa',
    likes: '602',
    price: '499.99',
    oldPrice: '699.99',
    discount: '40%',
    midCoins: '19',
    brand: 'Zara',
    previewImg: firstImg,
    parameters: [
      {
        id: 1,
        title: 'Color',
        options: [
          {id: 1, value: 'Pink', additionalClass: '-pink'},
          {id: 2, value: 'Yellow', additionalClass: '-yellow'},
          {id: 3, value: 'Red', additionalClass: '-red'},
          {id: 4, value: 'Grey', additionalClass: '-grey'},
          {id: 5, value: 'Black', additionalClass: '-black'},
          {id: 6, value: 'Orange', additionalClass: '-orange'},
          {id: 7, value: 'Green', additionalClass: '-green'}
        ]
      },
      {
        id: 2,
        title: 'Size',
        options: [
          {id: 1, value: 'UK XS, 42', additionalClass: ''},
          {id: 2, value: 'UK S, 36', additionalClass: ''},
          {id: 3, value: 'UK M, 40', additionalClass: ''},
          {id: 4, value: 'UK L, 44', additionalClass: ''},
          {id: 5, value: 'UK XL, 48', additionalClass: ''},
          {id: 6, value: 'UK XXL, 52', additionalClass: ''},
          {id: 7, value: 'UK XXXL, 58', additionalClass: ''}
        ]
      }
    ],
    images: [firstImg, secondImg],
    videos: []
  }
];

export const post = {
  previewImg: firstImg,
  images: [],
  videos: [video1, video2],
  title: "FILT'R INSTANT RETOUCH PALLETTE",
  description:
    'For Rihanna, creating the perfect soft matte base is the most important part of any look Trendy overview for last season, tenden collaboration, FW The magazine covers international, national and local fashion and beauty trends and news. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
};
