import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Scrollbars from 'react-scrollbars-custom';

import ItemInfo from 'components/ItemInfo';
import WithSlider from 'components/WithSlider';
import PreviewProduct from 'components/QuickView/desktop/components/PreviewProduct';
import Icon from 'components/Icon';
import {
  MultipleContainer,
  PreviewImage,
  PreviewImagesWrap,
  SingleSlider,
  SliderImage,
  BuyAll,
  BuyAllBtn
} from './styled';
import Slider from 'react-slick';
// import VideoPlayer from '../VideoPlayer';
import VideoPlayer from 'components/VideoPlayer';

const MediaViewer = ({
  multipleSwitcher,
  setPostSelection,
  postSelection,
  multiple,
  activeProduct,
  setActiveProduct,
  products,
  postData,
  setOrderInfoForMultiple,
  inCart,
  orderInfoForMultiple,
  productVideo
}) => {
  const [slideWasChanged, setSlideWasChanged] = useState(0);
  const [activeProductIndex, setActiveProductIndex] = useState(0);
  const [commentsDisplaying, setCommentsDisplaying] = useState(false);

  const switchActiveProduct = (index) => {
    setActiveProduct(products[index]);
    setPostSelection(false);
    setActiveProductIndex(index);
  };

  const switchToPost = () => {
    setPostSelection(true);
    setActiveProduct(postData);
  };

  const showOrderInfo = (productIndex) => {
    switchActiveProduct(productIndex);
  };

  const settings = {
    dots: true,
    afterChange: () => {
      setSlideWasChanged((prevState) => prevState + 1);
    }
  };

  return (
    <MultipleContainer>
      <div className="imgs-container">
        <SingleSlider>
          <Slider {...settings}>
            {/* {productVideo && <VideoPlayer playing video={productVideo} videoStopTrigger={slideWasChanged} />} */}
            {productVideo && <VideoPlayer autoPlay src={productVideo} />}
            {activeProduct?.map((el) => (
              <SliderImage key={el?.id} src={el?.sourceUrl} alt={el?.altText} />
            ))}
          </Slider>
        </SingleSlider>
        {multipleSwitcher && (
          <PreviewImagesWrap multiple={multiple} multipleSwitcher={multipleSwitcher}>
            <>
              {postData && (
                <PreviewImage active={postSelection} onClick={switchToPost} src={postData.previewImg} alt="img" />
              )}

              {!multiple &&
                products.map((el, i) => {
                  if (i > 3) return null;
                  return (
                    <PreviewImage
                      active={!postSelection && activeProductIndex === i}
                      key={i}
                      onClick={() => switchActiveProduct(i)}
                      src={el.previewImg}
                      alt="prev"
                    />
                  );
                })}

              <Scrollbars
                clientwidth={4}
                noDefaultStyles={false}
                noScroll={false}
                style={{height: '306px', width: '100%'}}
                thumbYProps={{className: 'thumbY'}}
              >
                {multiple &&
                  products.map((el, i) => {
                    return (
                      <PreviewProduct
                        orderInfoForMultiple={orderInfoForMultiple}
                        setOrderInfoForMultiple={setOrderInfoForMultiple}
                        active={!postSelection && activeProductIndex === i}
                        key={i}
                        onClick={() => showOrderInfo(i)}
                        data={el}
                        inCart={inCart}
                      />
                    );
                  })}
              </Scrollbars>
              {multiple && !orderInfoForMultiple && (
                <BuyAll>
                  <div>
                    <span>20% discount</span>
                    <br />
                    if bought all together
                  </div>
                  <BuyAllBtn>
                    <Icon color="#fff" type="cart" width={18} height={18} />
                    <span>Buy All for BD 1499.99</span>
                  </BuyAllBtn>
                </BuyAll>
              )}
            </>
          </PreviewImagesWrap>
        )}
      </div>
    </MultipleContainer>
  );
};

MediaViewer.defaultProps = {
  multipleSwitcher: false,
  setPostSelection: () => {},
  multiple: false,
  postSelection: false,
  orderInfoForMultiple: false,
  inCart: []
};

MediaViewer.propTypes = {
  multipleSwitcher: PropTypes.bool,
  setPostSelection: PropTypes.func,
  postSelection: PropTypes.bool,
  multiple: PropTypes.bool,
  // activeProduct: PropTypes
  setActiveProduct: PropTypes.func.isRequired,
  products: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  // postData,
  setOrderInfoForMultiple: PropTypes.func.isRequired,
  inCart: PropTypes.arrayOf(PropTypes.any),
  orderInfoForMultiple: PropTypes.bool
};

export default MediaViewer;
