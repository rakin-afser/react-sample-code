import styled from 'styled-components/macro';
import {secondaryFont} from 'constants/fonts';
import {grayBackroundColor} from 'constants/colors';

export const MultipleCard = styled.div`
  cursor: pointer;
  max-height: 159px;
  margin: 8px 0;
  border-radius: 2px;
  padding: 8px 8px 12px;

  ${({isSelected}) =>
    isSelected ? {boxShadow: '0px 2px 9px rgba(0, 0, 0, 0.28)'} : {boxShadow: ' 0px 2px 4px rgba(0, 0, 0, 0.13)'}}
  img {
    width: 94px !important;
    height: 104px !important;
  }

  .product-name {
    font-size: 10px;
    line-height: 12px;
    color: #000000;
    margin: 5px 0;
  }

  .price {
    font-size: 8px;
    margin: 0;
    color: #ed484f;
    justify-content: space-between;
    display: flex;
    align-items: center;

    p {
      margin-bottom: 0;
    }

    span {
      font-size: 12px;
    }
  }
`;

export const MultipleContainer = styled.div`
  margin-top: 18px;

  .imgs-container {
    display: flex;
  }
`;

export const PreviewImagesWrap = styled.div`
  width: ${({multiple}) => (multiple ? '100%' : '94px')};
  height: 362px;
`;

export const PreviewImage = styled.img`
  position: relative;
  width: 65px !important;
  height: 65px !important;
  margin-left: 14px !important;
  margin-bottom: 10px;
  cursor: pointer;
  border-bottom: ${({active}) => (active ? '2px solid #ED494F' : '2px solid transparent')};

  //&::after {
  //  content: '';
  //  position: absolute;
  //  left: 50%;
  //  top: 50%;
  //  display: flex;
  //  width: 30px;
  //  height: 30px;
  //  transform: translate(-50%, -50%);
  //  background: #000000;
  //  opacity: 0.4;
  //  border: 0.5px solid #000000;
  //  box-sizing: border-box;
  //  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.19);
  //}
`;

export const SingleSlider = styled.div`
  & > div {
    margin: 0;
  }

  .slick-dots {
    bottom: -23px !important;

    li {
      &.slick-active button {
        &:after {
          background: #656565;
          width: 8px;
          height: 8px;
        }
      }
      button {
        position: relative;
        padding: 0;

        &:before {
          display: none;
        }

        &:after {
          content: '';
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
          width: 6px;
          height: 6px;
          background: ${grayBackroundColor};
          border-radius: 50%;
          transition: all 0.3s ease;
        }
      }
    }
  }

  .slick-prev {
    left: 8px !important;
    transition: ease 0.4s !important;
  }

  .slick-next {
    right: 8px !important;
    transition: ease 0.4s !important;
  }

  .slick-next,
  .slick-prev {
    opacity: 0;
  }

  &:hover {
    .slick-next,
    .slick-prev {
      opacity: 1;
    }
  }

  .vjs-custom-wrapper {
    height: 362px;
  }
`;

export const SliderImage = styled.img`
  width: 362px !important;
  height: 362px !important;
  object-fit: contain;
`;

export const BuyAll = styled.div`
  margin-bottom: 29px;
  padding-right: 24px;
  margin-top: 15px;
  padding-left: 108px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-family: ${secondaryFont};
  font-size: 14px;
  color: #000000;

  span {
    font-weight: 600;
  }
`;

export const BuyAllBtn = styled.button`
  padding: 0 29px;
  display: flex;
  align-items: center;
  background-color: #000;
  color: #fff;
  transition: ease 0.4s;
  height: 36px;
  border-radius: 24px;
  font-size: 16px;
  font-weight: bold;
  border: 1px solid transparent;

  span {
    font-family: ${secondaryFont};
  }

  svg {
    margin-right: 15px;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    background-color: #fff;
    color: #000;
    border: 1px solid #000;

    svg {
      path {
        fill: #000;
      }
    }
  }
`;
