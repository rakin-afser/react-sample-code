import styled from 'styled-components';
import {mainFont, secondaryFont} from 'constants/fonts';
import {midCoinsColor, primaryColor} from 'constants/colors';

export const PreviewImage = styled.img`
  margin-right: 30px;
  width: 65px;
  min-width: 65px;
  height: 65px;
  min-height: 65px;
  margin-left: 4px;
  margin-bottom: 4px;
  cursor: pointer;
`;

export const Main = styled.div``;

export const PreviewWrap = styled.div`
  margin-left: 5px;
  margin-right: 8px;
  padding: 4px 12px 4px 4px;
  display: flex;
  align-items: center;
  flex: 1;
  border-radius: 2px;
  cursor: pointer;
  transition: ease 0.4s;
  box-shadow: ${({active}) => (active ? '0 2px 6px rgba(0, 0, 0, 0.13)' : '0 2px 6px transparent')};

  &:hover {
    box-shadow: 0 2px 6px rgba(0, 0, 0, 0.13);
  }
`;

export const PreviewTitle = styled.h3`
  margin-bottom: 0;
  font-family: ${secondaryFont};
  font-weight: 500;
  font-size: 12px;
  color: #000;
`;

export const Info = styled.div`
  width: 286px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const RatingWrapper = styled.div`
  svg {
    margin-left: 0;
    margin-right: 4px;
  }
`;

export const Price = styled.div`
  font-family: ${mainFont};
  font-size: 12px;
  color: #000;

  span {
    font-size: 14px;
  }
`;

export const OldPrice = styled.div`
  font-family: ${secondaryFont};
  font-size: 11px;
  text-decoration-line: line-through;
  color: #a7a7a7;
`;

export const MidCoins = styled.div`
  font-family: ${secondaryFont};
  font-size: 11px;
  color: ${midCoinsColor};

  svg {
    margin-left: 3px;
    position: relative;
    top: 2px;
  }

  span {
    font-family: ${mainFont};
    font-size: 12px;
  }
`;

export const Button = styled.button`
  font-size: 12px;
  line-height: 1;
  margin-left: auto;
  padding: 0 17px;
  display: ${({hide}) => (hide ? 'none' : 'flex')};
  align-items: center;
  border: 1px solid ${primaryColor};
  border-radius: 24px;
  height: 22px;
  color: ${primaryColor};
  transition: ease 0.4s;

  &:hover {
    background-color: ${primaryColor};
    color: #fff;
  }
`;

export const AddedToCart = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
  font-family: ${mainFont};
  font-size: 12px;
  color: #545454;
`;
