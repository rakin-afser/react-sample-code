import React from 'react';
import PropTypes from 'prop-types';
import StarRating from 'components/StarRating';
import {
  PreviewImage,
  Main,
  PreviewTitle,
  PreviewWrap,
  Info,
  Price,
  OldPrice,
  MidCoins,
  Button,
  AddedToCart,
  RatingWrapper
} from './styled';
import {ReactComponent as IconCheck} from './img/iconCheck.svg';
import {ReactComponent as IconCoins} from './img/iconCoins.svg';

const PreviewProduct = ({active, data, setOrderInfoForMultiple, orderInfoForMultiple, inCart, ...props}) => {
  return (
    <PreviewWrap orderInfoIsActive={orderInfoForMultiple} active={active} {...props}>
      <PreviewImage src={data.previewImg} alt="img" />
      {!orderInfoForMultiple && (
        <Main>
          <PreviewTitle>{data.title}</PreviewTitle>
          <RatingWrapper>
            <StarRating starWidth={10} starHeight={10} />
          </RatingWrapper>
          <Info>
            <Price>
              <span>{data.price}</span>
            </Price>
            <OldPrice>{data.oldPrice}</OldPrice>
            <MidCoins>
              Midcoins: <span>+{data.midCoins}</span>
              <IconCoins />
            </MidCoins>
          </Info>
        </Main>
      )}
      {!orderInfoForMultiple && inCart && inCart.includes(data.id) ? (
        <AddedToCart>
          <IconCheck style={{marginRight: 6}} /> Added to Cart
        </AddedToCart>
      ) : (
        <Button hide={orderInfoForMultiple} onClick={() => setOrderInfoForMultiple(true)}>
          View
        </Button>
      )}
    </PreviewWrap>
  );
};

PreviewProduct.defaultProps = {
  active: 0,
  data: [],
  inCart: false,
  orderInfoForMultiple: false
};

PreviewProduct.propTypes = {
  setOrderInfoForMultiple: PropTypes.func.isRequired,
  active: PropTypes.number,
  data: PropTypes.oneOfType([PropTypes.shape(), PropTypes.arrayOf(PropTypes.any)]),
  inCart: PropTypes.bool,
  orderInfoForMultiple: PropTypes.bool
};

export default PreviewProduct;
