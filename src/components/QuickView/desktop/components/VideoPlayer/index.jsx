import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';
import {PlayBtn, VideoWrapper} from 'components/QuickView/desktop/components/VideoPlayer/styled';
import {ReactComponent as IconPlay} from './img/iconPlay.svg';
import {ReactComponent as IconPause} from './img/iconPause.svg';

const VideoPlayer = ({video, playing, videoStopTrigger}) => {
  const [videoPlay, setVideoPlay] = useState(false);

  const onTogglePlay = () => {
    if (!videoPlay) {
      setVideoPlay(true);
    } else {
      setVideoPlay(false);
    }
  };

  useEffect(() => {
    setVideoPlay(false);
  }, [videoStopTrigger]);

  return (
    <VideoWrapper>
      <ReactPlayer
        loop
        playsinline
        onEnded={() => {
          setVideoPlay(false);
        }}
        playing={videoPlay}
        controls
        url={video}
        width="100%"
        height="100%"
      />
      <PlayBtn onClick={onTogglePlay}>{!videoPlay ? <IconPlay /> : <IconPause />}</PlayBtn>
    </VideoWrapper>
  );
};

VideoPlayer.defaultProps = {
  videoStopTrigger: 0
};

VideoPlayer.propTypes = {
  video: PropTypes.node.isRequired,
  videoStopTrigger: PropTypes.number
};

export default VideoPlayer;
