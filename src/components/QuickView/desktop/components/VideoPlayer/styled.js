import styled from 'styled-components';

export const PlayBtn = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  align-items: center;
  justify-content: center;
  width: 68px;
  height: 68px;
  background: #fafafa;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  border-radius: 50%;
  cursor: pointer;
  opacity: 0;
  transition: ease 0.4s;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    box-shadow: 0 2px 7px rgba(0, 0, 0, 0.3);

    svg {
      path {
        fill: #464646;
      }
    }
  }
`;

export const VideoWrapper = styled.div`
  position: relative;
  height: 362px;
  width: 362px;

  video {
    width: 362px;
    height: 362px;
    left: 0;
    top: 0;
    object-fit: cover;
  }

  &:hover ${PlayBtn} {
    opacity: 1;
  }
`;
