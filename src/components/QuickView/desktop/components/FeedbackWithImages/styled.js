import styled from 'styled-components/macro';
import {secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const FeedbackContainer = styled.div`
  padding-left: 11px;
`;

export const FeedbackTitle = styled.h2`
  margin: 0 0 48px;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  text-align: center;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const ProductBlock = styled.div`
  padding: 16px 20px 9px 16px;
  border: 1px solid #e4e4e4;
  border-radius: 3px;
  margin: 29px 0 24px;
`;

export const ProductImage = styled.img`
  width: 70px !important;
  height: 70px !important;
  margin-right: 16px;
`;

export const ProductInfoBlock = styled.div`
  display: flex;

  h3 {
    font-weight: normal;
    font-size: 14px;
    margin-bottom: 2px;
  }
`;

export const PriceBlock = styled.div`
  font-size: 12px;
  color: #8f8f8f;
  margin-bottom: 3px;
`;

export const CurrentPrice = styled.span`
  font-weight: bold;
  color: #ed494f;
`;

export const OldPrice = styled.span`
  font-weight: bold;
  text-decoration-line: line-through;
`;

export const RatingBlock = styled.div`
  font-size: 12px;
  display: flex;

  div {
    margin-right: 9px;
  }

  svg {
    margin: 0;
    width: 8px;
    height: 8px;
  }
`;

export const DescriptionBlock = styled.div`
  margin-bottom: 135px;

  p {
    margin-top: 11px;
  }
`;

export const LowerWrapper = styled.div`
  display: flex;
  justify-content: space-between;

  .free-shipping {
    margin: 0;
    font-size: 10px;
  }
`;

export const BuyButton = styled.button`
  position: relative;
  top: -6px;
  background: #ed484f;
  border-radius: 24px;
  padding: 7px 8px;
  color: #fff;
  cursor: pointer;
  outline: none;
  border: none;
  display: flex;
  align-items: baseline;
  font-weight: 500;
  width: 103px;
  justify-content: center;
  height: 32px;
  transition: ease 0.4s;

  span {
    line-height: initial;
  }

  .currency {
    text-transform: uppercase;
    font-size: 8px;
  }

  i {
    margin-right: 5px;
    align-self: center;
    cursor: pointer;
  }

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    background-color: transparent;
    border: 1px solid ${primaryColor};
    color: ${primaryColor};

    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const Information = styled.div`
  width: calc(100% - 70px);

  h3 {
    margin-bottom: 0;
  }
`;

export const ButtonsBlock = styled.div`
  display: flex;
  align-items: center;

  button {
    margin-right: 22px;
  }
`;

export const IconWrapper = styled.div`
  position: relative;
  top: -4px;
  left: 2px;
  cursor: pointer;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;
