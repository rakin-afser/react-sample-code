import React, {useState} from 'react';
import StarRating from 'components/StarRating';
import Icon from 'components/Icon';
import prodIcon from 'images/photo.png';
import CommentsComponent from 'components/Comments';
import ItemInfo from 'components/ItemInfo';
import ExpandedText from 'components/ExpandedText';
import {
  FeedbackContainer,
  FeedbackTitle,
  ProductBlock,
  ProductImage,
  ProductInfoBlock,
  PriceBlock,
  CurrentPrice,
  OldPrice,
  RatingBlock,
  DescriptionBlock,
  LowerWrapper,
  BuyButton,
  Information,
  ButtonsBlock,
  IconWrapper
} from './styled';

const FeedbackWithImages = () => {
  const [commentDisplaying, setCommentDisplaying] = useState(false);
  return (
    <FeedbackContainer>
      {!commentDisplaying && <FeedbackTitle>Feedback</FeedbackTitle>}
      <ProductBlock>
        <ProductInfoBlock>
          <ProductImage src={prodIcon} alt="product" />
          <Information>
            <h3>2018 Floral Dresses ...</h3>
            <PriceBlock>
              BD<CurrentPrice>75.00</CurrentPrice> <OldPrice>BD120.00</OldPrice>
            </PriceBlock>
            <LowerWrapper>
              <div>
                <RatingBlock>
                  <StarRating starWidth={8} starHeight={8} stars="5" /> (746)
                </RatingBlock>
                <p className="free-shipping">Free Shipping</p>
              </div>
              <ButtonsBlock>
                <BuyButton>
                  <Icon color="#FFFF" type="cart" width={15} height={15} />
                  <span className="currency">BD</span>
                  <span>526.00</span>
                </BuyButton>
                <IconWrapper>
                  <Icon width={19} height={19} type="bookmark" />
                </IconWrapper>
              </ButtonsBlock>
            </LowerWrapper>
          </Information>
        </ProductInfoBlock>
      </ProductBlock>
      {commentDisplaying ? (
        <CommentsComponent hideComments={setCommentDisplaying} />
      ) : (
        <div>
          <DescriptionBlock>
            <StarRating stars="5" starWidth={20} starHeight={20} />

            <ExpandedText
              textAlign="left"
              margin="8px"
              text="This product is amazing! I have finally found a moisturizer that actually works. I have very dry skin year
              round and nothing until this product has given me enough hydration. I was having trouble with my skin
              being so This product is amazing! I have finally found a moisturizer that actually works. I have very dry skin year
              round and nothing until this product has given me enough hydration. I was having trouble with my skin
              being so"
              divider={150}
            />
          </DescriptionBlock>
          <ItemInfo isModal displayComments={setCommentDisplaying} />
        </div>
      )}
    </FeedbackContainer>
  );
};

export default FeedbackWithImages;
