import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router';

import {
  Followers,
  Name,
  ModalHeader,
  ImgAndInfoWrapper,
  HeaderRight,
  Avatar,
  Status,
  AvatarInner,
  HeaderTopLine,
  Influencer,
  RaitingWrapper,
  RatingAndFollowers,
  HeaderRightSide,
  HeaderWrapper,
  ExpandedTextWrap
} from 'components/QuickView/desktop/components/Header/styled';
import Button from 'components/Buttons';
import star from 'images/blueStar.png';
import StarRating from 'components/StarRating';
import ExpandedText from 'components/ExpandedText';
import AvatarPlaceholder from 'images/avatarPlaceholder.png';
import useGlobal from 'store';

const Header = ({
  multipleSwitcher,
  followers,
  rating,
  name,
  shopUrl = '',
  showRating,
  showFollowers,
  showInfluencer,
  avatar,
  online,
  postData,
  showRight,
  withRight
}) => {
  const history = useHistory();
  const [, globalActions] = useGlobal();

  const goToShopPage = () => {
    history.push(`/shop/${shopUrl}/products`);
    globalActions.setQuickView(null);
  };

  return (
    <HeaderWrapper>
      <ModalHeader showRating={showRating} switcher={multipleSwitcher}>
        <ImgAndInfoWrapper>
          <Avatar onClick={goToShopPage}>
            <AvatarInner>
              <img src={avatar || AvatarPlaceholder} alt="logo" />
            </AvatarInner>
            <Status online={online}>&nbsp;</Status>
          </Avatar>
          <HeaderRight>
            <HeaderTopLine>
              <Name>{name}</Name>
              <Button type="follow" />
            </HeaderTopLine>

            <RatingAndFollowers>
              {showInfluencer && (
                <Influencer>
                  <img src={star} alt="star" />
                  Influencer
                </Influencer>
              )}
              {showRating && (
                <RaitingWrapper>
                  <StarRating stars={rating} /> <span>({rating})</span>
                </RaitingWrapper>
              )}
              {showFollowers && <Followers>{followers} followers</Followers>}
            </RatingAndFollowers>
          </HeaderRight>
        </ImgAndInfoWrapper>
      </ModalHeader>
      {withRight && !showRight && (
        <HeaderRightSide>
          <h2>{postData.title}</h2>
          <ExpandedTextWrap>
            <ExpandedText text={postData.description} textAlign="left" divider={160} />
          </ExpandedTextWrap>
        </HeaderRightSide>
      )}
    </HeaderWrapper>
  );
};

Header.defaultProps = {
  name: '',
  rating: '',
  followers: '',
  online: false,
  avatar: null,
  postData: '',
  showRight: false,
  withRight: false
};

Header.propTypes = {
  avatar: PropTypes.node,
  multipleSwitcher: PropTypes.bool.isRequired,
  followers: PropTypes.string,
  rating: PropTypes.string,
  name: PropTypes.string,
  showRating: PropTypes.bool.isRequired,
  showInfluencer: PropTypes.bool.isRequired,
  showFollowers: PropTypes.bool.isRequired,
  online: PropTypes.bool,
  postData: PropTypes.shape(),
  showRight: PropTypes.bool,
  withRight: PropTypes.bool
};

export default Header;
