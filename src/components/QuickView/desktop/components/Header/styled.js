import styled from 'styled-components/macro';
import {secondaryFont} from 'constants/fonts';
import {secondaryTextColor} from 'constants/colors';

export const HeaderWrapper = styled.div`
  display: flex;
  align-items: flex-start;
`;

export const ModalHeader = styled.div`
  width: 370px;
  display: flex;
  justify-content: space-between;
  ${({switcher}) => (switcher ? {marginLeft: '0'} : null)};

  button {
    position: relative;
    top: ${({showRating}) => (showRating ? '-4px' : '27px')};
    margin: 11px 0 0 0;
    width: 76px;
    height: 22px;
    font-size: 12px;
    cursor: pointer;
  }

  h3 {
    font-weight: 600;
    font-size: 18px;
    margin-bottom: 0;
  }
`;

export const ImgAndInfoWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
`;

export const Avatar = styled.div`
  position: relative;
  width: 60px;
  min-width: 60px;
  height: 60px;
  min-height: 60px;
  margin-right: 16px;
  cursor: pointer;
`;

export const AvatarInner = styled.div`
  border-radius: 50%;
  overflow: hidden;
  border: 1px solid #e4e4e4;

  img {
    width: 100%;
    height: 100%;
  }
`;

export const Status = styled.div`
  position: absolute;
  right: 0;
  bottom: 4px;
  width: 14px;
  height: 14px;
  border: 2px solid #fff;
  border-radius: 50%;
  background-color: ${({online}) => (online ? '#2ecc71' : '#E4E4E4')};
`;

export const HeaderRight = styled.div`
  width: 100%;
`;

export const HeaderTopLine = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const RatingAndFollowers = styled.div`
  display: flex;
  align-items: center;

  .followers-number {
    margin-left: 21px;
    font-weight: 500;
    font-size: 14px;
  }
`;

export const Influencer = styled.span`
  margin-right: 15px;
  background: #e5f1ff;
  border-radius: 24px;
  font-size: 12px;
  line-height: 14px;
  color: #1b5dab;
  padding: 5px 8px;
  display: flex;
  align-items: center;

  img {
    width: 12px;
    height: 12px;
    margin-right: 4px;
  }
`;

export const RaitingWrapper = styled.div`
  margin-right: 9px;
  display: flex;
  align-items: center;
  margin-bottom: 0;

  svg {
    margin: 0 4px 0 0;
  }

  span {
    margin-left: 9px;
    position: relative;
  }
`;

export const Followers = styled.div`
  font-family: ${secondaryFont};
  font-weight: 500;
  font-size: 14px;
  color: ${secondaryTextColor};
`;

export const Name = styled.div`
  padding-top: 10px;
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 18px;
  line-height: 1;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const HeaderRightSide = styled.div`
  position: relative;
  padding-left: 104px;
  flex: 1;
  font-family: ${secondaryFont};
  font-size: 14px;
  color: #464646;
`;

export const ExpandedTextWrap = styled.div`
  position: absolute;
  right: 0;
  z-index: 5;
  max-width: 491px;
  padding-right: 60px;
  padding-bottom: 10px;
  background-color: #fff;
`;
