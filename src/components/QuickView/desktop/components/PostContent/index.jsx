import React, {useState} from 'react';
import CommentsComponent from 'components/Comments';
import Tags from 'components/Tags';
import {Container, HeadText, PostText} from './styled';

const PostContent = ({withComments = false, changeCommentsDsiplaying = (f) => f}) => (
  <Container withComments={withComments}>
    <HeadText>FILT'R INSTANT RETOUCH PALLETTE</HeadText>
    <PostText>
      For Rihanna, creating the perfect soft matte base is the most important part of any look Trendy overview for last
      season, tenden collaboration, FW The magazine{' '}
      {withComments ? (
        <span>...</span>
      ) : (
        <span>
          covers international, national and local fashion and beauty trends and news. Lorem ipsum dolor sit amet,
          consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        </span>
      )}
    </PostText>
    <Tags styles={{marginBottom: '6px'}} />
    {withComments && <CommentsComponent hideComments={changeCommentsDsiplaying} />}
  </Container>
);

export default PostContent;
