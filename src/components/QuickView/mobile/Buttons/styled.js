import styled from 'styled-components';
import {primaryColor} from 'constants/colors';
import Btn from 'components/Btn';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin: 32px -5px 0;

  ${({fixed}) => {
    if (fixed) {
      return `
        margin-top: 0;
        position: fixed;
        bottom: 0;
        left: 0;
        right: 0;
        padding: 16px;
        background: #fff;
        border-top: 1px solid #e4e4e4;
      `;
    }
  }}
`;

export const Button = styled(Btn)`
  margin: 0 5px;
  width: 100%;
`;

export const Error = styled.p`
  color: ${primaryColor};
  margin-top: 10px;
  margin-bottom: 10px;
`;

export const PopupWrapper = styled.div`
  position: fixed;
  top: ${({active}) => (active ? '0px' : '-64px')};
  left: 0;
  right: 0;
  height: 64px;
  background: #65c97a;
  display: flex;
  align-items: center;
  padding: 13px 26px;
  transition: all 0.3s ease;
`;

export const CheckIcon = styled.div`
  width: 24px;
  height: 24px;
  border: 1px solid #2ecc71;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 14px 0 11px;

  svg {
    fill: #2ecc71;
    max-width: 12px;
    max-height: 9px;
  }
`;
