import React, {useState} from 'react';
import {object, func, bool, string, shape} from 'prop-types';
import {useHistory} from 'react-router-dom';
import useGlobal from 'store';
import {Wrapper, Button, Error} from './styled';
import parse from 'html-react-parser';
import {useAddToCartMutation} from 'hooks/mutations';

const Buttons = ({
  postId,
  databaseId,
  disabled,
  variation,
  goToProductPage = () => {},
  style,
  fixed,
  setShowMessage,
  showMessage,
  setShow,
  showBuyNow,
  setShowVariationsPopup
}) => {
  const [, globalActions] = useGlobal();

  const [addedToCart, setAddedToCart] = useState(false);
  const history = useHistory();
  const [addToCart, {loading, error}] = useAddToCartMutation({
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart() {
            return data.addToCart.cart;
          }
        }
      });
    },
    onCompleted() {
      setAddedToCart(true);
      setTimeout(() => {
        if (setShowMessage) setShowMessage(true);
      }, 1000);
    }
  });

  function onAdd() {
    if (variation && setShowVariationsPopup) {
      setShowVariationsPopup(true);
      return;
    }

    const purchaseSource = postId ? {source_type: 'FEED_POST', id: postId} : {};

    if (!addedToCart) {
      addToCart({
        variables: {
          input: {
            productId: databaseId,
            quantity: 1,
            variationId: variation?.databaseId,
            purchase_source: purchaseSource
          }
        }
      });
    } else {
      globalActions.setQuickView(null);
      globalActions.setProductPopup(null);
      history.push('/cart');
    }
  }

  // function onBuyNow() {
  //   if (variation && setShowVariationsPopup) {
  //     setShowVariationsPopup(true);
  //     return;
  //   }

  //   buyNow({
  //     variables: {
  //       input: {
  //         productId: databaseId,
  //         quantity: 1,
  //         variationId: variation?.databaseId
  //       }
  //     }
  //   });
  // }

  function onSwitchDisplay() {
    if (addedToCart) {
      globalActions.setQuickView(null);
    } else {
      goToProductPage();
    }
  }

  function renderError() {
    if (!error) {
      return null;
    }

    if (error) return <Error>{parse(error?.message || '')}</Error>;

    return null;
  }

  return (
    <>
      {renderError()}
      <Wrapper style={style} fixed={fixed}>
        <Button kind="primary" disabled={disabled} onClick={onAdd} loading={loading}>
          {addedToCart ? 'Go to Cart' : 'Add to Cart'}
        </Button>
        <Button kind="back-border" disabled={disabled} onClick={onSwitchDisplay} type="primary">
          {addedToCart ? 'Continue Shopping' : 'Go to Product Page'}
        </Button>
      </Wrapper>{' '}
    </>
  );
};

Buttons.defaultProps = {
  style: {},
  fixed: false,
  showMessage: false,
  setShowMessage: () => {},
  setShow: () => {}
};

Buttons.propTypes = {
  style: shape,
  fixed: bool,
  showMessage: bool,
  setShowMessage: func,
  setShow: func
};

export default Buttons;
