import React, {useEffect, useRef, useState} from 'react';
import {array} from 'prop-types';
import Slider from 'react-slick';

import Popup from './popup';

import {Wrapper, Slide, SlideImage, VideoWrapper} from './styled';
import VideoPlayer from 'components/VideoPlayer';

const MinSlider = ({slides, video}) => {
  const [index, setIndex] = useState(0);
  const slider = useRef(null);
  const sliderPopup = useRef(null);

  const settings = {
    dots: true,
    arrows: false,
    infinite: false,
    touchThreshold: 15,
    speed: 200,
    onSwipe: (direction) => {
      if (direction === 'left' && slides.length - 1 > index) {
        sliderPopup && sliderPopup.current && sliderPopup.current.slickGoTo(index + 1);
        setIndex(index + 1);
      } else if (direction === 'right' && index > 0) {
        sliderPopup && sliderPopup.current && sliderPopup.current.slickGoTo(index - 1);
        setIndex(index - 1);
      }
    }
  };

  let firstClientX, clientX;

  const preventTouch = (e) => {
    const minValue = 5;

    clientX = e.touches[0].clientX - firstClientX;

    if (Math.abs(clientX) > minValue) {
      e.preventDefault();
      e.returnValue = false;

      return false;
    }
  };

  const touchStart = (e) => {
    firstClientX = e.touches[0].clientX;
  };

  useEffect(() => {
    if (slider && slider.current) {
      slider.current.addEventListener('touchstart', touchStart);
      slider.current.addEventListener('touchmove', preventTouch, {
        passive: false
      });
    }

    return () => {
      if (slider && slider.current) {
        slider.current.removeEventListener('touchstart', touchStart);
        slider.current.removeEventListener('touchmove', preventTouch, {
          passive: false
        });
      }
    };
  });

  return (
    <Wrapper ref={slider}>
      <Slider {...settings}>
        {video && (
          <Slide key="video-1">
            <VideoWrapper>
              <VideoPlayer autoPlay src={video} />
            </VideoWrapper>
          </Slide>
        )}
        {slides &&
          slides.length &&
          slides.map((slide, key) => (
            <Slide key={key}>
              <SlideImage
                src={slide}
                onClick={() => {
                  setIndex(key);

                  if (sliderPopup && sliderPopup.current) {
                    sliderPopup.current.slickGoTo(key);
                  }
                }}
              />
            </Slide>
          ))}
      </Slider>
    </Wrapper>
  );
};

MinSlider.defaultProps = {
  slides: []
};

MinSlider.propTypes = {
  slides: array
};

export default MinSlider;
