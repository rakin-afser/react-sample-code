import React, {useState} from 'react';
import {number, func, bool, object} from 'prop-types';

import Icon from 'components/Icon';
import Popup from './popup';
import useGlobal from 'store';
import {Wrapper, Counter, CounterLabel, Divider} from './styled';
import {useUser} from 'hooks/reactiveVars';
import {onShareProduct} from 'util/heplers';

const Counters = ({
  style,
  triggerLike,
  likes,
  isLiked,
  comments,
  showCommentsPopup,
  setShowCommentsPopup,
  item = {}
}) => {
  const [, setGlobalState] = useGlobal();
  const [user] = useUser();
  const {databaseId, seller, slug, name} = item;
  const formatNumbers = (number) => {
    return number.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
  };

  const onClick = () => {
    if (user?.databaseId) {
      setGlobalState.setActionsPopup(true, {productId: databaseId, seller, slug, name});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  const onLike = () => {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    if (triggerLike) triggerLike();
  };

  const onComment = () => {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    setShowCommentsPopup(true);
  };

  return (
    <Wrapper style={style}>
      <Counter onClick={onLike}>
        <Icon type={isLiked ? 'liked' : 'like'} />
        <CounterLabel type={isLiked ? 'liked' : 'like'}>{formatNumbers(likes)}</CounterLabel>
      </Counter>

      <Counter onClick={onComment}>
        <Icon type="message" />
        <CounterLabel>{formatNumbers(comments)}</CounterLabel>
      </Counter>

      <Counter
        onClick={() => onShareProduct({title: name, text: 'I would like to recommend this product at testSample', slug})}
      >
        <Icon type="share" />
      </Counter>

      <Divider />

      <Counter onClick={onClick}>
        <Icon type="bookmarkIcon" />
      </Counter>

      <Popup showCommentsPopup={showCommentsPopup} setShowCommentsPopup={setShowCommentsPopup} />
    </Wrapper>
  );
};

Counters.defaultProps = {
  style: {},
  likes: null,
  comments: null,
  showCommentsPopup: false,
  setShowCommentsPopup: () => {}
};

Counters.propTypes = {
  style: object,
  likes: number,
  comments: number,
  showCommentsPopup: bool,
  setShowCommentsPopup: func
};

export default Counters;
