import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import {useLazyQuery} from '@apollo/client';

import useGlobal from 'store';
import ProductAdded from 'components/ProductPage/mobile/ProductAdded';
import SizeGuide from 'components/ProductPage/mobile/SizeGuide';
import BackLink from './BackLink';
import MainSlider from 'components/ProductPage/mobile/MainSlider';
import Info from './Info';
import Counters from './Сounters';
import {Wrapper} from '../styled';
import {useVariations} from 'hooks';
import {getVariationByAttrs, renderCoinsByVariation} from 'util/heplers';
import useModal from 'hooks/useModal';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';
import {PRODUCT_QUICKVIEW} from 'queries';
import Loader from 'components/Loader';

const QuickViewMobile = () => {
  const [globalState, globalActions] = useGlobal();
  const {quickView} = globalState;
  const [show, setShow] = useState(false);
  const [showAddToList, setShowAddToList] = useState(false);
  const [showCommentsPopup, setShowCommentsPopup] = useState(false);
  const [showVariationsPopup, setShowVariationsPopup] = useState(false);
  const [showMessage, setShowMessage] = useState(false);
  const [showSizeGuide, setShowSizeGuide] = useState(false);
  const history = useHistory();
  const {closeModal} = useModal();
  const {slug, loadProductData} = quickView || {};
  const [getProductData, {data: productData, loading}] = useLazyQuery(PRODUCT_QUICKVIEW);

  useEffect(() => {
    if (loadProductData) {
      getProductData({variables: {id: slug}});
    }
  }, []);

  const product = productData?.product || quickView || {};

  const {
    databaseId,
    name,
    image,
    isLiked,
    totalLikes,
    reviewCount,
    commentCount,
    galleryImages,
    midCoins,
    averageRating,
    variations,
    productCategories,
    product_video_url: productVideoUrl,
    availableShipping
  } = product;

  const [triggerLike] = useAddUpdateLikedProductMutation({
    variables: {input: {id: databaseId}},
    product: quickView
  });

  useEffect(() => {
    if (quickView) {
      setShow(true);
      document.body.classList.add('overflow-hidden');
    }
  }, [quickView]);

  const onClose = () => {
    setShow(false);
    closeModal();

    document.body.classList.remove('overflow-hidden');
    globalActions.setQuickView(null);
  };

  const goToProductPage = () => {
    onClose();
    history.push(`/product/${slug}`);
  };

  const [vars, varState, setVarState] = useVariations(product);
  const variation = getVariationByAttrs(product, varState);

  const mainImage = image ? [{mimeType: 'image', mediaItemUrl: image?.sourceUrl}] : [];
  const productVideo = productVideoUrl ? [{mimeType: 'video', mediaItemUrl: productVideoUrl}] : [];
  const gallery = galleryImages?.nodes || [];
  const getSlides = [...productVideo, ...mainImage, ...gallery];

  const {price, regularPrice, salePrice} = variation || quickView || {};

  if (loading)
    return (
      <Wrapper show={show}>
        <Loader />
      </Wrapper>
    );

  return (
    <Wrapper show={show}>
      <BackLink onClick={onClose} />
      <MainSlider slides={getSlides} autoPlay skipViewPort hidePlay />
      <Counters
        item={quickView}
        triggerLike={triggerLike}
        isLiked={isLiked}
        likes={`${totalLikes}`}
        comments={`${commentCount}`}
        // share={share}
        // bookmark={bookmark}
        showAddToList={showAddToList}
        setShowAddToList={setShowAddToList}
        showCommentsPopup={showCommentsPopup}
        setShowCommentsPopup={setShowCommentsPopup}
      />
      <Info
        databaseId={databaseId}
        goToProductPage={goToProductPage}
        name={name}
        rating={averageRating}
        reviews={reviewCount}
        price={price}
        regularPrice={regularPrice}
        salePrice={salePrice}
        coins={renderCoinsByVariation(midCoins, variation)}
        showVariationsPopup={showVariationsPopup}
        setShowVariationsPopup={setShowVariationsPopup}
        showMessage={showMessage}
        setShowMessage={setShowMessage}
        setShowSizeGuide={setShowSizeGuide}
        variations={variations}
        variation={variation}
        productCategories={productCategories}
        vars={vars}
        varState={varState}
        setVarState={setVarState}
        availableShipping={availableShipping}
      />

      <ProductAdded showMessage={showMessage} setShowMessage={setShowMessage} />

      <SizeGuide showSizeGuide={showSizeGuide} setShowSizeGuide={setShowSizeGuide} />
    </Wrapper>
  );
};

QuickViewMobile.defaultProps = {};

QuickViewMobile.propTypes = {};

export default QuickViewMobile;
