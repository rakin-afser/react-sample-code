import React, {useState} from 'react';
import {string, number, array, func, bool} from 'prop-types';

import Icon from 'components/Icon';

import Buttons from '../Buttons';
import Popup from './popup';

import {
  Wrapper,
  Name,
  Rating,
  Reviews,
  PriceBlock,
  Price,
  OldPrice,
  Sale,
  Coins,
  Delivery,
  DeliveryLabel,
  DeliveryDate,
  Divider
} from './styled';
import {getDiscountPercent} from 'util/heplers';
import Variation from './components/Variation';

const Info = ({
  postId,
  databaseId,
  name,
  rating,
  reviews,
  price,
  regularPrice,
  salePrice,
  coins,
  showVariationsPopup,
  setShowVariationsPopup,
  showMessage,
  goToProductPage,
  setShowMessage,
  setShowSizeGuide,
  variations,
  variation,
  productCategories,
  vars,
  varState,
  setVarState,
  availableShipping
}) => {
  const sale = getDiscountPercent(regularPrice, price);

  function renderDelivery() {
    const {city, available} = availableShipping || {};
    if (!available) return null;

    return (
      <Delivery>
        <DeliveryLabel>Deliver to</DeliveryLabel> <strong>{city}</strong>
        {/* <Divider />
        <span>
          <strong>Free</strong> delivery by <DeliveryDate>Sat, Nov 23</DeliveryDate>
        </span> */}
      </Delivery>
    );
  }

  return (
    <Wrapper>
      <Name onClick={goToProductPage}>{name}</Name>
      <Rating>
        <Icon type="star" svgStyle={{width: 14, height: 14, fill: rating >= 1 ? '#FFC131' : '#ccc'}} />
        <Icon type="star" svgStyle={{width: 14, height: 14, fill: rating >= 2 ? '#FFC131' : '#ccc'}} />
        <Icon type="star" svgStyle={{width: 14, height: 14, fill: rating >= 3 ? '#FFC131' : '#ccc'}} />
        <Icon type="star" svgStyle={{width: 14, height: 14, fill: rating >= 4 ? '#FFC131' : '#ccc'}} />
        <Icon type="star" svgStyle={{width: 14, height: 14, fill: rating >= 5 ? '#FFC131' : '#ccc'}} />

        {reviews ? <Reviews>({reviews})</Reviews> : null}
      </Rating>
      <PriceBlock>
        <Price>{salePrice ? price : regularPrice}</Price>
        {salePrice ? <OldPrice>{regularPrice}</OldPrice> : null}
        {salePrice && sale ? <Sale>-{sale}% off</Sale> : null}

        {coins ? (
          <Coins>
            Mid Coins: <span>+{coins}</span> <Icon type="coins" />
          </Coins>
        ) : null}
      </PriceBlock>
      {renderDelivery()}
      <Variation variations={variations} setShowVariationsPopup={setShowVariationsPopup} variation={variation} />
      {/* {Object.keys(vars || {})?.length > 0 ? ( <Variations vars={vars} setShowVariationsPopup={setShowVariationsPopup} />
      ) : null} */}
      <Buttons
        postId={postId}
        setShowVariationsPopup={setShowVariationsPopup}
        goToProductPage={goToProductPage}
        variation={variation}
        disabled={variations && !variation}
        databaseId={databaseId}
        setShowMessage={setShowMessage}
      />
      <Popup
        name={name}
        coins={coins}
        databaseId={databaseId}
        showVariationsPopup={showVariationsPopup}
        setShowVariationsPopup={setShowVariationsPopup}
        setShowMessage={setShowMessage}
        setShowSizeGuide={setShowSizeGuide}
        productCategories={productCategories}
        variations={variations}
        variation={variation}
        vars={vars}
        varState={varState}
        setVarState={setVarState}
      />
    </Wrapper>
  );
};

// Info.defaultProps = {
//   name: null,
//   subName: null,
//   rating: 0,
//   reviews: 0,
//   price: null,
//   newPrice: null,
//   coins: null,
//   colors: [],
//   sizes: [],
//   color: null,
//   size: null,
//   showVariationsPopup: false,
//   quantity: 1,
//   setColor: () => {},
//   setSize: () => {},
//   setShowVariationsPopup: () => {},
//   setShowSizeGuide: () => {},
//   setQuantity: () => {}
// };

// Info.propTypes = {
//   name: string,
//   subName: string,
//   rating: number,
//   reviews: number,
//   price: number,
//   newPrice: number,
//   coins: number,
//   colors: array,
//   sizes: array,
//   color: string,
//   size: string,
//   showVariationsPopup: bool,
//   setColor: func,
//   setSize: func,
//   setShowVariationsPopup: func,
//   setShowSizeGuide: func,
//   quantity: number,
//   setQuantity: func
// };

export default Info;
