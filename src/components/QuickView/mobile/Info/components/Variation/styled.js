import styled from 'styled-components';

export const Wrapper = styled.div`
  border-top: 1px solid #efefef;
  border-bottom: 1px solid #efefef;
  margin: 16px -16px 0;
  padding: 4px 45px 4px 16px;
  position: relative;
  display: flex;
  align-items: center;

  > i {
    position: absolute;
    top: 50%;
    right: 27px;
    transform: translateY(-50%);
  }
`;

export const Image = styled.img`
  width: 58px;
  height: 58px;
  border-radius: 12px;
  object-fit: contain;
  margin-right: 16px;
`;

export const ImagePlaceholder = styled.div`
  width: 58px;
  height: 58px;
  border-radius: 12px;
  margin-right: 16px;
  background-color: #ccc;
`;

export const Attributes = styled.div`
  display: grid;
  grid-auto-flow: column;
  grid-template-rows: 1fr 1fr;
  grid-column-gap: 10px;
`;

export const Attribute = styled.div`
  display: flex;
`;

export const Label = styled.div`
  color: #666666;
  padding-right: 10px;
`;

export const Value = styled.div`
  font-weight: 500;
  color: #000;
`;

export const ColorAttribute = styled.div`
  background-color: ${({color}) => color || 'transparent'};
  border-radius: 12px;
  width: 40px;
  height: 40px;
  grid-row: 1/3;
`;
