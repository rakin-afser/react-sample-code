import React, {useEffect, useRef} from 'react';
import Scrollbar from 'react-scrollbars-custom';
import {
  Wrapper,
  FormGroup,
  FormTitle,
  FormValues,
  FormValue,
  ColorImage,
  Color,
  ColorList,
  ColorListWrapper,
  StyledSlider,
  BlockWrapper,
  Info,
  ProductName,
  ProductPrice,
  ProductPercent,
  PriceBlock
} from './styled';
import {Coins} from 'components/QuickView/mobile/Info/styled';
import Icon from 'components/Icon';
import {getDiscountPercent} from 'util/heplers';

const PopUpContent = ({variation, coins, variations, vars, varState, setVarState, name}) => {
  const sliderRef = useRef(null);
  const {slickGoTo} = sliderRef?.current || {};

  useEffect(() => {
    if (variation && slickGoTo && variations.nodes?.length > 0) {
      const slideIndex = variations?.nodes?.findIndex(({id}) => id === variation?.id);
      slickGoTo(slideIndex);
    }
  }, [variation, slickGoTo, variations]);

  function onClick(value, label) {
    setVarState({...varState, [label]: value});
  }

  function renderPopUpContent() {
    return Object.keys(vars || {})?.map((varName) => {
      const {placeholder, options} = vars[varName];
      switch (placeholder) {
        case 'Color':
          return (
            <FormGroup key={varName}>
              <FormTitle>{placeholder}</FormTitle>
              <ColorListWrapper>
                <ColorList>
                  {options?.map((option) => (
                    <Color
                      selected={Object.values(varState)?.includes(option?.value) ? 1 : 0}
                      onClick={() => onClick(option?.value, varName)}
                      key={option?.value}
                      color={option?.label}
                      disabled={option?.disabled}
                    >
                      <Icon type="checkbox" color="#000" />
                    </Color>
                  ))}
                </ColorList>
              </ColorListWrapper>
            </FormGroup>
          );

        default:
          return (
            <FormGroup key={varName}>
              <FormTitle>{placeholder}</FormTitle>
              <FormValues>
                <Scrollbar
                  disableTracksWidthCompensation
                  style={{height: 40}}
                  trackXProps={{
                    renderer: (props) => {
                      const {elementRef, ...restProps} = props;
                      return <span {...restProps} ref={elementRef} className="TrackX" />;
                    }
                  }}
                >
                  {options?.map((option) => (
                    <FormValue
                      active={Object.values(varState)?.includes(option?.value) ? 1 : 0}
                      onClick={() => onClick(option?.value, varName)}
                      key={option?.value}
                      disabled={option?.disabled}
                    >
                      {option?.label}
                    </FormValue>
                  ))}
                </Scrollbar>
              </FormValues>
            </FormGroup>
          );
      }
    });
  }

  const withImages = variations?.nodes?.every((v) => v.image);
  const discountPercent = getDiscountPercent(variation?.regularPrice, variation?.salePrice);

  return (
    <Wrapper>
      <BlockWrapper>
        {withImages ? (
          <StyledSlider fade ref={sliderRef} infinite={false} touchMove={false}>
            {variations?.nodes?.map((v) => {
              const {id, sourceUrl, altText} = v?.image || {};
              return <ColorImage key={id} src={sourceUrl} alt={altText} />;
            })}
          </StyledSlider>
        ) : null}
        <Info>
          <ProductName>{name?.length > 60 ? `${name?.slice(0, 60)}...` : name}</ProductName>
          <PriceBlock>
            {variation?.salePrice && (
              <ProductPrice sale={variation?.salePrice}>
                {variation?.salePrice} {discountPercent && <ProductPercent>-{discountPercent}% off</ProductPercent>}
              </ProductPrice>
            )}
            <ProductPrice lineThrough={variation?.salePrice}>{variation?.regularPrice}</ProductPrice>
          </PriceBlock>
          {coins ? (
            <Coins>
              Mid Coins: <span>+{coins}</span> <Icon type="coins" />
            </Coins>
          ) : null}
        </Info>
      </BlockWrapper>
      {renderPopUpContent()}
    </Wrapper>
  );
};

export default PopUpContent;
