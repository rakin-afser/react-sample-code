import styled, {css} from 'styled-components/macro';
import {Link} from 'react-router-dom';

import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding: 0 16px 32px;
`;

export const Name = styled.p`
  color: #000000;
  margin: 0 0 7px 0;
  display: flex;
  align-items: center;
  font-size: 18px;
  font-weight: 500;
`;

export const NameValue = styled.h1`
  font-size: 16px;
  line-height: 19px;
  color: #000000;
  margin: 0;
  line-height: 1;
  margin-right: 15px;
`;

export const SubName = styled.p`
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 19px;
  color: #000000;
  margin: 0 0 9px;
  letter-spacing: 0.4px;
`;

export const Rating = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;

  i {
    margin-right: 2px;
  }
`;

export const Reviews = styled.span`
  display: inline-flex;
  padding-left: 6px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 1;
  color: #7a7a7a;
`;

export const PriceBlock = styled.div`
  display: flex;
  flex-shrink: 0;
  align-items: center;
  margin: 0 0 0 auto;
  align-items: flex-end;
`;

export const Price = styled.span`
  font-weight: 500;
  font-size: 18px;
  color: #555;
  letter-spacing: 0;
  line-height: 20px;

  small {
    font-size: 10px;
    font-weight: normal;
    position: relative;
    left: 4px;
  }

  ${({onSale}) =>
    onSale &&
    css`
      font-weight: 700;
      color: ${primaryColor};
    `}
`;

export const OldPrice = styled.span`
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  text-decoration-line: line-through;
  color: #999999;
  margin-left: 10px;
  letter-spacing: 0;

  small {
    font-size: 10px;
    line-height: 10px;
  }
`;

export const Sale = styled.span`
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  line-height: 1;
  color: ${primaryColor};
  margin-left: auto;
`;

export const Coins = styled.span`
  margin-left: auto;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 1;
  text-align: right;
  color: #398287;
  position: relative;
  top: 4px;
  letter-spacing: -0.2px;
  display: flex;
  align-items: center;

  span {
    font-size: 14px;
  }

  svg {
    stroke: #398287;
    position: relative;
    top: 1px;
  }

  i {
    margin-left: 4px;
    position: relative;
    top: -2px;
  }
`;

export const CoinsLabel = styled.span`
  margin-right: auto;
  font-weight: 500;
  font-size: 10px;
  line-height: 12px;
  color: #000000;
`;

export const Delivery = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #000000;
  display: flex;
  align-items: baseline;
  margin: 22px 0 0;

  span {
    font-size: 12px;
  }
`;

export const DeliveryLabel = styled.div`
  margin-right: 4px;
`;

export const DeliveryDate = styled.strong`
  color: #2a6924;
`;

export const Divider = styled.div`
  margin-left: auto;
`;

export const PopupOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 100;
  background: rgba(0, 0, 0, 0.4);
  transition: all 0.3s ease;

  opacity: ${({active}) => (active ? 1 : 0)};
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
`;

export const PopupWrapper = styled.div`
  position: fixed;
  left: 10px;
  right: 10px;
  bottom: 10px;
  border-radius: 12px;
  z-index: 101;
  background: #fff;
  transition: all 0.3s ease;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  max-width: 100vw;
  padding: 17px;

  opacity: ${({active}) => (active ? 1 : 0)};
  transform: translateY(${({active}) => (active ? '0px' : '50px')});
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
`;

export const IconWrapper = styled.div`
  position: absolute;
  top: -3px;
  right: 0;
  z-index: 110;
  cursor: pointer;
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
