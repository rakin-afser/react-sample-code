import React from 'react';

import Icon from 'components/Icon';
import Buttons from '../Buttons';
import PopUpContent from './components/PopUpContent';

import {PopupOverlay, PopupWrapper, IconWrapper} from './styled';

const Popup = ({
  databaseId,
  showVariationsPopup,
  setShowVariationsPopup,
  setShowMessage,
  setShowSizeGuide,
  variations,
  variation,
  vars,
  varState,
  setVarState,
  name,
  coins
}) => {
  return (
    <>
      <PopupOverlay active={showVariationsPopup} onClick={() => setShowVariationsPopup(false)} />
      <PopupWrapper active={showVariationsPopup}>
        <IconWrapper onClick={() => setShowVariationsPopup(false)}>
          <Icon type="close" svgStyle={{width: 24, height: 24, color: '#1A1A1A'}} />
        </IconWrapper>
        <PopUpContent
          name={name}
          coins={coins}
          variation={variation}
          variations={variations}
          vars={vars}
          varState={varState}
          setVarState={setVarState}
        />
        <Buttons
          showBuyNow
          variation={variation}
          disabled={variations && !variation}
          databaseId={databaseId}
          style={{marginTop: 0}}
          setShowMessage={setShowMessage}
        />
      </PopupWrapper>
    </>
  );
};

export default Popup;
