import React from 'react';
import {func} from 'prop-types';

import Icon from 'components/Icon';
import {Wrapper} from './styled';

const BackLink = ({onClick}) => {
  return (
    <Wrapper onClick={onClick}>
      <Icon type="arrowBack" />
    </Wrapper>
  );
};

BackLink.defaultProps = {
  onClick: () => {}
};

BackLink.propTypes = {
  onClick: func
};

export default BackLink;
