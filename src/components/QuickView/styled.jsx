import styled from 'styled-components';

export const Wrapper = styled.div`
  position: fixed;
  top: ${({show}) => show ? '0' : '-50%'};
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1100;
  background: #fff;
  overflow-x: hidden;
  overflow-y: auto;
  opacity: ${({show}) => show ? '1' : '0'};
  pointer-events: ${({show}) => show ? 'all' : 'none'};
`;