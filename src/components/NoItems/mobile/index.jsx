import React from 'react';
import {Wrap, Title, Desc} from './styled';

const NoItems = ({icon, title, desc}) => {
  return (
    <Wrap>
      {icon}
      <Title>{title}</Title>
      <Desc>{desc}</Desc>
    </Wrap>
  );
};

export default NoItems;
