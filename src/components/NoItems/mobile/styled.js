import styled from 'styled-components';

export const Wrap = styled.div`
  margin-top: 90px;
  text-align: center;
`;

export const Title = styled.h5`
  font-weight: 500;
  font-size: 16px;
  line-height: 1.18;
  margin-bottom: 7px;
  color: #000000;
  padding-top: 65px;
`;

export const Desc = styled.p`
  color: #999999;
  max-width: 235px;
  margin: 0 auto;
`;
