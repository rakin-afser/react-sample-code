import React from 'react';
import PropTypes from 'prop-types';
import CommentsComponent from 'components/Comments';
import ExpandedText from 'components/ExpandedText';
import Tags from 'components/Tags';
import {Container, HeadText, PostText} from './styled';

const PostContent = ({withComments, changeCommentsDsiplaying, data}) => (
  <Container withComments={withComments}>
    <HeadText>{data.title}</HeadText>
    <PostText>
      <ExpandedText text={data.description} textAlign="left" divider={135} />
    </PostText>
    <Tags styles={{marginBottom: '6px'}} />
    {withComments && <CommentsComponent hideComments={changeCommentsDsiplaying} />}
  </Container>
);

PostContent.defaultProps = {
  withComments: false,
  changeCommentsDsiplaying: (f) => f
};

PostContent.propTypes = {
  withComments: PropTypes.bool,
  changeCommentsDsiplaying: PropTypes.func,
  data: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.any), PropTypes.shape()]).isRequired
};

export default PostContent;
