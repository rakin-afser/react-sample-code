import styled from 'styled-components/macro';

export const Container = styled.div`
  margin: 25px 0 0 25px;
  margin-bottom: ${({withComments}) => (!withComments ? '300px' : '5px')};
`;

export const HeadText = styled.h2`
  font-weight: 600;
  font-size: 20px;
  line-height: 124%;
  margin-bottom: 14px;
  max-width: 350px;
`;

export const PostText = styled.p`
  font-weight: normal;
  font-size: 14px;
  margin-bottom: 16px;
`;

export const SeeMore = styled.span`
  color: #ed494f;
  cursor: pointer;
`;
