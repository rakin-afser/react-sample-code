import React from 'react';
import {Select, Option, Label, Wrap, WrapInner, ChevronDown} from './styled';
import Icon from 'components/Icon';

const SelectNative = (props) => {
  const {label, options} = props;
  return (
    <Wrap>
      <Label>{label}</Label>
      <WrapInner>
        <ChevronDown type="chevronDown" color="#666" />
        <Select>
          {options?.map((option) => (
            <Option key={option.value} value={option.value}>
              {option.label}
            </Option>
          ))}
        </Select>
      </WrapInner>
    </Wrap>
  );
};

export default SelectNative;
