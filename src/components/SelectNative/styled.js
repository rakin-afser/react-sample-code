import styled from 'styled-components';
import Icon from 'components/Icon';

export const Wrap = styled.div`
  display: flex;
  flex-direction: column;
`;

export const WrapInner = styled.div`
  background-color: #fff;
  position: relative;
  z-index: 1;
`;

export const ChevronDown = styled(Icon)`
  position: absolute;
  top: 50%;
  transform: translateY(-50%);
  right: 15px;
  z-index: -1;
`;

export const Select = styled.select`
  outline: none;
  border-radius: 2px;
  color: #000;
  font-weight: 700;
  border: 1px solid #a7a7a7;
  height: 40px;
  padding: 0 15px;
  appearance: none;
  background-color: transparent;
  width: 100%;
`;

export const Label = styled.label`
  font-weight: 500;
  color: #000;
  margin-bottom: 9px;
`;

export const Option = styled.option`
  color: #000;
`;
