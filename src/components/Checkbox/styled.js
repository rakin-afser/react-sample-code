import styled from 'styled-components';
import {Checkbox} from 'antd';

const Wrapper = styled(Checkbox)`
  &&&& {
    ${({customStyles}) => customStyles || null}
  }
  &&& {
    font-family: 'Helvetica Neue', sans-serif;
    color: #000000;
    display: flex;
    align-items: center;
    .ant-checkbox-checked .ant-checkbox-inner {
      background-color: #000;
    }
    .ant-checkbox {
      margin-right: 4px;
      input {
        width: 20px;
        height: 20px;
      }
      .ant-checkbox-inner {
        border-color: #000;
        border-radius: 4px;
        width: 20px;
        height: 20px;
      }
      .ant-checkbox-inner::after {
        left: 30%;
      }
    }
    .ant-checkbox:after {
      border: none;
    }
  }
`;

export default Wrapper;
