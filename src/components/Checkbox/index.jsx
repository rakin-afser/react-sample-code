import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './styled';

const Checkbox = ({children, customStyles, ...props}) => {
  return (
    <Wrapper {...props} customStyles={customStyles}>
      {children}
    </Wrapper>
  );
};

Checkbox.defaultProps = {
  children: '',
  customStyles: ''
};

Checkbox.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string, PropTypes.object]),
  customStyles: PropTypes.string
};

export default Checkbox;
