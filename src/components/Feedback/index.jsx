import React from 'react';
import moment from 'moment';

import {
  Container,
  UserAvatar,
  Wrap,
  WrapSecond,
  Header,
  Item,
  ItemCustom,
  UserName,
  FollowBtn,
  InfluenceBtn,
  Text,
  Date,
  Settings,
  Dot,
  Example,
  HelpBtn,
  Reply,
  FeedbackItem,
  Plus,
  Send,
  AddComment,
  Star,
  ExampleLast,
  HoverBlock,
  IconWrapper,
  IconsGroup,
  StatusBadge
} from './styled';

import pic from './img/feedback-avatar.png';
import pic2 from './img/example.png';
import heart from './img/heart.svg';
import {ReactComponent as Heart} from './img/heart2.svg';
import plus from './img/plus.svg';
import star2 from './img/star_konic.svg';
import {InputMessage, UserPic} from 'components/Comments/styled';
import myAvatar from 'components/Comments/img/avatar1.png';
import send from 'components/Comments/img/send.svg';
import {ReactComponent as SubmitReview} from 'images/svg/submit-review.svg';
import MessageIcon from 'assets/MessageIcon';
import userPlaceholder from 'images/placeholders/user.jpg';

const Feedback = ({data, setRating, isForShopPage = false}) => {
  return (
    <Container isForShopPage={isForShopPage}>
      {data?.map((item, index) => {
        const {authorId, authorNicename, content, date, followed, id, image, rating, title} = item || {};

        return (
          <FeedbackItem key={index}>
            <Header>
              <Item flex>
                <Wrap>
                  <StatusBadge status="online" size="large" offset={[-42, 53]}>
                    <UserAvatar src={image || userPlaceholder} />
                  </StatusBadge>
                </Wrap>
                <WrapSecond>
                  <UserName>{authorNicename}</UserName>
                  <InfluenceBtn>
                    <Star src={star2} alt="star" />
                    Influencer
                  </InfluenceBtn>
                  <FollowBtn>
                    <Plus src={plus} />
                    Follow
                  </FollowBtn>
                </WrapSecond>
              </Item>
              <ItemCustom isForShopPage={isForShopPage}>
                <Wrap>
                  <div>{setRating(rating || 5)}</div>
                  <Date>{moment(date).format('DD MMM YYYY')}</Date>
                </Wrap>
                <Text block>
                  <span dangerouslySetInnerHTML={{__html: content}} />
                </Text>
              </ItemCustom>
              <Item flex>
                <Settings>
                  <Dot />
                </Settings>
              </Item>
            </Header>
            {/* <Item flex center>
              <Example src={pic2} />
              <Example src={pic2} />
              <ExampleLast>
                <HoverBlock>+3</HoverBlock>
                <Example src={pic2} />
              </ExampleLast>
            </Item> */}
            {/* <IconsGroup>
              <IconWrapper>
                <Heart />
                <Text>653</Text>
              </IconWrapper>
              <IconWrapper>
                <MessageIcon color="#EA2C34" />
                <Text>546</Text>
              </IconWrapper>
            </IconsGroup> */}
          </FeedbackItem>
        );
      })}

      {/* <Reply>
        Reply to
        <Text left>@anna_smith</Text>
      </Reply>
      <AddComment isForShopPage={isForShopPage}>
        <UserPic src={myAvatar} />
        <InputMessage placeholder="Add a comment" />
        <Send>
          <SubmitReview />
        </Send>
      </AddComment> */}
    </Container>
  );
};

export default Feedback;
