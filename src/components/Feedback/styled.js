import {Badge, Button} from 'antd';
import styled from 'styled-components/macro';

export const Container = styled.section`
  width: 1014px;
  border-top: 1px solid #e4e4e4;
  margin: 24px auto 0 auto;
  ${({isForShopPage}) => (isForShopPage ? 'max-width: 868px; width: 100%;' : null)};
`;

export const FeedbackItem = styled.div`
  padding-top: 42px;
  padding-bottom: 40px;
  border-bottom: 1px solid #e4e4e4;
`;

export const Wrap = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${({bottom}) => (bottom ? '17px' : '0px')};
  justify-content: space-between;

  & svg {
    margin-right: 4px;
  }
`;

export const WrapSecond = styled.div``;

export const UserAvatar = styled.img`
  position: relative;
  margin-right: 32px;
  width: 62px;
  height: 62px;
  border-radius: 50%;

  &::before {
    content: '';
    position: absolute;
    width: 14px;
    height: 14px;
    background: #c3c3c3;
    border: 2px solid #ffffff;
    border-radius: 50%;
    right: 0;
    bottom: 0;
  }
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 14px;
`;

export const Item = styled.div`
  display: ${({flex}) => (flex ? 'flex' : 'block')};
  align-items: ${({center}) => (center ? 'center' : 'flex-start')};
  justify-content: ${({spaceBetween}) => (spaceBetween ? 'space-between' : 'flex-start')};
  margin-left: ${({center}) => (center ? '292px' : '0')};
`;

export const ItemCustom = styled.div`
  display: ${({flex}) => (flex ? 'flex' : 'block')};
  align-items: ${({center}) => (center ? 'center' : 'flex-start')};
  justify-content: ${({spaceBetween}) => (spaceBetween ? 'space-between' : 'flex-start')};
  margin-left: ${({isForShopPage}) => (isForShopPage ? '97px' : '0')};
  flex-grow: 1;
`;

export const UserName = styled.span`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const FollowBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 99px;
  height: 28px;
  background: transparent;
  border: 1px solid #ed484f;
  border-radius: 24px;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #ed484f;
  cursor: pointer;
  margin-top: 16px;
`;

export const Plus = styled.img`
  margin-right: 11px;
`;

export const InfluenceBtn = styled.span`
  max-width: 85px;
  height: 20px;
  background: #e5f1ff;
  border-radius: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #1b5dab;
  margin-top: 8px;
  margin-bottom: 5px;
`;

export const Star = styled.img`
  display: inline-block;
  margin-bottom: 2px;
  margin-right: 4px;
`;

export const Text = styled.span`
  display: ${({block}) => (block ? 'block' : 'inline')};
  max-width: 600px;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: ${({link}) => (link ? '#ED484F' : '#545454')};
  margin-left: ${({left}) => (left ? '8px' : '0px')};
  margin-top: ${({block}) => (block ? '17px' : '0px')};
`;

export const IconWrapper = styled.div`
  display: flex;
  align-items: center;

  & + & {
    margin-left: 23px;
  }

  svg {
    margin-right: 6px;
  }
`;

export const IconsGroup = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const Date = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
`;

export const Settings = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 32px;
  height: 34px;
  margin-left: 12px;
  outline: none;
  background: transparent;
  border: none;
  text-decoration: none;
  cursor: pointer;
  margin-top: -8px;
`;

export const Dot = styled.i`
  position: relative;
  width: 4px;
  height: 4px;
  background: #7c7e82;
  border-radius: 50%;

  &::before {
    position: absolute;
    bottom: calc(100% + 3.5px);
    left: 0;
    content: '';
    width: 4px;
    height: 4px;
    background: #7c7e82;
    border-radius: 50%;
  }

  &::after {
    position: absolute;
    top: calc(100% + 3.5px);
    left: 0;
    content: '';
    width: 4px;
    height: 4px;
    background: #7c7e82;
    border-radius: 50%;
  }
`;

export const Example = styled.img`
  margin: 12px 4px 21px 0;
  width: 50px;
  height: 50px;
  position: relative;
  display: inline-block;
  ::after {
    position: absolute;
    top: 0;
    left: 0;
    height: 50px;
    width: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 99;
    background: rgba(0, 0, 0, 0.5);
    content: '+3';
    font-size: 16px;
    color: white;
  }
`;

export const ExampleLast = styled.div`
  margin: 12px 4px 21px 0;
  width: 50px;
  height: 50px;
  position: relative;
  display: inline-block;
  & img {
    width: 100%;
    margin: 0;
  }
`;

export const HoverBlock = styled.div`
  background: rgba(0, 0, 0, 0.7);
  position: absolute;
  z-index: 999;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  display: flex;
  align-items: center;
  text-align: center;
  justify-content: center;
  color: #ffffff;
`;

export const HelpBtn = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 28px;
  background: #e4e4e4;
  border: none;
  box-sizing: border-box;
  border-radius: 24px;
  cursor: pointer;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #545454;
  margin-right: 12px;
  padding: 0 10px;
`;

export const Reply = styled.div`
  border-bottom: 1px solid #e4e4e4;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
  padding: 12px 16px;
`;

export const Send = styled(Button)`
  &&& {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    width: 40px;
    height: 40px;
    border: none;
    box-sizing: border-box;
    cursor: pointer;
  }
`;

export const AddComment = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 8px 16px;
  border-bottom: 1px solid #e4e4e4;
  margin-bottom: 69px;
  ${({isForShopPage}) => (isForShopPage ? 'margin-bottom: 0; padding: 8px 16px 25px;' : null)};
`;

export const StatusBadge = styled(Badge)`
  .ant-badge-dot {
    border: 2px solid #ffffff;
    background: ${({status}) => (status === 'online' ? '#2ecc71' : '#C3C3C3')};
    width: 14px;
    height: 14px;
  }
`;
