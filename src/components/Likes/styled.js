import styled from 'styled-components';

export const LikesWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const LikesIcon = styled.div`
  cursor: pointer;
  ${({styles}) => styles}
  
  svg.iconHeart {
    display: ${({isLiked}) => (isLiked ? 'none' : 'block')};
  }

  svg.iconFilledHeart {
    display: ${({isLiked}) => (isLiked ? 'block' : 'none')};
  }

  &:hover {
    svg.iconHeart {
      display: none;
    }

    svg.iconFilledHeart {
      display: block;
    }
`;

export const LikesCnt = styled.span`
  margin-left: 5.5px;
  margin-top: 1px;
  user-select: none;
`;
