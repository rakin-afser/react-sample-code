import React from 'react';
import PropTypes from 'prop-types';
import FilledHeart from 'assets/FilledHeart';
import Heart from 'assets/Heart';

import {LikesWrapper, LikesIcon, LikesCnt} from 'components/Likes/styled';
import {userId} from 'util/heplers';

const Likes = ({likesCount, isLiked, showCount, width, height, iconStyles, triggerLike = () => {}, ...props}) => {
  const onClick = () => {
    if (userId) {
      triggerLike();
    }
  };

  return (
    <LikesWrapper {...props}>
      <LikesIcon isLiked={isLiked} onClick={onClick} styles={iconStyles}>
        <FilledHeart isLiked={false} width={width} height={height} />
        <Heart isLiked={!!isLiked} width={width} height={height} />
      </LikesIcon>
      {showCount && likesCount ? <LikesCnt>{likesCount}</LikesCnt> : null}
    </LikesWrapper>
  );
};

Likes.defaultProps = {
  showCount: false,
  isLiked: false,
  width: 16,
  height: 15,
  iconStyles: null
};

Likes.propTypes = {
  likesCount: PropTypes.number.isRequired,
  isLiked: PropTypes.bool,
  showCount: PropTypes.bool,
  width: PropTypes.number,
  height: PropTypes.number,
  iconStyles: PropTypes.shape()
};

export default Likes;
