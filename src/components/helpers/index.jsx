import React from 'react';
import Star from '../../assets/ProductPage/Star';

export const setRating = (value) => {
  let Rating = [];
  for (let i = 0; i < value; i++) {
    Rating.push(<Star key={i} />);
  }
  if (value < 5) {
    for (let i = 0; i < 5 - value; i++) {
      Rating.push(<Star key={`${i}a`} color="#efefef" />);
    }
  }
  return Rating;
};
