import React, {useState} from 'react';
import useGlobal from 'store';
import {CSSTransition} from 'react-transition-group';
import Main from 'components/Messenger/components/MainNew';
import {Wrapper} from './styled';

const Messenger = () => {
  const [globalState, setGlobalState] = useGlobal();
  const {messenger} = globalState;

  const onShow = () => {
    setGlobalState.setMessenger({visible: true, minify: false, data: messenger.data});
  };

  const onMinify = () => {
    if (!messenger.minify) {
      setGlobalState.setMessenger({visible: true, minify: true, data: messenger.data});
    } else {
      setGlobalState.setMessenger({visible: true, minify: false, data: messenger.data});
    }
  };

  const onClose = () => {
    setGlobalState.setMessenger({visible: false, minify: false, data: messenger.data});
  };

  return (
    // <CSSTransition in={messenger.visible === 'visible'} timeout={1500}>
    <Wrapper visible={messenger.visible} minify={messenger.minify}>
      <Main onClose={onClose} onShow={onShow} onMinify={onMinify} />
    </Wrapper>
    // </CSSTransition>
  );
};

Messenger.defaultProps = {};

Messenger.propTypes = {};

export default Messenger;
