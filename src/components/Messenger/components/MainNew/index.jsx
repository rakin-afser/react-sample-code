import React, {useRef, useEffect, useState, useCallback, useMemo} from 'react';
import Talk from 'talkjs';
import PropTypes from 'prop-types';
import {useUser} from 'hooks/reactiveVars';
import Icons from 'components/Icon';
import {
  MainWrapper,
  Body,
  AvatarWrapper,
  CloseBtn,
  MinifyBtn,
  Header,
  UserFollowers,
  UserName,
  UserNameWrapper
} from 'components/Messenger/styled';
import Avatar from 'components/Avatar';
import {abbreviateToNumber} from 'util/heplers';
import useGlobal from 'store';

const Main = ({onShow, onMinify, onClose}) => {
  const [globalState] = useGlobal();
  const [user] = useUser();
  const talkJsContainer = useRef();
  const {data, visible} = globalState.messenger;

  const onShowClose = () => {
    onMinify();
  };

  const onCloseAction = () => {
    onClose();
  };

  useEffect(() => {
    Talk.ready.then(() => {
      let inbox;

      if (data) {
        const other = new Talk.User({
          id: data.databaseId,
          name: data.username,
          email: 'use1@emal.com',
          photoUrl: data.profile_picture,
          role: 'customer'
        });

        const conversation = window.talkJsSession.getOrCreateConversation(
          Talk.oneOnOneId(window.talkJsSession.me, other)
        );

        conversation.setParticipant(window.talkJsSession.me);
        conversation.setParticipant(other);

        inbox = window.talkJsSession.createInbox({selected: conversation, showChatHeader: false});
      } else {
        if (window.talkJsSession && window.talkJsSession.createInbox)
          inbox = window.talkJsSession.createInbox({showChatHeader: false});
      }
      const {mount} = inbox || {};
      if (mount) {
        mount(talkJsContainer?.current);
      }
    });
  }, [data, user]);

  return (
    <MainWrapper>
      <Header>
        <AvatarWrapper>
          <Avatar img={user?.profile_picture} width="32" height="32" isOnline />
        </AvatarWrapper>
        <UserNameWrapper>
          <UserName>{user?.email}</UserName>
        </UserNameWrapper>
        <MinifyBtn onClick={onShowClose}>
          <span>&nbsp;</span>
        </MinifyBtn>
        <CloseBtn onClick={onCloseAction}>
          <Icons type="close" />
        </CloseBtn>
      </Header>
      <Body ref={talkJsContainer} />
    </MainWrapper>
  );
};

Main.propTypes = {
  onShow: PropTypes.func.isRequired,
  onMinify: PropTypes.func.isRequired
};

export default Main;
