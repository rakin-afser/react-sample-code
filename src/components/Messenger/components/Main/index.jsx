import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {CSSTransition} from 'react-transition-group';
import Scrollbars from 'react-scrollbars-custom';
import {
  AvatarWrapper,
  Body,
  BtnSend,
  CloseBtn,
  MinifyBtn,
  EmojiBtn,
  Footer,
  Header,
  InputWrapper,
  UserFollowers,
  UserName,
  EmojiWrapper
  // CloseImojiBtn
} from 'components/Messenger/styled';
import Avatar from 'components/Avatar';
import {messagesData, user2} from 'components/Messenger/exampleData';
import {abbreviateToNumber} from 'util/heplers';
import Icons from 'components/Icon';
import Message from 'components/Messenger/components/Message';
import Emoji from 'components/Emoji';
import avatar1 from 'images/avatar3.png';
import {Textarea} from 'components/Textarea/styled';

const Main = ({onClose, onMinify}) => {
  const [messages, setMessages] = useState(messagesData || []);
  const [input, setInput] = useState('');
  const [showEmoji, setShowEmoji] = useState(false);

  const onSend = () => {
    const newMessage = {
      id: Date.now(),
      user: 'jeffree_star',
      avatar: avatar1,
      text: input,
      time: Date.now(),
      watched: false,
      me: true
    };

    setMessages((prev) => [...prev, newMessage]);
    setInput('');
  };

  const onChange = (e) => {
    setInput(e.target.value);
  };

  return (
    <>
      <Header>
        <AvatarWrapper>
          <Avatar img={user2.avatar} width="60" height="60" isOnline />
        </AvatarWrapper>
        <div>
          <UserName>{user2.name}</UserName>
          <UserFollowers>{abbreviateToNumber(user2.followers)} followers</UserFollowers>
        </div>
        <MinifyBtn onClick={onMinify}>-</MinifyBtn>
        <CloseBtn onClick={onClose}>
          <Icons type="close" />
        </CloseBtn>
      </Header>
      <Body>
        {!showEmoji && (
          <Scrollbars
            clientwidth={4}
            noDefaultStyles={false}
            noScroll={false}
            style={{height: '100%', paddingRight: 13}}
            thumbYProps={{className: 'thumbY'}}
          >
            <div style={{paddingRight: 10}}>
              {messages.map((el) => (
                <Message key={el.id} data={el} />
              ))}
            </div>
          </Scrollbars>
        )}
      </Body>
      <Footer>
        <Icons type="smallClip" width={24} height={24} color="#999999" />
        <InputWrapper>
          <Textarea rows={2} onChange={onChange} />
          <BtnSend onKeyDown={onSend} onClick={onSend}>
            <Icons type="send" />
          </BtnSend>
          <EmojiBtn onClick={() => setShowEmoji(true)}>
            <Icons type="smile" />
          </EmojiBtn>
        </InputWrapper>
      </Footer>
      <CSSTransition classNames="fade" in={showEmoji} timeout={300} unmountOnExit>
        <EmojiWrapper>
          {/*<CloseImojiBtn>*/}
          {/*  <Icons type="cross" color="#000" />*/}
          {/*</CloseImojiBtn>*/}
          <Emoji />
        </EmojiWrapper>
      </CSSTransition>
    </>
  );
};

Main.propTypes = {
  onClose: PropTypes.func.isRequired,
  onMinify: PropTypes.func.isRequired
};

export default Main;
