import React, {useEffect, useCallback, useState, useMemo} from 'react';
import {useUser} from 'hooks/reactiveVars';
import Talk from 'talkjs';
import {useWindowSize} from '@reach/window-size';
import Messenger from 'components/Messenger/index';

const TalkJsSession = ({children}) => {
  const [user] = useUser();
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  useEffect(() => {
    if (user) {
      Talk.ready.then(() => {
        const me = new Talk.User({
          id: user.databaseId,
          name: user?.email,
          email: 'use1@emal.com',
          photoUrl: user.profile_picture,
          role: 'customer'
        });

        window.talkJsSession = new Talk.Session({
          appId: 'tbQFbpY2',
          me
        });

        window.talkJsSession.unreads.on('change', (unreadConversations) => {
          const unreadCount = unreadConversations.length;
        });
      });
    }
  }, [user]);

  return (
    <>
      {children}
      {!isMobile && user && <Messenger />}
    </>
  );
};

export default TalkJsSession;
