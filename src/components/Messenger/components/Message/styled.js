import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const AvatarWrap = styled.div`
  margin-right: ${({me}) => (me ? '16px' : '0')};
  margin-left: ${({me}) => (me ? '0' : '10px')};
  order: ${({me}) => (me ? 0 : 1)};
`;

export const ContentWrap = styled.div`
  flex: 1;
`;

export const UserName = styled.h4`
  margin: 0 0 4px 0;
  padding: 0;
  text-align: ${({me}) => (me ? 'left' : 'right')};
  font-weight: 500;
  font-size: 14px;
  color: #000;
`;

export const Content = styled.div`
  margin-bottom: 6px;
  padding: 12px 8px 12px 12px;
  background: ${({me}) => (me ? 'rgba(229, 241, 255, 0.6)' : 'rgba(241, 241, 241, 0.6)')};
  border-radius: 4px 4px 0 4px;
  font-size: 14px;
  line-height: 140%;
  color: #000;
`;

export const TimeWrapper = styled.div`
  margin-bottom: 24px;
  display: flex;
`;

export const Time = styled.time`
  display: block;
  text-align: ${({me}) => (me ? 'right' : 'left')};
  font-size: 12px;
  color: #999999;
`;
