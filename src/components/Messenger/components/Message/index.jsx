import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Avatar from 'components/Avatar';
import FadeOnMount from 'components/Transitions';
import Icons from 'components/Icon';
import {Wrapper, ContentWrap, UserName, AvatarWrap, Content, TimeWrapper, Time} from './styled';

const Message = ({data}) => {
  return (
    <FadeOnMount>
      <Wrapper>
        <AvatarWrap me={data.me ? 1 : 0}>
          <Avatar img={data.avatar} width="36" height="36" />
        </AvatarWrap>
        <ContentWrap>
          <UserName me={data.me ? 1 : 0}>{data.user}</UserName>
          <Content me={data.me ? 1 : 0}>{data.text}</Content>
          <TimeWrapper>
            <Icons type="doubleCheck" />
            <Time me={data.me ? 1 : 0} dateTime={data.time}>
              {moment(data.time).format('LL')}
            </Time>
          </TimeWrapper>
        </ContentWrap>
      </Wrapper>
    </FadeOnMount>
  );
};

Message.propTypes = {
  data: PropTypes.shape().isRequired
};

export default Message;
