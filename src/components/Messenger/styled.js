import styled from 'styled-components/macro';
import {secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';
import media from 'constants/media';

export const Wrapper = styled.div`
  position: fixed;
  left: 15vw;
  bottom: 10px;
  width: 334px;
  height: 426px;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 8px 8px 0 0;
  transform: translateY(91%);
  z-index: 1;
  opacity: 0;
  transition: ease 0.9s;

  ${({visible}) =>
    visible &&
    `
    opacity: 1;
  `};

  ${({minify}) => (minify ? 'transform: translateY(0)' : 'transform: translateY(91%);')};

  //&.enter {
  //  transition: ease 1s;
  //}
  //
  //&.enter-active {
  //  transition: ease 1s;
  //  transform: translateY(0);
  //  opacity: 1;
  //}
  //
  //&.enter-done {
  //  transform: translateY(0);
  //  opacity: 1;
  //}
  //
  //&.exit {
  //  transition: ease 1s;
  //  opacity: 1;
  //  transform: translateY(0);
  //}
  //
  //&.exit-active {
  //  transition: ease 1s;
  //  transform: translateY(91%);
  //}
  //
  //&.exit-done {
  //}

  @media (max-width: ${media.mobileMax}) {
    padding: 0 16px 0 16px;
    position: relative;
    bottom: auto;
    right: auto;
    left: auto;
    top: 0;
    transform: translateY(0);
    width: 100%;
    height: calc(100% - 41px);
    box-shadow: none;
    border-radius: 0;
    background: transparent;
    opacity: 1;
  }
`;

export const MainWrapper = styled.div`
  @media (max-width: ${media.mobileMax}) {
    height: 100%;
  }
`;

export const Header = styled.div`
  padding: 18px 20px;
  position: relative;
  display: flex;
  align-items: center;
  height: 48px;
  border-bottom: 1px solid #e4e4e4;

  @media (max-width: ${media.mobileMax}) {
    display: none;
  }
`;

export const AvatarWrapper = styled.div`
  margin-right: 20px;

  & .avatar-online-icon {
    width: 11px;
    height: 11px;
    bottom: -1px;
    right: -3px;
  }
`;

export const UserName = styled.h4`
  margin: 0;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const UserNameWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const UserFollowers = styled.h4`
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #464646;
`;

export const CloseBtn = styled.button`
  position: absolute;
  right: 17px;
  top: 12px;

  svg {
    path {
      fill: #000;
      transition: ease 0.4s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const MinifyBtn = styled.button`
  position: absolute;
  right: 52px;
  top: 15px;
  display: flex;
  align-items: flex-end;
  height: 13px;
  justify-content: center;
  font-size: 36px;
  line-height: 0.8;
  color: #000;
  transition: ease 0.4s;
  cursor: pointer;

  span {
    user-select: none;
    display: block;
    width: 12px;
    height: 2px;
    border-radius: 1px;
    background-color: #000;
    transition: ease 0.4s;
  }

  &:hover {
    span {
      background-color: ${primaryColor};
    }
  }
`;

export const Body = styled.div`
  position: relative;
  padding: 10px 7px 10px 7px;
  overflow: hidden;
  height: 378px;

  @media (max-width: ${media.mobileMax}) {
    padding: 0;
    height: 100%;
  }
`;

export const Footer = styled.div`
  padding: 17px 20px;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  border-top: 1px solid #e4e4e4;
`;

export const InputWrapper = styled.div`
  position: relative;
  flex: 1;

  input {
    position: absolute;
    width: 100%;
  }
`;

export const EmojiWrapper = styled.div`
  position: relative;
`;

export const BtnSend = styled.button`
  position: absolute;
  right: 8px;
  top: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  background-color: #ccc;
  border-radius: 50%;
  transition: ease 0.5s;

  &:hover {
    background-color: ${primaryColor};
  }
`;

export const EmojiBtn = styled.button`
  position: absolute;
  right: 30px;
  top: 50%;
  transition: ease 0.5s;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    svg {
      path {
        stroke: ${primaryColor};
      }
    }
  }
`;

export const CloseImojiBtn = styled.button``;
