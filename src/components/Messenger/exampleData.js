import avatar2 from 'images/avatar2.png';
import avatar1 from 'images/avatar3.png';

export const messagesData = [
  {
    id: 1,
    user: 'jeffree_star',
    avatar: avatar1,
    text: 'Not much... just browsing some webistes and trying to find some interesting things',
    time: 'March 17, 2021 03:24:00',
    watched: true,
    me: true
  },
  {
    id: 2,
    user: 'davidbowie',
    avatar: avatar2,
    text: 'Hey Leroy!',
    time: 'March 17, 2021 08:24:00',
    watched: true,
    me: false
  },
  {
    id: 3,
    user: 'davidbowie',
    avatar: avatar2,
    text: 'What’s up?',
    time: 'March 17, 2021 08:24:00',
    watched: true,
    me: false
  }
];

export const user1 = {
  name: 'jeffree_star',
  avatar: avatar2
};

export const user2 = {
  name: 'jeffree_star',
  avatar: avatar1,
  followers: 2100
};
