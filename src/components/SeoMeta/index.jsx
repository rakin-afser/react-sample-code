import React from 'react';
import {Helmet} from 'react-helmet';

const SeoMeta = ({title = '', image = ''}) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={title} />

      <meta property="og:title" content={title} />
      <meta property="og:description" content={title} />
      <meta property="og:image" content={image} />

      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={title} />
      <meta property="twitter:image" content={image} />
    </Helmet>
  );
};

export default SeoMeta;
