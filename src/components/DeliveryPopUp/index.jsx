import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Select from 'components/SelectTrue';
import {DeliveryPopUpContainer, SaveButton, CancelButton, ButtonContainer} from './styled';

const deliveryCountry = [
  {value: 'bahrain', label: 'Bahrain'},
  {value: 'ukraine', label: 'Ukraine'}
];

const deliveryCity = [
  {value: 'manama', label: 'Manama'},
  {value: 'zaporizhya', label: 'Zaporizhya'}
];

const deliveryArea = [
  {value: 'area1', label: 'Area1'},
  {value: 'area2', label: 'Area2'}
];

const DeliveryPopUp = ({onClick}) => {
  const [selectedCountry, setSelectedCountry] = useState('bahrain');
  const [selectedCity, setSelectedCity] = useState('manama');
  const [selectedArea, setSelectedArea] = useState('area1');

  const onSubmit = () => {
    onClick();
  };

  return (
    <DeliveryPopUpContainer>
      <h4>Delivery Location</h4>
      <p>Country</p>
      <Select
        value={selectedCountry}
        valueKey="value"
        labelKey="label"
        options={deliveryCountry}
        onChange={(v) => setSelectedCountry(v)}
      />
      <p>City</p>
      <Select value={selectedCity} valueKey="value" options={deliveryCity} onChange={(v) => setSelectedCity(v)} />
      <p>Area</p>
      <Select value={selectedArea} valueKey="value" options={deliveryArea} onChange={(v) => setSelectedArea(v)} />
      <ButtonContainer>
        <CancelButton onClick={onClick} type="link">
          Cancel
        </CancelButton>
        <SaveButton onClick={onSubmit} type="danger">
          Save
        </SaveButton>
      </ButtonContainer>
    </DeliveryPopUpContainer>
  );
};

DeliveryPopUp.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default DeliveryPopUp;
