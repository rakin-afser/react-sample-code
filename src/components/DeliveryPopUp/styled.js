import styled from 'styled-components/macro';
import {Button} from 'antd';
import {secondaryFont} from 'components/Header/constants';

export const DeliveryPopUpContainer = styled.div`
  h4 {
    margin-bottom: 20px;
    font-family: ${secondaryFont};
    font-style: normal;
    font-weight: 600;
    font-size: 20px;
  }

  p {
    margin-bottom: 5px;
  }

  .ant-select {
    margin-bottom: 20px;

    &:nth-of-type(3) {
      margin-bottom: 17px;
    }
  }

  box-shadow: 0 4px 20px rgba(0, 0, 0, 0.15);
  border-radius: 4px;
  position: absolute;
  top: 43px;
  left: -78px;
  width: 272px;
  height: 380px;
  z-index: 10;
  background-color: #fff;
  padding: 17px 16px 20px;

  &::before {
    content: '';
    position: absolute;
    top: -7px;
    right: 46px;
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 0 10px 10px 10px;
    border-color: transparent transparent #ffffff transparent;
  }

  &.fade-enter {
    opacity: 0;
    transform: scale(0.5);
  }

  &.fade-enter-active {
    opacity: 1;
    transform: translateX(0);
    transition: opacity 800ms, transform 800ms;
  }

  &.fade-enter-done {
    opacity: 1;
    transform: translateX(0);
    transition: opacity 600ms, transform 600ms;
  }

  &.fade-exit {
    opacity: 1;
  }

  &.fade-exit-active {
    opacity: 0;
    transform: scale(0.5);
    transition: opacity 600ms, transform 600ms;
  }

  &.fade-exit-done {
    opacity: 0;
    transition: opacity 600ms, transform 600ms;
  }
`;

export const ButtonContainer = styled.div`
  margin: 0;
  text-align: center;

  button {
    margin: 0 10px;
    font-family: 'Helvetica', sans-serif;
    transition: ease 0.4s;
  }
`;

export const CancelButton = styled(Button)`
  color: #333333;

  span {
    font-size: 14px;
  }
`;

export const SaveButton = styled(Button)`
  border-radius: 24px !important;
  padding: 5px 30px !important;

  span {
    font-size: 14px;
    color: #fff;
  }
`;
