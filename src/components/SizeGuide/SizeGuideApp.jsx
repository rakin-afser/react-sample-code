import React from 'react';
import {useTranslation} from 'react-i18next';
import './SizeGuideApp.css';
import Genders from './components/Genders';
import cancel from './assets/img/Cancel.svg';

function SizeGuide({close, setClose}) {
  const {t} = useTranslation('sizeGuide');
  return (
    <>
      <div className="size-guide-modal">
        <div className="header">
          <p className="header-title">{t('header.sizeGuide')}</p>
          <img className="header-close" src={cancel} alt="close" onClick={() => setClose(false)} />
        </div>
        <Genders />
      </div>
    </>
  );
}

export default SizeGuide;
