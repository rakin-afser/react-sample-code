import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import Jackets from './Jackets';
import './men.css';
import Shoes from './Shoes';
import Shorts from './Shorts';
import Tops from './Tops';
import Trousers from './Trousers';

const Men = () => {
  const {t} = useTranslation('sizeGuide');
  const things = [
    `${t('men.tops')}`,
    `${t('men.trousers')}`,
    `${t('men.jackets')}`,
    `${t('men.shorts')}`,
    `${t('men.shoes')}`
  ];
  const select = [<Tops />, <Trousers />, <Jackets />, <Shorts />, <Shoes />];
  const [selectedThings, setSelectedGenderThings] = useState(0);

  const handleThings = (index) => {
    setSelectedGenderThings(index);
  };
  return (
    <div className="men">
      <div className="wrapper-men__things">
        {things.map((item, index) => {
          return (
            <div
              key={index}
              className={index === selectedThings ? 'men-things__active' : 'men-things'}
              onClick={() => handleThings(index)}
            >
              <p className="men-things__item">{item}</p>
            </div>
          );
        })}
      </div>
      {select.map((el, index) => {
        if (index === selectedThings) {
          return (
            <div className="men-things__table" key={index}>
              {el}
            </div>
          );
        }
      })}
    </div>
  );
};

export default Men;
