import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import InfantShoesKids from './InfantShoesKids';
import JuniorShoesKids from './JuniorShoesKids';
import ToddlerShoesKids from './ToddlerShoesKids';

const KidsShoes = () => {
  const {t} = useTranslation('sizeGuide');
  const shoes = [`${t('kids.ageShoes.infant')}`, `${t('kids.ageShoes.toddler')}`, `${t('kids.ageShoes.junior')}`];
  const [selectedShoes, setSelectedShoes] = useState(0);
  const selectShoes = [<InfantShoesKids />, <ToddlerShoesKids />, <JuniorShoesKids />];

  const handleShoes = (index) => {
    setSelectedShoes(index);
  };
  return (
    <>
      <div className="kids-shoes">
        {shoes.map((item, index) => {
          return (
            <div
              key={index}
              className={index === selectedShoes ? 'kids-things__active' : 'kids-things'}
              onClick={() => handleShoes(index)}
            >
              <p className="kids-things__item">{item}</p>
            </div>
          );
        })}
      </div>
      {selectShoes.map((el, index) => {
        if (index === selectedShoes) {
          return (
            <div key={index} className="kids-things__table">
              {el}
            </div>
          );
        }
      })}
    </>
  );
};

export default KidsShoes;
