import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import ClothesBabies from './ClothesBabies';
import ClothesChildren from './ClothesChildren';
import ClothesTeenBoys from './ClothesTeenBoys';
import ClothesTeenGirls from './ClothesTeenGirls';

const KidsClothes = () => {
  const {t} = useTranslation('sizeGuide');
  const ageClothes = [
    `${t('kids.ageClothes.babies')}`,
    `${t('kids.ageClothes.children')}`,
    `${t('kids.ageClothes.teenBoys')}`,
    `${t('kids.ageClothes.teenGirls')}`
  ];
  const [selectedAge, setSelectedAge] = useState(0);
  const selectAge = [<ClothesBabies />, <ClothesChildren />, <ClothesTeenBoys />, <ClothesTeenGirls />];
  const handleAgeClothes = (index) => {
    setSelectedAge(index);
  };
  return (
    <>
      <div className="kids-clothes">
        {ageClothes.map((item, index) => {
          return (
            <div
              key={index}
              className={index === selectedAge ? 'kids-things__active' : 'kids-things'}
              onClick={() => handleAgeClothes(index)}
            >
              <p className="kids-things__item">{item}</p>
            </div>
          );
        })}
      </div>
      {selectAge.map((el, index) => {
        if (index === selectedAge) {
          return (
            <div key={index} className="kids-things__table">
              {el}
            </div>
          );
        }
      })}
    </>
  );
};

export default KidsClothes;
