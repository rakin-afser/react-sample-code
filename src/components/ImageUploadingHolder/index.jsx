import React, {useRef} from 'react';
import PropTypes from 'prop-types';
import FadeOnMount from 'components/Transitions';
import {Container, Wrapper, Icon, Txt, DeleteImgBtn} from './styled';
import {ReactComponent as CloseIcon} from './img/cross.svg';

const ImageUploadingHolder = ({images, addImage, deleteImage}) => {
  const inputRef = useRef(null);

  const onAddImage = () => {
    inputRef.current.click();
  };

  const onChangeImage = (e) => {
    const imageUrl = {id: Date.now(), url: URL.createObjectURL(e.target.files[0])};
    addImage(imageUrl);
  };

  const onDeleteImage = (id) => {
    deleteImage(id);
  };

  return (
    <Container>
      {images &&
        images.map((el) => (
          <FadeOnMount>
            <Wrapper key={el.id}>
              <img src={el.url} alt="uploaded img" />
              <DeleteImgBtn onClick={() => onDeleteImage(el.id)}>
                <CloseIcon />
              </DeleteImgBtn>
            </Wrapper>
          </FadeOnMount>
        ))}
      {images.length < 5 ? (
        <Wrapper style={{paddingTop: '10px'}} onClick={onAddImage}>
          <Icon />
          <Txt>Add Photos/ Videos</Txt>
          <input ref={inputRef} onChange={onChangeImage} type="file" style={{display: 'none'}} />
        </Wrapper>
      ) : null}
    </Container>
  );
};

ImageUploadingHolder.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  addImage: PropTypes.func.isRequired,
  deleteImage: PropTypes.func.isRequired
};

export default ImageUploadingHolder;
