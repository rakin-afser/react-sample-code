import styled from 'styled-components';
import {secondaryTextColor} from 'constants/colors';
import {ReactComponent as PlusIcon} from './img/plus.svg';

export const Container = styled.div`
  display: flex;
`;

export const Wrapper = styled.div`
  position: relative;
  margin-right: 8px;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 69px;
  height: 69px;
  background: #ffffff;
  box-shadow: 0 2.11765px 7.94118px rgba(0, 0, 0, 0.1);
  border-radius: 1.58824px;
  overflow: hidden;
  cursor: pointer;
  transition: ease 0.6s;
  flex-shrink: 0;

  &:hover {
    box-shadow: 0 2.11765px 10px rgba(0, 0, 0, 0.2);
  }

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export const Icon = styled(PlusIcon)`
  margin-bottom: 12px;
  margin-left: auto;
  margin-right: auto;
`;

export const Txt = styled.div`
  font-size: 10px;
  line-height: 120%;
  text-align: center;
  letter-spacing: 0.06em;
  color: ${secondaryTextColor};
`;

export const DeleteImgBtn = styled.div`
  position: absolute;
  top: 1px;
  right: 2px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 14px;
  height: 14px;
  border-radius: 50%;
  background: rgba(250, 250, 250, 0.6);
  z-index: 1;
  transition: ease 0.6s;

  &:hover {
    background: rgba(250, 250, 250, 1);
  }
`;
