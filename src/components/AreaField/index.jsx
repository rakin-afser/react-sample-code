import React, {useState} from 'react';
import {Wrap, AreaField as TextArea, Label, Counter} from './styled';

const AreaField = (props) => {
  const [count, setCount] = useState(0);

  const {label, counter, maxLength = 400, value,required, ...rest} = props;
  return (
    <Wrap>
      <Label>
        {label}
        {required && <span>*</span>}
      </Label>
      <TextArea {...rest} {...{maxLength}} {...{value}}
                // onChange={(e) => setCount(e.target.value.length)}
      />
      {counter && (
        <Counter>
          {count}/{maxLength} symbols
        </Counter>
      )}
    </Wrap>
  );
};

export default AreaField;
