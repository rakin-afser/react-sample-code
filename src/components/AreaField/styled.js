import styled from 'styled-components';
import {Input} from 'antd';
import {primaryColor} from 'constants/colors';

const {TextArea} = Input;

export const Label = styled.label`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #000;
  margin-bottom: 6px;
  display: block;

  span {
    color: ${primaryColor};
    margin-left: 3px;
  }
`;

export const Wrap = styled.div`
  position: relative;
`;

const sizes = {
  large: 122
};

export const AreaField = styled(TextArea)`
  &&& {
    box-shadow: none;
    resize: none;
    margin-bottom: 16px;
    height: ${({size}) => `${size ? sizes[size] : 82}px`};
    padding: 7px 10px;
    border-color: #a7a7a7;

    &::placeholder {
      color: #cccccc;
    }

    &:hover,
    &:focus {
      border-color: #4a90e2;
    }
  }
`;

export const Counter = styled.span`
  font-size: 12px;
  color: #545454;
  opacity: 0.6;
  position: absolute;
  bottom: 21px;
  right: 8px;
`;
