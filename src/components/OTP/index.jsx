import React, {useState} from 'react';
import SingleOtpInput from './components/SingleOtpInput';
import {Wrapper, Container, ErrorMessage} from './styled';

// keyCode constants
const BACKSPACE = 8;
const LEFT_ARROW = 37;
const RIGHT_ARROW = 39;
const UP_ARROW = 38;
const DOWN_ARROW = 40;
const DELETE = 46;
const SPACEBAR = 32;

const OTP = (props) => {
  const {value, numInputs = 6, error} = props;
  const [active, setActive] = useState(0);

  const getOtpValue = () => (value ? value.toString().split('') : []);

  // Focus on input by index
  const focusInput = (input) => {
    const activeInput = Math.max(Math.min(numInputs - 1, input), 0);
    setActive(activeInput);
  };

  // Focus on next input
  const focusNextInput = () => {
    focusInput(active + 1);
  };

  // Focus on previous input
  const focusPrevInput = () => {
    focusInput(active - 1);
  };

  // Helper to return OTP from input
  const handleOtpChange = (otp) => {
    const {onChange} = props;
    const otpValue = otp.join('');

    onChange(otpValue);
  };

  // Change OTP value at focused input
  const changeCodeAtFocus = (val) => {
    const otp = getOtpValue();
    const [v] = val;
    otp[active] = v;
    handleOtpChange(otp);
  };

  const handleOnChange = (e) => {
    const {value: val} = e.target;
    if (!/\d/.test(val)) return;
    changeCodeAtFocus(val);
    focusNextInput();
  };

  const handleOnFocus = (e, i) => {
    setActive(i);
    e.target.select();
  };

  const handleOnKeyDown = (e) => {
    if (e.keyCode === BACKSPACE || e.key === 'Backspace') {
      e.preventDefault();
      changeCodeAtFocus('');
      focusPrevInput();
    } else if (e.keyCode === DELETE || e.key === 'Delete') {
      e.preventDefault();
      changeCodeAtFocus('');
    } else if (e.keyCode === LEFT_ARROW || e.key === 'ArrowLeft') {
      e.preventDefault();
      focusPrevInput();
    } else if (e.keyCode === RIGHT_ARROW || e.key === 'ArrowRight') {
      e.preventDefault();
      focusNextInput();
    } else if (
      e.keyCode === UP_ARROW ||
      e.keyCode === DOWN_ARROW ||
      e.key === 'ArrowUp' ||
      e.key === 'ArrowDown' ||
      e.keyCode === SPACEBAR ||
      e.key === ' ' ||
      e.key === 'Spacebar' ||
      e.key === 'Space'
    ) {
      e.preventDefault();
    }
  };

  // Handle pasted OTP
  const handleOnPaste = (e) => {
    e.preventDefault();

    const otp = getOtpValue();
    let nextActiveInput = active;

    // Get pastedData in an array of max size (num of inputs - current position)
    const pastedData = e.clipboardData
      .getData('text/plain')
      .slice(0, numInputs - active)
      .split('');

    // Paste data from focused input onwards
    for (let pos = 0; pos < numInputs; ++pos) {
      if (pos >= active && pastedData.length > 0) {
        otp[pos] = pastedData.shift();
        nextActiveInput += 1;
      }
    }

    setActive(nextActiveInput);
    focusInput(nextActiveInput);
    handleOtpChange(otp);
  };

  const otp = getOtpValue();

  function renderInputs() {
    const inputs = [];
    for (let i = 0; i < numInputs; i++) {
      inputs.push(
        <SingleOtpInput
          key={i}
          onChange={handleOnChange}
          onFocus={(e) => handleOnFocus(e, i)}
          onKeyDown={handleOnKeyDown}
          onPaste={handleOnPaste}
          focus={active === i}
          value={otp[i]}
        />
      );
    }

    return inputs;
  }

  return (
    <Wrapper>
      <Container>{renderInputs()}</Container>
      <ErrorMessage>{error}</ErrorMessage>
    </Wrapper>
  );
};

export default OTP;
