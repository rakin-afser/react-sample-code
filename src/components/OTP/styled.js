import styled from 'styled-components';

export const Wrapper = styled.div.attrs(({styles}) => ({style: {...styles}}))`
  max-width: 320px;
  margin: 0 auto 58px;
`;

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ErrorMessage = styled.span`
  display: block;
  text-align: center;
  font-weight: 500;
  font-size: 12px;
  line-height: 1.32;
  color: #ee121d;
  margin-top: 2px;
`;
