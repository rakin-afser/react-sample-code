import React, {useRef, useEffect} from 'react';
import {FieldsWrapper} from './styled';

const SingleOtpInput = (props) => {
  const {value, focus, ...rest} = props;

  const ref = useRef();

  useEffect(() => {
    const {current: inputEl} = ref;
    if (inputEl && focus) {
      inputEl.focus();
      inputEl.select();
    }
  }, [focus]);

  return (
    <FieldsWrapper>
      <input type="number" ref={ref} maxLength={1} value={value || ''} {...rest} />
    </FieldsWrapper>
  );
};

export default SingleOtpInput;
