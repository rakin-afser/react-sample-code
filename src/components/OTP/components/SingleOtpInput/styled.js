import styled from 'styled-components';

export const FieldsWrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;

  input {
    width: 40px;
    height: 34px;
    color: #000;
    border: none;
    border-bottom: 1px solid #e4e4e4;
    border-radius: 0;
    padding: 0 10px 5px;
    text-align: center;
    font-size: 24px;
    outline: none;

    &[type='number'] {
      -moz-appearance: textfield;
    }

    &::-webkit-outer-spin-button,
    &::-webkit-inner-spin-button {
      -webkit-appearance: none;
    }

    &::placeholder {
      font-size: 24px;
      color: #8f8f8f;
    }

    &:focus {
      color: black;
      border-color: #8f8f8f;
    }

    &.active.error,
    &.error {
      color: black;
      border-color: #ed484f;
    }
  }
`;
