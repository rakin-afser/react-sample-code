import styled from 'styled-components';
import {primaryColor} from 'constants/colors';

export const AdvertisementArea = styled.div`
  margin: 0;
  width: 100%;
  font-size: 12px;
  color: #999;
`;

export const AdvertisementRow = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
`;

export const ImageBox = styled.div`
  margin: 0 0 10px;
`;

export const AdvertisementClose = styled.button`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0;
  color: #999;
  fill: #999;
  transition: ease 0.4s;
  cursor: pointer;

  &:hover {
    color: ${primaryColor};
    fill: ${primaryColor};
  }

  i {
    margin-left: 2px;
    transform: scale(0.7);
  }
`;
