import React from 'react';
import Icon from 'components/Icon';

import {AdvertisementArea, AdvertisementRow, AdvertisementClose, ImageBox} from './styled';

const AdvertisementBox = ({data, close = (f) => f}) => {
  return (
    <AdvertisementArea>
      <AdvertisementRow>
        {data.title && <div>{data.title}</div>}
        <AdvertisementClose onClick={close}>
          <div>Close Advertisement</div>
          <Icon type="close" color="inherit" />
        </AdvertisementClose>
      </AdvertisementRow>
      <ImageBox>{data.image && <img src={data.image} alt={data.title} />}</ImageBox>
    </AdvertisementArea>
  );
};

export default AdvertisementBox;
