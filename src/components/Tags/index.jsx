import React from 'react';
import {useHistory} from 'react-router-dom';
import qs from 'qs';

import {TagsContainer, ProductTag} from './styled';

const Tags = ({styles = null, color = '#f0646a', items}) => {
  const {push} = useHistory();

  const tagClick = (tag) => {
    const queryString = qs.stringify({
      filters: {hashtags: [tag]}
    });
    push(`/search/posts/all?${queryString}`);
  };

  return items?.length ? (
    <TagsContainer styles={styles}>
      {items?.map((el) => (
        <ProductTag onClick={() => tagClick(`${el.name}`)} color={color} key={el?.id}>
          #{el?.name}
        </ProductTag>
      ))}
    </TagsContainer>
  ) : null;
};

export default Tags;
