import styled from 'styled-components';

export const TagsContainer = styled.div`
  ${({styles}) => (styles ? {...styles} : null)}
`;

export const ProductTag = styled.span`
  font-size: 14px;
  cursor: pointer;
  color: ${({color}) => color};
  margin-right: 10px;
`;
