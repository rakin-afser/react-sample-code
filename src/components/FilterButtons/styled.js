import styled from 'styled-components';

export const Wrap = styled.div`
  overflow: auto;
  display: flex;
`;

export const Tags = styled.div`
  display: flex;
  padding: ${({padding}) => padding};
  position: relative;
`;

export const Tag = styled.button`
  border: 1px solid #cccccc;
  height: 28px;
  border-radius: 22px;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #666666;
  white-space: nowrap;
  padding: 0 20px 0;
  transition: all 0.5s;

  ${({selected}) =>
    selected &&
    `
      color: #666666;
      border-color: #EFEFEF;
      background-color: #EFEFEF;
    `}

  &:not(:last-child) {
    margin-right: 8px;
  }
`;
