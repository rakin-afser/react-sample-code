import React, {useState} from 'react';
import {Wrap, Tags, Tag} from './styled';

const defaultItems = [
  {id: 'last-week', name: 'Last Week'},
  {id: 'last-month', name: 'Last Month'},
  {id: 'last-3-month', name: 'Last 3 Months'},
  {id: 'all', name: 'All'}
];

const FilterButtons = ({
  data = defaultItems,
  setActiveDate = () => {},
  onClick = () => {},
  selectedId = 0,
  wrapPadding = '16px'
}) => {
  const [selectedItem, setSelectedItem] = useState(data[selectedId]);

  function itemClick(item) {
    if (item?.id === selectedItem?.id) {
      setSelectedItem({});
    } else {
      setSelectedItem(item);
    }
    onClick(item);
    setActiveDate(item?.name);
  }

  return (
    <Wrap>
      <Tags padding={wrapPadding}>
        {data.map((item) => (
          <Tag selected={item?.id === selectedItem?.id} onClick={() => itemClick(item)} key={item?.id}>
            {item?.name}
          </Tag>
        ))}
      </Tags>
    </Wrap>
  );
};

export default FilterButtons;
