import styled from 'styled-components';

export const Wrap = styled.div`
  margin-top: 24px;
  text-align: center;

  @media (min-width: 767px) {
    text-align: right;
  }
`;

export const Form = styled.form`
  @media (min-width: 767px) {
    width: 100%;
    max-width: 350px;
    margin: 0 auto;
  }
`;
