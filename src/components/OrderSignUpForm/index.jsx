import React from 'react';
import {Formik} from 'formik';
import Input from 'components/Input';
import {useRegisterCustomerMutation, useValidateUsernameMutation} from 'hooks/mutations';
import {validatePassword} from 'util/validators';
import {message} from 'antd';
import Btn from 'components/Btn';
import {Wrap, Form} from './styled';
import {useHistory} from 'react-router';
import {useWindowSize} from '@reach/window-size';

const OrderSignUpForm = ({email}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  const {push} = useHistory();
  const [validateUsername] = useValidateUsernameMutation();
  const [signup, {loading: loadingSignup, data: dataSignUp}] = useRegisterCustomerMutation({
    onCompleted() {
      message.success(
        `Congratulations! Your account has been successfully created! In a 3 seconds you will be redirected to Sign In page.`
      );
      setTimeout(() => {
        push('/auth/sign-in');
      }, 3000);
    },
    onError(error) {
      message.error(error.message);
    }
  });

  async function validateUsernameInput(value) {
    let error;

    if (value) {
      try {
        const {data} = await validateUsername({variables: {input: {username: value}}});
        if (data.validateUsername?.code === 200) {
          error = 'This Username is already taken. Please select another one.';
          return error;
        }
      } catch (err) {
        console.log('err', err);
      }
    }

    return error;
  }

  return (
    <Formik
      initialValues={{
        username: '',
        password: ''
      }}
      validate={async (values) => {
        const errors = {};

        if (!values.username) {
          errors.username = 'Please enter a valid username';
        }

        if (!values.password) {
          const passwordValid = validatePassword(values.password);
          if (!passwordValid.isValid) errors.password = passwordValid.message;
        }

        return errors;
      }}
      onSubmit={(values) => {
        signup({variables: {input: {...values, email}}});
      }}
    >
      {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur}) => {
        return (
          <Form onSubmit={handleSubmit}>
            <Input
              validate={validateUsernameInput}
              onBlur={handleBlur}
              userNameAvailable={!errors.username && touched.username && values.username}
              key="username"
              type="text"
              name="username"
              id="username"
              label="Select Username"
              onChange={handleChange}
              error={touched.username && errors.username ? errors.username : ''}
              value={values.username}
              reset={values.username.length > 0}
              resetCallback={() => {
                setFieldValue('username', '');
              }}
              required
            />

            <Input
              key="password"
              type="password"
              name="password"
              id="password"
              onBlur={handleBlur}
              autoComplete="new-password"
              label="Set Password"
              onChange={handleChange}
              error={touched.password && values.password.length < 1 ? errors.password : ''}
              value={values.password}
              reset={values.password.length > 0}
              showPassword
              passwordChars
              required
            />

            <Wrap>
              <Btn type="submit" kind={isMobile ? 'bare' : 'pr-bare'} loading={loadingSignup} disabled={dataSignUp}>
                Create Account
              </Btn>
            </Wrap>
          </Form>
        );
      }}
    </Formik>
  );
};

export default OrderSignUpForm;
