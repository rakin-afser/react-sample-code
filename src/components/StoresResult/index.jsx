import React from 'react';
import {useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import Store from 'components/Cards/Store';
import {Grid} from 'containers/SearchResult/containers/styled';

const StoresResult = ({data}) => {
  const history = useHistory();

  const onClick = (e, slug) => {
    history.push(`/shop/${slug}/products`);
  };
  return (
    <Grid margin="40px 0 0 0">
      {data?.map((item, index) => (
        <Store key={index} data={item} onClick={(e) => onClick(e, item.storeUrl)} />
      ))}
    </Grid>
  );
};

StoresResult.propTypes = {
  data: PropTypes.shape.isRequired
};

export default StoresResult;
