import adidasCover from './imgs/adidas.jpg';
import adidasCover2 from './imgs/adidas2.jpg';
import converseCover from './imgs/gucci.jpg';
import rayBanCover from './imgs/rayban.jpg';
import rayBanCover2 from './imgs/rayban2.jpg';
import levisCover from './imgs/levis.jpg';
import levisCover2 from './imgs/levis2.jpg';
import bosaCover from './imgs/bosa.jpg';
import zaraCover from './imgs/zara.jpg';
import zaraCover2 from './imgs/zara2.jpg';
import columbiaCover from './imgs/columbia.png';
import championCover2 from './imgs/champion.jpg';
// logos
import adidasLogo from './imgs/logos/adidas.png';
import rayBanLogo from './imgs/logos/ray-ban.png';
import levisLogo from './imgs/logos/levis.png';
import bosaLogo from './imgs/logos/bosa.png';
import converseLogo from './imgs/logos/converse.png';
import zaraLogo from './imgs/logos/zara.png';
import columbiaLogo from './imgs/logos/columbia.png';
import championLogo from './imgs/logos/champion.png';

export default [
  {
    cover: adidasCover,
    logo: adidasLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: converseCover,
    logo: converseLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: rayBanCover,
    logo: rayBanLogo,
    title: 'Ray Ban Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: levisCover,
    logo: levisLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: levisCover2,
    logo: levisLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: bosaCover,
    logo: bosaLogo,
    title: 'Bosa magazine',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: zaraCover,
    logo: zaraLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: columbiaCover,
    logo: columbiaLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: adidasCover2,
    logo: adidasLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: zaraCover2,
    logo: zaraLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: rayBanCover2,
    logo: rayBanLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  },
  {
    cover: championCover2,
    logo: championLogo,
    title: 'Adidas Official',
    followers: '905k',
    stars: 4,
    voted: 335
  }
];
