import React from 'react';
import {useQuery} from '@apollo/client';
import {useHistory} from 'react-router-dom';

import {CustomBadge} from 'components/Counters/styled';
import ShoppingCart from 'assets/ShoppingCart';
import {CART_QUANTITY} from 'queries';

const Cart = ({isMobile, children = <ShoppingCart />, badgeColor = '#ED484F'}) => {
  const history = useHistory();
  const {data} = useQuery(CART_QUANTITY);

  return (
    <CustomBadge
      offset={[-3, 3]}
      count={parseInt(data?.cart?.totalQuantity || 0)}
      isMobile={isMobile}
      badgeColor={badgeColor}
      onClick={() => {
        history.push('/cart');
      }}
    >
      {children}
    </CustomBadge>
  );
};

export default Cart;
