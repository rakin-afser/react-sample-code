import React from 'react';
import {useHistory} from 'react-router-dom';

import {CustomBadge} from 'components/Counters/styled';
import Icon from 'components/Icon';

const Orders = ({
  isMobile,
  children = <Icon type="checkMark" color="#464646" width={24} height={24} />,
  badgeColor = '#ED484F'
}) => {
  const history = useHistory();
  const counter = 10;

  return (
    <CustomBadge
      offset={[-3, 3]}
      count={counter}
      isMobile={isMobile}
      badgeColor={badgeColor}
      onClick={() => {
        history.push('/profile/orders');
      }}
    >
      {children}
    </CustomBadge>
  );
};

export default Orders;
