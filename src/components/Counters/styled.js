import styled from 'styled-components/macro';
import {Badge} from 'antd';

export const CustomBadge = styled(Badge)`
  &&& {
    .ant-scroll-number {
      margin-top: 3px;
      height: 14px;
      background: ${({badgeColor}) => badgeColor};
      border-radius: 10px;
      line-height: 16px;
      font-size: 10px;
      padding: 0 5px !important;
    }
  }
`;
