import styled from 'styled-components';
import {
  mainWhiteColor as white,
  mainBlackColor as black,
  headerShadowColor as gray400,
  transparentTextColor as gray300
} from 'constants/colors';

export const Textarea = styled.textarea`
  display: block;
  width: 100%;
  height: ${({height}) => height + 'px' || '100px'};
  background: ${white};
  border: 1px solid ${gray400};
  padding: 13px 15.87px;
  resize: none;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: ${black};

  &::placeholder {
    color: ${gray300};
  }
`;
