import React from 'react';
import {Textarea} from './styled';

export default React.forwardRef((props, ref) => <Textarea ref={ref} {...props} />);
