import React from 'react';

import useGlobal from 'store';

import Avatar from 'components/Avatar';
import {ReactComponent as IconStar} from 'containers/ProductPage/Components/Feedback/img/iconStar.svg';
import {Row, Column, Name, UserType, Text} from 'components/UserInfo/styled';
import FollowButton from 'components/FollowButton';
import Btn from 'components/Btn';
import {useFollowUnfollowUserMutation, useDeletePostMutation} from 'hooks/mutations';
import {useUser} from 'hooks/reactiveVars';

import {MoreButtonArea, MoreButton} from '../styled';

import moreIcon from '../img/More_Button.svg';

const UserInfo = ({
  onClick = () => {},
  id,
  databaseId,
  src,
  type,
  authorDatabaseId,
  followers,
  firstName,
  lastName,
  isOnline,
  avatarWidth = 50,
  avatarHeight = 50,
  isFollowed
}) => {
  const [globalState, setGlobalState] = useGlobal();
  const [followUnfollowUser, {loading}] = useFollowUnfollowUserMutation({id, isFollowed});
  const [user] = useUser();

  const isBelongs = id === user?.id;

  const [deletePost] = useDeletePostMutation({variables: {input: {id, status: 'TRASH'}}});

  const buttons = [
    isBelongs && {
      title: 'Edit'
    },
    isBelongs && {
      title: 'Delete',
      onClick: () => {
        deletePost();
        setGlobalState.setOptionsPopup({active: false});
      }
    },
    {
      title: 'Report',
      onClick: () => {
        setGlobalState.setOptionsPopup({active: false});
        setGlobalState.setReportPopup({active: true, subjectId: databaseId, subjectSource: 'post_report'});
      }
    },
    {
      title: isFollowed ? 'Unfollow' : 'Follow',
      onClick: () => {
        followUnfollowUser({variables: {input: {id: authorDatabaseId}}});
        setGlobalState.setOptionsPopup({active: false});
      }
    }
  ];

  const onMore = () => {
    if (user) {
      setGlobalState.setOptionsPopup({active: true, buttons});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  const onFollow = () => {
    if (user) {
      followUnfollowUser({variables: {input: {id: databaseId}}});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  return (
    <Row>
      <Avatar
        onClick={onClick}
        width={avatarWidth}
        height={avatarHeight}
        isOnline={isOnline}
        img={src}
        borderColor="#E4E4E4"
      />
      <Column
        padding="0 0 0 8px"
        style={{flexGrow: '1', flexDirection: 'initial', alignItems: 'center', width: 'calc(100% - 50px)'}}
      >
        <Row style={{flexWrap: 'wrap'}}>
          <Name onClick={onClick} margin="0 0 2px 0" size={16} style={{width: '100%', lineHeight: 1.1}}>
            {firstName} {lastName}
          </Name>
          {type === 'influencer' && (
            <UserType>
              <IconStar />
              Influencer
            </UserType>
          )}
          <Text margin={type === 'influencer' ? '0 39px 0 10px' : '0 39px 0 0'}>{followers} followers</Text>
        </Row>
        {databaseId && user?.databaseId === databaseId ? null : (
          <Btn
            style={{marginLeft: 'auto'}}
            onClick={onFollow}
            kind={isFollowed ? 'following-md' : 'follow-md'}
            loading={loading}
          />
        )}
        <MoreButtonArea>
          <MoreButton onClick={onMore} src={moreIcon} alt="moreSettings " />
        </MoreButtonArea>
      </Column>
    </Row>
  );
};

export default UserInfo;
