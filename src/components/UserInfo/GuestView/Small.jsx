import React from 'react';

import Avatar from 'components/Avatar';
import {ReactComponent as IconStar} from 'containers/ProductPage/Components/Feedback/img/iconStar.svg';
import {Row, Column, Name, UserType, Text} from 'components/UserInfo/styled';
import FollowButton from 'components/FollowButton';
import Btn from 'components/Btn';
import {useFollowUnfollowUserMutation} from 'hooks/mutations';
import {useUser} from 'hooks/reactiveVars';

const UserInfo = ({
  onClick = () => {},
  id,
  databaseId,
  src,
  type,
  followers,
  firstName,
  lastName,
  isOnline,
  avatarWidth = 60,
  avatarHeight = 60,
  isFollowed
}) => {
  const [followUnfollowUser, {loading}] = useFollowUnfollowUserMutation({id, isFollowed});
  const [user] = useUser();

  return (
    <Row>
      <Avatar
        onClick={onClick}
        width={avatarWidth}
        height={avatarHeight}
        isOnline={isOnline}
        img={src}
        borderColor="#E4E4E4"
      />
      <Column padding="0 17px" style={{flexGrow: '1'}}>
        <div>
          <Name onClick={onClick} margin="0 0 7px 0" size={16}>
            {firstName} {lastName}
          </Name>
        </div>
        <Row>
          {type === 'influencer' && (
            <UserType>
              <IconStar />
              Influencer
            </UserType>
          )}
          <Text margin={type === 'influencer' ? '0 39px 0 10px' : '0 39px 0 0'}>{followers} followers</Text>
          {databaseId && user?.databaseId === databaseId ? null : (
            <Btn
              onClick={() => followUnfollowUser({variables: {input: {id: databaseId}}})}
              kind={isFollowed ? 'following-md' : 'follow-md'}
            />
          )}
        </Row>
      </Column>
    </Row>
  );
};

export default UserInfo;
