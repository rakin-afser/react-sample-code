import React from 'react';

import Icon from 'components/Icon';
import ExpandedText from 'components/ExpandedText';
import Buttons from 'components/Buttons';
import {
  About,
  Avatar,
  AvatarPic,
  Location,
  Name,
  Statistic,
  StatisticCount,
  Profession,
  AvatarWrap,
  ButtonsWrapper
} from 'components/UserInfo/styled';

const UserInfo = ({user = {}}) => {
  return (
    <About>
      <AvatarWrap>
        <Avatar>
          <AvatarPic src={user.avatar} />
        </Avatar>
      </AvatarWrap>
      <Name>{user.alias}</Name>
      {user.location && (
        <Location>
          <Icon type="marker" />
          {user.location}
        </Location>
      )}
      <div>
        <Statistic>
          <StatisticCount>{user.followers}</StatisticCount>
          followers
        </Statistic>
        <Statistic>
          <StatisticCount>{user.following}</StatisticCount>
          following
        </Statistic>
      </div>
      <ButtonsWrapper>
        <Buttons type="follow" />
        <Buttons type="sendMessage" />
      </ButtonsWrapper>
      {user.profession && <Profession>{user.profession}</Profession>}
      <ExpandedText margin={'0'} text={user.description} divider={57} moreText="More" lessText="Less" />
    </About>
  );
};

export default UserInfo;
