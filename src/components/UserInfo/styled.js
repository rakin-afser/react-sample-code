import {secondaryFont} from 'constants/fonts';

import styled from 'styled-components/macro';

export const About = styled.div`
  background: #fff;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 33px 33px 49px 33px;
  border-radius: 4px;
  width: 100%;
  height: fit-content;
`;

export const AvatarWrap = styled.div`
  position: relative;
`;

export const Avatar = styled.div`
  width: 150px;
  height: 150px;
  border-radius: 50%;
  overflow: hidden;
`;

export const AvatarPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Name = styled.span`
  cursor: pointer;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 600;
  font-size: ${({size}) => size || '20'}px;
  letter-spacing: 0.019em;
  color: #000000;
  margin: ${({margin}) => margin || '16px 0'};
  display: inline-block;
`;

export const Location = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: ${secondaryFont};
  font-size: 14px;
  color: #464646;
  margin: 0 0 26px 0;

  & svg {
    width: 18px;
    height: 20px;
    margin-right: 8px;
  }

  & svg path {
    fill: #464646;
  }
`;

export const Statistic = styled.span`
  font-family: ${secondaryFont};
  font-size: 14px;
  color: #545454;

  &:first-child {
    margin-right: 8px;
  }
`;

export const StatisticCount = styled.span`
  font-weight: 600;
  font-family: ${secondaryFont};
  font-size: 18px;
  letter-spacing: 0.019em;
  color: #000;
  margin-right: 3px;
`;

export const Profession = styled.span`
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  text-align: center;
  letter-spacing: -0.016em;
  color: #000000;
  margin: 24px 0 8px;
  text-transform: uppercase;
`;

export const ButtonsWrapper = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  margin-top: 12px;
  width: 100%;
  justify-content: space-around;

  & button {
    margin: 12px 2px 0 2px;
  }
`;

export const Row = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: ${({padding}) => padding || '0'};
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex-shrink: 0;
  padding: ${({padding}) => padding || '0'};
`;

export const UserType = styled.div`
  background: #e5f1ff;
  border-radius: 24px;
  padding: 4px 8px 3px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #1b5dab;
  display: flex;

  & svg {
    margin-right: 4px;
  }
`;

export const Text = styled.div`
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  color: #464646;
  margin: ${({margin}) => margin || '0'};
`;

export const MoreButton = styled.img``;

export const MoreButtonArea = styled.div`
  position: relative;
  right: 0;
  padding: 0;
  margin: 0 0 0 16px;
  height: 16px;
  cursor: pointer;
  border-radius: 20px;
  transition: ease 0.4s;

  &:hover {
    transform: scale(1.12);
    background-color: #eaeaea;
  }

  img {
    display: block;
    margin: 5px 0 0;
  }
`;
