import React, {useEffect} from 'react';
import Icon from 'components/Icon';
import {primaryColor} from 'constants/colors';
import {Wrapper, Message} from './styled';
import {useLazyQuery, useMutation} from '@apollo/client';
import {useHistory} from 'react-router';
import qs from 'qs';
import {CHECKOUTDOTCOM_PAYMENT_HANDLER} from 'queries';
import {testSample_VALIDATE_TAPPAY_AUTH_TOKEN} from 'mutations';

const PaymentHandler = () => {
  const {push, location} = useHistory();
  const {tap_id: tapId, 'cko-session-id': ckoId} = qs.parse(location?.search?.replace('?', '')) || {};

  const [validateOrder] = useMutation(testSample_VALIDATE_TAPPAY_AUTH_TOKEN, {
    onCompleted(data) {
      const {success, status} = data?.testSampleValidateTapPayAuthToken || {};
      if (success) {
        push(`/checkout/success?tap_id=${tapId}`);
      } else {
        push('/checkout/failed', {status});
      }
    }
  });

  useEffect(() => {
    if (tapId) {
      validateOrder({variables: {input: {auth_token: tapId}}});
    }
  }, [validateOrder, tapId]);

  const [getPayment] = useLazyQuery(CHECKOUTDOTCOM_PAYMENT_HANDLER, {
    onCompleted(data) {
      const {success, status} = data?.testSampleCheckoutDotComPayment || {};
      if (success) {
        push(`/checkout/success?ckoId=${ckoId}`);
      } else {
        push('/checkout/failed', {status});
      }
    },
    onError() {
      push('/checkout/failed');
    }
  });

  useEffect(() => {
    if (ckoId) {
      getPayment({variables: {ckoId}});
    }
  }, [getPayment, ckoId]);

  return (
    <Wrapper>
      <Icon type="loader" width={50} height={10} fill={primaryColor} />
      <Message>Your Payment is being finalized...</Message>
    </Wrapper>
  );
};

export default PaymentHandler;
