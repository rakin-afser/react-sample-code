import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: calc(100vh - 70px);

  @media (max-width: 767px) {
    height: 100vh;
  }
`;

export const Message = styled.p`
  padding-top: 20px;
  padding-bottom: 20px;
  margin-bottom: 0;
  font-size: 16px;
`;
