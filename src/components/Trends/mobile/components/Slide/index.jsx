import React, {useEffect, useState} from 'react';
import {
  Wrapper,
  // Play,
  ActionBar,
  ActionItem,
  ActionButton,
  Mute,
  Add,
  VideoProgress,
  ProductList,
  ProductItem,
  ProductImage,
  InputFile,
  Footer,
  AuthorWrap,
  Author,
  Follow,
  Actions,
  AuthorAvatar,
  Title
} from './styled';
import ReactPlayer from 'react-player';
import Icon from 'components/Icon';
import {useAddUpdateLikedPostMutation, useFollowUnfollowUserMutation} from 'hooks/mutations';
import Comments from 'components/Comments';
import {useHistory} from 'react-router';
import {v1} from 'uuid';
import {useUser} from 'hooks/reactiveVars';
import useGlobal from 'store';

const Slide = (props) => {
  const [muted, setMuted] = useState(true);
  const [user] = useUser();
  const [globalState, setGlobalState] = useGlobal();

  const {
    id,
    databaseId,
    galleryImages,
    isLiked,
    totalLikes,
    commentCount,
    relatedProducts,
    author,
    active,
    title,
    slug
  } = props;

  const {id: authorId, databaseId: authorDatabaseId, username, isFollowed, avatar} = author?.node || {};

  const [showCommentsPopup, setShowCommentsPopup] = useState(false);
  const [playing, setPlaying] = useState(true);
  const [progress, setProgress] = useState(0);
  const [showProducts, setShowProducts] = useState(false);
  const {push} = useHistory();

  const [triggerLike] = useAddUpdateLikedPostMutation({post: props});
  const [followUser] = useFollowUnfollowUserMutation({id: authorId, isFollowed});

  useEffect(() => {
    if (!active && !playing) {
      setPlaying(true);
    }
  }, [active, playing]);

  useEffect(() => {
    if (globalState.productPopup) {
      setPlaying(false);
    } else {
      setPlaying(true);
    }
  }, [globalState.productPopup]);

  function onProgress(e) {
    const {played} = e || {};
    if (played >= 0.3 && !showProducts) setShowProducts(true);
    setProgress(played * 100);
  }

  async function onChangeFile(e) {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      try {
        const medias = await Promise.all(
          files.map((file) => {
            return new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.addEventListener('load', (ev) => {
                resolve({
                  id: v1(),
                  file,
                  result: ev.target.result
                });
              });
              reader.addEventListener('error', reject);
              reader.readAsDataURL(file);
            });
          })
        );
        push('/post/new', {medias});
      } catch (error) {
        console.log(`error`, error);
      }
    }
  }

  const isPlaying = playing && active && !showCommentsPopup;

  const buttons = [
    {
      title: 'Report',
      onClick: () => {
        setGlobalState.setOptionsPopup({active: false});
      }
    },
    {
      title: 'Follow',
      onClick: () => {
        setGlobalState.setOptionsPopup({active: false});
      }
    }
  ];

  const onShare = async () => {
    const shareData = {
      title: `testSample - ${title}`,
      text: '',
      url: `${window.location.origin}/post/${databaseId}`
    };

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <Wrapper>
        <Actions
          onClick={() => {
            setGlobalState.setOptionsPopup({active: true, buttons});
            setPlaying(false);
          }}
        >
          <span />
          <span />
          <span />
        </Actions>
        <Mute type={muted ? 'muted' : 'unmuted'} />
        {/* {isPlaying ? null : <Play type="play" fill="#fff" onClick={() => setPlaying(true)} />} */}
        {galleryImages?.nodes?.[0]?.mediaItemUrl ? (
          <>
            <ReactPlayer
              loop
              playsinline
              onPause={() => setPlaying(false)}
              onPlay={() => setPlaying(true)}
              onClick={() => setMuted(!muted)}
              progressInterval={250}
              onProgress={onProgress}
              muted={muted}
              playing={isPlaying}
              config={{
                file: {
                  attributes: {
                    poster: galleryImages?.nodes?.[0]?.poster?.sourceUrlLarge
                  }
                }
              }}
              width="100%"
              height="100%"
              url={galleryImages?.nodes?.[0]?.mediaItemUrl}
            />
          </>
        ) : null}
        <ActionBar>
          <ActionItem>
            <ActionButton onClick={() => setShowCommentsPopup(true)}>
              <Icon type="message" color="currentColor" />
              {commentCount || ''}
            </ActionButton>
          </ActionItem>
          <ActionItem>
            <ActionButton onClick={() => triggerLike({variables: {input: {id: databaseId}}})}>
              <Icon type={isLiked ? 'liked' : 'like'} color="#fff" />
              {totalLikes || ''}
            </ActionButton>
          </ActionItem>
          <ActionItem>
            <ActionButton onClick={onShare}>
              <Icon type="share" />
              11
            </ActionButton>
          </ActionItem>
        </ActionBar>
        <Add>
          <InputFile onChange={onChangeFile} type="file" accept="video/*" />
          <Icon type="plus" color="currentColor" width="13" height="13" />
        </Add>
        <VideoProgress percent={progress} />
        <Footer>
          <AuthorWrap>
            <AuthorAvatar src={avatar.url} alt={username} />
            <Author>@{username}</Author>
            {user?.id === authorId ? null : (
              <Follow onClick={() => followUser({variables: {input: {id: authorDatabaseId}}})}>
                {isFollowed ? 'Following' : 'Follow'}
              </Follow>
            )}
          </AuthorWrap>
          <Title>{title}</Title>
          {relatedProducts?.nodes?.length ? (
            <ProductList active={showProducts}>
              {relatedProducts?.nodes?.map((pr) => (
                <ProductItem
                  key={pr.id}
                  onClick={() =>
                    setGlobalState.setProductPopup(true, {products: relatedProducts, initialProductSlug: pr?.slug})
                  }
                >
                  <ProductImage src={pr.image.sourceUrl} alt={pr.image.altText} />
                </ProductItem>
              ))}
            </ProductList>
          ) : null}
        </Footer>
      </Wrapper>
      <Comments
        postId={id}
        commentOn={databaseId}
        isModal={false}
        showCommentsPopup={showCommentsPopup}
        setShowCommentsPopup={setShowCommentsPopup}
      />
    </>
  );
};

export default Slide;
