import styled from 'styled-components';
import Icon from 'components/Icon';
import {Progress} from 'antd';

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;

  video {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
`;

export const Play = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 60px;
  height: 60px;
  z-index: 10;
  border: 1px solid #ffffff;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
  border-radius: 50%;
`;

export const ActionBar = styled.ul`
  position: absolute;
  right: 16px;
  bottom: 115px;
  display: flex;
  flex-direction: column;
`;

export const ActionItem = styled.li`
  & + & {
    margin-top: 15px;
  }
`;

export const ActionButton = styled.button`
  width: 45px;
  height: 45px;
  color: #fff;
  background-color: rgba(97, 95, 95, 0.2);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  border-radius: 50%;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  font-size: 11px;
  line-height: 1.5;
`;

export const Mute = styled(Icon)`
  position: absolute;
  top: 80px;
  right: 16px;
  z-index: 10;
  width: 30px;
  height: 30px;
  background-color: rgba(27, 27, 27, 0.51);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.04);
  border-radius: 50%;
`;

export const Add = styled.button`
  background-color: rgba(255, 255, 255, 0.41);
  border-radius: 50%;
  width: 45px;
  height: 45px;
  position: absolute;
  bottom: 55px;
  right: 12px;
  color: #252525;
  font-size: 12px;
  font-weight: 500;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;

export const VideoProgress = styled(Progress)`
  position: absolute !important;
  bottom: 0;
  left: 0;
  right: 0;
  font-size: 0 !important;

  .ant-progress-inner {
    background-color: rgba(44, 42, 42, 0.2);
    border-radius: 0;
  }

  .ant-progress-bg {
    border-radius: 0 !important;
  }
`;

VideoProgress.defaultProps = {
  strokeColor: '#ED484F',
  strokeWidth: 4,
  showInfo: false,
  strokeLinecap: 'square'
};

export const ProductList = styled.ul`
  display: flex;
  padding-top: ${({active}) => (active ? '24px' : '0px')};
  max-height: ${({active}) => (active ? '66px' : '0px')};
  position: relative;
  overflow: hidden;
  transition: all 0.3s ease;
`;

export const ProductItem = styled.li`
  & + & {
    margin-left: 9px;
  }
`;

export const ProductImage = styled.img`
  border-radius: 8px;
  width: 42px;
  height: 42px;
`;

export const InputFile = styled.input`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1;
  opacity: 0;
  width: 100%;
`;

export const AuthorWrap = styled.div`
  display: flex;
  align-items: center;
`;

export const Author = styled.p`
  color: #fff;
  font-size: 14px;
  font-weight: 600;
  margin-bottom: 0;
  margin-right: 10px;
  min-width: 0;
  max-width: 100%;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

export const AuthorAvatar = styled.img`
  width: 25px;
  height: 25px;
  border-radius: 50%;
  margin-right: 6px;
`;

export const Footer = styled.div`
  padding: 0 17px;
  position: absolute;
  bottom: 46px;
  left: 0;
  right: 78px;
`;

export const Follow = styled.button`
  width: 79px;
  height: 30px;
  border-radius: 30px;
  background-color: rgba(255, 255, 255, 0.3);
  color: #fff;
  padding: 0;
`;

export const Actions = styled.div`
  position: absolute;
  top: 24px;
  right: 16px;
  background-color: rgba(27, 27, 27, 0.51);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  width: 42px;
  height: 42px;
  border-radius: 50%;
  z-index: 10;
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    width: 3.5px;
    height: 3.5px;
    border-radius: 50%;
    background: #fff;

    &:not(:last-child) {
      margin-right: 3.5px;
    }
  }
`;

export const Title = styled.p`
  margin: 7px 0 0;
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 140%;
  letter-spacing: 0.02em;
  color: #ffffff;
`;
