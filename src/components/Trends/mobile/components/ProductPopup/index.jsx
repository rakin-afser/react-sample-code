import React, {useState, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {useLazyQuery} from '@apollo/client';

import {
  StyledModal,
  ModalContent,
  SliderWrapMobile,
  Image,
  Previews,
  PreviewItem
} from 'components/Trends/mobile/components/ProductPopup/styled';
import BackButton from 'components/BackButton';
import Loader from 'components/Loader';
import {PRODUCT_QUICKVIEW} from 'queries';
import useGlobal from 'store';
import MainSlider from 'components/ProductPage/mobile/MainSlider';
import {ReactComponent as IconPlay} from 'components/QuickView/desktop/components/VideoPlayer/img/iconPlay.svg';
import Info from 'components/QuickView/mobile/Info';
import {useVariations} from 'hooks';
import {getVariationByAttrs, renderCoinsByVariation} from 'util/heplers';
import SizeGuide from 'components/ProductPage/mobile/SizeGuide';
import {v1} from 'uuid';
import {useWindowSize} from '@reach/window-size';

const ProductPopup = () => {
  const [globalState, setGlobalState] = useGlobal();
  const history = useHistory();
  const [media, setMedia] = useState([]);
  const [currentSlide, setCurrentSlide] = useState({});
  const [showSizeGuide, setShowSizeGuide] = useState(false);
  const [showVariationsPopup, setShowVariationsPopup] = useState(false);
  const [showMessage, setShowMessage] = useState(false);
  const {height} = useWindowSize();

  const [getProduct, {data: productData, loading: productLoading}] = useLazyQuery(PRODUCT_QUICKVIEW);

  const {products, initialProductSlug} = globalState.productPopupProps || {};

  useEffect(() => {
    if (currentSlide?.slug) {
      getProduct({variables: {id: currentSlide?.slug}});
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentSlide?.slug]);

  useEffect(() => {
    const productsSlides =
      products?.nodes?.map((item) => ({
        id: item?.slug,
        preview: item?.image?.sourceUrl,
        children: [{mimeType: 'image', mediaItemUrl: item?.image?.sourceUrl}],
        slug: item?.slug,
        databaseId: item?.databaseId
      })) || [];

    setMedia(productsSlides);
    setCurrentSlide(productsSlides.find((i) => i?.slug === initialProductSlug) || media?.[0]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalState.productPopup]);

  const onClose = () => {
    setGlobalState.setProductPopup(null);
  };

  const goToProductPage = () => {
    onClose();
    history.push(`/product/${productData?.product?.slug}`);
  };

  const getProductSliders = () => {
    const {image, galleryImages, product_video_url: productVideoUrl} = productData?.product || {};

    const mainImage = image ? [image] : [];
    const productVideo = productVideoUrl ? [{id: v1(), mimeType: 'video', mediaItemUrl: productVideoUrl}] : [];
    const gallery = galleryImages?.nodes || [];

    return [...mainImage, ...productVideo, ...gallery];
  };

  const {databaseId: databaseProductId, name, reviewCount, midCoins, averageRating, variations, productCategories} =
    productData?.product || {};

  const [vars, varState, setVarState] = useVariations(productData?.product);
  const variation = getVariationByAttrs(productData?.product, varState);

  const {price, regularPrice, salePrice} = variation || productData?.product || {};

  return (
    <StyledModal
      visible={globalState.productPopup}
      onCancel={onClose}
      style={{
        display: 'flex',
        padding: 0,
        maxHeight: `calc(${height}px - 10px - 20px)`,
        bottom: 'calc(10px + env(safe-area-inset-bottom))',
        width: '520px !important',
        top: 20,
        maxWidth: '526px',
        margin: 'auto auto 0'
      }}
      footer={null}
      destroyOnClose
    >
      <BackButton onClick={onClose} />
      <ModalContent>
        <SliderWrapMobile>
          {productLoading ? (
            <Loader wrapperHeight="319px" wrapperWidth="100%" />
          ) : (
            <MainSlider slides={getProductSliders()} />
          )}
        </SliderWrapMobile>

        {products?.nodes?.length > 1 && (
          <Previews>
            {media?.map((item, index) => (
              <PreviewItem onClick={() => setCurrentSlide(item)} isActive={currentSlide?.id === item?.id} key={index}>
                <Image src={item.preview} />
                {item.type?.split('/')?.[0] === 'video' && <IconPlay />}
              </PreviewItem>
            ))}
          </Previews>
        )}

        <Info
          databaseId={databaseProductId}
          goToProductPage={goToProductPage}
          name={name}
          rating={averageRating}
          reviews={reviewCount}
          price={price}
          regularPrice={regularPrice}
          salePrice={salePrice}
          coins={renderCoinsByVariation(midCoins, variation)}
          showVariationsPopup={showVariationsPopup}
          setShowVariationsPopup={setShowVariationsPopup}
          showMessage={showMessage}
          setShowMessage={setShowMessage}
          setShowSizeGuide={setShowSizeGuide}
          variations={variations}
          variation={variation}
          productCategories={productCategories}
          vars={vars}
          varState={varState}
          setVarState={setVarState}
        />
        <SizeGuide showSizeGuide={showSizeGuide} setShowSizeGuide={setShowSizeGuide} />
      </ModalContent>
    </StyledModal>
  );
};

export default ProductPopup;
