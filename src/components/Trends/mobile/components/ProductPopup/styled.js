import styled, {css} from 'styled-components/macro';
import {Modal} from 'antd';
import {primaryColor} from 'constants/colors';

export const StyledModal = styled(Modal)`
  width: calc(100% - 20px) !important;

  & .ant-modal-content {
    background: #fff;
    border-radius: 12px;
    width: 100%;
    margin: 0;
  }

  & .ant-modal-body {
    display: flex;
    flex-direction: column;
    padding: 0;
    height: 100%;
    margin: 0;
  }

  & .ant-modal-close {
    display: none;
  }
`;

export const SliderWrapMobile = styled.div`
  position: relative;

  & video {
    width: 100%;
    max-height: 293px;
  }

  & .slick-slide {
    max-height: 293px;
    & img {
      max-height: 293px;
      max-width: 100%;
    }
  }
`;

export const ModalContent = styled.div`
  overflow-y: auto;
  flex-grow: 1;
  position: relative;
  border-radius: 12px;
`;

export const PreviewItem = styled.div`
  position: relative;
  cursor: pointer;
  width: 60px;
  height: 60px;
  margin: 0 8px 10px 0;
  flex-direction: row;
  display: flex;
  flex-shrink: 0;
  border: 1px solid #efefef;
  border-radius: 8px;

  &::after {
    content: '';
    width: 100%;
    height: 2px;
    position: absolute;
    background: ${primaryColor};
    bottom: -8px;
    opacity: 0;
    transition: opacity 0.4s;
  }

  ${({isActive}) =>
    isActive &&
    css`
      &::after {
        opacity: 1;
      }
    `}
`;

export const Previews = styled.div`
  margin: 2px 0 10px 16px;
  max-width: 100%;
  overflow-x: auto;
  display: flex;
  align-items: center;
  transition: margin 0.5s;
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  flex-shrink: 0;
  border-radius: 8px;
`;
