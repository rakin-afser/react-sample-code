import React, {useEffect, useState} from 'react';
import {createPortal} from 'react-dom';
import {CSSTransition} from 'react-transition-group';
import {StyledSwiper, Wrapper, Back} from './styled';
import {SwiperSlide} from 'swiper/react';
import Slide from '../Slide';

const FlashVideo = ({doFetchMore, hasNextPage, data, active, setActive, initialSlide}) => {
  const [activeSlide, setActiveSlide] = useState(0);
  const prev = activeSlide > 0 ? activeSlide - 1 : 0;
  const next = activeSlide + 1;

  useEffect(() => {
    if (active) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [active]);

  const settings = {
    direction: 'vertical',
    loop: false,
    initialSlide: initialSlide || 0,
    onInit(swiper) {
      const {activeIndex} = swiper || {};
      setActiveSlide(activeIndex);
    },
    onTransitionEnd(swiper) {
      const {activeIndex} = swiper || {};
      setActiveSlide(activeIndex);
    },
    onReachEnd() {
      if (hasNextPage) {
        doFetchMore();
      }
    }
  };

  return createPortal(
    <CSSTransition in={active} unmountOnExit timeout={300}>
      <Wrapper>
        <Back type="arrowBack" color="#fff" onClick={() => setActive(false)} />
        <StyledSwiper {...settings}>
          {data?.posts?.nodes?.map((post, i) => (
            <SwiperSlide key={post?.id}>
              {[prev, activeSlide, next]?.includes(i) ? <Slide {...post} active={i === activeSlide} /> : null}
            </SwiperSlide>
          ))}
        </StyledSwiper>
      </Wrapper>
    </CSSTransition>,
    document.getElementById('modal')
  );
};

export default FlashVideo;
