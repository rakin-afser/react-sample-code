import styled from 'styled-components';
import Icon from 'components/Icon';
import {Swiper} from 'swiper/react';

export const StyledSwiper = styled(Swiper)`
  width: 100%;
  height: 100%;
`;

export const Wrapper = styled.div`
  background-color: #070707;
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 1000;

  &.enter {
    opacity: 0;
    transform: translateY(-100%);
  }

  &.enter-active {
    opacity: 1;
    transform: translateY(0);
    transition: opacity 0.3s ease, transform 0.3s ease;
  }

  &.exit {
    opacity: 1;
    transform: translateY(0);
  }

  &.exit-active {
    opacity: 0;
    transform: translateY(-100%);
    transition: opacity 0.3s ease, transform 0.3s ease;
  }
`;

export const Back = styled(Icon)`
  position: absolute;
  top: 24px;
  left: 16px;
  background-color: rgba(27, 27, 27, 0.51);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  width: 42px;
  height: 42px;
  border-radius: 50%;
  z-index: 10;
`;
