import React, {useState} from 'react';
import {useQuery} from '@apollo/client';
import Skeleton from 'react-loading-skeleton';

import {TRENDS} from 'queries';
import {Wrapper, TrendImage, TrendItem, TrendsList, AddNew, InputFile} from './styled';
import FlashVideo from './components/FlashVideo';
import productPlaceholder from 'images/placeholders/product.jpg';
import Icon from 'components/Icon';
import {v1} from 'uuid';
import {useHistory} from 'react-router';
import {useUser} from 'hooks/reactiveVars';
import useGlobal from 'store';

const Trends = () => {
  const [, {setIsRequireAuth}] = useGlobal();
  const [user] = useUser();
  const {username} = user || {};
  const {push} = useHistory();
  const variables = {
    first: 10,
    where: {testSampleType: 'flash'}
  };
  const {data, loading, error, fetchMore} = useQuery(TRENDS, {
    variables,
    notifyOnNetworkStatusChange: true
  });
  const {endCursor, hasNextPage} = data?.posts?.pageInfo || {};
  const [active, setActive] = useState(false);
  const [initialSlide, setInitialSlide] = useState(0);

  const onPostClick = (postIndex) => {
    setInitialSlide(postIndex);
    setActive(true);
  };

  async function onChange(e) {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      try {
        const medias = await Promise.all(
          files.map((file) => {
            return new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.addEventListener('load', (ev) => {
                resolve({
                  id: v1(),
                  file,
                  result: ev.target.result
                });
              });
              reader.addEventListener('error', reject);
              reader.readAsDataURL(file);
            });
          })
        );
        push('/post/new', {medias});
      } catch (err) {
        console.log(`err`, err);
      }
    }
  }

  function doFetchMore() {
    if (!loading && hasNextPage) {
      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.posts?.nodes || [];
          const newNodes = fetchMoreResult?.posts?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            posts: {
              ...fetchMoreResult?.posts,
              nodes
            }
          };
        },
        variables: {
          ...variables,
          after: endCursor
        }
      });
    }
  }

  const galleryImages = data?.posts?.nodes?.map((a) => a?.galleryImages?.nodes)?.flat() || [];
  return (
    <Wrapper>
      <TrendsList>
        <AddNew onClick={username ? null : () => setIsRequireAuth(true)}>
          {username ? <InputFile onChange={onChange} type="file" /> : null}
          <Icon type="plusRounded" width={29} height={29} fill="#fff" />
        </AddNew>
        {loading &&
          [...Array(8).keys()].map((i) => (
            <Skeleton key={i} style={{display: 'flex', marginRight: 8, width: 92, borderRadius: 8}} height={135} />
          ))}
        {galleryImages?.map((mediaItem, index) => (
          <TrendItem onClick={() => onPostClick(index)} key={mediaItem?.id}>
            <TrendImage src={mediaItem?.poster?.sourceUrl || productPlaceholder} alt={mediaItem?.poster?.altText} />
            <Icon type="playCircle" width={29} height={29} fill="#fff" />
          </TrendItem>
        ))}
        <FlashVideo
          doFetchMore={doFetchMore}
          hasNextPage={hasNextPage}
          data={data}
          active={active}
          setActive={setActive}
          initialSlide={initialSlide}
        />
      </TrendsList>
    </Wrapper>
  );
};

export default Trends;
