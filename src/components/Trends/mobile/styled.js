import styled from 'styled-components';

export const Wrapper = styled.div``;

export const TrendsList = styled.ul`
  display: flex;
  padding: 16px;
  overflow-x: auto;
`;

export const TrendItem = styled.li`
  position: relative;
  & + & {
    margin-left: 8px;
  }

  & i {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const TrendImage = styled.img`
  width: 92px;
  height: 135px;
  border: 1px solid #efefef;
  border-radius: 8px;
  object-fit: ${({placeholder}) => (placeholder ? '100% 100%' : 'cover')};
`;

export const AddNew = styled.li`
  position: relative;
  width: 92px;
  height: 135px;
  flex-shrink: 0;
  border: 1px solid #efefef;
  border-radius: 8px;
  margin-right: 8px;

  & i {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const InputFile = styled.input`
  opacity: 0;
  position: absolute;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  z-index: 1;
  width: 100%;
`;
