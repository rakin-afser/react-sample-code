import React from 'react';
import {bool} from 'prop-types';

import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';

import {Wrapper, ActionButton} from './styled';

const Actions = ({inline, item = {}}) => {
  const [, setGlobalState] = useGlobal();
  const [user] = useUser();
  const {databaseId, seller, slug, name} = item;

  const onDotsClick = () => {
    if (user?.databaseId) {
      setGlobalState.setActionsPopup(true, {productId: databaseId, seller, slug, name});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  return (
    <Wrapper>
      <ActionButton inline={inline} onClick={onDotsClick}>
        <span />
        <span />
        <span />
      </ActionButton>
    </Wrapper>
  );
};

Actions.defaultProps = {
  inline: false
};

Actions.propTypes = {
  inline: bool
};

export default Actions;
