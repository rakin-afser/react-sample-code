import React, {useEffect, useState, useRef} from 'react';
import Div100vh from 'react-div-100vh';
import Scrollbar from 'react-scrollbars-custom';
import {useLazyQuery, useMutation} from '@apollo/client';
import FollowBtn from 'components/ShopsToFollow/components/FollowBtn';
import {useHistory} from 'react-router-dom';

import useGlobal from 'store';
import Icon from 'components/Icon';
import {
  Popup,
  PopupOverlay,
  PopupContent,
  Item,
  ItemLabel,
  ItemRight,
  AddedToWishlist,
  IconShare,
  ItemThumb,
  ItemName,
  Title,
  Search,
  SearchContainer,
  SearchReset,
  AddNewList,
  List,
  ListItem,
  ListItemCheck,
  ListItemLock,
  Field,
  FieldGroup,
  FieldLabel,
  Buttons,
  CreateButton,
  CancelButton,
  SavedTo,
  SavedIcon,
  RemoveFromListIcon
} from './styled';
import SearchIcon from 'assets/Search';
import CloseIcon from 'assets/CloseIcon';
import {
  GET_LISTS_BY_USER,
  ADD_LIST,
  ADD_PRODUCT_TO_LIST,
  DELETE_PRODUCT_FROM_LIST
} from 'components/AddToList/api/queries';
import {useUser} from 'hooks/reactiveVars';
import useShare from 'hooks/useShare';
import userPlaceholder from 'images/placeholders/user.jpg';

const ActionPopup = () => {
  const history = useHistory();
  const [globalState, setGlobalState] = useGlobal();
  const {productId, seller, name, slug, callback} = globalState.actionsPopupData || {};
  const searchInput = useRef();
  const [user] = useUser();
  const userId = user?.databaseId;
  const {share} = useShare();

  const [getListsByUser, {data: listsData, loading: listsLoading, error: listsError}] = useLazyQuery(
    GET_LISTS_BY_USER,
    {
      variables: {where: {user_id: +userId}}
    }
  );

  const [addNewList, {data: addListData, loading: addListLoading, error: addListError}] = useMutation(ADD_LIST, {
    variables: {userId: +userId}
  });

  const [
    addProductToList,
    {data: addProductToListData, loading: addProductToListLoading, error: addProductToListError}
  ] = useMutation(ADD_PRODUCT_TO_LIST, {variables: {userId: +userId}});

  const [
    deleteProductFromList,
    {data: deleteProductFromListData, loading: deleteProductFromListLoading, error: deleteProductFromListError}
  ] = useMutation(DELETE_PRODUCT_FROM_LIST, {variables: {userId: +userId}});

  const userLists = listsData?.lists?.nodes || [];
  const wishlist = userLists?.find((item) => item?.list_name?.toLowerCase() === 'wishlist') || {};

  const [loading, setLoading] = useState(null);
  const [followed, setFollowed] = useState(seller?.followed);
  const [defaultShow, setDefaultShow] = useState(false);
  const [addToList, setAddToList] = useState(false);
  const [createNewList, setCreateNewList] = useState(false);
  const [willPrivate, setWillPrivate] = useState(false);
  const [newList, setNewList] = useState('');
  const [searchValue, setSearchValue] = useState('');
  const [savedToList, setSavedToList] = useState(null);
  const [removedFromList, setRemovedFromList] = useState(null);
  const showSaved = savedToList && savedToList.title;
  const showRemoved = removedFromList && removedFromList.title;

  useEffect(() => {
    setFollowed(seller?.followed);
  }, [seller]);

  useEffect(() => {
    if (userId && addToList) {
      getListsByUser();
    }
  }, [userId, addToList, getListsByUser]);

  const checkAdded = (item) => {
    return (
      item?.addedProducts?.nodes?.length &&
      item?.addedProducts?.nodes?.map((item) => item?.databaseId)?.includes(productId)
    );
  };

  const onClose = () => {
    setGlobalState.setActionsPopup(false);
  };

  const addToListClick = (item) => {
    setLoading(item?.list_id);

    if (checkAdded(item)) {
      deleteProductFromList({
        variables: {listId: item?.list_id, productId},
        update(cache, {newData}) {
          cache.modify({
            fields: {
              lists() {
                return newData;
              }
            }
          });
        }
      });

      setTimeout(() => {
        if (callback) callback(false);
        setRemovedFromList(item?.list_name);
        setLoading(null);
      }, 500);
    } else {
      addProductToList({
        variables: {listId: item?.list_id, productId},
        update(cache, {newData}) {
          cache.modify({
            fields: {
              lists() {
                return newData;
              }
            }
          });
        }
      });

      setTimeout(() => {
        if (callback) callback(true);
        setSavedToList(item?.list_name);
        setLoading(null);
      }, 500);
    }
  };

  const onShare = () => {
    share({title: name, type: 'product', text: 'I would like to recommend this product at testSample', data: {slug}});
  };

  const renderWishlist = () => {
    if (loading === wishlist?.list_id) {
      return <Icon type="loader" svgStyle={{width: 24, height: 5, fill: '#ED484F'}} />;
    }

    if (checkAdded(wishlist)) {
      return (
        <AddedToWishlist>
          <AddedToWishlist>
            <RemoveFromListIcon>
              <Icon type="close" svgStyle={{color: '#000'}} />
            </RemoveFromListIcon>
            <span>Remove</span>
          </AddedToWishlist>
        </AddedToWishlist>
      );
    }

    return <Icon type="plus" width={14} height={14} />;
  };

  useEffect(() => {
    if (globalState.actionsPopup) {
      document.body.classList.add('overflow-hidden');
      setDefaultShow(true);
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [globalState.actionsPopup]);

  useEffect(() => {
    if (savedToList && savedToList.title) {
      setTimeout(() => {
        setSavedToList(null);
        document.body.classList.remove('overflow-hidden');
        setGlobalState.setActionsPopup(false);
      }, 4000);
    }
  }, [setGlobalState, savedToList]);

  useEffect(() => {
    if (removedFromList && removedFromList.title) {
      setTimeout(() => {
        setRemovedFromList(null);
        document.body.classList.remove('overflow-hidden');
        setGlobalState.setActionsPopup(false);
      }, 4000);
    }
  }, [removedFromList, setGlobalState]);

  return (
    <Popup show={globalState.actionsPopup || showSaved || showRemoved}>
      <Div100vh
        style={{
          height: '100vh',
          width: '100%',
          maxHeight: 'calc(100rvh)',
          display: 'flex'
        }}
      >
        <PopupOverlay
          show={globalState.actionsPopup}
          showSaved={showSaved || showRemoved}
          onClick={() => {
            setGlobalState.setActionsPopup(false);
            setDefaultShow(false);
            setAddToList(false);
            setCreateNewList(false);
            setSearchValue('');
            setSavedToList(null);
            setRemovedFromList(null);
          }}
        />
        <PopupContent
          style={{
            padding: '20px 24px',
            boxShadow: '0px 4px 20px rgba(0, 0, 0, 0.15)'
          }}
          show={showSaved || showRemoved}
        >
          {savedToList && savedToList.title ? (
            <SavedTo>
              <SavedIcon>
                <Icon type="checkbox" />
              </SavedIcon>
              <span>
                Saved to “{savedToList.title}” {savedToList.private ? <Icon type="lock" /> : null}
              </span>
              <a href="#" onClick={() => {}}>
                Change
              </a>
            </SavedTo>
          ) : null}

          {removedFromList && removedFromList.title ? (
            <SavedTo>
              <SavedIcon>
                <Icon type="checkbox" />
              </SavedIcon>
              <span>
                Removed from “{removedFromList.title}” {removedFromList.private ? <Icon type="lock" /> : null}
              </span>
              <a href="#" onClick={() => {}}>
                Change
              </a>
            </SavedTo>
          ) : null}
        </PopupContent>

        <PopupContent show={defaultShow}>
          <Item onClick={() => addToListClick(wishlist)}>
            <ItemLabel>
              <span>Add to WishList</span>
              <Icon type="lock" />
            </ItemLabel>
            <ItemRight>{renderWishlist()}</ItemRight>
          </Item>

          <Item
            onClick={() => {
              setDefaultShow(false);
              setAddToList(true);
            }}
          >
            <ItemLabel>
              <span>My Lists</span>
            </ItemLabel>
            <ItemRight>
              <Icon type="plus" width={14} height={14} />
            </ItemRight>
          </Item>

          <Item>
            <ItemLabel>
              <span>Share</span>
            </ItemLabel>
            <ItemRight>
              <IconShare onClick={onShare}>
                <Icon type="share" svgStyle={{width: 13, height: 15}} />
              </IconShare>
            </ItemRight>
          </Item>

          <Item>
            <ItemThumb
              src={seller?.gravatar || userPlaceholder}
              onClick={() => {
                history.push(`/shop/${seller?.id}/products`);
                onClose();
              }}
            />
            <ItemName>{seller?.name}</ItemName>
            <FollowBtn storeId={seller?.id} onCompleted={() => setFollowed(!followed)} isFollowed={followed} />
          </Item>
        </PopupContent>

        <PopupContent show={addToList}>
          <Title>
            Add to List
            <span
              onClick={() => {
                setAddToList(false);
                setDefaultShow(true);
              }}
            >
              Back
            </span>
          </Title>

          <SearchContainer>
            <SearchIcon color="#999999" />
            <Search
              value={searchValue}
              onChange={(e) => {
                const {value} = e.target;
                setSearchValue(value);
              }}
              ref={searchInput}
              placeholder="Search your Lists"
            />

            {searchValue.length > 0 ? (
              <SearchReset onClick={() => setSearchValue('')}>
                <CloseIcon width={12} height={12} color="#fff" />
              </SearchReset>
            ) : null}
          </SearchContainer>

          <List>
            <Scrollbar
              disableTracksWidthCompensation
              style={{height: 160}}
              trackXProps={{
                renderer: (props) => {
                  const {elementRef, ...restProps} = props;
                  return <span {...restProps} ref={elementRef} className="TrackX" />;
                }
              }}
            >
              {userLists ? (
                userLists
                  ?.filter((item) => item?.list_name.toLowerCase().includes(searchValue.toLowerCase()))
                  ?.map((item, key) => {
                    return (
                      <ListItem
                        key={key}
                        private={item.is_private}
                        checked={checkAdded(item)}
                        onClick={() => addToListClick(item)}
                      >
                        {item.list_name}
                        {item.is_private && (
                          <ListItemLock>
                            <Icon type="lock" />
                          </ListItemLock>
                        )}
                        {loading === item.list_id && (
                          <AddedToWishlist>
                            <Icon type="loader" svgStyle={{width: 20, height: 4, fill: '#ED484F'}} />
                          </AddedToWishlist>
                        )}
                        {checkAdded(item) && loading !== item.list_id ? (
                          <AddedToWishlist>
                            <AddedToWishlist>
                              <RemoveFromListIcon>
                                <Icon type="close" svgStyle={{color: '#000'}} />
                              </RemoveFromListIcon>
                              <span>Remove</span>
                            </AddedToWishlist>
                          </AddedToWishlist>
                        ) : null}
                      </ListItem>
                    );
                  })
              ) : (
                <ListItem style={{justifyContent: 'center'}}>Your list is empty!</ListItem>
              )}
            </Scrollbar>
          </List>

          <AddNewList
            onClick={() => {
              setAddToList(false);
              setCreateNewList(true);
            }}
          >
            <Icon type="plus" />
            <span>Create New List</span>
          </AddNewList>
        </PopupContent>

        <PopupContent show={createNewList}>
          <Title>Create New List</Title>

          <FieldGroup>
            <FieldLabel htmlFor="list">Enter List Name</FieldLabel>
            <Field
              id="list"
              value={newList}
              onChange={(e) => {
                setNewList(e.target.value);
              }}
              placeholder="Your List Name"
            />
          </FieldGroup>

          <List>
            <ListItem
              checked={willPrivate}
              onClick={() => {
                setWillPrivate(!willPrivate);
              }}
            >
              Private{' '}
              <ListItemLock>
                <Icon type="lock" />
              </ListItemLock>
              {willPrivate && (
                <ListItemCheck>
                  <Icon type="checkbox" />
                </ListItemCheck>
              )}
            </ListItem>
          </List>

          <Buttons>
            <CreateButton
              onClick={() => {
                addNewList({
                  variables: {listName: newList, isPrivate: willPrivate},
                  update(cache, {newData}) {
                    cache.modify({
                      fields: {
                        lists() {
                          return newData;
                        }
                      }
                    });
                  }
                });
                setCreateNewList(false);
                setSavedToList({
                  title: newList,
                  private: willPrivate
                });
              }}
            >
              Create
            </CreateButton>
            <CancelButton
              onClick={() => {
                setAddToList(true);
                setCreateNewList(false);
              }}
            >
              Cancel
            </CancelButton>
          </Buttons>
        </PopupContent>
      </Div100vh>
    </Popup>
  );
};

ActionPopup.defaultProps = {};

ActionPopup.propTypes = {};

export default ActionPopup;
