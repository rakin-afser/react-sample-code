import React from 'react';
import PropTypes from 'prop-types';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';
import Btn from 'components/Btn';

const FollowBtn = ({isFollowed, id}) => {
  const [mutationFollowStore] = useFollowUnFollowStoreMutation({followed: isFollowed});

  return (
    <Btn
      kind={isFollowed ? 'follow-md' : 'following-md'}
      onClick={() =>
        mutationFollowStore({
          variables: {input: {id: parseInt(id)}}
        })
      }
    />
  );
};

FollowBtn.defaultProps = {
  isFollowed: false
};

FollowBtn.propTypes = {
  isFollowed: PropTypes.bool,
  id: PropTypes.string.isRequired
};

export default FollowBtn;
