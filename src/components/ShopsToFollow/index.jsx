import React from 'react';
import parse from 'html-react-parser';
import {useQuery, gql} from '@apollo/client';
import {useHistory} from 'react-router-dom';
import Icon from 'components/Icon';
import StarRating from 'components/StarRating';
import avatarPlaceholder from 'images/avatarPlaceholder.png';
import Error from 'components/Error';
import FollowBtn from './components/FollowBtn';
import {Heading, Line, Shops, View, RatingWrapper, Img, Title} from './styled';

const STORES = gql`
  query Stores {
    stores(first: 3, where: {sortBy: mostPopular}) {
      nodes {
        id
        name
        storeUrl
        rating
        followed
        gravatar
      }
    }
  }
`;

const ShopsToFollow = ({forFeed}) => {
  const history = useHistory();
  const {data, loading, error} = useQuery(STORES);
  const filtredShops = data?.stores?.nodes?.filter((el) => el.name !== '');

  if (error) return <Error />;

  return (
    <Shops forFeed={forFeed}>
      <Line>
        <Heading>Shops to follow</Heading>
        <View onClick={() => history.push(`/search/stores/`)}>
          View All <Icon type="arrow" width={15} height={12} color="#666666" />
        </View>
      </Line>

      {loading ? (
        <Icon type="loader" />
      ) : (
        filtredShops.map((el, index) => (
          <Line forFeed={forFeed} key={index}>
            <div style={{display: 'flex'}}>
              <Img
                onClick={() => history.push(`/shop/${el.storeUrl}/products`)}
                src={el?.gravatar || avatarPlaceholder}
                alt={el?.name}
              />
              <div>
                <Title onClick={() => history.push(`/shop/${el.storeUrl}/products`)}>{parse(el?.name || '')}</Title>
                <RatingWrapper style={{display: 'flex'}}>
                  <StarRating stars={el?.rating} starHeight={8} starWidth={8} />
                  {el?.rating ? <span>({el?.rating})</span> : null}
                </RatingWrapper>
              </div>
            </div>
            <FollowBtn id={el.id} isFollowed={el?.followed} />
          </Line>
        ))
      )}
    </Shops>
  );
};
export default ShopsToFollow;
