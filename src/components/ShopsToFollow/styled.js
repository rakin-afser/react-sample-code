import styled from 'styled-components/macro';
import {blue} from 'constants/colors';

export const Shops = styled.div`
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  padding: 0 25px 24px 19px;

  ${({forFeed}) =>
    forFeed &&
    `
    box-shadow: none;
    padding: 0;
  `};
`;

export const Line = styled.div`
  position: relative;
  padding: 22px 0 5px;
  display: flex;
  align-items: center;
  justify-content: ${({flexStart}) => (flexStart ? 'flex-start' : 'space-between')};
  margin-bottom: 0 !important;

  span {
    font-weight: normal;
    font-size: 12px;
    color: #464646;
    margin-left: 8px;
  }

  img {
    width: 40px;
    height: 40px;
    margin-right: 16px;
    border-radius: 50%;
    transition: ease 0.4s;
    box-shadow: 0 0 0 rgba(000, 000, 000, 0);
    cursor: pointer;

    &:hover {
      box-shadow: 0 0 4px rgba(000, 000, 000, 0.5);
    }
  }

  h3 {
    font-size: 14px;
    margin-bottom: 2px;
    cursor: pointer;
    transition: ease 0.4s;
    text-overflow: ellipsis;
    overflow: hidden;
    width: 110px;
    white-space: nowrap;

    &:hover {
      color: ${blue};
    }
  }

  button {
    font-size: 12px;
  }

  &:first-child {
    margin-bottom: 40px;
  }

  &:last-child {
    margin-bottom: 40px;
  }

  ${({forFeed}) =>
    forFeed &&
    `
    img {
      border-radius: 6px;
    }
  `};
`;

export const Heading = styled.h2`
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 18px;
  line-height: 140%;
  color: #000;
  margin: 0;
`;

export const View = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #000000;
  transition: ease 0.4s;
  cursor: pointer;

  svg {
    transition: ease 0.4s;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    color: ${blue};

    svg {
      transform: scale(1.2) translateX(4px);

      path {
        fill: ${blue};
      }
    }
  }

  i {
    position: relative;
    top: 2px;
    margin-left: 5px;
  }
`;

export const RatingWrapper = styled.div`
  svg {
    margin: 0;
  }
`;

export const Img = styled.img``;

export const Title = styled.h3``;
