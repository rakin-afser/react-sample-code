import styled from 'styled-components/macro';
import {blue} from 'constants/colors';

export const Shops = styled.div`
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  padding: 0 0 24px 0;
  margin-bottom: 8px;
  border: 1px solid #efefef;
`;

export const Line = styled.div`
  position: relative;
  padding: 18px 16px 5px;
  display: flex;
  align-items: center;
  justify-content: ${({flexStart}) => (flexStart ? 'flex-start' : 'space-between')};

  span {
    font-weight: normal;
    font-size: 12px;
    color: #464646;
    margin-left: 8px;
  }

  img {
    width: 40px;
    height: 40px;
    margin-right: 16px;
  }

  h3 {
    font-size: 14px;
    line-height: 1;
    margin-top: 4px;
    margin-bottom: 2px;
  }

  button {
    width: 76px;
    font-size: 12px;
  }

  &:first-child {
    margin-bottom: 3px;
    padding: 14px 16px 5px;
  }
`;

export const Heading = styled.h2`
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 16px;
  line-height: 140%;
  color: #000;
  margin: 0;
`;

export const View = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #000000;
  transition: ease 0.4s;
  cursor: pointer;

  svg {
    transition: ease 0.4s;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    color: ${blue};

    svg {
      transform: scale(1.2) translateX(4px);

      path {
        fill: ${blue};
      }
    }
  }

  i {
    position: relative;
    margin-left: 5px;
  }
`;

export const RatingWrapper = styled.div`
  svg {
    margin-left: 0;
  }

  svg + svg {
    margin-left: 2px;
  }
`;
