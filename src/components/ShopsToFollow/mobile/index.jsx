import React from 'react';
import Icon from 'components/Icon';
import StarRating from 'components/StarRating';
import Button from 'components/Buttons';
import avatar from 'images/avatar.png';
import {Heading, Line, Shops, View, RatingWrapper} from './styled';

const data = [
  {avatar: avatar, title: 'Louis Vuitton'},
  {avatar: avatar, title: 'Zara'},
  {avatar: avatar, title: 'Miu Miu'}
];

const ShopsToFollow = (props) => {
  return (
    <Shops>
      <Line>
        <Heading>Shops to follow</Heading>
        <View>
          View All <Icon type="arrow" width={6} height={9} color="#666666" />
        </View>
      </Line>
      {data.map((el, index) => (
        <Line key={index}>
          <div style={{display: 'flex'}}>
            <img src={el.avatar} alt="avatar" />
            <div>
              <h3>{el.title}</h3>
              <RatingWrapper style={{display: 'flex'}}>
                <StarRating starHeight={10} starWidth={10} />
                <span>(335)</span>
              </RatingWrapper>
            </div>
          </div>
          <Button type="follow" />
        </Line>
      ))}
    </Shops>
  );
};

export default ShopsToFollow;
