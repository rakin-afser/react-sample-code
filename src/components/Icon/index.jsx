import React from 'react';

import AddMessage from 'assets/AddMessage';
import Arrow from 'assets/Arrow';
import ArrowFooter from 'assets/ArrowFooter';
import ArrowDown from 'assets/ArrowDown';
import BellIcon from 'assets/BellIcon';
import BurgerIcon from 'assets/BurgerIcon';
import Bookmark from 'assets/Bookmark';
import BoxIcon from 'assets/BoxIcon';
import Check from 'assets/Check';
import Checkmark from 'assets/Checkmark';
import Plus from 'assets/Plus';
import Heart from 'assets/Heart';
import Like from 'assets/Like';
import Liked from 'assets/Liked';
import Shop from 'assets/Shop';
import Bag from 'assets/Bag';
import Bags from 'assets/Bags';
import Pencil from 'assets/Pencil';
import Marker from 'assets/Marker';
import Eye from 'assets/Eye';
import CoinsProfile from 'assets/Profile/Coins';
import Close from 'assets/CloseIcon';
import ArrowBack from 'assets/ArrowBack';
import Star from 'assets/Star';
import Coins from 'assets/Coins';
import Message from 'assets/MessageIcon';
import Share from 'assets/Share';
import BookmarkIcon from 'assets/BookmarkIcon';
import FilledBookmark from 'assets/FilledBookmark';
import Send from 'assets/Send';
import Checkbox from 'assets/Checkbox';
import ShoppingCart from 'assets/ShoppingCart';
import Lens from 'assets/ProductPage/Lens';
import RoundPlus from 'assets/ProductPage/RoundPlus';
import RoundCheckbox from 'assets/RoundCheckbox';
import Lock from 'assets/Lock';
import Filter from 'assets/Filter';
import Loader from 'assets/Loader';
import MessageDots from 'assets/MessageDots';
import SendMessage from 'assets/SendMessage';
import Settings from 'assets/Settings';
import PlayVideo from 'assets/PlayVideo';
import Cancel from 'assets/Cancel';
import CheckCircle from 'assets/CheckCircle';
import CheckCircleFilled from 'assets/CheckCircleFilled';
import CloseCircle from 'assets/CloseCircle';
import Edit from 'assets/Edit';
import Back from 'assets/Back';
import Search from 'assets/Search';
import More from 'assets/More';
import Dots from 'assets/Dots';
import Cross from 'assets/Cross';
import CrossThin from 'assets/CrossThin';
import Tile from 'assets/Tile';
import TileOutlined from 'assets/TileOutlined';
import List from 'assets/List';
import House from 'assets/House';
import Work from 'assets/Work';
import Instagram from 'assets/Instagram';
import InstagramGrad from 'assets/InstagramGrad';
import Facebook from 'assets/Facebook';
import SmallFacebook from 'assets/SmallFacebook';
import SmallInstagram from 'assets/SmallInstagram';
import SmallCamera from 'assets/SmallCamera';
import SmallClip from 'assets/SmallClip';
import Play from 'assets/Play';
import PlayRed from 'assets/PlayRed';
import SocialFacebook from 'assets/SocialFacebook';
import SocialInstagram from 'assets/SocialInstagram';
import SocialLinkedIn from 'assets/SocialLInkedIn';
import SocialSnapChat from 'assets/SocialSnapChat';
import SocialTikTok from 'assets/SocialTikTok';
import SocialTwitter from 'assets/SocialTwitter';
import SocialYouTube from 'assets/SocialYouTube';
import QuestionCircle from 'assets/QuestionCircle';
import Speaker from 'assets/Speaker';
import Gear from 'assets/Gear';
import ShopNew from 'assets/ShopNew';

import DoubleCheck from 'assets/DoubleCheck';
import Smile from 'assets/Smile';
import {Container} from './styled';
import ChevronRight from '../../assets/ChevronRight';
import ChevronDown from 'assets/ChevronDown';
import Export from 'assets/Export';
import Cards from 'assets/Cards';
import Feed from 'assets/Feed';
import Orders from 'assets/Profile/Orders';
import Logout from 'assets/Logout';
import User from 'assets/Profile/User';
import GearThin from 'assets/GearThin';
import StarOutlined from 'assets/StarOutlined';
import CircleInfo from 'assets/CircleInfo';
import Trash from 'assets/Trash';
import Flag from 'assets/Flag';
import Expand from 'assets/Expand';
import Unmuted from 'assets/Unmuted';
import Muted from 'assets/Muted';
import ThinHouse from 'assets/HamburgerMenu/ThinHouse';
import ThinShop from 'assets/HamburgerMenu/ThinShop';
import ThinFeed from 'assets/HamburgerMenu/Feed';
import Car from 'assets/HamburgerMenu/Car';
import CheckMark from 'assets/HamburgerMenu/CheckMark';
import Coin from 'assets/Checkout/Coin';
import Google from 'assets/Google';
import FacebookRound from 'assets/FacebookRound';
import ChevronLeft from 'assets/ChevronLeft';
import Camera from 'assets/Camera';
import PlayCircle from 'assets/PlayCircle';
import Whatsapp from 'assets/HamburgerMenu/Whatsapp';
import Twitter from 'assets/HamburgerMenu/Twitter';
import BoldShareIcon from 'assets/HamburgerMenu/BoldShareIcon';
import Copy from 'assets/HamburgerMenu/Copy';
import Cart from 'assets/HamburgerMenu/Cart';
import BoldHouse from 'assets/HamburgerMenu/BoldHouse';
import PlayRounded from 'assets/Posts/PlayRounded';
import PlusRounded from 'assets/Posts/PlusRounded';
import Target from 'assets/Target';
import FeedNew from 'assets/FeedNew';
import ShopHamburger from 'assets/ShopHamburger';
import CategoriesNew from 'assets/CategoriesNew';
import Dashboard from 'assets/Dashboard';
import MessageSquare from 'assets/MessageSquare';
import DotsVertical from 'assets/DotsVertical';

export const icons = {
  addMessage: AddMessage,
  arrow: Arrow,
  arrowFooter: ArrowFooter,
  arrowDown: ArrowDown,
  bellIcon: BellIcon,
  burgerIcon: BurgerIcon,
  bookmark: Bookmark,
  boxIcon: BoxIcon,
  check: Check,
  checkmark: Checkmark,
  checkMark: CheckMark,
  checkCircle: CheckCircle,
  checkCircleFilled: CheckCircleFilled,
  plus: Plus,
  heart: Heart,
  like: Like,
  liked: Liked,
  shop: Shop,
  bags: Bags,
  bag: Bag,
  car: Car,
  pencil: Pencil,
  marker: Marker,
  eye: Eye,
  cancel: Cancel,
  coinsProfile: CoinsProfile,
  coins: Coins,
  coin: Coin,
  close: Close,
  closeCircle: CloseCircle,
  arrowBack: ArrowBack,
  star: Star,
  starOutlined: StarOutlined,
  message: Message,
  messageDots: MessageDots,
  share: Share,
  bookmarkIcon: BookmarkIcon,
  filledBookmarkIcon: FilledBookmark,
  send: Send,
  checkbox: Checkbox,
  cart: ShoppingCart,
  lens: Lens,
  roundPlus: RoundPlus,
  roundCheckbox: RoundCheckbox,
  lock: Lock,
  loader: Loader,
  filter: Filter,
  sendMessage: SendMessage,
  settings: Settings,
  playVideo: PlayVideo,
  edit: Edit,
  back: Back,
  search: Search,
  more: More,
  chevronLeft: ChevronLeft,
  chevronRight: ChevronRight,
  cross: Cross,
  crossThin: CrossThin,
  dots: Dots,
  tile: Tile,
  tileOutlined: TileOutlined,
  list: List,
  house: House,
  work: Work,
  instagram: Instagram,
  instagramGrad: InstagramGrad,
  facebook: Facebook,
  smallFacebook: SmallFacebook,
  smallInstagram: SmallInstagram,
  smallCamera: SmallCamera,
  smallClip: SmallClip,
  play: Play,
  playRed: PlayRed,
  socialFacebook: SocialFacebook,
  socialInstagram: SocialInstagram,
  socialLinkedIn: SocialLinkedIn,
  socialSnapChat: SocialSnapChat,
  socialTikTok: SocialTikTok,
  socialTwitter: SocialTwitter,
  socialYouTube: SocialYouTube,
  thinFeed: ThinFeed,
  thinHouse: ThinHouse,
  thinShop: ThinShop,
  questionCircle: QuestionCircle,
  speaker: Speaker,
  gear: Gear,
  gearThin: GearThin,
  chevronDown: ChevronDown,
  smile: Smile,
  doubleCheck: DoubleCheck,
  export: Export,
  feed: Feed,
  cards: Cards,
  orders: Orders,
  logout: Logout,
  user: User,
  circleInfo: CircleInfo,
  trash: Trash,
  flag: Flag,
  expand: Expand,
  unmuted: Unmuted,
  muted: Muted,
  google: Google,
  facebookRound: FacebookRound,
  shopNew: ShopNew,
  camera: Camera,
  playCircle: PlayCircle,
  whatsapp: Whatsapp,
  twitter: Twitter,
  boldShareIcon: BoldShareIcon,
  copy: Copy,
  boldCart: Cart,
  boldHouse: BoldHouse,
  playRounded: PlayRounded,
  plusRounded: PlusRounded,
  target: Target,
  feedNew: FeedNew,
  shopHamburger: ShopHamburger,
  categoriesNew: CategoriesNew,
  dashboard: Dashboard,
  'msg-square': MessageSquare,
  dotsVertical: DotsVertical
};

const Icons = ({type, svgStyle, color, color2, width, height, onClick, ...props}) => {
  const Icon = icons[type];

  if (!icons[type]) return '';

  return (
    <Container {...props} onClick={onClick}>
      <Icon {...props} color={color} color2={color2} stroke={color} width={width} height={height} {...svgStyle} />
    </Container>
  );
};

export default Icons;
