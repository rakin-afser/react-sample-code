import styled from 'styled-components';

export const Container = styled.i`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  font-size: 0;
`;
