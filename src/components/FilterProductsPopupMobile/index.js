import React, {useEffect, useState} from 'react';
import Div100vh from 'react-div-100vh';
import useGlobal from 'store';
import {Popup, PopupOverlay, PopupContent, Item, ItemLabel} from './styled';
import {ReactComponent as Check} from 'images/icons/check.svg';

const sortBy = [
  {
    name: 'New Arrivals',
    value: 'arrivals',
    selected: false
  },
  {
    name: 'Featured Products',
    value: 'featured_products',
    selected: false
  },
  {
    name: 'Highest Price',
    value: 'ascending',
    selected: false
  },
  {
    name: 'Lowest Price',
    value: 'descending',
    selected: false
  },
  {
    name: 'Deals',
    value: 'deals',
    selected: false
  }
];

const FilterProductsPopupMobile = ({onClick, sortOptions = sortBy}) => {
  const [globalState, globalActions] = useGlobal();
  const [defaultShow, setDefaultShow] = useState(false);
  const [sort, setSort] = useState(sortOptions);

  useEffect(() => {
    if (globalState.productFiltersPopup) {
      document.body.classList.add('overflow-hidden');
      setDefaultShow(true);
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [globalState.productFiltersPopup]);

  function onSort(item) {
    onClick(item.value);
    setSort(sort.map((el) => (el.name === item.name ? {...el, selected: true} : {...el, selected: false})));
    globalActions.setProductFiltersPopup(false);
  }

  return (
    <Popup show={globalState.productFiltersPopup}>
      <Div100vh
        style={{
          height: '100vh',
          width: '100%',
          maxHeight: 'calc(100rvh)',
          display: 'flex'
        }}
      >
        <PopupOverlay
          show={globalState.productFiltersPopup}
          onClick={() => {
            globalActions.setProductFiltersPopup(false);
          }}
        />
        <PopupContent show={defaultShow}>
          {sort.map((item) => (
            <Item key={item.name} selected={item.selected} onClick={() => onSort(item)}>
              <ItemLabel>
                <span>{item.name}</span>
                {item.selected && <Check />}
              </ItemLabel>
            </Item>
          ))}
        </PopupContent>
      </Div100vh>
    </Popup>
  );
};

FilterProductsPopupMobile.defaultProps = {};

FilterProductsPopupMobile.propTypes = {};

export default FilterProductsPopupMobile;
