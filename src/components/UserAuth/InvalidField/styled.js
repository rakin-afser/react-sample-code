import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 7px 16px 9px;
  border-radius: 8px;
  background-color: #ee989b;
  display: flex;
  margin-bottom: 16px;
`;

export const TextWrapper = styled.div`
  color: #ffffff;
  line-height: 140%;
  margin-left: 20px;
`;

export const Heading = styled.h3`
  font-weight: 500;
  font-size: 14px;
  margin-bottom: 4px;
  color: #ffffff;
`;

export const Text = styled.p`
  font-weight: normal;
  font-size: 12px;
  margin-bottom: 0;
  color: #ffffff;
`;
