import React from 'react';
import Icon from 'components/Icon';

import {Wrapper, TextWrapper, Heading, Text} from './styled';

const InvalidField = () => (
  <Wrapper>
    <Icon type="cancel" color="#ED484F" />
    <TextWrapper>
      <Heading>Invalid Username or Password</Heading>
      <Text>Please try again or click Forgot Username or Forgot Password link below.</Text>
    </TextWrapper>
  </Wrapper>
);

export default InvalidField;
