import styled from 'styled-components';
import {Button, Modal} from 'antd';

export const Wrap = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const StyledModal = styled(Modal)`
  &&& {
    color: #464646;
    line-height: 140%;
    font-size: 16px;

    .ant-modal {
      &-content {
        border-radius: 0;
      }

      &-body {
        font-size: 16px;
      }
    }
  }
`;

export const Title = styled.h2`
  text-align: center;
  font-weight: 700;
  font-size: 28px;
  line-height: 120%;
  letter-spacing: 0.011em;
  margin-bottom: 24px;
`;

export const Desc = styled.p`
  text-align: center;
`;

export const Please = styled.p`
  margin: 36px 0 24px;
`;

export const Verify = styled(Button)`
  &&& {
    background: #ed484f;
    color: #ffffff;
    border-radius: 24px;
    padding: 0 20px;
    font-style: normal;
    font-weight: 500;
    line-height: 132%;
    letter-spacing: -0.024em;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

export const Resend = styled(Button)`
  &&& {
    margin-top: 36px;
    background-color: transparent;
    border: none;
    color: #1890ff;
  }
`;
