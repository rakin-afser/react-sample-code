import React, {useEffect, useState} from 'react';
import OTP from 'components/OTP';
import {StyledModal, Title, Desc, Please, Verify, Resend, Wrap} from './styled';
import Icon from 'components/Icon';
import {useMutation} from '@apollo/client';
import {ValidateOTP} from 'mutations';

const ModalOTP = (props) => {
  const [code, setCode] = useState();

  const [validateOtp, {data: dataValidateOtp, error: errorValidateOtp}] = useMutation(ValidateOTP);

  const {visible, setVisible, email, resend, onRegister} = props;

  function onVerify() {
    validateOtp({variables: {input: {email, otp: parseInt(code)}}});
  }

  useEffect(() => {
    if (dataValidateOtp?.validateOTP?.code === '200' && !errorValidateOtp) {
      onRegister();
    }
  }, [dataValidateOtp, errorValidateOtp]);

  return (
    <StyledModal
      visible={visible}
      footer={null}
      maskClosable={false}
      destroyOnClose
      onCancel={() => setVisible(false)}
      closeIcon={<Icon type="crossThin" fill="#000" />}
    >
      <Title>Verify Email</Title>
      <Desc>
        A new OTP code has been sent to your email as <strong>{email}</strong>
      </Desc>
      <Please>Please Enter OTP</Please>
      <OTP value={code} onChange={setCode} />
      <Wrap>
        <Verify type="danger" onClick={() => onVerify()}>
          Verify
        </Verify>
        <Resend onClick={resend}>Resend OTP</Resend>
      </Wrap>
    </StyledModal>
  );
};

export default ModalOTP;
