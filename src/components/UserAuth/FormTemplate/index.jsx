import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import Social from '../Social';
import CreateFields from '../CreateFields';
import Close from '../CloseButton';
import Arrow from 'assets/ArrowLeft';
import Logo from 'assets/LogoIcon';
import Icon from 'components/Icon';
import InvalidField from '../InvalidField';
import {
  Overlay,
  SignForm,
  Submit,
  FormItem,
  Checked,
  Heading,
  FieldsWrap,
  Title,
  Agreement,
  Line,
  LinkTo,
  BackToSign,
  Header,
  ForgotBlock,
  LineBlock,
  TopBlock,
  BottomBlock
} from './styled';
import {gql, useMutation} from '@apollo/client';
import ModalOTP from './components/ModalOTP';
import {useHistory} from 'react-router';
import {SendOTP} from 'mutations';

const Register = gql`
  mutation Register($input: RegisterNewCustomerInput!) {
    registerNewCustomer(input: $input) {
      message
      user_id
    }
  }
`;

const Login = gql`
  mutation Login($input: LoginUserInput!) {
    loginUser(input: $input) {
      message
      token
    }
  }
`;

const ValidateEmail = gql`
  mutation ValidateEmail($input: ValidateEmailInput!) {
    validateEmail(input: $input) {
      message
      code
    }
  }
`;

const ValidateUsername = gql`
  mutation ValidateUsername($input: ValidateUsernameInput!) {
    validateUsername(input: $input) {
      message
      code
    }
  }
`;

const FormTemplate = ({form, formData, formType, validator}) => {
  const [submitError, setSubmitError] = useState(false);
  const [visible, setVisible] = useState(false);
  const [email, setEmail] = useState('');
  const history = useHistory();

  useEffect(() => {
    document.querySelector('body').classList.add('overflow-hidden');
    document.querySelector('html').classList.add('overflow-hidden');
    return () => {
      document.querySelector('body').classList.remove('overflow-hidden');
      document.querySelector('html').classList.remove('overflow-hidden');
    };
  });

  const [login, {data}] = useMutation(Login);
  const [register, {data: dataRegister, error: errorRegister}] = useMutation(Register);

  const [validateEmail, {data: dataValidateEmail, error: errorValidateEmail}] = useMutation(ValidateEmail);
  const [validateUsername, {data: dataValidateUsername, error: errorValidateUsername}] = useMutation(ValidateUsername);
  const [sendOTP] = useMutation(SendOTP);

  useEffect(() => {
    // Validate email and username
    // Then open ModalOTP
    if (
      dataValidateEmail?.validateEmail?.code === 404 &&
      !errorValidateEmail &&
      dataValidateUsername?.validateUsername?.code === 404 &&
      !errorValidateUsername
    ) {
      setVisible(true);
      sendOTP({
        variables: {
          input: {
            email
          }
        }
      });
    }
  }, [dataValidateEmail, dataValidateUsername, email]);

  const {getFieldDecorator, validateFields} = form;
  const isSignIn = formType === 'signIn';
  const isSignUp = formType === 'signUp';
  const isForgot = formType === 'forgot';

  function onRegister() {
    const values = form.getFieldsValue();
    register({
      variables: {
        input: {
          email: values.Email,
          firstName: values.FirstName,
          lastName: values.LastName,
          password: values.Password,
          username: values.SelectUsername
        }
      }
    });
  }

  useEffect(() => {
    if (dataRegister?.registerNewCustomer?.user_id && !errorRegister) {
      history.push('/');
    }
  }, [dataRegister, errorRegister]);

  const onSubmitHandler = (e) => {
    e.preventDefault();
    validateFields(async (err, values) => {
      if (!err) {
        switch (formType) {
          case 'signIn':
            login({
              variables: {
                input: {username: values.EmailorUsername, password: values.Password}
              }
            });
            break;
          case 'signUp':
            validateEmail({
              variables: {
                input: {
                  email: values.Email
                }
              }
            });

            setEmail(values.Email);

            validateUsername({
              variables: {
                input: {
                  username: values.SelectUsername
                }
              }
            });
            break;

          default:
            break;
        }
      } else {
        console.log('Received values of form: ', values);
        setSubmitError(true);
      }
    });
  };

  function renderModal() {
    return (
      <ModalOTP
        onRegister={onRegister}
        visible={visible}
        setVisible={setVisible}
        email={email}
        resend={() => sendOTP({variables: {input: {email}}})}
      />
    );
  }

  return (
    <Overlay>
      <Header>
        <Logo />
      </Header>
      <SignForm onSubmit={onSubmitHandler}>
        <TopBlock>
          <Close />
          {isForgot ? (
            <BackToSign to="/auth/sign-in">
              <Arrow />
            </BackToSign>
          ) : null}
          <Heading>{formData.heading}</Heading>
          {submitError && <InvalidField />}
          <Title forgot={isForgot}>
            {formData.title}&#160;
            {!isForgot ? (
              <LinkTo block="true" to={formType === 'signIn' ? '/auth/sign-up' : '/auth/sign-in'} $fontWeight={600}>
                {formType === 'signIn' ? ' Create account' : 'Log in'}
              </LinkTo>
            ) : null}
          </Title>
          <FieldsWrap>
            {CreateFields(form, formData)}
            {isSignUp ? (
              <FormItem checkbox>
                {getFieldDecorator('agreement', {
                  valuePropName: 'checked',
                  rules: [{required: true, message: 'Please accept terms and conditions', type: 'boolean'}]
                })(
                  <Checked>
                    <Agreement>
                      I have read and I agree to comply with the&#160;
                      <LinkTo to="/">Terms & Conditions</LinkTo>&#160; of the&#160;
                      <LinkTo to="/">seller agreement</LinkTo>
                    </Agreement>
                  </Checked>
                )}
              </FormItem>
            ) : null}
            {isSignIn ? (
              <ForgotBlock>
                <LinkTo to="/forgot">Forgot Username?</LinkTo>
                <LinkTo to="/forgot">Forgot Password?</LinkTo>
              </ForgotBlock>
            ) : null}
          </FieldsWrap>
          <Submit forgot={isForgot} type="primary" htmlType="submit">
            <div>{formData.submitTitle}</div>
            <Icon type="arrow" color="#fff" width={12} height={10} />
          </Submit>
        </TopBlock>
        <BottomBlock>
          <LineBlock>
            <Line signIn={isSignIn} hide={isForgot} />
            <span>Or</span>
            <Line signIn={isSignIn} hide={isForgot} />
          </LineBlock>
          {!isForgot ? <Social /> : null}
        </BottomBlock>
      </SignForm>
    </Overlay>
  );
};

const WrappedFormTemplate = SignForm.create({name: 'auth'})(FormTemplate);

FormTemplate.propTypes = {
  form: PropTypes.isRequired
};

export default WrappedFormTemplate;
