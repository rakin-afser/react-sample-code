import React from 'react';
// @ deprecated
import FormTemplate from '../FormTemplate';

const formData = {
  heading: 'Sign in',
  title: "Don't have an account yet?",
  submitTitle: 'Sign in',
  fields: [
    {
      label: 'Email or Username ',
      type: 'text',
      errorMessage: 'Please enter a valid username or email address',
      isRequired: true
    },
    {
      label: 'Password',
      type: 'password',
      errorMessage: 'Please enter a valid password',
      isRequired: true
    }
  ]
};
const SignIn = () => <FormTemplate formData={formData} formType="signIn" />;
export default SignIn;
