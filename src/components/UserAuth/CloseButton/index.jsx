import React from 'react';
import {CloseButton} from './styled';
import CloseIcon from 'assets/CloseIcon';

const Close = () => (
	<CloseButton to='/'>
		<CloseIcon color="#999999" />
	</CloseButton>
);

export default Close;
