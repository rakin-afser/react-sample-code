import React from 'react';
import google from './img/google.png';
import facebook from './img/facebook.png';
import {SocialName, SocialButton, ButtonText} from './styled';

const Social = ({disabled}) => (
  <>
    <SocialButton disabled={disabled}>
      <img src={google} alt="" />
      <ButtonText>Continue with </ButtonText>
      <SocialName color="#287BDD">Google</SocialName>
    </SocialButton>
    <SocialButton disabled={disabled}>
      <img src={facebook} alt="" />
      <ButtonText>Continue with </ButtonText>
      <SocialName color="#287BDD">Facebook</SocialName>
    </SocialButton>
  </>
);

export default Social;
