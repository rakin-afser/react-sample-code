import styled from 'styled-components';
import {Button} from 'antd';

export const ButtonText = styled.div`
  margin-left: 45px;
  margin-bottom: -3px;

  @media (max-width: 425px) {
    margin-left: 20px;
    margin-bottom: -4px;
  }
`;

export const SocialName = styled.div`
  font-size: inherit;
  font-weight: 700;
  color: ${({color}) => color};
  margin-left: 4px;
  font-style: normal;
`;

export const SocialButton = styled(Button)`
  &&& {
    width: 220px;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    position: relative;
    width: 267px;
    height: 48px;
    border-color: #e4e4e4;
    border-radius: 100px;
    margin-bottom: 16px;
    color: #464646;
    position: relative;
    padding: 20px 0;

    @media (max-width: 425px) {
      width: 270px;
      padding: 25px 0;
      justify-content: center;
    }

    img {
      width: 20px;
      height: 20px;
      position: absolute;
      top: 50%;
      left: 15px;
      transform: translateY(-50%);

      @media (max-width: 425px) {
        left: 30px;
      }
    }

    opacity: ${({disabled}) => (disabled ? '0.5' : 1)};
  }
`;
