import styled from 'styled-components';
import {secondaryTextColor} from '../../../../constants/colors';

export const Container = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px 0;
`;

export const Line = styled.i`
  width: 100%;
  height: 1px;
  background: #e4e4e4;
`;

export const Title = styled.p`
  margin: 0;
  padding: 0 8px;
  color: ${secondaryTextColor}
  font-size: 14px;
`;
