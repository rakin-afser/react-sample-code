import React from 'react';
import {string} from 'prop-types';
import {Container, Line, Title} from './styled';

const SeparatorLineAndText = ({title}) => {
  return (
    <Container>
      <Line />
      <Title>{title}</Title>
      <Line />
    </Container>
  );
};

SeparatorLineAndText.defaultProps = {
  title: 'Or'
};

SeparatorLineAndText.propTypes = {
  title: string
};
export default SeparatorLineAndText;
