import styled from 'styled-components';
import {secondaryTextColor} from '../../../../constants/colors';

export const Btn = styled.button.attrs(({styles}) => ({style: {...styles}}))`
  opacity: ${({disabled}) => (disabled ? '0.5' : 1)}
  position: relative;
  width: 267px;
  height: 48px;
  border: 1px solid #E4E4E4;
  border-radius: 100px;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: ${secondaryTextColor}
  display: flex;
  align-items: center;
    
  svg {
    position: absolute;
    left: 31px;
  }  
`;

export const Text = styled.span`
  width: 100%;
  text-align: center;
  padding-left: ${({socialName}) => (socialName === 'facebook' ? '32px' : '22px')};
`;

export const SocialName = styled.span`
  color: ${({color}) => color}
  font-weight: 700
`;
