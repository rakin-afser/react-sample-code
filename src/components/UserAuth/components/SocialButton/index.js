import React, {useEffect} from 'react';
import Amplify, {Auth, Hub} from 'aws-amplify';
import PropTypes from 'prop-types';
import {Btn, SocialName, Text} from './styled';
import FacebookRound from 'assets/FacebookRound';
import Google from 'assets/Google';
import {useGoogleApi} from 'react-gapi';
import {useHistory} from 'react-router-dom';
import {getErrorByCode} from '../../../../util/responseDecoding';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import {localStorageKey} from 'constants/config';

const socialIcons = {
  facebook: <FacebookRound />,
  google: <Google />
};

const SocialButton = ({socialName, disabled, styles, onClick}) => {
  const [global, globalActions] = useGlobal();
  const {push} = useHistory();
  const [, setUser] = useUser();

  const handleClick = async (e, provider) => {
    e.preventDefault();
    if (onClick) {
      onClick();
      return;
    }
  };

  return (
    <Btn
      onClick={(e) => {
        handleClick(e, socialName === 'facebook' ? 'Facebook' : socialName === 'google' ? 'Google' : '');
      }}
      disabled={disabled}
      styles={styles}
      // type={'button'}
    >
      {socialIcons[socialName]}
      <Text socialName={socialName}>
        Continue with
        {socialName === 'facebook' ? (
          <SocialName color="#175195"> Facebook</SocialName>
        ) : socialName === 'google' ? (
          <SocialName color="#287BDD"> Google</SocialName>
        ) : (
          ''
        )}
      </Text>
    </Btn>
  );
};

SocialButton.propTypes = {
  socialName: PropTypes.string.isRequired,
  onClick: PropTypes.func
};

export default SocialButton;
