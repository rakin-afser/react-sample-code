import styled from 'styled-components';
import {secondaryTextColor} from '../../../../constants/colors';
import media from '../../../../constants/media';

export const Text = styled.p.attrs(({styles}) => ({style: {...styles}}))`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  text-align: center;
  padding: 0px 30px;
  color: ${secondaryTextColor}
  margin-top: 25px;
  
  @media (min-width: ${media.mobileMax}) {
    font-size: 16px;
    padding: 0px 20px;
  }
`;
