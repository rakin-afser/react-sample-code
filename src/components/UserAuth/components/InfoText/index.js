import React from 'react';
import {Text} from './styled';

const InfoText = ({text, styles}) => {
  return <Text styles={styles}>{text}</Text>;
};

export default InfoText;
