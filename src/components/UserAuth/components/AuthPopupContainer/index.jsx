import React, {useEffect} from 'react';
import {any} from 'prop-types';
import {PopupContainer, Container} from './styled';
import useGlobal from 'store';
import {useHistory} from 'react-router';

const AuthPopupContainer = ({children, showBackDrop, showPopup}) => {
  const [globalState] = useGlobal();
  const {push} = useHistory();
  useEffect(() => {
    document.querySelector('body').classList.add('overflow-hidden');
    document.querySelector('html').classList.add('overflow-hidden');
    return () => {
      document.querySelector('body').classList.remove('overflow-hidden');
      document.querySelector('html').classList.remove('overflow-hidden');
    };
  });

  return (
    <Container
      afterClose={() => push('/')}
      visible={showPopup ? showPopup : globalState.authPopup}
      closable={false}
      footer={false}
      wrapClassName="auth-modal-container"
      mask={showBackDrop}
      styles={{
        padding: 0,
        '& .ant-modal': {
          top: 10,
          width: 404
        }
      }}
      bodyStyle={{
        padding: 0
        // display: 'flex',
        // flexDirection: 'column',
        // alignItems: 'center',
        // width: 404,
        // // margin: '0 auto',
        // background: '#fff',
        // // boxShadow:' 0px 2px 20px rgba(0, 0, 0, 0.07)',
        // borderRadius: 8,

        // marginTop: 48
      }}
    >
      <PopupContainer>{children}</PopupContainer>
    </Container>
  );
};
AuthPopupContainer.propTypes = {
  children: any
};

export default AuthPopupContainer;
