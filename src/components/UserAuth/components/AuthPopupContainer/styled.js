import styled from 'styled-components';
import {Modal} from 'antd';
import media from '../../../../constants/media';

export const Container = styled(Modal)`
  &&& {
    .ant-modal {
      width: 404px !important;
      top: 10px !important;
    }
  }
  .auth-modal-container .ant-modal {
    width: 404px !important;
    top: 10px !important;
  }

  .ant-modal {
    width: 404px !important;
    top: 10px !important;
  }

  & > div.ant-modal {
    width: 404px !important;
    top: 10px !important;
  }
`;

export const Overlay = styled.div`
  // position: fixed;
  // top: 0;
  // right: 0;
  // bottom: 0;
  // left: 0;
  // display: flex;
  // justify-content: center;
  // align-items: flex-start;
  // background: rgba(0, 0, 0, 0.4);
  // z-index: 1100;
  // overflow: auto;
`;

export const PopupContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 404px;
  margin: 0 auto;
  background: #fff;
  box-shadow: 0px 2px 20px rgba(0, 0, 0, 0.07);
  border-radius: 8px;
  margin-top: 48px;

  @media (max-width: ${media.mobileMax}) {
    width: 100%;
    border-radius: 0;
    margin-top: 0;
    box-shadow: none;
  }
`;
