import React from 'react';
import {Container, LinkTo} from './styled';

const ForgotBox = ({backUrl}) => {
  return (
    <Container>
      <LinkTo to={{pathname: '/auth/recover-username', state: {backUrl: backUrl}}}>Forgot Username?</LinkTo>
      <LinkTo to={{pathname: '/auth/reset-password', state: {backUrl: backUrl}}}>Forgot Password?</LinkTo>
    </Container>
  );
};

export default ForgotBox;
