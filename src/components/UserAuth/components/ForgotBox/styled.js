import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const Container = styled.div`
  text-align: center;
  margin-top: 25px;

  & a:first-child {
    margin-right: 32px;
  }
`;

export const LinkTo = styled(Link)`
  display: inline-block;
  color: #4a90e2;
  font-weight: 500;
`;
