import React from 'react';
import {string, func, bool} from 'prop-types';
import {Container, Title, Icon} from './styled';
import CloseIcon from '../../../../assets/CloseIcon';
import ArrowBack from '../../../../assets/Back';

const AuthHeading = ({title, onClickBack, onClickClose, isMobile}) => {
  return (
    <Container>
      <Icon onClick={onClickBack} right={true}>
        {onClickBack && (
          <ArrowBack
            stroke={isMobile ? '#000' : '#8F8F8F'}
            width={24}
            height={24}
            styles={{
              transform: 'scale(1.25)',
              position: 'relative',
              left: -5,
              top: -1
            }}
          />
        )}
      </Icon>
      <Title>{title}</Title>
      <Icon onClick={onClickClose} right={false}>
        {onClickClose && (
          <CloseIcon
            styles={{
              position: 'relative',
              top: -3,
              right: -6
            }}
            color={isMobile ? '#000' : '#8F8F8F'}
          />
        )}
      </Icon>
    </Container>
  );
};

AuthHeading.propTypes = {
  title: string,
  onClickBack: func,
  onClickClose: func,
  isMobile: bool
};

export default AuthHeading;
