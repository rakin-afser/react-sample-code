import styled from 'styled-components';
import media from '../../../../constants/media';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 19px 18px 13px 18px;

  @media (min-width: ${media.mobileMax}) {
    padding: 42px 18px 32px 19px;
  }
`;

export const Title = styled.div`
  font-weight: 500;
  line-height: 22px;
  font-size: 18px;
  color: #000;

  @media (min-width: ${media.mobileMax}) {
    font-size: 22px;
    text-align: center;
  }
`;

export const Icon = styled.div`
  width: 24px;
  height: 24px;
  cursor: pointer;

  svg {
    @media (min-width: ${media.mobileMax}) {
      ${({right}) =>
        right
          ? `
            top: -27px !important;
            left: 0px !important;
            transform: scale(1.22) !important;
            `
          : `
            top: -26px !important;
            left: -1px !important;
            transform: scale(1) !important;
            `}
    }
  }
`;
