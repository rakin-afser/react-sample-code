import React from 'react';
import {bool, object} from 'prop-types';
import {Container, Title, LinkTo} from './styled';
import useGlobal from 'store';

const AuthTint = ({isSingIn, styles}) => {
  const [, globalActions] = useGlobal();
  const resetSignUpInfo = () => {
    globalActions.setSignUpInfo(null);
  };
  return (
    <Container styles={styles}>
      <Title> {isSingIn ? "Don't have an account yet?" : 'Already have an account?'} </Title>
      &nbsp;
      <LinkTo onClick={resetSignUpInfo} to={isSingIn ? '/auth/sign-up' : '/auth/sign-in'}>
        {isSingIn ? 'Create Account' : 'Sign in'}
      </LinkTo>
    </Container>
  );
};

AuthTint.propTypes = {
  isSingIn: bool,
  styles: object
};

export default AuthTint;
