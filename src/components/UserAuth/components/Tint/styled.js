import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const Container = styled.div.attrs(({styles}) => ({style: {...styles}}))`
  text-align: center;
`;

export const Title = styled.h3`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  display: inline-block;
`;

export const LinkTo = styled(Link)`
  display: inline-block;
  color: #4a90e2;
  font-weight: 500;
`;
