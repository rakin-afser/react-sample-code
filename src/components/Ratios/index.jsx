import React from 'react';
import {RatioList, RatioItem, RatioSymbol} from './styled';
import {ratios} from 'constants/common';

const Ratios = ({ratio, setRatio}) => {
  return (
    <RatioList>
      {ratios.map((r, i) => {
        const [width, height] = r?.split(':') || [];
        return (
          <RatioItem key={r} onClick={() => setRatio(ratios[i])} active={r === ratio ? 1 : 0}>
            <RatioSymbol {...{width, height}} />
            {r}
          </RatioItem>
        );
      })}
    </RatioList>
  );
};

export default Ratios;
