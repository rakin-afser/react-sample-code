import styled from 'styled-components';

export const RatioList = styled.ul`
  display: flex;
  justify-content: space-around;
  padding-top: 8px;
  padding-bottom: 8px;
`;

export const RatioItem = styled.li`
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  font-weight: 500;
  color: ${({active}) => (active ? '#ed484f' : '#c3c3c3')};
  transition: color 0.3s ease;
  cursor: pointer;
`;

export const RatioSymbol = styled.span`
  display: block;
  width: ${({width, height}) => (width / height) * 20}px;
  height: ${({width, height}) => (height / width) * 20}px;
  border: 2px solid currentColor;
`;
