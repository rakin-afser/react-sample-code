import React from 'react';
import {Wrap, ItemsWrap, Items, Item, Title, InnerWrap, Stats, Options} from './styled';
import Grid from 'components/Grid';
import Icon from 'components/Icon';
import useGlobal from 'store';

const List = ({title, onClick = () => {}, isPrivate, totalFollowers, products = []}) => {
  const [, setGlobalState] = useGlobal();

  const buttons = [
    {
      title: 'Save'
    },
    {
      title: 'Share',
      onClick: async () => {
        const shareData = {
          title: 'testSample',
          text: 'Share',
          url: window.location.href
        };

        try {
          if (navigator.share) {
            await navigator.share(shareData);
          }
        } catch (err) {
          console.log(err);
        }
      }
    },
    {
      title: 'Report',
      warn: true,
      onClick: () => {
        setGlobalState.setOptionsPopup({active: false});
        setGlobalState.setReportPopup({active: true});
      }
    }
  ];

  return (
    <Wrap>
      <ItemsWrap>
        <Items>
          {Array(Math.max(4, products?.length))
            .fill(1)
            .map((_, index) => (
              <Item
                style={products?.[index] ? {backgroundImage: `url(${products?.[index]?.image?.sourceUrl})`} : {}}
                onClick={() => {
                  setGlobalState.setQuickView({...products?.[index], loadProductData: true});
                }}
              />
            ))}
        </Items>
      </ItemsWrap>
      <InnerWrap onClick={onClick}>
        <Options>
          {isPrivate ? <Icon type="lock" style={{stroke: '#7C7E82'}} /> : <Icon type="eye" color="#7C7E82" />}

          <Icon onClick={() => setGlobalState.setOptionsPopup({active: true, buttons})} type="dots" />
        </Options>
        <Title>{title}</Title>
        <Grid>
          <Stats>
            <span>{totalFollowers}</span> Followers
          </Stats>
          <Stats>
            <span>{products?.length}</span> Products
          </Stats>
        </Grid>
      </InnerWrap>
    </Wrap>
  );
};

export default List;
