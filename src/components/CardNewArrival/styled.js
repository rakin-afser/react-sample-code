import styled from 'styled-components/macro';
import {mainWhiteColor, mainBlackColor, transparentTextColor, secondaryTextColor, primaryColor} from 'constants/colors';
import {FlexContainer} from 'globalStyles';
import media from 'constants/media';

export const Card = styled(FlexContainer)`
  position: relative;
  flex-direction: column;
  width: 228px;
  background: ${mainWhiteColor};
  cursor: pointer;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;

  @media (max-width: ${media.mobileMax}) {
    min-width: 159px;
    width: 159px;
    height: auto;
    margin: 0 4px;
    box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
    border-radius: 4px;

    svg {
      max-width: 12px;
      max-height: 14px;
    }

    ${({inline}) => {
      if (inline) {
        return `
          width: 100%;
          height: auto;
          background: #FFFFFF;
          border: 1px solid #EEEEEE;
          box-sizing: border-box;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.01);
          border-radius: 2px;
          padding: 10px;
          display: flex;
          flex-wrap: wrap;
          flex-direction: initial;
          margin: 0;
          max-width: calc(100vw - 64px);
        `;
      }
    }}
  }
`;

export const Info = styled(FlexContainer)`
  width: 100%;

  @media (max-width: ${media.mobileMax}) {
    flex-wrap: wrap;
    margin-bottom: 2px;

    ${({inline}) => {
      if (inline) {
        return `
            order: 2;
            flex-wrap: wrap;
          `;
      }
    }}
  }
`;

export const Tools = styled(FlexContainer)`
  align-items: center;
  width: 100%;
  position: relative;

  ${({inline}) => {
    if (inline) {
      return `
          order: 3;
          margin-top: 18px;
          display: none;
        `;
    }
  }}
`;

export const ImageContainer = styled.div`
  position: relative;
  width: 100%;

  ${({fixedHeight}) =>
    fixedHeight &&
    `
      height: 168px;
  `};

  ${({inline}) => {
    if (inline) {
      return `
      position: static;
      width: auto !important
    `;
    }
  }};
`;

export const ImageWrapper = styled.div``;

export const Image = styled.img`
  width: 228px;
  height: 228px;
  border-radius: 4px 4px 0 0;
  object-fit: cover;

  @media (max-width: ${media.mobileMax}) {
    height: 168px;
    object-fit: cover;
    border-radius: 4px 4px 0 0;
    width: 100%;

    ${({inline}) => {
      if (inline) {
        return `
            width: 100px;
            height: 100px;
            border-radius: 1px;
            margin: 0;
          `;
      }
    }}
  }
`;

export const CardFooter = styled(FlexContainer)`
  width: 100%;
  height: 128px;
  flex-direction: column;
  letter-spacing: -0.6px;
  padding: 9px 16px 12px 16px;
  flex-grow: 1;

  @media (max-width: ${media.mobileMax}) {
    padding: 6px 9px 15px;
    position: relative;
    height: auto;

    ${({inline}) => {
      if (inline) {
        return `
            width: calc(100% - 100px);
            padding-left: 10px;
            margin: 0 0 auto;
            padding: 0 0 0 10px;
          `;
      }
    }}
  }
`;

export const Price = styled.div`
  font-size: 16px;
  color: ${({sale}) => (sale ? primaryColor : '#000')};
  line-height: 140%;
  letter-spacing: -0.016em;

  @media (min-width: ${media.tablet}) {
    small {
      font-size: 85%;
      letter-spacing: 0.4px;
      color: #8f8f8f;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 14px;
    font-weight: 500;
    color: ${({sale}) => (sale ? primaryColor : '#000')};
    line-height: 1;
    margin-top: 2px;

    small {
      font-weight: 500;
      font-size: 12px;
      line-height: 10px;
      color: #666666;
      letter-spacing: 0;
    }

    ${({inline}) => {
      if (inline) {
        return `
            order: 2;
            font-size: 14px;
          `;
      }
    }}
  }
`;

export const OldPrice = styled.span`
  font-size: 12px;
  color: #a7a7a7;
  margin-left: 8px;
  text-decoration: line-through;

  @media (max-width: ${media.mobileMax}) {
    font-weight: normal;
    font-size: 12px;
    line-height: 10px;
    text-decoration-line: line-through;
    color: #999999;
    margin-left: 10px;

    small {
      font-weight: normal;
      font-size: 12px;
      color: #999;
    }

    ${({inline}) => {
      if (inline) {
        return `
          font-size: 12px;
          margin-left: 10px;
        `;
      }
    }}
  }
`;

export const Shipping = styled.div`
  font-size: 12px;
  color: ${transparentTextColor};

  @media (min-width: ${media.tablet}) {
    margin: 4px auto 8px 0;
    letter-spacing: -0.2px;
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 10px;
    line-height: 1;
    color: #999999;
    margin-right: -1px;
    margin-top: 9px;
    width: 100%;
    letter-spacing: -0.3px;

    ${({inline}) => {
      if (inline) {
        return `
            order: 1;
            font-style: normal;
            font-weight: normal;
            font-size: 12px;
            line-height: 1.4;
            color: #8F8F8F;
            margin: 1px 0 10px;
            width: 100%;
          `;
      }
    }}
  }
`;

export const Title = styled.p`
  width: 100%;
  font-size: 16px;
  //margin: 0 0 10px;
  color: ${mainBlackColor};
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  flex-grow: 1;

  @media (min-width: ${media.tablet}) {
    //margin: 7px 0 5px;
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 14px;
    color: #000;
    //margin: 7px 0 0;
    font-weight: normal;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-family: 'SF Pro Display', sans-serif;
    line-height: 14px;
    letter-spacing: 0;
    margin-right: auto;
    max-width: 90%;

    ${({inline}) => {
      if (inline) {
        return `
            order: 1;
            // margin: 0 0 2px;
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;
            color: #000000;
          `;
      }
    }}
  }
`;

export const Likes = styled(FlexContainer)`
  font-size: 14px;

  @media (min-width: ${media.tablet}) {
    width: 24px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    left: -2px;
    bottom: 1px;

    svg {
      max-width: 17px;
      max-height: 15px;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    position: absolute;
    bottom: 8px;
    right: 8px;
    width: 24px;
    height: 24px;
    background: rgba(255, 255, 255, 0.76);
    box-shadow: 0 4px 4px rgba(0, 0, 0, 0.13);
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;

    svg {
      max-width: 12px;
      max-height: 11px;
    }

    ${({inline}) => {
      if (inline) {
        return `
          position: static;
          order: 5;
          margin: 14px auto 0 0;
        `;
      }
    }}
  }
`;

export const LikesCount = styled.span`
  color: ${secondaryTextColor};
  margin-left: 6px;

  @media (max-width: ${media.mobileMax}) {
    margin-left: 4px;
  }
`;

export const Sale = styled.div`
  position: absolute;
  right: 4px;
  top: 4px;
  background: #f0646a;
  width: 35px;
  height: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #ffffff;

  @media (max-width: ${media.mobileMax}) {
    width: 27px;
    height: 17px;
    right: 8px;
    top: 8px;
    background: #f0646a;
    border-radius: 2px;
    font-style: normal;
    font-weight: 200;
    font-size: 10px;
    text-align: center;
    color: #ffffff;
    line-height: 17px;
    display: flex;
    align-items: center;
    justify-content: center;

    strong {
      font-weight: 700;
      padding: 0 1px;
    }
  }
`;

export const Actions = styled.div`
  position: absolute;
  right: 9px;
  bottom: 11px;
  display: flex;
  cursor: pointer;

  span {
    width: 4px;
    height: 4px;
    border-radius: 50%;
    background: #7c7e82;
    margin-left: 3px;
  }

  ${({inline}) => {
    if (inline) {
      return `
        bottom: inherit;
        right: 0;
        top: 10px;
      `;
    }
  }}
`;

export const BookmarkIcon = styled.div``;

export const PlayBtn = styled.div`
  position: absolute;
  bottom: 8px;
  left: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 22px;
  height: 22px;
  z-index: 1;

  svg {
    width: 38px;
    height: 38px;
    max-width: none !important;
    max-height: none !important;
  }
`;
