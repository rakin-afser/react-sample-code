import React, {useState} from 'react';
import {useWindowSize} from '@reach/window-size';
import useGlobal from 'store';
import Actions from 'components/Actions';
import AddToList from 'components/AddToList';
import Bookmark from 'assets/Bookmark';
import InCardVideo from 'components/InCardVideo';
import productPlaceholder from 'images/placeholders/product.jpg';
import {
  CardFooter,
  ImageContainer,
  Image,
  Card,
  Likes,
  Title,
  Shipping,
  Price,
  Tools,
  Info,
  OldPrice,
  Sale,
  BookmarkIcon,
  ImageWrapper,
  PlayBtn
} from './styled';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';
import Icon from 'components/Icon';

const CardNewArrival = ({
  node = {},
  inline,
  alwaysImage,
  onClick,
  onRemoveFromSaveForLater,
  onFromSaveForLaterToCart,
  ...props
}) => {
  const [globalState, setGlobalState] = useGlobal();
  const {width} = useWindowSize();
  const [showAddToList, setShowAddToList] = useState(false);
  const isMobile = width <= 767;

  const {
    databaseId,
    name,
    image,
    isLiked,
    price,
    product_video_url: productVideoUrl,
    productVideoPoster,
    shippingType,
    variations
  } = node;

  const {salePrice, regularPrice} = variations?.nodes?.[0] || node || {};

  const [triggerLike] = useAddUpdateLikedProductMutation({
    variables: {input: {id: databaseId}},
    product: node
  });

  let sale = null;

  if (price && salePrice)
    sale = (100 - (salePrice?.replace(/^\D+/g, '') / regularPrice?.replace(/^\D+/g, '')) * 100).toFixed(0);

  const onCardClick = () => {
    if (typeof onClick === 'function') {
      onClick();
    } else {
      setGlobalState.setQuickView({...node, loadProductData: true});
    }
  };

  if (!node) {
    return null;
  }

  const renderTools = () => {
    return (
      <Tools inline={inline}>
        <Likes onClick={triggerLike}>
          <Icon type={isLiked ? 'liked' : 'like'} color="#8F8F8F" />
        </Likes>
        <BookmarkIcon onClick={() => setShowAddToList(true)}>
          <Bookmark isWished={false} />
        </BookmarkIcon>
        {!isMobile && (
          <AddToList productId={databaseId} showAddToList={showAddToList} setShowAddToList={setShowAddToList} />
        )}
      </Tools>
    );
  };

  return (
    <Card inline={inline} className="CardNewArrival__card">
      <ImageContainer fixedHeight={!alwaysImage && productVideoUrl} inline={inline} {...props}>
        <ImageWrapper onClick={onCardClick}>
          {!alwaysImage && productVideoUrl ? (
            <InCardVideo
              onPlayAction={() => setGlobalState.setPlayInCardVideo(databaseId)}
              playing={globalState.playInCardVideo === databaseId}
              withPlayBtn
              src={productVideoUrl}
            />
          ) : (
            <Image
              src={productVideoPoster || image?.sourceUrl || productPlaceholder}
              alt={image?.altText}
              inline={inline}
            />
          )}
          {productVideoUrl ? (
            <PlayBtn>
              <Icon type="playRed" />
            </PlayBtn>
          ) : null}
          {sale && !isNaN(sale) && !inline ? (
            <Sale>
              -<strong>{sale}</strong>%
            </Sale>
          ) : null}
        </ImageWrapper>
        {!inline && isMobile ? (
          <Likes onClick={triggerLike}>
            <Icon type={isLiked ? 'liked' : 'like'} color="#8F8F8F" />
          </Likes>
        ) : null}
      </ImageContainer>
      <CardFooter inline={inline}>
        <Info inline={inline}>
          <Price inline={inline} sale={sale}>
            {salePrice || ''}
            {salePrice ? <OldPrice inline={inline}>{regularPrice}</OldPrice> : `${regularPrice}`}
          </Price>
        </Info>
        <Title inline={inline} title={name}>
          {name}
        </Title>
        <Shipping inline={inline}>{shippingType}&nbsp;</Shipping>

        {isMobile ? (
          <>
            {inline && (
              <Likes inline={inline} onClick={triggerLike}>
                <Icon type={isLiked ? 'liked' : 'like'} color="#8F8F8F" />
              </Likes>
            )}
            <Actions inline={inline} item={node} />
          </>
        ) : (
          renderTools()
        )}
      </CardFooter>
    </Card>
  );
};

export default CardNewArrival;
