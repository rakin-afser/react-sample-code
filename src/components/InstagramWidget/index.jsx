import React from 'react';
import PropTypes from 'prop-types';
import InstagramTransparent from 'assets/InstagramTransparent';
import {Wrap, Title, Text, Container, Img, ImgWrap, Connect} from './styled';
import ins_1 from './img/ins-1.jpg';
import ins_2 from './img/ins-2.jpg';
import ins_3 from './img/ins-3.jpg';
import ins_4 from './img/ins-4.jpg';
import ins_5 from './img/ins-5.jpg';
import ins_6 from './img/ins-6.jpg';

const InsagramWidget = ({noWrapperStyles}) => {
  return (
    <Wrap noStyled={noWrapperStyles}>
      <Title>Instagram Photos</Title>
      <ImgWrap>
        {[ins_1, ins_2, ins_3, ins_4, ins_5, ins_6].map((img, i) => (
          <Img key={i} src={img} />
        ))}
      </ImgWrap>
      <Container>
        <Text>Share your Instagram posted as well</Text>
        <Connect>
          <InstagramTransparent />
          Connect
        </Connect>
      </Container>
    </Wrap>
  );
};

InsagramWidget.defaultProps = {
  noWrapperStyles: false
};

InsagramWidget.propTypes = {
  noWrapperStyles: PropTypes.bool
};

export default InsagramWidget;
