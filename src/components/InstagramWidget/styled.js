import {Button} from 'antd';
import styled from 'styled-components';

export const Wrap = styled.div`
  margin-top: ${({noStyled}) => (noStyled ? '0' : '15px')};
  padding: 24px 24px 28px;
  background-color: #ffffff;
  box-shadow: ${({noStyled}) => (noStyled ? 'none' : '0px 4px 21px rgba(0, 0, 0, 0.06)')};
  border-radius: ${({noStyled}) => (noStyled ? '0' : '4px')};
`;

export const Title = styled.h4`
  font-size: 18px;
  color: #464646;
  margin-bottom: 11px;
  line-height: 1;
`;

export const Img = styled.img`
  height: 85px;
  flex: 0 0 auto;
  width: 33.333%;
  padding: 1.5px;
`;

export const ImgWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -1.5px;
`;

export const Text = styled.p`
  color: #999999;
  font-size: 12px;
  line-height: 1.16;
  max-width: 120px;
  margin-bottom: 0;
  margin-right: 10px;
`;

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding-top: 16px;
  justify-content: space-between;
`;

export const Connect = styled(Button)`
  &&& {
    background-color: #ed484f;
    border-radius: 4px;
    border: none;
    color: #ffffff;
    padding: 8px 11px;
    height: 36px;
    display: inline-flex;
    align-items: center;
    font-size: 12px;

    svg {
      margin-right: 10px;
    }
  }
`;
