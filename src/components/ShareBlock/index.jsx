import React, {useState, useRef} from 'react';
import Slider from 'react-slick';
import Icon from 'components/Icon';
import PropTypes from 'prop-types';
import {share as shareData, followers as shareUserData} from 'constants/staticData';
import FadeOnMount from 'components/Transitions';
import arrowImage from './imgs/actualArrow.svg';

import {
  ButtonContainer,
  TopEmailBlock,
  BottomTextAreaBlock,
  SliderButton,
  Follower,
  ShareUserList,
  Description,
  ActionsWrapper,
  ShareButton,
  SearchInput,
  TitleShare,
  TitleShare2,
  SliderWrapper,
  AvatarStyled,
  ShareLink,
  SearchWrapper,
  ShareWrapper
} from './styled';

const ShareBlock = ({onClose, title, noCloseBtn}) => {
  const [users, setUsers] = useState([]);
  const [sharedUsers, setSharedUsers] = useState([]);
  const [shareByEmail, setShareByEmail] = useState(false);

  const shareWithUser = (username) => {
    if (!sharedUsers.includes(username)) {
      setSharedUsers((oldArray) => [...sharedUsers, username]);
    }
  };

  const handleSearch = ({target: {value}}) => {
    if (value.length) {
      const searchResult = shareUserData.filter(
        ({name}) => name.toLowerCase().indexOf(value.toLowerCase()) > -1 && name
      );
      setUsers(searchResult);
    } else {
      setUsers([]);
    }
  };

  const slider = useRef();

  const settings = {
    dots: false,
    infinite: true,
    speed: 200,
    slidesToShow: 6,
    slidesToScroll: 1
  };

  const next = () => {
    slider.current.slickNext();
  };

  const previous = () => {
    slider.current.slickPrev();
  };

  return (
    <FadeOnMount>
      <ShareWrapper>
        <TitleShare className="share-block__title">{title}</TitleShare>
        {!noCloseBtn && (
          <Icon className="share-block__close" type="close" svgStyle={{fill: '#000'}} onClick={() => onClose(false)} />
        )}
        {shareByEmail ? (
          <div>
            <TopEmailBlock>
              <p>Enter Email Address</p>
              <SearchInput allowClear placeholder="Search for Username" />
            </TopEmailBlock>
            <BottomTextAreaBlock>
              <p>Add a Message</p>
              <textarea resize="none" placeholder="Search for Username" />
              <ButtonContainer>
                <ShareButton>Share</ShareButton>
              </ButtonContainer>
            </BottomTextAreaBlock>
          </div>
        ) : (
          <div className="share-block__body">
            <SliderWrapper>
              <SliderButton onClick={previous}>
                <img src={arrowImage} alt="" />
              </SliderButton>
              <SliderButton isRight onClick={next}>
                <img src={arrowImage} alt="" />
              </SliderButton>
              <div style={{padding: 0}}>
                <Slider ref={slider} {...settings}>
                  {shareData.map((el, key) => {
                    const isEmail = title === 'Email';
                    return (
                      <div
                        role="button"
                        tabIndex="0"
                        key={key}
                        onKeyDown={() => (isEmail ? setShareByEmail(true) : null)}
                        onClick={() => (isEmail ? setShareByEmail(true) : null)}
                      >
                        <ShareLink>
                          <AvatarStyled src={el.image} alt={el.title} custom="margin: 0;width: 32px;height: 32px;" />
                          <span>{el.title}</span>
                        </ShareLink>
                      </div>
                    );
                  })}
                </Slider>
              </div>
            </SliderWrapper>
            <SearchWrapper>
              <TitleShare2 className="share-block__title2">Or share with</TitleShare2>
              <SearchInput onChange={handleSearch} allowClear placeholder="Search for Username" />
            </SearchWrapper>
            <ShareUserList>
              {users.map(({avatar, accountName, name}, i) => {
                const shared = sharedUsers.includes(accountName);

                return (
                  <Follower key={i} styles={{marginRight: '16px'}}>
                    <AvatarStyled icon="user" src={avatar} />
                    <Description>
                      <strong>{name}</strong>
                      <small>{accountName}</small>
                    </Description>
                    <ActionsWrapper>
                      <ShareButton isShared={shared} onClick={() => shareWithUser(accountName)}>
                        {shared ? 'Shared' : 'Share'}
                      </ShareButton>
                    </ActionsWrapper>
                  </Follower>
                );
              })}
            </ShareUserList>
          </div>
        )}
      </ShareWrapper>
    </FadeOnMount>
  );
};

ShareBlock.defaultProps = {
  onClose: null,
  title: null,
  noCloseBtn: false
};

ShareBlock.propTypes = {
  onClose: PropTypes.func,
  title: PropTypes.string,
  noCloseBtn: PropTypes.bool
};

export default ShareBlock;
