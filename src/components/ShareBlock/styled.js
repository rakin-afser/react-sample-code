import styled from 'styled-components/macro';
import {Input, Button, Avatar as AvatarAntd} from 'antd';

export const ShareWrapper = styled.div`
  width: 100%;
  position: relative;
  background-color: #ffffff;
  box-shadow: 0 4px 20px rgba(0, 0, 0, 0.15);
  border-radius: 8px;

  .share-block__close {
    position: absolute;
    right: 8px;
    top: 8px;
    cursor: pointer;

    path {
      fill: #000;
      transition: ease 0.4s;
    }

    &:hover {
      path {
        fill: #ea2c34;
      }
    }
  }
`;

export const ShareButton = styled.button`
  background: #ffffff;
  border: 1px solid;
  color: #000000;
  width: 80px;
  height: 28px;
  box-sizing: border-box;
  border-radius: 24px;
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  ${({isShared}) =>
    isShared ? {borderColor: '#999999', color: '#999999'} : {borderColor: '#000000', color: '#000000'}}
`;

export const RedCloseButton = styled(Button)`
  &&& {
    width: 16px;
    padding: 0;
    display: flex;
    align-items: center;
  }
`;

export const ActionsWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Description = styled.div`
  display: grid;
  grid-gap: 3px 0;
  flex-grow: 1;
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  strong {
    font-weight: bold;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
  }
  small {
    font-weight: normal;
    font-size: 12px;
    line-height: 132%;
    color: #7a7a7a;
  }
`;
export const AvatarStyled = styled(AvatarAntd)`
  &&& {
    ${({custom}) => custom || null}
  }
  && {
    width: 50px;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 8px;
    i {
      font-size: 30px;
    }
  }
`;
export const Follower = styled.div`
  display: flex;
  align-items: center;
  margin: 0 12px 16px 24px;
  ${({styles}) => (styles ? {...styles} : null)}
`;
export const FollowersList = styled.div`
  min-height: 200px;
  max-height: 417px;
  margin: 0 -24px 0;
`;

export const ShareUserList = styled.div`
  padding-bottom: 1px;
`;

export const SearchInput = styled(Input)`
  &&&& {
    font-family: 'Helvetica Neue', sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 19px;
    color: #000000;
    border-color: #e4e4e4;
    margin: 1px 0 16px;
    .ant-input {
      padding: 9px 24px 8px;
      background: #ffffff;
      border: 1px solid #e4e4e4;
      box-sizing: border-box;
      border-radius: 2px;
      color: #000000;
    }
  }
`;

export const TitleShare = styled.h3`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  padding: 12px 16px 15px;
  margin: 0;
`;

export const TitleShare2 = styled(TitleShare)`
  padding: 16px 0 15px;
`;

export const SliderWrapper = styled.div`
  position: relative;

  .slick-arrow.slick-prev {
    display: none !important;
  }

  .slick-arrow.slick-next {
    display: none !important;
  }
`;

export const SliderButton = styled.button`
  border: none;
  background: none;
  display: flex;
  align-items: center;
  position: absolute;
  top: 10px;
  padding: 0;
  box-shadow: 0 2px 9px rgba(0, 0, 0, 0.06);
  border-radius: 3px;
  z-index: 9999;
  ${({isRight}) => (isRight ? {right: 4, transform: 'rotate(180deg)'} : {left: 4})}
`;

export const ShareLink = styled.div`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  line-height: 140%;
  text-align: center;
  color: #666666;
  width: 100%;
  display: grid;
  grid-gap: 9px;
  justify-items: center;
  cursor: pointer;
  transition: ease 0.4s;

  &:hover {
    color: #ea2c34;
  }
`;

export const SearchWrapper = styled.div`
  padding: 9px 16px;
  .title {
    font-family: 'Helvetica', sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 140%;
    color: #464646;
    margin-bottom: 7px;
  }
`;

export const TopEmailBlock = styled.div`
  padding: 0 16px 8px;
  border-bottom: 1px solid #e4e4e4;
  p {
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    margin-bottom: 7px;
    color: #464646;
  }
`;

export const BottomTextAreaBlock = styled.div`
  padding: 24px 16px;
  p {
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    margin-bottom: 7px;
    color: #464646;
  }
  textarea {
    background: #ffffff;
    border: 1px solid #e4e4e4;
    box-sizing: border-box;
    border-radius: 2px;
    resize: none;
    width: 358px;
    height: 80px;
    padding: 10px 16px;
    color: #000;
  }
`;

export const ButtonContainer = styled.div`
  margin-top: 21px;
  text-align: right;
`;
