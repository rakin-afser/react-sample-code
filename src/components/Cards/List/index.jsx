import React, {useState} from 'react';
import {useQuery, useLazyQuery, gql} from '@apollo/client';
import PropTypes from 'prop-types';
import avatarPlaceholder from 'images/avatarPlaceholder.png';
import Buttons from 'components/Buttons';
import {ListItemContainer, ListHead, ListInfo, ImgContainer, TagItem, BlackText} from './styled';

const GET_USER = gql`
  query GetUser($userId: ID!) {
    user(id: $userId, idType: DATABASE_ID) {
      id
      avatar {
        url
      }
      nickname
      name
      username
    }
  }
`;

const List = ({data, ...props}) => {
  const {data: userData, loading: userLoading, error: userError} = useQuery(GET_USER, {
    variables: {userId: data.user_id}
  });

  const productData = data?.addedProducts?.nodes;

  return (
    <ListItemContainer {...props}>
      <ListHead>
        <div>
          <img src={userData?.user?.avatar?.url || avatarPlaceholder} alt={userData?.user?.name} />
          <BlackText>{data?.list_name}</BlackText>
        </div>
        <Buttons type="follow" />
      </ListHead>
      <ListInfo>
        <span>
          <BlackText>{data.totalFollowers}</BlackText> Followers
        </span>{' '}
        <span>
          <BlackText>{productData?.length}</BlackText> Products
        </span>
      </ListInfo>
      <ImgContainer>
        <img key={1} src={productData?.[0]?.image?.sourceUrl} alt={productData?.[0]?.name} />
        <img key={2} src={productData?.[1]?.image?.sourceUrl} alt={productData?.[1]?.name} />
        <img key={3} src={productData?.[2]?.image?.sourceUrl} alt={productData?.[2]?.name} />
      </ImgContainer>
      <p>{data?.hashtags ? data?.hashtags?.map((el, index) => <TagItem key={index}>#{el}</TagItem>) : 'No hashtags'}</p>
    </ListItemContainer>
  );
};

List.propTypes = {
  data: PropTypes.shape().isRequired
};

export default List;
