import styled from 'styled-components';

export const ListItemContainer = styled.div`
  width: 100%;
  max-width: 282px;
  padding: 16px;
  border: 1px solid #efefef;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.04);
  border-radius: 4px;
  margin-bottom: 24px;
  transition: ease 0.7s;
  transform: scale(1);

  p {
    text-align: left;
    margin: 0;
  }

  &:hover {
    box-shadow: 0 4px 4px rgba(0, 0, 0, 0.3);
    transform: scale(1.06);
    cursor: pointer;
  }
`;

export const ListHead = styled.div`
  margin-bottom: 7px;

  &,
  & div {
    display: flex;
    align-items: center;
    justify-content: space-between;
  }

  img {
    width: 30px;
    height: 30px;
    border-radius: 50%;
    margin-right: 7px;
  }
`;

export const ListInfo = styled.p`
  margin-bottom: 10px !important;
`;

export const ImgContainer = styled.div`
  margin-bottom: 10px;
  display: flex;
  justify-content: space-between;

  img {
    width: 78px;
    height: 78px;
    border-radius: 4px;
    object-fit: cover;

    &:first-child {
      background-color: #f8ebe2;
    }

    &:nth-child(2) {
      background-color: #e6edf3;
    }

    &:last-child {
      background-color: #f0d9d9;
    }
  }
`;

export const TagItem = styled.span`
  margin-right: 5px;
`;

export const BlackText = styled.span`
  color: #000000;
  font-weight: bold;
  padding-right: 10px;
  max-width: 128px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
