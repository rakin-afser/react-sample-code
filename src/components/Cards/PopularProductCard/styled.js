import styled from 'styled-components';

export const Wrap = styled.div`
  width: 100%;
  background: #ffffff;
  border-radius: 4px;
  position: relative;
  padding-bottom: 16px;
  margin-bottom: 24px;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.06);
`;

export const Image = styled.img`
  width: 100%;
  height: 190px;
`;

export const Name = styled.span`
  display: block;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  color: #000000;
  margin: 8px 12px 0;
`;

export const Sale = styled.div`
  position: absolute;
  background: #f0646a;
  border-radius: 4px;
  right: 4px;
  top: 4px;
  z-index: 10;
  color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 200;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #ffffff;
  width: 35px;
  height: 18px;
  & span {
    font-weight: 500;
  }
`;
