import photo1 from './img/Img.png';
import photo2 from './img/Img-1.png';
import photo3 from './img/Img-2.png';

export default [
  {
    id: 1,
    image: photo1,
    sale: 40,
    name: 'Queen Sweatshirt Funny Honey with rhinestones'
  },
  {
    id: 2,
    image: photo2,
    sale: 40,
    name: 'Queen Sweatshirt Funny Honey with rhinestones'
  },
  {
    id: 3,
    image: photo3,
    sale: 40,
    name: 'Queen Sweatshirt Funny Honey with rhinestones'
  }
];
