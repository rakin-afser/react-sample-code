import React from 'react';
import {Wrap, Image, Name, Sale} from './styled.js';

const PopularProductCard = ({product}) => {
  return (
    <Wrap>
      {!!product.sale ? <Sale>-<span>{product.sale}</span>%</Sale> : null }
      <Image src={product.image} alt={product.name}/>
      <Name>{product.name}</Name>
    </Wrap>
  )
}

export default PopularProductCard;
