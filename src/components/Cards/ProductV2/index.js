import React, {useState} from 'react';
import {useMutation} from '@apollo/client';

import useGlobal from 'store';
import BookmarkIcon from 'assets/Bookmark';
import HeartIcon from 'assets/Heart';
import EnlargeIcon from 'assets/Enlarge';
import {
  Card,
  CardImage,
  Image,
  CardImageShadow,
  EnlargeIconWrapper,
  CardBody,
  Flex,
  Price,
  PriceBD,
  PriceValue,
  PriceWithoutDiscount,
  Name,
  Description,
  Actions,
  FavouriteItem,
  WishedItem,
  Discount
} from 'components/Cards/ProductV2/styled';
import productPlaceholder from 'images/placeholders/product.jpg';
import {TRIGGER_LIKE} from 'mutations';
import {userId} from 'util/heplers';

const ProductCard = ({
  name = '',
  shippingType,
  slug,
  img,
  isDiscount,
  isLiked,
  databaseId,
  isWished,
  regularPrice,
  salePrice,
  item
}) => {
  const [_, setGlobalState] = useGlobal();
  const [liked, setLiked] = useState(isLiked);
  const [triggerLike] = useMutation(TRIGGER_LIKE, {variables: {input: {product_id: databaseId, user_id: userId}}});

  const productClick = async () => {
    setGlobalState.setQuickView({quickViewType: 'product', data: item});
  };

  const onLikeClick = () => {
    if (userId) {
      setLiked(!liked);
      triggerLike();
    }
  };

  let discountPercent = null;
  if (regularPrice && salePrice)
    discountPercent = (100 - (salePrice?.replace(/^\D+/g, '') / regularPrice?.replace(/^\D+/g, '')) * 100).toFixed(0);

  return (
    <Card>
      <CardImage>
        <Image src={img || productPlaceholder} />
        <CardImageShadow onClick={() => productClick()}>
          <EnlargeIconWrapper>
            <EnlargeIcon />
          </EnlargeIconWrapper>
        </CardImageShadow>
      </CardImage>
      <CardBody>
        <Flex>
          <Price>
            {/* <PriceBD>BD</PriceBD> */}
            <PriceValue isDiscount={isDiscount}>{salePrice || regularPrice}</PriceValue>
            {isDiscount && <PriceWithoutDiscount>{regularPrice}</PriceWithoutDiscount>}
          </Price>
          <Description>{shippingType}</Description>
        </Flex>
        <Name>{name}</Name>
        <Actions>
          <FavouriteItem onClick={onLikeClick}>
            <HeartIcon isLiked={liked} />
          </FavouriteItem>
          <WishedItem>
            <BookmarkIcon isWished={isWished} width={20} height={20} />
          </WishedItem>
        </Actions>
      </CardBody>
      {isDiscount && (
        <Discount>
          -<b>{discountPercent}</b>
          &#37;
        </Discount>
      )}
    </Card>
  );
};

export default ProductCard;
