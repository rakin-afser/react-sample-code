import styled from 'styled-components/macro';
import media from 'constants/media';
import {primaryColor} from 'constants/colors';

export const StoreContainer = styled.div`
  width: ${({mini}) => (mini ? 'auto' : '273px')};
  height: ${({mini}) => (mini ? '184px' : '214px')};
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  margin-bottom: ${({mini}) => (mini ? '0' : '36px')};
  transition: ease 0.9s;
  transform: scale(1);
  overflow: hidden;
  border-radius: ${({mini}) => (mini ? '12px' : '0')};

  &:hover {
    @media (min-width: 768px) {
      box-shadow: 0 2px 8px rgba(0, 0, 0, 0.6);
      transform: scale(1.1);
      cursor: pointer;
    }
  }

  ${({mini}) =>
    mini &&
    `
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);

    &:hover {
      transform: scale(1);
      box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.2);
    }

    @media (max-width: ${media.mobileMax}) {
      height: 160px;
    }
  `}
`;

export const Banner = styled.div`
  position: relative;
  height: ${({mini}) => (mini ? '102px' : '92px')};

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  @media (max-width: ${media.mobileMax}) {
    ${({mini}) => mini && `height: 78px;`}
  }
`;

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.44), rgba(0, 0, 0, 0.44));

  i {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const InfoBlock = styled.div`
  position: relative;
  padding: 0 16px 8px;

  img {
    width: ${({mini}) => (mini ? '58px' : '72px')};
    height: ${({mini}) => (mini ? '58px' : '72px')};
    position: absolute;
    top: ${({mini}) => (mini ? '-36px' : '-32px')};
    left: ${({mini}) => (mini ? '10px' : '16px')};
    border-radius: 50%;
    overflow: hidden;
    box-shadow: 0 0 4px rgba(000, 000, 000, 0.2);
    background-color: #fff;
  }

  ${({mini}) =>
    mini &&
    `
    padding: 0 16px 0;

    @media (max-width: ${media.mobileMax}) {
      img {
        width: 50px;
        height: 50px;
      }
    }
  `}
`;

export const FollowContainer = styled.div`
  text-align: right;
  margin: 8px 0 13px;
`;

export const StoreName = styled.p`
  margin-bottom: 0;
  font-size: 14px;
  text-align: left;

  ${({mini}) =>
    mini &&
    `
        margin-bottom: 2px;
        font-weight: 500;
        font-size: 14px;
        line-height: 140%;
        color: #000000;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
    `}
`;

export const FollowersAndRating = styled.div`
  display: flex;
  margin-bottom: 0;
  font-size: 12px;
  text-align: left;

  div {
    margin: 0 10px;
  }

  ${({mini}) =>
    mini &&
    `
        .star-rating {
          svg {
          margin-left: 0;
          margin-right: 1px;
         }
    `}
`;

export const RatedQuantity = styled.span`
  ${({mini}) =>
    mini &&
    `
        display: none;
    `}
`;

export const Button = styled.button`
  background-color: ${primaryColor};
  width: 99px;
  height: 28px;
  border-radius: 24px;

  svg {
    position: relative;
    top: 1px;

    circle {
      r: 4;
    }
  }
`;
