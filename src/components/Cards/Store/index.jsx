import React from 'react';
import {bool, func, shape, element} from 'prop-types';
import defaultAvatar from 'images/avatarPlaceholder.png';
import bannerPlaceHolder from 'images/placeholder.png';

import StarRating from 'components/StarRating';
import Tags from 'components/Tags';
import Icons from 'components/Icon';
import {
  StoreContainer,
  Banner,
  Overlay,
  InfoBlock,
  FollowContainer,
  StoreName,
  FollowersAndRating,
  RatedQuantity
} from './styled';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';
import {useUser} from 'hooks/reactiveVars';
import Btn from 'components/Btn';

const Store = ({data, handleClick, icon, mini, overlay, ...props}) => {
  const [user] = useUser();
  const [followUnfollowStore] = useFollowUnFollowStoreMutation({
    followed: data?.followed
  });

  const onFollowUnfollowStore = (storeId) => {
    followUnfollowStore({
      variables: {input: {id: parseInt(storeId)}}
    });
  };

  const onContainerClick = () => {
    if (overlay) {
      onFollowUnfollowStore(data?.id);
    }

    return null;
  };

  return (
    <StoreContainer mini={mini} {...(overlay && {onClick: onContainerClick})}>
      <Banner mini={mini} {...props}>
        <img src={data?.banner || bannerPlaceHolder} alt="cover" />
        {overlay && data?.followed && (
          <Overlay>
            <Icons type="checkmark" color="#fff" width={30} height={30} />
          </Overlay>
        )}
      </Banner>
      <InfoBlock mini={mini}>
        <img src={data?.gravatar || defaultAvatar} alt="logo" />
        <FollowContainer mini={mini} onClick={() => handleClick(data)}>
          <Btn kind={`${data?.followed ? 'following-sm' : 'follow-sm'}`} />
        </FollowContainer>
        <StoreName mini={mini}>{data?.name}</StoreName>
        <FollowersAndRating mini={mini}>
          <span>{data?.totalFollowers} followers</span> <StarRating stars={data?.rating} starWidth={7} starHeight={7} />{' '}
          {data?.rating ? <RatedQuantity mini={mini}>({data?.rating})</RatedQuantity> : null}
        </FollowersAndRating>
        <Tags styles={{fontSize: '10px', marginTop: '5px'}} />
      </InfoBlock>
    </StoreContainer>
  );
};

Store.defaultProps = {
  handleClick: () => {},
  followState: false,
  icon: React.FC,
  mini: false,
  overlay: false
};

Store.propTypes = {
  data: shape().isRequired,
  handleClick: func,
  followState: bool,
  icon: element,
  mini: bool,
  overlay: bool
};

export default Store;
