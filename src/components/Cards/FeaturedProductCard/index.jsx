import React from 'react';
import {useHistory} from 'react-router-dom';

import {
  Card,
  Avatar,
  About,
  AvatarLogo,
  Name,
  Time,
  Poster,
  PosterPic,
  Shop,
  Likes,
  Prices,
  PriceValue,
  Shipping,
  Bookmark,
  Red
} from './styled';
import Icons from '../../Icon';
import Grid from 'components/Grid';
import productPlaceholder from 'images/placeholders/product.jpg';

const FeaturedProductCard = ({content, margin}) => {
  const {push} = useHistory();

  const {name, image, salePrice, regularPrice, shippingType, slug} = content;

  return (
    <Card margin={margin} onClick={() => push(`/product/${slug}`)}>
      <Poster>
        <PosterPic src={image?.sourceUrl || productPlaceholder} alt={image?.altText} />
      </Poster>
      <Grid sb padding="0 8px" margin="4px 0">
        <Prices>
          <PriceValue isDiscounted>
            <Red>{salePrice}</Red>
          </PriceValue>
          <PriceValue onSale={salePrice}>{regularPrice}</PriceValue>
        </Prices>
      </Grid>
      <Grid aic padding="0 10px 0 8px" margin="0 0 4px 0">
        <Name>{name}</Name>
      </Grid>
      <Shipping>{shippingType}</Shipping>
    </Card>
  );
};

export default FeaturedProductCard;
