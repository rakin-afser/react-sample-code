import styled, {css} from 'styled-components';

export const Card = styled.div`
  position: relative;
  //width: 160px;
  background: #ffffff;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.04);
  box-sizing: border-box;
  border-radius: 4px;
  margin: ${({margin}) => (margin ? `${margin}` : 0)};
  margin-bottom: 5px;
  transition: ease 0.8s;

  &:hover {
    box-shadow: 0 2px 6px rgba(237, 72, 79, 0.8);
    cursor: pointer;
  }
`;

export const Poster = styled.div`
  width: 100%;
  height: 118px;
  position: relative;
`;

export const PosterPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 4px 4px 0px 0px;
`;

export const Avatar = styled.div`
  width: 44px;
  height: 44px;
`;

export const About = styled.div`
  margin: 0 0 0 8px;
`;

export const AvatarLogo = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Name = styled.span`
  display: block;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 100%;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  color: #000;
  padding: 0 0 10px;
`;

export const Shop = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: SF Pro Display, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #464646;

  & svg {
    margin: 0 10px 0 0;
    fill: #8f8f8f;
  }
`;

export const Likes = styled(Shop)``;

export const Time = styled.span`
  position: absolute;
  top: 11px;
  right: 15px;
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #656565;
`;

export const Prices = styled.div``;

export const Shipping = styled.span`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-align: right;
  color: #8f8f8f;
  margin-left: 8px;
`;

export const PriceValue = styled.span`
  font-size: 14px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  line-height: 140%;
  color: #999;

  ${({isDiscounted}) =>
    isDiscounted &&
    css`
      color: #a7a7a7;
      font-size: 16px;
    `};

  ${({onSale}) =>
    onSale &&
    css`
      margin-left: 5px;
      text-decoration: line-through;
      color: #a7a7a7;
    `};
`;

export const Bookmark = styled.div`
  > div {
    display: flex;
  }

  svg {
    fill: #8f8f8f;
  }
`;

export const Red = styled.span`
  color: #ed484f;
`;
