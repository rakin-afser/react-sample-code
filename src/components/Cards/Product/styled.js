import styled, {css} from 'styled-components/macro';
import {primaryColor} from 'constants/colors';
import Btn from 'components/Btn';

export const Card = styled.div`
  position: relative;
  max-width: ${({maxWidth}) => maxWidth || '272px'};
  width: ${({maxWidth}) => maxWidth || '272px'} !important;
  background: #ffffff;
  box-shadow: 0 2px 9px rgba(0, 0, 0, 0.09);
  border-radius: 4px;
  margin: ${({margin}) => (margin ? `${margin}` : 0)};

  .hover-container {
    opacity: 0;
    transition: ease 0.4s;
  }

  &:hover {
    .hover-container {
      opacity: 1;
    }
  }

  ${({saveForLaterCard}) => saveForLaterCard && 'max-width: 198px'};

  @media (max-width: 1130px) {
    & {
      max-width: 236px;
    }
  }
`;

export const Poster = styled.div`
  width: 100%;
  height: ${({saveForLaterCard}) => (saveForLaterCard ? '184px' : '228px')};
  cursor: pointer;
  position: relative;
`;

export const BlackCover = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background-color: black;
  opacity: 0.5;
`;

export const HoverLens = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 9;
`;

export const PosterPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 4px 4px 0px 0px;
`;

export const Avatar = styled.div`
  width: 44px;
  height: 44px;
`;

export const About = styled.div`
  margin: 0 0 0 8px;
`;

export const AvatarLogo = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Name = styled.span`
  cursor: pointer;
  display: block;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #000000;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 90%;
  text-align: left;
`;

export const Shop = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: SF Pro Display, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #464646;

  & svg {
    margin: 0 10px 0 0;
    fill: #8f8f8f;
  }
`;

export const Time = styled.span`
  position: absolute;
  top: 11px;
  right: 15px;
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #656565;
`;

export const Prices = styled.div``;

export const Shipping = styled.span`
  font-size: 10px;
  padding-top: 4px;
  line-height: 140%;
  text-align: left;
  color: #8f8f8f;
  display: block;
  width: 100%;
`;

export const PriceValue = styled.span`
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #000;

  ${({isDiscounted}) =>
    isDiscounted &&
    css`
      margin-left: 5px;
      text-decoration: line-through;
      font-size: 13px;
      color: #999;
    `} ${({salePrice}) =>
    salePrice &&
    css`
      color: ${primaryColor};
    `}
`;

export const Bookmark = styled.div`
  cursor: pointer;
  display: flex;
  align-items: center;

  svg path {
    transition: 0.4s ease;
  }

  &:hover {
    & svg path {
      fill: ${primaryColor};
    }
  }
`;

export const InfoContainer = styled.div`
  padding: 8px 14px 10px;
  min-height: 120px;
`;

export const DiscountLabel = styled.div`
  position: absolute;
  right: 4px;
  top: 4px;
  background: ${primaryColor};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  font-weight: normal;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #ffffff;
  z-index: 1;
  padding: 2px 4px;

  strong {
    font-weight: 700;
    padding: 0 1px;
  }
`;

export const RemoveFromSFL = styled.button`
  position: absolute;
  top: 5px;
  left: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 22px;
  height: 22px;
  border-radius: 50%;
  background: #e4e4e4;
  z-index: 5;
`;

export const AddToCartBtn = styled(Btn)`
  font-size: 14px;
  height: 28px;
  padding: 0 14px;
  margin-left: auto;
`;
