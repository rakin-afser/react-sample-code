import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';

import Grid from 'components/Grid';
import Icon from 'components/Icon';
import AddToList from 'components/AddToList';
import Video from 'components/Cards/Video';
import useGlobal from 'store';
import Likes from 'components/Likes';
import HoverInfo from './components/HoverInfo';
import Icons from '../../Icon';

import {
  Card,
  Name,
  Poster,
  PosterPic,
  Prices,
  PriceValue,
  Shipping,
  Bookmark,
  InfoContainer,
  HoverLens,
  BlackCover,
  DiscountLabel,
  AddToCartBtn,
  RemoveFromSFL
} from './styled';

import productPlaceholder from 'images/placeholders/product.jpg';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';
import LazyLoad from 'react-lazyload';

const Product = ({
  content = {},
  margin,
  index,
  hideLens = false,
  hideStoreBlock = false,
  inQuickView = true,
  saveForLaterCard,
  onRemoveFromSaveForLater,
  onFromSaveForLaterToCart,
  ...props
}) => {
  const {
    databaseId,
    variationId,
    name,
    image,
    isLiked,
    totalLikes,
    shippingType,
    slug,
    seller,
    // eslint-disable-next-line camelcase
    product_video_url,
    variations
  } = content;
  const {salePrice, regularPrice} = variations?.nodes?.[0] || content || {};
  const [showAddToList, setShowAddToList] = useState(false);
  const [globalState, setGlobalState] = useGlobal();
  const {push} = useHistory();
  const [triggerLike] = useAddUpdateLikedProductMutation({
    variables: {input: {id: databaseId}},
    product: content
  });

  const onClickAddToList = () => setShowAddToList(true);

  const onShowQuickView = async () => {
    if (!inQuickView) {
      push(`/product/${content?.slug}`);
      return;
    }
    setGlobalState.setQuickView({quickViewType: 'product', data: content});
  };

  const WithPoster = () => {
    return (
      <>
        <div className="hover-container">
          {!hideStoreBlock && (
            <HoverInfo name={seller?.name} id={seller?.id} image={seller?.gravatar} followed={seller?.followed} />
          )}
          <div role="button" tabIndex="0" onKeyDown={() => {}} onClick={() => onShowQuickView()}>
            <BlackCover className="black-cover" />
            {!hideLens && (
              <HoverLens>
                <Icon type="lens" />
              </HoverLens>
            )}
          </div>
        </div>
        <LazyLoad once style={{height: '100%'}}>
          <PosterPic
            onClick={() => onShowQuickView()}
            src={image?.sourceUrl || productPlaceholder}
            alt={image?.altText}
          />
        </LazyLoad>
      </>
    );
  };

  const showPoster = () => {
    // eslint-disable-next-line camelcase
    if (product_video_url) {
      return (
        <>
          <div className="hover-container">{!hideStoreBlock && <HoverInfo />}</div>
          <LazyLoad once style={{height: '100%'}}>
            {/* eslint-disable-next-line camelcase */}
            <Video src={product_video_url} play={!globalState.quickView} onClick={() => onShowQuickView()} />
          </LazyLoad>
        </>
      );
    }

    return WithPoster();
  };

  let discountPercent = null;

  if (regularPrice && salePrice)
    discountPercent = (100 - (salePrice?.replace(/^\D+/g, '') / regularPrice?.replace(/^\D+/g, '')) * 100).toFixed(0);

  const renderTools = () => {
    if (!saveForLaterCard) {
      return (
        <>
          <Likes isLiked={isLiked} triggerLike={triggerLike} likesCount={totalLikes} showCount />
          <Bookmark onClick={onClickAddToList}>
            <Icons isWished={false} type="bookmark" width={17} height={15} color="#8f8f8f" />
          </Bookmark>
          <AddToList showAddToList={showAddToList} setShowAddToList={setShowAddToList} productId={databaseId} />
        </>
      );
    }

    return (
      <AddToCartBtn
        style={{justifyContent: 'flex-end'}}
        onClick={() => onFromSaveForLaterToCart(databaseId, variationId)}
        kind="primary-small"
      >
        Add to cart
      </AddToCartBtn>
    );
  };

  return (
    <div>
      <Card saveForLaterCard={saveForLaterCard} margin={margin} {...props}>
        {saveForLaterCard && (
          <RemoveFromSFL
            onClick={() => {
              onRemoveFromSaveForLater(databaseId, variationId);
            }}
          >
            <Icons type="close" width={20} height={20} color="#000" />
          </RemoveFromSFL>
        )}

        {discountPercent && !isNaN(discountPercent) ? (
          <DiscountLabel>
            -<strong>{discountPercent}</strong>%
          </DiscountLabel>
        ) : null}
        <Poster saveForLaterCard={saveForLaterCard} onClick={() => onShowQuickView()}>
          {showPoster()}
        </Poster>
        <InfoContainer>
          <Grid aic>
            <Name onClick={() => push(`/product/${content?.slug}`)}>{name}</Name>
          </Grid>
          <Grid customStyles={{height: '22px'}} sb margin="6px 0 0" flexWrap>
            <Prices>
              {!!salePrice && <PriceValue salePrice>{salePrice}</PriceValue>}
              {!!regularPrice && <PriceValue isDiscounted={salePrice}>{regularPrice}</PriceValue>}
            </Prices>
            <Shipping>{shippingType}</Shipping>
          </Grid>
          <Grid aic sb margin="35px 0 0">
            {renderTools()}
          </Grid>
        </InfoContainer>
      </Card>
    </div>
  );
};

export default Product;
