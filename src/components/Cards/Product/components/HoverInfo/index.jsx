import React from 'react';
import {useMutation} from '@apollo/client';
import {FOLLOW_STORE} from 'mutations';
import Icon from 'components/Icon';
import userPlaceholder from 'images/placeholders/user.jpg';
import {userId} from 'util/heplers';
import {InfoContainer, SellerImage, SellerName, FollowBtn} from './styled';

const HoverInfo = ({name, image, followed, id}) => {
  const [mutationFollowStore, {loading: loadingFollowStore}] = useMutation(FOLLOW_STORE, {variables: {userId}});

  const renderLoader = (text) => {
    if (loadingFollowStore) {
      return <Icon type="loader" />;
    }

    return text;
  };

  const onFollowUnfollowStore = (e) => {
    e.stopPropagation();

    mutationFollowStore({
      variables: {storeId: +id},
      update(cache, {storeData}) {
        cache.modify({
          fields: {
            products() {
              return storeData;
            }
          }
        });
      }
    });
  };

  return (
    <InfoContainer>
      <SellerImage src={image || userPlaceholder} />
      <SellerName>{name}</SellerName>
      {followed ? (
        <FollowBtn onClick={onFollowUnfollowStore}>{renderLoader('Following')}</FollowBtn>
      ) : (
        <>
          <FollowBtn onClick={onFollowUnfollowStore}>
            <Icon type="plus" />
            {renderLoader('Follow')}
          </FollowBtn>
        </>
      )}
    </InfoContainer>
  );
};

export default HoverInfo;
