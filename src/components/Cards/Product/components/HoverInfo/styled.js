import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const InfoContainer = styled.div`
  position: absolute;
  top: -38px;
  text-align: center;
  left: 0;
  right: 0;
  padding: 4px 16px;
  background: #ffffff;
  mix-blend-mode: normal;
  box-shadow: 0 4px 20px rgba(0, 0, 0, 0.15);
  border-radius: 4px 4px 0 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 38px;
`;

export const SellerImage = styled.img`
  border-radius: 50%;
  flex-shrink: 0;
  width: 30px;
  height: 30px;
  margin-right: 8px;
  object-fit: cover;
`;

export const SellerName = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #000000;
  flex-grow: 1;
  text-align: left;
  white-space: nowrap;
  max-width: 70%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const FollowBtn = styled.div`
  font-size: 12px;
  color: #ed484f;

  i {
    position: relative;
    top: 1px;
    margin-right: 7px;

    svg {
      circle {
        fill: ${primaryColor};
      }
    }
  }
`;
