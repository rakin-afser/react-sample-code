import {Link} from 'react-router-dom';
import styled from 'styled-components/macro';

export const CardContainer = styled.div`
  position: relative;
`;

export const MediaWrapper = styled.div`
  padding-bottom: 100%;
  position: relative;
`;

export const Image = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  object-fit: cover;
  border-radius: 8px;
  width: 100%;
  height: 100%;
`;

export const VideoCard = styled.div`
  border-radius: 8px;
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  overflow: hidden;
`;

export const VideoIcon = styled.div`
  position: absolute;
  bottom: -11px;
  right: -3px;
  z-index: 9;
`;

export const StyledLink = styled(Link)`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 10;
`;
