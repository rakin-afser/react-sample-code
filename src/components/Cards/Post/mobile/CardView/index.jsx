import React from 'react';

import Icon from 'components/Icon';
import {Image, MediaWrapper, CardContainer, VideoIcon, StyledLink} from './styled';
import placeholder from 'images/placeholders/product.jpg';
import ProductsCounter from 'components/ProductsCounter';
import LazyLoad from 'react-lazyload';

const CardView = ({data}) => {
  const {databaseId, relatedProducts, galleryImages} = data || {};

  function renderMedia(media) {
    switch (media?.mimeType?.split('/')?.[0]) {
      case 'video':
        return (
          <>
            <Image src={media?.poster?.sourceUrl || placeholder} alt={media?.poster?.altText} />
            <VideoIcon>
              <Icon type="playRed" />
            </VideoIcon>
          </>
        );
      case 'image':
        return <Image src={media?.sourceUrl || placeholder} alt={media?.altText} />;
      default:
        return <Image src={placeholder} alt="placeholder" />;
    }
  }

  return (
    <CardContainer>
      <StyledLink to={`/post/${databaseId}`} />
      <LazyLoad>
        {galleryImages?.nodes?.slice(0, 1)?.map((media) => (
          <MediaWrapper key={media?.id}>{renderMedia(media)}</MediaWrapper>
        ))}
        {!galleryImages?.nodes?.length ? <MediaWrapper>{renderMedia()}</MediaWrapper> : null}
      </LazyLoad>
      {relatedProducts?.nodes?.length ? (
        <ProductsCounter count={relatedProducts?.nodes?.length} top="4px" right="4px" left="unset" bottom="unset" />
      ) : null}
    </CardContainer>
  );
};

export default CardView;
