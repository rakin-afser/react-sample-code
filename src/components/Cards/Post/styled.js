import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const Header = styled.div`
  transition: ease 0.4s;
`;

export const PosterWrapper = styled.div`
  position: relative;
  height: 100%;
`;

export const PosterOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 0;
  background: linear-gradient(0deg, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4));
  transition: ease 0.4s;

  svg {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const Card = styled.div`
  position: relative;
  width: 273px;
  height: ${({noHeader}) => (noHeader ? 'auto' : '366px')};
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin: ${({margin}) => (margin ? `${margin}` : 0)};
  transition: ease 0.4s;
  cursor: pointer;
  overflow: hidden;

  &:hover ${Header} {
  }

  &:hover ${PosterOverlay} {
    opacity: 1;
  }
`;

export const VideoWrapper = styled.div`
  width: 100%;
  height: 100%;

  .card-video {
    border-radius: 0;
  }
`;

export const Poster = styled.div`
  width: 100%;
  height: ${({noHeader}) => (noHeader ? '231px' : '260px')};
`;

export const PosterPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Avatar = styled.div`
  width: 44px;
  height: 44px;
  border-radius: 50%;
  overflow: hidden;
  box-shadow: 0 0 4px #f3f3f3;
`;

export const About = styled.div`
  margin: 0 0 0 8px;
`;

export const AvatarLogo = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Name = styled.span`
  display: block;
`;

export const Shop = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: SF Pro Display, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #464646;

  & svg {
    margin: 0 10px 0 0;

    path {
      transition: ease 0.2s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const Time = styled.span`
  position: absolute;
  top: 11px;
  right: 15px;
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #656565;
`;

export const IconVideoWrapper = styled.div`
  margin-left: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 22px;
  height: 22px;
  background: #ffffff;
  border-radius: 50%;
  box-shadow: 0 0.870467px 7.8342px rgba(0, 0, 0, 0.27);

  svg {
    margin: 2px 0 0 0;
  }
`;
