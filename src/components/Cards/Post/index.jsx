import React, {useState} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import Likes from 'components/Likes';
import Grid from 'components/Grid';
import Video from 'components/Cards/Video';
import Icon from 'components/Icon';
import useGlobal from 'store';
import {
  Header,
  Card,
  Avatar,
  About,
  AvatarLogo,
  Name,
  Time,
  PosterWrapper,
  PosterOverlay,
  Poster,
  PosterPic,
  Shop,
  VideoWrapper,
  IconVideoWrapper
} from './styled';
import {setRating} from '../../helpers';
import Icons from '../../Icon';
import productPlaceholder from 'images/placeholders/product.jpg';
import {useAddUpdateLikedPostMutation} from 'hooks/mutations';
import userPlaceholder from 'images/placeholders/user.jpg';
import PostView from 'components/Modals/PostView';

const Post = ({margin, noHeader, content = {}, ...props}) => {
  const [globalState] = useGlobal();
  const [visible, setVisible] = useState(false);
  const [triggerLike] = useAddUpdateLikedPostMutation({post: globalState.quickView?.postData || {}});

  const {relatedProducts, author, databaseId, date, galleryImages, totalLikes, isLiked, rating, video} = content;
  const posterMedia = galleryImages?.nodes?.[0];

  const onClick = () => {
    setVisible(true);
  };

  const onLike = (e) => {
    triggerLike({variables: {input: {id: databaseId}}});
  };

  const showPoster = (media) => {
    switch (media?.mimeType?.split('/')?.[0]) {
      case 'video':
        return (
          <VideoWrapper>
            <Video borderRadius={0} play={false} onClick={onClick} src={media?.mediaItemUrl} />
          </VideoWrapper>
        );
      case 'image':
        return (
          <PosterWrapper onClick={onClick}>
            <PosterOverlay>
              <Icon type="lens" />
            </PosterOverlay>
            <PosterPic src={media?.sourceUrl} alt={media?.altText} />
          </PosterWrapper>
        );
      default:
        return (
          <PosterWrapper onClick={onClick}>
            <PosterOverlay>
              <Icon type="lens" />
            </PosterOverlay>
            <PosterPic src={productPlaceholder} alt="post" />
          </PosterWrapper>
        );
    }
  };

  return (
    <Card noHeader={noHeader} margin={margin} {...props}>
      {!noHeader && (
        <Header>
          <Time>{moment(date).format('MMM Do YY')}</Time>
          <Grid aic height={64} padding="0 0 0 16px">
            <Avatar>
              <AvatarLogo src={author?.node?.avatar?.url || userPlaceholder} />
            </Avatar>
            <About>
              <Name>{author?.node?.name}</Name>
              {setRating(rating)}
            </About>
          </Grid>
        </Header>
      )}
      <Poster noHeader={noHeader}>{showPoster(posterMedia)}</Poster>
      <Grid sb aic height={43} padding="0 15px">
        <Shop>
          {relatedProducts?.nodes?.length ? (
            <>
              <Icons type="bags" /> {relatedProducts?.nodes?.length}
            </>
          ) : null}
          {/** button with play icon no need any more  */}
          {/* {posterMedia?.mimeType?.split('/')?.[0] === 'video' && (* /}
          {/*   <IconVideoWrapper> */}
          {/*     <IconPlaySmall /> */}
          {/*   </IconVideoWrapper> */}
          {/*  )} */}
        </Shop>
        <Likes triggerLike={onLike} isLiked={!!isLiked} likesCount={totalLikes} showCount />
      </Grid>
      <PostView visible={visible} setVisible={setVisible} postId={databaseId} />
    </Card>
  );
};

Post.defaultProps = {
  margin: '0',
  noHeader: false
};

Post.propTypes = {
  content: PropTypes.shape().isRequired,
  margin: PropTypes.string,
  noHeader: PropTypes.bool
};

export default Post;
