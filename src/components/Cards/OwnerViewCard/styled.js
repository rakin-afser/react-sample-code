import styled from 'styled-components/macro';
import {mainWhiteColor, mainBlackColor, transparentTextColor, secondaryTextColor, primaryColor} from 'constants/colors';
import {FlexContainer} from 'globalStyles';
import media from 'constants/media';

export const Card = styled(FlexContainer)`
  position: relative;
  flex-direction: column;
  width: auto;
  background: ${mainWhiteColor};
  cursor: pointer;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  max-width: 100%;

  @media (max-width: ${media.mobileMax}) {
    min-width: 159px;
    width: 159px;
    height: auto;
    margin: 0 4px;
    box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
    border-radius: 4px;

    svg {
      max-width: 12px;
      max-height: 14px;
    }

    ${({inline}) => {
      if (inline) {
        return `
          width: 100%;
          height: auto;
          background: #FFFFFF;
          border: 1px solid #EEEEEE;
          box-sizing: border-box;
          box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.01);
          border-radius: 2px;
          padding: 10px;
          display: flex;
          flex-wrap: wrap;
          flex-direction: initial;
          margin: 0;
        `;
      }
    }}
  }
`;

export const Info = styled(FlexContainer)`
  width: 100%;

  @media (max-width: ${media.mobileMax}) {
    flex-wrap: wrap;

    ${({inline}) => {
      if (inline) {
        return `
            order: 2;
            flex-wrap: wrap;
          `;
      }
    }}
  }
`;

export const Tools = styled(FlexContainer)`
  align-items: center;
  width: 100%;
  position: relative;

  ${({inline}) => {
    if (inline) {
      return `
          order: 3;
          margin-top: 18px;
          display: none;
        `;
    }
  }}
`;

export const ImageContainer = styled.div`
  position: relative;
  .hover-zoom {
    z-index: -1;
  }
  &:hover {
    .hover-zoom {
      z-index: 1;
    }
  }
  ${({inline}) => {
    if (inline) {
      return `
      position: static;
    `;
    }
  }}
`;

export const Image = styled.img`
  width: 262px;
  height: 265px;
  border-radius: 4px 4px 0 0;
  object-fit: cover;

  @media (max-width: ${media.mobileMax}) {
    height: 174px;
    object-fit: cover;
    border-radius: 4px 4px 0 0;
    width: 100%;

    ${({inline}) => {
      if (inline) {
        return `
            width: 100px;
            height: 100px;
            border-radius: 1px;
            margin: 0;
          `;
      }
    }}
  }
`;

export const CardFooter = styled(FlexContainer)`
  width: 100%;
  height: 104px;
  letter-spacing: -0.6px;
  padding: 6px 16px 10px;

  height: 104px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: unset;
  position: relative;

  @media (min-width: ${media.tablet}) {
    /* display: block; */
  }

  @media (max-width: ${media.mobileMax}) {
    padding: 6px 7px 8px;
    position: relative;
    height: auto;

    ${({inline}) => {
      if (inline) {
        return `
            width: calc(100% - 100px);
            padding-left: 10px;
            margin: 0 0 auto;
            padding: 0 0 0 10px;
          `;
      }
    }}
  }
`;

export const Price = styled.div`
  font-size: 16px;
  color: ${({sale}) => (sale ? primaryColor : '#000')};
  line-height: 140%;
  letter-spacing: -0.016em;

  @media (min-width: ${media.tablet}) {
    /* letter-spacing: -1.4px; */

    small {
      font-size: 85%;
      letter-spacing: 0.4px;
      color: #8f8f8f;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 14px;
    font-weight: 500;
    line-height: 12px;
    color: ${({sale}) => (sale ? primaryColor : '#000')};
    line-height: 1;
    margin-top: 2px;

    small {
      font-weight: 500;
      font-size: 12px;
      line-height: 10px;
      color: #666666;
      letter-spacing: 0;
    }

    ${({inline}) => {
      if (inline) {
        return `
            order: 2;
            font-size: 14px;
          `;
      }
    }}
  }
`;

export const OldPrice = styled.span`
  font-size: 12px;
  color: #a7a7a7;
  display: inline-block;
  margin-left: 8px;
  text-decoration: line-through;

  @media (max-width: ${media.mobileMax}) {
    font-weight: normal;
    font-size: 12px;
    line-height: 10px;
    text-decoration-line: line-through;
    color: #999999;
    margin-left: 10px;

    small {
      font-weight: normal;
      font-size: 12px;
      color: #999;
    }

    ${({inline}) => {
      if (inline) {
        return `
          font-size: 12px;
          margin-left: 10px;
        `;
      }
    }}
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    ` 
    margin-left: 0;
    margin-right: 8px;
  `}
`;

export const Shipping = styled.div`
  font-size: 12px;
  color: ${transparentTextColor};

  @media (min-width: ${media.tablet}) {
    margin: 4px auto 8px 0;
    letter-spacing: -0.2px;
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 10px;
    line-height: 1;
    color: #999999;
    margin-right: -1px;
    letter-spacing: -0.1px;
    margin-top: 6px;
    width: 100%;
    letter-spacing: -0.3px;

    ${({inline}) => {
      if (inline) {
        return `
            order: 1;
            font-style: normal;
            font-weight: normal;
            font-size: 12px;
            line-height: 1.4;
            color: #8F8F8F;
            margin: 1px 0 10px;
            width: 100%;
          `;
      }
    }}
  }
`;

export const Title = styled.p`
  width: 100%;
  font-size: 16px;
  margin: 0 0 10px;
  color: ${mainBlackColor};
  flex-grow: 1;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 90%;

  @media (min-width: ${media.tablet}) {
    margin: 7px 0 5px;
  }

  @media (max-width: ${media.mobileMax}) {
    font-size: 10px;
    color: #000;
    margin: 9px 0 0;
    font-weight: normal;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-family: 'SF Pro Display';
    line-height: 14px;
    letter-spacing: 0;

    ${({inline}) => {
      if (inline) {
        return `
            order: 1;
            margin: 0 0 2px;
            font-style: normal;
            font-weight: 500;
            font-size: 14px;
            line-height: 17px;
            color: #000000;
          `;
      }
    }}
  }
`;

export const Likes = styled(FlexContainer)`
  font-size: 14px;

  @media (min-width: ${media.tablet}) {
    width: 24px;
    height: 24px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    left: -2px;
    bottom: 1px;

    svg {
      max-width: 17px;
      max-height: 15px;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    position: absolute;
    bottom: 8px;
    right: 8px;
    width: 24px;
    height: 24px;
    background: rgba(255, 255, 255, 0.76);
    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;

    svg {
      max-width: 12px;
      max-height: 11px;
    }

    ${({inline}) => {
      if (inline) {
        return `
          position: static;
          order: 5;
          margin: 14px auto 0 0;
        `;
      }
    }}
  }
`;

export const LikesCount = styled.span`
  color: ${secondaryTextColor};
  margin-left: 6px;

  @media (max-width: ${media.mobileMax}) {
    margin-left: 4px;
  }
`;

export const Sale = styled.div`
  position: absolute;
  right: 4px;
  top: 4px;
  width: 35px;
  height: 18px;
  background: #f0646a;
  border-radius: 4px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 200;
  font-size: 12px;
  line-height: 15px;
  color: #ffffff;
  display: flex;
  align-items: baseline;
  justify-content: center;
  padding-top: 1px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    ` 
    right: unset;
    left: 4px;
  `}
`;

export const Actions = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;

  .heart {
    transition: fill 0.4s;
    fill: #${({heart}) => (heart ? `ED484F` : `CCCCCC`)};

    &:hover {
      fill: #${({heart}) => (heart ? `ED484F` : `666666`)};
    }
  }
`;

export const BookmarkIcon = styled.div``;

export const ShopingBagNum = styled.div`
  position: absolute;
  border-radius: 12.1429px;
  top: 6px;
  right: 13px;
  display: flex;
  flex-direction: row;

  span {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 13px;
    letter-spacing: -0.016em;
    color: #7a7a7a;
    margin-left: 2px;
    padding-top: 1px;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    ` 
    right: unset;
    left: 13px;
    span {
      margin-right: 2px;
      margin-left: 0;
    }
  `}
`;

export const HoverZoomWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  background: #00000066;
  border-radius: 4px 4px 0px 0px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  z-index: -1;
`;

export const EditList = styled.div`
  ${({activePopup}) => (activePopup ? `display: block;` : `display:none;`)}
  position: absolute;
  width: 160px;
  top: 30px;
  transform: translate(-100%, -100%);
  left: -15px;
  background: #ffffff;
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.28);
  border-radius: 8px;
  z-index: 99;
`;

export const Action = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  border-bottom: ${({boldBorder}) => (boldBorder ? '1px solid #C3C3C3' : '1px solid #FAFAFA')};
  background: transparent;
  cursor: pointer;
  color: #464646;
  padding: 11px 16px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  letter-spacing: 0.2px;

  &:hover {
    color: #000000;
  }

  &:last-child {
    color: #b91319;
    border-bottom: none;
    &::after {
      content: '';
      position: absolute;
      right: -22px;
      border: 15px solid transparent;
      border-left: 15px solid #fff;
    }
  }
`;

export const PopupWrapper = styled.div`
  position: relative;
`;
