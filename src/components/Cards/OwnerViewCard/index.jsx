import React, {useState, useRef} from 'react';
import {useMutation} from '@apollo/client';

import {ReactComponent as HeartIcon} from 'images/svg/heart.svg';
import {ReactComponent as DotsIcon} from 'images/svg/dots.svg';
import {ReactComponent as ShopingBagIcon} from 'images/svg/shopping-bag.svg';
import {ReactComponent as BiggerIcon} from 'images/svg/bigger.svg';
import useOutsideClick from 'util/useOutsideClick';
import useGlobal from 'store';
import {TRIGGER_LIKE} from 'mutations';
import {useUser} from 'hooks/reactiveVars';

import {
  CardFooter,
  HoverZoomWrapper,
  ImageContainer,
  Image,
  Card,
  Title,
  Price,
  Info,
  OldPrice,
  Sale,
  Actions,
  ShopingBagNum,
  EditList,
  Action,
  PopupWrapper
} from './styled';

const CardWrapper = ({
  productCount = 0,
  onSale,
  sale,
  onClick,
  productImage,
  price,
  oldPrice,
  title,
  isLiked,
  actions,
  databaseId
}) => {
  const popupRef = useRef();
  const [user] = useUser();
  const [, setGlobalState] = useGlobal();
  const [liked, setLiked] = useState(isLiked);
  const [activePopup, setActivePopup] = useState(false);
  const [triggerLike] = useMutation(TRIGGER_LIKE);

  useOutsideClick(popupRef, () => {
    setActivePopup(false);
  });

  const onCardClick = () => {
    if (typeof onClick === 'function') {
      onClick();
    } else {
      // setGlobalState.setQuickView(node);
    }
  };

  const onLike = () => {
    if (user?.databaseId) {
      setLiked(!liked);
      //triggerLike({variables: {input: {product_id: databaseId, user_id: user?.databaseId}}});
    }
  };

  return (
    <Card>
      <ImageContainer onClick={onCardClick}>
        {sale ? (
          <Sale>
            -<strong>{sale}</strong>%
          </Sale>
        ) : null}
        <HoverZoomWrapper className="hover-zoom">
          <BiggerIcon />
        </HoverZoomWrapper>
        <Image src={productImage} alt="product" />
      </ImageContainer>
      <CardFooter>
        <Info>
          <Price sale={onSale}>
            {price ? price : null}
            {oldPrice && <OldPrice>{oldPrice}</OldPrice>}
          </Price>
        </Info>
        <Title>{title}</Title>
        <Actions heart={liked}>
          <HeartIcon className="heart" onClick={onLike} />
          <PopupWrapper ref={popupRef}>
            <DotsIcon onClick={() => setActivePopup((prev) => !prev)} />
            <EditList activePopup={activePopup}>
              {actions ? (
                actions.map(({title: actionTitle, handleClick}, key) => (
                  <Action key={key} onClick={handleClick}>
                    {actionTitle}
                  </Action>
                ))
              ) : (
                <>
                  <Action>Share</Action>
                  <Action>Delete Product</Action>
                </>
              )}
            </EditList>
          </PopupWrapper>
        </Actions>
        <ShopingBagNum>
          <ShopingBagIcon width={16} height={16} /> <span>{productCount}</span>
        </ShopingBagNum>
      </CardFooter>
    </Card>
  );
};

export default CardWrapper;
