import React, {useRef, useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from 'react-player';
import {VideoWrapper, Wrapper, VideoOverlay, ControlsWrapper, PlayIcon, LensIcon, SpeakerIcon} from './styled';

const Video = ({src, play, ...props}) => {
  const [muted, setMuted] = useState(true);
  const [isPlayAction, setIsPlayAction] = useState(false);
  const [playVideo, setPlayVideo] = useState(false);
  const videoRef = useRef(null);

  useEffect(() => {
    if (!play) {
      setPlayVideo(false);
    }
  }, [play]);

  useEffect(() => {
    let videoTimeout;

    if (playVideo) {
      videoTimeout = setTimeout(() => {
        setPlayVideo(false);
      }, 7000);
    }

    return () => {
      clearTimeout(videoTimeout);
    };
  }, [playVideo]);

  const onPlayVideo = () => {
    if (!isPlayAction) {
      setPlayVideo(true);
    }
  };

  const onPauseVideo = () => {
    if (!isPlayAction) {
      setPlayVideo(false);
    }
  };

  const onPlayToggle = (e) => {
    e.stopPropagation();

    if (playVideo) {
      setPlayVideo(false);
      setIsPlayAction(true);
    } else {
      setPlayVideo(true);
      setIsPlayAction(true);
    }
  };

  const onUnmute = (e) => {
    e.stopPropagation();
    setMuted((prev) => !prev);
  };

  return (
    <Wrapper className="card-video" onMouseEnter={onPlayVideo} onMouseLeave={onPauseVideo}>
      <VideoOverlay />
      <VideoWrapper>
        <ReactPlayer
          {...props}
          ref={videoRef}
          muted={muted}
          playsinline
          onEnded={() => {
            setPlayVideo(false);
          }}
          playing={playVideo}
          controls={false}
          url={src}
          width="100%"
          height="100%"
        />
      </VideoWrapper>
      <SpeakerIcon muted={muted} onClick={onUnmute} />
      <ControlsWrapper>
        <PlayIcon play={playVideo ? 1 : 0} onClick={onPlayToggle} />
        <LensIcon />
      </ControlsWrapper>
    </Wrapper>
  );
};

Video.defaultProps = {
  play: false
};

Video.propTypes = {
  play: PropTypes.bool,
  src: PropTypes.string.isRequired
};

export default Video;
