import styled from 'styled-components/macro';
import {ReactComponent as PlayIco} from './img/play.svg';
import {ReactComponent as LensIco} from './img/lens.svg';
import {ReactComponent as SpeakerIco} from './img/speaker.svg';
import media from 'constants/media';

export const VideoOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(0deg, rgba(255, 255, 255, 0.29), rgba(255, 255, 255, 0.29));
  transition: ease 0.4s;
  opacity: 0;
`;

export const VideoWrapper = styled.div`
  width: 100%;
  height: 100%;

  video {
    object-fit: cover;
  }
`;

export const ControlsWrapper = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 34px;
`;

export const PlayIcon = styled(PlayIco)`
  position: relative;
  top: -3px;
  left: 4px;
  transition: ease 0.4s;
  opacity: 0;
  transform: ${({play}) => (play ? 'scale(1)' : 'scale(.8)')};

  & #path1 {
    transition: ease 0.6s;
    stroke-width: 2;
    stroke: transparent;
  }

  &:hover {
    & #path1 {
      stroke: #fafafa;
    }
  }
`;

export const LensIcon = styled(LensIco)`
  position: relative;
  top: -5px;
  right: 7px;
  transition: ease 0.4s;
  opacity: 0;

  & #path1 {
    transition: ease 0.6s;
    stroke-width: 2;
    stroke: transparent;
  }

  &:hover {
    & #path1 {
      stroke: #fafafa;
    }
  }
`;

export const SpeakerIcon = styled(SpeakerIco)`
  position: absolute;
  top: 6px;
  right: 3px;
  opacity: 0;
  transition: ease 0.4s;
  display: none;

  & #path1 {
    transition: ease 0.6s;
    stroke-width: 2;
    stroke: transparent;
  }

  & #path2 {
    display: ${({muted}) => (!muted ? 'none' : 'block')};
  }

  & #path3 {
    display: ${({muted}) => (!muted ? 'none' : 'block')};
  }

  &:hover {
    & #path1 {
      stroke: #fafafa;
    }
  }
`;

export const Wrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  border-radius: 4px 4px 0 0;
  overflow: hidden;

  &:hover ${PlayIcon} {
    opacity: 1;
  }

  &:hover ${VideoOverlay} {
    opacity: 1;
  }

  &:hover ${LensIcon} {
    opacity: 1;
  }

  &:hover ${SpeakerIcon} {
    opacity: 1;
  }
`;
