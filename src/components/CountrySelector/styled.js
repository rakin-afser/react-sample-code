import styled from 'styled-components';
import media from 'constants/media';

export const Wrapper = styled.div`
  margin: 0 0 16px auto;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  @media (max-width: ${media.mobileMax}) {
    margin: 0 auto 16px auto;
    justify-content: center;
  }
`;

export const CountryItem = styled.div`
  margin-bottom: 6px;
  display: flex;
  align-items: center;

  img {
    margin-right: 6px;
    width: 20px;
    height: 20px;
  }
`;

export const Right = styled.div`
  display: flex;
  align-items: center;
  margin-left: 10px;
`;

export const PreviewImg = styled.img`
  width: 20px;
  height: 20px;
  margin-right: 3px;
`;
