import React, {useState} from 'react';
import {Wrapper, CountryItem, Right, PreviewImg} from './styled';
import bahrain from './images/bahrain.svg';
import kuwait from './images/kuwait.svg';
import oman from './images/oman.svg';
import qatar from './images/qatar.svg';
import saudiArabia from './images/saudi-arabia.svg';
import uae from './images/united-arab-emirates.svg';
import SelectLanguage from 'components/SelectLanguage';
import {createStyles, makeStyles, withStyles} from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';

const countries = [
  {id: 1, title: 'Bahrain', img: bahrain},
  {id: 2, title: 'UAE', img: uae},
  {id: 3, title: 'Saudi Arabia', img: saudiArabia},
  {id: 4, title: 'Kuwait', img: kuwait},
  {id: 5, title: 'Oman', img: oman},
  {id: 6, title: 'Qatar', img: qatar}
];

const CustomInput = withStyles((theme) =>
  createStyles({
    input: {
      color: '#000',
      fontSize: '14px',
      borderRadius: 10,
      position: 'relative',
      backgroundColor: 'transparent',
      border: 'none',
      width: '50px',
      padding: '4px 6px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: 'sans-serif',
      '&:focus': {
        borderColor: theme.palette.primary.main,
        borderRadius: 10,
        backgroundColor: 'transparent'
      }
    }
  })
)(InputBase);

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  formControl: {
    zIndex: 1,
    border: 'none',
    minWidth: 30,
    padding: 0
  },
  selectEmpty: {
    border: '1px solid lightgrey',
    borderRadius: 10,
    marginTop: 45,
    minWidth: '195px !important',
    boxShadow: 'none'
  },
  menuItem: {
    fontSize: '14px',
    display: 'flex',
    padding: '5px',
    borderBottom: '1px solid lightgrey',
    background: 'transparent',
    transition: '.2s ease',
    width: '95%',
    margin: '0 auto',
    '&:hover': {
      borderColor: 'transparent'
    }
  },
  icon: {
    fill: '#000'
  }
}));

const CountrySelector = () => {
  const [currentCountry, setCurrentCountry] = useState('Bahrain');

  const countryTitle = countries.filter((el) => el.title === currentCountry)[0].title;
  const countryImg = countries.filter((el) => el.title === currentCountry)[0].img;

  const classes = useStyles();

  const handleChange = (event) => {
    setCurrentCountry(event.target.value);
  };

  const Input = () => {
    return (
      <FormControl variant="outlined" className={classes.formControl}>
        <Select
          classes={{
            icon: classes.icon
          }}
          value={countryTitle}
          onChange={handleChange}
          inputProps={{
            name: 'value',
            MenuProps: {disableScrollLock: true}
          }}
          renderValue={(value) => `${value}`}
          input={<CustomInput />}
          MenuProps={{classes: {paper: classes.selectEmpty}}}
        >
          {countries.map((el, idx) => (
            <MenuItem key={idx} className={classes.menuItem} value={el.title}>
              <CountryItem>
                <img src={el.img} alt={el.title} />
                {el.title}
              </CountryItem>
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  };

  return (
    <Wrapper>
      {/** temprorary hidded */}
      <div style={{display: 'none'}}>
        <SelectLanguage />
      </div>
      {/*|*/}
      <Right>
        <PreviewImg src={countryImg} alt={countryTitle} />
        <Input />
      </Right>
    </Wrapper>
  );
};

CountrySelector.propTypes = {};

export default CountrySelector;
