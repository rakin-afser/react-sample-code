import React from 'react';
import { oneOfType, number, string, node } from 'prop-types';

import {
  Wrapper
} from "./styled";

const ErrorMessage = ({ children }) => (
  <Wrapper>
    {children}
  </Wrapper>
);

ErrorMessage.propTypes = {
  children: oneOfType([number, string, node])
};

ErrorMessage.defaultProps = {
  children: null
};

export default ErrorMessage;
