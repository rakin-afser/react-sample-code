import styled from 'styled-components';

export const Wrapper = styled.div`
  color: #ED484F;
  margin-top: 2px;
  min-height: 16px;
`;
