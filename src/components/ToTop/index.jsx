import React, {useEffect, useState} from 'react';
import {Wrap} from './styled';
import Icon from 'components/Icon';

const ToTop = () => {
  const [active, setActive] = useState(false);
  const [scroll, setScroll] = useState(0);

  useEffect(() => {
    const onScroll = () => {
      setScroll(window.scrollY);
      if (window.scrollY > window.innerHeight && scroll > window.scrollY) {
        setActive(true);
        setTimeout(() => {
          setActive(false);
        }, 5000);
      } else {
        setActive(false);
      }
    };
    window.addEventListener('scroll', onScroll);
    return () => {
      window.removeEventListener('scroll', onScroll);
    };
  }, [scroll, setActive, setScroll]);

  return (
    <Wrap onClick={() => window.scrollTo({top: 0, behavior: 'smooth'})} active={active}>
      <Icon type="back" />
    </Wrap>
  );
};

export default ToTop;
