import styled from 'styled-components';

export const Wrap = styled.span`
  display: flex;
  position: fixed;
  width: 37px;
  align-items: center;
  justify-content: center;
  border-radius: 4px 0 0 4px;
  background: #d6d6d5;
  top: 50%;
  margin-top: -15px;
  right: 0;
  cursor: pointer;
  z-index: 9;
  box-shadow: 2px 2px 3px rgb(0 0 0 / 20%);
  transform: translateX(${({active}) => (active ? '0' : '100%')});
  transition: transform 0.5s ease-in-out;

  i {
    transform: rotate(90deg);
  }
`;
