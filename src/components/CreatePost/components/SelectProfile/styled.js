import styled from 'styled-components';

export const ListItem = styled.div`
  display: flex;
  padding: 16px 24px;
  align-items: center;
  overflow: auto;
  border-bottom: ${({isLast}) => (isLast ? 'none' : '1px solid #f4f1f1')};
  p {
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    margin: 0;
  }
`;
export const Avatar = styled.img`
  width: 36px;
  height: 36px;
  margin: 0 16px 0 24px;
`;
