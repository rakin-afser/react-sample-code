import React from 'react';
import {Radio} from 'antd';
import ratryn from 'components/Post/img/kathryn_mccoy.png';
import hazelGlory from '../../images/hazelGlory.png';

import {ListItem, Avatar} from './styled';

const profiles = [
  {name: 'kathryn_mccoy', avatar: ratryn, id: 1},
  {name: 'Hazel & Glory', avatar: hazelGlory, id: 2}
];

const SelectProfile = ({selectProfile = (f) => f, selected = 1}) => (
  <Radio.Group style={{width: '100%'}} onChange={(e) => selectProfile(e.target.value)} value={selected}>
    {profiles.map((el, i) => (
      <ListItem isLast={i === profiles.length - 1}>
        <Radio key={el.id} value={el.id} />
        <Avatar src={el.avatar} alt={el.name} />
        <p>{el.name}</p>
        <div />
      </ListItem>
    ))}
  </Radio.Group>
);

export default SelectProfile;
