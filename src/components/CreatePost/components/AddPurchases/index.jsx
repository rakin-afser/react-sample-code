import React, {useState, useEffect} from 'react';
import {useQuery} from '@apollo/client';
import Icon from 'components/Icon';
import loupe from '../../images/loupe.png';
import {
  SearchBlock,
  TextBlock,
  ListBlock,
  SearchInputContainer,
  ListItem,
  CurrentPrice,
  CurrentPriceRed,
  SelectedCover,
  BlackCover,
  ListWrapper
} from './styled';
import {GET_ORDERS} from 'queries';
import {SELLER_PRODUCTS} from 'components/CreatePost/api/queries';
import {useUser} from 'hooks/reactiveVars';

const AddPurchases = ({userId, setCurrentStep = (f) => f, selectedPurchases = [], addPurchases = (f) => f}) => {
  const [user] = useUser();
  const {isSeller} = user || {};
  const [search, setSearch] = useState('');
  const query = user?.isSeller ? SELLER_PRODUCTS : GET_ORDERS;
  const variables = user?.isSeller ? {vendorId: user?.databaseId, first: 12, search} : {customerId: user?.databaseId};
  const {data, loading} = useQuery(query, {
    variables,
    onCompleted(dataItems) {
      const {products, orders} = dataItems || {};
      const isNoItems = user?.isSeller ? !products?.nodes?.length : !orders?.nodes?.length;
      if (isNoItems) {
        setCurrentStep(2);
      }
    }
  });
  const [searchedData, setSearchedData] = useState(data);

  const products = user?.isSeller
    ? data?.products?.nodes
    : data?.orders?.nodes
        ?.reduce(
          (accOrder, currOrder) => [
            ...accOrder,
            ...currOrder?.lineItems?.nodes?.map((item) => ({...item?.product, ...item?.variation}))
          ],
          []
        )
        ?.filter((p, i, a) => a.findIndex((pr) => pr.id === p.id) === i);

  useEffect(() => {
    if (products?.length && !loading) {
      setSearchedData(products);
    }
  }, [loading]);

  const onSearch = (e) => {
    const {
      target: {value}
    } = e;

    setSearch(value);

    if (user?.isSeller) return;

    if (value) {
      const searchValue = value.toLowerCase();
      setSearchedData(products?.filter((item) => item.name.toLowerCase().includes(searchValue)));
    } else {
      setSearchedData(products);
    }
  };

  return (
    <div>
      <SearchBlock>
        <SearchInputContainer>
          <input type="text" value={search} onChange={onSearch} placeholder="Search for product" />
          <img src={loupe} alt="loupe" />
        </SearchInputContainer>
        <TextBlock>
          <h3>{isSeller ? 'My Inventory' : 'Recent Purchases'}</h3>
          <span>{selectedPurchases.length > 1 && `${selectedPurchases.length} Items selected`}</span>
        </TextBlock>
        <ListWrapper>
          {loading && <Icon type="loader" svgStyle={{width: 24, height: 5, fill: '#ED484F'}} />}
          {!loading && !searchedData?.length && 'Not found results!'}
          <ListBlock>
            {!!searchedData?.length &&
              searchedData?.map((el) => (
                <ListItem key={el.id} onClick={() => addPurchases(el)}>
                  <SelectedCover>
                    <BlackCover isSelected={selectedPurchases?.map((i) => i?.databaseId)?.includes(el?.databaseId)}>
                      <Icon type="checkbox" color="#fff" />
                    </BlackCover>
                    <img src={el.image.sourceUrl} alt={el.name} />
                  </SelectedCover>
                  <h3>{el.name}</h3>
                  <p>
                    <CurrentPrice dangerouslySetInnerHTML={{__html: el.currencySymbol}} />
                    <CurrentPriceRed>{el.price}</CurrentPriceRed>
                    {/* <OldPrice dangerouslySetInnerHTML={{__html: el.currencySymbol}}/>
                        <OldPriceLineThrough>{el.price}</OldPriceLineThrough> */}
                  </p>
                </ListItem>
              ))}
          </ListBlock>
        </ListWrapper>
      </SearchBlock>
    </div>
  );
};

export default AddPurchases;
