import React, {useState, useRef} from 'react';
import axios from 'axios';
import {v1, v4 as uuidv4} from 'uuid';
import Icon from 'components/Icon';
import deleteImage from '../../images/delete_image.png';
import imgIcon from '../../images/img_icon.png';
import addIcon from '../../images/add_icon.png';

import {
  Wrapper,
  FilledWrapper,
  EmptyWrapper,
  CenteredBlock,
  IconsContainer,
  InputZone,
  WrapperMain,
  MediaHolder,
  SubImagesBlock,
  SubImage,
  AddButton,
  YoutubeTitle,
  YoutubeInput,
  CloseImage,
  SubImageContainer,
  IconPlay,
  ImageMain,
  VideoMain
} from './styled';
import Ratios from 'components/Ratios';

const AttachBlock = ({
  uploads = [],
  setUploads,
  video = '',
  noYoutubeLink,
  noPreview,
  addImages = (f) => f,
  updateImage = (f) => f,
  deleteItem = (f) => f,
  setVideo = (f) => f,
  setRatio = (f) => f,
  ratio,
  small
}) => {
  const [width, height] = ratio?.split(':') || [];
  const [main, setMain] = useState('');
  const videoRef = useRef(null);
  const [previewVideoStart, setPreviewVideoStart] = useState(false);

  const selectImage = (val) => {
    const stelectedFile = val.target.files[0];

    if (stelectedFile) {
      if (stelectedFile.size > 1000000) {
        alert('Maximum size of image should be 1MB.');
      } else {
        const imageData = {
          id: uuidv4(),
          name: stelectedFile.name,
          type: stelectedFile.type,
          url: URL.createObjectURL(stelectedFile),
          file: stelectedFile,
          loading: true
        };

        addImages(imageData);
        // uploadImage(imageData);
      }
    }
  };

  async function onUpload(e) {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      try {
        const images = await Promise.all(
          files.map((file) => {
            return new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.addEventListener('load', (ev) => {
                resolve({
                  id: v1(),
                  file,
                  result: ev.target.result
                });
              });
              reader.addEventListener('error', reject);
              reader.readAsDataURL(file);
            });
          })
        );
        setUploads([...uploads, ...images]);
      } catch (error) {
        console.log(`error`, error);
      }
    }
  }

  const selectedIndex = uploads?.findIndex((a) => a?.id === main);
  const mainIndex = selectedIndex >= 0 ? selectedIndex : 0;

  function renderContent() {
    function renderMain() {
      const onVideoPlay = () => {
        if (previewVideoStart) {
          setPreviewVideoStart(false);
          videoRef.current.pause();
        } else {
          setPreviewVideoStart(true);
          videoRef.current.play();
        }
      };

      switch (uploads?.[mainIndex]?.file?.type?.split('/')?.[0]) {
        case 'video':
          return (
            <>
              <IconPlay previewVideoStart={previewVideoStart} onClick={onVideoPlay} type="play" />
              <CloseImage
                isMain
                onClick={() => setUploads(uploads?.filter((_a, i) => i !== mainIndex))}
                src={deleteImage}
                alt="close"
              />
              <VideoMain ref={videoRef} src={uploads?.[mainIndex]?.result} muted />
            </>
          );
        case 'image':
          return (
            <>
              <CloseImage
                isMain
                onClick={() => setUploads(uploads?.filter((_a, i) => i !== mainIndex))}
                src={deleteImage}
                alt="close"
              />
              <ImageMain src={uploads?.[mainIndex]?.result} alt="preview" />
            </>
          );
        default:
          return null;
      }
    }

    function renderSubs(el) {
      switch (el?.file?.type?.split('/')?.[0]) {
        case 'video':
          return (
            <>
              <IconPlay type="play" onClick={() => setMain(el?.id)} />
              <CloseImage
                onClick={() => setUploads(uploads?.filter((a) => a.id !== el.id))}
                src={deleteImage}
                alt="close"
              />
            </>
          );
        case 'image':
          return (
            <>
              <CloseImage
                onClick={() => setUploads(uploads?.filter((a) => a.id !== el.id))}
                src={deleteImage}
                alt="close"
              />
              <SubImage src={el?.result} onClick={() => setMain(el?.id)} alt="preview" />
            </>
          );
        default:
          return null;
      }
    }

    if ((uploads.length && !noPreview) || small) {
      return (
        <FilledWrapper>
          {!small && (
            <WrapperMain ratio={height / width}>
              <MediaHolder>{renderMain()}</MediaHolder>
            </WrapperMain>
          )}
          <SubImagesBlock isBlockFilled={uploads.length === 5}>
            {uploads.map((el) => (
              <SubImageContainer key={el.id}>{renderSubs(el)}</SubImageContainer>
            ))}
            {uploads.length < 5 && (
              <AddButton>
                <InputZone type="file" onChange={onUpload} multiple />
                <Icon type="plus" />
                <p>Add More</p>
              </AddButton>
            )}
          </SubImagesBlock>
        </FilledWrapper>
      );
    }

    if (!noPreview) {
      return (
        <EmptyWrapper ratio={height / width}>
          <InputZone type="file" onChange={onUpload} multiple />
          <CenteredBlock>
            <IconsContainer>
              <img src={imgIcon} alt="img-icon" />
              <img className="add-icom-image" src={addIcon} alt="add-icon" />
            </IconsContainer>
            <p>Upoload Images or Video</p>
          </CenteredBlock>
        </EmptyWrapper>
      );
    }

    return null;
  }

  return (
    <div>
      <Wrapper>{renderContent()}</Wrapper>
      <Ratios ratio={ratio} setRatio={setRatio} />
      {noYoutubeLink ? null : (
        <YoutubeTitle>
          Add YouTube Link
          <YoutubeInput
            placeholder="Here you can put your YouTube link"
            value={video}
            onChange={(e) => setVideo(e.target.value)}
          />
        </YoutubeTitle>
      )}
    </div>
  );
};

export default AttachBlock;
