import styled from 'styled-components/macro';
import Icon from 'components/Icon';

export const Wrapper = styled.div`
  max-width: 242px;
  margin: 0 auto;
`;

export const FilledWrapper = styled.div`
  margin: 12px auto 19px;
`;

export const EmptyWrapper = styled.div`
  padding-bottom: ${({ratio}) => (ratio ? ratio * 100 : 100)}%;
  position: relative;
  background: #ffffff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin: 59px auto;
  transition: padding-bottom 0.3s ease-in;
`;

export const CenteredBlock = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-weight: bold;
  font-size: 14px;
  line-height: 140%;
  color: #ed484f;
  text-align: center;
`;

export const IconsContainer = styled.div`
  display: inline-block;
  position: relative;
  margin-bottom: 9px;
  .add-icom-image {
    position: absolute;
    top: -13px;
    right: -13px;
  }
`;

export const InputZone = styled.input`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  cursor: pointer;
  z-index: 100;
  opacity: 0;
  width: 100%;
`;

export const WrapperMain = styled.div`
  position: relative;
  margin: 0 auto 16px;
  padding-bottom: ${({ratio}) => (ratio ? ratio * 100 : 100)}%;
  transition: padding-bottom 0.3s ease;
`;

export const MediaHolder = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

export const ImageMain = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const VideoMain = styled.video`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const SubImagesBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: ${({isBlockFilled}) => (isBlockFilled ? 'space-between' : 'flex-start')};
`;

export const SubImage = styled.img`
  position: relative;
  width: 100%;
  height: 100%;
  border-radius: 4px;
  cursor: pointer;
  object-fit: cover;
`;

export const CloseImage = styled.img`
  cursor: pointer;
  width: 24px;
  height: 24px;
  position: absolute;
  z-index: 9;
  ${({isMain}) => (isMain ? {top: '9px', right: '9px'} : {top: '3px', right: '3px'})}
`;

export const AddButton = styled.div`
  position: relative;
  width: 72px;
  height: 72px;
  background: #ffffff;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  font-size: 10px;
  color: #ed484f;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  &,
  * {
    cursor: pointer;
  }
  p {
    margin: 2px 0 0;
    text-align: center;
    line-height: 13px;
  }
`;

export const YoutubeTitle = styled.label`
  font-weight: bold;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  margin-bottom: 7px;
  &:focus,
  &:active,
  &:focus-within {
    color: #4a90e2;
  }
`;

export const YoutubeInput = styled.input`
  background: #ffffff;
  border: 1px solid #a7a7a7;
  box-sizing: border-box;
  border-radius: 2px;
  width: 100%;
  margin-bottom: 16px;
  height: 38px;
  padding: 10px 10px 11px 16px;
  color: #000;
`;

export const SubImageContainer = styled.div`
  position: relative;
  ${({isBlockFilled}) => (isBlockFilled ? null : {marginRight: '5px'})};
  width: 72px;
  height: 72px;
`;

export const Loader = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(255, 255, 255, 0.7);
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1;
`;

export const IconPlay = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 1;
  border-radius: 50%;
  background-color: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.3);
  width: 50px;
  height: 50px;
  cursor: pointer;
  transition: ease 0.4s;

  ${({previewVideoStart}) =>
    previewVideoStart &&
    `
    opacity: 0;
  `};

  &:hover {
    opacity: 1;
  }

  svg {
    margin-left: 3px;
  }
`;
