import Slider from 'react-slick';
import styled from 'styled-components';
import Icon from 'components/Icon';

export const Wrapper = styled.div`
  padding: 9px 24px 0;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 22px;
`;

export const User = styled.div`
  display: flex;
  img {
    object-fit: cover;
    width: 48px;
    height: 48px;
    border-radius: 50%;
    margin-right: 9px;
  }
`;

export const TextBlock = styled.div`
  h3 {
    font-weight: bold;
    font-size: 16px;
    line-height: 140%;
    color: #000000;
    margin: 5px 0 0;
  }
`;

export const SubText = styled.p`
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  display: flex;
  align-items: center;
  color: #8f8f8f;
  margin-bottom: 0;
`;

export const ButtonBlock = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const TitleBlock = styled.h4`
  margin-bottom: 4px;
  font-weight: 700;
  font-size: 16px;
  line-height: 140%;
  color: #3a3a3a;
`;

export const DescriptionBlock = styled.div`
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-transform: lowercase;
  color: #000000;
  mix-blend-mode: normal;
  opacity: 0.8;
  margin-bottom: 16px;
`;

export const PostImage = styled.div`
  width: 260px;
  margin: 16px auto;
`;

export const ProductBlock = styled.div`
  padding: 10px 9px;
  background: #ffffff;
  border: 1px solid #eeeeee;
  box-sizing: border-box;
  border-radius: 2px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 10px;
`;

export const ProductInfo = styled.div`
  display: flex;
  img {
    width: 50px;
    height: 50px;
    margin-right: 14px;
  }
  h3 {
    font-weight: 500;
    font-size: 14px;
    line-height: 140%;
    display: flex;
    align-items: center;
    color: #000000;
    margin-bottom: 2px;
    white-space: nowrap;
    position: relative;
    overflow: hidden;
    max-width: 146px;
    text-overflow: ellipsis !important;
  }
`;

export const ShopButton = styled.button`
  border: 1px solid #ed484f;
  border-radius: 24px;
  padding: 7px 12px;
  color: #ed484f;
  margin-right: 7px;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  background-color: #fff;
  width: 99px;
`;

export const PostFooter = styled.div`
  margin-top: 18px;
  i {
    cursor: pointer;
    margin-right: 25px;
  }
`;

export const CancelButton = styled.button`
  font-weight: normal;
  font-size: 14px;
  height: 23px;
  color: ${({isBack}) => (isBack ? '#ED484F' : '#656565')};
  border: none;
  background: none;
  margin-right: ${({isBack}) => (isBack ? '13px' : '32px')};
  cursor: pointer;
`;
export const NextButton = styled.button`
  background: ${({isNotAvailable}) => (isNotAvailable ? '#C3C3C3' : '#ed484f')};
  border: none;
  border-radius: 24px;
  font-weight: 500;
  font-size: 14px;
  color: #ffffff;
  width: ${({isPost}) => (isPost ? '82px' : '95px')};
  height: 28px;
  cursor: pointer;
`;

export const StyledSlider = styled(Slider)`
  width: 260px;
  margin: 0 auto;
  padding-bottom: 30px;

  .slick-dots {
    bottom: 0 !important;

    & .slick-active button:before {
      color: #ea2c34;
    }

    & button:before {
      font-size: 7px;
      color: #e4e4e4;
      opacity: 1 !important;
    }
  }

  .slick-dots li {
    margin: 0 -2px;
  }
`;

export const Slide = styled.div`
  position: relative;
`;

export const RatioHolder = styled.div`
  padding-bottom: ${({ratio}) => (ratio ? ratio * 100 : 100)}%;
  transition: padding-bottom 0.3s ease;
`;

export const MediaHolder = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
`;

export const SlideVideo = styled.video`
  width: 100%;
  height: 100%;
  display: block;
  object-fit: cover;
`;

export const SlideImg = styled.img`
  width: 100%;
  height: 100%;
  display: block;
  object-fit: cover;
`;

export const IconPlay = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 1;
  border-radius: 50%;
  background-color: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.3);
  width: 50px;
  height: 50px;
  cursor: pointer;
  transition: ease 0.4s;

  ${({previewVideoStart}) =>
    previewVideoStart &&
    `
    opacity: 0;
  `};

  &:hover {
    opacity: 1;
  }

  svg {
    margin-left: 3px;
  }
`;
