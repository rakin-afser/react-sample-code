import React, {useState, Fragment, useRef} from 'react';
import moment from 'moment';

import Tags from 'components/Tags';
import {
  Wrapper,
  Header,
  User,
  TextBlock,
  SubText,
  ButtonBlock,
  DescriptionBlock,
  PostImage,
  ProductBlock,
  ProductInfo,
  ShopButton,
  PostFooter,
  NextButton,
  CancelButton,
  StyledSlider,
  SlideVideo,
  SlideImg,
  IconPlay,
  TitleBlock,
  Slide,
  RatioHolder,
  MediaHolder
} from './styled';
import axiosClient from 'axiosClient';
import axios from 'axios';
import {useUser} from 'hooks/reactiveVars';
import parse from 'html-react-parser';
import {useCreatePostMutation} from 'hooks/mutations';
import Btn from 'components/Btn';

const PostResult = ({data, onClose, uploads, back = (f) => f}) => {
  const {title, content, video, relatedProducts, tags, ratio} = data;
  const [width, height] = ratio?.split(':') || [];
  const [user] = useUser();
  const [loading, setLoading] = useState(false);
  const videoRef = useRef(null);
  const [previewVideoStart, setPreviewVideoStart] = useState(false);
  const [post, {loading: loadingPost}] = useCreatePostMutation();

  async function onPost() {
    const filesToUpload = uploads?.map((upload) => {
      const formData = new FormData();
      formData.append('file', upload?.file);
      return axiosClient.post('/', formData);
    });
    try {
      setLoading(true);
      const res = await axios.all(filesToUpload);
      const galleryId = res?.map((item) => item.data.id);
      setLoading(false);

      post({
        variables: {
          input: {
            status: 'PUBLISH',
            ratio,
            title,
            content,
            video,
            tags,
            relatedProducts: relatedProducts?.nodes?.map((p) => p.databaseId),
            galleryId
          }
        }
      });
    } catch (error) {
      setLoading(false);
      console.log(`error`, error);
    }
  }

  function renderSlides(slide) {
    const onVideoPlay = () => {
      if (previewVideoStart) {
        setPreviewVideoStart(false);
        videoRef.current.pause();
      } else {
        setPreviewVideoStart(true);
        videoRef.current.play();
      }
    };

    switch (slide?.file?.type?.split('/')?.[0]) {
      case 'video':
        return (
          <>
            <SlideVideo ref={videoRef} src={slide?.result} muted />
            <IconPlay previewVideoStart={previewVideoStart} onClick={onVideoPlay} type="play" />
          </>
        );
      case 'image':
        return <SlideImg src={slide?.result} alt="preview" />;
      default:
        return null;
    }
  }

  return (
    <Wrapper>
      <Header>
        <User>
          <img src={user?.avatar?.url} alt={user?.name} />
          <TextBlock>
            <h3>{user?.name}</h3>
            <SubText>{moment().format('DD MMM YYYY')}</SubText>
          </TextBlock>
        </User>
      </Header>
      <TitleBlock>{title}</TitleBlock>
      <DescriptionBlock>
        {content}
        <Tags items={tags?.nodes} />
      </DescriptionBlock>
      <PostImage>
        <StyledSlider dots arrows={false}>
          {uploads?.map((slide) => (
            <Slide>
              <RatioHolder ratio={height / width} />
              <MediaHolder>{renderSlides(slide)}</MediaHolder>
            </Slide>
          ))}
        </StyledSlider>
      </PostImage>
      {relatedProducts?.nodes?.length
        ? relatedProducts?.nodes?.map((product) => (
            <ProductBlock>
              <ProductInfo>
                <img src={product?.image?.sourceUrl} alt={product?.name} />
                <div>
                  <h3>{product?.name}</h3>
                  {product?.seller ? <SubText>Seller: {product?.seller?.name}</SubText> : null}
                  {product?.shippingType ? <SubText>{product?.shippingType}</SubText> : null}
                </div>
              </ProductInfo>
              <ShopButton>
                <span>{parse(product?.currencySymbol || '')}</span>
                {product?.price}
              </ShopButton>
            </ProductBlock>
          ))
        : null}
      <PostFooter>
        <ButtonBlock>
          <CancelButton style={{marginTop: '5px'}} isBack onClick={back}>
            Back to Edit
          </CancelButton>
          <Btn kind="primary-small" loading={loading || loadingPost} onClick={() => onPost()}>
            Post
          </Btn>
        </ButtonBlock>
      </PostFooter>
    </Wrapper>
  );
};

export default PostResult;
