import React, {useState, useEffect} from 'react';
import {useQuery} from '@apollo/client';
import {Mentions} from 'antd';

import {
  Tags,
  Tag
} from 'containers/MyProfile/MyProfileMobile/components/Posts/containers/New/components/Details/styled';
import {TitleInput, TopBlock, TagsBlock, ContentWrapper, Desc} from './styled';
import Icon from 'components/Icon';
import {GET_TAGS} from 'components/CreatePost/api/queries';

const {Option} = Mentions;

const PostDescription = ({
  title = '',
  setTitle = () => {},
  content = '',
  setContent = (f) => f,
  tags = [],
  addTag = () => {},
  removeTag = () => {}
}) => {
  const [currentTag, setCurrentTag] = useState('');
  const [currentOptions, setCurrentOptions] = useState([]);
  const {data: tagsData, loading: tagsLoading, error: tagsError} = useQuery(GET_TAGS, {
    variables: {search: currentTag}
  });

  useEffect(() => {
    const options = tagsData?.tags?.nodes?.map((i) => i.name) || [];
    if (!tagsLoading) {
      if (options.includes(currentTag)) {
        setCurrentOptions(options);
      } else {
        setCurrentOptions([...options, currentTag]);
      }
    }
  }, [currentTag, tagsData, tagsLoading]);

  const onSelect = (tag) => {
    addTag(tag?.value);

    if (tag?.value) {
      const newContent = content.replace(`#${tag?.value}`, '');
      
      setContent(newContent.replace('#', ''));
    }
  };

  const onChange = (value) => {
    setContent(value);
  };

  const onSearch = (value) => {
    setCurrentTag(value);
  };

  return (
    <div>
      <TopBlock>
        <TitleInput type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Title" />
        <Desc
          rows={9}
          prefix="#"
          placeholder="You can put here description for your post"
          onChange={onChange}
          value={content}
          onSelect={onSelect}
          onSearch={onSearch}
        >
          {currentOptions.map((option) => (
            <Option key={option} value={option}>
              #{option}
            </Option>
          ))}
        </Desc>
      </TopBlock>

      <TagsBlock>
        <ContentWrapper>
          {tags?.length ? <div style={{marginBottom: '10px'}}>Tags</div> : null}
          <Tags>
            {tags?.map((tag, i) => (
              <Tag key={i}>
                #{tag} <Icon onClick={() => removeTag(tag)} type="close" color="#464646" />
              </Tag>
            ))}
          </Tags>
        </ContentWrapper>
      </TagsBlock>
    </div>
  );
};

export default PostDescription;
