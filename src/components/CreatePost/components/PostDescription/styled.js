import styled from 'styled-components';
import {Mentions} from 'antd';

export const TopBlock = styled.div`
  border-bottom: 1px solid #efefef;
  padding: 18px 0;
  min-height: 242px;
  max-height: 242px;
`;

export const TitleInput = styled.input`
  width: 100%;
  height: 40px;
  border: none;
  outline: none;
  padding: 0;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const TagsBlock = styled.div`
  padding: 13px 0;
  min-height: 225px;
  h3 {
    font-weight: 500;
    font-size: 14px;
    color: #000000;
    margin-bottom: 7px;
  }
  p {
    margin-bottom: 0;
  }
`;

export const Tag = styled.div`
  display: inline-block;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #666666;
  margin: 0 15px 15px 0;
  white-space: nowrap;
`;

export const AddTagsButton = styled.button`
  width: 76px;
  height: 25px;
  background: #efefef;
  border-radius: 70px;
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  color: #000000;
  border: none;
  cursor: pointer;
  margin-right: 15px;
`;

export const TagsInput = styled.input`
  outline: none;
  padding: 0;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  border: none;
`;

export const ContentWrapper = styled.div`
  position: relative;
`;

export const Wrapper = styled.div`
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.28);
  background: #ffffff;
  border-radius: 8px;
  padding-top: 12px;
  position: absolute;
  left: 17px;
  bottom: 56px;
  z-index: 10;
  .ScrollbarsCustom-Wrapper {
    right: 0 !important;
  }
  ${({customStyles}) => (customStyles ? {...customStyles} : null)}
`;

export const Title = styled.h3`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 16px;
  margin-bottom: 20px;
`;

export const ItemsList = styled.ul`
  max-height: 224px;
  -ms-overflow-style: none; // IE 10+
  overflow: -moz-scrollbars-none; // Firefox
  overflow-y: auto;
  &::-webkit-scrollbar {
    display: none; // Safari and Chrome
  }
`;

export const Item = styled.li`
  padding: 10px 5px 10px 16px;
  border-bottom: 1px solid #efefef;
  width: 100%;
  cursor: pointer;
  display: flex;
`;

export const Name = styled.h3`
  font-weight: 500;
  font-size: 12px;
  margin-bottom: 2px;
`;

export const Subtitle = styled.p`
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #a7a7a7;
  margin: 0;
`;

export const Avatar = styled.img`
  width: 36px;
  height: 36px;
  margin-right: 12px;
`;

export const Desc = styled(Mentions)`
  &&& {
    margin-bottom: 24px;
    font-size: 14px;
    line-height: 140%;
    color: #000;
    border: none;
    height: 198px;
    width: 100%;
    outline: none;
    padding: 0;
    resize: none;
    box-shadow: none;

    &::placeholder {
      padding-top: 4px;
      color: #999;
      font-size: 14px;
      font-weight: 400;
    }

    > textarea {
      padding: 0;
    }
  }
`;
