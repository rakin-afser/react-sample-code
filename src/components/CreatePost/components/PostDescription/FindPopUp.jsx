import React from 'react';
import Scrollbars from 'react-scrollbars-custom';

import {Wrapper, TopContainer, Title, ItemsList, Item, Name, Subtitle} from './styled';

const FindPopUp = ({
  query = '',
  withoutFiltering,
  availiableTags,
  select = (f) => f,
  type,
  withHeader = true,
  customStyles
}) => {
  const selectedData = availiableTags;
  const arrayTodisplay = withoutFiltering
    ? availiableTags
    : selectedData.filter((el) => el.name.toLowerCase().includes(query.toLowerCase()));

  return type && arrayTodisplay.length ? (
    <Wrapper customStyles={customStyles}>
      {withHeader && (
        <TopContainer>
          <Title>{type === '#' ? 'Hashtags' : 'Reply to'}</Title>
        </TopContainer>
      )}
      <ItemsList>
        <Scrollbars
          clientWidth={4}
          noDefaultStyles={false}
          noScroll={false}
          style={{height: '224px', width: '240px'}}
          thumbYProps={{className: 'thumbY'}}
        >
          {arrayTodisplay.map((el, i) => (
            <Item key={i} onClick={() => select(arrayTodisplay[i])}>
              <div>
                <Name>{el?.name}</Name>
                <Subtitle>{el?.subtitle}</Subtitle>
              </div>
            </Item>
          ))}
        </Scrollbars>
      </ItemsList>
    </Wrapper>
  ) : null;
};

export default FindPopUp;
