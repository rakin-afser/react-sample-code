import styled from 'styled-components';

export const SocialBlock = styled.div`
  margin: 16px 0 320px;
  padding-top: 26px;
`;

export const SocialRow = styled.p`
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #656565;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 18px;
`;
