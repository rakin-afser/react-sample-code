import React from 'react';
import {Switch} from 'antd';
import {SocialBlock, SocialRow} from './styled';

const SocialSharing = () => (
  <SocialBlock>
    <SocialRow>
      Facebook
      <Switch />
    </SocialRow>
    <SocialRow>
      Instagram
      <Switch />
    </SocialRow>
    <SocialRow>
      WhatsApp
      <Switch />
    </SocialRow>
  </SocialBlock>
);

export default SocialSharing;
