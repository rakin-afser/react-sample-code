import React from 'react';
import {Modal} from 'antd';
import {WarningText} from './styled';
import {CancelButton, NextButton} from '../../styled';

const AddPurchases = ({close = (f) => f, cancelClosing = (f) => f}) => (
  <Modal visible={true} footer={null} closeIcon={null}>
    <WarningText>Are you sure you want to close? Changes you made will not be saved.</WarningText>
    <CancelButton onClick={cancelClosing}>Cancel</CancelButton>
    <NextButton onClick={close}>Continue</NextButton>
  </Modal>
);

export default AddPurchases;
