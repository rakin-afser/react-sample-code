import styled from 'styled-components';

export const WarningText = styled.p`
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #999999;
`;
