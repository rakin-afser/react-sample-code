import React, {useEffect, useState} from 'react';
import {Modal} from 'antd';
import Icon from 'components/Icon';

import AttachBlock from './components/AttachBlock';
import AddPurchases from './components/AddPurchases';
import PostDescription from './components/PostDescription';
// import SocialSharing from './components/SocialSharing';
import PostResult from './components/PostResult';
import arrowBack from './images/arrow_left.png';
import {
  LeftBlock,
  SkipButton,
  CloseContainer,
  Header,
  ModalBody,
  ProgressBarWrapper,
  ProgressBar,
  ArrowBack,
  Title,
  Description,
  ButtonsBlock,
  CancelButton,
  NextButton
} from './styled';
import {useUser} from 'hooks/reactiveVars';

const CreatePost = ({visible, onClose}) => {
  const [user] = useUser();
  const {isSeller} = user || {};

  const [ratio, setRatio] = useState('1:1');
  const [currentStep, setCurrentStep] = useState(0);
  const [video, setVideo] = useState('');
  const [uploads, setUploads] = useState([]);
  const [purchases, setPurchases] = useState([]);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [tags, setTags] = useState([]);

  const addImages = (value) => {
    setUploads((oldArray) => [...oldArray, value]);
  };

  const addPurchases = (item) => {
    if (purchases?.map((i) => i?.databaseId)?.includes(item?.databaseId)) {
      const newArray = [...purchases];
      newArray.splice(item, 1);
      setPurchases([...purchases]?.filter((i) => i.databaseId !== item?.databaseId));
    } else {
      setPurchases((oldArray) => [...oldArray, item]);
    }
  };

  const addTag = (tag) => {
    if (!tag) return;
    if (!tags.includes(tag)) {
      setTags([...tags, tag]);
    }
  };

  const removeTag = (tag) => {
    setTags(tags.filter((item) => item !== tag));
  };

  const data = {
    author: {
      node: {
        ...user
      }
    },
    ratio,
    title,
    content,
    galleryImages: {
      nodes: uploads?.map((upload) => ({
        mediaItemUrl: URL.createObjectURL(upload?.file),
        mimeType: upload?.file?.type?.split('/')?.[0] === 'video' ? 'default-video' : upload?.file?.type
      }))
    },
    tags: {nodes: tags?.map((i) => ({name: i}))},
    featuredImage: {node: {sourceUrl: uploads?.[0]?.result, altText: title}},
    relatedProducts: {nodes: purchases},
    video
  };

  const updateImage = (id, params) => {
    setUploads((oldArray) => {
      const newArray = oldArray.map((el) => {
        if (el.id === id) {
          const newEl = {
            ...el,
            ...params
          };

          return newEl;
        }

        return el;
      });

      return newArray;
    });
  };

  const showScreen = [
    // {title: 'Select Profile', component: <SelectProfile selected={profileId} selectProfile={setProfileId} />},
    {
      title: 'Add Photos / Videos',
      description: 'You may upload up to 5 files. Maximum 1Mb per file',
      component: (
        <AttachBlock
          ratio={ratio}
          setRatio={setRatio}
          uploads={uploads}
          setUploads={setUploads}
          addImages={addImages}
          updateImage={updateImage}
          video={video}
          setVideo={setVideo}
          noYoutubeLink
        />
      )
    },
    {
      title: isSeller ? 'Add Products' : 'Add Purchases',
      component: (
        <AddPurchases setCurrentStep={setCurrentStep} selectedPurchases={purchases} addPurchases={addPurchases} />
      )
    },
    {
      title: 'Description',
      component: (
        <PostDescription
          title={title}
          setTitle={setTitle}
          setContent={setContent}
          tags={tags}
          content={content}
          addTag={addTag}
          removeTag={removeTag}
        />
      )
    }
    // {
    //   title: 'Social Sharing',
    //   component: <SocialSharing />
    // }
  ];

  return (
    <Modal
      wrapClassName="create-post-profile"
      visible={visible}
      onCancel={onClose}
      width={425}
      footer={null}
      destroyOnClose
      bodyStyle={{padding: '15px 0 16px', minHeight: currentStep ? '583px' : '248px'}}
      closeIcon={
        <CloseContainer>
          <Icon color="#fff" type="close" />
        </CloseContainer>
      }
    >
      {currentStep < 3 ? (
        <div>
          <Header>
            {currentStep ? (
              <ProgressBarWrapper>
                <ProgressBar step={currentStep} isFinalStep={currentStep === 3} />
              </ProgressBarWrapper>
            ) : null}
            <LeftBlock>
              {currentStep ? (
                <ArrowBack onClick={() => setCurrentStep(currentStep - 1)}>
                  <img src={arrowBack} alt="arrow" />
                </ArrowBack>
              ) : null}
              <Title>{showScreen[currentStep].title}</Title>
            </LeftBlock>
            {currentStep === 1 ? (
              <SkipButton onClick={() => setCurrentStep(currentStep + 1)}>
                <span>Skip</span>
                <Icon type="arrow" width={8} height={10} color="#ED484F" />
              </SkipButton>
            ) : null}
          </Header>

          <ModalBody withPadding={currentStep >= 0}>
            {showScreen[currentStep].description && <Description>{showScreen[currentStep].description}</Description>}
            {showScreen[currentStep].component}
            <ButtonsBlock>
              <CancelButton onClick={onClose}>Cancel</CancelButton>
              <NextButton
                disabled={currentStep >= 0 && !uploads.length}
                isNotAvailable={currentStep >= 0 && !uploads.length}
                onClick={() => setCurrentStep(currentStep + 1)}
              >
                <span>Next</span> <Icon type="arrow" width={8} height={10} color={!uploads.length ? '#ccc' : '#000'} />
              </NextButton>
            </ButtonsBlock>
          </ModalBody>
        </div>
      ) : (
        <PostResult onClose={onClose} data={data} uploads={uploads} back={() => setCurrentStep(currentStep - 1)} />
      )}
    </Modal>
  );
};

export default CreatePost;
