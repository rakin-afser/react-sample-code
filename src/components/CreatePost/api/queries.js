import {gql} from '@apollo/client';

export const SELLER_PRODUCTS = gql`
  query($search: String, $cursor: String, $first: Int, $vendorId: Int) {
    products(where: {vendor: $vendorId, search: $search}, first: $first, after: $cursor) {
      nodes {
        name
        id
        databaseId
        image {
          id
          sourceUrl(size: SHOP_CATALOG)
          altText
        }
        ... on SimpleProduct {
          regularPrice
          price
          salePrice
        }
        ... on VariableProduct {
          regularPrice
          price
          salePrice
        }
      }
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
        total
      }
    }
  }
`;

export const GET_TAGS = gql`
  query getHashtags($first: Int, $after: String, $search: String) {
    tags(where: {search: $search}, first: $first, after: $after) {
      nodes {
        name
        id
        slug
        databaseId
      }
      pageInfo {
        total
      }
    }
  }
`;
