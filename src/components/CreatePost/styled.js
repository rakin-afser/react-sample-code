import styled from 'styled-components';

export const Header = styled.div`
  padding: 0 24px;
  border-bottom: 1px solid #efefef;
  display: flex;
  align-items: center;
  padding-bottom: 14px;
  justify-content: space-between;
  h3 {
    margin-bottom: 0;
  }
`;

export const CloseContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 28px;
  height: 28px;
  transition: ease 0.6s;
  border-radius: 7px;

  svg {
    transition: ease 0.6s;
  }

  &:hover {
    background: #f2f2f2;

    svg,
    svg path {
      fill: #000;
    }
  }
`;

export const ArrowBack = styled.div`
  img {
    cursor: pointer;
    margin-right: 14px;
  }
`;

export const LeftBlock = styled.div`
  display: flex;
  align-items: center;
`;

export const ProgressBarWrapper = styled.div`
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
  position: absolute;
  top: 0;
  left: 1px;
  height: 5px;
  background-color: #efefef;
  width: calc(100% - 2px);
`;

export const ProgressBar = styled(ProgressBarWrapper)`
  left: 0;
  border-top-right-radius: ${({isFinalStep}) => (isFinalStep ? '8px' : '0')};
  background-color: #ed484f;
  width: ${({step}) => `${step * 25}%`};
`;

export const Title = styled.h3`
  font-weight: 500;
  font-size: 16px;
  margin-bottom: 14px;
  color: #000000;
`;

export const Description = styled.p`
  color: #464646;
  margin: 16px 0 0;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  text-align: center;

  color: #666666;
`;

export const ModalBody = styled.div`
  padding: ${({withPadding}) => (withPadding ? '0 24px' : '0')};
`;

export const SkipButton = styled.div`
  color: #ed484f;
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  display: flex;
  align-items: center;
  cursor: pointer;
  span {
    margin-right: 9px;
  }
`;

export const ButtonsBlock = styled.div`
  padding-top: 15px;
  background-color: white;
  border-top: 1px solid #e4e4e4;
  text-align: right;
  bottom: 16px;
`;

export const CancelButton = styled.button`
  font-weight: normal;
  font-size: 14px;
  height: 23px;
  color: ${({isBack}) => (isBack ? '#ED484F' : '#656565')};
  border: none;
  background: none;
  margin-right: ${({isBack}) => (isBack ? '13px' : '32px')};
  cursor: pointer;
  transition: ease 0.6s;

  &:hover {
    color: #ed484f;
  }
`;

export const NextButton = styled.button`
  background: #fff;
  font-weight: 500;
  font-size: 14px;
  color: ${({isNotAvailable}) => (isNotAvailable ? '#ccc' : '#000')};
  border-color: ${({isNotAvailable}) => (isNotAvailable ? '#ccc' : '#000')};
  width: ${({isPost}) => (isPost ? '82px' : '95px')};
  height: 28px;
  border: 1px solid;
  border-radius: 24px;
  cursor: pointer;
  transition: ease 0.6s;
  i {
    vertical-align: middle;
    margin-left: 5px;
    svg,
    svg path {
      fill: ${({isNotAvailable}) => (isNotAvailable ? '#ccc' : '#000')};
    }
  }
  ${({isNotAvailable}) =>
    isNotAvailable || {
      '&:hover': {
        backgroundColor: '#ed484f',
        border: '#ed484f',
        color: '#fff',
        'svg path': {
          fill: '#fff'
        }
      }
    }};
`;
