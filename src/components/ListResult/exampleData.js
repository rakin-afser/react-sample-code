import userAvatar from './img/avatar.png';

import first from './img/1_1.jpg';
import first2 from './img/1_2.jpg';
import first3 from './img/1_3.jpg';

import second from './img/2_1.jpg';
import second2 from './img/2_2.jpg';
import second3 from './img/2_3.png';

import third from './img/3_1.jpg';
import third2 from './img/3_2.jpg';
import third3 from './img/3_3.jpg';

import fourth from './img/4_1.jpg';
import fourth2 from './img/4_2.jpg';
import fourth3 from './img/4_3.jpg';

export default [
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [first, first2, first3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [second, second2, second3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [third, third2, third3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [fourth, fourth2, fourth3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [first, first2, first3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [second, second2, second3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [third, third2, third3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [fourth, fourth2, fourth3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [first, first2, first3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [second, second2, second3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [third, third2, third3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [fourth, fourth2, fourth3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [first, first2, first3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [second, second2, second3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [third, third2, third3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  },
  {
    avatar: userAvatar,
    title: 'Dream Dreams',
    followers: '3k',
    products: 999,
    images: [fourth, fourth2, fourth3],
    tags: ['Cream', 'canvas', 'Big', 'tags']
  }
];
