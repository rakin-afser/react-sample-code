import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import List from 'components/Cards/List';
import Error from 'components/Error';
import {Grid} from './styled';

const ListResult = ({data, error}) => {
  const histroy = useHistory();

  if (error) return <Error />;

  if (!data?.length) return null;

  return (
    <Grid>
      {data?.map((item, index) => (
        <List onClick={() => histroy.push('profile/lists')} key={index} data={item} />
      ))}
    </Grid>
  );
};

ListResult.propTypes = {
  data: PropTypes.shape().isRequired,
  error: PropTypes.bool.isRequired
};

export default ListResult;
