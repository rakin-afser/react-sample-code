import styled from 'styled-components';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  position: absolute;
  left: 16px;
  top: 24px;
  width: 42px;
  height: 42px;
  background: #ffffff;
  border-radius: 50%;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 10;

  svg {
    stroke: #000;
    transition: all 0.3s ease;

    &:hover {
      stroke: ${primaryColor};
    }
  }
`;
