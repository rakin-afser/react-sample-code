import React from 'react';
import {func} from 'prop-types';
import {useHistory} from 'react-router-dom';

import Icon from 'components/Icon';
import {Wrapper} from './styled';

const BackButton = ({onClick}) => {
  const history = useHistory();

  const onButtonClick = () => {
    if (typeof onClick === 'function') {
      onClick();
    } else {
      history.goBack();
    }
  };

  return (
    <Wrapper onClick={onButtonClick}>
      <Icon type="arrowBack" />
    </Wrapper>
  );
};

BackButton.propTypes = {
  onClick: func
};

export default BackButton;
