import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {ControlButton, PageCount} from './styled';
import Grid from 'components/Grid';
import Icons from 'components/Icon';

const Pagination = ({
  isMobile,
  currentPage = 1,
  totalPages = 10,
  nextPage = () => {},
  prevPage = () => {},
  ...props
}) => {
  const [total, setTotal] = useState(isNaN(totalPages) ? 1 : totalPages);

  useEffect(() => {
    if (!isNaN(totalPages)) {
      setTotal(totalPages);
    }
  }, [totalPages]);

  return (
    <Grid {...props} jcfe aic margin={isMobile ? '0 0' : '42px 0'}>
      <ControlButton onClick={prevPage} prev isMobile={isMobile}>
        <Icons type="arrow" />
        Previous
      </ControlButton>
      <PageCount isMobile={isMobile}>
        Page {currentPage}/{total}
      </PageCount>
      <ControlButton onClick={nextPage} next isMobile={isMobile}>
        Next
        <Icons type="arrow" />
      </ControlButton>
    </Grid>
  );
};

Pagination.defaultProps = {
  isMobile: false
};

Pagination.propTypes = {
  isMobile: PropTypes.bool
};

export default Pagination;
