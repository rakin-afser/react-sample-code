import styled from 'styled-components';
import media from '../../constants/media';

export const Controls = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #333333;
  background: transparent;
  border: none;
  outline: none;
  cursor: pointer;

  & svg {
    ${({prev}) => (prev ? 'transform: rotate(180deg)' : null)};
  }

  & svg path {
    fill: #333;
  }
`;

export const ControlButton = styled.button`
  border: 1px solid #e4e4e4;
  color: #545454;
  box-sizing: border-box;

  width: 100px;

  justify-content: space-between;
  font-size: 14px;
  line-height: 140%;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  display: flex;
  align-items: center;
  background: #fff;
  outline: none;
  padding: 2px 27px 1px 20px;
  ${({prev}) => (prev ? 'padding: 2px 16px 1px 8px' : null)};
  ${({isMobile}) => 'border-radius:' + (isMobile ? '30px' : '4px')};
  ${({isMobile}) => 'height:' + (isMobile ? '30px' : '40px')};

  @media (max-width: ${media.mobileMax}) {
    padding: 2px 21px 2px 25px ${({prev}) => (prev ? 'padding: 2px 12px 1px 8px' : null)};
  }

  &:hover {
    color: #000;
    border-color: #000;
    svg path {
      fill: #000;
    }
  }
  & svg {
    ${({prev, isMobile}) =>
      prev && !isMobile
        ? 'transform: rotate(180deg); margin-right: 5px;'
        : 'margin-left: 5px;'
        ? prev && isMobile
          ? 'transform: rotate(180deg); margin-right: 14px;'
          : 'margin-left: 14px;'
        : ''};
    ${({isMobile}) => 'height:' + (isMobile ? '12px' : 'auto')};
    ${({isMobile}) => 'width:' + (isMobile ? '7px' : 'auto')};
  }

  & svg path {
    fill: #e4e4e4;
    width: 12px;
    height: 12px;
    ${({next}) => (next ? 'fill: #000;' : null)};
    ${({prev}) => (prev ? 'fill: #545454;' : null)};
  }
  ${({next}) => (next ? 'border-color: #545454; color: #000;' : null)}
`;

export const PageCount = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  color: #666666;
  text-align: center;
  ${({isMobile}) => 'margin:' + (isMobile ? '0 38px' : '0 13px')};
`;
export const Dots = styled.span``;
