import React from 'react';
import Btn, {components} from 'components/Btn';
import styled from 'styled-components';

const Container = styled.div`
  border: 1px solid #999;
  margin: 10px;
  padding: 10px;
`;

const Wrap = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-auto-rows: auto;
  grid-gap: 10px;
  align-items: center;
  justify-content: flex-start;
`;

export default function() {
  return (
    <div style={{display: 'flex', flexWrap: 'wrap'}}>
      {Object.keys(components).map((btn) => (
        <Container key={btn}>
          <div>
            <strong>{btn}</strong>
          </div>
          <Wrap>
            <div>{`<Btn kind="${btn}"/>`}</div>
            <Btn kind={btn} />
            {components?.[btn]?.modes?.map((mode) => (
              <>
                <div>{`<Btn kind="${btn}" ${mode} />`}</div>
                <Btn kind={btn} {...{[mode]: 1}} />
              </>
            ))}
          </Wrap>
        </Container>
      ))}
    </div>
  );
}
