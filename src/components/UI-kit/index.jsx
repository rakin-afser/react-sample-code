import React from 'react';
import styled from 'styled-components';
import {Select} from 'antd';
import {useQuery} from '@apollo/client';

import Likes from 'components/Likes';
import Expanded from 'components/Expanded';
import Icons from './Icons';
import Buttons from './Buttons';
import Btn from './Btn';
import PopularTags from 'components/PopularTags';
import Switcher from 'components/Buttons/Switcher';
import SearchField from 'components/SearchField/Mobile';
import SelectField from 'components/SelectField';
import UserInfo from 'components/UserInfo/GuestView/Small';
import Avatar from 'components/Avatar';
import ShopInfo from 'components/ShopInfo/desktop/Small';
import Loader from 'components/Loader';
import Error from 'components/Error';
import ProductGridAsymmetrical from 'components/ProductGridAsymmetrical/mobile';
import NothingFound from 'components/NothingFound';
import ProductsCounter from 'components/ProductsCounter';

import {PRODUCTS} from 'queries';
import {country} from 'constants/staticData';
import image1 from 'images/productImage.png';

const {Option} = Select;

const tagsList = [
  '#Cream',
  '#canvas',
  '#Big',
  '#tags',
  '#Cream',
  '#canvas',
  '#Big',
  '#tags',
  '#Big',
  '#tags',
  '#Cream',
  '#canvas',
  '#Big',
  '#tags',
  '#Cream',
  '#canvas',
  '#Big',
  '#tags'
];

const Container = styled.div`
  padding: 60px 60px;
`;

const KitHeading = styled.h1`
  font-size: 36px;
  text-align: center;
`;

const HorizontalDivider = styled.div`
  border-bottom: 1px solid #000;
  margin: 50px 0;
`;

export default function UIkit() {
  const {data} = useQuery(PRODUCTS, {
    variables: {
      search: ''
    }
  });

  return (
    <Container>
      <KitHeading>UI kit</KitHeading>
      <HorizontalDivider />

      <h1>Buttons</h1>
      <Switcher />
      <Buttons />
      <Btn />
      <HorizontalDivider />

      <h1>Dropdowns</h1>
      <SelectField label="Country">
        {Object.keys(country).map((key) => (
          <Option value={key} key={key}>
            {key}
          </Option>
        ))}
      </SelectField>
      <HorizontalDivider />

      <h1>Search fields</h1>
      <h3>Mobile</h3>
      <SearchField placeholder="Search" />
      <HorizontalDivider />

      <h1>Icons</h1>
      <div className="icons">
        <Icons />
      </div>
      <HorizontalDivider />

      <h1>Likes</h1>
      <Likes likesCount={0} showCount />
      <HorizontalDivider />

      <h1>Products badge</h1>
      <div style={{width: 200, height: 200, background: 'gray', position: 'relative'}}>
        <ProductsCounter count={2} />
        <ProductsCounter count={123} bottom="20px" left="20px" top="unset" right="unset" />
      </div>

      <HorizontalDivider />

      <h1>Avatar</h1>
      <Avatar isOnline={false} img={image1} borderColor="chartreuse" />
      <Avatar isOnline img={image1} width={40} height={40} iconStyles={{width: 12, height: 12, bottom: 0, rigth: 0}} />
      <HorizontalDivider />

      <h1>Loader</h1>
      <Loader wrapperWidth="100%" wrapperHeight="100px" />
      <HorizontalDivider />

      <h1>Error</h1>
      <Error>Error: some error</Error>
      <HorizontalDivider />

      <h1>Nothing Found</h1>
      <NothingFound hideHelpText />
      <HorizontalDivider />

      <h1>Info</h1>
      <h3>User Small (guest view)</h3>
      <UserInfo src={image1} />
      <br />
      <h3>Shop Small (guest view)</h3>
      <ShopInfo src={image1} />
      <HorizontalDivider />

      <h1>Reusable smooth expansion Panel</h1>
      <Expanded
        contentVisible="visible content can by anything, component element or text"
        hideBtn="hide"
        showBtn="show"
      >
        hidden can be anything component, element or text content hidden content hidden content hidden content
      </Expanded>
      <HorizontalDivider />

      <h1>Popular tags</h1>
      <PopularTags data={tagsList} />
      <HorizontalDivider />

      <h1>Product grid asymmetrical (mobile)</h1>
      {data?.products?.nodes?.length ? <ProductGridAsymmetrical data={data?.products?.nodes} /> : 'Not found'}
      <HorizontalDivider />
    </Container>
  );
}
