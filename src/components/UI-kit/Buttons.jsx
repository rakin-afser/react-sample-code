import React from 'react';
import Button, {components} from '../Buttons';

export default function() {
  return (
    <div style={{display: 'flex', flexWrap: 'wrap'}}>
      {Object.keys(components).map((btn) => (
        <div key={btn} style={{padding: 10, textAlign: 'center'}}>
          <div>{btn}</div>
          <Button type={btn} />
        </div>
      ))}
    </div>
  );
}
