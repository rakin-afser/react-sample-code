import React from 'react';
import Icon from '../Icon';
import {icons} from 'components/Icon';

export default function() {
  return (
    <div style={{display: 'flex', flexWrap: 'wrap'}}>
      {Object.keys(icons).map((icon) => (
        <span
          style={{
            padding: 10,
            display: 'inline-flex',
            justifyContent: 'center',
            flexDirection: 'column',
            alignItems: 'center'
          }}
        >
          <span>{icon}</span>
          <Icon color="#000" fill="#000" type={icon} />
        </span>
      ))}
    </div>
  );
}
