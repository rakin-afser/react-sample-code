import React from 'react';
import {Wrap, Title, Photos, InstagramItem, Action, CallToAction, Button, InstagramLogo} from './styled.js';
import instagramPhotos from './exampleData.js';
import instagramLogo from './img/instagram_logo.svg';

const InstagramPhotos = () => {

  const getInstagramPhotos = (photos) => {
    return photos.map((photo, index) => <InstagramItem key={index} src={photo} alt='Instagram Fancy Photo'/>);
  }

  return (
    <Wrap>
      <Title>Instagram Photos</Title>
      <Photos>
        {getInstagramPhotos(instagramPhotos)}
      </Photos>
      <Action>
        <CallToAction>Share your Instagram posted as well</CallToAction>
        <Button>
          <InstagramLogo src={instagramLogo} alt='instagram icon'/>
          Connect
        </Button>
      </Action>
    </Wrap>
  )
}

export default InstagramPhotos;
