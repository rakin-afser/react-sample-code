import styled from 'styled-components';

export const Wrap = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 21px rgba(0, 0, 0, 0.06);
  border-radius: 4px;
  width: 100%;
  padding: 24px;
  margin-top: 32px;
`;

export const Title = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 132%;
  display: flex;
  align-items: center;
  letter-spacing: -0.024em;
  color: #464646;
  margin-bottom: 16px;
`;

export const Photos = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  align-items: flex-start;
`;

export const InstagramItem = styled.img`
  width: 82px;
  height: 82px;
  margin-bottom: 3px;
`;

export const Action = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 13px;
`;

export const CallToAction = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #999999;
  width: 50%;
`;

export const Button = styled.button`
  outline: none;
  border: none;
  cursor: pointer;
  box-shadow: none;
  background: #ed484f;
  border-radius: 4px;
  width: 98px;
  height: 36px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #ffffff;
  padding: 0;
  padding: 0 10px;
  padding-top: 3px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
`;

export const InstagramLogo = styled.img`
  margin-right: 10px;
  margin-bottom: 3px;
`;
