import styled from 'styled-components/macro';

export const SearchContainer = styled.div`
  position: relative;
  width: 100%;
  margin-right: 16px;
`;

export const SearchInput = styled.input`
  width: 100%;
  height: 42px;
  background: #efefef;
  border-radius: 4px;
  padding: 11px 44px 11px 55px;
  font-size: 14px;
  color: #000;
  border: 0;
  outline: none;
  box-shadow: none;
  caret-color: red;
  text-overflow: ellipsis;

  &:placeholder {
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    color: #666666;
  }
`;

export const SearchIcon = styled.span`
  position: absolute;
  left: 14px;
  top: 50%;
  transform: translate(0, -50%);
  max-width: 20px;
  max-height: 20px;
`;

export const SearchReset = styled.span`
  position: absolute;
  right: 16px;
  top: 50%;
  transform: translate(0, -50%);
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background: #cccccc;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;
