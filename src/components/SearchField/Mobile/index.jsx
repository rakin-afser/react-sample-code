import React, {useRef, useEffect} from 'react';

import Search from 'assets/Search';
import CloseIcon from 'assets/CloseIcon';

import {SearchContainer, SearchInput, SearchIcon, SearchReset} from 'components/SearchField/Mobile/styled';

const SearchFieldMobile = ({
  searchValue = '',
  placeholder = '',
  onChange = () => {},
  onReset = () => {},
  immediateFocus = false
}) => {
  const searchInput = useRef(null);

  useEffect(() => {
    if (immediateFocus) {
      searchInput.current.focus();
    }
  }, []);

  return (
    <SearchContainer>
      <SearchIcon>
        <Search color="#999" />
      </SearchIcon>
      <SearchInput placeholder={placeholder} value={searchValue} onChange={onChange} ref={searchInput} />
      {!!searchValue.length && (
        <SearchReset
          onClick={() => {
            onReset();
            searchInput.current.focus();
          }}
        >
          <CloseIcon width={12} height={12} color="#fff" />
        </SearchReset>
      )}
    </SearchContainer>
  );
};

export default SearchFieldMobile;
