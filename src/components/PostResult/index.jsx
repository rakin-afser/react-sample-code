import React from 'react';
import PropTypes from 'prop-types';

import Post from 'components/Cards/Post';
import {Grid} from './styled';

const Result = ({data}) => {
  const onClick = () => {};

  return (
    <Grid>
      {data?.map((item, i) => (
        <Post key={item?.id} onClick={() => onClick()} index={i} content={item} margin="0 0 40px 0" />
      ))}
    </Grid>
  );
};

Result.propTypes = {
  data: PropTypes.shape.isRequired
};

export default Result;
