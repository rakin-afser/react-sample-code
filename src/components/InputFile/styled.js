import styled from 'styled-components';

export const Label = styled.label`
  color: #666;
  background-color: transparent;
  border: 1px solid #666666;
  height: 28px;
  max-width: 122px;
  margin-left: 16px;
  font-size: 12px;
  border-radius: 30px;
  padding: 0 21px;
  display: inline-flex;
  align-items: center;

  input {
    display: none;
  }
`;
