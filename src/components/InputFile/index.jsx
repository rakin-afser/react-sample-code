import React from 'react';
import {Label} from './styled';

const InputFile = (props) => {
  const {children, ...rest} = props;
  return (
    <Label>
      {children}
      <input {...rest} type="file" />
    </Label>
  );
};

export default InputFile;
