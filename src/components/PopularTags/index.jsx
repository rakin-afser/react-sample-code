import React from 'react';
import {useQuery} from '@apollo/client';
import {useHistory} from 'react-router-dom';
import qs from 'qs';
import Loader from 'assets/Loader';
import Error from 'components/Error';
import {Tags, HashTag} from './styled';
import {GET_POPULAR_TAGS} from 'queries';

const PopularTags = ({filled = false, onClick}) => {
  const {data, loading, error} = useQuery(GET_POPULAR_TAGS, {
    variables: {where: {hideEmpty: true, order: 'DESC', orderby: 'COUNT'}, first: 10}
  });
  const {push} = useHistory();

  const tagClick = (tag) => {
    if (typeof onClick === 'function') {
      onClick();
    } else {
      const queryString = qs.stringify({
        filters: {hashtags: [tag]}
      });
      push(`/search/posts/all?${queryString}`);
    }
  };

  if (error) return <Error />;
  if (loading) return <Loader />;

  return (
    <Tags>
      {data?.tags?.nodes?.map((tag, key) => {
        if (key < 30) {
          return (
            <HashTag filled={filled} key={key} onClick={() => tagClick(`#${tag.name}`)}>
              #{tag.name}
            </HashTag>
          );
        }
        return null;
      })}
    </Tags>
  );
};

export default PopularTags;
