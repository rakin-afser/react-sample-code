import styled, {css} from 'styled-components';

export const Tags = styled.div`
  display: flex;
  margin-top: 16px;
  flex-wrap: wrap;
`;

export const HashTag = styled.button`
  outline: none;
  margin: 0 16px 16px 0;
  border: 1px solid #c3c3c3;
  box-sizing: border-box;
  border-radius: 4px;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  text-align: center;
  color: #545454;
  padding: 5px 20px;
  background: #fff;
  transition: ease 0.6s;

  &:hover {
    color: #000;
    border-color: #000;
  }

  ${({filled}) =>
    filled &&
    css`
      background: #f2f2f2;
      border-radius: 22px;
      border: none;
      padding: 5px 12px;
      font-family: SF Pro Display;
      font-weight: 600;
      font-size: 12px;
      color: #000000;
      margin: 0 10px 15px 0;

      &:hover {
      }
    `}
`;
