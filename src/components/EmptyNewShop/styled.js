import styled, {keyframes} from 'styled-components';

const loading = keyframes`
to {
  background-position: calc(100% + 100px) 50%;
}
`;

export const Card = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 280px;
  padding: 8px 10px;
  margin: 10px auto;
  background-color: silver;
  position: relative;

  &:after {
    display: block;
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(90deg, transparent 0, silver 50%, transparent 100%);
    background-size: 100px 100%;
    background-position: -100px 50%;
    background-repeat: no-repeat;
    z-index: 1;
    top: 0;
    left: 0;
    animation: ${loading} 1.5s infinite;
  }
`;

export const Avatar = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  background-color: white;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ShopName = styled.div`
  width: 120px;
  height: 15px;
  margin-bottom: 10px;
  background-color: white;
`;

export const Rating = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Stars = styled.div`
  width: 60px;
  height: 15px;
  background-color: white;
`;

export const Followers = styled.div`
  width: 40px;
  height: 15px;
  background-color: white;
`;

export const FollowButton = styled.div`
  width: 60px;
  height: 24px;
  border-radius: 16px;
  background-color: white;
`;
