import React from 'react';
import {Card, Avatar, Content, ShopName, Rating, Stars, Followers, FollowButton} from './styled';

const EmptyNewShop = () => (
  <Card>
    <Avatar />
    <Content>
      <ShopName />
      <Rating>
        <Stars />
        <Followers />
      </Rating>
    </Content>
    <FollowButton />
  </Card>
);

export default EmptyNewShop;
