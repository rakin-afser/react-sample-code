import styled from 'styled-components';
import {secondaryColor} from '../../../../constants/colors';

export const PostTagsWrap = styled.div`
  padding: 5px 16px 8px 16px;
`;
export const PostTag = styled.span`
  margin-top: 5px;
  margin-right: 9px;
  color: ${({color}) => (color === 'primary' ? secondaryColor : '#000')}
  font-weight: 500;
  font-size: 14px;
  line-height: 19.5px;
`;
