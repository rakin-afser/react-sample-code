import React from 'react';
import {PostTag, PostTagsWrap} from './styled';

const PostTagsMobile = ({tags, color}) => (
  <PostTagsWrap>
    {tags.map((tag) => (
      <PostTag color={color} key={tag}>
        #{tag}
      </PostTag>
    ))}
  </PostTagsWrap>
);

PostTagsMobile.defaultProps = {
  color: 'dark',
  title: 'Feedback'
};

export default PostTagsMobile;
