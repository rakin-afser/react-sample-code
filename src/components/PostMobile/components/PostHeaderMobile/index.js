import React from 'react';
import PropTypes from 'prop-types';
import {Header, HeaderAvatar, HeaderInfo, HeaderLeft, HeaderRight, HeaderTime, HeaderUserName} from './styled';
import FollowButton from 'components/FollowButton';
import ThreeDots from '../../../ThreeDots';

const PostHeaderMobile = ({borderBottom, photo, name, time, showFollowBtn}) => {
  return (
    <Header borderBottom={borderBottom}>
      <HeaderLeft>
        <HeaderAvatar photo={photo} />
        <HeaderInfo>
          <HeaderUserName>{name}</HeaderUserName>
          <HeaderTime>{time}</HeaderTime>
        </HeaderInfo>
      </HeaderLeft>
      <HeaderRight>
        {showFollowBtn && <FollowButton />}
        <ThreeDots />
      </HeaderRight>
    </Header>
  );
};

PostHeaderMobile.defaultProps = {
  photo: '',
  name: '',
  time: '',
  showFollowBtn: false,
  borderBottom: false
};

PostHeaderMobile.propTypes = {
  photo: PropTypes.string,
  name: PropTypes.string,
  time: PropTypes.string,
  showFollowBtn: PropTypes.bool,
  borderBottom: PropTypes.bool
};

export default PostHeaderMobile;
