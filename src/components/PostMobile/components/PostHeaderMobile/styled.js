import styled from 'styled-components';

export const Container = styled.div``;

export const Header = styled.div`
  padding: 16px 17px 7px 18px;
  border-top: 1px solid #f0f0f0;
  border-bottom: ${({borderBottom}) => (borderBottom ? '1px solid #f0f0f0' : '')}
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const HeaderInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-left: 8px;
`;

export const HeaderAvatar = styled.div`
  width: 50px;
  height: 50px;
  background-image: ${({photo}) => 'url(' + photo + ')'};
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
`;

export const HeaderLeft = styled.div`
  display: flex;
`;

export const HeaderRight = styled.div`
  display: flex;
  align-items: center;
  padding-bottom: 15px;

  & button {
    margin-right: 16.3px;
  }
`;

export const HeaderUserName = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 22px;
  color: #000;
`;

export const HeaderTime = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 16px;
  color: #7a7a7a;
`;
