import React, {useState} from 'react';
import StarRating from 'components/StarRating';
import {RatingBlockMobile, Wrap, Actions, Action} from './styled';
import ThumbUp from 'assets/ThumbUp';
import ThumbDown from 'assets/ThumbDown';

const PostRatingMobile = ({rating = 0, title = 'Feedback', initialState = {likes: 0, dislikes: 0}}) => {
  const [current, setCurrent] = useState(null);
  const {likes, dislikes} = initialState;

  function onLike() {
    setCurrent(current === 'like' ? null : 'like');
  }

  function onDislike() {
    setCurrent(current === 'dislike' ? null : 'dislike');
  }

  return (
    <RatingBlockMobile>
      <Wrap>
        <span>{title}</span>
        <StarRating stars={Number(rating)} starWidth="12px" starHeight="12px" />
      </Wrap>
      <Actions>
        <Action active={current === 'like'} onClick={onLike}>
          <ThumbUp fill={current === 'like' ? '#4A90E2' : '#999'} />
          {current === 'like' ? likes + 1 : likes}
        </Action>
        <Action active={current === 'dislike'} onClick={onDislike}>
          <ThumbDown fill={current === 'dislike' ? '#4A90E2' : '#999'} />
          {current === 'dislike' ? dislikes + 1 : dislikes}
        </Action>
      </Actions>
    </RatingBlockMobile>
  );
};

export default PostRatingMobile;
