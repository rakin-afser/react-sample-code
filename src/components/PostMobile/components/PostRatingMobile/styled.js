import {mainFont} from 'constants/fonts';
import styled, {css} from 'styled-components';

export const RatingBlockMobile = styled.div`
  display: flex;
  font-weight: normal;
  justify-content: space-between;
  color: #666666;
  font-size: 16px;
  font-weight: 500;
  padding-top: 16px;
  margin-top: 13px;
  padding-bottom: 5px;
  position: relative;

  &::before {
    content: '';
    border-top: 1px solid #e4e4e4;
    position: absolute;
    top: 0;
    left: -16px;
    right: -16px;
  }

  span {
    margin-right: 9px;
  }

  svg {
    margin: 0 1px 2px 0;
  }
`;

export const Wrap = styled.div`
  display: flex;
`;

export const Actions = styled.div`
  display: flex;
  align-items: center;
  font-family: ${mainFont};
  font-weight: 500;
  font-size: 16px;
  line-height: 1;

  > div + div {
    margin-left: 17px;
  }

  svg {
    margin-right: 5px;
  }
`;

export const Action = styled.div`
  display: flex;
  align-items: center;
  color: #999999;

  ${({active}) =>
    active &&
    css`
      color: #4a90e2;
    `}
`;
