import React from 'react';
import {
  ProductBox,
  ProductPhoto,
  ProductLeft,
  ProductTitle,
  ProductDelivery,
  ProductInfo,
  ProductPrice,
  Currency
} from './styled';
import ShoppingCart from '../../../../assets/ShoppingCart';

const PostProductBoxMobile = ({photo, name, delivery, price}) => (
  <ProductBox>
    <ProductLeft>
      <ProductPhoto image={photo} />
      <ProductInfo>
        <ProductTitle>{name}</ProductTitle>
        <ProductDelivery>{delivery}</ProductDelivery>
      </ProductInfo>
    </ProductLeft>
    <ProductPrice>
      <ShoppingCart width={18} height={18} color="#666" />
      <Currency>BD</Currency>
      <span>{price}</span>
    </ProductPrice>
  </ProductBox>
);

export default PostProductBoxMobile;
