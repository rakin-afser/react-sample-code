import styled from 'styled-components';
export const ProductBox = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #efefef;
  border-radius: 4px;
  padding: 4px 18px 4px 4px;
  margin: 0 16px 10px 16px;
`;

export const ProductLeft = styled.div`
  display: flex;
  align-items: center;
`;

export const ProductPhoto = styled.div`
  background-image: url('${({image}) => image}');
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
  width: 52px;
  height: 52px;
`;

export const ProductInfo = styled.div`
  margin-left: 17px;
`;

export const ProductTitle = styled.div`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 19.6px;
  color: #000000;
`;

export const ProductDelivery = styled.div`
  margin-top: 4px;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 14.3px;
  color: #666;
`;

export const ProductPrice = styled.div`
  height: 28px;
  padding: 0 6px;
  border: 0.5px solid #cccccc;
  border-radius: 24px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 1.4px;
  display: flex;
  align-items: center;

  & span {
    font-weight: 500;
    font-size: 14px;
    margin-left: 1px;
  }

  & svg {
    margin-right: 6px;
  }
`;

export const Currency = styled.div`
  position: relative;
  bottom: -1px;
`;
