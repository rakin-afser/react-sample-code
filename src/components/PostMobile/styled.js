import styled from 'styled-components';
export const Container = styled.div``;

export const PostTitle = styled.div`
  font-family: Helvetica, sans-serif;
  font-weight: 500;
  font-size: 16px;
  line-height: 22.4px;
  color: #000;
  padding: 13px 16px 0 16px;
  letter-spacing: 0.2px;
`;

export const PostMessage = styled.div`
  margin-top: 5px;
  padding: 0 40px 0 16px;
  color: #000;
  font-size: 14px;
  line-height: 20px;
`;

export const SliderWrap = styled.div`
  position: relative;
`;
