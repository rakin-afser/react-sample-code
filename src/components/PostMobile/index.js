import React, {useState} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import PostHeaderMobile from './components/PostHeaderMobile';
import {Container, PostTitle, PostMessage, SliderWrap} from './styled';
import PostRatingMobile from './components/PostRatingMobile';
import PostTagsMobile from './components/PostTagsMobile';
import PackageLabel from '../PackageLabel';
import MainSlider from '../ProductPage/mobile/MainSlider';
import PostProductBoxMobile from './components/PostProductBoxMobile';
import CommentMobile from '../../components/CommentMobile/index';
import Counters from '../ProductPage/mobile/Сounters';

const PostMobile = ({
  avatar,
  name,
  title,
  message,
  time,
  rating,
  showFollowBtn,
  tags,
  sliderCount,
  slides,
  productPhoto,
  productName,
  productDelivery,
  productPrice,
  comments,
  likes,
  shared,
  bookmarks,
  headerBorderBottom
}) => {
  const [showCommentsPopup, setShowCommentsPopup] = useState(false);
  const handleClickShowAll = () => {
    setShowCommentsPopup(true);
  };
  return (
    <Container>
      <PostHeaderMobile
        borderBottom={headerBorderBottom}
        photo={avatar}
        name={name}
        time={time}
        showFollowBtn={showFollowBtn}
      />
      {rating && <PostRatingMobile rating={rating} />}
      {title && <PostTitle>{title}</PostTitle>}
      {message && <PostMessage>{message}</PostMessage>}
      {tags && tags.length > 0 && <PostTagsMobile color="primary" tags={tags} />}
      {slides && (
        <SliderWrap>
          <MainSlider slides={slides} rightTopComponent={<PackageLabel count={sliderCount} />} />
        </SliderWrap>
      )}
      {productName && (
        <PostProductBoxMobile photo={productPhoto} name={productName} delivery={productDelivery} price={productPrice} />
      )}
      <Counters
        likes={likes}
        comments={comments ? comments.length : 0}
        share={shared}
        bookmark={bookmarks}
        style={{
          borderTop: '1px solid #E4E4E4'
        }}
        showCommentsPopup={showCommentsPopup}
        setShowCommentsPopup={setShowCommentsPopup}
      />
      {comments && (
        <CommentMobile
          name={comments[0].name}
          message={comments[0].message}
          replies={comments[0].replies}
          time={comments[0].time}
          showAllCommentsBtn={comments.length > 0}
          likes={comments[0].likes}
          onClickShowAll={handleClickShowAll}
        />
      )}
      <Counters showCommentsPopup={showCommentsPopup} setShowCommentsPopup={setShowCommentsPopup} />
    </Container>
  );
};

PostMobile.propTypes = {
  avatar: PropTypes.string,
  name: PropTypes.string,
  title: PropTypes.string,
  message: PropTypes.string,
  time: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.instanceOf(moment), PropTypes.string]),
  rating: PropTypes.number,
  showFollowBtn: PropTypes.bool,
  tags: PropTypes.array,
  sliderCount: PropTypes.number,
  slides: PropTypes.array,
  productPhoto: PropTypes.string,
  productName: PropTypes.string,
  productDelivery: PropTypes.string,
  productPrice: PropTypes.string,
  comments: PropTypes.array,
  likes: PropTypes.number,
  shared: PropTypes.number,
  bookmarks: PropTypes.number,
  headerBorderBottom: PropTypes.bool
};

export default PostMobile;
