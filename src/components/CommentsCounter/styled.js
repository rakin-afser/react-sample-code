import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const CommentsWrapper = styled.div`
  display: flex;
  align-items: center;

  svg {
    cursor: pointer;
    path {
      transition: ease 0.4s;
      fill: ${({isActive}) => (isActive ? primaryColor : '#8F8F8F')};
    }
  }
`;

export const Count = styled.span`
  margin-left: 5.5px;
  margin-top: 1px;
`;
