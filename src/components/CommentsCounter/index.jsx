import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';

import {useUser} from 'hooks/reactiveVars';

import CommentsIcon from 'assets/Comments';
import {CommentsWrapper, Count} from 'components/CommentsCounter/styled';

const CommentsCounter = ({commentsCount = 0, showCount = true, onClick = () => {}, isActive = false}) => {
  const [user] = useUser();
  const history = useHistory();

  const handleClick = () => {
    if (!user?.databaseId) {
      history.push('/auth/sign-in');
      return;
    }
    
    if (onClick) onClick();
  };

  return (
    <CommentsWrapper isActive={isActive} onClick={handleClick}>
      <CommentsIcon fill={'#8F8F8F'} />
      {showCount && commentsCount ? <Count>{commentsCount}</Count> : null}
    </CommentsWrapper>
  );
};

export default CommentsCounter;
