import React, {useState} from 'react';
import {createStyles, makeStyles, withStyles} from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputBase from '@material-ui/core/InputBase';
import MenuItem from '@material-ui/core/MenuItem';
import useGlobal from 'store';
import i18n from 'i18n';

const CustomInput = withStyles((theme) =>
  createStyles({
    input: {
      color: '#000',
      fontSize: '14px',
      borderRadius: 10,
      position: 'relative',
      backgroundColor: 'transparent',
      border: 'none',
      width: '22px',
      padding: '4px 6px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: 'sans-serif',
      '&:focus': {
        borderColor: theme.palette.primary.main,
        borderRadius: 10,
        backgroundColor: 'transparent'
      }
    }
  })
)(InputBase);

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  formControl: {
    zIndex: 1,
    border: 'none',
    minWidth: 30,
    padding: 0
  },
  selectEmpty: {
    border: '1px solid lightgrey',
    borderRadius: 10,
    marginTop: 45,
    minWidth: '195px !important',
    boxShadow: 'none'
  },
  menuItem: {
    fontSize: '14px',
    display: 'flex',
    padding: '5px',
    borderBottom: '1px solid lightgrey',
    background: 'transparent',
    transition: '.2s ease',
    width: '95%',
    margin: '0 auto',
    '&:hover': {
      borderColor: 'transparent'
    }
  },
  icon: {
    fill: '#000'
  }
}));

const SelectLanguage = () => {
  const [, globalStateAction] = useGlobal();
  const classes = useStyles();
  const [state, setState] = useState({
    value: 'English',
    name: ''
  });

  i18n.on('languageChanged', (lng) => {
    globalStateAction.setArabic(lng === 'Arabic');
  });

  const handleChange = (event) => {
    const {name} = event.target;
    const {value} = event.target;
    setState({
      ...state,
      [name]: event.target.value
    });
    i18n.changeLanguage(!value ? 'en' : value);
  };

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <Select
        classes={{
          icon: classes.icon
        }}
        value={state.value}
        onChange={handleChange}
        inputProps={{
          name: 'value',
          MenuProps: {disableScrollLock: true}
        }}
        renderValue={(value) => `${value}`}
        input={<CustomInput />}
        MenuProps={{classes: {paper: classes.selectEmpty}}}
      >
        <MenuItem className={classes.menuItem} value="English">
          {i18n.t('languages.en')}
        </MenuItem>
        <MenuItem className={classes.menuItem} value="Arabic">
          {i18n.t('languages.ar')}
        </MenuItem>
      </Select>
    </FormControl>
  );
};

export default SelectLanguage;
