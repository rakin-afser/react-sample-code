import styled from 'styled-components';
import {Button, Input} from 'antd';

export const Wrap = styled.div`
  margin-bottom: 16px;

  label {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 140%;
    color: #464646;
    margin-bottom: 6px;
    display: block;
  }
`;

export const Wrapper = styled.div`
  display: flex;
`;

export const MobileCode = styled.span`
  border-radius: 4px;
  background-color: #c3c3c3;
  padding: 4px;
  color: #fff;
  min-width: 33px;
  white-space: nowrap;
`;

export const InputField = styled(Input)`
  &.ant-input-affix-wrapper {
    display: flex;
    border: 1px solid #a7a7a7;
    border-radius: 2px;
    padding: 4.5px 12px;
    transition: border-color 0.3s ease-in;

    &:hover {
      border-color: #4a90e2;
    }
  }

  &&& {
    .ant-input-prefix {
      position: static;
      transform: none;
    }

    .ant-input {
      height: 26px;
      &:focus {
        box-shadow: none;
      }

      &:not(:first-child) {
        padding: 0 0 0 5px;
      }

      border: none;
    }
  }
`;

export const Verify = styled(Button)`
  &&& {
    background-color: transparent;
    color: #545454;
    border: 1px solid #c3c3c3;
    font-size: 14px;
    font-weight: 400;
    flex: 0 0 94px;
    margin-left: 24px;
    height: auto;
  }
`;
