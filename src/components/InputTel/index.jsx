import React from 'react';
import {country as countries} from 'constants/staticData';
import {Wrap, InputField, MobileCode, Verify, Wrapper} from './styled';

const InputTel = (props) => {
  const {country, onChange, label, ...rest} = props;

  const onChangeTel = (e) => {
    const {value} = e.target;
    const reg = /^-?\d*(\.\d*)?$/;
    if ((!isNaN(value) && reg.test(value)) || value === '' || value === '-') {
      props.onChange(value);
    }
  };

  const prefixModilePhone = <MobileCode>{countries[country] ? countries[country].dialCode : '+'}</MobileCode>;

  return (
    <Wrap>
      {label ? <label>{label}</label> : null}
      <Wrapper>
        <InputField onChange={(e) => onChangeTel(e)} maxLength={20} {...rest} prefix={prefixModilePhone} />
        <Verify>Verify</Verify>
      </Wrapper>
    </Wrap>
  );
};

export default InputTel;
