import styled, {css} from 'styled-components';
import {secondaryTextColor, primaryColor} from 'constants/colors';

export const AvatarWrap = styled.div`
  position: relative;
  flex-shrink: 0;
  width: ${({width}) => width}px;
  height: ${({height}) => height}px;

  ${(p) =>
    p.isMobile &&
    css`
      margin-right: ${p.theme?.isArabic ? '0' : '16'}px;
      margin-left: ${p.theme?.isArabic ? '16' : '0'}px;
      flex-shrink: 0;
    `}
`;

export const AvatarPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  border: ${({borderColor}) => (!!borderColor ? `1px solid ${borderColor}` : 'none')};
  border-radius: 50%;
  object-fit: cover;
`;

export const EditAvatar = styled.i`
  position: absolute;
  bottom: -3px;
  right: 2px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 33px;
  height: 33px;
  background: #c3c3c3;
  border-radius: 50%;
  border: 3px solid #ffffff;
  transition: ease 0.3s;

  &:hover {
    cursor: pointer;
    background: ${secondaryTextColor};
  }

  ${(p) =>
    p.isMobile &&
    `
    bottom: 1px;
    right: 4px;
    background: ${primaryColor};
    width: 22px;
    height: 22px;
    border: 2px solid #ffffff;

    & svg {
      width: 9px;
    }
  `}
`;

export const OnlineIcon = styled.span`
  width: 16px;
  height: 16px;
  background-color: #2ecc71;
  border: 2px solid #fff;
  border-radius: 50%;
  position: absolute;
  bottom: 3px;
  right: -2px;

  ${({isOnline}) =>
    !isOnline &&
    css`
      display: none;
    `}
  ${({styles}) => styles || null};
`;
