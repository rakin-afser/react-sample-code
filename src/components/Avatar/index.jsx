import React, {useState} from 'react';
import PropTypes from 'prop-types';

import {AvatarPic, AvatarWrap, EditAvatar, OnlineIcon} from './styled';
import Icon from 'components/Icon';
import useGlobal from 'store';
import userPlaceholder from 'images/placeholders/user.jpg';

const Avatar = ({
  img,
  setEdit = () => {},
  isMobile = false,
  width = isMobile ? 90 : 120,
  height = isMobile ? 90 : 120,
  isOnline = false,
  borderColor = '',
  iconStyles,
  onClick = () => {}
}) => {
  const [error, setError] = useState(false);
  const [global] = useGlobal();
  const {user} = global;

  return (
    <AvatarWrap onClick={onClick} isMobile={isMobile} width={width} height={height}>
      <AvatarPic
        src={error ? userPlaceholder : img}
        alt="user avatar"
        borderColor={borderColor}
        onError={() => setError(true)}
      />
      {user && setEdit ? (
        <EditAvatar isMobile={isMobile} onClick={() => setEdit(true)}>
          <Icon type="pencil" />
        </EditAvatar>
      ) : null}
      <OnlineIcon className="avatar-online-icon" isOnline={isOnline} styles={iconStyles} />
    </AvatarWrap>
  );
};

Avatar.propTypes = {
  img: PropTypes.node.isRequired,
  setEdit: PropTypes.func
};

export default Avatar;
