import styled from 'styled-components/macro';

export const NothingFoundContainer = styled.div`
  margin: auto;
  text-align: center;
  max-width: 80%;
  padding-top: 20px;
`;

export const NothingFoundTitle = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  text-align: center;
  color: #000000;
  margin: 40px 0 15px;
`;

export const NothingFoundText = styled.p`
  margin: 0;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: #999999;
  margin-bottom: 30px;
`;
