import React from 'react';
import {Link} from 'react-router-dom';

import {NothingFoundContainer, NothingFoundTitle, NothingFoundText} from 'components/NothingFound/styled';
import SearchNotFound from 'assets/SearchNoFound';

const NothingFound = ({hideAdditionalText = false, hideHelpText = false}) => {
  return (
    <NothingFoundContainer>
      <SearchNotFound />
      <NothingFoundTitle>No Results Found</NothingFoundTitle>
      {!hideAdditionalText && <NothingFoundText>Try checking your spelling or use more general terms</NothingFoundText>}
      {!hideHelpText && (
        <NothingFoundText>
          <p>Need help?</p>
          <div>
            Visit the <Link to="/about/faqs">FAQ</Link> section or <Link to="/about/contact-us">Contact Us</Link>
          </div>
        </NothingFoundText>
      )}
    </NothingFoundContainer>
  );
};

export default NothingFound;
