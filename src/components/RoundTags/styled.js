import styled from 'styled-components';

export const List = styled.ul`
  margin: 0 ${({wrap}) => (wrap ? '-8px' : '0')} 16px;
  display: flex;
  flex-wrap: ${({wrap}) => (wrap ? 'wrap' : 'nowrap')};
  ${({wrap}) =>
    !wrap &&
    `
    overflow: auto
    -ms-overflow-style: none;  /* IE and Edge */
    scrollbar-width: none;  /* Firefox */

    &::-webkit-scrollbar {
      display: none;
    }
  `}
`;

export const Item = styled.li`
  ${({wrap}) =>
    wrap
      ? `
    padding: 8px;
    `
      : `
    padding-top: 8px;
    padding-bottom: 8px;
    padding-left: 16px;

    &:last-child {
      padding-right: 16px;
    }
  `}
`;

export const Tag = styled.span`
  display: inline-flex;
  align-items: center;
  height: 32px;
  padding-right: 5px;
  padding-left: 13px;
  border: 1px solid #e4e4e4;
  border-radius: 60px;
  color: #464646;
  font-size: 12px;
  white-space: nowrap;

  i {
    padding: 8px;
  }
`;
