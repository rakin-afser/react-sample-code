import React, {useEffect} from 'react';
import {Transition, TransitionGroup} from 'react-transition-group';
import {Tag, Item, List} from './styled';
import Icon from 'components/Icon';

const RoundTags = (props) => {
  const {setQuantity, onFollowInterest, tags} = props;
  const duration = 300;

  const transitionStyles = {
    entering: {
      opacity: 1,
      marginRight: 0,
      transition: `opacity ${duration}ms ease-in-out, margin ${duration}ms ease-in-out`
    },
    entered: {
      opacity: '',
      marginTop: '',
      marginRight: ''
    },
    exiting: {
      opacity: 0,
      transition: `opacity ${duration}ms ease-in-out, margin ${duration}ms ease-in-out`
    },
    exited: {
      opacity: '',
      marginTop: ''
    }
  };

  useEffect(() => {
    if (setQuantity) {
      setQuantity(tags.length);
    }
  }, [tags, setQuantity]);

  function onEnter(node) {
    // node.style.marginLeft = `-${node.clientWidth}px`;
    // node.style.marginTop = `-${node.clientHeight / 2}px`;
    // node.style.marginBottom = `-${node.clientHeight / 2}px`;
    node.style.opacity = 0;

    return node.offsetHeight;
  }

  function onExiting(node) {
    node.style.marginTop = `-${node.clientHeight / 2}px`;
    // node.style.marginBottom = `-${node.clientHeight / 2}px`;
    node.style.marginLeft = `-${node.clientWidth}px`;
  }

  return (
    <List wrap={props.wrap}>
      <TransitionGroup component={null}>
        {tags?.map((tag) => (
          <Transition key={tag.id} timeout={duration} unmountOnExit onEnter={onEnter} onExiting={onExiting}>
            {(status) => (
              <Item wrap={props.wrap} style={{...transitionStyles[status]}}>
                <Tag>
                  {tag.title}
                  <Icon onClick={() => onFollowInterest(tag.databaseId)} type="cross" fill="currentColor" />
                </Tag>
              </Item>
            )}
          </Transition>
        ))}
      </TransitionGroup>
    </List>
  );
};

export default RoundTags;
