import React from 'react';
import 'emoji-mart/css/emoji-mart.css';
import {Picker} from 'emoji-mart';
import PropTypes from 'prop-types';

const Emoji = (props) => {
  return (
    <>
      <Picker
        title="Pick your emoji"
        onSelect={() => {}}
        set="apple"
        style={{position: 'absolute', bottom: '20px', right: '20px'}}
      />
    </>
  );
};

Emoji.propTypes = {};

export default Emoji;
