import {ReactComponent as CloseIcon} from 'images/svg/auth-popup-close.svg';
import React from 'react';
import {useHistory} from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';
import useGlobal from 'store';
import {CloseButton, Popup, PopupContent, PopupOverlay, Signin, Signup, Title} from './styled';

const AuthRequest = () => {
  const [{isRequireAuth}, {setIsRequireAuth, setAuthPopup}] = useGlobal();
  const {push} = useHistory();

  const handleClickSignUp = () => {
    setAuthPopup(true);
    setIsRequireAuth(false);
    push('/auth/sign-up');
  };

  const handleClickSignIn = () => {
    setAuthPopup(true);
    setIsRequireAuth(false);
    push('/auth/sign-in');
  };

  return (
    <CSSTransition in={isRequireAuth} timeout={300} unmountOnExit>
      <Popup show={isRequireAuth}>
        <PopupOverlay onClick={() => setIsRequireAuth(false)} show={isRequireAuth} />
        <PopupContent show={isRequireAuth}>
          <CloseButton onClick={() => setIsRequireAuth(false)}>
            <CloseIcon />
          </CloseButton>
          <Title>
            Please <span>Sign in</span> or <span>Create account</span> to continue
          </Title>
          <Signin className="ant-btn ant-btn-primary ant-btn-round" onClick={handleClickSignIn}>
            Sign In
          </Signin>
          <Signup className="ant-btn ant-btn-round" onClick={handleClickSignUp}>
            Create Account
          </Signup>
        </PopupContent>
      </Popup>
    </CSSTransition>
  );
};

export default AuthRequest;
