import styled from 'styled-components';

export const PopupContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: auto 10px 10px 10px;
  background: #ffffff;
  border-radius: 12px;
  padding: 15px 25px 32px;
  width: calc(100% - 20px);
  position: relative;
  z-index: 2;
  position: absolute;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const Popup = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100vw;
  z-index: 10000;
  display: flex;
  flex-wrap: wrap;

  &.enter {
    opacity: 0;

    ${PopupContent} {
      transform: translateY(100%);
    }
  }
  &.enter-active {
    opacity: 1;
    transition: opacity 300ms ease;

    ${PopupContent} {
      transform: translateY(0);
      transition: transform 300ms ease;
    }
  }
  &.exit {
    opacity: 1;

    ${PopupContent} {
      transform: translateY(0);
    }
  }
  &.exit-active {
    opacity: 0;
    transition: opacity 300ms ease;

    ${PopupContent} {
      transform: translateY(100%);
      transition: transform 300ms ease;
    }
  }
`;

export const PopupOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0, 0, 0, 0.5);
`;

export const Title = styled.p`
  color: #c4c4c4;
  margin-bottom: 32px;
  max-width: 213px;
  margin: 0 auto 25px;
  text-align: center;
  font-weight: 500;

  span {
    color: #000;
  }
`;

export const Signin = styled.div`
  background-color: #ed484f !important;
  border-color: #ed484f !important;
  max-width: 186px;
  width: 100%;
  height: 40px !important;
  display: inline-flex !important;
  align-items: center;
  justify-content: center;
`;

export const Signup = styled.div`
  border-color: #999 !important;
  color: #666 !important;
  max-width: 186px;
  width: 100%;
  margin-top: 13px;
  height: 40px !important;
  display: inline-flex !important;
  align-items: center;
  justify-content: center;
`;

export const CloseButton = styled.button`
  position: absolute;
  top: 17px;
  left: 17px;
  padding: 0;
`;
