import React, {useState} from 'react';
import Post from 'components/Post';
import postData from 'components/Post/examplePostDataMobile';
import myProfileExampleData from 'components/Post/myProfileExampleData';
import Stores from 'components/Stores';
import PopCategories from 'components/PopCategories';
import {PostColumnMobile} from './styled';
import Loader from 'components/Loader';

const PostsContent = ({data, isUserLogeIn, stores, endRefEl, loading}) => {
  const [visible, setVisible] = useState([]);
  const active = visible?.length === 0 ? null : Math.min(...visible);

  const renderPosts = (posts) => {
    return posts?.map((post, i) => (
      <Post
        active={i === active}
        visible={visible}
        setVisible={setVisible}
        order={i}
        key={post?.id}
        data={post}
        isUserLogeIn={isUserLogeIn}
      />
    ));
  };

  const last = data?.length + 1;
  const penultimate = data?.length - 3;

  return (
    <PostColumnMobile>
      {data?.length ? <Stores order={3} stores={stores} /> : null}
      {renderPosts(data)}
      {loading ? <Loader order={last} wrapperHeight="auto" wrapperWidth="100%" /> : null}
      {!loading && endRefEl ? {...endRefEl, props: {order: penultimate}} : null}
      {/* <FeedFooter order={77} /> */}
      {/* <PopCategories /> */}
    </PostColumnMobile>
  );
};

export default PostsContent;
