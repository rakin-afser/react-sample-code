import styled from 'styled-components';

export const PostColumnMobile = styled.div`
  width: 100%;
  background: #fff;
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
