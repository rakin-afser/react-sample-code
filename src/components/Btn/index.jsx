import React from 'react';
import {
  AddNew,
  Follow,
  Following,
  SeeMore,
  Primary,
  Continue,
  ContinueBig,
  PrimaryBig,
  PrimaryBold,
  PrimarySmall,
  PrimaryBare,
  FollowSm,
  FollowingSm,
  FollowingMd,
  FollowMd,
  GoogleAuth,
  FacebookAuth,
  BackLg,
  PostType,
  BackBorder,
  Subscribe,
  PrimaryBareSm,
  ContinueSm,
  Sell,
  Multisell,
  FollowText,
  FollowingText,
  Message
} from './styled';
import Icon from 'components/Icon';
import {primaryColor} from 'constants/colors';

export const components = {
  primary: {
    component: Primary,
    title: 'Primary',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      color: '#ED484F'
    },
    modes: ['loading', 'disabled']
  },
  'primary-big': {
    component: PrimaryBig,
    title: 'Primary big',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      color: '#ED484F'
    },
    modes: ['loading', 'disabled']
  },
  'primary-bold': {
    component: PrimaryBold,
    title: 'Primary bold',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      color: '#ED484F'
    },
    modes: ['loading', 'disabled']
  },
  'primary-small': {
    component: PrimarySmall,
    title: 'Primary small',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      color: '#ED484F'
    },
    modes: ['loading', 'disabled']
  },
  seeMore: {
    component: SeeMore,
    before: {
      type: 'arrow'
    }
  },
  follow: {
    component: Follow,
    title: 'Follow',
    loader: {
      type: 'loader',
      width: 50,
      height: 15
    },
    modes: ['loading']
  },
  following: {
    component: Following,
    title: 'Following',
    loader: {
      type: 'loader',
      width: 50,
      height: 15
    },
    modes: ['loading']
  },
  'follow-md': {
    component: FollowMd,
    title: 'Follow',
    loader: {
      type: 'loader',
      width: 42,
      height: 12
    },
    modes: ['loading']
  },
  'following-md': {
    component: FollowingMd,
    title: 'Following',
    loader: {
      type: 'loader',
      width: 42,
      height: 12,
      fill: '#fff'
    },
    modes: ['loading']
  },
  'follow-md-iconless': {
    component: FollowMd,
    title: 'Follow',
    loader: {
      type: 'loader',
      width: 42,
      height: 12
    },
    modes: ['loading']
  },
  'follow-sm': {
    component: FollowSm,
    title: 'Follow',
    loader: {
      type: 'loader',
      width: 40,
      height: 10
    },
    modes: ['loading']
  },
  'following-sm': {
    component: FollowingSm,
    title: 'Following',
    loader: {
      type: 'loader',
      width: 40,
      height: 10
    },
    modes: ['loading']
  },
  'follow-text': {
    component: FollowText,
    title: 'Follow',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: primaryColor
    },
    modes: ['loading']
  },
  'following-text': {
    component: FollowingText,
    title: 'Following',
    before: {
      type: 'checkbox'
    },
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: primaryColor
    },
    modes: ['loading']
  },
  addNew: {
    component: AddNew,
    title: 'Add New',
    props: {
      before: 1
    },
    before: {
      type: 'plus',
      width: 14,
      height: 14
    }
  },
  continue: {
    component: Continue,
    title: 'Save & Continue',
    after: {
      type: 'chevronRight',
      width: 5,
      height: 8,
      fill: 'currentColor'
    },
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: '#fff'
    },
    modes: ['loading', 'disabled']
  },
  'continue-big': {
    component: ContinueBig,
    title: 'Save & Continue',
    after: {
      type: 'chevronRight',
      width: 7,
      height: 12,
      fill: 'currentColor'
    },
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: '#fff'
    },
    modes: ['loading', 'disabled']
  },
  bare: {
    component: ContinueBig,
    title: 'Bare',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: '#fff'
    },
    modes: ['loading', 'disabled']
  },
  'bare-sm': {
    component: ContinueSm,
    title: 'Bare',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: '#fff'
    },
    modes: ['loading', 'disabled']
  },
  'pr-bare': {
    component: PrimaryBare,
    title: 'Primary Bare',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: '#fff'
    },
    modes: ['loading', 'disabled']
  },
  'pr-bare-sm': {
    component: PrimaryBareSm,
    title: 'Primary Bare',
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      fill: '#fff'
    },
    modes: ['loading', 'disabled']
  },
  googleAuth: {
    component: GoogleAuth,
    before: {
      type: 'google'
    },
    title: (
      <span>
        Continue with <strong>Google</strong>
      </span>
    )
  },
  facebookAuth: {
    component: FacebookAuth,
    before: {
      type: 'facebookRound'
    },
    title: (
      <span>
        Continue with <strong>Facebook</strong>
      </span>
    )
  },
  'back-lg': {
    component: BackLg,
    before: {
      type: 'chevronLeft'
    },
    title: 'Back'
  },
  camera: {
    component: PostType,
    before: {
      type: 'camera'
    },
    title: 'Post',
    modes: ['active', 'disabled']
  },
  flash: {
    component: PostType,
    before: {
      type: 'playCircle'
    },
    title: 'Flash',
    modes: ['active', 'disabled']
  },
  'back-border': {
    component: BackBorder,
    modes: ['active', 'disabled']
  },
  subscribe: {
    component: Subscribe,
    title: 'Subscribe',
    after: {
      type: 'chevronRight',
      width: 7,
      height: 12
    },
    loader: {
      type: 'loader',
      width: 50,
      height: 15,
      color: '#ED484F'
    },
    modes: ['loading', 'disabled']
  },
  sell: {
    component: Sell,
    title: 'USD 10.00',
    before: {
      type: 'cart',
      width: 15,
      height: 15
    },
    loader: {
      type: 'loader',
      width: 42,
      height: 12
    },
    modes: ['loading', 'disabled']
  },
  multisell: {
    component: Multisell,
    title: 'USD 10.00',
    before: {
      type: 'cart',
      width: 15,
      height: 15
    },
    loader: {
      type: 'loader',
      width: 42,
      height: 12
    },
    modes: ['loading', 'disabled']
  },
  'msg-square': {
    component: Message,
    title: <Icon type="msg-square" />
  }
};

const Btn = (props) => {
  const {kind, children, loading, ...rest} = props;
  if (!kind || !components?.[kind]) {
    return children;
  }

  const Component = components[kind].component;
  const chldrn = children || components[kind].title || null;
  const before = components[kind].before || null;
  const after = components[kind].after || null;
  const loader = components[kind].loader || null;
  const properties = components[kind].props || null;

  return (
    <Component {...properties} {...rest} loading={loading ? 1 : 0}>
      {before ? <Icon {...before} /> : null}
      {loading && loader ? <Icon {...loader} className="loader" /> : null}
      {chldrn || null}
      {after ? <Icon {...after} /> : null}
    </Component>
  );
};

export default Btn;
