import {Button} from 'antd';
import {primaryColor} from 'constants/colors';
import media from 'constants/media';
import styled, {css} from 'styled-components';

const Parent = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  border: none;
  outline: none;
  text-decoration: none;
  font-family: 'Helvetica Neue', sans-serif;
  cursor: pointer;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;

  ${({mt}) =>
    mt
      ? css`
          margin-top: ${mt};
        `
      : null}
  ${({mb}) =>
    mb
      ? css`
          margin-bottom: ${mb};
        `
      : null}
  ${({mr}) =>
    mr
      ? css`
          margin-right: ${mr};
        `
      : null}
  ${({ml}) =>
    ml
      ? css`
          margin-left: ${ml};
        `
      : null}

  ${({loading}) =>
    loading
      ? css`
          pointer-events: none;
        `
      : null}

  ${({full}) =>
    full
      ? css`
          width: 100% !important;
        `
      : null}

`;

export const SeeMore = styled(Parent)`
  position: relative;
  width: 47px;
  height: 47px;
  border-radius: 50%;
  border: 3px solid #ed484f;
  font-family: Helvetica, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  text-align: center;
  color: #000000;
  margin: ${({withText}) => (withText ? '0 0 24px 0' : '0')};

  &::before {
    content: ${({withText}) => (withText ? `'See more'` : null)};
    position: absolute;
    top: calc(100% + 10px);
    left: 50%;
    transform: translateX(-50%);
    white-space: nowrap;
  }
`;

export const AddNew = styled(Button)`
  &&& {
    width: 100%;
    border: 1px solid #efefef;
    background: #ffffff;
    height: 72px;
    color: #000;
    font-weight: 500;

    i {
      margin-right: ${({before}) => (before ? 21 : 0)}px;
      margin-left: ${({after}) => (after ? 21 : 0)}px;
      line-height: 1;
    }
  }
`;

export const Follow = styled(Parent)`
  width: 99px;
  height: 29px;
  font-size: 14px;
  line-height: 1;
  color: ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border: 1px solid ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border-radius: 29px;
  padding: 0;
  transition: background-color 0.3s ease, color 0.3s ease, border-color 0.3s ease;
  position: relative;
  background-color: ${({loading}) => (loading ? '#ed484f' : '#fff')};

  i:first-child {
    margin-right: 8px;
  }

  path {
    fill: currentColor;
  }

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  @media (min-width: ${media.tablet}) {
    &:hover {
      color: #fff;
      background: #ed484f;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    height: 25px;
    width: 83px;
    font-size: 12px;
    line-height: 13px;
  }
`;

export const Following = styled(Follow)`
  color: ${({loading}) => (loading ? '#ed484f' : '#545454')};
  border: 1px solid ${({loading}) => (loading ? '#ed484f' : '#c3c3c3')};
`;

export const FollowMd = styled(Parent)`
  width: 80px;
  height: 26px;
  font-size: 12px;
  line-height: 1;
  color: ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border: 1px solid ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border-radius: 26px;
  padding: 0;
  transition: background-color 0.3s ease, color 0.3s ease, border-color 0.3s ease;
  position: relative;
  background-color: ${({loading}) => (loading ? '#ed484f' : '#fff')};

  i:not(.loader) {
    margin-right: 8px;

    svg {
      width: 9px;
      height: 9px;
      position: relative;
    }

    path {
      fill: currentColor;
    }
  }

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  @media (min-width: ${media.tablet}) {
    &:hover {
      color: #fff;
      background: #ed484f;
    }
  }
`;

export const FollowingMd = styled(FollowMd)`
  color: ${({loading}) => (loading ? '#ed484f' : '#545454')};
  border: 1px solid ${({loading}) => (loading ? '#ed484f' : '#c3c3c3')};
`;

export const FollowSm = styled(Parent)`
  width: 64px;
  height: 20px;
  font-size: 11px;
  line-height: 20px;
  color: ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border: 1px solid ${({active}) => (active ? '#8F8F8F' : '#ed484f')};
  border-radius: 24px;
  padding: 0;
  transition: background-color 0.3s ease, color 0.3s ease, border-color 0.3s ease;
  position: relative;
  background-color: ${({loading}) => (loading ? '#ed484f' : '#fff')};

  i:not(.loader) {
    margin-right: 8px;
    svg {
      width: 9px;
      height: 9px;
      position: relative;
    }

    path {
      fill: currentColor;
    }
  }

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  @media (min-width: ${media.tablet}) {
    &:hover {
      color: #fff;
      background: #ed484f;
    }
  }
`;

export const FollowingSm = styled(FollowSm)`
  color: ${({loading}) => (loading ? '#ed484f' : '#545454')};
  border: 1px solid ${({loading}) => (loading ? '#ed484f' : '#c3c3c3')};
`;

export const FollowText = styled(Parent)`
  font-size: 14px;
  line-height: 1.2;
  font-weight: 700;
  color: ${primaryColor};
  position: relative;

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    opacity: 0;
  }

  ${({loading}) =>
    loading
      ? css`
          color: #fff;

          i.loader {
            opacity: 1;
          }
        `
      : null}
`;

export const FollowingText = styled(Parent)`
  font-size: 14px;
  line-height: 1.2;
  font-weight: 700;
  color: #8f8f8f;
  position: relative;

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    opacity: 0;
  }

  i:not(.loader) {
    margin-right: 5px;
    svg {
      width: 11px;
      height: 11px;
      position: relative;
    }

    path {
      fill: currentColor;
    }
  }

  ${({loading}) =>
    loading
      ? css`
          color: #fff;

          i.loader {
            opacity: 1;
          }
        `
      : null}
`;

export const Primary = styled(Parent)`
  padding: 0 10px;
  overflow: hidden;
  height: 40px;
  border-radius: 40px;
  background-color: #ed484f;
  color: #fff;
  transition: color 0.3s ease-in, background-color 0.3s ease-in;
  position: relative;
  ${({maxw}) =>
    maxw
      ? css`
          max-width: ${maxw};
          width: 100%;
        `
      : null};
  ${({min}) =>
    min
      ? css`
          min-width: ${min}px;
        `
      : null};

  &[disabled] {
    background-color: #f27f84;
    cursor: not-allowed;
  }

  i {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    opacity: 0;
    transition: opacity 0.2s ease-in;
    border-radius: inherit;
  }

  &::before {
    content: '';
    background-color: inherit;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }

  ${({loading}) =>
    loading
      ? css`
          i {
            opacity: 1;
          }

          &::before {
            opacity: 1;
          }
        `
      : null}

  &:not([disabled]):hover {
    background-color: #f27f84;
  }
`;

export const PrimaryBig = styled(Primary)`
  font-size: 18px;
  font-weight: 500;
`;

export const PrimaryBold = styled(Primary)`
  font-weight: 700;
`;

export const PrimarySmall = styled(Primary)`
  padding: 0 35px;
  height: 32px;
`;

export const Continue = styled(Parent)`
  border: 1px solid currentColor;
  color: #000;
  height: 32px;
  line-height: 32px;
  border-radius: 32px;
  font-size: 14px;
  padding: 0 18px;
  position: relative;
  transition: color 0.3s ease-in, background-color 0.3s ease-in;

  i:not(.loader) {
    margin-left: 10px;
  }

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    opacity: ${({loading}) => (loading ? 1 : 0)};
    transition: opacity 0.3s ease-in;
    border-radius: inherit;
  }

  &:hover {
    color: #fff;
    background-color: #000;
  }

  &[disabled] {
    cursor: not-allowed;
    color: #888;
  }

  &::before {
    content: '';
    background-color: inherit;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    opacity: ${({loading}) => (loading ? 1 : 0)};
    transition: opacity 0.2s ease-in;
  }

  ${({loading}) =>
    loading
      ? css`
          border-color: #ed484f !important;
          background-color: #ed484f;
          pointer-events: none;
        `
      : null}
`;

export const ContinueBig = styled(Continue)`
  height: 40px;
  line-height: 40px;
  border-radius: 40px;
  font-weight: 700;

  ${({full}) =>
    full
      ? css`
          max-width: 295px;
          width: 100%;
        `
      : null}

  i:not(.loader) {
    margin-left: 30px;
  }
`;

export const ContinueSm = styled(Continue)`
  height: 29px;
  line-height: 29px;
  border-radius: 29px;
  font-weight: 500;
  padding-right: 16px;
  padding-left: 16px;
  font-size: 12px;

  ${({full}) =>
    full
      ? css`
          max-width: 295px;
          width: 100%;
        `
      : null}

  i:not(.loader) {
    margin-left: 30px;
  }
`;

export const PrimaryBare = styled(Parent)`
  border: 1px solid currentColor;
  color: #ed484f;
  height: 40px;
  line-height: 40px;
  border-radius: 40px;
  font-size: 14px;
  padding: 0 18px;
  position: relative;
  transition: color 0.3s ease-in, background-color 0.3s ease-in;
  font-weight: 500;

  i:not(.loader) {
    margin-left: 10px;
  }

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    opacity: ${({loading}) => (loading ? 1 : 0)};
    transition: opacity 0.3s ease-in;
    border-radius: inherit;
  }

  &[disabled] {
    cursor: not-allowed;
    color: #f99;
  }

  &::before {
    content: '';
    background-color: inherit;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    opacity: ${({loading}) => (loading ? 1 : 0)};
    transition: opacity 0.2s ease-in;
  }

  ${({loading}) =>
    loading
      ? css`
          border-color: #ed484f !important;
          background-color: #ed484f;
          pointer-events: none;
        `
      : null}
`;

export const PrimaryBareSm = styled(Parent)`
  border: 1px solid currentColor;
  color: #ed484f;
  height: 30px;
  line-height: 30px;
  border-radius: 30px;
  font-size: 12px;
  padding: 0 18px;
  position: relative;
  transition: color 0.3s ease-in, background-color 0.3s ease-in;
  font-weight: 500;

  i:not(.loader) {
    margin-left: 10px;
  }

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    opacity: ${({loading}) => (loading ? 1 : 0)};
    transition: opacity 0.3s ease-in;
    border-radius: inherit;
  }

  &[disabled] {
    cursor: not-allowed;
    color: #f99;
  }

  &::before {
    content: '';
    background-color: inherit;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    opacity: ${({loading}) => (loading ? 1 : 0)};
    transition: opacity 0.2s ease-in;
  }

  ${({loading}) =>
    loading
      ? css`
          border-color: #ed484f !important;
          background-color: #ed484f;
          pointer-events: none;
        `
      : null}
`;

export const Social = styled.button`
  background: #ffffff;
  border: 1px solid #e4e4e4;
  box-sizing: border-box;
  border-radius: 100px;
  color: #464646;
  padding: 14px 24px;
  display: inline-block;
  line-height: 1;
  width: 100%;
  max-width: 246px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  i,
  span {
    vertical-align: middle;
  }

  i {
    padding-right: 19px;
  }
`;

export const GoogleAuth = styled(Social)`
  strong {
    color: #287bdd;
  }
`;
export const FacebookAuth = styled(Social)`
  strong {
    color: #175195;
  }
`;

export const BackLg = styled(Parent)`
  height: 40px;
  background: #464646;
  border-radius: 40px;
  color: #fff;
  padding: 0 20px;

  i {
    margin-right: 13px;
  }
`;

export const PostType = styled(Parent)`
  height: 30px;
  border-radius: 30px;
  padding-right: 17px;
  padding-left: 15px;
  color: #000;
  background-color: transparent;
  border: 1px solid #000;
  transition: color 0.3s ease, border-color 0.3s ease, background-color 0.3s ease;

  i {
    margin-right: 12px;
  }

  path {
    fill: currentColor;
  }

  ${({active}) =>
    active
      ? css`
          color: #fff;
          background-color: #000;
          border: 1px solid #000;
        `
      : null}

  &:disabled {
    color: #b2b2b2;
    background-color: transparent;
    border-color: #b2b2b2;
  }
`;

export const BackBorder = styled(Parent)`
  height: 40px;
  background: transparent;
  border: 1px solid rgb(102, 102, 102);
  border-radius: 40px;
  color: rgb(102, 102, 102);
  padding: 0 20px;

  i {
    margin-right: 13px;
  }
`;

export const Subscribe = styled(Parent)`
  overflow: hidden;
  height: 35px;
  border-radius: 35px;
  background-color: #ed484f;
  color: #fff;
  transition: color 0.3s ease-in, background-color 0.3s ease-in;
  position: relative;
  font-size: 16px;
  ${({min}) =>
    min
      ? css`
          min-width: ${min}px;
        `
      : css`
          min-width: 151px;
        `}

  &[disabled] {
    background-color: #f27f84;
    cursor: not-allowed;
  }

  i.loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 2;
    opacity: 0;
    transition: opacity 0.2s ease-in;
    border-radius: inherit;
  }

  i:not(.loader) {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 20px;

    path {
      fill: currentColor;
    }
  }

  &::before {
    content: '';
    background-color: inherit;
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1;
    opacity: 0;
    transition: opacity 0.2s ease-in;
  }

  ${({loading}) =>
    loading
      ? css`
          i.loader {
            opacity: 1;
          }

          &::before {
            opacity: 1;
          }
        `
      : null}

  &:not([disabled]):hover {
    background-color: #f27f84;
  }
`;

export const Sell = styled(Parent)`
  height: 28px;
  border-radius: 30px;
  padding: 0 5px;
  color: ${primaryColor};
  border: 1px solid ${({loading}) => (loading ? primaryColor : 'currentColor')};
  background-color: ${({loading}) => (loading ? primaryColor : '#fff')};
  justify-content: space-between;
  transition: color 0.3s ease-in, background-color 0.3s ease-in, border-color 0.3s ease-in;
  position: relative;
  font-size: 12px;
  ${({ml}) =>
    ml
      ? css`
          margin-left: ${ml};
        `
      : null}

  .loader {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  svg {
    fill: currentColor;
    margin-right: 10px;
  }

  &:hover {
    color: #fff;
    background-color: ${primaryColor};
    border-color: ${primaryColor};
  }
`;

export const Multisell = styled(Sell)`
  color: ${({loading}) => (loading ? primaryColor : '#666')};
`;

export const Message = styled(Parent)`
  padding: 0;
`;
