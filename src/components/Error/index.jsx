import React, {useEffect} from 'react';

import {Wrapper, Content, Title, Text, Icon} from 'components/Error/styled';

const Error = ({title = 'Something went wrong', customText = '', children}) => {
  useEffect(() => {
    console.log(children);
  }, [children]);

  return (
    <Wrapper>
      <Icon>&#9785;</Icon>
      <Content>
        {title && <Title>{title}</Title>}
        {customText ? (
          <Text>{customText}</Text>
        ) : (
          <Text>
            We are already notified, and will solve issue as soon as possible, but if you have something to add, please
            contact the support team here: <a href="mailto:admin@testSample.com">admin@testSample.com</a>
          </Text>
        )}
      </Content>
    </Wrapper>
  );
};

export default Error;
