import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  margin: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Content = styled.div`
  margin: 0 20px;
  max-width: 800px;
`;

export const Title = styled.div`
  color: #ed484f;
  border-radius: 4px;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  padding: 5px 0;
  line-height: normal;
`;

export const Text = styled.div`
  color: #ed484f;
  border-radius: 4px;
  font-style: normal;
  font-weight: 400;
  padding: 5px 0;

  & a {
    color: #ed484f;
    text-decoration: underline;
  }
`;

export const Icon = styled.div`
  color: #ed484f;
  font-style: normal;
  font-weight: 500;
  font-size: 30px;
`;
