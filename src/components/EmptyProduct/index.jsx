import React from 'react';
import {Card, Image, Price, Title, AdditionalInfo} from './styled';

const EmptyProduct = () => (
  <Card>
    <Image />
    <Price />
    <Title />
    <AdditionalInfo />
  </Card>
);

export default EmptyProduct;
