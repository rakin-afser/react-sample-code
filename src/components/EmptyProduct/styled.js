import styled, {keyframes} from 'styled-components';

const loading = keyframes`
to {
  background-position: calc(100% + 100px) 50%;
}
`;

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  width: 200px;
  padding: 12px 14px;
  margin: 10px;
  background-color: silver;
  position: relative;

  &:after {
    display: block;
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(90deg, transparent 0, silver 50%, transparent 100%);
    background-size: 100px 100%;
    background-position: -100px 50%;
    background-repeat: no-repeat;
    z-index: 1;
    top: 0;
    left: 0;
    animation: ${loading} 1.5s infinite;
  }
`;

export const Image = styled.div`
  width: 100%;
  height: 100px;
  margin-bottom: 14px;
  background-color: white;
`;

export const Price = styled.div`
  width: 120px;
  height: 18px;
  margin-bottom: 14px;
  background-color: white;
`;

export const Title = styled.div`
  width: 150px;
  height: 14px;
  margin-bottom: 14px;
  background-color: white;
`;

export const AdditionalInfo = styled.div`
  width: 100px;
  height: 18px;
  background-color: white;
`;
