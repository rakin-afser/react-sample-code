import userAvatar from './img/userAvatar.png';
import userAvatar2 from './img/userAvatar2.png';
import userAvatar3 from './img/userAvatar3.png';
import postPic from './img/postPic.jpg';
import postPic2 from './img/postPic2.png';
import postPic3 from './img/postPic3.png';

export default {
  user: {
    avatar: userAvatar,
    firstName: 'Kathryn',
    lastName: 'Serera',
    alias: 'kathryn_mccoy',
    shopName: 'Hazel & Glory',
    shopPic: postPic2,
    seller: true
  },
  notifications: {
    title: 'Notifications',
    href: '/profile/notifications',
    children: [
      {
        id: '1',
        avatar: userAvatar,
        title: 'Kevin Baltimorian',
        info: '3min started following you.',
        type: 'following'
      },
      {
        id: '2',
        avatar: userAvatar2,
        title: 'Nika Pink',
        info: '2w started following you.',
        type: 'follow'
      },
      {
        id: '3',
        avatar: userAvatar3,
        postPreview: postPic,
        title: 'Oleg Welton',
        description:
          'Wow! So good suit and tie, I like it very much. Please, give me the contact of the store where you bought it.',
        info: '1d replited to your post:',
        type: 'replied'
      },
      {
        id: '4',
        avatar: userAvatar,
        title: 'Kevin Baltimorian',
        description: '',
        info: '3min started following you.',
        type: 'following'
      },
      {
        id: '5',
        avatar: userAvatar3,
        postPreview: postPic2,
        title: 'Oleg Welton',
        description:
          'Wow! So good suit and tie, I like it very much. Please, give me the contact of the store where you bought it.',
        info: '1d replited to your post:',
        type: 'replied'
      },
      {
        id: '6',
        avatar: userAvatar2,
        postPreview: postPic3,
        title: 'Jorge Webb',
        description: 'Oh, Hi)))Could you give me one?',
        info: '2d mentioned you:',
        type: 'replied'
      },
      {
        id: '7',
        avatar: userAvatar2,
        title: 'Nika Pink',
        info: '2w started following you.',
        type: 'follow'
      },
      {
        id: '8',
        avatar: userAvatar3,
        postPreview: postPic,
        title: 'Oleg Welton',
        description:
          'Wow! So good suit and tie, I like it very much. Please, give me the contact of the store where you bought it.',
        info: '1d replited to your post:',
        type: 'replied'
      },
      {
        id: '9',
        avatar: userAvatar3,
        postPreview: postPic2,
        title: 'Oleg Welton',
        description:
          'Wow! So good suit and tie, I like it very much. Please, give me the contact of the store where you bought it.',
        info: '1d replited to your post:',
        type: 'replied'
      },
      {
        id: '10',
        avatar: userAvatar2,
        postPreview: postPic3,
        title: 'Jorge Webb',
        description: 'Oh, Hi)))Could you give me one?',
        info: '2d mentioned you:',
        type: 'replied'
      }
    ]
  },
  message: {
    title: 'Messages',
    href: '/profile/messages',
    children: [
      {
        id: '1',
        avatar: userAvatar,
        title: 'Jorge Webb',
        description: 'I would like to return th dress can lorem ipsum',
        date: 'Today, 10:30 pm',
        counter: '1',
        status: true,
        notRead: 35
      },
      {
        id: '2',
        avatar: userAvatar,
        title: 'Jorge Webb',
        description: 'So cute. I agree with you. If you agree! Another message ',
        date: 'Wed',
        counter: '1',
        status: false,
        notRead: 1
      },
      {
        id: '3',
        avatar: userAvatar,
        title: 'Jorge Webb',
        description: 'Yes, sure)',
        date: 'Today, 10:30 pm',
        counter: '1',
        status: true,
        notRead: 3
      },
      {
        id: '4',
        avatar: userAvatar,
        title: 'Jorge Webb',
        description: 'Hello, this dress is 100% silk.',
        date: 'Today, 10:30 pm',
        counter: '1',
        status: false,
        notRead: 7
      },
      {
        id: '5',
        avatar: userAvatar,
        title: 'Jorge Webb',
        description: 'Me too))))',
        date: 'Wed',
        counter: '1',
        status: true,
        notRead: 0
      },
      {
        id: '6',
        avatar: userAvatar,
        title: 'Jorge Webb',
        description: 'So cute. I agree with you. If you agree! Another message',
        date: 'Wed',
        counter: '1',
        status: false,
        notRead: 0
      }
    ]
  },
  cart: {
    title: 'Cart',
    currency: 'BD',
    children: [
      {
        id: '1',
        pic: postPic3,
        name: 'Winter coat',
        price: 132,
        midcoins: 10
      },
      {
        id: '2',
        pic: postPic2,
        name: 'Sneakers MIU MIU',
        price: 292,
        midcoins: 9
      },
      {
        id: '3',
        pic: postPic3,
        name: '123456',
        price: 132,
        midcoins: 18
      },
      {
        id: '4',
        pic: postPic3,
        name: 'Lorem Ipsum',
        price: 292
      },
      {
        id: '5',
        pic: postPic2,
        name: 'Sneakers MIU MIU',
        price: 292
      }
    ]
  }
};
