import React from 'react';
import {Link} from 'react-router-dom';
import Scrollbar from 'react-scrollbars-custom';
import moment from 'moment';
import Header from 'components/NotificationBar/components/Header';
import useGlobal from 'store';

import {
  Avatar,
  AvatarWrap,
  ItemDescription,
  ItemText,
  MessageDate,
  NotificationsItem
} from 'components/NotificationBar/styled';
import avatarPlaceholder from 'images/avatarPlaceholder.png';

const Messages = ({data, user, closeMessages}) => {
  const unreadMessage = window.talkJsSession.unreads.K;
  const [globalState, setGlobalState] = useGlobal();
  const {messenger} = globalState;

  const onShowMessenger = () => {
    setGlobalState.setMessenger({visible: true, minify: true, data: messenger.data});
    closeMessages();
  };

  return (
    <>
      <Header data={data} user={user} />
      <Scrollbar style={{height: 400}}>
        {/*{data?.children?.map((item, i) => (*/}
        {/*  <NotificationsItem message key={`messages-${item.id}`}>*/}
        {/*    <AvatarWrap>*/}
        {/*      <Link to={`/user/${item.title}/lists`}>*/}
        {/*        <Avatar message src={item.avatar} />*/}
        {/*      </Link>*/}
        {/*      {item.status ? <UserStatus /> : null}*/}
        {/*    </AvatarWrap>*/}
        {/*    <ItemDescription>*/}
        {/*      <ItemText title>{item.title}</ItemText>*/}
        {/*      <ItemText message>{item.description}</ItemText>*/}
        {/*    </ItemDescription>*/}
        {/*    {!!item.notRead && <MessageCounter>{item.notRead}</MessageCounter>}*/}
        {/*    <MessageDate>{item.date}</MessageDate>*/}
        {/*  </NotificationsItem>*/}
        {/*))}*/}
        {unreadMessage?.map((item, i) => {
          return (
            <NotificationsItem onClick={onShowMessenger} message key={item.lastMessage.senderId}>
              <AvatarWrap>
                <Link to={`/profile/${item.lastMessage.senderId}/posts`}>
                  <Avatar message src={item.lastMessage.sender.photoUrl || avatarPlaceholder} />
                </Link>
                {/*{item.status ? <UserStatus /> : null}*/}
              </AvatarWrap>
              <ItemDescription>
                <ItemText title>{item.lastMessage.sender.name}</ItemText>
                <ItemText message>{item.lastMessage.body}</ItemText>
              </ItemDescription>
              {/*{!!item.notRead && <MessageCounter>{item.notRead}</MessageCounter>}*/}
              <MessageDate>{moment(item.lastMessage.timestamp).fromNow()}</MessageDate>
            </NotificationsItem>
          );
        })}
      </Scrollbar>
    </>
  );
};

export default Messages;
