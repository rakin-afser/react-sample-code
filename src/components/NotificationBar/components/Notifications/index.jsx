import React from 'react';
import {useQuery} from '@apollo/client';
import {Link} from 'react-router-dom';
import Scrollbar from 'react-scrollbars-custom';
import moment from 'moment';

import {
  NotificationsItem,
  Avatar,
  ItemDescription,
  ItemText,
  FollowButton,
  ItemPhoto
} from 'components/NotificationBar/styled';
import Header from 'components/NotificationBar/components/Header';
import Loader from 'components/Loader';
import {useUser} from 'hooks/reactiveVars';
import {GET_NOTIFICATIONS} from 'components/NotificationBar/api/queries';
import Checkmark from 'assets/Checkmark';
import Plus from 'assets/Plus';
import userPlaceholder from 'images/placeholders/user.jpg';

const header = {title: 'Notifications', href: '/profile/notifications'};

const Notifications = ({}) => {
  const [user] = useUser();
  const {data, loading, error} = useQuery(GET_NOTIFICATIONS, {
    variables: {id: user?.databaseId}
  });

  return (
    <>
      <Header data={header} user={user} />
      <Scrollbar style={{height: '100%'}}>
        {loading && <Loader wrapperWidth="100%" wrapperHeight="100px" />}
        {!loading && !data?.notifications?.length && (
          <NotificationsItem>You have no notifications yet</NotificationsItem>
        )}
        {data?.notifications?.map((item, i) => (
          <NotificationsItem key={`notifications-${item.id}`}>
            <Link to={`/user/${item.title}/lists`}>
              <Avatar src={item.avatar || userPlaceholder} />
            </Link>
            <ItemDescription>
              <ItemText>!username!</ItemText>
              <ItemText gray>{moment(item.notification_time).fromNow()}</ItemText>
              <ItemText>{item.message}</ItemText>
            </ItemDescription>
            {item.type === 'follow' || item.type === 'following' ? (
              <FollowButton transparent={item.type === 'following'}>
                {item.type === 'follow' ? <Plus /> : <Checkmark width={15} />}
                {item.type}
              </FollowButton>
            ) : (
              <ItemPhoto src={item.postPreview} />
            )}
          </NotificationsItem>
        ))}
      </Scrollbar>
    </>
  );
};

export default Notifications;
