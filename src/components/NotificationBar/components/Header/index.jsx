import React, {useState} from 'react';
import {useUser} from 'hooks/reactiveVars';
import {BarHeader, BarLink, BarTitle, Tab, TabsWrapper} from 'components/NotificationBar/styled';
import Arrow from 'assets/Arrow';

const Header = ({data, user}) => {
  const [userData] = useUser();

  const [activeTab, setActiveTab] = useState('user');
  return (
    <>
      {user.seller && (
        <TabsWrapper activeTab={activeTab}>
          <Tab active={activeTab === 'user'} onClick={() => setActiveTab('user')}>
            {userData.name}
          </Tab>
          <Tab active={activeTab === 'shop'} onClick={() => setActiveTab('shop')}>
            Shop name
          </Tab>
        </TabsWrapper>
      )}
      <BarHeader>
        <BarTitle>{data?.title}</BarTitle>
        <BarLink href={data?.href}>
          View All
          <Arrow height={15} />
        </BarLink>
      </BarHeader>
    </>
  );
};

export default Header;
