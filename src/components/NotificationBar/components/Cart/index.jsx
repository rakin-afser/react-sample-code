import React, {useEffect} from 'react';
import {useLazyQuery, useMutation} from '@apollo/client';

import {
  Coins,
  BarHeader,
  NotificationsItem,
  ItemDescription,
  ItemText,
  Quantity,
  QuantityAdd,
  QuantityRemove,
  QuantityTitle,
  QuantityCounter,
  CartHeading,
  CartCounter,
  StyledScrollbar,
  CartContainer,
  SubTotal,
  SubtotalCount,
  CartCheckout,
  CartItemPreview,
  CartView,
  Actions,
  ItemLink,
  CartItemLink,
  ItemLinkContainer
} from 'components/NotificationBar/styled';
import CoinsIcon from 'assets/Profile/Coins';
import {useCurrency, useUser} from 'hooks/reactiveVars';
import Grid from 'components/Grid';
import {CART_MINI} from 'queries';
import {UpdateItemQuantities} from 'mutations';
import Loader from 'components/Loader';
import {getCheckoutLink} from 'util/heplers';

const Cart = () => {
  const [currency] = useCurrency();
  const [getMiniCart, {data: dataCart, loading: loadingCart}] = useLazyQuery(CART_MINI);
  const [updateQuantity] = useMutation(UpdateItemQuantities);
  const [user] = useUser();

  useEffect(() => {
    getMiniCart();
  }, [getMiniCart]);

  const midcoinsSum = dataCart?.cart?.contents?.nodes
    ?.map((item) => item.midCoins)
    ?.reduce((count, item) => (item ? count + item : count), 0);

  const quantity = dataCart?.cart?.contents?.nodes
    ?.map((item) => item.quantity)
    ?.reduce((count, item) => (item ? count + item : count), 0);

  function renderCart() {
    if (loadingCart) {
      return <Loader wrapperWidth="100%" wrapperHeight="100px" />;
    }

    if (!dataCart?.cart?.contents?.nodes?.length) {
      return (
        <Grid padding="20px">
          <p>Shopping cart is empty</p>
        </Grid>
      );
    }

    function decrQuantity(key, quantity) {
      const newQuantity = quantity - 1;
      if (newQuantity >= 0) {
        updateQuantity({
          variables: {input: {items: [{key, quantity: newQuantity}]}},
          update(cache, {data}) {
            cache.modify({
              fields: {
                cart() {
                  return data.updateItemQuantities.cart;
                }
              }
            });
          }
        });
      }
    }

    function incrQuantity(key, quantity) {
      const newQuantity = quantity + 1;
      updateQuantity({
        variables: {input: {items: [{key, quantity: newQuantity}]}},
        update(cache, {data}) {
          cache.modify({
            fields: {
              cart() {
                return data.updateItemQuantities.cart;
              }
            }
          });
        }
      });
    }

    return (
      <CartContainer>
        <StyledScrollbar style={{height: 400}}>
          {dataCart?.cart?.contents?.nodes?.map((item) => {
            if (!item) {
              return null;
            }

            const {quantity, total, key, midCoins} = item;
            const {id, name, image, slug} = item.product.node;

            return (
              <NotificationsItem cart key={`notifications-${id}`}>
                <CartItemLink to={`product/${slug}`}>
                  <CartItemPreview src={image?.sourceUrl} alt={image?.altText} />
                </CartItemLink>
                <ItemDescription cart>
                  <ItemLinkContainer>
                    <ItemLink to={`product/${slug}`}>{name}</ItemLink>
                  </ItemLinkContainer>
                  <ItemText>
                    {/* <ItemText gray>{currency}</ItemText> */}
                    <ItemText size={14}>{total}</ItemText>
                    {!!item?.midCoins && <ItemText midcoins>+{midCoins} MidCoins</ItemText>}
                  </ItemText>
                  <Quantity>
                    <QuantityTitle>Quantity:</QuantityTitle>
                    <QuantityRemove onClick={() => decrQuantity(key, quantity)} />
                    <QuantityCounter>{quantity}</QuantityCounter>
                    <QuantityAdd onClick={() => incrQuantity(key, quantity)} />
                  </Quantity>
                </ItemDescription>
              </NotificationsItem>
            );
          })}
          {/* <CartPromo>Free Shipping on orders over {currency} 200.000</CartPromo> */}
          <SubTotal>
            {midcoinsSum ? (
              <Coins>
                <span>MidCoins:</span> +{midcoinsSum?.toFixed(2)}
                <CoinsIcon />
              </Coins>
            ) : null}
            Subtotal:<SubtotalCount>{dataCart?.cart?.total}</SubtotalCount>
          </SubTotal>
        </StyledScrollbar>
        <Actions>
          <CartCheckout to="/cart">View Cart</CartCheckout>
          <CartView to={getCheckoutLink(dataCart?.cart?.subcarts?.subcarts, user)}>Checkout</CartView>
        </Actions>
      </CartContainer>
    );
  }

  return (
    <>
      <BarHeader cart>
        <CartHeading>My Cart</CartHeading>
        <CartCounter>
          {quantity || 0} item{quantity > 1 || !quantity ? 's' : ''}
        </CartCounter>
      </BarHeader>
      {renderCart()}
    </>
  );
};

export default Cart;
