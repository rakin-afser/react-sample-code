import React, {useEffect} from 'react';
import Messages from 'components/NotificationBar/components/Messages';

import {
  BarContainer,
  CloseBtn,
  OuterWrapper,
  InnerWrapper,
} from './styled';
import templateData from 'components/NotificationBar/templateData';
import Profile from 'components/Profile';
import pusherJs from 'services/pusher';
import Notifications from 'components/NotificationBar/components/Notifications';
import Cart from 'components/NotificationBar/components/Cart';

function getItem(name, closeHandler) {
  switch (name) {
    case 'message':
      return <Messages data={templateData.message} closeMessages={closeHandler} user={templateData.user} />;
    case 'cart':
      return <Cart data={templateData.cart} />;
    case 'profile':
      return <Profile data={templateData.user} />;
    case 'notifications':
    default:
      return <Notifications data={templateData.notifications} user={templateData.user} />;
  }
}

const NotificationBar = ({contentType, closeHandler}) => {

  useEffect(() => {
    document.addEventListener('click', closeBar);
    return () => {
      document.removeEventListener('click', closeBar);
    };
  });

  useEffect(() => {
    // pusherJs.bind('my-channel', 'my-event', () => console.log('event'));
    // return () => pusherJs.unbind('my-channel', 'my-event', () => console.log('event'));
  }, []);

  const closeBar = (ev) => {
    if (!ev.target.closest('.barContainer') && !ev.target.closest('.barControls')) {
      closeHandler(false);
    }
  };

  return (
    <OuterWrapper>
      <InnerWrapper>
        <BarContainer profile={contentType === 'profile'} className="barContainer">
          <CloseBtn onClick={closeHandler}>X</CloseBtn>
          {getItem(contentType, closeHandler)}
        </BarContainer>
      </InnerWrapper>
    </OuterWrapper>
  );
};

export default NotificationBar;
