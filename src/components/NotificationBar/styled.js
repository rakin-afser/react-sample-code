import styled, {css} from 'styled-components/macro';
import Scrollbar from 'react-scrollbars-custom';

import {primaryColor, grayTextColor, midCoinsColor, disabledLinkColor} from 'constants/colors';
import {Link} from 'react-router-dom';

export const OuterWrapper = styled.div`
  cursor: default;
  position: absolute;
  left: 0;
  z-index: 999;
  transition: 0.5s;
  background: rgba(0, 0, 0, 0.4);
  width: 100%;
  min-height: 100vh;
  overflow: hidden;
  opacity: 0;
  display: flex;
  justify-content: flex-end;
  align-items: flex-start;

  &.notificationBar-enter-active {
    opacity: 1;
  }

  &.notificationBar-enter-done {
    opacity: 1;
  }
`;

export const InnerWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  display: flex;
  justify-content: flex-end;
  max-width: 1440px;
`;

export const BarContainer = styled.div`
  right: 0;
  width: 340px;
  height: calc(100vh - 80px);
  max-height: 460px;
  overflow: hidden;
  background: #fff;
  border-radius: 8px;
  margin: 4px;

  .ScrollbarsCustom-TrackY {
    border-radius: 3px !important;
    width: 4px !important;
  }

  .ScrollbarsCustom-ThumbY {
    background: #9b9b9b !important;
    border-radius: 3px !important;
  }

  .ScrollbarsCustom-Wrapper {
    right: 0 !important;
  }

  ${({profile}) =>
    profile &&
    css`
      width: 235px;
      margin-right: 80px;
      height: auto;
    `}
`;

export const BarHeader = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
  border-bottom: 1px solid #efefef;
  padding: 16px 24.5px 16px 16px;

  ${({cart}) =>
    cart &&
    `
    align-items: center;
    padding: 16px;
    justify-content: center;
  `}
`;

export const TabsWrapper = styled(BarHeader)`
  padding: 25px 0 0 0;
  position: relative;

  &::before {
    content: '';
    position: absolute;
    width: 50%;
    height: 2px;
    background-color: #000;
    left: 0;
    bottom: 0;
    transition: left 0.4s;
  }

  ${({activeTab}) =>
    activeTab === 'shop' &&
    `
    &::before {
      left: 50%;
    }
  `}
`;

export const Tab = styled.div`
  padding-bottom: 8px;
  font-weight: bold;
  font-size: 14px;
  color: ${disabledLinkColor};
  width: 50%;
  display: flex;
  justify-content: center;
  cursor: pointer;
  transition: color 0.4s;

  &:hover {
    color: #000;
  }

  ${({active}) =>
    active &&
    `
    color: #000;
  `}
`;

export const BarTitle = styled.span`
  font-size: 16px;
  color: #000;
  flex-grow: 1;
`;

export const BarLink = styled.a`
  display: flex;
  align-items: flex-start;
  flex-grow: 1;
  justify-content: flex-end;
  font-size: 14px;
  color: #000;
  transition: color ease 0.4s;
  cursor: pointer;

  & svg {
    margin-left: 16.5px;
    padding-top: 1px;
    flex-shrink: 0;
    transition: margin-left ease 0.4s;

    path {
      transition: fill ease 0.4s;
      fill: #666666;
    }
  }

  &:hover {
    color: ${primaryColor};
    & svg {
      margin-left: 10px;
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const BarContent = styled.div`
  overflow: hidden;
  max-height: 400px;
`;

export const NotificationsItem = styled.div`
  position: relative;
  display: flex;
  align-items: flex-start;
  border-bottom: 1px solid #efefef;
  padding: 16px;
  transition: box-shadow ease 0.4s;
  font-weight: 400;

  ${({message}) =>
    message &&
    `
    align-items: center;
    &:hover {
      cursor: pointer;
      box-shadow: 0 0 11px rgba(0,0,0,0.2);
    }
  `}

  ${({cart}) =>
    cart &&
    `
    border-bottom: none;
    padding: 16px 17px 0 16px;
  `}
`;

export const ItemDescription = styled.div`
  margin-left: 10px;
  ${({cart}) =>
    cart &&
    `
    align-self: stretch;
    margin-left: 0;
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: space-between;
    width: 100%;
  `}
`;

export const Avatar = styled.img`
  display: block;
  object-fit: cover;
  flex-shrink: 0;
  border-radius: 50%;
  width: ${({message}) => (message ? '48px' : '40px')};
  height: ${({message}) => (message ? '48px' : '40px')};
  margin: 0 0 auto 0;
`;

export const ItemHeading = styled.span`
  font-size: 12px;
  color: #000;
  margin-bottom: 4px;

  &::after {
    ${({info}) =>
      info &&
      `
      content: '${info}';
      margin-left: 4px;
      color: #999999;
    `}
  }
`;

export const ItemLinkContainer = styled.div``;

export const ItemLink = styled(Link)`
  font-size: 12px;
  color: inherit;
  max-width: 220px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  display: inline-block;
`;

export const ItemText = styled.span`
  font-size:  ${({size}) => (size ? size : '12')}px;
  color: ${({gray}) => (gray ? '#999' : '#000')};
  margin-right: 4px;

  ${({message}) =>
    message &&
    `
      display: block;
      text-overflow: ellipsis;
      white-space: nowrap;
      max-width: 200px;
      overflow: hidden;
      margin: 0;
      color: #666666;
  `}
  ${({title}) =>
    title &&
    `
      font-size: 14px;
      display: block;
      margin: 0 0 6px 0;
  `}

  ${({cart}) =>
    cart &&
    `
    font-size: 14px;
    display: block;
    margin: 0;
  `}
  ${({midcoins}) =>
    midcoins &&
    `
    color: ${midCoinsColor};
  `}
`;

export const ItemPhoto = styled.img`
  display: block;
  max-width: 100%;
  margin-left: 25px;
  flex-shrink: 0;
  object-fit: cover;
`;

export const FollowButton = styled.div`
  font-size: 12px;
  font-weight: 300;
  border-radius: 24px;
  color: #fff;
  background: ${primaryColor};
  text-transform: capitalize;
  padding: 4px 15px;
  display: flex;
  cursor: pointer;
  min-width: 99px;
  align-items: center;
  justify-content: center;
  transition: box-shadow ease 0.4s;

  & svg {
    margin-right: 6px;
    flex-shrink: 0;
    path {
      fill: #fff;
    }
  }

  &:hover {
    box-shadow: 0 3px 4px rgba(0, 0, 0, 0.2);
  }

  ${({transparent}) =>
    transparent &&
    `
      padding: 3px 14px;
      min-width: unset;
      background: transparent;
      color: ${grayTextColor};
      border: 1px solid ${grayTextColor};
      & svg{
        path{
          fill: ${grayTextColor};
        }
      }
    `};
`;

export const MessageDate = styled.span`
  position: absolute;
  right: 16px;
  top: 8px;
  font-size: 12px;
  font-weight: 300;
  color: #999;
`;

export const MessageCounter = styled.i`
  position: absolute;
  right: 14px;
  bottom: 22px;
  display: flex;
  justy-content: center;
  align-items: center;
  border-radius: 50%;
  background: #ed484f;
  color: #fff;
  font-weight: 300;
  font-size: 12px;
  padding: 0 5px;
  font-style: normal;
`;

export const UserStatus = styled.i`
  display: block;
  position: absolute;
  top: 38px;
  left: 36px;
  width: 12px;
  height: 12px;
  border-radius: 8px;
  border: 2px solid #fff;
  background: #2ecc71;
`;

export const AvatarWrap = styled.div`
  position: relative;
`;

export const CartHeading = styled.span`
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  color: #000;
  letter-spacing: 0.016em;
`;

export const CartText = styled.span`
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  color: #000;
  letter-spacing: 0.016em;
`;

export const CartCounter = styled.span`
  margin-left: 11px;
  font-weight: 500;
  font-family: SF Pro Display;
  font-style: normal;
  line-height: 140%;
  font-size: 14px;
  color: #656565;
`;

export const Quantity = styled.div`
  display: flex;
  align-items: center;
`;

export const QuantityTitle = styled.span`
  font-size: 14px;
  flex-grow: 1;
  color: #a7a7a7;
`;

export const QuantityCounter = styled.div`
  margin: 0 11px;
  font-size: 12px;
  color: #666666;
`;

export const QuantityAdd = styled.button`
  background-color: #efefef;
  border: none;
  border-radius: 2px;
  outline: none;
  transition: all ease 0.3s;
  padding: 0 7px;

  &:hover {
    background-color: #cfcfcf;
  }

  &::before {
    content: '+';
  }
`;

export const QuantityRemove = styled(QuantityAdd)`
  padding: 0 9px;
  &::before {
    content: '-';
  }
`;

export const CartItemPreview = styled.img`
  width: 72px;
  height: 72px;
  border-radius: 4px;
  object-fit: cover;
`;

export const CartItem = styled.div`
  display: flex;
  align-items: center;
  margin: 0 0 21px 0;
`;

export const CartItemLink = styled(Link)`
  display: block;
  margin-right: 16px;
  flex-shrink: 0;
  max-width: 100%;
`;

export const CartPromo = styled.span`
  display: block;
  font-size: 14px;
  line-height: 20px;
  color: #666666;
  padding: 16px 16px 12px 16px;
  text-align: end;
`;

export const CartContainer = styled.div`
  position: relative;
  padding-bottom: 60px;
  height: calc(100% - 60px);
`;

export const StyledScrollbar = styled(Scrollbar)`
  height: 100% !important;
`;

export const CloseBtn = styled.button`
  display: none;
  position: absolute;
  top: 16px;
  right: 16px;
  width: 24px;
  height: 24px;

  @media (max-width: 767px) {
    display: block;
  }
`;

export const SubTotal = styled.span`
  display: flex;
  align-items: baseline;
  font-size: 14px;
  line-height: 20px;
  color: #a7a7a7;
  padding: 0 16px 21px 16px;
`;

export const SubtotalCount = styled.span`
  color: #000000;
  font-weight: 600;
  font-size: 18px;
  padding-left: 4px;
`;

export const Coins = styled.span`
  font-size: 14px;
  flex-grow: 1;
  font-weight: 700;
  color: ${midCoinsColor};
  align-items: center;
  display: flex;

  & svg {
    width: 16px;
    height: 16px;
    margin: 0 0 2px 2px;
  }

  & span {
    font-size: 12px;
    margin-right: 4px;
  }
`;

export const CartCurrency = styled.span`
  font-size: 14px;
  margin: 0 4px 0 8px;
  line-height: 20px;
`;

export const Actions = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 16px 16px 16px;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

export const CartCheckout = styled(Link)`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  min-width: 145px;
  font-size: 14px;
  padding: 6px 41px;
  border-radius: 24px;
  color: #545454;
  border: 1px solid #c3c3c3;
  background: transparent;
  outline: none;
  text-decoration: none;
  transition: box-shadow ease 0.4s;

  &:hover,
  active {
    color: #545454;
    box-shadow: 0 3px 4px rgba(0, 0, 0, 0.4);
  }
`;

export const CartView = styled(CartCheckout)`
  border: none;
  font-weight: 500;
  color: #fff;
  background: ${primaryColor};
  &:hover,
  active {
    color: #fff;
  }
`;
