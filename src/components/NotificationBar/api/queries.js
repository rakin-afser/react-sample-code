import {gql} from '@apollo/client';

export const GET_NOTIFICATIONS = gql`
  query GetNotifications($id: Int) {
    notifications(id: $id) {
      avatar
      id
      message
      notification_time
      notification_type
      read_status
      receiver
      slug
    }
  }
`;
