import React from 'react';
import {object, string, bool} from 'prop-types';

import {Wrapper} from './styled';
import Icon from '../Icon';
import {allColors} from '../../constants/colors';

/*
  @PARAMS
  variant: contained | outlined | '',
  backgroundColor: name of variable from src/constants/colors.js
  color: name of variable from src/constants/colors.js
  title: string title
  styles: styles as object
  isLoading: if true - show animation
  disabled: disable the button
  iconLeft: icon on the left, use name of icon, available icons here (import Icon from 'src/components/Icon')
  iconRight: icon on the right, use name of icon, available icons here (import Icon from 'src/components/Icon')
  animationColor: name of variable from src/constants/colors.js / gets color props by default

  @Example:
  <ButtonCustom title="Sign in" variant="outlined" color="mainWhiteColor" iconRight="chevronRight" />
* */

const ButtonCustom = ({
  variant,
  color,
  backgroundColor,
  title,
  styles,
  isLoading,
  disabled,
  iconLeft,
  iconRight,
  animationColor,
  iconStyles,
  ...props
}) => {
  return (
    <Wrapper
      {...props}
      styles={{...styles}}
      color={color}
      variant={variant}
      backgroundColor={backgroundColor}
      disabled={disabled}
      iconLeft={iconLeft}
      iconRight={iconRight}
      isLoading={isLoading}
    >
      {isLoading ? (
        <Icon
          type="loader"
          svgStyle={{width: 24, height: 8, fill: allColors[animationColor ? animationColor : color]}}
        />
      ) : (
        <>
          {iconLeft && <Icon type={iconLeft} svgStyle={{fill: allColors[color], ...iconStyles}} />}
          {title}
          {iconRight && <Icon type={iconRight} svgStyle={{fill: allColors[color], ...iconStyles}} />}
        </>
      )}
    </Wrapper>
  );
};

ButtonCustom.defaultProps = {
  children: null,
  backgroundColor: 'primaryColor',
  variant: '',
  color: 'mainBlackColor',
  title: '',
  styles: {},
  isLoading: false,
  disabled: false,
  iconLeft: null,
  iconRight: null,
  type: 'button',
  animationColor: '',
  iconStyles: {}
};

ButtonCustom.propTypes = {
  backgroundColor: string,
  variant: string,
  color: string,
  title: string.isRequired,
  styles: object,
  isLoading: bool,
  disabled: bool,
  iconLeft: string,
  iconRight: string,
  type: string,
  animationColor: string,
  iconStyles: object
};

export default ButtonCustom;
