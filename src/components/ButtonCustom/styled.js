import styled from 'styled-components';
import {allColors} from '../../constants/colors';
import media from '../../constants/media';

export const Wrapper = styled.button.attrs(({styles}) => ({style: {...styles}}))`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;
  outline: none;
  cursor: pointer;
  height: 40px;
  background: #ed484f;
  border-radius: 24px;
  transition: all 0.3s ease;
  display: flex;
  align-items: center;
  text-align: center;
  max-width: 168px;
  width: 100%;
  
  ${({disabled}) => (disabled ? `opacity: 0.5` : `opacity: 1`)}
  
  ${({iconLeft, iconRight, isLoading}) =>
    iconLeft && !isLoading
      ? `
          justify-content: flex-start;
          padding-left: 27px;
          & svg {
            margin-right: 33px
          }
        `
      : iconRight && !isLoading
      ? `
          justify-content: flex-end;
          padding-right: 27px;
          & svg {
            margin-left: 33px
          }
        `
      : `justify-content: center`}
  
  ${({variant, color, backgroundColor}) =>
    variant === 'contained'
      ? `
        background: ${allColors[backgroundColor]};
        border: none;
        color: ${allColors[color]};
      `
      : variant === 'outlined'
      ? `
        background: transparent;
        border: 1px solid ${allColors[color]};
        color: ${allColors[color]};
      `
      : `
        background: transparent;
        border: none;
        color: ${allColors[color]};
  `}
  
  @media (min-width: ${media.mobileMax}){
    height: 36px;
    max-width: 160px;
  }
`;
