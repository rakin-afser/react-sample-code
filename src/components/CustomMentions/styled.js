import {Mentions} from 'antd';
import styled from 'styled-components';

export const RelatedUser = styled.div`
  display: flex;
  align-items: center;
`;

export const RelatedAvatar = styled.img`
  border-radius: 50%;
  width: 36px;
  height: 36px;
  margin-right: 12px;
`;

export const RelatedName = styled.div``;

export const StyledMentions = styled(Mentions)`
  &&& {
    background-color: transparent;
    border: none;
    box-shadow: none;
    width: calc(100% - 34px);

    textarea {
      background-color: transparent;
    }
  }
`;
