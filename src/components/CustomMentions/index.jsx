import React from 'react';
import {StyledMentions, RelatedUser, RelatedAvatar, RelatedName} from './styled';
import userPlaceholder from 'images/placeholders/user.jpg';

const CustomMentions = (props) => {
  const {Option} = StyledMentions;
  const {relations, ...rest} = props;
  return (
    <StyledMentions {...rest}>
      {relations?.map(({id, username, name, avatar}) => (
        <Option key={id} value={username}>
          <RelatedUser>
            <RelatedAvatar src={avatar?.url || userPlaceholder} alt={username} />
            <RelatedName>{name}</RelatedName>
          </RelatedUser>
        </Option>
      ))}
    </StyledMentions>
  );
};

export default CustomMentions;
