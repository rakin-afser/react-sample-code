import React, {useState, useRef, useEffect} from 'react';
import Talk from 'talkjs';
import ContactsColumn from './components/ContactsColumn';
import Header from './components/Header';
import InputBlock from './components/InputBlock';
import ChatContent from './components/ChatContent';
import {Wrapper, MainBlock} from './styled';
import FadeOnMount from 'components/Transitions';

const Chat = () => {
  const [contact, setContact] = useState({});
  const [message, setMessage] = useState('');
  const [allMessages, setAllMessages] = useState(['hello', 'how are you']);
  const chatRef = useRef();

  const sendToChat = () => {
    setAllMessages((oldArray) => [...oldArray, message]);
    setMessage('');
  };

  useEffect(() => {
    Talk.ready.then(() => {
      setTimeout(() => {
        const inbox = window.talkJsSession.createInbox({showChatHeader: false});

        inbox.mount(chatRef.current);
      }, 300);
    });
  }, []);

  return (
    <FadeOnMount>
      <Wrapper ref={chatRef}>
        {/*<ContactsColumn selectContact={setContact} selectedContact={contact} />*/}
        {/*{Object.keys(contact).length ? (*/}
        {/*  <MainBlock>*/}
        {/*    <Header selectedContact={contact} />*/}
        {/*    <ChatContent data={allMessages} />*/}
        {/*    <InputBlock text={message} changeText={setMessage} sendMessage={sendToChat} />*/}
        {/*  </MainBlock>*/}
        {/*) : null}*/}
      </Wrapper>
    </FadeOnMount>
  );
};

export default Chat;
