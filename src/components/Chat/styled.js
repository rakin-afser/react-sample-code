import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 20px;
  margin: 0 0 0 32px;
  background: #ffffff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  width: 868px;
  height: calc(100vh - 106px);
  display: flex;
`;

export const MainBlock = styled.div`
  width: 568px;
`;
