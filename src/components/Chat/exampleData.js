import robertRoe from './images/robert_roe.png';
import margotRobbie from './images/margot_robbie.png';
import annaSmith from './images/anna_smith.png';
import annaSmith2 from './images/anna_smith2.png';
import annaSmith3 from './images/anna_smith3.png';
import annaSmith4 from './images/anna_smith4.png';
import annaSmith5 from './images/anna_smith5.png';
import annaSmith6 from './images/anna_smith6.png';
import annaSmith7 from './images/anna_smith7.png';
import annaSmith8 from './images/anna_smith8.png';
import annaSmith9 from './images/anna_smith9.png';

const data = [
  {
    avatar: annaSmith,
    username: 'anna_smith',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: annaSmith2,
    username: 'anna_smisath',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: robertRoe,
    username: 'robert_roe',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: annaSmith3,
    username: 'anna_sdamith',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: margotRobbie,
    username: 'margot_robbie',
    status: 'Currently working',
    time: '10:19',
    isOnline: true
  },
  {
    avatar: annaSmith4,
    username: 'an1asdna_smith',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: annaSmith5,
    username: 'anna_smith22',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: annaSmith6,
    username: 'anna_smith228',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: annaSmith7,
    username: 'anna_smithsaa',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: annaSmith8,
    username: 'aasdnna_smith',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  },
  {
    avatar: annaSmith9,
    username: 'annasda_smith',
    status: 'Currently working',
    time: '10:19',
    isOnline: false
  }
];

export default data;
