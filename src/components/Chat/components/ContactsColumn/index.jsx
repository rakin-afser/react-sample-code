import React, {useState} from 'react';
import Scrollbars from 'react-scrollbars-custom';
import Icon from 'components/Icon';
import data from '../../exampleData';

import {
  Wrapper,
  SearchBlock,
  InputWrapper,
  ContactItem,
  Time,
  UserInfo,
  ImgContainer,
  OnlineIcon,
  TextBlock
} from './styled';

const ContactsColumn = ({selectContact = (f) => f, selectedContact = {}}) => {
  const [availableContacts, setAvailableContacts] = useState(data);

  const searchHandler = (value) => {
    setAvailableContacts(data.filter((el) => el.username.includes(value)));
  };

  return (
    <Wrapper>
      <SearchBlock>
        <InputWrapper>
          <input type="text" onChange={(e) => searchHandler(e.target.value)} placeholder="Search in Messages…" />
          <Icon type="search" color="#7a7a7a" />
        </InputWrapper>
      </SearchBlock>
      <Scrollbars
        clientwidth={4}
        noDefaultStyles={false}
        noScroll={false}
        style={{height: 'calc(100% - 79px)'}}
        thumbYProps={{className: 'thumbY'}}
      >
        {availableContacts.map((el) => (
          <ContactItem
            isSelected={el.username === selectedContact.username}
            key={el.username}
            onClick={() => selectContact(el)}
          >
            <UserInfo>
              <ImgContainer>
                <img src={el.avatar} alt="username" />
                {el.isOnline && <OnlineIcon />}
              </ImgContainer>
              <TextBlock>
                <h3>{el.username}</h3>
                <p>{el.status}</p>
              </TextBlock>
            </UserInfo>
            <Time>{el.time}</Time>
          </ContactItem>
        ))}
      </Scrollbars>
    </Wrapper>
  );
};

export default ContactsColumn;
