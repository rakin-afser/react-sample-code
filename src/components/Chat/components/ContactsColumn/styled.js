import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 300px;
  height: 100%;
  border-right: 1px solid #e4e4e4;
  .ScrollbarsCustom-Wrapper {
    width: 100%;
  }
`;

export const SearchBlock = styled.div`
  padding: 20px 8px 19px 18px;
  border-bottom: 1px solid #e4e4e4;
  input {
    border: none;
    padding: 10px 18px 12px 49px;
    background: #f6f6f6;
    border-radius: 4px;
    color: #000;
    width: 100%;
    height: 39px;
  }
  i {
    position: absolute;
    left: 18px;
    top: 50%;
    transform: translateY(-50%);
  }
`;

export const InputWrapper = styled.div`
  position: relative;
`;

export const ContactItem = styled.div`
  border-bottom: 1px solid #e4e4e4;
  padding: 12px 12px 13px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
  ${({isSelected}) => (isSelected ? {backgroundColor: '#fafafa'} : null)}
  &:hover {
    background-color: #fafafa;
  }
`;

export const Time = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #8f8f8f;
  align-self: baseline;
`;

export const UserInfo = styled.div`
  display: flex;
`;

export const ImgContainer = styled.div`
  position: relative;
  margin-right: 26px;
  img {
    width: 40px;
    height: 40px;
  }
`;

export const OnlineIcon = styled.div`
  width: 9px;
  height: 9px;
  background-color: #2ecc71;
  border-radius: 50%;
  position: absolute;
  bottom: 2px;
  right: 0;
`;

export const TextBlock = styled.div`
  h3 {
    font-weight: 500;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    margin-bottom: 5px;
  }
  p {
    margin-bottom: 0;
    font-weight: normal;
    font-size: 12px;
    line-height: 132%;
    color: #8f8f8f;
  }
`;
