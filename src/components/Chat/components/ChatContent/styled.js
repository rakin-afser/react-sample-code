import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 32px 24px;
  /* height: 720px; */
  height: calc(100vh - 242px);
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  align-items: flex-end;
  overflow-y: auto;
`;

export const MyMessageWrapper = styled.span`
  background: #f4f4f4;
  border-radius: 20px;
  padding: 12px 16px;
  margin-bottom: 2px;
`;
