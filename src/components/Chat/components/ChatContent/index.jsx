import React from 'react';
import Icon from 'components/Icon';

import {Wrapper, MyMessageWrapper} from './styled';

const ChatContent = ({data = []}) => (
  <Wrapper>
    {data.map((el, i) => (
      <MyMessageWrapper>{el}</MyMessageWrapper>
    ))}
  </Wrapper>
);

export default ChatContent;
