import React from 'react';
import Icon from 'components/Icon';
import send from 'components/Comments/img/send.svg';
import AttachIcon from '../../images/attach.png';

import {Wrapper, ButtonsBlock, Send} from './styled';

const InputBlock = ({text = '', changeText = (f) => f, sendMessage = (f) => f}) => (
  <Wrapper>
    <input type="text" value={text} onChange={(e) => changeText(e.target.value)} placeholder="Type a message..." />

    <ButtonsBlock>
      <Icon type="shop" color="#7a7a7a" />
      <img src={AttachIcon} alt="attach" />
      <Send onClick={sendMessage}>
        <img src={send} alt="send" />
      </Send>
    </ButtonsBlock>
  </Wrapper>
);

export default InputBlock;
