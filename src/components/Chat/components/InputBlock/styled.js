import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 12px 24px;
  border-top: 1px solid #e4e4e4;
  display: flex;
  justify-content: space-between;
  input {
    border: none;
    outline: none;
    width: 100%;
    padding: 0;
  }
`;

export const ButtonsBlock = styled.div`
  display: flex;
  align-items: center;
  i,
  img {
    cursor: pointer;
  }
  i {
    margin-right: 16px;
  }
`;

export const Send = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 32px;
  height: 32px;
  border-radius: 50%;
  background: #ed494f;
  border: none;
  outline: none;
  cursor: pointer;
  margin-left: 26px;
`;
