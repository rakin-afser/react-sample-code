import React from 'react';
import Icon from 'components/Icon';

import {Wrapper, UserInfo, ImgContainer, OnlineIcon, TextBlock, ButtonsBlock} from './styled';

const Header = ({selectedContact = {}}) => (
  <Wrapper>
    <UserInfo>
      <ImgContainer>
        <img src={selectedContact.avatar} alt={selectedContact.username} />
        {selectedContact.isOnline && <OnlineIcon />}
      </ImgContainer>
      <TextBlock>
        <h3>{selectedContact.username}</h3>
        <p>{selectedContact.isOnline ? 'Online' : 'Offline'}</p>
      </TextBlock>
    </UserInfo>
    <ButtonsBlock>
      <Icon type="search" width="15px" height="15px" color="#7a7a7a" />
      <Icon type="more" width="15px" height="15px" color="#7a7a7a" />
    </ButtonsBlock>
  </Wrapper>
);

export default Header;
