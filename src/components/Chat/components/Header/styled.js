import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 12px 30px 11px 24px;
  border-bottom: 1px solid #e4e4e4;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const UserInfo = styled.div`
  display: flex;
`;

export const ImgContainer = styled.div`
  position: relative;
  margin-right: 7px;
  img {
    width: 55px;
    height: 55px;
  }
`;

export const OnlineIcon = styled.div`
  width: 13px;
  height: 13px;
  background-color: #2ecc71;
  border-radius: 50%;
  position: absolute;
  bottom: 3px;
  right: 0;
`;

export const TextBlock = styled.div`
  h3 {
    font-weight: 500;
    font-size: 18px;
    line-height: 140%;
    color: #000000;
    margin-bottom: 5px;
  }
  p {
    margin-bottom: 0;
    font-weight: normal;
    font-size: 14px;
    line-height: 132%;
    color: #8f8f8f;
  }
`;

export const ButtonsBlock = styled.div`
  i:first-child {
    margin-right: 25px;
  }
  i {
    cursor: pointer;
  }
`;
