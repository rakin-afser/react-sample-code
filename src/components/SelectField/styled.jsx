import styled from 'styled-components';

export const Wrapper = styled.div`
  position: relative;
  margin-bottom: 16px;
  color: #000;

  .input-placeholder {
    position: absolute;
    font-size: 1rem;
    top: 26px;
    transition: 0.1s all;
    z-index: 1;
    color: #ccc;

    &.placeholder-sm {
      font-size: 14px;
    }
  }

  .ant-select {
    box-sizing: border-box;
    border-radius: 2px;
    width: 100% !important;
    height: 40px;
    background: #fff;
    border: 1px solid #a7a7a7;

    &-open {
      .ant-select {
        &-selection {
          box-shadow: none;
        }
      }
    }

    &-arrow {
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      right: 22px;
    }

    &-search--inline {
      position: absolute;
      top: 0;
      left: -1px;
      right: -1px;
    }
    &-arrow {
      top: calc(50% + 6px);
    }

    &-selection {
      border: 0;
      position: relative;
      top: 1px;
      height: 38px;
      box-shadow: none;
      &__rendered {
        height: 38px;
        line-height: 38px;
        padding: 0 15px;
        margin: -1px 0 0 0;
        position: relative;
      }

      &__placeholder {
        left: 15px;
      }
    }

    &:-webkit-autofill,
    &:-webkit-autofill-strong-password {
      background: lighten(#4a90e2, 39%);
    }

    &:hover {
      background: #fff;
      border-color: #a7a7a7;
    }

    &:focus,
    &:active {
      background: #fff;
      border-color: #4a90e2;
    }

    &:disabled {
      background: #e4e4e4;
      border-color: #a7a7a7;

      &:hover,
      &:active,
      &:focus {
        background: #e4e4e4;
        border-color: #a7a7a7;
      }
    }

    input {
      height: 38px;
      padding: 0 15px;
      width: 100%;
      box-shadow: none;
    }
  }

  &.error {
    input,
    .ant-select {
      border-color: #ed484f;
    }
  }
  label {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 140%;
    color: #464646;
    margin-bottom: 6px;
    display: block;
  }

  ${({loading}) =>
    loading &&
    `
    .ant-select {
      &-arrow {
        opacity: 0;
      }
    }
  `}

  ${({disabled}) =>
    disabled &&
    `
    .ant-select-disabled .ant-select-selection  {
      background: #E4E4E4;

        &:hover,
        &:active,
        &:focus {
          background: #E4E4E4;
        }
    }

    label {
      cursor: not-allowed;
    }

    .ant-select {
      border-color: #A7A7A7;

      &-selection {
        background: #E4E4E4;

        &:hover,
        &:active,
        &:focus {
          background: #E4E4E4;
        }
      }

      &:hover,
      &:active,
      &:focus {
        background: #E4E4E4;
        border-color: #A7A7A7;
      }
    }
  `}
`;
