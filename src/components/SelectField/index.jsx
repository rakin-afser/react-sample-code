import React from 'react';
import {Select} from 'antd';

import ErrorMessage from 'components/ErrorMessage';

import {IconPreloader, IconDropdown} from 'helpers/icons';

import {Wrapper} from './styled';

const {Option} = Select;

const SelectField = ({
  placeholder,
  className,
  onChange,
  onFocus,
  onBlur,
  onSearch,
  placeholderSm,
  name,
  error,
  status,
  id,
  type = 'text',
  options = [],
  loading,
  label,
  required,
  children,
  disabled,
                       defaultValue,
  ...optionals

                     }) => (
  <Wrapper className={`${error ? 'error' : ''} ${className || ''}`} loading={loading} disabled={disabled}>
    {label ? (
      <>
        <label htmlFor={id}>
          {label} {required ? <span style={{color: '#ED484F'}}>*</span> : null}
        </label>
      </>
    ) : null}
    <Select
      showSearch
      style={{width: 200}}
      placeholder={placeholder}
      optionFilterProp="children"
      onChange={onChange}
      onFocus={onFocus}
      onBlur={onBlur}
      onSearch={onSearch}
      suffixIcon={<IconDropdown />}
      disabled={disabled}
      defaultValue={defaultValue}
      filterOption={(input, option) =>
        option.props.children
          .toString()
          .toLowerCase()
          .indexOf(input.toString().toLowerCase()) >= 0
      }
      {...optionals}
    >
      {options &&
        options.map((option) => (
          <Option key={option.value} value={option.value}>
            {option.text}
          </Option>
        ))}
      {children}
    </Select>
    {loading ? (
      <IconPreloader
        width={16}
        height={16}
        style={{
          position: 'absolute',
          top: 35,
          right: 19,
          width: 20,
          height: 20
        }}
      />
    ) : null}
    {error ? <ErrorMessage>{error}</ErrorMessage> : status ? <ErrorMessage>{status}</ErrorMessage> : null}
  </Wrapper>
);

export default SelectField;
