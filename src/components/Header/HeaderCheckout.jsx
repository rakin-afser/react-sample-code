import React from 'react';

import {HeaderCheckoutWrapper, HeaderCheckoutContainer, LogoContainerCheckout} from './styled';
import LogoIcon from '../../assets/LogoIcon';

const HeaderCheckout = () => {
  return (
    <HeaderCheckoutWrapper>
      <HeaderCheckoutContainer>
        <LogoContainerCheckout href="/">
          <LogoIcon />
        </LogoContainerCheckout>
      </HeaderCheckoutContainer>
    </HeaderCheckoutWrapper>
  );
};

export default HeaderCheckout;
