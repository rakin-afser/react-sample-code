import styled, {css} from 'styled-components/macro';
import {Link} from 'react-router-dom';
import {Button, Badge} from 'antd';
import {FlexContainer} from 'globalStyles';
import media from 'constants/media';
import {mainWhiteColor, mainBlackColor, primaryColor, headerShadowColor, midCoinsColor} from 'constants/colors';
import {headerHeight, mobileHeaderHeight} from './constants';

export const HeaderWrapper = styled.div`
  position: relative;
  top: 0;
  z-index: 1000;
  background: ${mainWhiteColor};
  box-shadow: inset 0px -1px 0px ${headerShadowColor};

  @media (max-width: ${media.mobileMax}) {
    transform: translateY(${({visible}) => (visible ? 0 : '-100%')});
    transition: transform 0.3s ease;
    position: ${({sticky}) => (sticky ? 'fixed' : 'relative')};
    left: 0;
    right: 0;
  }
`;

export const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  height: ${({isMobile}) => (isMobile ? mobileHeaderHeight : headerHeight)};
  margin: 0 auto;
  padding: 0 70px;
  max-width: 1440px;

  @media (max-width: 1360px) {
    padding: 0 30px;
  }

  @media (max-width: ${media.mobileMax}) {
    padding: 0 16px;
    background: ${mainWhiteColor};
    box-shadow: ${({shadow}) => (shadow ? '0 2px 10px rgba(0, 0, 0, 0.1)' : 'none')};
    justify-content: space-between;
    ${({size}) => size && `height: ${size}px`};

    span:only-of-type {
      //margin-left: auto;
    }

    ${({isSearchNearBasket}) =>
      isSearchNearBasket &&
      `
      justify-content: flex-end;

      span ~ span {
        margin-left: 23px;
      }
    `}
  }
`;

export const SellButton = styled(Button)`
  &&& {
    font-family: SF Pro Display;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    height: 30px;
    color: ${mainBlackColor};
    font-weight: 700;
    background: ${mainWhiteColor};
    font-size: 14px;
    padding: 0 14px 0 14px;
    line-height: 20px;
    border: 1px solid #c3c3c3;
    border-radius: 24px;
    margin-right: 35px;

    & svg {
      margin-right: 5px;

      path {
        transition: ease 0.4s;
      }
    }

    &:hover {
      color: #fff;
      background: ${primaryColor};

      svg {
        path {
          fill: #fff;
        }
      }
    }

    @media (max-width: 1250px) {
      display: none;
    }
  }
`;

export const AvatarContainer = styled(FlexContainer)`
  max-width: 150px;
  height: 40px;
  margin: 0 18px 0 0;
  cursor: pointer;

  img {
    min-width: 32px;
    height: 32px;
    border-radius: 50%;
  }

  @media (max-width: 1150px) {
    display: none;
  }
`;

export const StyledName = styled.div`
  font-weight: bold;
  white-space: nowrap;
  width: 70px;
  overflow: hidden;
  text-overflow: ellipsis;
  transition: ease 0.4s;
`;

export const StyledHi = styled.div`
  transition: ease 0.4s;
`;

export const Name = styled.div`
  font-size: 14px;
  line-height: 17px;
  color: #000;
  cursor: pointer;

  &:hover {
    ${StyledName} {
      color: ${midCoinsColor};
    }

    ${StyledHi} {
      color: ${midCoinsColor};
    }
  }
`;

export const BarControls = styled.div`
  display: flex;
  align-items: center;
  margin-left: ${({isMobile}) => (isMobile ? 'auto' : 'auto')};
`;

export const LeftButton = styled.button`
  position: ${({isMobile}) => (isMobile ? 'absolute' : 'relative')};
  left: ${({isMobile}) => (isMobile ? '14px' : '0')};
  z-index: 100;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0;
  margin: ${({isMobile}) => (isMobile ? '0 26px 0 0' : '0 22px 0 0')};
  background: none;
  border: none;
  cursor: pointer;
  outline: none;
`;

export const HeaderTitle = styled.div`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #000;
  position: absolute;
  top: auto;
  max-width: 75%;
  left: 50%;
  transform: translateX(-50%);
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const LogoContainer = styled(Link)`
  width: 99px;
  padding-top: 6px;
  margin-right: 64px;
  margin-left: 3px;

  & svg {
    max-width: none;
  }

  @media (max-width: 900px) {
    margin-right: 25px;
  }

  ${({isMobile}) =>
    isMobile &&
    `
    margin: 0;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 82px;
    min-width: 82px;

    & svg {
      max-width: 82px;
    }
  `}
`;

export const LinksContainer = styled.div`
  display: flex;
  align-items: center;
  letter-spacing: 0.6px;

  ${({mobile, hideLinks}) =>
    hideLinks
      ? `display: none`
      : mobile
      ? `
      position: absolute;
      top: 48px;
      left: 0;
      width: 100%;
      background: #fff;
      z-index: 1;
      & a {
        width: 50%;
        display: flex;
        justify-content: center;
        align-items: center;
        height: 40px;
        margin: 0 !important;
      }
    `
      : null};
  ${({hide}) => hide && 'top: -100%'};

  a {
    padding: 5px 0 8px;
    cursor: pointer;
    color: #999;
    border: none;
    font-size: 14px;
    letter-spacing: 0;
    font-style: normal;
    font-weight: bold;
    line-height: 140%;

    &.active {
      color: ${mainBlackColor};
      padding-bottom: 8px;
      border-bottom: 2px solid ${primaryColor};
    }

    @media (max-width: 767px) {
      ${({active}) => (active ? null : 'border-bottom: 2px solid #EFEFEF;')};
      padding: 8px 0 4px;

      a {
        line-height: 1.4;
      }
    }

    &:not(:last-child) {
      margin-right: 42px;
    }

    &:hover {
      color: ${mainBlackColor};
      padding-bottom: 1px;
      border-bottom: 2px solid ${primaryColor};
    }
  }

  @media (max-width: 900px) {
    a {
      &:not(:last-child) {
        margin-right: 22px;
      }
    }
  }
`;

export const BadgesContainer = styled(FlexContainer)`
  position: relative;

  span {
    cursor: pointer;
  }

  .ant-badge-count {
    width: 16px;
    height: 16px;
    font-size: 12px;
    line-height: 16px;
    border-radius: 50%;
    padding: 0;
    min-width: 16px;
  }

  button:nth-of-type(2) {
    margin-right: 28px;
  }
`;

export const CustomBadge = styled(Badge)`
  ${({ismobile}) =>
    ismobile &&
    css`
      // width: 48px;
      height: 24px;
      display: flex !important;
      align-items: center;
      justify-content: center;
    `}
`;

export const AuthControls = styled.div`
  position: relative;
  left: 2px;
  display: flex;
  align-items: center;
  margin: 0 47px 0 0;
`;

export const AuthSign = styled.div`
  font-size: 14px;
  line-height: 19px;
  text-decoration: none;
  outline: none;
  color: #000;
  cursor: pointer;
`;

export const AuthCreate = styled(AuthSign)`
  color: #ed494f;
  font-weight: 700;
  line-height: 20px;
`;

export const AuthDivider = styled.i`
  width: 2px;
  height: 24px;
  background: #e4e4e4;
  border-radius: 10px;
  margin: 0 16px;
`;

export const HeaderCheckoutWrapper = styled.div`
  background: #ffffff;
  box-shadow: inset 0px -1px 0px #e4e4e4;
  height: 64px;
`;

export const HeaderCheckoutContainer = styled.div`
  max-width: 1200px;
  margin: auto;
`;

export const LogoContainerCheckout = styled.a`
  & svg {
    max-width: 114px;
    margin: 16px 0px;
  }
`;

export const RightWrapperMobile = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`;

export const LanguageSelectorWrapper = styled.div`
  margin-left: 8px;
`;

export const SearchWrapper = styled.div`
  & svg {
    margin-top: 5px;
  }
`;
