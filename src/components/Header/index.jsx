import React from 'react';

import HeaderDesktop from './HeaderDesktop';
import HeaderMobile from './HeaderMobile';

const Header = ({
  size,
  isMobile,
  hideLinks,
  hideHeaderLinks,
  isFooterShort,
  showBurgerButton,
  showRingButton,
  showSearchButton,
  showBasketButton,
  showBackButton,
  title,
  sticky,
  shadow,
  sellerHeader,
  customRightIcon
}) => {
  return isMobile ? (
    <HeaderMobile
      size={size}
      isMobile={isMobile}
      hideLinks={hideLinks}
      showBurgerButton={showBurgerButton}
      showRingButton={showRingButton}
      showSearchButton={showSearchButton}
      showBasketButton={showBasketButton}
      showBackButton={showBackButton}
      title={title}
      sticky={sticky}
      shadow={shadow}
      sellerHeader={sellerHeader}
      customRightIcon={customRightIcon}
    />
  ) : (
    <HeaderDesktop sellerHeader={sellerHeader} />
  );
};

export default Header;
