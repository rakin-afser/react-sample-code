import React, {useState} from 'react';
import Div100vh from 'react-div-100vh';
import {useHistory} from 'react-router-dom';
import FadeOnMount from 'components/Transitions';
import {useApolloClient} from '@apollo/client';

import {
  Wrapper,
  WrapperInner,
  Overlay,
  Content,
  Banner,
  BannerTitle,
  BannerInfo,
  AuthBlock,
  LogoutContainer,
  LogoutButton
} from './styled';
import {MenuItem, MenuLink, Arrow, ButtonNow} from 'components/Header/components/Menu/styled';
import Menu from 'components/Header/components/Menu';
import UserBlock from 'components/Header/components/UserBlock';
import BlackCircle from 'components/Header/components/BlackCircle';
import Footer from 'components/Header/components/Footer';
import ReferEarn from 'components/Header/components/ReferEarn';
import banner from 'images/hamburgerMenu/banner.png';
import Divider from 'components/Divider';
import {Signin, Signup} from 'components/AuthRequest/styled';
import useGlobal from 'store';
import Icon from 'components/Icon';
import {useUser} from 'hooks/reactiveVars';
import CountryDropdown from 'components/Header/components/CountryDropdown';

const Hamburger = ({showHamburger, setShowHamburger}) => {
  const [_, {setIsRequireAuth, setAuthPopup}] = useGlobal();
  const [currentTab, setCurrentTab] = useState(null);
  const [user] = useUser();
  const {push} = useHistory();
  const client = useApolloClient();

  const handleLogout = async (e) => {
    e.preventDefault();
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    localStorage.removeItem('woo-session');
    client.resetStore();
    window.location.href = '/';
  };

  const handleClickSignUp = () => {
    setAuthPopup(true);
    setIsRequireAuth(false);
    onClose();
    push('/auth/sign-up');
  };

  const handleClickSignIn = () => {
    setAuthPopup(true);
    setIsRequireAuth(false);
    onClose();
    push('/auth/sign-in');
  };

  const onClose = () => {
    setShowHamburger(false);
    setTimeout(() => setCurrentTab(null), 600);
  };

  return (
    <Wrapper>
      <Overlay active={showHamburger} onClick={onClose} />
      <WrapperInner active={showHamburger}>
        <Div100vh
          style={{
            maxHeight: '100rvh'
          }}
        >
          <Content>
            {currentTab === 'refer-earn' && (
              <FadeOnMount transition="all ease 0.3s" style={{flexGrow: '1'}}>
                <ReferEarn />
              </FadeOnMount>
            )}
            {!currentTab && (
              <>
                <UserBlock />
                <Menu />
                {user?.databaseId ? (
                  <>
                    <MenuItem>
                      <Icon type="categoriesNew" width={22} height={22} />
                      <MenuLink to="/categories">Categories</MenuLink>
                      <Arrow>
                        <span
                          style={{
                            paddingRight: 16,
                            position: 'relative',
                            top: -1,
                            color: '#8F8F8F',
                            pointerEvents: 'none'
                          }}
                        >
                          See All
                        </span>
                        <Icon type="arrow" color="#000" height={14} width={12} />
                      </Arrow>
                    </MenuItem>
                    <MenuItem color="#FAFAFA">
                      <MenuLink
                        to="/categories/bags-shoes"
                        color="#464646"
                        style={{fontWeight: 'normal', paddingLeft: 32}}
                      >
                        Bags & Shoes
                      </MenuLink>
                      <Arrow>
                        <Icon type="arrow" color="#CCCCCC" height={14} width={12} />
                      </Arrow>
                    </MenuItem>
                    <MenuItem color="#FAFAFA">
                      <MenuLink to="/categories/beauty" color="#464646" style={{fontWeight: 'normal', paddingLeft: 32}}>
                        Beauty
                      </MenuLink>
                      <Arrow>
                        <Icon type="arrow" color="#CCCCCC" height={14} width={12} />
                      </Arrow>
                    </MenuItem>
                    <Divider style={{height: 40, minHeight: 40, background: '#FAFAFA'}} />
                    <MenuItem color="#fff" borderTop onClick={() => setCurrentTab('refer-earn')}>
                      <Icon type="coinsProfile" width={21} height={21} />
                      <MenuLink>Invite Friends & Earn Midcoins</MenuLink>
                      <Arrow>
                        <Icon type="arrow" color="#000" height={14} width={12} />
                      </Arrow>
                    </MenuItem>
                    <MenuItem color="#fff">
                      <Icon type="shopNew" />
                      <MenuLink>Start a Shop</MenuLink>
                      <ButtonNow href="https://seller.testSample.com/">
                        Sell Now <Icon type="arrow" color="currentColor" height={8} width={5} />
                      </ButtonNow>
                    </MenuItem>
                    {/* <MenuItem color="#fff">
                      <Icon type="coinsProfile" width={21} height={21} />
                      <MenuLink style={{ color: '#464646' }}>Leave a Review & Unlock Coins</MenuLink>
                      <Arrow>
                        <Icon type="arrow" color="#000" height={14} width={12} />
                      </Arrow>
                    </MenuItem> */}
                  </>
                ) : (
                  <>
                    <Banner>
                      <BannerTitle>Shop & Earn Rewards</BannerTitle>
                      <img src={banner} alt="" />
                      <BlackCircle />
                    </Banner>
                    <BannerInfo>Sign up & Get 5% midcoins on your 1st purchase</BannerInfo>
                    <AuthBlock>
                      <Signin className="ant-btn ant-btn-primary ant-btn-round" onClick={handleClickSignIn}>
                        Sign In
                      </Signin>
                      <Signup className="ant-btn ant-btn-round" onClick={handleClickSignUp}>
                        Create Account
                      </Signup>
                    </AuthBlock>
                  </>
                )}
              </>
            )}
            {user?.email && currentTab !== 'refer-earn' ? (
              <LogoutContainer onClick={handleLogout}>
                <Icon type="logout" fill="#828282" />
                <LogoutButton>Log Out</LogoutButton>
              </LogoutContainer>
            ) : (
              <MenuItem onClick={() => console.log(123)}>
                <Icon type="shopNew" width={24} height={24} />
                <MenuLink>Start a Shop</MenuLink>
                <ButtonNow href="https://seller.testSample.com/">
                  Now <Icon type="arrow" color="currentColor" height={8} width={5} />
                </ButtonNow>
              </MenuItem>
            )}
            <CountryDropdown color={user?.email && currentTab !== 'refer-earn' ? '#fff' : '#fff'} />
            <Footer />
          </Content>
        </Div100vh>
      </WrapperInner>
    </Wrapper>
  );
};

export default Hamburger;
