import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1100;
  pointer-events: none;
`;

export const Overlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1100;
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
  background: rgba(0, 0, 0, 0.6);
  transition: all 0.3s ease;
  opacity: ${({active}) => (active ? 1 : 0)};
`;

export const Content = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1100;
  background: #fff;
  display: flex;
  flex-direction: column;
`;

export const WrapperInner = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 64px;
  bottom: 0;
  z-index: 1100;
  background: #fff;
  overflow-x: hidden;
  overflow-y: auto;
  transition: all 0.6s ease;
  transform: ${({active}) => (active ? 'translateX(0)' : 'translateX(-100%)')};
  pointer-events: ${({active}) => (active ? 'all' : 'none')};
`;

export const BannerTitle = styled.div`
  font-style: normal;
  font-weight: 800;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  position: absolute;
  top: 15px;
  left: 16px;
  max-width: 187px;
`;

export const BannerInfo = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  line-height: 140%;
  color: #000000;
  padding: 4px 0 0 9px;
`;

export const Banner = styled.div`
  position: relative;

  & img {
    width: 100%;
    height: 149px;
    object-fit: cover;
    margin-top: 5px;
  }
`;

export const AuthBlock = styled.div`
  flex-grow: 1;
  padding: 32px 0 19px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Country = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  color: #666666;
  font-size: 12px;
  margin: 0 10px;
`;

export const Divider = styled.div`
  flex-shrink: 0;
  width: 100%;
  height: ${({height}) => height || '10px'};
  background-color: ${({color}) => color || '#FAFAFA'};
`;

export const LogoutContainer = styled.div`
  background: #ffffff;
  padding: 0 16px;
  display: flex;
  align-items: center;
  height: 53px;
  min-height: 53px;
  border-top: 1px solid #efefef;
  border-bottom: 1px solid #efefef;
  margin-top: 22px;
  box-shadow: 0 0 0 50px #fafafa;
`;

export const LogoutButton = styled.button`
  background: #ffffff;
  border: 0;
  border-radius: 0;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  font-weight: 500;
  color: #8f8f8f;
  padding: 0;
  margin-left: 16px;
`;
