import React from 'react';
import {Link} from 'react-router-dom';
import {useTranslation} from 'react-i18next';

import {Wrapper, Links, CopyrightText} from './styled';

const Footer = () => {
  const {t} = useTranslation();

  return (
    <Wrapper>
      <Links>
        <Link to='/privacy-policy'>Privacy Policy</Link>
        <Link to='/terms-conditions'>Terms of Use</Link>
        <Link to='/about/faqs'>Help</Link>
      </Links>
      <CopyrightText>{t('footer.copyright')}</CopyrightText>
    </Wrapper>
  );
};

export default Footer;
