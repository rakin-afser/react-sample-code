import styled from 'styled-components/macro';

export const Wrapper = styled.div``;

export const Links = styled.div`
  padding: 25px 18px 3px 7px;
  display: flex;

  & a {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    color: #7a7a7a;
    flex-grow: 1;
    text-align: center;
    position: relative;

    &:not(:last-child):after {
      content: '';
      position: absolute;
      width: 4px;
      height: 4px;
      background: #7a7a7a;
      border-radius: 50%;
      top: 6px;
      right: -2px;
    }

    &:hover {
      color: #7a7a7a;
    }
  }
`;

export const CopyrightText = styled.div`
  background: #efefef;
  padding: 12.5px 20px;
  text-align: center;
  font-size: 10px;
`;
