import styled from 'styled-components/macro';

export const Circle = styled.div`
  position: absolute;
  background: rgba(0, 0, 0, 0.67);
  color: #fff;
  border-radius: 50%;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 65px;
  height: 65px;
  right: 12px;
  top: 44px;
`;

export const FirstText = styled.div`
  margin: 2px 0;
`;

export const Number = styled.div`
  font-size: 49px;
  line-height: 90%;
  display: flex;
  margin-left: 0px;
`;

export const Percentage = styled.span`
  font-size: 23px;
  line-height: 130%;
`;

export const LastText = styled.div`
  max-width: 60%;
  text-align: center;
  line-height: 130%;
`;
