import React from 'react';

import {Circle, Number, Percentage} from './styled';

const BlackCircle = () => {
  return (
    <Circle>
      <Number>
        <span>5</span>
        <Percentage>%</Percentage>
      </Number>
    </Circle>
  );
};

export default BlackCircle;
