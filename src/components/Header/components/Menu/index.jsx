import React from 'react';

import {Wrapper, BigItem, ItemsBlock, BigItemTitle} from './styled';
import Icon from 'components/Icon';
import {useUser} from 'hooks/reactiveVars';
import GuestView from 'components/Header/components/Menu/GuestView';
import Cart from 'components/Counters/Cart';
import Orders from 'components/Counters/Orders';
import Coins from 'components/Counters/Coins';

const Menu = () => {
  const [user] = useUser();

  return user?.databaseId ? (
    <Wrapper color="#FAFAFA" style={{flexGrow: '1'}}>
      <ItemsBlock>
        <BigItem to="/profile/orders">
          <Orders />
          <BigItemTitle>My Orders</BigItemTitle>
        </BigItem>

        <BigItem to="/cart">
          <Cart>
            <Icon type="boldCart" color="#464646" width={24} height={24} />
          </Cart>
          <BigItemTitle style={{paddingTop: '2px'}}>My Cart</BigItemTitle>
        </BigItem>

        <BigItem to="/profile/lists/wishlist">
          <Icon type="starOutlined" color="#464646" width={22} height={22} />
          <BigItemTitle>My Wishlist</BigItemTitle>
        </BigItem>
      </ItemsBlock>

      <ItemsBlock>
        <BigItem to="/account-settings/addresses">
          <Icon type="boldHouse" color="#464646" width={22} height={22} />
          <BigItemTitle>My Address</BigItemTitle>
        </BigItem>

        <BigItem to="/profile/rewards">
          <Coins />
          <BigItemTitle>My Rewards</BigItemTitle>
        </BigItem>

        <BigItem to="/profile/settings">
          <Icon type="gearThin" color="#464646" width={22} height={22} />
          <BigItemTitle>Settings</BigItemTitle>
        </BigItem>
      </ItemsBlock>
    </Wrapper>
  ) : (
    <GuestView />
  );
};

export default Menu;
