import styled, {css} from 'styled-components/macro';
import {Link} from 'react-router-dom';

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
  padding: 3px 0 36px;
  background-color: ${({color}) => color || '#fff'};
`;

// TODO: need switch 'a' to Link from 'react-router-dom'
export const CustomLink = styled.a`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: #000000;

  &:not(:last-child) {
    margin-bottom: 23px;
  }
`;

export const MenuItem = styled.div`
  padding: 15px 18px;
  height: 53px;
  display: flex;
  background-color: ${({color}) => color || '#fff'};
  align-items: center;
  border-bottom: 1px solid #efefef;
  position: relative;

  i > svg {
    max-width: 21px;
  }

  ${({borderTop}) =>
    borderTop &&
    css`
      border-top: 1px solid #efefef;
    `}
`;

export const MenuLink = styled(Link)`
  font-family: Helvetica Neue;
  flex-grow: 1;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  padding: 0 11px;
  color: ${({color}) => color || '#000000'};

  &:hover {
    color: #000000;
  }
`;

export const DefaultLink = styled.a`
  font-family: Helvetica Neue;
  flex-grow: 1;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  padding: 0 11px;
  color: ${({color}) => color || '#000000'};

  &:hover {
    color: #000000;
  }
`;

export const Arrow = styled.div`
  & i {
    position: absolute;
    right: 14px;
    top: 50%;
    transform: translateY(-50%);
  }
`;

export const CartWrapper = styled.div`
  padding: 6px 18px 0 18px;
  display: flex;
  border-left: 1px solid #efefef;
`;

export const ButtonNow = styled.a`
  border: 1px solid currentColor;
  border-radius: 24px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  color: #ed484f;
  padding: 4px 10px 4px 13px;
  flex-shrink: 0;
  margin-right: -8px;

  & i {
    margin-left: 5px;
  }
`;

export const ItemsBlock = styled.div`
  padding: 27px 13px 0;
  display: flex;
  align-items: center;
  justify-content: space-around;

  &:first-child {
    border-top: 1px solid #efefef;
  }
`;

export const BigItem = styled(Link)`
  padding: 12px 13px 33px;
  width: 76px;
  height: 76px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #ffffff;
  box-shadow: -2px -2px 4px rgba(0, 0, 0, 0.07), 2px 2px 4px rgba(0, 0, 0, 0.03);
  border-radius: 8px;
  position: relative;
`;

export const BigItemTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 11px;
  text-align: center;
  padding-top: 6px;
  text-align: center;
  white-space: nowrap;
  color: #464646;
  position: absolute;
  bottom: 9px;
  left: 2px;
  right: 2px;
`;
