import React from 'react';

import {Wrapper, MenuItem, MenuLink, ButtonNow, CartWrapper, Arrow} from './styled';
import Icon from 'components/Icon';
import Cart from 'components/Counters/Cart';

const GuestView = () => {
  return (
    <Wrapper>
      {/* <MenuItem onClick={() => console.log(123)}>
        <Icon type="coinsProfile" width={24} height={24} />
        <MenuLink>Shop & Earn Rewards</MenuLink>
        <CartWrapper>
          <Cart badgeColor="#999" />
        </CartWrapper>
      </MenuItem> */}
      <MenuItem>
        <Icon type="shopHamburger" width={21} height={20} />
        <MenuLink to="/">Shop</MenuLink>
        <Arrow>
          <Icon type="arrow" color="#000" height={14} width={12} />
        </Arrow>
      </MenuItem>
      <MenuItem>
        <Icon type="feedNew" width={24} height={24} />
        <MenuLink to="/feed">Feed</MenuLink>
        <Arrow>
          <Icon type="arrow" color="#000" height={14} width={12} />
        </Arrow>
      </MenuItem>
      <MenuItem>
        <Icon type="categoriesNew" width={22} height={22} />
        <MenuLink to="/categories">Categories</MenuLink>
        <Arrow>
          <Icon type="arrow" color="#000" height={14} width={12} />
        </Arrow>
      </MenuItem>
      <MenuItem color="#FAFAFA">
        <MenuLink to="/categories/bags-shoes" color="#464646" style={{ fontWeight: 'normal', paddingLeft: 32 }}>Bags & Shoes</MenuLink>
        <Arrow>
          <Icon type="arrow" color="#CCCCCC" height={14} width={12} />
        </Arrow>
      </MenuItem>
      <MenuItem color="#FAFAFA">
        <MenuLink to="/categories/beauty" color="#464646" style={{ fontWeight: 'normal', paddingLeft: 32 }}>Beauty</MenuLink>
        <Arrow>
          <Icon type="arrow" color="#CCCCCC" height={14} width={12} />
        </Arrow>
      </MenuItem>
    </Wrapper>
  );
};

export default GuestView;
