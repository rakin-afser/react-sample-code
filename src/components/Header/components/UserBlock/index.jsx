import React from 'react';
import {useHistory} from 'react-router-dom';

import {Wrapper, SimpleText, SignIn, UserName, UserInfo, Avatar, CreateAccount} from './styled';
import {useUser} from 'hooks/reactiveVars';
import userPlaceholder from 'images/placeholders/user.jpg';
import Icon from 'components/Icon';

const UserBlock = () => {
  const [user] = useUser();
  const {push} = useHistory();

  const goProfile = () => {
    push('/profile/posts');
  };

  return user?.databaseId ? (
    <Wrapper background="#FAFAFA" onClick={goProfile}>
      <Avatar>
        <img src={user?.avatar?.url || userPlaceholder} alt="" />
      </Avatar>
      <UserInfo>
        <SimpleText>Hello,</SimpleText>
        <UserName>{user?.name}</UserName>
        <Icon type="arrow" color="#000" height={14} width={12} />
      </UserInfo>
    </Wrapper>
  ) : (
    <Wrapper background="#fff" unlogined>
      <SimpleText unlogined>Hello</SimpleText>
      <SignIn to="/auth/sign-in">Sign In</SignIn>
      <CreateAccount to="/auth/sign-up">Create Account</CreateAccount>
    </Wrapper>
  );
};

export default UserBlock;
