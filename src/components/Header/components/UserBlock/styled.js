import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';

export const Wrapper = styled.div`
  padding: 18px;
  display: flex;
  background-color: ${({background}) => background};

  ${({unlogined}) => {
    if (unlogined) {
      return `
        display: flex;
        align-items: center;
        flex-wrap: wrap;
        background-color: #FAFAFA;
        padding: 34px 18px 20px;
      `;
    }

    return ``;
  }}
`;

const Text = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-size: 14px;
  font-weight: bold;
  line-height: 17px;
`;

const TextLink = styled(Link)`
  font-family: Helvetica Neue;
  font-style: normal;
  font-size: 14px;
  font-weight: bold;
  line-height: 17px;
`;

export const SimpleText = styled(Text)`
  font-weight: normal;
  color: #999999;

  ${({unlogined}) => {
    if (unlogined) {
      return `
        margin-right: auto;
      `;
    }

    return ``;
  }}
`;

export const SignIn = styled(TextLink)`
  display: inline-block;
  padding-right: 13px;
  position: relative;
  top: -2px;
  border-right: 1px solid #cccccc;
  color: #000000;
`;

export const CreateAccount = styled(TextLink)`
  display: inline-block;
  padding-left: 13px;
  padding-right: 25px;
  position: relative;
  top: -2px;
  color: #ed484f;
}
`;

export const Avatar = styled.div`
  border: 1px solid #efefef;
  border-radius: 50%;
  overflow: hidden;
  width: 34px;
  height: 34px;
  display: inline-block;
  flex-shrink: 0;

  & img {
    width: 100%;
    height: 100%;
  }
`;

export const UserInfo = styled.div`
  display: inline-block;
  padding-left: 12px;
  padding-top: 2px;
  position: relative;
  flex-grow: 1;

  & i {
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
  }
`;

export const UserName = styled(Text)`
  color: #000000;
  font-weight: 500;
`;
