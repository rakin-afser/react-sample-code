import styled, {css} from 'styled-components/macro';

export const Wrapper = styled.div`
  flex-grow: 1;
  padding: 30px 15px;
`;

export const Title = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  color: #000000;
  margin-bottom: 19px;
`;

export const Description = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #000000;
  margin-bottom: 35px;
`;

export const TextAreaWrapper = styled.div`
  margin: 0 auto 10px;
  max-width: 300px;
`;

export const CopyButton = styled.div`
  border-radius: 100px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #000;
  padding: 11px 16px;
  max-width: 300px;
  text-align: center;
  margin: 0 auto 10px;
  background: #c4c4c4;
  transition: all 0.8s;

  & span {
    display: flex;
    justify-content: center;
    margin-right: 20px;

    & i {
      margin-right: 16px;
    }
  }

  ${({copied}) =>
    copied &&
    css`
      background: #000000;
      color: #ffffff;
    `}
`;

export const ShareBlock = styled.div`
  margin: 30px 0 0;
`;

export const ShareTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  color: #000000;
  margin-bottom: 14px;
`;

export const ShareIcons = styled.div`
  display: flex;

  & i {
    margin-right: 27px;

    &:last-child {
      margin: 0 0 0 auto;
    }
  }
`;

export const LinkTextArea = styled.textarea`
  background: #ffffff;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  border-radius: 100px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #000000;
  padding: 11px 16px 11px 16px;
  margin: 0 auto 10px;
  max-width: 300px;
  width: 100%;
  overflow: auto;
  outline: none;
  box-shadow: none;
  resize: none;
`;
