import React, {useState, useRef} from 'react';

import {
  Wrapper,
  LinkTextArea,
  Title,
  Description,
  TextAreaWrapper,
  CopyButton,
  ShareBlock,
  ShareTitle,
  ShareIcons
} from './styled';
import Icon from 'components/Icon';
import {useUser} from 'hooks/reactiveVars';
import {useAffiliateLinkQuery} from 'hooks/queries';

const ReferEarn = () => {
  const [isCopied, setIsCopied] = useState(false);
  const textAreaRef = useRef(null);

  const [user] = useUser();
  const {data} = useAffiliateLinkQuery({variables: {id: user?.databaseId}});
  const {affiliateLink} = data?.user || {};

  const copyLink = (e) => {
    textAreaRef.current.select();
    document.execCommand('copy');
    e.target.focus();
    setIsCopied(true);
  };

  return (
    <Wrapper>
      <Title>Refer & Earn</Title>
      <Description>
        Share your <b>unique link</b> by facebook, twitter, instagram, whatsapp or email. Earn midcoins when each friend
        who signs up with your referral link makes a purchase.
      </Description>
      <TextAreaWrapper>
        <LinkTextArea rows={1} readOnly ref={textAreaRef} value={`${window.location.host}/ref/${affiliateLink}`} />
      </TextAreaWrapper>

      <CopyButton copied={isCopied} onClick={(e) => copyLink(e)}>
        {isCopied ? (
          'Copied'
        ) : (
          <span>
            <Icon type="copy" />
            Copy Link
          </span>
        )}
      </CopyButton>
      <ShareBlock>
        <ShareTitle>Share</ShareTitle>
        <ShareIcons>
          <Icon type="twitter" width={27} height={27} />
          <Icon type="instagram" fill="#000" width={27} height={27} />
          <Icon type="facebook" fill="#000" width={27} height={27} />
          <Icon type="whatsapp" width={27} height={27} />
          <Icon type="boldShareIcon" />
        </ShareIcons>
      </ShareBlock>
    </Wrapper>
  );
};

export default ReferEarn;
