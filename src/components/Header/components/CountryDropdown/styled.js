import styled, {css} from 'styled-components/macro';

export const Country = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  color: #000;
  font-size: 11px;
  position: absolute;
  left: 41px;
  bottom: 4px;
  margin: 0 10px;
`;

export const ItemsWrapper = styled.div``;

export const CountryItem = styled.div`
  padding: 10px 20px;
  display: flex;
  background-color: ${({color}) => color || '#fff'};
  align-items: center;
  border-bottom: 1px solid #efefef;
  position: relative;
  transition: background-color 0.3s;

  & img {
    margin-right: 15px;
    width: 20px;
    height: auto;
    max-height: 21px;
  }

  & span {
    font-family: Helvetica Neue;
    flex-grow: 1;
    font-size: 14px;
    line-height: 17px;
    color: #000;
  }

  ${({selected}) =>
    selected &&
    css`
      background-color: #f7f7f7;
    `}

  &.fade-enter {
    opacity: 0;
    transform: translateX(-${({percent}) => percent}%);
  }

  &.fade-enter-active {
    opacity: 1;
    transform: translateX(0);
    transition: opacity 0.8s, transform 0.8s;
  }

  &.fade-enter-done {
    opacity: 1;
    transform: translateX(0);
    transition: opacity 600ms, transform 600ms;
  }

  &.fade-exit {
    opacity: 1;
  }

  &.fade-exit-active {
    opacity: 0;
    transform: translateX(-${({percent}) => percent}%);
    transition: opacity 600ms, transform 600ms;
  }

  &.fade-exit-done {
    opacity: 0;
    transition: opacity 0.8s, transform 0.8s;
  }
`;
