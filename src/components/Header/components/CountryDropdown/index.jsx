import React, {useState} from 'react';
import Scrollbar from 'components/Scrollbar';
import {CSSTransition} from 'react-transition-group';

import {Country, CountryItem, ItemsWrapper} from './styled';
import {MenuItem, MenuLink, Arrow} from 'components/Header/components/Menu/styled';
import Icon from 'components/Icon';
import {countries} from 'components/Header/constants';
import Checkmark from 'assets/Checkmark';

const CountryDropdown = ({onChange = () => {}, color = '#fafafa'}) => {
  const [isOpen, setIsOpen] = useState(null);
  const [selected, setSelected] = useState('BH');

  const onCountryClick = (countryCode) => {
    setSelected(countryCode);
    onChange(countryCode);
    setIsOpen(false);
  };

  return (
    <>
      <MenuItem color={color} onClick={() => setIsOpen(!isOpen)}>
        <Icon type="car" />
        <MenuLink style={{paddingBottom: 10}}>Ship to</MenuLink>
        <Country>{countries.find((i) => i.id === selected)?.title || 'None'}</Country>
        <Arrow>
          <Icon type="arrow" color="#000" height={12} width={14} />
        </Arrow>
      </MenuItem>

      <ItemsWrapper isOpen={isOpen}>
        <Scrollbar autoHeight autoHeightMax={400}>
          {countries?.map((item, index) => (
            <CSSTransition key={item?.id} classNames="fade" in={isOpen} timeout={400} unmountOnExit>
              <CountryItem
                percent={100 * (index + 1)}
                selected={item.id === selected}
                key={item?.id}
                onClick={() => onCountryClick(item?.id)}
              >
                <img src={item.img} alt={item.title} />
                <span>{item?.title}</span>
                {item.id === selected && <Checkmark width={20} />}
              </CountryItem>
            </CSSTransition>
          ))}
        </Scrollbar>
      </ItemsWrapper>
    </>
  );
};

export default CountryDropdown;
