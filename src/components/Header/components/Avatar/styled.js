import styled from 'styled-components';

export const UserPhoto = styled.img.attrs(({styles, src}) => ({style: {...styles}, src: src}))`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  flex-shrink: 0;
`;

export const FakePhoto = styled.div.attrs(({styles}) => ({style: {...styles}}))`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  color: #fff;
  display: flex;
  flex-shrink: 0;
  justify-content: center;
  align-items: center;
  background-color: ${({bgColor}) => bgColor};
`;
