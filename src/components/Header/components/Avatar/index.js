import React, {useCallback, useMemo} from 'react';
import {object, string} from 'prop-types';
import {FakePhoto, UserPhoto} from './styled';
import {
  primaryColor,
  blueColor,
  starFillColor,
  midCoinsColor,
  blue,
  secondaryColor
} from '../../../../constants/colors';

const colors = [primaryColor, blueColor, starFillColor, midCoinsColor, blue, secondaryColor];

const Avatar = ({src, name, styles}) => {
  const generateColor = () => {
    const randomIndex = Math.floor(Math.random() * 5);
    return colors[randomIndex];
  };

  const getRandomColor = useMemo(() => {
    return generateColor();
  }, []);

  return src ? (
    <UserPhoto src={src} alt="avatar" styles={styles} />
  ) : (
    <FakePhoto styles={styles} bgColor={getRandomColor}>
      <span>{name?.charAt(0)}</span>
    </FakePhoto>
  );
};

Avatar.propTypes = {
  src: string,
  name: string,
  styles: object
};

export default Avatar;
