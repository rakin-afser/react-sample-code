import styled, {keyframes} from 'styled-components/macro';
import {headerShadowColor, primaryColor, secondaryTextColor} from 'constants/colors';

import {headerHeight, mobileHeaderHeight} from '../constants';

const open = keyframes`
  from {
    opacity: 0;
  } 
  to {
    opacity: 1;
  }
`;

const close = keyframes`
  from {
    opacity: 1;
  } 
  to {
    opacity: 0;
  }
`;

export const MenuOuterWrapper = styled.div`
  position: absolute;
  top: ${({isMobile}) => (isMobile ? mobileHeaderHeight : headerHeight)};
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  overflow: hidden;
  background: rgba(0, 0, 0, 0.4);
  z-index: 1024;
  cursor: default;
  opacity: 0;
  left: 0;

  &.sideMenu-enter-done {
    animation: ${open} 0.5s forwards;
  }
  &.sideMenu-exit-active {
    animation: ${close} 0.5s forwards;
  }
`;

export const MenuInnerWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 1440px;
`;

export const MenuContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: fit-content;
`;

export const MenuGroup = styled.div`
  position: relative;
  padding: 16px 0;
  border-bottom: 1px solid ${headerShadowColor};
  width: 240px;

  ${(p) =>
    p.second &&
    `
    background-color: #fff;
    border-bottom: none;
    transition: all ease 0.5s;
    border-radius: ${p.isRounded ? '0 8px 8px 0' : '0'};
    opacity: ${p.isOpen ? '1' : '0'};
    width: ${p.isOpen ? '240px' : '0'};
    overflow: hidden;
  `}

  ${(p) =>
    p.third &&
    `
    background-color: #f5f5f5;
    border-bottom: none;
    border-radius: 0 8px 8px 0;
    transition: all ease 0.4s;
    opacity: ${p.isOpen ? '1' : '0'};
    width: ${p.isOpen ? '172px' : '0'};
    overflow: hidden;
  `}
`;

export const MenuItem = styled.div`
  cursor: pointer;
  color: ${secondaryTextColor};
  padding: 8px 9.5px 8px 16px;
  font-weight: 400;
  font-size: 14px;
  transition: all ease 0.4s;
  display: flex;
  justify-content: space-between;
  align-items: center;
  white-space: pre-wrap;

  & svg {
    flex-shrink: 0;

    path {
      transition: all ease 0.3s;
      fill: #DDDDDD;
    }
  }

  ${(p) =>
    p.second &&
    `
    padding: 8px 16.5px 8px 24px;
  `}

  ${(p) =>
    p.third &&
    `
    font-size: 12px;
    color: #414141;
    padding: 4px 10px 4px 28px;
    position: relative;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    display: block;
    width: 100%;

    &::after{
      content: '';
      position: absolute;
      height: 2px;
      background-color: ${primaryColor};
      transition: width ease 0.3s;
      width: 0;
      left: 28px;
      bottom: 0;
    }
    &:hover,
    &:active {
      &::after{
        width: 20%;
      }
    }
  `}

  ${(p) =>
    p.withoutChildren &&
    `
    &:hover{
      background-color: #f5f5f5;
      color: #000;
    }
  `}

  ${({selected}) =>
    selected &&
    `
    background-color: #f5f5f5;
    color: #000;
    // font-weight: 500;

    & svg {
      path {
        fill: ${primaryColor};
      }
    }
  `}
`;

export const LeftSide = styled.div`
  margin: 4px 0 0 5px;
  background-color: #fff;
  display: flex;
  flex-direction: column;
  min-width: 240px;
  transition: border-radius ease 0.4s;
  border-radius: ${(p) => (p.isRounded ? '8px' : '8px 0 0 8px')};
`;

export const RightSide = styled.div`
  display: flex;
  margin: 4px 0 0 1px;
  max-height: 100%;
`;

export const CTABlock = styled.div`
  padding: 16px 16px 16px 19px;
  display: flex;
  max-width: 240px;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;

  & svg {
    flex-shrink: 0;
    width: 100px;
  }
`;

export const CTATitle = styled.div`
  width: 100%;
  margin-bottom: 16.5px;
  font-weight: 700;
  font-size: 14px;
  color: ${secondaryTextColor};
`;

export const CTAButton = styled.button`
  min-width: 86px;
  max-width: 50%;
  padding: 5px 9px;
  font-weight: 700;
  font-size: 10px;
  color: #fff;
  background-color: ${primaryColor};
  border-radius: 16.2px;
  font-family: Helvetica Neue;
  position: relative;
  overflow: hidden;
  border: 1px solid ${primaryColor};
  transition: all ease 0.6s;

  &:hover {
    color: ${primaryColor};
    background-color: #fff;
  }
`;
