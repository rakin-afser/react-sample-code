export const defaultCategories = [
  {
    id: '1',
    href: '#',
    name: 'Clothing',
    children: [
      {
        id: '1-1',
        name: 'Women’s Clothing',
        href: '#',
        children: [
          {
            id: '1-1-1',
            href: '#',
            name: 'Dresses'
          },
          {
            id: '1-1-2',
            href: '#',
            name: 'Tops'
          }
        ]
      },
      {
        id: '1-2',
        name: 'Men’s Clothing',
        href: '#',
        children: [
          {
            id: '1-2-1',
            name: 'Shirts',
            href: '#'
          },
          {
            id: '1-2-2',
            name: 'Suits & Dresswear',
            href: '#'
          },
          {
            id: '1-2-3',
            name: 'Pajamas & Robes',
            href: '#'
          }
        ]
      },
      {
        id: '1-3',
        name: 'Lingerie',
        href: '#',
        children: [
          {
            id: '1-3-1',
            href: '#',
            name: 'Thongs & Knickerssexy'
          },
          {
            id: '1-3-2',
            href: '#',
            name: 'Lingeriebackless'
          }
        ]
      }
    ]
  },
  {
    id: '2',
    href: '#',
    name: 'Perfumes & Beauty',
    children: [
      {
        id: '2-1',
        name: 'Swimwear',
        href: '#',
        children: [
          {
            id: '2-1-1',
            href: '#',
            name: 'Bikini Sets'
          },
          {
            id: '2-1-2',
            href: '#',
            name: 'One-Piece Suits'
          }
        ]
      },
      {
        id: '2-2',
        name: 'Maternity',
        href: '#',
        children: [
          {
            id: '2-2-1',
            href: '#',
            name: 'Maternity Wear'
          },
          {
            id: '2-2-2',
            href: '#',
            name: 'Maternity Underwear'
          }
        ]
      }
    ]
  },
  {id: '3', href: '#', name: 'Bags & Accessories', children: []},
  {id: '4', href: '#', name: 'Jewelry & Watches', children: []},
  {id: '5', href: '#', name: 'Electronics', children: []},
  {id: '6', href: '#', name: 'Shoes', children: []},
  {id: '7', href: '#', name: 'Home', children: []},
  {id: '8', href: '#', name: 'Toys & Teens', children: []},
  {id: '9', href: '#', name: 'Art & Crafts', children: []}
];
