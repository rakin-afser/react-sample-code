import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

import {
  LeftSide,
  MenuInnerWrapper,
  MenuOuterWrapper,
  RightSide,
  MenuItem,
  CTABlock,
  CTATitle,
  CTAButton,
  MenuContainer,
  MenuGroup
} from './styled';
import Arrow from '../../../assets/Arrow';
import LogoIcon from '../../../assets/LogoIcon';
import {useQuery} from '@apollo/client';
import {NAV_CATEGORIES} from 'queries';

export const SideMenu = ({isOpen, setIsMenuOpened}) => {
  const [selectedMainCategory, setSelectedMainCategory] = useState(null);
  const [selectedSubCategory, setSelectedSubCategory] = useState(null);
  let timeout;

  const {data} = useQuery(NAV_CATEGORIES);

  useEffect(() => {
    setSelectedSubCategory(null);
  }, [selectedMainCategory]);

  const menuOpen = () => {
    clearTimeout(timeout);
    setIsMenuOpened(true);
  };

  const menuClose = () => {
    if (isOpen) {
      // delay before menu is closed
      timeout = setTimeout(() => setIsMenuOpened(false), 1000);
    }
  };

  return (
    <MenuOuterWrapper isOpen={isOpen}>
      <MenuInnerWrapper>
        <MenuContainer onMouseLeave={menuClose} onMouseOver={menuOpen}>
          <LeftSide isRounded={!selectedMainCategory}>
            <MenuGroup>
              {data?.productCategories?.nodes.map((item) => (
                
                  <Link to={`/categories/${selectedMainCategory?.slug}`}>
                    <MenuItem
                      key={item.id}
                      onMouseOver={() => setSelectedMainCategory(item)}
                      selected={selectedMainCategory?.id === item.id}
                    >
                      {item.name}
                      <Arrow height={13} />
                    </MenuItem>
                  </Link>
              ))}
            </MenuGroup>
            <Block title="Enjoy Shopping" buttonText="Shop Now !" />
          </LeftSide>
          <RightSide>
            <MenuGroup second isOpen={!!selectedMainCategory} isRounded={!selectedSubCategory}>
              {selectedMainCategory?.children?.nodes?.length ? (
                selectedMainCategory?.children?.nodes?.map((item) => (
                  <Link to={`/categories/${selectedMainCategory?.slug}/${selectedSubCategory?.slug}`}>
                    <MenuItem
                      second
                      key={item.id}
                      onMouseOver={() => setSelectedSubCategory(item)}
                      selected={selectedSubCategory?.id === item.id}
                    >
                    
                      {item.name}
                      <Arrow height={13} />
                    </MenuItem>
                  </Link>
                ))
              ) : (
                <MenuItem
                  second
                  withoutChildren
                  onClick={() => console.log(selectedMainCategory?.id, selectedMainCategory?.href)}
                >
                  All {selectedMainCategory?.name}
                </MenuItem>
              )}
            </MenuGroup>
            <MenuGroup third isOpen={!!selectedSubCategory}>
              {selectedSubCategory?.children?.nodes?.map((item) => (
                <Link key={item.id} to={`/categories/${selectedMainCategory?.slug}/${selectedSubCategory?.slug}/${item?.slug}`}>
                    <MenuItem third>
                      {item.name}
                    </MenuItem>
                  </Link>
              ))}
            </MenuGroup>
          </RightSide>
        </MenuContainer>
      </MenuInnerWrapper>
    </MenuOuterWrapper>
  );
};

const Block = ({title, buttonText}) => (
  <CTABlock>
    <CTATitle>{title}</CTATitle>
    <LogoIcon />
    <CTAButton>{buttonText}</CTAButton>
  </CTABlock>
);
