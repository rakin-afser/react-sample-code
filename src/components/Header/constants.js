import bahrain from 'components/CountrySelector/images/bahrain.svg';
import kuwait from 'components/CountrySelector/images/kuwait.svg';
import oman from 'components/CountrySelector/images/oman.svg';
import qatar from 'components/CountrySelector/images/qatar.svg';
import saudiArabia from 'components/CountrySelector/images/saudi-arabia.svg';
import uae from 'components/CountrySelector/images/united-arab-emirates.svg';

export const headerHeight = '72px';
export const mobileHeaderHeight = '47px';
export const sideMenuWidth = '240px';

export const mainFont = "'Helvetica Neue', sans-serif";
export const secondaryFont = "'SF Pro Display', sans-serif";

export const countries = [
  {id: 'BH', title: 'Bahrain', img: bahrain},
  {id: 'AE', title: 'UAE', img: uae},
  {id: 'SA', title: 'Saudi Arabia', img: saudiArabia},
  {id: 'KW', title: 'Kuwait', img: kuwait},
  {id: 'OM', title: 'Oman', img: oman},
  {id: 'QA', title: 'Qatar', img: qatar}
];
