import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import {useQuery} from '@apollo/client';

import useGlobal from 'store';
import SelectLanguage from 'components/SelectLanguage';
import Hamburger from './components/Hamburger';
import MessagesPopup from './components/Messages/Popup';
import NotificationsPopup from './components/Notifications/Popup';
import ArrowBack from 'assets/ArrowBack';
import LogoIcon from 'assets/LogoIcon';
import Search from 'assets/Search';
import ShoppingCart from 'assets/ShoppingCart';
import {
  CustomBadge,
  HeaderContainer,
  HeaderTitle,
  HeaderWrapper,
  LogoContainer,
  LeftButton,
  RightWrapperMobile,
  LanguageSelectorWrapper,
  SearchWrapper
} from './styled';
import {CART_QUANTITY} from 'queries';
import Icon from 'components/Icon';

const Header = ({
  size,
  isMobile,
  hideLinks,
  showBurgerButton,
  showSearchButton,
  showBasketButton,
  title,
  sticky,
  shadow,
  showBackButton,
  sellerHeader,
  customRightIcon
}) => {
  const history = useHistory();
  const [globalState] = useGlobal();
  const [showHamburger, setShowHamburger] = useState(false);
  const [tab, setTab] = useState(0);
  const [me, setMe] = useState(null);
  const [visible, setVisible] = useState(true);
  const [showMessages, setShowMessages] = useState(false);
  const [showNotifications, setShowNotifications] = useState(false);
  const [subCategoryActive, setSubCategoryActive] = useState(null);
  const {data: dataCart} = useQuery(CART_QUANTITY);
  const {totalQuantity} = dataCart?.cart || {};

  useEffect(() => {
    if (showHamburger || showNotifications || showMessages) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden');
    }
  }, [showHamburger, showNotifications, showMessages]);

  useEffect(() => {
    let prevScrollpos = window.pageYOffset;
    function onScroll() {
      const currentScrollPos = window.pageYOffset;
      if (window.scrollY < 50 || prevScrollpos > currentScrollPos) {
        setVisible(true);
      } else {
        setVisible(false);
      }
      prevScrollpos = currentScrollPos;
    }
    window.addEventListener('scroll', onScroll);

    return () => window.removeEventListener('scroll', onScroll);
  }, []);

  return (
    <>
      <HeaderWrapper visible={visible} sticky={sticky}>
        <HeaderContainer isMobile={isMobile} shadow={shadow} isSearchNearBasket={showBackButton}>
          {showBackButton && (
            <LeftButton isMobile={isMobile} onClick={() => history.push(globalState.headerBackButtonLink)}>
              <ArrowBack stroke="#000" />
            </LeftButton>
          )}
          {!sellerHeader && (
            <CustomBadge count={0} ismobile={isMobile.toString()}>
              {showBurgerButton && (
                <Icon
                  type="burgerIcon"
                  color="#000"
                  onClick={() => setShowHamburger(true)}
                  style={{marginRight: '20px'}}
                />
              )}
              {showSearchButton && (
                <SearchWrapper onClick={() => history.push('/search/all')}>
                  <Search />
                </SearchWrapper>
              )}
            </CustomBadge>
          )}
          {title ? (
            <HeaderTitle>{title}</HeaderTitle>
          ) : (
            <LogoContainer
              to="/"
              isMobile={isMobile}
              onClick={() => {
                setShowNotifications(false);
                setShowMessages(false);
              }}
            >
              <LogoIcon />
            </LogoContainer>
          )}

          <RightWrapperMobile>
            {customRightIcon}
            {!sellerHeader && showBasketButton && (
              <CustomBadge
                offset={[-3, 3]}
                count={totalQuantity}
                ismobile={isMobile.toString()}
                onClick={() => {
                  history.push('/cart'); // temp
                  // setShowHamburger(true);
                  // setTab(me ? 3 : 1);
                  // setShowNotifications(false);
                  // setShowMessages(false);
                }}
              >
                <ShoppingCart />
              </CustomBadge>
            )}
            {sellerHeader && (
              <LanguageSelectorWrapper>
                <SelectLanguage />
              </LanguageSelectorWrapper>
            )}
          </RightWrapperMobile>
        </HeaderContainer>
      </HeaderWrapper>
      <Hamburger
        me={me}
        setMe={setMe}
        showHamburger={showHamburger}
        setShowHamburger={setShowHamburger}
        setShowNotifications={setShowNotifications}
        setShowMessages={setShowMessages}
        tab={tab}
        setTab={setTab}
        subCategoryActive={subCategoryActive}
        setSubCategoryActive={setSubCategoryActive}
      />
      <NotificationsPopup
        showNotifications={showNotifications}
        setShowHamburger={setShowHamburger}
        setShowNotifications={setShowNotifications}
      />
      <MessagesPopup
        showMessages={showMessages}
        setShowHamburger={setShowHamburger}
        setShowMessages={setShowMessages}
      />
    </>
  );
};

Header.defaultProps = {
  isMobile: true,
  hideLinks: false,
  showBurgerButton: true,
  showSearchButton: true,
  showBackButton: false,
  showBasketButton: true,
  title: '',
  sticky: true,
  shadow: true,
  sellerHeader: false
};

Header.propTypes = {
  isMobile: PropTypes.bool,
  hideLinks: PropTypes.bool,
  showSearchButton: PropTypes.bool,
  showBackButton: PropTypes.bool,
  showBasketButton: PropTypes.bool,
  title: PropTypes.string,
  sticky: PropTypes.bool,
  shadow: PropTypes.bool,
  sellerHeader: PropTypes.bool
};

export default Header;
