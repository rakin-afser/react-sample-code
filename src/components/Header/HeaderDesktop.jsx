import React, {useEffect, useState, useRef} from 'react';
import {NavLink, useHistory} from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';
import LogoIcon from 'assets/LogoIcon';
import SearchInput from 'containers/HomePage/components/SearchInput';
import BoxIcon from 'assets/BoxIcon';
// import BurgerIcon from 'assets/BurgerIcon';
import SelectLanguage from 'components/SelectLanguage';
import Avatar from './components/Avatar';
import NotificationBar from '../NotificationBar';
import {SideMenu} from './SideMenu/SideMenu';
import Badge from '../Badges';
import audio from 'audio/click.wav';
import pusherJs from 'services/pusher';

import {
  HeaderContainer,
  SellButton,
  AvatarContainer,
  StyledName,
  StyledHi,
  BadgesContainer,
  LogoContainer,
  LinksContainer,
  HeaderWrapper,
  Name,
  LeftButton as Burger,
  BarControls,
  AuthControls,
  AuthCreate,
  AuthSign,
  AuthDivider
} from './styled';
import BurgerIcon from '../../assets/BurgerIcon';
import useGlobal from 'store';
import {useQuery} from '@apollo/client';
import {CART_QUANTITY, NAV_CATEGORIES} from 'queries';
import {token, userId} from 'util/heplers';
import {useUser} from 'hooks/reactiveVars';

const Header = ({sellerHeader}) => {
  const {push, location} = useHistory();
  const [globalState, globalActions] = useGlobal();
  const [isBarOpened, setIsBarOpened] = useState(false);
  const [isMenuOpened, setIsMenuOpened] = useState(false);
  const [barContentType, setBarContentType] = useState(null);
  const [unreadMessages, setUnreadMessages] = useState(null);
  const [unreadNotifications, setUnreadNotifications] = useState(null);
  const audioRef = useRef();

  if (window.talkJsSession) {
    // console.log(window.talkJsSession.unreads.K.length);

    window.talkJsSession.unreads.on('change', (unreadConversations) => {
      const unreadCount = unreadConversations.length;
      setUnreadMessages(unreadCount);
    });

    window.talkJsSession.on('message', (unreadConversations) => {
      if (!unreadConversations.isByMe) {
        audioRef.current.play();
      }
    });
  }

  const {data: dataCart} = useQuery(CART_QUANTITY);
  const [user] = useUser();

  let badges = [
    {type: 'notifications', counter: unreadNotifications || ''},
    {type: 'message', counter: unreadMessages || ''},
    {type: 'cart', counter: parseInt(dataCart?.cart?.totalQuantity || 0)}
  ];
  badges = user?.email ? badges : badges.slice(2, 3);

  useEffect(() => {
    pusherJs.bind('my-channel', 'my-event', () => {
      setUnreadNotifications(unreadNotifications ? unreadNotifications + 1 : 1);
    });
    return () =>
      pusherJs.unbind('my-channel', 'my-event', () => {
        setUnreadNotifications(unreadNotifications ? unreadNotifications + 1 : 1);
      });
  }, [unreadNotifications]);

  const controlsClick = (type) => {
    if (isBarOpened && type === barContentType) {
      setIsBarOpened(false);
    } else {
      setBarContentType(type);
      setIsMenuOpened(false);
      setIsBarOpened(true);
    }
  };

  const handleClickSignUp = () => {
    globalActions.setAuthPopup(true);
    push('/auth/sign-up');
  };

  const handleClickSignIn = () => {
    globalActions.setAuthPopup(true);
    push('/auth/sign-in');
  };

  function renderSellButton() {
    const href = user
      ? `https://seller.testSample.net/sign-in?token=${token}&userId=${userId}`
      : 'https://seller.testSample.net/sign-in';

    if (user?.capabilities?.includes('seller')) {
      return (
        <SellButton
          onClick={() => {
            window.location.href = href;
          }}
        >
          <BoxIcon />
          My Store
        </SellButton>
      );
    }

    return (
      <SellButton
        onClick={() => {
          window.location.href = href;
        }}
      >
        <BoxIcon />
        SELL
      </SellButton>
    );
  }

  function renderLinks() {
    if (sellerHeader) return null;
    if (location?.pathname === '/') {
      return (
        <LinksContainer>
          <NavLink exact to="/feed">
            Feed
          </NavLink>
        </LinksContainer>
      );
    }

    return (
      <LinksContainer>
        <NavLink exact to="/">
          Marketplace
        </NavLink>
      </LinksContainer>
    );
  }

  return (
    <HeaderWrapper>
      <HeaderContainer>
        <audio ref={audioRef} style={{display: 'none'}}>
          <source src={audio} type="audio/wav" />
        </audio>

        {!sellerHeader && (
          <>
            <Burger onMouseOver={() => setIsMenuOpened(true)} onClick={() => isMenuOpened && setIsMenuOpened(false)}>
              <BurgerIcon />
            </Burger>
            <CSSTransition in={isMenuOpened} timeout={200} classNames="sideMenu" unmountOnExit>
              <SideMenu isOpen={isMenuOpened} setIsMenuOpened={setIsMenuOpened} />
            </CSSTransition>
          </>
        )}
        <LogoContainer to="/">
          <LogoIcon />
        </LogoContainer>
        {renderLinks()}
        <SearchInput />
        {!sellerHeader && renderSellButton()}
        {!sellerHeader && (
          <BarControls className="barControls">
            {user?.email ? (
              <>
                <AvatarContainer onClick={() => controlsClick('profile')}>
                  {user.avatar ? <Avatar src={user?.avatar?.url} /> : <Avatar name={user?.firstName} />}
                </AvatarContainer>
                <Name onClick={() => controlsClick('profile')} flexDirection="column">
                  <>
                    <StyledHi>Hi,</StyledHi>
                    <StyledName>{user?.firstName}</StyledName>
                  </>
                </Name>
              </>
            ) : (
              <AuthControls>
                <AuthSign onClick={handleClickSignIn}>Sign In</AuthSign>
                <AuthDivider />
                <AuthCreate onClick={handleClickSignUp}>Create Account</AuthCreate>
              </AuthControls>
            )}

            <BadgesContainer className="badgesContainer">
              {badges?.map((item) => (
                <Badge
                  key={item.type}
                  selected={barContentType === item.type && isBarOpened}
                  action={() => controlsClick(item.type)}
                  {...item}
                />
              ))}
            </BadgesContainer>
          </BarControls>
        )}
        {sellerHeader && (
          <div style={{marginLeft: 'auto'}}>
            <SelectLanguage />
          </div>
        )}
      </HeaderContainer>

      <CSSTransition in={isBarOpened} timeout={500} classNames="notificationBar" unmountOnExit>
        <NotificationBar contentType={barContentType} closeHandler={() => setIsBarOpened(false)} />
      </CSSTransition>
    </HeaderWrapper>
  );
};

export default Header;
