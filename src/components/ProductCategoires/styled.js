import styled from 'styled-components/macro';

export const Wrap = styled.div`
  max-width: 300px;
  width: 100%;
  padding: 24px 24px 52px;
  margin-top: 20px;
  background: #ffffff;
  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.06);
  border-radius: 4px;
`;

export const Title = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 140%;
  padding-bottom: 11px;
  color: #000;
  position: relative;

  &::after {
    content: '';
    background-color: #e4e4e4;
    height: 1px;
    position: absolute;
    bottom: 0;
    left: -24px;
    right: -24px;
  }
`;

export const StyledCategory = styled.div`
  font-size: 16px;
  line-height: 140%;
  margin: 12px 0;
  color: #464646;
  font-weight: normal;
`;

export const CategoryName = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  color: #000000;
  cursor: pointer;
  ${({active}) => (active ? 'font-weight: 500' : 'null')};
`;

export const CategoryQuantity = styled.span`
  color: #656565;
`;

export const Categories = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const SubCategoryWrapper = styled.div`
  font-size: 14px;
  line-height: 140%;
  margin: 8px 0 0 16px;
  color: #464646;
  font-weight: normal;
`;
