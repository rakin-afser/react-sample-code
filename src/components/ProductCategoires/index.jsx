import React from 'react';
import {useParams} from 'react-router-dom';

import PriceSelect from 'components/PriceSelect';
import {Wrap, Title, StyledCategory, CategoryQuantity, Categories, CategoryName, SubCategoryWrapper} from './styled';

const ProductCategoires = ({categories = [], currentCategory, goToCategory = () => {}, filtersValue, setFilters=()=>{}}) => {
  const {shopName, page, categoryName, subCategoryName, subCategoryName2} = useParams();

  const getSubCategory = (parentId) => categories?.filter((item) => item?.parent === parentId);

  const totalProducts = () =>
    categories?.filter((item) => item?.parent === '0')?.reduce((count, item) => count + item?.productIds?.length, 0);

  return (
    <Wrap>
      <Title>Categories</Title>
      <Categories>
        <StyledCategory>
          <CategoryName onClick={() => goToCategory('', 0)} active={!currentCategory}>
            View All
          </CategoryName>{' '}
          <CategoryQuantity>({totalProducts()})</CategoryQuantity>
        </StyledCategory>
        {categories
          ?.filter((item) => item?.parent === '0')
          ?.map((item) => (
            <StyledCategory key={item?.slug}>
              <CategoryName onClick={() => goToCategory(item?.slug, 1)} active={item?.slug === currentCategory}>
                {item?.name?.replace('&amp;', '&')}
              </CategoryName>{' '}
              <CategoryQuantity>({item?.productIds?.length})</CategoryQuantity>
              {categoryName === item?.slug &&
                getSubCategory(item?.id)?.map((subItem) => (
                  <SubCategoryWrapper>
                    <CategoryName
                      onClick={() => goToCategory(subItem?.slug, 2)}
                      active={subItem?.slug === currentCategory}
                    >
                      {subItem?.name?.replace('&amp;', '&')}
                    </CategoryName>{' '}
                    <CategoryQuantity>({subItem?.productIds?.length})</CategoryQuantity>
                    {subCategoryName === subItem?.slug &&
                      getSubCategory(subItem?.id)?.map((subItem2) => (
                        <SubCategoryWrapper>
                          <CategoryName
                            onClick={() => goToCategory(subItem2?.slug, 3)}
                            active={subItem2?.slug === currentCategory}
                          >
                            {subItem2?.name?.replace('&amp;', '&')}
                          </CategoryName>{' '}
                          <CategoryQuantity>({subItem2?.productIds?.length})</CategoryQuantity>
                        </SubCategoryWrapper>
                      ))}
                  </SubCategoryWrapper>
                ))}
            </StyledCategory>
          ))}
      </Categories>
      <PriceSelect title="Price" onChange={(value) => setFilters({price: value})} currentPrice={filtersValue?.price}/>
    </Wrap>
  );
};

export default ProductCategoires;
