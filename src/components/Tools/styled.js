import styled from 'styled-components/macro';
import media from 'constants/media';

export const Wrapper = styled.div`
  background: rgba(228, 228, 228, 0.2);
  mix-blend-mode: normal;
  border: 1px solid rgba(167, 167, 167, 0.2);
  box-sizing: border-box;
  border-radius: 19px;
  padding: 3px 28px 3px 18px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  max-width: 334px;
  margin-left: auto;
  margin-bottom: ${({marginBottom}) => marginBottom || '38px'};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  letter-spacing: -0.256px;
  color: #000;

  @media (max-width: ${media.mobileMax}) {
    padding: 7px 28px;
    margin-bottom: 0;
    order: 2;
    max-width: 330px;
    width: 100%;
    font-size: 16px;
  }
`;

export const Country = styled.div`
  display: inline-flex;
  flex-wrap: wrap;
  align-items: center;

  span {
    margin-left: 10px;
  }

  svg {
    path {
      fill: #000;
    }
  }
`;

export const Divider = styled.div`
  display: inline-flex;
  flex-wrap: wrap;
  align-items: center;
  width: 1px;
  height: 21px;
  background: #c3c3c3;
  border-radius: 10px;
`;

export const Currency = styled.div``;
