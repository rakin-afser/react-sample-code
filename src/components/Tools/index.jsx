import React from 'react';
import SelectLanguage from 'components/SelectLanguage';
import Marker from 'assets/Marker';
import {Wrapper, Country, Divider, Currency} from './styled';
import CountrySelector from 'components/CountrySelector';

const Tools = () => {
  return (
    // <Wrapper>
    //   <Country>
    //     <Marker />
    //     <span>Ukraine</span>
    //   </Country>
    //   <Divider />
    //   <SelectLanguage />
    //   <Divider />
    //   <Currency>$ (USD)</Currency>
    // </Wrapper>
    <CountrySelector />
  );
};

export default Tools;
