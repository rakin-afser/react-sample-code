import styled from 'styled-components';

export const Container = styled.div`
  padding: 16px 16px 21px 16px;
`;

export const Title = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  margin-bottom: 20px;
  color: #000;
`;

export const Cash = styled.span`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  font-size: 9px;
  line-height: 12.5px;
  color: #000;
`;

export const Description = styled.div`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 16.7px;
  color: #000;
  margin-bottom: 15px;
`;

export const Icons = styled.div`
  display: flex;
  align-items: center;

  & svg {
    margin-right: 25px;
  }
`;
