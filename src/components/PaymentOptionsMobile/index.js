import React from 'react';
import {Container, Title, Icons, Description, Cash} from './styled';
import Mastercard from 'assets/ProductPage/Mastercard';
import Visa from 'assets/ProductPage/Visa';
import Paypal from 'assets/ProductPage/Paypal';
import AmericanExpress from 'assets/ProductPage/AmericanExpress';

const PaymentOptionsMobile = () => {
  return (
    <Container>
      <Title>Payment Options</Title>
      <Description>
        testSample keeps your payment information secure. testSample shops never receive your credit card information.
      </Description>
      <Icons>
        <Mastercard />
        <Visa />
        <Paypal />
        <AmericanExpress />
        <Cash>Cash</Cash>
      </Icons>
    </Container>
  );
};

export default PaymentOptionsMobile;
