import styled from 'styled-components';
import media from '../../constants/media';

export const Wrapper = styled.div`
  position: relative;
  margin-bottom: 15px;
 color: #000;

  &.radio,
  &.checkbox {
    display: inline-block !important;

    &:hover {
      label {
        border-color: #ed484f;
      }
    }

    &.error {
      .check {
        border-color: #ed484f;
      }
    }

    input {
      display: none;

      &:checked ~ label {
        border-color: #ed484f;
      }
    }

    .check {
      cursor: pointer;
      display: inline-block !important;
      vertical-align: top;
      width: 20px !important;
      height: 20px !important;
      border-radius: 50%;
      margin: 1px 0 0 0 !important;
      border: 2px solid #999;
      transition: all 0.3s ease;
      position: relative;

      &:before {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 10px;
        height: 10px;
        transition: all 0.3s ease;
        background: #000;
        opacity: 0;
        border-radius: 50%;
      }

      .icon {
        display: none;
      }
    }

    .text {
      display: inline-block;
      vertical-align: top;
      padding-left: 16px;
      font-weight: normal;
      cursor: pointer;
    }

    + .check {
      display: inline-block !important;
      vertical-align: middle;
      margin-bottom: 0 !important;
      cursor: pointer;
      padding-left: 14px;
    }

    label {
      margin: 0;
    }
  }

  &.checkbox {
    input {
      &:checked ~ label .check {
        background: #000;
      }
    }

    .check {
      width: 16px !important;
      height: 16px !important;
      border-radius: 2px;
      border-width: 1px;
      position: relative;

      .icon {
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        max-width: 10px;
        max-height: 8px;
        transform: translate(-50%, -50%);
      }
    }
  }

  &.radio {
    input {
      &:checked ~ label {
        .check {
          border-color: #000;

          &:before {
            opacity: 1;
          }
        }
      }
    }

    label {
      margin-bottom: 0 !important;
      display: flex;
      align-items: center;
    }

    .text {
      margin-top: 2px;
    }
  }
  label {
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 140%;
    color: #000;
    margin-bottom: 8px;
    display: block;
  }

  input {
    background: #fff;
    border: 1px solid #C3C3C3;#
    border-radius: 2px;
    padding: 8px 38px 7px 15px;
    width: 100%;
    min-height: 40px;
    font-size: 14px;
    outline: none;

    &::placeholder {
      color: #cccccc;
    }

    &:-webkit-autofill,
    &:-webkit-autofill-strong-password {
      // background: lighten(#4a90e2, 39%) !important;
    }
    
    &:-webkit-autofill,
    &:-webkit-autofill:hover, 
    &:-webkit-autofill:focus, 
    &:-webkit-autofill:active  {
        -webkit-box-shadow: 0 0 0 30px #fff inset !important;
    }

    &:hover {
      background: #fff;
      border-color: #a7a7a7;
    }

    &:focus,
    &:active {
      background: #fff;
      border-color: #4a90e2;
      border-color: #4A90E2;
      background-color: #eaf1fe;
      &:-webkit-box-shadow: 0 0 0 30px #eaf1fe inset !important;
      &:-webkit-autofill,
      &:-webkit-autofill:focus, 
      &:-webkit-autofill:active  {
          -webkit-box-shadow: 0 0 0 30px #eaf1fe inset !important;
      }
    }

    &:disabled {
      cursor: not-allowed;
      background: #e4e4e4;
      border-color: #a7a7a7;

      &:hover,
      &:active,
      &:focus {
        background: #e4e4e4;
        border-color: #a7a7a7;
      }
    }

    &:focus {
      ~ .chars {
        color: #666666;

        &.typing {
          color: #000;
        }
      }
    }
  }

  &.tel {
    input {
      padding-left: 74px;
    }
  }

  .number {
    position: absolute;
    top: 31px;
    left: 7px;
    width: 54px;
    height: 28px;
    background: #4a90e2;
    border-radius: 4px;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 1;
    color: #ffffff;
    display: flex;
    align-items: center;
    padding: 4px 7px;
    transition: all 0.3s ease;
  }

  .input-placeholder {
    position: absolute;
    font-size: 1rem;
    top: 26px;
    transition: 0.1s all;
    z-index: 1;
    color: rgba(#e4e4e4, 0.2);

    &.placeholder-sm {
      font-size: 14px;
    }
  }

  &.error {
    font-size: 12px;
    line-height: 1.4;
    color: #464646;

    input {
      border-color: #ed484f;
    }
  }

  .icon--double-check {
    position: absolute;
    right: 18px;
    bottom: 14px;
  }

  .reset {
    position: absolute;
    top: 28px;
    right: 3px;
    transform: scale(1.15);
    height: 40px;
    width: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;

    svg {
      max-width: 16px;
      max-height: 16px;
    }
  }

  .show-password {
    position: absolute;
    top: 27px;
    right: 3px;
    height: 40px;
    width: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
  }

  @media (max-width: 767px) {
    > .form-error {
      position: inherit;
    }
  }

  .chars {
    display: flex;
    justify-content: space-between;
    margin-top: -2px;
    font-style: normal;
    font-weight: normal;
    font-size: 10px;
    line-height: 12px;
    color: #cccccc;
    position: relative;
    
    @media (min-width: ${media.mobileMax}){
      font-size: 9.3px;
    }
  
    &:before {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      transition: all 0.3s ease;
      height: 2px;
      background: #2ecc71;
      border-radius: 2px;
      opacity: 0;
    }

    &.active:before {
      opacity: 1;
    }

    &.typing {
      color: #000;
    }

    span {
      width: 19.2%;
      padding-top: 8px;
      border-top: 2px solid transparent;
      text-align: center;
      transition: all 0.3s ease;
  
      &:first-child {
        margin-left: 0;
      }

      &:last-child {
        margin-right: 0;
      }

      &.active {
        color: #656565;
        border-top: 2px solid #2ecc71;
      }
      &.inactive {
        color: #000;
        border-top: 2px solid #F50B0B;
      }
    }
  }

  ${({disabled}) =>
    disabled &&
    `
    input {
      border-color: #C3C3C3;
    }
    
    label {
      cursor: not-allowed;
    }
  `}
`;
