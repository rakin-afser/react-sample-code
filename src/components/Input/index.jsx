import React, {useState} from 'react';
import {useWindowSize} from '@reach/window-size';
import {Field} from 'formik';

import ErrorMessage from 'components/ErrorMessage';

import {IconDoubleCheck, IconCheck, IconLoader, IconReset, IconEye, IconEyeCrossed} from 'helpers/icons';

import {Wrapper} from './styled';

const Input = React.forwardRef(
  (
    {
      validate,
      placeholder,
      value,
      className,
      onChange,
      onBlur,
      placeholderSm,
      name,
      error,
      status,
      id,
      loading,
      type = 'text',
      checked,
      number,
      readOnly,
      userNameAvailable = false,
      reset = false,
      resetCallback = null,
      showPassword = false,
      passwordChars,
      children,
      label,
      required,
      disabled,
      autoComplete,
      ...optionals
    },
    ref
  ) => {
    const [passwordVisible, setPasswordVisible] = useState(false);
    const [customType, setCustomType] = useState(null);
    const {width} = useWindowSize();
    const isMobile = width <= 767;

    const togglePasswordVisibility = () => {
      if (type === 'password') {
        if (passwordVisible) {
          setCustomType('password');
        } else {
          setCustomType('text');
        }
      }
      setPasswordVisible(!passwordVisible);
    };

    const validPassword = () => {
      return (
        /[A-Z]/.test(value) &&
        /[a-z]/.test(value) &&
        /[0-9]/.test(value) &&
        value.length >= 8 &&
        /[ !@#$%^~&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value)
      );
    };

    return (
      <Wrapper
        className={`${type} ${error ? 'error' : ''} ${className || ''}custom-form-field`}
        loading={loading}
        disabled={disabled}
        {...optionals}
      >
        {label ? (
          <>
            <label htmlFor={id}>
              {label} {required ? <span style={{color: '#ED484F'}}>*</span> : null}
            </label>
          </>
        ) : null}
        {validate ? (
          <Field
            validate={validate}
            ref={ref}
            type={customType || type}
            name={name}
            value={value}
            id={id}
            onChange={onChange}
            onBlur={onBlur}
            placeholder={placeholder}
            checked={checked}
            readOnly={readOnly}
            disabled={disabled}
            autoComplete={autoComplete}
          />
        ) : (
          <input
            ref={ref}
            type={customType || type}
            name={name}
            value={value}
            id={id}
            onChange={onChange}
            onBlur={onBlur}
            placeholder={placeholder}
            checked={checked}
            readOnly={readOnly}
            disabled={disabled}
            autoComplete={autoComplete}
          />
        )}
        {type === 'tel' && number ? <span className="number">{number}</span> : null}
        {!userNameAvailable && !loading && reset?.toString() && resetCallback ? (
          <span onClick={resetCallback} className="reset">
            <IconReset />
          </span>
        ) : null}
        {showPassword && (
          <span onClick={togglePasswordVisibility} className="show-password">
            {passwordVisible ? <IconEyeCrossed /> : <IconEye />}
          </span>
        )}
        {type === 'radio' || type === 'checkbox' ? (
          <label htmlFor={id}>
            <span className="check">
              <IconCheck />
            </span>

            {children && <span className="text">{children}</span>}
          </label>
        ) : null}
        {!loading && userNameAvailable ? <IconDoubleCheck /> : null}
        {loading ? (
          <IconLoader
            width={20}
            height={4}
            fill="#ED484F"
            style={{
              position: 'absolute',
              top: 10,
              right: 16,
              width: 20,
              height: 20
            }}
          />
        ) : null}
        {!error && passwordChars && (
          <div className={`chars${validPassword() ? ' active' : ''}${value && value.length > 0 ? ' typing' : ''}`}>
            <span className={value && value.length > 0 && /[A-Z]/.test(value) ? 'active' : 'inactive'}>
              One{isMobile ? <br /> : ' '}Uppercase
            </span>
            <span className={value && value.length > 0 && /[a-z]/.test(value) ? 'active' : 'inactive'}>
              One{isMobile ? <br /> : ' '}Lowercase
            </span>
            <span className={value && value.length > 0 && /[0-9]/.test(value) ? 'active' : 'inactive'}>
              One{isMobile ? <br /> : ' '}Numeral
            </span>
            <span className={/[ !@#$%^~&*()_+\-=\[\]{};':"\\|,.<>\/?]/.test(value) ? 'active' : 'inactive'}>
              One{isMobile ? <br /> : ' '}Symbol
            </span>
            <span className={value && value.length >= 8 ? 'active' : 'inactive'}>
              Eight{isMobile ? <br /> : ' '}Characters
            </span>
          </div>
        )}
        {error ? <ErrorMessage>{error}</ErrorMessage> : status ? <ErrorMessage>{status}</ErrorMessage> : null}
      </Wrapper>
    );
  }
);

export default Input;
