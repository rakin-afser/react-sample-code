import styled from 'styled-components';

export const Container = styled.div`
  background: #fff;
  opacity: 0.9;
  padding: 1.5px 10.7px;
  display: flex;
  align-items: center;
  border-radius: 13px;
  font-weight: 400;
  font-size: 12px;
  & div {
    margin-right: 9px;
  }
`;

export const Icon = styled.div`
  display: flex;
`;
