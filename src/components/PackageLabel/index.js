import React from 'react';
import {Container, Icon} from './styled';
import Package from '../../assets/Package';

const PackageLabel = ({count}) => {
  return (
    <Container>
      <Icon>
        <Package />
      </Icon>
      {count}
    </Container>
  );
};

export default PackageLabel;
