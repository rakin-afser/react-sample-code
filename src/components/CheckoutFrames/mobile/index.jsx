import React, {useEffect, useState} from 'react';
import {Wrap, ErrorMessage} from '../styles';
import {Frames, CardNumber, ExpiryDate, Cvv} from 'frames-react';
import {Row, Col} from 'antd';
import {primaryColor} from 'constants/colors';
import Loader from 'components/Loader';

const CheckoutFrames = ({publicKey, callback}) => {
  const [loaded, setLoaded] = useState(false);
  const [fieldsValidation, setFieldsValidation] = useState();

  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'https://cdn.checkout.com/js/framesv2.min.js';
    script.async = true;
    document.head.appendChild(script);
    script.onload = () => setLoaded(true);
    return () => {
      document.head.removeChild(script);
      setLoaded(false);
    };
  }, []);

  function isShowError(field) {
    const {isValid = false, isEmpty = true} = field || {};
    if (isEmpty) return false;
    if (isValid && !isEmpty) return false;
    if (!isValid && isEmpty) return false;
    return true;
  }

  if (!loaded) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

  return (
    <>
      <Wrap>
        <Frames
          config={{
            debug: false,
            publicKey,
            localization: {
              cardNumberPlaceholder: 'Card Number',
              expiryMonthPlaceholder: 'MM',
              expiryYearPlaceholder: 'YY',
              cvvPlaceholder: 'CVV'
            },
            style: {
              base: {
                fontSize: '22px',
                border: '1px solid #A7A7A7',
                height: '40px',
                padding: '5px 16px',
                borderRadius: '2px',
                fontFamily: "'SF Pro Display', sans-serif",
                color: '#000'
              },
              autofill: {
                backgroundColor: 'yellow'
              },
              invalid: {
                color: primaryColor,
                borderColor: primaryColor
              },
              placeholder: {
                base: {
                  color: '#999999',
                  fontSize: '22px'
                }
              }
            }
          }}
          ready={() => {}}
          frameActivated={(e) => {}}
          frameFocus={(e) => {}}
          frameBlur={(e) => {}}
          frameValidationChanged={(e) => {
            const {element, isValid, isEmpty} = e;
            setFieldsValidation((prev) => ({
              ...prev,
              [element]: {
                isValid,
                isEmpty
              }
            }));
          }}
          paymentMethodChanged={(e) => {
            console.log(`paymentMethodChanged`, e);
          }}
          cardSubmitted={() => {}}
          cardTokenized={(e) => {
            callback(e.token);
          }}
          cardTokenizationFailed={(e) => {}}
        >
          <Row gutter={[12, 20]}>
            <Col>
              <CardNumber />
              {isShowError(fieldsValidation?.['card-number']) ? (
                <ErrorMessage>Please enter a valid card number</ErrorMessage>
              ) : null}
            </Col>
            <Col span={12}>
              <ExpiryDate />
              {isShowError(fieldsValidation?.['expiry-date']) ? (
                <ErrorMessage>You must enter a valid expiration date</ErrorMessage>
              ) : null}
            </Col>
            <Col span={12}>
              <Cvv />
              {isShowError(fieldsValidation?.cvv) ? (
                <ErrorMessage>Please enter a valid security code</ErrorMessage>
              ) : null}
            </Col>
            {/* <Col span={14}>
              <Label htmlFor="checkout-cardholder">Cardholder Name</Label>
              <Input onChange={(e) => setName(e.target.value)} id="checkout-cardholder" type="text" />
            </Col> */}
          </Row>
        </Frames>
      </Wrap>
    </>
  );
};

export default CheckoutFrames;
