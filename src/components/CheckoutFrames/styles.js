import {primaryColor} from 'constants/colors';
import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';

export const Wrap = styled.div`
  .card-number-frame,
  .expiry-date-frame,
  .cvv-frame {
    height: 40px;
  }
`;

export const Label = styled.label`
  color: #000;
  font-weight: 700;
  padding-bottom: 7px;
  display: inline-block;
  line-height: 1;
`;

export const ErrorMessage = styled.span`
  font-family: ${secondaryFont};
  color: ${primaryColor};
  font-size: 12px;
  line-height: 1;
  display: inline-block;
  vertical-align: top;
  margin-top: 5px;
`;
