import styled from 'styled-components/macro';
import {mainWhiteColor, mainBlackColor} from '../../constants/colors';
import {FlexContainer} from '../../globalStyles';
import {Link} from 'react-router-dom';

export const Card = styled(FlexContainer)`
  position: relative;
  flex-direction: column;
  width: 268px;
  height: 175px;
  background: ${mainWhiteColor};
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin: 0;
  overflow: hidden;
`;

export const Image = styled.img`
  width: 100%;
  height: 109px;
  object-fit: cover;
`;

export const CardFooter = styled(FlexContainer)`
  width: 100%;
  height: 66px;
  padding: 8px 10px 11px;
`;

export const Title = styled(Link)`
  display: block;
  font-weight: 500;
  font-size: 18px;
  letter-spacing: -0.6px;
  color: ${mainBlackColor};
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  max-width: 140px;
`;

export const StyledLink = styled(Link)`
  width: 100%;
`;
