import React from 'react';

import {FlexContainer, FollowersCount} from 'globalStyles';
import {CardFooter, Image, Card, Title, StyledLink} from './styled';
import productPlaceholder from 'images/placeholders/product.jpg';
import LazyLoad from 'react-lazyload';
import Btn from 'components/Btn';
import {useFollowUnfollowCategoryMutation} from 'hooks/mutations';

const CardCategory = ({id, databaseId, name, image, totalFollowers, isFollowed, slug}) => {
  const [followUnfollow] = useFollowUnfollowCategoryMutation({id, isFollowed});

  return (
    <Card>
      <StyledLink to={`/categories/${slug}`}>
        <LazyLoad once style={{height: '100%'}}>
          <Image src={image?.sourceUrl || productPlaceholder} alt={image?.altText} />
        </LazyLoad>
      </StyledLink>
      <CardFooter>
        <FlexContainer alignItems="flex-start" flexDirection="column" style={{lineHeight: 1.5}}>
          <Title to={`/categories/${slug}`} title={name}>
            {name}
          </Title>
          <FollowersCount>{totalFollowers} followers</FollowersCount>
        </FlexContainer>
        <Btn
          kind={isFollowed ? 'following-md' : 'follow-md'}
          onClick={() =>
            followUnfollow({
              variables: {input: {id: databaseId}}
            })
          }
        />
      </CardFooter>
    </Card>
  );
};

export default CardCategory;
