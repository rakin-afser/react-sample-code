import React from 'react';
import BadgeAtom from './styled';

import LetterIcon from 'assets/LetterIcon';
import ShoppingCart from 'assets/ShoppingCart';
import BellIcon from 'assets/BellIcon';
import Search from 'assets/Search';

const Badge = ({type, counter, action, selected}) => {
  const getIcon = () => {
    switch (type) {
      case 'message':
        return <LetterIcon />;
      case 'cart':
        return <ShoppingCart />;
      case 'search':
        return <Search color="#000" />;
      case 'notifications':
      default:
        return <BellIcon />;
    }
  };

  return (
    <BadgeAtom name={type} counter={counter} onClick={action} selected={selected}>
      {getIcon()}
    </BadgeAtom>
  );
};

export default Badge;
