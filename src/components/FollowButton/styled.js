import styled from 'styled-components/macro';
import {mainFont, secondaryFont} from 'constants/fonts';

export const FollowBtn = styled.button`
  margin-left: auto;
  padding-left: ${({followed}) => (followed ? '14px' : '20px')};
  padding-right: ${({followed}) => (followed ? '14px' : '20px')};
  outline: none;
  background: #fff;
  height: 26px;
  border: 1px solid ${({followed}) => (followed ? '#8F8F8F' : '#EA2C34')};
  border-radius: 24px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-family: ${({followed}) => (followed ? secondaryFont : mainFont)};
  font-weight: 500;
  font-size: 12px;
  line-height: 14px;
  cursor: pointer;
  color: ${({followed}) => (followed ? '#8F8F8F' : '#EA2C34')};

  svg {
    margin-right: 9px;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    ` 
    svg {
      margin: 0 0 0 9px;
    }
  `}
`;

export const IconLoader = styled.span`
  margin-top: -1px;

  svg {
    margin: 0;
  }
`;

export const IconCheck = styled.span`
  svg {
    fill: #7a7a7a;
    max-width: 10px;
    max-height: 8px;
  }
`;
