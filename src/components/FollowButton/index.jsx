import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icon';
import {FollowBtn, IconLoader, IconCheck} from './styled';

const FollowButton = ({isFollowed, loading, ...props}) => {
  const renderFollow = () => {
    if (loading) {
      return (
        <IconLoader>
          <Icon type="loader" svgStyle={{width: 24, height: 5, fill: '#ED484F'}} />
        </IconLoader>
      );
    }

    if (isFollowed) {
      return (
        <>
          {/*<IconCheck style={{marginLeft: 0}}>*/}
          {/*<Icon type="checkbox" />*/}
          {/*</IconCheck>*/}
          Following
        </>
      );
    }

    return (
      <>
        {/*<Icon type="plus" />*/}
        Follow
      </>
    );
  };

  return (
    <FollowBtn followed={isFollowed} {...props}>
      {renderFollow()}
    </FollowBtn>
  );
};

FollowButton.defaultProps = {
  isFollowed: false,
  loading: false
};

FollowButton.propTypes = {
  isFollowed: PropTypes.bool,
  loading: PropTypes.bool
};

export default FollowButton;
