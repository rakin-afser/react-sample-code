import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Wrapper} from './styled';

const FadeOnMount = ({children, ...props}) => {
  const [opacity, setOpacity] = useState(true);

  useEffect(() => {
    setTimeout(() => setOpacity(false), 100);
  }, []);

  return (
    <Wrapper opacity={+opacity} {...props}>
      {children}
    </Wrapper>
  );
};

FadeOnMount.propTypes = {
  children: PropTypes.node.isRequired
};

export default FadeOnMount;
