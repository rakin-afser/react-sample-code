import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  opacity: ${({opacity}) => (opacity ? 0 : 1)};
  transition: ${({transition}) => transition || 'all ease 0.6s'};
`;
