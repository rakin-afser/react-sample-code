import React from 'react';
import {Container, Heading} from './styled';

export default function({heading, children, customStyles, customStylesHeading}) {
  return (
    <Container customStyles={customStyles}>
      <Heading customStylesHeading={customStylesHeading}>{heading}</Heading>
      {children}
    </Container>
  );
}
