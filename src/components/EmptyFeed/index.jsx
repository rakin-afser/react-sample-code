import React from 'react';
import {
  Card,
  Header,
  Avatar,
  HeaderInfo,
  UserName,
  Date,
  ContentSection,
  Title,
  Description,
  Tags,
  Tag,
  Media,
  Footer,
  Circle,
  RoundedRectangle
} from './styled';

const EmptyFeed = () => (
  <Card>
    <Header>
      <Avatar />
      <HeaderInfo>
        <UserName />
        <Date />
      </HeaderInfo>
    </Header>
    <ContentSection>
      <Title />
      <Description />
      <Tags>
        <Tag />
        <Tag />
      </Tags>
      <Media />
    </ContentSection>
    <Footer>
      <Circle />
      <Circle />
      <RoundedRectangle />
      <RoundedRectangle />
      <Circle />
    </Footer>
  </Card>
);

export default EmptyFeed;
