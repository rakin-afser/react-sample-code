import styled, {keyframes} from 'styled-components';

const loading = keyframes`
to {
  background-position: calc(100% + 100px) 50%;
}
`;

export const Card = styled.div`
  width: 500px;
  margin: 30px 0;
  padding: 20px 30px;
  background-color: #eee;
  position: relative;

  &:after {
    display: block;
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    background-image: linear-gradient(90deg, transparent 0, #eee, #f5f5f5, #eee, transparent 100%);
    background-size: 100px 100%;
    background-position: -100px 50%;
    background-repeat: no-repeat;
    z-index: 1;
    top: 0;
    left: 0;
    animation: ${loading} 1.5s infinite;
  }
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
`;

export const Avatar = styled.div`
  border-radius: 50%;
  width: 60px;
  height: 60px;
  background-color: white;
`;

export const HeaderInfo = styled.div`
  display: flex;
  flex-direction: column;
`;

export const UserName = styled.div`
  width: 150px;
  height: 20px;
  margin: 0 0 10px 10px;
  background-color: white;
`;

export const Date = styled.div`
  width: 100px;
  height: 20px;
  margin-left: 10px;
  background-color: white;
`;

export const ContentSection = styled.div`
  display: flex;
  margin-top: 50px;
  flex-direction: column;
`;

export const Title = styled.div`
  width: 150px;
  height: 25px;
  margin-bottom: 20px;
  background-color: white;
`;

export const Description = styled.div`
  margin-bottom: 5px;
  width: 100%;
  height: 300px;
  background-color: white;
`;

export const Tags = styled.div`
  display: flex;
  margin: 20px 0;
`;

export const Tag = styled.div`
  width: 80px;
  height: 20px;
  margin-right: 20px;
  background-color: white;
`;

export const Media = styled.div`
  margin-bottom: 5px;
  width: 100%;
  height: 500px;
  background-color: white;
`;

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 15px 0;
`;

export const Circle = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background-color: white;
`;

export const RoundedRectangle = styled.div`
  width: 100px;
  height: 50px;
  border-radius: 20px;
  background-color: white;
`;
