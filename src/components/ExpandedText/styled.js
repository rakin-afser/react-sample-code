import styled from 'styled-components';
import {secondaryFont} from 'constants/fonts';

export const Expanded = styled.div`
  margin: ${({margin}) => margin};
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-align: ${({textAlign}) => textAlign};
  color: #464646;
  width: 100%;
  max-height: ${({expandedHeight, isShow}) => (isShow ? '1000px' : `${expandedHeight}px`)};
  ${({isHtml}) => (isHtml ? 'margin-top: 0;' : null)};
  transition: ease 0.6s;
  overflow: hidden;
`;

export const MoreButton = styled.button`
  color: #ed494f;
  outline: none;
  border: none;
  background: #fff;
  display: inline;
  cursor: pointer;
`;

export const MoreText = styled.span`
  ${({isShow}) => (isShow ? 'dispay: inline' : 'display: none')};
`;

export const Dots = styled.span``;
