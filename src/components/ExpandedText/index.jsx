import React, {useState, useRef, useEffect} from 'react';
import {node, number, bool, string} from 'prop-types';
import {Expanded, Dots, MoreText, MoreButton} from './styled.js';

const ExpandedText = ({text, isExpanded = false, divider, isHtml, margin, textAlign, moreText, lessText}) => {
  const [isShow, setIsShow] = useState(isExpanded);
  const [expandedHeight, setExpandedHeight] = useState(null);
  const expandedRef = useRef(null);

  useEffect(() => {
    if (expandedRef.current) {
      const refHeight = expandedRef.current.getBoundingClientRect().height;
      setExpandedHeight(refHeight);
    }
  }, []);

  if (isHtml) {
    return (
      <Expanded
        textAlign={textAlign}
        margin={margin}
        isShow={isShow}
        expandedHeight={expandedHeight}
        ref={expandedRef}
        isHtml
      >
        <Dots>{isShow ? '' : '...'}</Dots>
        <MoreText isShow={isShow}>{text}</MoreText>
        <MoreButton
          onClick={() => {
            isShow ? setIsShow(false) : setIsShow(true);
          }}
        >
          {isShow ? lessText : moreText}
        </MoreButton>
      </Expanded>
    );
  }

  if (text?.length > divider) {
    return (
      <Expanded textAlign={textAlign} margin={margin} isShow={isShow} expandedHeight={expandedHeight} ref={expandedRef}>
        {text.slice(0, divider)}
        <Dots>{isShow ? '' : '...'}</Dots>
        <MoreText isShow={isShow}>{text.slice(divider)}</MoreText>
        <MoreButton
          onClick={() => {
            isShow ? setIsShow(false) : setIsShow(true);
          }}
        >
          {isShow ? lessText : moreText}
        </MoreButton>
      </Expanded>
    );
  }
  return (
    <Expanded textAlign={textAlign} margin={margin}>
      {text}
    </Expanded>
  );
};

ExpandedText.propTypes = {
  text: node,
  divider: number,
  isHtml: bool,
  margin: string,
  textAlign: string,
  lessText: string,
  moreText: string
};

ExpandedText.defaultProps = {
  text: '',
  divider: 57,
  isHtml: false,
  margin: '24px 0 0 0',
  textAlign: 'center',
  lessText: 'See Less',
  moreText: 'See More'
};

export default ExpandedText;
