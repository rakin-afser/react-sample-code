import React, {useState, useEffect} from 'react';
import _ from 'lodash';
import {useParams} from 'react-router-dom';

import PriceSelect from 'components/PriceSelect';
import Input from 'components/Input';
import Grid from 'components/Grid';
import Select from 'components/SelectTrue';
import Icon from 'components/Icon';
import {
  CategoryQuantity,
  CategoriesContainer,
  MenuWrapper,
  CategoryTitle,
  CategoryItem,
  CategorySubItem,
  SubMenuWrapper,
  BackLink,
  ShowMoreButton
} from './styled';
import {offers, price, size, colors, shopLocation, shipTo, shopLocationCustom} from './exampleData';

const Categories = ({
  data,
  categoryHistory = [],
  filters = {},
  setFilters = () => {},
  categoryClick = () => {},
  more = false
}) => {
  const {categorySlug, subCategorySlug1, subCategorySlug2} = useParams();
  const [inputs, setInputs] = useState(filters);
  const [currentCategory, setCurrentCategory] = useState(
    categorySlug ? data?.children?.nodes?.find((item) => item.slug === categorySlug) || data : data
  );
  const currentLevel = subCategorySlug2 ? 3 : subCategorySlug1 ? 2 : categorySlug ? 1 : 0;

  useEffect(() => {
    setCurrentCategory(getCurrentCategory());
  }, [categorySlug, subCategorySlug1, subCategorySlug2, data]);

  useEffect(() => {
    setFilters(inputs);
  }, [inputs]);

  const changeFilters = (filter, itemId, values) => {
    if (!!inputs[filter.id] && !filter.unic) {
      if (isItemSelected(filter.id, itemId)) {
        //remove item from selected filters
        setInputs({
          ...inputs,
          [filter.id]: [...inputs[filter.id].filter((elId) => elId !== itemId)]
        });
      } else {
        //add item to exist array
        setInputs({
          ...inputs,
          [filter.id]: [...new Set([...inputs[filter.id], itemId])]
        });
      }
    } else {
      //add item
      if (filter?.id === 'price') {
        setInputs({...inputs, [filter.id]: [itemId], priceValue: values});
      } else {
        setInputs({...inputs, [filter.id]: [itemId]});
      }
      
      if (filter?.id === 'size') {
        setInputs({...inputs, [filter.id]: [itemId], sizeValue: values});
      } else {
        setInputs({...inputs, [filter.id]: [itemId]});
      }
    }
  };

  const isItemSelected = (filterId, itemId) => {
    return !!inputs[filterId]?.includes(itemId);
  };

  const getCurrentPrice = () => {
    const priceValue = filters?.priceValue || [];
    return [Number(priceValue?.[0]), Number(priceValue?.[1])];
  };

  const getCurrentCategory = () => {
    return [...categoryHistory]?.pop();
  };

  const mappedCategory =
    currentLevel === 2 || currentLevel === 3 ? data?.children?.nodes?.find((item) => item.slug === categorySlug) : data;

  return (
    <CategoriesContainer>
      <CategoryTitle>{currentCategory?.name}</CategoryTitle>
      {!!categorySlug && (
        <BackLink onClick={() => categoryClick()}>
          <Icon type="arrow" color="#000" height={14} width={12} />
          {data?.name}
        </BackLink>
      )}
      {!!subCategorySlug1 && (
        <BackLink onClick={() => categoryClick(categorySlug, 1)}>
          <Icon type="arrow" color="#000" height={14} width={12} />
          {categoryHistory?.[1]?.name}
        </BackLink>
      )}
      <MenuWrapper slip={!!categorySlug}>
        {mappedCategory?.children?.nodes?.map((item) => (
          <CategoryItem key={item.slug} isActive={categorySlug === item.slug || subCategorySlug1 === item.slug}>
            <div onClick={() => categoryClick(item?.slug, subCategorySlug1 ? 2 : 1)}>
              <span>{item.name}</span>{' '}
              {item.count || typeof item.count === 'number' ? (
                <CategoryQuantity>/ {item.count}</CategoryQuantity>
              ) : null}
              {/* {clearAll ? <ClearButton onClick={clearAll}>Reset All</ClearButton> : null} */}
            </div>
            {(currentCategory?.slug === item?.slug || subCategorySlug1 === item?.slug) &&
              !_.isEmpty(item?.children?.nodes) && (
                <SubMenuWrapper>
                  {item?.children?.nodes?.map((subItem) => (
                    <CategorySubItem
                      onClick={() => categoryClick(subItem?.slug, currentLevel + 1)}
                      isActive={subCategorySlug1 === subItem.slug || subCategorySlug2 === subItem.slug}
                      key={subItem.slug}
                    >
                      {subItem.name}{' '}
                      {subItem.count || typeof subItem.count === 'number' ? (
                        <CategoryQuantity>({subItem.count})</CategoryQuantity>
                      ) : null}
                    </CategorySubItem>
                  ))}
                </SubMenuWrapper>
              )}
          </CategoryItem>
        ))}
      </MenuWrapper>
      {more && (
        <>
          <div style={{marginBottom: 40}}>
            <h4 style={{marginBottom: 18}} className="list-item">
              {offers.name}
            </h4>
            {offers?.children?.map((el) => (
              <Grid key={el.id}>
                <Input
                  onChange={() => changeFilters(offers, el.id)}
                  type="checkbox"
                  id={`${offers.id}-${el.id}`}
                  checked={isItemSelected(offers.id, el.id)}
                  name={offers.id}
                />
                <span style={{marginLeft: 12}}>{el.name}</span>
              </Grid>
            ))}
          </div>

          <div style={{marginBottom: 40}}>
            <h4 style={{marginBottom: 18}} className="list-item">
              {size.name}
            </h4>
            {size?.children?.map((el) => (
              <Grid key={el.id}>
                <Input
                  onChange={() => changeFilters(size, el.id, el?.values)}
                  type="radio"
                  name="size"
                  id={`${size.id}-${el.id}`}
                  value={el.name}
                  checked={isItemSelected(size.id, el.id)}
                />
                <span style={{marginLeft: 12}}>{el.name}</span>
              </Grid>
            ))}
          </div>

          <div style={{marginBottom: 40}}>
            <h4 style={{marginBottom: 18}} className="list-item">
              {price.name}
            </h4>
            {price?.children?.map((el) => (
              <Grid key={el.id}>
                <Input
                  onChange={() => changeFilters(price, el.id, el?.values)}
                  type="radio"
                  name="price"
                  id={`${price.id}-${el.id}`}
                  value={el.name}
                  checked={isItemSelected(price.id, el.id)}
                />
                <span style={{marginLeft: 12}}>{el.name}</span>
              </Grid>
            ))}
            <PriceSelect
              currencyType={price.type}
              currentPrice={getCurrentPrice()}
              min={price.min}
              max={price.max}
              disabled={!isItemSelected(price.id, 'custom')}
              onChange={(value) => changeFilters({id: 'price', unic: true}, 'custom', value)}
            />
          </div>

          {/* Hide untill created api */}
          {/* <div style={{marginBottom: 40}}>
            <h4 style={{marginBottom: 18}} className="list-item">
              {colors.name}
            </h4>
            {colors?.children?.map((el) => (
              <Grid key={el.id}>
                <Input
                  onChange={() => changeFilters(colors, el.id)}
                  type="checkbox"
                  name="color"
                  id={`${colors.id}-${el.id}`}
                  checked={isItemSelected(colors.id, el.id)}
                />
                <span style={{marginLeft: 12}}>{el.name}</span>
              </Grid>
            ))}
          </div> */}

          {/* Hide untill created api */}
          {/* <div style={{marginBottom: 40}}>
            <h4 style={{marginBottom: 18}} className="list-item">
              {shopLocation.name}
            </h4>
            {shopLocation?.children?.map((el) => (
              <Grid key={el.id}>
                <Input
                  onChange={() => changeFilters(shopLocation, el.id)}
                  type="radio"
                  name="location"
                  id={`${shopLocation.id}-${el.id}`}
                  value={el.id}
                  checked={isItemSelected(shopLocation.id, el.id)}
                />
                <span style={{marginLeft: 12}}>{el.name}</span>
              </Grid>
            ))}
            <Select
              name="customLocation"
              onChange={(itemId) => changeFilters(shopLocationCustom, itemId)}
              value={inputs?.[shopLocationCustom.id]}
              options={shopLocationCustom?.children}
              labelKey="name"
              valueKey="id"
              disabled={!isItemSelected(shopLocation.id, 'custom')}
            />
          </div> */}

          {/* Hide untill created api */}
          {/* <div style={{marginBottom: 40}}>
            <h4 style={{marginBottom: 18}} className="list-item">
              {shipTo.name}
            </h4>
            <Select
              name="shipTo"
              onChange={(itemId) => changeFilters(shipTo, itemId)}
              value={inputs?.[shipTo.id]}
              options={shipTo?.children}
              labelKey="name"
              valueKey="id"
            />
          </div> */}
        </>
      )}
    </CategoriesContainer>
  );
};

export default Categories;
