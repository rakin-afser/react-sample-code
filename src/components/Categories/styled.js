import styled, {css} from 'styled-components/macro';

export const CategoriesContainer = styled.div`
  a:hover,
  .ant-menu-submenu-title:hover,
  .ant-menu-item:hover {
    color: inherit;
  }

  .list-item {
    color: #000;
  }

  ul {
    border: none;
  }

  .sub-category-name span,
  .category-value span {
    color: #545454;
    font-size: 12px;
  }

  .ant-menu-item-group-list,
  .ant-menu-submenu-title,
  li {
    font-weight: normal;
    font-size: 14px;
    padding: 0 !important;
  }

  .category-name {
    font-weight: 500;
    font-size: 14px;
  }

  li.ant-menu-item.ant-menu-item-selected,
  .ant-menu-submenu-selected .ant-menu-submenu-title {
    background-color: #fff !important;
  }

  .ant-menu-submenu-selected li.ant-menu-item.ant-menu-item-selected span,
  .ant-menu-submenu-selected .ant-menu-submenu-selected .sub-category-name span,
  li.ant-menu-submenu.ant-menu-submenu-inline.list-item.ant-menu-submenu-open .ant-menu-item-selected .category-value {
    font-weight: 500 !important;
    font-size: 14px !important;
    color: #000;
  }

  .ant-menu-inline .ant-menu-item::after {
    border-right: none !important;
  }

  .category-value {
  }

  .ant-menu-submenu-arrow {
    display: none;
  }

  .ant-menu-submenu-open li {
    padding-left: 17px !important;
  }
`;

export const StyledCategory = styled.div`
  text-align: left;
  font-weight: normal;
  font-size: 14px
  line-height: 140%;
  letter-spacing: 0;
  margin: 13px 0;
`;

export const ClearButton = styled.span`
  font-weight: normal;
  font-size: 14px;
  color: #ed494f;
  float: right;
`;

export const CurrentCategory = styled.h3`
  font-size: 18px;
  letter-spacing: -0.024em;
`;

export const CategoryQuantity = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 11px;
  color: #999999;
`;

export const MenuWrapper = styled.div`
  margin-bottom: 40px;
  transition: margin 0.5s;

  ${({slip}) =>
    slip &&
    css`
      margin-left: 22px;
    `}
`;

export const SubMenuWrapper = styled.div`
  padding: 12px 0 4px 16px;
`;

export const CategoryItem = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #000000;
  padding: 8px 0;
  cursor: pointer;
  text-align: left;
  transition: color 0.4s;

  ${({isActive}) =>
    isActive &&
    css`
      font-weight: 500;
    `}
`;

export const CategorySubItem = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #545454;
  padding: 4px 0;
  transition: color 0.4s;

  ${({isActive}) =>
    isActive &&
    css`
      color: #000;
      font-weight: 500;
    `}
`;

export const CategoryTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  color: #000000;
  letter-spacing: -0.024em;
  margin-bottom: 25px;
  text-align: left;
`;

export const BackLink = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #656565;
  margin-bottom: 9px;
  cursor: pointer;
  display: flex;
  align-items: end;
  line-height: normal;

  & svg {
    transform: rotate(180deg);
    margin-right: 12px;
  }
`;

export const ShowMoreButton = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  display: flex;
  align-items: center;
  color: #000000;
`;
