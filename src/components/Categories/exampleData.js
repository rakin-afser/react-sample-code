export const allCategories = [
  {
    name: 'Clothings',
    count: 8902,
    values: [
      {name: 'All Clothings'},
      {
        name: "Women's Clothing",
        count: 2,
        values: [
          {name: "All Women's Clothing"},
          {name: 'Dresses', count: 0},
          {name: 'Activewear', count: 0},
          {name: 'Tops', count: 0},
          {name: 'Jeans', count: 0},
          {name: 'Shorts & Capris', count: 0},
          {name: 'Skirts', count: 0},
          {name: 'Swimwear', count: 1},
          {name: 'Pants & Leggings', count: 0},
          {name: 'Pajamas & Robes', count: 0},
          {name: 'Bras, Panties & Lingerie', count: 0},
          {name: 'Arabian Wear', count: 0},
          {name: 'Indian Wear', count: 0},
          {name: 'Pakistani Wear', count: 0},
          {name: 'Abayas', count: 0},
          {name: 'Kaftans', count: 0},
          {name: 'Sarees', count: 0},
          {name: 'Salwar Kameez', count: 0},
          {name: 'Lehengas', count: 0},
          {name: 'Indo Western', count: 0},
          {name: 'Kurtas & Shalwar Kameez', count: 0},
          {name: 'Unstitched Fabric', count: 0},
          {name: 'Pants, Palazzos & Capris', count: 0},
          {name: 'Dupattas, Stoles & Shawls', count: 0},
          {name: 'Abayas & Hijabs', count: 0},
          {name: 'Formal Wear', count: 0}
        ]
      },
      {
        name: 'Maternity',
        count: 0,
        values: [
          {name: 'All Maternity'},
          {name: 'Maternity Wear', count: 0},
          {name: 'Maternity Underwear', count: 0},
          {name: 'Pajamas & Nightgowns', count: 0}
        ]
      },
      {
        name: "Men's Clothing",
        count: 1,
        values: [
          {name: "All Men's Clothing"},
          {name: 'Shirts', count: 0},
          {name: 'Shorts', count: 0},
          {name: 'Jeans', count: 0},
          {name: 'Casual', count: 1},
          {name: 'Pants', count: 0},
          {name: 'Suits & Dresswear', count: 0},
          {name: 'Activewear', count: 0},
          {name: 'Swimwear', count: 0},
          {name: 'Underwear', count: 0},
          {name: 'Pajamas & Robes', count: 0},
          {name: 'Workwear', count: 0}
        ]
      },
      {name: 'Teen Girls', count: 0},
      {name: 'Teen Boys', count: 0},
      {name: 'Other', count: 0}
    ]
  },
  {name: 'Shoes', count: 82},
  {name: 'Bags & Accessories', count: 82},
  {name: 'Beauty & Health', count: 82},
  {name: 'Electronics', count: 32},
  {name: 'Fashion', count: 82},
  {name: 'Appliances', count: 92},
  {name: 'Jewelry & Watches', count: 82}
];

export const offers = {
  id: 'offers',
  name: 'Offers',
  children: [
    {id: 'free', name: 'FREE delivery/shipping'},
    {id: 'sale', name: 'On Discount/Sale'}
  ]
};

export const size = {
  id: 'size',
  name: 'Size',
  children: [
    {id: 'S', name: 'S'},
    {id: 'M', name: 'M'},
    {id: 'L', name: 'L'},
    {id: 'XL', name: 'XL'}
  ]
};

export const price = {
  name: 'Price',
  id: 'price',
  unic: true,
  min: 10,
  max: 1000,
  type: '$',
  defaultId: 'price1',
  children: [
    {id: 'price1', name: 'Any price', values: [10, 1000]},
    {id: 'price2', name: 'Under $250', values: [10, 250]},
    {id: 'price3', name: '$250 to $500', values: [250, 500]},
    {id: 'price4', name: '$500 to $1,000', values: [500, 1000]},
    {id: 'custom', name: 'Custom'}
  ]
};

export const colors = {
  id: 'color',
  name: 'Color',
  children: [
    {
      name: 'Pink',
      id: 'pink'
    },
    {
      name: 'Red',
      id: 'red'
    },
    {
      name: 'Yellow',
      id: 'yellow'
    },
    {
      name: 'Green',
      id: 'green'
    },
    {
      name: 'Orange',
      id: 'orange'
    }
  ]
};

export const countries = [
  {
    name: 'Ukraine',
    id: 'ukraine'
  },
  {
    name: 'USA',
    id: 'usa'
  },
  {
    name: 'England',
    id: 'england'
  },
  {
    name: 'Spain',
    id: 'spain'
  },
  {
    name: 'Custom',
    id: 'custom'
  }
];

export const customCountries = [
  {
    name: 'Russia',
    id: 'russia'
  },
  {
    name: 'Albania',
    id: 'albania'
  },
  {
    name: 'Cuba',
    id: 'cuba'
  },
  {
    name: 'Egypt',
    id: 'egypt'
  }
];

export const shipTo = {id: 'shipTo', name: 'Ship to', unic: true, children: customCountries};

export const shopLocation = {id: 'shopLocation', unic: true, name: 'Shop Location', children: countries};

export const shopLocationCustom = {id: 'shipToCustom', unic: true, children: customCountries};
