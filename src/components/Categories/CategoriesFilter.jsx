import React, {useState} from 'react';
import {Menu} from 'antd';

import {
  StyledCategory,
  CategoryQuantity,
  CategoriesContainer,
  ClearButton,
  CurrentCategory
} from 'components/Categories/styled';
import {allCategories} from './exampleData';

const {SubMenu} = Menu;

const CategoryToDisplay = ({className, name, count, clearAll}) => (
  <StyledCategory className={className}>
    <span>{name}</span> {count || typeof count === 'number' ? <CategoryQuantity>({count})</CategoryQuantity> : null}
    {clearAll ? <ClearButton onClick={clearAll}>Reset All</ClearButton> : null}
  </StyledCategory>
);

const CategoriesFilter = ({values = [], select = (f) => f}) => {
  const [currentCategory, setCurrentCategory] = useState('All Categories');

  const onCategorySelect = (data) => {
    select(data.selectedKeys);
    setCurrentCategory(data.key);
  };

  return (
    <CategoriesContainer>
      <CurrentCategory>{currentCategory}</CurrentCategory>
      <Menu
        style={{marginBottom: 40}}
        mode="inline"
        multiple
        selectedKeys={values}
        onSelect={onCategorySelect}
        onDeselect={(data) => select(data.selectedKeys)}
      >
        {allCategories.map((listItem, index) => (
          <SubMenu
            className="list-item"
            key={listItem.name}
            title={
              <div>
                <CategoryToDisplay
                  clearAll={index ? null : () => select([])}
                  className="category-name"
                  name={listItem.name}
                  count={listItem.count}
                />
              </div>
            }
          >
            {listItem.values &&
              listItem.values.map((subItem) =>
                subItem.values ? (
                  <SubMenu
                    key={subItem.name}
                    title={
                      <CategoryToDisplay className="sub-category-name" name={subItem.name} count={subItem.count} />
                    }
                  >
                    {subItem.values.map((el) => (
                      <Menu.Item key={el.name}>
                        <CategoryToDisplay className="category-value" name={el.name} count={el.count} />
                      </Menu.Item>
                    ))}
                  </SubMenu>
                ) : (
                  <Menu.Item key={subItem.name}>
                    <CategoryToDisplay name={subItem.name} count={subItem.count} />
                  </Menu.Item>
                )
              )}
          </SubMenu>
        ))}
      </Menu>
    </CategoriesContainer>
  );
};

export default CategoriesFilter;
