import styled from 'styled-components';

export const Dots = styled.div`
  & div {
    width: 4px;
    height: 4px;
    border-radius: 50%;
    background-color: #7c7e82;
    margin-left: 4px;
    display: inline-flex;
  }
  & div:first-child {
    margin-left: 0;
  }
`;
