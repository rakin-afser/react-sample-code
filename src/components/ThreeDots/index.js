import React from 'react';
import useGlobal from 'store';
import {Dots} from './styled';

const ThreeDots = () => {
  const [, globalActions] = useGlobal();

  const buttons = [
    {
      title: 'Edit'
    },
    {
      title: 'Delete'
    }
  ];

  return (
    <Dots onClick={() => globalActions.setOptionsPopup({active: true, buttons})}>
      <div />
      <div />
      <div />
    </Dots>
  );
};

export default ThreeDots;
