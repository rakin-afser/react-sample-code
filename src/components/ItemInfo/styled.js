import styled, {css} from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const Container = styled.div`
  position: relative;
`;

export const HorizontalDivider = styled.i`
  display: block;
  width: 100%;
  height: 1px;
  background: #e4e4e4;
`;

export const Bottom = styled.div`
  justify-content: space-between;
  display: flex;

  svg {
    cursor: pointer;
  }
`;

export const FooterCol = styled.span`
  display: flex;
  align-items: center;
`;

export const Action = styled.button`
  border: none;
  background-color: transparent;
  padding: 0 10px;
  border-radius: 22px;
  height: 26px;
  background: #f5f5f5;
  border: 1px solid #ccc;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  ${({mla}) =>
    mla
      ? css`
          margin-left: auto;
        `
      : null};

  &:not(:last-child) {
    margin-right: 10px;
  }

  span {
    font-size: 10px;
    color: #999;
    line-height: 13px;
    margin-left: 5.5px;
  }
`;
