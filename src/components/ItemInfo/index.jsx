import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import {useMutation} from '@apollo/client';

import {useUser} from 'hooks/reactiveVars';
import {ADD_UPDATE_LIKED_PRODUCT} from 'mutations';

import Likes from 'components/Likes';

import Bookmarks from 'assets/ProductPage/Bookmarks';
import Comments from 'assets/ProductPage/Comments';
import Links from 'assets/ProductPage/Links';
import Share from 'assets/Share';

import AddToList from 'components/AddToList';
import {Container, Bottom, HorizontalDivider, FooterCol, Action} from './styled';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';
import Icon from 'components/Icon';
import {stripTags} from 'util/heplers';

const ItemInfo = ({data, additionalStyles, isModal, displayComments}) => {
  const {databaseId, isLiked, totalLikes, commentCount, slug, name, description} = data?.product || {};

  const [user] = useUser();
  const history = useHistory();
  const [showAddToList, setShowAddToList] = useState(false);

  const [triggerLike] = useAddUpdateLikedProductMutation({product: data?.product});

  const onLike = () => {
    if (!user?.databaseId) {
      history.push('/auth/sign-in');
      return;
    }

    triggerLike({variables: {input: {id: databaseId}}});
  };

  const onComment = () => {
    if (!user?.email) {
      history.push('/auth/sign-in');
      return;
    }

    displayComments(true);
  };

  const onShare = async () => {
    const shareData = {
      title: name,
      text: stripTags(description),
      url: `${window.location.origin}/product/${slug}`
    };

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  const onSaveToList = () => {
    if (!user?.databaseId) {
      history.push('/auth/sign-in');
      return;
    }

    setShowAddToList(true);
  };

  const sizes = {
    width: 15,
    height: 15
  };

  return (
    <Container>
      {isModal ? null : <HorizontalDivider style={{marginTop: 15, marginBottom: 17}} />}
      <Bottom>
        <Action onClick={onLike}>
          <Icon {...sizes} type={isLiked ? 'liked' : 'like'} color="#8F8F8F" />
          {totalLikes ? <span>{totalLikes}</span> : null}
        </Action>
        <Action onClick={onComment}>
          <Icon {...sizes} type="message" color="#8F8F8F" />
          {commentCount ? <span>{commentCount}</span> : null}
        </Action>
        <Action onClick={onShare} mla={isModal ? 0 : 1}>
          <Icon {...sizes} type="share" fill="#8F8F8F" />
          {/* <span>1</span> */}
        </Action>
        <Action onClick={onSaveToList}>
          <Icon {...sizes} type="bookmarkIcon" fill="#8F8F8F" />
          {/* <span>1</span> */}
        </Action>
      </Bottom>
      <AddToList showAddToList={showAddToList} setShowAddToList={setShowAddToList} />
    </Container>
  );
};

ItemInfo.defaultProps = {
  additionalStyles: null,
  isModal: false,
  displayComments: (f) => f
};

ItemInfo.propTypes = {
  additionalStyles: PropTypes.string,
  isModal: PropTypes.bool,
  displayComments: PropTypes.func,
  data: PropTypes.shape().isRequired
};

export default ItemInfo;
