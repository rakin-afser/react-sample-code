import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const Results = styled.div`
  font-family: Helvetica Neue;
  font-size: 14px;
  line-height: 16px;
  color: #000;
`;

export const RightBlock = styled.div`
  display: flex;
  align-items: center;
`;

export const ResTitle = styled.div`
  position: relative;
  margin-right: 27px;
  font-family: Helvetica Neue;
  font-size: 14px;
  line-height: 17px;
  font-weight: 500;
  color: #000;
`;

export const Badge = styled.div`
  position: absolute;
  right: -8px;
  top: 0;
  width: 6px;
  height: 6px;
  background-color: #ed484f;
  border-radius: 50%;
`;

export const Icon = styled.div`
  display: flex;
`;
