import React from 'react';
import PropTypes from 'prop-types';
import FilterIcon from 'assets/Filter';
import Chevron from 'assets/Chevron';

import {Container, Results, ResTitle, RightBlock, Badge, Icon} from './styled';
import useGlobal from '../../store';

const icons = {
  filter: <FilterIcon width={20} height={20} />,
  chevron: <Chevron />
};

const FilterBarMobile = ({results, title, resultLabel, redDot, iconName, onClick}) => {
  const [globalState, globalActions] = useGlobal();

  const handleClick = () => {
    globalActions.setProductFiltersPopup(true);
  };

  return (
    <Container>
      <Results>
        {results} {resultLabel}
      </Results>
      <RightBlock onClick={onClick || handleClick}>
        <ResTitle>
          {title}
          {redDot && <Badge />}
        </ResTitle>
        <Icon>{icons[iconName]}</Icon>
      </RightBlock>
    </Container>
  );
};

FilterBarMobile.defaultProps = {
  results: 0,
  title: 'Sort & Filter',
  resultLabel: 'Results',
  redDot: true,
  iconName: 'filter',
  onClick: () => {}
};

FilterBarMobile.propTypes = {
  results: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  title: PropTypes.string,
  resultLabel: PropTypes.string,
  redDot: PropTypes.bool,
  iconName: PropTypes.string,
  onClick: PropTypes.func
};

export default FilterBarMobile;
