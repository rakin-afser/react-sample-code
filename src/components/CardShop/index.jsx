import React from 'react';
import {string, number, bool} from 'prop-types';
import StarRatings from 'react-star-ratings';
import {FollowersCount} from 'globalStyles';
import {starFillColor} from 'constants/colors';
import {CardFooter, Image, Card, Title, FollowersContainer, ImageWrapper, Location} from './styled';
import productPlaceholder from 'images/placeholders/product.jpg';
import Btn from 'components/Btn';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';
import LazyLoad from 'react-lazyload';
import parse from 'html-react-parser';

const CardShop = ({id, title, imgSrc, rating, isFollowing, user, location, totalFollowers, onClick}) => {
  const [followUnfollowStore] = useFollowUnFollowStoreMutation({followed: isFollowing});
  return (
    <Card user={user}>
      <ImageWrapper user={user} onClick={onClick}>
        <LazyLoad once style={{height: '100%'}}>
          <Image src={imgSrc || productPlaceholder} alt="product" />
        </LazyLoad>
      </ImageWrapper>

      {user && (
        <>
          <Title>{parse(title || '')}</Title>
          <Location>{location}</Location>
        </>
      )}

      <CardFooter>
        <Title user={user}>{parse(title || '')}</Title>
        <FollowersContainer>
          <FollowersCount>{totalFollowers} followers</FollowersCount>
          <StarRatings
            numberOfStars={5}
            starRatedColor={starFillColor}
            rating={rating}
            starDimension="9px"
            starSpacing="1px"
          />
        </FollowersContainer>
        <Btn
          kind={isFollowing ? 'following' : 'follow'}
          onClick={() => followUnfollowStore({variables: {input: {id: parseInt(id)}}})}
        />
        {/* <FollowButton isfollowing={isFollowing ? 1 : 0}>
        {isFollowing ? (
          <div>Following</div>
        ) : (
          <FlexContainer>
            <Icon type="plus" />
            <div>Follow</div>
          </FlexContainer>
        )}
      </FollowButton> */}
      </CardFooter>
    </Card>
  );
};

CardShop.defaultProps = {
  isFollowing: false,
  user: false,
  location: ''
};

CardShop.propTypes = {
  title: string.isRequired,
  imgSrc: string.isRequired,
  rating: number.isRequired,
  isFollowing: bool,
  user: bool,
  location: string
};

export default CardShop;
