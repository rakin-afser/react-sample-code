import styled from 'styled-components';
import {mainWhiteColor, mainBlackColor} from 'constants/colors';
import {FlexContainer} from 'globalStyles';

export const Card = styled(FlexContainer)`
  position: relative;
  flex-direction: column;
  width: 172px;
  height: 212px;
  background: ${mainWhiteColor};
  cursor: pointer;
  box-shadow: 0 2px 9px rgba(0, 0, 0, 0.06);
  margin: 10px;

  ${({user}) =>
    user &&
    `
      padding-top: 20px;
  `}
`;

export const FollowersContainer = styled(FlexContainer)`
  width: 100%;
  margin-bottom: 9px;
`;

export const ImageWrapper = styled.div`
  width: 100%;
  max-height: 100px;

  ${({user}) =>
    user &&
    `
      width: 55px;
      height: 55px;
  `}
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const CardFooter = styled(FlexContainer)`
  width: 100%;
  height: 112px;
  flex-direction: column;
  padding: 9px 15px 15px 15px;
`;

export const Title = styled.div`
  font-size: 14px;
  font-weight: bold;
  color: ${mainBlackColor};

  ${({user}) =>
    user &&
    `
    opacity: 0;
  `};
`;

export const Location = styled.div`
  font-size: 12px;
  color: #000;
`;
