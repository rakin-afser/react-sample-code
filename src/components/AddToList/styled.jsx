import styled, {css} from 'styled-components';

export const Wrapper = styled.div`
  position: absolute;
  bottom: 0;
  right: -8px;
  padding: 12px 16px 22px;
  background: #ffffff;
  border-radius: 8px;
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.28);
  width: 100vw;
  max-width: 212px;
  z-index: 100;

  &.enter {
    transform: translateY(20px);
    opacity: 0;
  }

  &.enter-active {
    opacity: 1;
    transform: translateY(0);
    transition: opacity ease 300ms, transform 300ms;
  }

  &.exit-active {
    opacity: 1;
  }

  &.exit {
    opacity: 0;
    transform: translateY(20px);
    transition: opacity ease 300ms, transform 300ms;
  }

  ${({savedToList}) =>
    savedToList &&
    css`
      bottom: 0;
      padding: 8px 16px;
    `}
`;

export const Title = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  margin-bottom: 18px;
`;

export const Close = styled.div`
  position: absolute;
  top: 12px;
  right: 8px;

  svg {
    fill: #666;
  }
`;

export const SearchContainer = styled.div`
  position: relative;
  margin: 0 0 14px;
  width: 100%;
  max-width: 100%;

  svg {
    position: absolute;
    left: 10px;
    top: 50%;
    transform: translate(0, -50%);
    max-width: 14px;
    max-height: 14px;
  }
`;

export const Search = styled.input`
  background: #efefef;
  width: 100%;
  height: 36px;
  border-radius: 4px;
  border: 0;
  padding: 0 16px 0 32px;
  outline: none;
  color: #000;
  font-size: 12px;

  &:placeholder {
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 17px;
    color: #999999;
  }
`;

export const SearchReset = styled.span`
  position: absolute;
  right: 10px;
  top: 50%;
  transform: translate(0, -50%);
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background: #cccccc;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;

  svg {
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const List = styled.div`
  margin: 0 -16px;
  width: calc(100% + 32px);

  .ScrollbarsCustom-TrackY {
    right: 14px !important;
    width: 4px !important;
    background: rgba(155, 155, 155, 0.2) !important;
    border-radius: 2px !important;
  }

  .ScrollbarsCustom-ThumbY {
    background: #c3c3c3 !important;
    border-radius: 2px !important;
  }
`;

export const ListItem = styled.div`
  height: 28px;
  position: relative;
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;
  padding: 0 16px;
  color: #666;
  background: #fff;
  transition: all 0.3s ease;

  &:hover {
    background: #f6f6f6;
  }

  ${({privated}) =>
    privated &&
    `
    padding: 0 0 0 48px;
    position: relative;
    font-weight: 500;
    font-size: 14px;
    line-height: 140%;

    &:hover {
      background: #fff;
    }
  `}

  ${({noFound}) =>
    noFound &&
    `
    justify-content: center;
    padding-top: 52px;

    &:hover {
      background: #fff;
    }
  `}
`;

export const ListItemCheck = styled.span`
  position: absolute;
  top: 50%;
  left: 16px;
  transform: translate(0, -50%);
  width: 20px;
  height: 20px;
  background: #fff;
  border: 1px solid #666;
  border-radius: 4px;
  transition: all 0.3s ease;
  display: flex;
  align-items: center;
  justify-content: center;

  i {
    max-width: 18px;
    max-height: 8px;
  }

  svg {
    max-width: 18px;
    max-height: 8px;
    fill: #fff;
  }

  ${({checked}) =>
    checked &&
    `
    background: #000;
    border: 1px solid #000;
  `}
`;

export const ListItemLock = styled.span`
  margin-left: 12px;
  margin-bottom: -2px;

  svg {
    max-width: 12px;
    max-height: 12px;
    stroke: #666666;
  }
`;

export const AddNewList = styled.button`
  outline: none;
  height: 42px;
  width: 100%;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  background: #fafafa;
  border: 1px solid #efefef;
  border-radius: 4px;
  margin-top: 16px;
  margin-left: 0 !important;
  margin-right: 0 !important;

  font-weight: 500;
  font-size: 13px;
  line-height: 16px;
  color: #000000;

  svg {
    width: 100px;
    height: 100px;
    max-width: 14px;
    max-height: 14px;
    margin-right: 12px;
  }
`;

export const AddedToWishlist = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #666666;
  display: flex;
  align-items: center;
  margin-left: auto;

  span {
    opacity: 0.8;
  }
`;

export const AddedToWishlistIcon = styled.div`
  width: 12px;
  height: 12px;
  border: 1px solid #2ecc71;
  border-radius: 50%;
  margin-right: 4px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;

  svg {
    fill: #2ecc71;
    max-width: 6px;
    max-height: 5px;
  }
`;

export const RemoveFromListIcon = styled.div`
  width: 12px;
  height: 12px;
  border: 1px solid #000;
  border-radius: 50%;
  margin-right: 4px;
  display: flex;
  align-items: center;
  justify-content: center;

  svg {
    fill: #000;
    max-width: 8px;
    max-height: 8px;
  }
`;

export const Buttons = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-top: 27px;
`;

export const CreateButton = styled.button`
  outline: none;
  cursor: pointer;
  width: 80px;
  height: 28px;
  background: #ed484f;
  border-radius: 24px;
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #ffffff;
  cursor: pointer;
  padding: 0;
  border: 0;
  margin: 0 !important;
`;

export const CancelButton = styled(CreateButton)`
  outline: none;
  cursor: pointer;
  padding: 0;
  background: transparent;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #666666;
  cursor: pointer;
  border: 0;
`;

export const FieldGroup = styled.div`
  margin-bottom: 19px;
`;

export const FieldLabel = styled.label`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  margin-bottom: 8px;
  display: block;
`;

export const Field = styled.input`
  outline: none;
  width: 100%;
  border: 1px solid #a7a7a7;
  border-radius: 2px;
  height: 40px;
  padding: 0 16px;
  font-size: 14px;
  line-height: 17px;
  color: #000;

  &:placeholder {
    font-size: 14px;
    line-height: 17px;
    color: #cccccc;
  }
`;

export const SavedTo = styled.div`
  display: flex;
  align-items: center;

  strong {
    display: block;
    font-weight: normal;
    font-size: 14px;
    line-height: 1.4;
    color: #666666;
    padding-left: 8px;
  }

  span {
    display: block;
    font-weight: 500;
    font-size: 12px;
    line-height: 15px;
    color: #545454;
    margin-top: 4px;

    svg {
      margin-left: 4px;
      stroke: #666666;
      max-width: 12px;
      max-height: 12px;
    }
  }

  i {
    position: relative;
    top: 1px;
  }

  a {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 17px;
    text-align: right;
    color: #4a90e2;
    margin-left: auto;
  }
`;

export const SavedIcon = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #2ecc71;
  flex-shrink: 0;

  svg {
    max-width: 11px;
    max-height: 8px;
    fill: #2ecc71;
  }
`;
