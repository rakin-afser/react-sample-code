import React, {useState, useRef, useEffect} from 'react';
import Scrollbar from 'react-scrollbars-custom';
import {useLazyQuery, useMutation} from '@apollo/client';

import Icon from 'components/Icon';
import {
  Wrapper,
  Title,
  Close,
  Search,
  SearchContainer,
  SearchReset,
  List,
  ListItem,
  ListItemLock,
  AddNewList,
  AddedToWishlist,
  Buttons,
  CancelButton,
  CreateButton,
  Field,
  FieldGroup,
  FieldLabel,
  ListItemCheck,
  SavedIcon,
  SavedTo,
  RemoveFromListIcon
} from './styled';
import {useUser} from 'hooks/reactiveVars';
import SearchIcon from 'assets/Search';
import CloseIcon from 'assets/CloseIcon';
import {
  GET_LISTS_BY_USER,
  ADD_LIST,
  ADD_PRODUCT_TO_LIST,
  DELETE_PRODUCT_FROM_LIST
} from 'components/AddToList/api/queries';
import {CSSTransition} from 'react-transition-group';
import Loader from 'components/Loader';

const AddToListContent = ({showAddToList, setShowAddToList, productId, wrapperStyle, onAdded}) => {
  const [user] = useUser();
  const userId = user?.databaseId;

  const [getListsByUser, {data: listsData, loading: listsLoading, error: listsError}] = useLazyQuery(
    GET_LISTS_BY_USER,
    {variables: {where: {user_id: +userId}}}
  );

  const [addNewList, {data: addListData, loading: addListLoading, error: addListError}] = useMutation(ADD_LIST, {
    variables: {userId: +userId}
  });

  const [
    addProductToList,
    {data: addProductToListData, loading: addProductToListLoading, error: addProductToListError}
  ] = useMutation(ADD_PRODUCT_TO_LIST, {variables: {userId: +userId}});

  const [
    deleteProductFromList,
    {data: deleteProductFromListData, loading: deleteProductFromListLoading, error: deleteProductFromListError}
  ] = useMutation(DELETE_PRODUCT_FROM_LIST, {variables: {userId: +userId}});

  const wrapper = useRef();
  const searchInput = useRef();

  const [defaultList, setDefaultList] = useState([]);
  const [loading, setLoading] = useState(null);
  const [searchValue, setSearchValue] = useState('');
  const [checkedList, setCheckedList] = useState([]);
  const [savedToList, setSavedToList] = useState(null);
  const [removedFromList, setRemovedFromList] = useState(null);
  const [willPrivate, setWillPrivate] = useState(false);
  const [newList, setNewList] = useState('');
  const [createNewList, setCreateNewList] = useState(false);
  const [list, setList] = useState(defaultList);
  const [timerID, setTimerID] = useState(null);

  useEffect(() => {
    if (user?.email) {
      getListsByUser();
    }
  }, [user?.databaseId]);

  const renderClose = () => {
    return (
      <Close onClick={() => setShowAddToList(false)}>
        <Icon type="close" svgStyle={{width: 18, height: 18, fill: '#666'}} />
      </Close>
    );
  };

  const renderAddToList = () => {
    function renderList() {
      if (listsLoading) {
        return <Loader wrapperWidth="100%" wrapperHeight="auto" />;
      }

      return listsData && listsData?.lists?.nodes?.length ? (
        listsData?.lists?.nodes
          ?.filter((item) => item?.list_name.toLowerCase().includes(searchValue.toLowerCase()))
          ?.map((item, key) => {
            const checkAdded = () => {
              return (
                item.addedProducts?.nodes?.length &&
                item.addedProducts?.nodes?.map((item) => item?.databaseId)?.includes(productId)
              );
            };

            return (
              <ListItem
                key={key}
                private={item.is_private}
                checked={checkedList.indexOf(item.list_id) >= 0}
                onClick={() => {
                  setLoading(item.list_id);

                  if (checkAdded()) {
                    deleteProductFromList({
                      variables: {listId: item.list_id, productId},
                      update(cache, {newData}) {
                        cache.modify({
                          fields: {
                            lists() {
                              return newData;
                            }
                          }
                        });
                      }
                    });

                    // delay 400ms after answer from Server
                    setTimeout(() => {
                      setLoading(null);
                      setRemovedFromList(item.list_name);
                      onAdded(false);
                    }, 500);
                  } else {
                    addProductToList({
                      variables: {listId: item.list_id, productId},
                      update(cache, {newData}) {
                        cache.modify({
                          fields: {
                            lists() {
                              return newData;
                            }
                          }
                        });
                      }
                    });
                    // setCheckedList(checkedList);

                    // delay 400ms after answer from Server
                    setTimeout(() => {
                      setLoading(null);
                      setSavedToList(item.list_name);
                      onAdded(true);
                    }, 500);
                  }
                }}
              >
                {item.list_name}
                {item.is_private && (
                  <ListItemLock>
                    <Icon type="lock" />
                  </ListItemLock>
                )}
                {loading === item.list_id && (
                  <AddedToWishlist>
                    <Icon type="loader" svgStyle={{width: 20, height: 4, fill: '#ED484F'}} />
                  </AddedToWishlist>
                )}
                {checkAdded() && loading !== item.list_id ? (
                  <AddedToWishlist>
                    <RemoveFromListIcon>
                      <Icon type="close" svgStyle={{color: '#000'}} />
                    </RemoveFromListIcon>
                    <span>Remove</span>
                  </AddedToWishlist>
                ) : /* <AddedToWishlist>
                            <AddedToWishlistIcon>
                              <Icon type="checkbox" />
                            </AddedToWishlistIcon>
                            <span>Added</span>
                          </AddedToWishlist> */
                null}
              </ListItem>
            );
          })
      ) : (
        <ListItem noFound>{user?.email ? 'Nothing was found!' : 'Sign in to testSample'}</ListItem>
      );
    }

    return (
      <>
        <Title>Add to List</Title>
        {renderClose()}

        <SearchContainer>
          <SearchIcon color="#999999" />
          <Search
            value={searchValue}
            onChange={(e) => {
              const {value} = e.target;

              const merge = (a, b, prop) => {
                const reduced = a.filter((aItem) => !b.find((bItem) => aItem[prop] === bItem[prop]));
                return reduced.concat(b);
              };

              setSearchValue(value);

              if (value.length > 0) {
                const result = merge(list, defaultList, 'title').filter((o) => o.title.includes(e.target.value));

                setList(result);
              } else {
                setList(defaultList);
              }
            }}
            ref={searchInput}
            placeholder="Search your Lists"
          />

          {searchValue.length > 0 ? (
            <SearchReset
              onClick={() => {
                setSearchValue('');
                setList(defaultList);
              }}
            >
              <CloseIcon width={12} height={12} color="#fff" />
            </SearchReset>
          ) : null}
        </SearchContainer>

        <List>
          <Scrollbar
            disableTracksWidthCompensation
            style={{height: 112}}
            trackXProps={{
              renderer: (props) => {
                const {elementRef, ...restProps} = props;
                return <span {...restProps} ref={elementRef} className="TrackX" />;
              }
            }}
          >
            {renderList()}
          </Scrollbar>
        </List>

        <AddNewList
          onClick={() => {
            setCreateNewList(true);
          }}
        >
          <Icon type="plus" />
          <span>Create New List</span>
        </AddNewList>
      </>
    );
  };

  const renderCreateNewList = () => {
    return (
      <>
        <Title>Create New List</Title>
        {renderClose()}

        <FieldGroup>
          <FieldLabel htmlFor="list">Enter List Name</FieldLabel>
          <Field
            id="list"
            value={newList}
            onChange={(e) => {
              setNewList(e.target.value);
            }}
            placeholder="Your List Name"
          />
        </FieldGroup>

        <List>
          <ListItem
            privated
            checked={willPrivate}
            onClick={() => {
              setWillPrivate(!willPrivate);
            }}
          >
            <ListItemCheck checked={willPrivate}>
              <Icon type="checkbox" />
            </ListItemCheck>
            Private{' '}
            <ListItemLock>
              <Icon type="lock" />
            </ListItemLock>
          </ListItem>
        </List>

        <Buttons>
          <CancelButton
            onClick={() => {
              setCreateNewList(false);
            }}
          >
            Cancel
          </CancelButton>
          <CreateButton
            onClick={() => {
              addNewList({
                variables: {listName: newList, isPrivate: willPrivate},
                update(cache, {newData}) {
                  cache.modify({
                    fields: {
                      lists() {
                        return newData;
                      }
                    }
                  });
                }
              });
              setCreateNewList(false);
            }}
          >
            Create
          </CreateButton>
        </Buttons>
      </>
    );
  };

  const renderSavedToList = () => {
    return (
      <>
        <SavedTo>
          <SavedIcon>
            <Icon type="checkbox" />
          </SavedIcon>
          <strong>
            Saved to List{' '}
            <span>
              {savedToList} {savedToList.private && <Icon type="lock" />}
            </span>
          </strong>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              setSavedToList(null);
            }}
          >
            Change
          </a>
        </SavedTo>
      </>
    );
  };

  const renderRemovedFromList = () => {
    return (
      <>
        <SavedTo>
          <SavedIcon>
            <Icon type="checkbox" />
          </SavedIcon>
          <strong>
            Removed from list{' '}
            <span>
              {removedFromList} {removedFromList.private && <Icon type="lock" />}
            </span>
          </strong>
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault();
              setRemovedFromList(null);
            }}
          >
            Change
          </a>
        </SavedTo>
      </>
    );
  };

  function handleClickOutside(event) {
    if (wrapper.current && !wrapper.current.contains(event.target) && showAddToList) {
      setShowAddToList(false);

      setTimeout(() => {
        setCreateNewList(false);
        setSavedToList(null);
        setRemovedFromList(null);
        setSearchValue('');
        setNewList('');
        setWillPrivate(false);
        setList(defaultList);
      }, 300);
    }
  }

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
      timerID && clearTimeout(timerID);
    };
  });

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (savedToList) {
        setShowAddToList(false);
      }
      if (removedFromList) {
        setShowAddToList(false);
      }
    }, 3000);

    return () => clearTimeout(timeout);
  }, [savedToList, removedFromList, setShowAddToList]);

  useEffect(() => {
    const timeout = setTimeout(() => {
      if (!showAddToList) {
        setSavedToList(null);
        setRemovedFromList(null);
        setCreateNewList(null);
      }
    }, 400);

    return () => clearTimeout(timeout);
  }, [showAddToList]);

  useEffect(() => {
    if (searchValue && searchValue.length > 0 && list.length === 0) {
      setNewList(searchValue);
    } else {
      setNewList('');
    }
  }, [searchValue, list]);

  const render = () => {
    if (!user?.databaseId) {
      return <div style={{textAlign: 'center', paddingTop: '10px'}}>Please sign in to testSample</div>;
    }

    if (removedFromList) {
      return renderRemovedFromList();
    }

    if (savedToList) {
      return renderSavedToList();
    }

    if (createNewList) {
      return renderCreateNewList();
    }

    return renderAddToList();
  };

  return (
    <Wrapper style={wrapperStyle} ref={wrapper} savedToList={savedToList || removedFromList}>
      {render()}
    </Wrapper>
  );
};

const AddToList = (props) => {
  const {showAddToList} = props;
  return (
    <CSSTransition in={showAddToList} timeout={300} unmountOnExit>
      <AddToListContent {...props} />
    </CSSTransition>
  );
};

export default AddToList;
