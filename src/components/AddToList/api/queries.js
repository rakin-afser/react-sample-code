import {gql} from '@apollo/client';

export const GET_LISTS_BY_USER = gql`
  query GetListsByUser($where: ListSearchWhere) {
    lists(first: 20, where: $where) {
      nodes {
        id
        user_id
        list_name
        list_description
        list_id
        is_private
        # hashtags #disable for now
        addedProducts {
          nodes {
            id
            databaseId
          }
        }
      }
    }
  }
`;

export const ADD_LIST = gql`
  mutation AddList($listName: String!, $userId: Int, $isPrivate: Boolean) {
    addList(input: {list_name: $listName, list_description: "", user_id: $userId, is_private: $isPrivate}) {
      clientMutationId
      status
    }
  }
`;

export const ADD_PRODUCT_TO_LIST = gql`
  mutation AddProductToList($listId: Int!, $productId: Int!, $userId: Int) {
    addProductToList(input: {id: $listId, product_id: $productId, user_id: $userId}) {
      clientMutationId
      status
    }
  }
`;

export const DELETE_PRODUCT_FROM_LIST = gql`
  mutation DeleteProductFromList($listId: Int!, $productId: Int!, $userId: Int) {
    deleteProductFromList(input: {id: $listId, product_id: $productId, user_id: $userId}) {
      clientMutationId
      status
    }
  }
`;
