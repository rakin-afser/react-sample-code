import avatar from 'images/avatar.png';

export const data = [
  {
    author: 'Leonardo Dicaprio',
    date: 'a month ago',
    message: 'Colors will depend heavily on the setting the character who wears the colores is in.',
    likes: 16,
    avatar: avatar
  },
  {
    author: 'Gordon Rivera',
    date: 'a month ago',
    message: 'Colors will depend heavily on the setting the character who wears the colores is in.',
    likes: 16,
    avatar: avatar,
    replies: [
      {
        author: 'Leonardo Dicaprio',
        replyTo: 'gigihadid',
        date: 'a month ago',
        message: 'What do you mean?',
        likes: 16,
        avatar: avatar
      },
      {
        author: 'Leonardo Dicaprio',
        replyTo: 'gigihadid',
        date: 'a month ago',
        message: 'Very nice!',
        likes: 16,
        avatar: avatar
      }
    ]
  },
  {
    author: 'Leonardo Dicaprio',
    date: 'a month ago',
    message: 'Colors will depend heavily on the setting the character who wears the colores is in.',
    likes: 16,
    avatar: avatar
  },
  {
    author: 'Leonardo Dicaprio',
    date: 'a month ago',
    message: 'Colors will depend heavily on the setting the character who wears the colores is in.',
    likes: 16,
    avatar: avatar
  },
  {
    author: 'Leonardo Dicaprio',
    date: 'a month ago',
    message: 'Colors will depend heavily on the setting the character who wears the colores is in.',
    likes: 16,
    avatar: avatar
  },
  {
    author: 'Leonardo Dicaprio',
    date: 'a month ago',
    message: 'Colors will depend heavily on the setting the character who wears the colores is in.',
    likes: 16,
    avatar: avatar
  },
  {
    author: 'Leonardo Dicaprio',
    date: 'a month ago',
    message: 'Colors will depend heavily on the setting the character who wears the colores is in.',
    likes: 16,
    avatar: avatar
  }
];
