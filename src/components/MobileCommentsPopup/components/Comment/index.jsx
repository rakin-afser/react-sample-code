import moment from 'moment';
import parse from 'html-react-parser';
import React from 'react';
import {
  Wrapper,
  CommentAvatar,
  CommentContent,
  CommentName,
  CommentMessage,
  CommentTools,
  CommentLikes,
  CommentReply,
  Report,
  Replies
} from './styled';
import Icon from 'components/Icon';
import {useAddUpdateLikedCommentMutation} from 'hooks/mutations';
import useGlobal from 'store';

const Comment = (props) => {
  const [, {setReportPopup}] = useGlobal();
  const {comment, onReply, isReply} = props;
  const {databaseId, author, date, content, replies, isLiked, totalLikes} = comment || {};
  const [triggerLike] = useAddUpdateLikedCommentMutation({comment});

  return (
    <Wrapper isReply={isReply}>
      <CommentAvatar src={author?.node?.avatar?.url || ''} />
      <CommentContent>
        <CommentName>
          {author?.node?.name}
          <span>{moment(date).format('DD MMM YYYY')}</span>
        </CommentName>
        <CommentMessage>{parse(content || '')}</CommentMessage>
        <CommentTools>
          <CommentLikes>
            <Icon
              onClick={() => triggerLike({variables: {input: {id: databaseId}}})}
              type={isLiked ? 'liked' : 'like'}
              width={13}
              height={13}
              color="#333"
            />
            <span>{totalLikes}</span>
          </CommentLikes>
          {replies?.nodes?.length ? <Replies></Replies> : null}
          <CommentReply onClick={onReply}>
            {/* <Icon type="message" width={13} height={13} color="#333" /> */}
            Reply
          </CommentReply>
          <Report
            onClick={() =>
              setReportPopup({
                active: true,
                subjectId: databaseId,
                subjectSource: 'comment_report'
              })
            }
          >
            <Icon type="flag" />
          </Report>
        </CommentTools>
      </CommentContent>
    </Wrapper>
  );
};

Comment.defaultProps = {
  isReply: false
};

export default Comment;
