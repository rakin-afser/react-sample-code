import styled, {css} from 'styled-components';

export const Wrapper = styled.div`
  padding: 16px;
  display: flex;
  width: 100%;
  flex-wrap: wrap;

  &:not(:first-child) {
    border-top: 1px solid rgba(0, 0, 0, 0.08);
  }

  ${({isReply}) => (isReply ? `padding-left: 49px;` : '')}
`;

export const CommentAvatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  object-fit: cover;
  ${({small}) =>
    small
      ? css`
          width: 30px;
          height: 30px;
          margin-right: 10px;
        `
      : null}
`;

export const CommentContent = styled.div`
  width: calc(100% - 40px);
  padding: 0 0 0 10px;
`;

export const CommentName = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  margin: 0 0 4px 0;
  display: flex;
  align-items: center;

  span {
    margin-left: auto;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 14px;
    color: #999999;
  }
`;

export const CommentMessage = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  margin: 0;
`;

export const CommentTools = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;
`;

export const CommentLikes = styled.div`
  display: flex;
  align-items: center;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #000000;

  svg {
    fill: #333333;
  }

  span {
    margin-left: 3px;
  }
`;

export const CommentReply = styled.span`
  margin-left: 12px;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 1;
  color: #999999;
  display: inline-flex;
  align-items: center;

  & span {
    color: #000;
    margin-right: 8px;
    margin-left: 8px;
    font-weight: 400;
  }
`;

export const CommentSubComments = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  border-top: 1px solid #efefef;
  margin-top: 16px;
  padding-top: 16px;
  padding-left: 70px;
`;

export const Mention = styled.span`
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #4a90e2;
`;

export const Report = styled.span`
  display: inline-block;
  margin-left: 18px;
  color: #999;
  transition: color 0.3s ease-in;
  line-height: 1;
  position: relative;
  top: 1px;

  &:hover {
    color: #ed484f;
  }
`;

export const Replies = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #999999;
`;
