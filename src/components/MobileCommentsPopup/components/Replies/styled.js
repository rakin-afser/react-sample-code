import styled from 'styled-components';

export const Wrapper = styled.div`
  position: absolute;
  top: 60px;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 11;
  background-color: #fff;
  display: flex;
  flex-direction: column;

  &.enter {
    transform: translateX(100%);
    opacity: 0;
  }

  &.enter-active {
    opacity: 1;
    transform: translateX(0);
    transition: opacity ease 300ms, transform 300ms;
  }

  &.exit-active {
    opacity: 1;
  }

  &.exit {
    opacity: 0;
    transform: translateX(100%);
    transition: opacity ease 300ms, transform 300ms;
  }
`;

export const CommentsHolder = styled.div`
  overflow-y: auto;
`;
