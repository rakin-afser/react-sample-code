import React, {useRef, useState} from 'react';
import {AddCommentForm, AddCommentAvatar, AddCommentField, AddCommentSend} from '../../styled';
import {Wrapper, CommentsHolder} from './styled';
import Icon from 'components/Icon';
import {useCreateCommentMutation} from 'hooks/mutations';
import {useUser} from 'hooks/reactiveVars';
import userPlaceholder from 'images/placeholders/user.jpg';
import {endElement} from 'util/elements';
import {useRelatedUsersLazyQuery, useReplyQuery} from 'hooks/queries';
import Comment from '../Comment';
import CustomMentions from 'components/CustomMentions';

const Replies = (props) => {
  const contentRef = useRef(null);
  const [commentText, setCommentText] = useState('');
  const {reply, commentOn} = props;
  const {id} = reply || {};
  const [endRef, {data, loading}] = useReplyQuery({variables: {id, first: 10}, contentRef, nextFirst: 5});
  const {hasNextPage} = data?.comment?.replies?.pageInfo || {};
  const {replies} = data?.comment || {};
  const [user] = useUser();
  const [getRelatedUsers, {data: dataRelated, loading: loadingRelated}] = useRelatedUsersLazyQuery();
  const {followers, following} = dataRelated?.user || {};
  const relatedUsers = [...(followers || []), ...(following || [])]?.filter(
    (p, i, a) => a.findIndex((pr) => pr.id === p.id) === i
  );

  const [createComment, {loading: createCommentLoading}] = useCreateCommentMutation({
    onCompleted() {
      setCommentText('');
    },
    commentId: id
  });

  const onCreateComment = (content) => {
    const {databaseId} = reply || {};
    createComment({
      variables: {input: {content, parent: databaseId, commentOn}}
    });
  };

  function onReply(rpl) {
    const {username} = rpl?.author?.node || {};
    setCommentText(`@${username} `);
  }

  return (
    <Wrapper>
      <Comment comment={reply} />
      {user?.databaseId ? (
        <AddCommentForm>
          {/* <AddCommentAvatar src={user?.avatar?.url || userPlaceholder} /> */}
          <CustomMentions
            loading={loadingRelated}
            onSearch={(v) => getRelatedUsers({variables: {search: v}})}
            value={createCommentLoading ? '' : commentText}
            onChange={(v) => setCommentText(v)}
            placeholder="Add a comment"
            relations={relatedUsers}
          />
          <AddCommentSend onClick={() => onCreateComment(commentText)} disabled={!(commentText && commentText.length)}>
            {createCommentLoading ? <Icon type="loader" color="#000" width={20} height={20} /> : <Icon type="send" width={14} height={12}/>}
          </AddCommentSend>
        </AddCommentForm>
      ) : null}
      <CommentsHolder ref={contentRef}>
        {replies?.nodes?.map((rpl) => (
          <Comment key={rpl?.id} comment={rpl} onReply={() => onReply(rpl)} />
        ))}
        {endElement(hasNextPage, loading, endRef)}
      </CommentsHolder>
    </Wrapper>
  );
};

export default Replies;
