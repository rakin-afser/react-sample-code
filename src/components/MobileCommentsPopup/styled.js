import styled, {css} from 'styled-components';
import {primaryColor} from 'constants/colors';

export const AddCommentForm = styled.div`
  display: flex;
  align-items: center;
  margin: 11px 16px;
  position: relative;
  height: 40px;
  background-color: #fff;
  border: 1px solid #efefef;
  box-sizing: border-box;
  border-radius: 4px;

  &:before {
    content: '';
    position: absolute;
    bottom: calc(100% + 13px);
    left: -16px;
    right: -16px;
    height: 1px;
    background: rgba(0, 0, 0, 0.08);
  }
  ${({absolute}) =>
    absolute
      ? css`
          position: absolute;
          bottom: 0;
          left: 0;
          right: 0;
          z-index: 10;
        `
      : css``}
`;

export const AddCommentAvatar = styled.img`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  object-fit: cover;
`;

export const AddCommentField = styled.textarea`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #000;
  height: 65px;
  line-height: 1.3;
  padding: 20px 10px;
  border: 0;
  outline: none;
  width: calc(100% - 40px - 32px);
  resize: none;
  background-color: transparent;

  &::placeholder {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    color: #999999;
  }
`;

export const AddCommentSend = styled.button`
  width: 24px;
  height: 24px;
  background: ${primaryColor};
  border: 0;
  outline: none;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 50%;
  transition: all 0.3s ease;

  &:disabled {
    cursor: not-allowed;
    background: #cccccc;
  }

  svg {
    fill: #fff;
    position: relative;
    top: 1px;
    left: 1px;
  }
`;

export const Wrapper = styled.div`
  margin: 0 -16px 0;
  height: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: column;
`;

export const NoComments = styled.div`
  margin: auto;
  text-align: center;

  .text {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    text-align: center;
    color: #999999;
    max-width: 224px;
    margin: 44px auto 0;
    display: block;
  }
`;
