import React, {useState} from 'react';
import Icon from 'components/Icon';
import {AddCommentForm, AddCommentAvatar, AddCommentField, AddCommentSend, Wrapper, NoComments} from './styled';
import useGlobal from 'store';
import userPlaceholder from 'images/placeholders/user.jpg';
import {useUser} from 'hooks/reactiveVars';
import {useCreateCommentMutation} from 'hooks/mutations';
import {CSSTransition} from 'react-transition-group';
import Replies from './components/Replies';
import {endElement} from 'util/elements';
import {useCommentsOfPostQuery, useRelatedUsersLazyQuery} from 'hooks/queries';
import Comment from './components/Comment';
import CustomMentions from 'components/CustomMentions';

import noComments from 'images/svg/no-comments.svg';

const Popup = ({postId, commentOn, reply, setReply, contentRef}) => {
  const [endRef, {data, loading}] = useCommentsOfPostQuery({
    variables: {
      id: postId,
      first: 10
    },
    contentRef,
    nextFirst: 5
  });
  const {hasNextPage} = data?.post?.comments?.pageInfo || {};

  const {comments} = data?.post || {};
  const [, {setIsRequireAuth}] = useGlobal();
  const [user] = useUser();
  const [commentText, setCommentText] = useState('');
  const [createComment, {loading: createCommentLoading}] = useCreateCommentMutation({
    onCompleted() {
      setCommentText('');
    },
    postId,
    commentId: reply?.id
  });
  const [getRelatedUsers, {data: dataRelated, loading: loadingRelated}] = useRelatedUsersLazyQuery();
  const {followers, following} = dataRelated?.user || {};
  const relatedUsers = [...(followers || []), ...(following || [])]?.filter(
    (p, i, a) => a.findIndex((pr) => pr.id === p.id) === i
  );

  const onCreateComment = (content) => {
    createComment({
      variables: {input: {content, parent: reply?.databaseId, commentOn}}
    });
  };

  function onReply(comment) {
    if (!user?.databaseId) {
      setIsRequireAuth(true);
    } else {
      setReply({...comment, active: true});
    }
  }

  // function renderInput(replies) {
  //   return replies?.nodes?.map((rpl) => {
  //     const {id, databaseId, author, totalLikes, date, content} = rpl;
  //     return (
  //       <CommentSubComments key={id}>
  //         <CommentAvatar src={author?.node?.avatar?.url || userPlaceholder} />
  //         <CommentContent>
  //           <CommentName>
  //             {author?.node?.name}
  //             <span>{moment(date).format('DD MMM YYYY')}</span>
  //           </CommentName>
  //           <CommentMessage>{parse(content || '')}</CommentMessage>

  //           <CommentTools>
  //             <CommentLikes>
  //               <Icon onClick={() => onLike(databaseId)} type="like" width={13} height={13} color="#333" />
  //               <span>{totalLikes}</span>
  //             </CommentLikes>
  //             <Report
  //               onClick={() =>
  //                 setReportPopup({
  //                   active: true,
  //                   subjectId: databaseId,
  //                   subjectSource: 'comment_report'
  //                 })
  //               }
  //             >
  //               <Icon type="flag" />
  //             </Report>

  //             <CommentReply onClick={() => {}}>
  //               <Icon type="message" />
  //               Reply
  //             </CommentReply>
  //           </CommentTools>
  //         </CommentContent>
  //       </CommentSubComments>
  //     );
  //   });
  // }

  return (
    <>
      <Wrapper>
        {
          comments?.nodes?.length && !loading
          ? 
            comments?.nodes?.map((comment) => {
              console.log(comment);
              if (comment.replies?.nodes) 
              return <>
                <Comment key={comment?.id} comment={comment} onReply={() => onReply(comment)} />
                {
                  comment.replies?.nodes?.map((reply) => {
                    return <Comment key={reply?.id} comment={reply} onReply={() => onReply(reply)} isReply />;
                  })
                }
              </>
            })
          : 
            !loading ?
              <NoComments>
                <img src={noComments} alt="There are no comments here yet. Be the first to comment" />
                <span className="text">There are no comments here yet. Be the first to comment</span>
              </NoComments>
            : null
        }
        {endElement(hasNextPage, loading, endRef)}
      </Wrapper>
      <CSSTransition in={reply?.active} timeout={300} unmountOnExit onExited={() => setReply(null)}>
        <Replies reply={reply} commentOn={commentOn} />
      </CSSTransition>
      {user?.databaseId ? (
        <AddCommentForm absolute={1}>
          {/* <AddCommentAvatar src={user?.avatar?.url || userPlaceholder} /> */}
          <CustomMentions
            loading={loadingRelated}
            onSearch={(v) => getRelatedUsers({variables: {search: v}})}
            value={createCommentLoading ? '' : commentText}
            onChange={(v) => setCommentText(v)}
            placeholder="Leave a comment..."
            relations={relatedUsers}
          />
          {/* <AddCommentField
            value={createCommentLoading ? '' : commentText}
            onChange={(e) => setCommentText(e.target.value)}
            placeholder="Add a comment"
          /> */}
          <AddCommentSend onClick={() => onCreateComment(commentText)} disabled={!(commentText && commentText.length)}>
            {createCommentLoading ? <Icon type="loader" color="#000" width={20} height={20} /> : <Icon type="send" width={14} height={12} />}
          </AddCommentSend>
        </AddCommentForm>
      ) : null}
    </>
  );
};

// Popup.defaultProps = {
//   title: 'Returns & Exchanges',
//   showCommentsPopup: false,
//   setShowCommentsPopup: () => {}
// };

// Popup.propTypes = {
//   title: string,
//   showCommentsPopup: bool,
//   setShowCommentsPopup: func
// };

export default Popup;
