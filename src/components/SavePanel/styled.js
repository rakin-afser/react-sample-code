import {Button} from 'antd';
import styled from 'styled-components';

export const Footer = styled.div`
  margin-top: ${({mt}) => mt && mt};
  margin-right: 20px;
  padding: 16px ${({px}) => `${px ? px : 16}px`};
  display: flex;
  box-shadow: inset 0px 1px 1px rgba(0, 0, 0, 0.03);
`;

export const StyledButton = styled(Button)`
  &&& {
    height: 40px;
    border-radius: 50px;
    font-weight: 500;
  }
`;

export const Cancel = styled(StyledButton)`
  &&& {
    color: #000;
    width: 100%;
    max-width: 120px;
    border: none;
  }
`;

export const BtnWrapper = styled.div`
  display: flex;
  justify-content: center;
  flex: 1 1 auto;
`;

export const Save = styled(StyledButton)`
  &&& {
    background-color: #ed484f;
    border-color: #ed484f;
    color: #fff;
    width: 100%;
    max-width: 186px;
  }
`;
