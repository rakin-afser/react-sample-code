import React from 'react';
import {Footer, BtnWrapper, Cancel, Save} from './styled';
import Icon from 'components/Icon';

const SavePanel = ({
  px = '',
  mt = '',
  saveTitle = 'Save',
  className = '',
  onSave = () => {},
  onCancel = () => {},
  isLoading = false
}) => {
  return (
    <Footer className={className} mt={mt} px={px}>
      <BtnWrapper>
        <Cancel onClick={() => onCancel()}>Cancel</Cancel>
      </BtnWrapper>
      <Save onClick={() => onSave()}>
        {isLoading ? <Icon type="loader" svgStyle={{width: 24, height: 8, fill: '#fff'}} /> : null}
        {saveTitle}
      </Save>
    </Footer>
  );
};

export default SavePanel;
