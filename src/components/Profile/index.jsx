import React from 'react';
import {useApolloClient} from '@apollo/client';

import {
  ProfileBlock,
  Heading,
  Title,
  ProfileContent,
  UsePhoto,
  ProfileLink,
  ProfileLinkOuter,
  LogoutBtn
} from './styled';
import Coins from 'assets/Profile/Coins';
import LogOut from 'assets/Profile/LogOut';
import Orders from 'assets/Profile/Orders';
import Settings from 'assets/Profile/Settings';
import User from 'assets/Profile/User';
import Avatar from '../Header/components/Avatar';
import {useUser} from 'hooks/reactiveVars';
import userPlaceholder from 'images/placeholders/user.jpg';

const Profile = ({data}) => {
  const [user] = useUser();
  const links = [
    {id: 'profile-rewards', text: 'Mid Coins', href: `/profile/rewards`, icon: Coins},
    {id: 'profile-profile', text: 'My Profile', href: `/profile/posts`, icon: User},
    {id: 'profile-orders', text: 'Orders', href: `/profile/orders`, icon: Orders},
    {id: 'profile-settings', text: 'Settings', href: `/profile/settings`, icon: Settings}
    // {id: 'profile-logout', text: 'Log Out', href: '#', icon: LogOut}
  ];

  const linksForSeller = [
    {id: 'profile-dashboard', text: 'Dashboard', href: `https://seller.testSample.net/dashboard/`, icon: Coins},
    {id: 'profile-rewards', text: 'My shop', href: `/shop/${user.databaseId}/products`, icon: User}
  ];

  const renderLinksByRole = () => {
    if (user?.capabilities?.includes('seller')) {
      return linksForSeller;
    }

    return links;
  };

  const renderLinks = renderLinksByRole();

  const client = useApolloClient();
  const handleLogout = async (e) => {
    e.preventDefault();
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    localStorage.removeItem('woo-session');
    client.resetStore();
    window.location.href = '/';

    // try {
    //   await Auth.signOut();
    //   globalActions.setUser(null);
    //   history.push('/');
    // } catch (err) {
    //   console.log(err);
    // }
  };

  return (
    <>
      <ProfileBlock>
        {user?.databaseId && (
          <>
            {user?.profile_picture ? <Avatar src={user?.profile_picture} /> : <Avatar name={user?.name} />}
            <Heading to={`/shop/${user.databaseId}/products`}>{user?.name}</Heading>
          </>
        )}
      </ProfileBlock>
      {/* temporary commented until we have seller profile functional */}
      {/* {user?.capabilities?.includes('seller') && (
        <ProfileBlock>
          <UsePhoto src={user?.profile_picture || userPlaceholder} />
          <Heading to={`/shop/${user?.databaseId}/products`} shop>
            My Shop
          </Heading>
        </ProfileBlock>
      )} */}
      <ProfileContent>
        {renderLinks.map((item, i) => {
          if (item.id === 'profile-dashboard') {
            return (
              <ProfileLinkOuter href={item.href}>
                <item.icon />
                <Title bold={!i} withIcon>
                  {item.text}
                </Title>
              </ProfileLinkOuter>
            );
          }
          return (
            <ProfileLink to={item.href}>
              <item.icon />
              <Title bold={!i} withIcon>
                {item.text}
              </Title>
            </ProfileLink>
          );
        })}
        <LogoutBtn onClick={handleLogout}>
          <LogOut />
          <Title withIcon>Log Out</Title>
        </LogoutBtn>
      </ProfileContent>
    </>
  );
};

export default Profile;
