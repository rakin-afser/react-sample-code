import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';

import {midCoinsColor} from 'constants/colors';

export const ProfileBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  border-bottom: 1px solid #e4e4e4;
  padding: 12px 16px;
`;

export const UsePhoto = styled.img`
  display: block;
  flex-shrink: 0;
  width: 32px;
  height: 32px;
  border-radius: 50%;
`;

export const Heading = styled(Link)`
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  color: #000;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  cursor: pointer;
  padding-left: 12px;

  ${({shop}) =>
    shop &&
    `
    font-weight: 400;
    color: #666;
    transition: color 0.3s;

    &:hover{
      cursor: pointer;
      color: #000;
    }
  `}
`;

export const ProfileContent = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px 16px 0 18px;
`;

export const Title = styled.span`
  display: block;
  font-size: 14px;
  color: #000;
  margin: ${({withIcon}) => (withIcon ? '0 0 0 16px' : '0 0 18px 0')};
  ${({last}) => last && 'margin-bottom: 0'};
  transition: 0.2s;

  ${({bold}) =>
    bold &&
    `
			font-weight: 700;
			color: ${midCoinsColor};
			`};
`;

export const ProfileLink = styled(Link)`
  display: flex;
  align-items: flex-start;
  padding-bottom: 16px;
  position: relative;

  & svg {
    margin-top: 2px;
    transition: transform 0.4s;
  }

  &:hover {
    & svg {
      transform: translateX(20%);
    }
  }
`;

export const ProfileLinkOuter = styled.a`
  display: flex;
  align-items: flex-start;
  padding-bottom: 16px;
  position: relative;

  & svg {
    margin-top: 2px;
    transition: transform 0.4s;
  }

  &:hover {
    & svg {
      transform: translateX(20%);
    }
  }
`;

export const LogoutBtn = styled.button`
  display: flex;
  align-items: flex-start;
  padding-bottom: 16px;
  position: relative;
  border: none;

  & svg {
    margin-top: 2px;
    transition: transform 0.4s;
  }

  &:hover {
    & svg {
      transform: translateX(20%);
    }
  }
`;
