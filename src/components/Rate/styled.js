import styled from 'styled-components/macro';
import {mainBlackColor, grayTextColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding-top: 8px;
`;

export const Row = styled.div`
  margin-bottom: ${({mb}) => mb && `${mb}px`};
  display: flex;
  justify-content: ${({right}) => right && 'flex-end'};
  justify-content: ${({between}) => between && 'space-between'};
  align-items: ${({center}) => center && 'center'};
`;

export const RateTxt = styled.div`
  font-size: 12px;
  color: ${mainBlackColor};
`;

export const CheckboxesContainer = styled.div`
  flex: 1;
  justify-content: space-between;
  display: flex;
  align-items: center;
`;

export const CheckboxWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-right: 10px;

  & label {
    margin-right: 4px;

    span + span {
      display: none;
    }
  }
`;

export const ChecksTitle = styled(RateTxt)`
  margin-right: 25px;
  font-weight: 500;
`;

export const CheckLabel = styled(RateTxt)`
  color: ${grayTextColor};
`;

export const ScoreTitle = styled(RateTxt)`
  padding-right: 3px;
  font-size: 14px;
  font-weight: 500;
`;
