import React, {useState, useEffect, useCallback} from 'react';
import PropTypes from 'prop-types';
import StarRatings from 'react-star-ratings';
import {
  CheckLabel,
  ChecksTitle,
  RateTxt,
  Row,
  ScoreTitle,
  Wrapper,
  CheckboxWrapper,
  CheckboxesContainer
} from 'components/Rate/styled';
import Checkbox from 'components/Checkbox';

const Rate = ({data, onChange = () => {}}) => {
  const [rating, setRating] = useState(data);
  const [generalOpinion, setGeneralOpinion] = useState('Positive');
  const [totalScore, setTotalScore] = useState(0);

  const onTotalScore = useCallback(() => {
    const total = Math.round((rating.item1 + rating.item2 + rating.item3) / 3);
    setTotalScore(total);
  }, [rating.item1, rating.item2, rating.item3]);

  const changeRating = (rate, e) => {
    setRating({...rating, [e]: rate});
  };

  useEffect(() => {
    onTotalScore();
  }, [onTotalScore, rating]);

  useEffect(() => {
    onChange({rating, total: totalScore, generalOpinion});
  }, [totalScore, generalOpinion]);

  return (
    <Wrapper>
      <Row mb={19}>
        <ChecksTitle>General Opinion</ChecksTitle>
        <CheckboxesContainer>
          <CheckboxWrapper>
            <Checkbox checked={generalOpinion === 'Positive'} onChange={() => setGeneralOpinion('Positive')} />
            <CheckLabel>Positive</CheckLabel>
          </CheckboxWrapper>
          <CheckboxWrapper>
            <Checkbox checked={generalOpinion === 'Neutral'} onChange={() => setGeneralOpinion('Neutral')} />
            <CheckLabel>Neutral</CheckLabel>
          </CheckboxWrapper>
          <CheckboxWrapper>
            <Checkbox checked={generalOpinion === 'Negative'} onChange={() => setGeneralOpinion('Negative')} />
            <CheckLabel>Negative</CheckLabel>
          </CheckboxWrapper>
        </CheckboxesContainer>
      </Row>
      <Row between center mb={11}>
        <RateTxt>Item as described ?</RateTxt>
        <StarRatings
          name="item1"
          rating={rating.item1}
          starDimension="27px"
          starSpacing="4.8px"
          starRatedColor="#FFC131"
          starEmptyColor="#CCCCCC"
          changeRating={(e, rate) => changeRating(e, rate)}
        />
      </Row>
      <Row between center mb={11}>
        <RateTxt>Seller's Communication?</RateTxt>
        <StarRatings
          name="item2"
          rating={rating.item2}
          starDimension="27px"
          starSpacing="4.8px"
          starRatedColor="#FFC131"
          starEmptyColor="#CCCCCC"
          changeRating={(e, rate) => changeRating(e, rate)}
        />
      </Row>
      <Row between center mb={11}>
        <RateTxt>Delivery Time?</RateTxt>
        <StarRatings
          name="item3"
          rating={rating.item3}
          starDimension="27px"
          starSpacing="4.8px"
          starRatedColor="#FFC131"
          starEmptyColor="#CCCCCC"
          changeRating={(e, rate) => changeRating(e, rate)}
        />
      </Row>
      <Row right>
        <ScoreTitle>
          Your Score <span>{totalScore}</span>
        </ScoreTitle>
      </Row>
    </Wrapper>
  );
};

Rate.propTypes = {
  data: PropTypes.objectOf(PropTypes.number).isRequired
};

export default Rate;
