import React from 'react';
import Likes from 'assets/ProductPage/Likes';
import Bookmarks from 'assets/ProductPage/Bookmarks';
import Comments from 'assets/ProductPage/Comments';
import {HorizontalDivider, Bottom, Icon, Links} from './styled';

const BottomInfo = () => (
  <div>
    <HorizontalDivider />
    <Bottom stylesForModal={isModal}>
      <Icon>
        {' '}
        <Likes />
        1,232
      </Icon>
      <Icon onClick={() => setCommentsDisplaying(true)}>
        <Comments />
        678
      </Icon>
      <Icon>
        <Links />
        675
      </Icon>
      <Icon>
        <Bookmarks />
        256
      </Icon>
    </Bottom>
  </div>
);
