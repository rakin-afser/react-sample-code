import styled from 'styled-components';

export const HorizontalDivider = styled.i`
  display: block;
  width: 100%;
  height: 1px;
  background: #e4e4e4;
`;

export const Bottom = styled.div`
  display: flex;
  ${({stylesForModal}) =>
    stylesForModal
      ? {margin: '14px 0 5px'}
      : {
          margin: '18px 27px 0 25px',
          paddingBottom: '25px'
        }}
`;

export const Icon = styled.i`
  cursor: pointer;
  display: flex;
  align-items: center;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  font-style: normal;
  line-height: 132%;
  color: #464646;
  margin-right: 36px;
  & svg {
    margin-right: 8px;
  }
`;
