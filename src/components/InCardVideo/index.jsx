import React, {useState, useRef} from 'react';
import ReactPlayer from 'react-player';
import {Wrapper, PlayBtn} from './styled';
import Icon from 'components/Icon';

const InCardVideo = ({src, withPlayBtn, activePlayCard, onPlayAction, playing, ...props}) => {
  const ref = useRef();
  const [play, setPlay] = useState(playing);

  const onPlay = (e) => {
    e.stopPropagation();
    if (onPlayAction) {
      onPlayAction();
    }
  };

  React.useEffect(() => {
    if (playing) {
      setPlay(true);
    } else {
      setPlay(false);
    }
  }, [playing]);

  return (
    <Wrapper {...props}>
      <ReactPlayer
        ref={ref}
        muted
        playsinline
        onEnded={() => {
          setPlay(false);
        }}
        playing={play}
        controls={false}
        url={src}
        width="100%"
        height="100%"
        loop
      />
      {withPlayBtn && (
        <PlayBtn onClick={onPlay}>
          <Icon type="playRed" />
        </PlayBtn>
      )}
    </Wrapper>
  );
};

export default InCardVideo;
