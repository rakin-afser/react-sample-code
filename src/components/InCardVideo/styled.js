import styled from 'styled-components';
import media from 'constants/media';

export const Wrapper = styled.div`
  position: relative;

  @media (max-width: ${media.mobileMax}) {
    video {
      height: 168px !important;
      object-fit: cover;
    }
  }
`;

export const PlayBtn = styled.div`
  position: absolute;
  bottom: 12px;
  left: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 30px;
  height: 30px;
  right: -3px;
  z-index: 1;

  svg {
    width: 38px;
    height: 38px;
    max-width: none !important;
    max-height: none !important;
  }
`;
