import styled from 'styled-components/macro';
import * as antd from 'antd';
import media from '../../constants/media';

export const Wrapper = styled.div`
  max-width: 1200px;
  margin: auto;

  .sign-in {
    display: flex;
    justify-content: center;
    margin-top: 64px;
  }

  .grid {
    padding: 32px 90px;
    display: flex;
  }
`;

export const Title = styled.h2`
  font-family: SF Pro Display;
  font-weight: bold;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin-top: 16px;
  margin-bottom: 0;
  padding-bottom: 16px;
  border-bottom: 2px solid #ed484f;
`;

export const SignForm = styled.div`
  max-width: 440px;
  border-right: ${({border}) => (border ? '1px solid #e4e4e4' : 'none')};
  display: flex;
  flex-direction: column;

  h2 {
    padding-right: 64px;
    margin-bottom: 12px;
  }

  h3 {
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
  }

  form {
    width: 360px;
    margin-right: 84px;
  }

  a {
    margin: 16px 84px 0 auto;
  }

  .ant-form-item-label {
    padding-bottom: 2px;
  }
`;

export const Submit = styled(antd.Button)`
  &&& {
    margin-left: auto;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    width: 160px;
    height: 36px;
    background: #ed484f;
    border-radius: 24px;
    font-weight: 500;
    font-size: 14px;
    line-height: 140%;
    border: none;
    ${({forgot}) => (forgot ? 'margin-top: 16px;' : null)};
    ${({styles}) => styles || null};

    @media (max-width: ${media.mobileMax}) {
      width: 168px;
      height: 40px;
      margin-bottom: 40px;
      font-weight: 500;
    }
  }
`;

export const VerifyEmailWrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  width: 404px;
  margin: 64px auto;
  padding: 40px 42px 48px;
  position: relative;

  .link {
    text-align: center;
    font-family: Helvetica;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
  }

  .back-arrow {
    position: absolute;
    top: 12px;
    left: 15px;
    cursor: pointer;
  }
`;

export const VerifyEmailForm = styled.div`
  position: relative;

  form {
    display: flex;
    flex-wrap: wrap;
  }

  form > div:not(:nth-child(6)) {
    margin-right: 16px;
  }

  form > div {
    margin-bottom: 0;
  }

  .ant-form-item-control-wrapper {
    width: 40px;
  }

  .ant-input-number {
    border-top: none;
    border-left: none;
    border-right: none;
    border-radius: 0;
    width: 40px;
    font-family: Helvetica;
    font-size: 24px;
    line-height: 140%;
    display: flex;
    align-items: center;
    text-align: center;
    color: #000000;
    box-shadow: none !important;
    border-color: ${({error}) => (error ? '#f5222d' : 'inherit')};
  }

  button {
    margin: 64px auto 36px;
  }

  .ant-form-explain {
    display: none;
  }

  .ant-input-number-handler-wrap {
    display: none;
  }

  .error-message {
    position: absolute;
    top: 35px;
    left: 0;
    right: 0;
    text-align: center;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 132%;
    color: #ed484f;
  }
`;

export const VerifyEmailTitle = styled.div`
  text-align: center;
  font-family: Helvetica Neue;
  color: #000000;
  line-height: 140%;
  font-style: normal;
  font-weight: normal;

  .ant-alert {
    padding: 8px 8px 8px 56px;

    i {
      top: 18px;
      left: 18px;
      bottom: 18px;
      font-size: 20px;
      display: flex;
      align-items: center;
    }
  }

  h2 {
    font-weight: bold;
    font-size: 28px;
    line-height: 120%;
    text-align: center;
    letter-spacing: 0.011em;
    margin-bottom: 24px;
  }

  h3 {
    font-weight: normal;
    font-size: 16px;
    line-height: 140%;
    letter-spacing: -0.016em;
    color: #464646;
  }

  strong {
    color: #000;
  }

  p {
    text-align: left;
    font-size: 16px;
    letter-spacing: -0.016em;
    color: #464646;
    margin: 36px 0 24px;
  }
`;

export const LocationWrapper = styled.div`
  width: 70%;

  .ant-select-search input {
    padding: 10px 0;
  }

  .ant-select-arrow {
    top: 13px;
    right: 5px;
  }

  .grid-columns {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 24px;

    .column {
      max-width: 264px;
      margin: 0 12px 0 39px;
    }
  }

  form {
    margin-right: 40px;
  }

  .actions {
    margin-top: 5px;

    button {
      border: 1px solid !important;
      border-color: #000 !important;
      color: #000 !important;
      width: 180px !important;
      height: 40px !important;
      margin-left: 0 !important;
      padding: 0 35px 0 15px;
      padding-top: 2px;

      svg {
        position: absolute;
        top: 50%;
        transform: translate(0, -50%);
        right: 15px;
      }
    }
  }

  .select-location {
    line-height: 140%;
    font-family: Helvetica Neue;
    font-style: normal;

    h3 {
      display: flex;
      align-items: center;
      font-weight: 500;
      font-size: 13px;
      line-height: 30px;
      color: #000000;
      margin-top: 3px;
      margin-bottom: -3px;

      svg {
        margin-right: 4px;
        position: relative;
        top: -3px;
      }
    }

    .text {
      font-weight: normal;
      font-size: 12px;
      line-height: 140%;
      color: #999999;
    }

    .description {
      font-weight: normal;
      font-size: 14px;
      color: #464646;
    }

    .ant-form-item-children > div {
      display: flex;
      margin-top: 10px;

      .ant-input-affix-wrapper {
        width: calc(100% - 33px);
        height: 33px;
      }

      .ant-input {
        height: 33px !important;
        min-height: 33px;
      }

      button {
        width: 25px;
        height: 25px;
        max-width: 25px;
        min-width: 25px;
        margin: auto 0 auto 8px;
        display: flex;
        align-items: center;
        justify-content: center;

        svg {
          max-width: 15px;
          max-height: 15px;
        }
      }
    }
  }

  .additional-directions {
    margin-top: 24px;
  }

  #location_additionalDirections {
    min-height: 45px !important;
    max-height: 45px !important;
    border: 1px solid #c3c3c3;
  }

  .ant-form-item {
    margin-bottom: 12px;
  }

  .ant-form-item-label {
    overflow: initial;
  }

  .ant-form label {
    font-size: 13px;
    line-height: 140%;
    color: #464646;
    position: relative;

    &:before {
      position: absolute;
      top: 0.5px;
      left: calc(100% + 4px);
    }

    &:after {
      display: none;
    }
  }
`;

export const CheckoutForm = styled.div`
  .ant-form-item-label {
    line-height: 14px;
    padding-bottom: 2px;
  }

  .ant-select-selection {
    background: #ffffff;
    border: 1px solid #c3c3c3;
    box-sizing: border-box;
    border-radius: 2px;
    height: 40px;

    .ant-select-selection__rendered {
      font-family: Helvetica Neue;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      height: 40px;
    }
  }

  .ant-select-selection > div {
    display: flex;
    align-items: center;
  }

  .ant-input-group-addon {
    background: #ffffff;
    border-color: #c3c3c3;
    border-radius: 2px;
    height: 40px;
  }

  input {
    background: #ffffff;
    border: 1px solid #c3c3c3;
    box-sizing: border-box;
    border-radius: 2px;
    height: 40px;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    color: #a7a7a7;
    height: 40px;
  }

  .exact-address {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 0 6.56px;
  }

  .grid-2 {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 0 8px;
  }

  .grid-4 {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    grid-gap: 0 8px;
  }
`;

export const StepTitleInactive = styled.div``;
export const StepTitleActive = styled.div`
  h2 {
    font-weight: 600;
    font-size: 16px;
    line-height: 124%;
    color: #000000;
    margin-bottom: 16px;
  }
`;

export const CartSummery = styled.div`
  background: #fff;
  position: relative;
  width: 30%;

  .wrapper {
    top: 20px;
    transition: 0.3s;
    ${({sticky}) =>
      sticky
        ? `position: fixed;
            height: auto;
          `
        : null}
    min-width: 300px;
    border: 1px solid #e4e4e4;
    border-radius: 3px;
    box-sizing: border-box;
    padding: 16px 16px 24px;
  }

  .title {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 20px;
    color: #000000;
    margin-bottom: 27px;
  }

  .row {
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 17px;

    small {
      font-size: 10px;
      line-height: 140%;
      text-transform: uppercase;
      margin-right: 4px;
    }

    .coupons {
      color: #ed494f;
    }
  }

  .redemptions {
    margin-bottom: 6px;
  }

  .redemptions-discount {
    margin-top: 9px;
    line-height: 17px;
    color: #666666;
  }

  .coins {
    display: flex;
    align-items: center;
    margin: 0;
    margin-bottom: 9px;

    h3 {
      font-family: Helvetica Neue;
      font-style: normal;
      font-weight: 500;
      font-size: 12px;
      line-height: 15px;
      color: #398287;
    }

    span {
      display: flex;
      align-items: center;
      font-weight: 500;
      color: #398287;

      svg {
        margin-left: 5px;
      }
    }
  }

  .total-pay {
    padding-top: 11px;
    padding-bottom: 20px;
    border-top: 1px solid #e4e4e4;
    color: #000000;
    font-size: 16px;
    line-height: 20px;
    margin-top: 24px;

    h3 {
      font-family: Helvetica Neue;
      font-style: normal;
      font-weight: bold;
      font-size: 16px;
      line-height: 20px;
      margin: 0;
    }

    span {
      font-weight: 500;
      font-size: 16px;
    }

    small {
      font-size: 12px;
      margin-right: 4px;
    }
  }

  .message {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    text-align: center;
    margin: 33px 0 16px;

    span {
      color: green;
    }
  }

  button {
    color: #fff !important;
    margin-left: 0 !important;
    width: 100% !important;
    height: 40px !important;
    max-width: 268px;
    transition: all 0.3s ease;

    &:disabled {
      background: #f9c0c3 !important;

      &:hover {
        color: #fff;
      }
    }
  }

  .error-message {
    position: absolute;
    bottom: -96px;
    left: 0;
    right: 0;
    background: #ee989b;
    border-radius: 4px;
    width: 300px;
    height: 56px;
    display: flex;
    align-items: center;
    padding: 18px;
    margin-top: 40px;

    span {
      font-family: Helvetica Neue;
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      line-height: 140%;
      color: #ffffff;
    }

    svg {
      margin-right: 18px;
    }
  }
`;

export const PaymentWrapper = styled.div`
  width: 70%;
  .order-wrapper {
    margin-right: 40px;

    .other-tab {
      margin: 0 0 24px;
      border: 1px solid #e4e4e4;
      box-sizing: border-box;
      border-radius: 3px;
      display: flex;
      align-items: center;
      padding: 11px 24px;
      font-family: Helvetica;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 132%;
      letter-spacing: -0.024em;
      color: #545454;

      & .title {
        margin-bottom: 0;
        margin-right: 10px;
        font-size: 18px;
        color: #000000;
      }

      .description {
        font-family: SF Pro Display;
        font-size: 14px;
        line-height: 140%;
        color: #545454;
      }

      .edit-icon {
        margin-left: auto;
      }
    }

    .ant-checkbox-wrapper {
      font-family: Helvetica Neue;
      font-weight: bold;
      color: #000000;

      .ant-checkbox-checked .ant-checkbox-inner {
        background-color: #000;
      }

      .ant-checkbox {
        margin-right: 4px;

        .ant-checkbox-inner {
          border-color: #000;
          border-radius: 4px;
          width: 20px;
          height: 20px;
        }

        .ant-checkbox-inner::after {
          left: 30%;
        }
      }

      .ant-checkbox:after {
        border: none;
      }
    }
  }

  .payment-title {
    font-weight: 600;
    font-size: 20px;
    line-height: 124%;
    color: inherit;
    padding: 0;
    margin-bottom: 24px;
  }

  .border-payment-method {
    padding-bottom: 24px !important;
  }

  .mid-coins {
    padding-bottom: 32px;
    margin-bottom: 32px;
    border-bottom: 1px solid #e4e4e4;

    .ant-form-item {
      margin-bottom: 0;
    }

    .coupen-form {
      padding: 16px 0 0 33px;

      .ant-form-item label {
        font-weight: normal;
        font-size: 11px;
        line-height: 140%;
        color: #000000;
      }

      .ant-col {
        max-width: 135px;
        margin-right: 24px;
      }

      .ant-input {
        color: #000;
      }
    }

    .submit-coupon {
      margin-left: 0 !important;
      height: 28px !important;
      width: 72px !important;
      border-color: #000 !important;
      font-weight: 500;
      font-size: 12px;
      line-height: 140%;
      color: #000000 !important;
    }
  }

  .coupon {
    padding-bottom: 32px;
    margin-bottom: -16px;
    border-bottom: 1px solid #e4e4e4;

    .ant-form-item label {
      font-weight: 500;
      font-size: 12px;
      line-height: 140%;
      color: #000000;
    }

    .ant-form-item-label > label::after {
      display: none;
    }

    .submit-coupon {
      margin-top: 42px !important;
      margin-left: 24px !important;
      height: 28px !important;
      width: 72px !important;
      border-color: #000 !important;
      font-weight: 500;
      font-size: 12px;
      line-height: 140%;
      color: #000000 !important;
    }

    .discount-check {
      margin-top: 44px !important;
    }

    .ant-row,
    .ant-col,
    input {
      max-width: 297px;
    }
  }

  .payment {
    box-sizing: border-box;
    border-radius: 3px;
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: normal;
    line-height: 124%;
    letter-spacing: 0.019em;
    color: #000000;
    border-bottom: 0;

    .cupon-checkbox {
      & > span {
        margin-left: 123px;
        color: #545454;
      }
    }

    .coupen-form {
      display: flex;
      align-items: center;

      .coupen-field {
        margin-top: 26px;
        font-family: Helvetica Neue;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 140%;
        color: #000000;
        width: 406px;
        margin-left: 33px;
        margin-bottom: 0;

        input {
          width: 406px;
          font-family: SF Pro Display;
          color: #000000;
        }
      }

      .submit-coupon {
        margin-top: 20px;
        border: 1px solid #ed484f;
        box-sizing: border-box;
        width: 96px;
        height: 36px;
        color: #ed484f;
        margin-left: 24px;
      }

      .discount-check {
        font-size: 12px;
        color: #ed494f;
        margin-top: 25px;
        margin-left: 25px;
      }
    }
  }

  .payment-method {
    border-top: none;

    & > div {
      display: grid;
      grid-template-columns: 406px;

      .ant-radio-group {
        display: grid !important;

        .ant-radio-wrapper {
          display: flex;
          align-items: center;
          box-sizing: border-box;
          border-radius: 4px;
          margin: 0px 0 16px;
          font-family: Helvetica Neue;
          line-height: 140%;
          color: #000000;

          & > span:last-child {
            width: 100%;
          }
        }

        .method {
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
      }

      .description {
        font-family: Helvetica Neue;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 132%;
        color: #7a7a7a;
        margin-bottom: 24px;
      }
    }

    .confirm-pay {
      font-family: Helvetica Neue;
      font-size: 18px;
      letter-spacing: -0.024em;
      color: #ffffff;
      width: 268px;
      height: 46px;
      transition: all 0.3s ease;

      &:disabled {
        background: #f9c0c3;

        &:hover {
          color: #fff;
        }
      }
    }
  }
`;

export const CommpliteWrapper = styled.div`
  .title {
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: bold;
    font-size: 26px;
    line-height: 31px;
    text-align: center;
    color: #000000;
    margin: 70px 0 7px;
  }

  .message {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 132%;
    letter-spacing: -0.024em;
    color: #656565;
    display: flex;
    justify-content: center;

    a {
      text-decoration: underline;
    }
  }

  .share-purchase {
    background: #ffffff;
    box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
    border-radius: 8px;
    max-width: 425px;
    margin: 40px auto;

    &__header {
      height: 65px;
      border-bottom: 1px solid #efefef;
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      line-height: 1;
      color: #000000;
      padding: 24px;
    }

    &__content {
      padding: 0 24px;
    }

    &__footer {
      margin: 0 24px;
      border-top: 1px solid #e4e4e4;
      padding: 16px 0;
      display: flex;
      align-items: center;
      justify-content: flex-end;

      .next {
        width: 95px;
        height: 28px;
        background: #ed484f;
        border-radius: 24px;
        font-weight: 500;
        font-size: 14px;
        line-height: 140%;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #ffffff;
        border: 0;
        outline: 0;
        transition: all 0.3s ease;

        svg {
          margin-left: 8px;
          margin-top: 1px;
        }

        &:disabled {
          background: #888;
          cursor: not-allowed;
        }
      }

      .cancel {
        width: 95px;
        height: 28px;
        background: transparent;
        border-radius: 24px;
        font-weight: 500;
        font-size: 14px;
        line-height: 140%;
        display: flex;
        align-items: center;
        justify-content: center;
        color: #656565;
        border: 0;
        outline: 0;
      }
    }
  }
`;

export const CreateAccount = styled.div`
  width: 351px;
  margin: 18px auto 480px;

  .name-field {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-gap: 19px;
  }

  button {
    border: 1px solid #ed484f !important;
    line-height: 140%;
    color: #ed484f;
    background: #fff !important;
    width: 127px;
    height: 40px;
  }

  .create-account-message {
    min-width: 400px;
    margin: 91px auto 590px;

    .title {
      font-family: SF Pro Display;
      font-style: normal;
      font-weight: bold;
      font-size: 20px;
      line-height: 24px;
      text-align: center;
      color: #000000;
      margin: 0 0 50px;
    }

    button {
      margin: 0 auto;
      width: 226px;
      height: 36px;
    }
  }
`;

export const EditLocationModal = styled(antd.Modal)`
  width: 425px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 24px;
  color: #404040;

  .ant-select-search input {
    padding: 10px 0;
  }

  .ant-select-arrow {
    top: 11px;
    right: 5px;
  }

  .ant-modal-body {
    padding: 24px;
  }

  .title {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 24px;
    color: #404040;
  }

  .actions {
    display: flex;
    align-items: center;
    justify-content: flex-end;

    .cancel {
      color: #000;
    }

    button {
      width: 80px !important;
      height: 32px !important;
    }
  }

  .exact-address {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 0 6.56px;
  }

  .grid-2 {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 0 8px;
  }

  .grid-3 {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    grid-gap: 0 8px;
  }
`;

export const ErrorDelivery = styled.div`
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  color: #ed484f;
  padding: 10px 24px 0;
`;

export const CheckoutMobileWrapper = styled.div`
  min-height: 100vh;
`;

export const Step = styled.div`
  padding: 0 16px 24px;

  ${({mini}) =>
    mini &&
    `
    padding: 0 16px;
  `}
`;

export const StepHeader = styled.div`
  padding: 16px 0;
  border-bottom: 1px solid #e4e4e4;
  font-style: normal;
  font-size: 18px;
  line-height: 22px;
  color: #000000;
  position: relative;
  margin-bottom: 16px;

  i {
    position: absolute;
    top: 50%;
    right: 3px;
    transform: translate(0, -50%) rotate(90deg);
    transition: ease 0.4s;

    ${({opened}) =>
      opened &&
      `
      transform: translate(0, -50%) rotate(-90deg);
    `}

    &.arrow {
      top: calc(50% + 1px);
      right: 3px;

      svg {
        width: 9px;
        height: 16px;
      }
    }

    &.pencil {
      right: 0;

      svg {
        width: 20px;
        height: 20px;
      }
    }

    svg {
      width: 20px;
      height: 20px;
    }
  }

  ${({mini}) =>
    mini &&
    `
    border: 0;
    margin-bottom: 0;

    i {
      top: 26px;

      &.arrow {
        top: 50%;
        transform: translate(0, -50%) rotate(90deg);
        right: 3px;

        svg {
          width: 9px;
          height: 16px;
        }
      }

      &.pencil {
        top: 27px;
        right: 0;

        svg {
          width: 20px;
          height: 20px;
        }
      }

      svg {
        width: 9px;
        height: 16px;
      }
    }
  `};
  ${({opened}) =>
    opened &&
    `
    i.arrow {
      transform: translate(0, -50%) rotate(-90deg);

      svg {
        width: 9px;
        height: 16px;
      }
    }
  `};
  ${({disabled}) =>
    disabled &&
    `
    font-size: 18px;
    line-height: 21px;
    color: #999999;
  `}
`;

export const StepContent = styled.div``;

export const StepFooter = styled.div`
  display: flex;
  padding: 16px;
`;

export const StepContentTitle = styled.h4`
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  margin-bottom: 16px;
`;

export const OrderItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 1.4;
  color: #000000;
  margin-bottom: 8px;

  small {
    font-size: 10px;
  }

  .red {
    color: #ed484f;
  }

  ${({total, coins}) => {
    if (total)
      return `
        border-top: 1px solid #CCCCCC;
        padding-top: 8px;
        font-size: 16px;
        line-height: 20px;
        font-weight: bold;
        margin-bottom: 16px;

        small {
          font-size: 12px;
          line-height: 20px;
          font-weight: bold;
        }
      `;

    if (coins)
      return `
        font-size: 12px;
        font-weight: 500;
        line-height: 15px;
        color: #398287;
        margin-bottom: 12px;

        span {
          display: inline-flex;
          align-items: center;
        }

        svg {
          stroke: #398287;
          margin-left: 3px;
        }
      `;
  }}
`;

export const ShippingAddressWrapper = styled.div`
  .shipping {
    width: 100vw;
    max-width: 680px;
    margin-right: 40px;

    &__content {
      padding: 24px;
      border: 1px solid #e4e4e4;
      border-radius: 3px;
      margin-bottom: 24px;

      &.inactive {
        h3 {
          margin-bottom: 0;
          font-weight: normal;
          font-size: 16px;
          line-height: 132%;
          color: #656565;
        }
      }

      h3 {
        font-weight: 600;
        font-size: 18px;
        line-height: 124%;
        color: #000000;
        margin-bottom: 26px;
      }
    }

    &__address {
      display: flex;
      align-items: flex-start;
      margin-bottom: 22px;

      &-icon {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        width: 20px;
        height: 20px;
        border: 1.5px solid #000000;
        border-radius: 50%;
        transition: all 0.3s ease;

        &::before {
          content: '';
          width: 10px;
          height: 10px;
          background-color: transparent;
          border-radius: 50%;
          display: block;
          transition: background-color 0.3s ease-in;
        }
      }

      &-checkbox {
        position: relative;
        display: flex;
        align-items: flex-start;
        width: 87%;

        input {
          display: none;

          &:checked {
            ~ .shipping__address {
              &-icon {
                /* background: #000; */

                &::before {
                  background-color: #000;
                }
              }

              &-info {
                .shipping__address {
                  &-name,
                  &-line {
                    color: #000;
                  }
                }
              }
            }
          }
        }
      }

      &-info {
        width: calc(73% - 20px);
        padding-left: 14px;
      }

      &-name,
      &-line {
        font-weight: normal;
        font-size: 14px;
        line-height: 140%;
        color: #545454;
        display: flex;
        transition: all 0.3s ease;
      }

      &-type {
        width: 27%;
        padding: 0 30px;
        text-align: center;
        font-weight: normal;
        font-size: 14px;
        line-height: 140%;
        color: #000000;
      }

      &-tools {
        margin-left: auto;
        display: inline-flex;
        align-items: flex-start;
        position: relative;
        top: -2px;
      }

      &-edit {
        width: 24px;
        height: 24px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;
        margin-right: 3px;
        position: relative;
        top: 1px;

        svg {
          fill: #545454;
        }
      }

      &-delete {
        width: 24px;
        height: 24px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        cursor: pointer;

        svg {
          max-width: 20px;
          max-height: 20px;
          fill: #545454;
        }
      }
    }

    &__footer {
      display: flex;
      align-items: center;
      padding-top: 24px;
      border-top: 1px solid #e4e4e4;

      &-add {
        font-weight: normal;
        font-size: 14px;
        line-height: 140%;
        color: #ed494f;
        cursor: pointer;

        svg {
          max-width: 9.5px;
          max-height: 9.5px;
          position: relative;
          top: -1px;
          margin-right: 9px;
        }
      }

      &-save {
        background: #ffffff;
        border: 1px solid #000;
        border-radius: 24px;
        cursor: pointer;
        margin-left: auto;
        width: 160px;
        height: 32px;
        display: inline-flex;
        align-items: center;
        justify-content: center;
        font-size: 14px;
        color: #000;
        transition: all 0.3s ease;

        svg {
          width: 5px;
          height: 8px;
          margin-left: 10px;
        }

        &.disabled {
          cursor: not-allowed;
          border-color: #888;
          color: #888;
        }
      }
    }
  }

  .popup {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 1000;
    padding: 50px 0;
    transition: all 0.3s ease;
    pointer-events: none;

    &.active {
      pointer-events: all;

      .popup {
        &__overlay {
          opacity: 1;
        }

        &__dialog {
          opacity: 1;
          transform: translate(0, 0);
        }
      }
    }

    &__overlay {
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      bottom: 0;
      z-index: 1001;
      background: rgba(0, 0, 0, 0.5);
      transition: all 0.3s ease;
      opacity: 0;
    }

    &__dialog {
      position: relative;
      background: #ffffff;
      width: 100%;
      margin: 0 auto;
      max-width: 460px;
      box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
      border-radius: 4px;
      z-index: 1002;
      transition: all 0.3s ease;
      opacity: 0;
      transform: translate(0, -50px);
    }

    &__close {
      position: absolute;
      top: 50%;
      right: 17px;
      transform: translate(0, -50%);
      width: 50px;
      height: 50px;
      display: flex;
      align-items: center;
      justify-content: center;
      cursor: pointer;
    }

    &__header {
      background: #e4e4e4;
      border-radius: 4px 4px 0px 0px;
      padding: 24px 36px;
      position: relative;
    }

    &__title {
      font-weight: normal;
      font-size: 16px;
      line-height: 18px;
      color: #404040;
    }

    &__content {
      background: #fff;
      padding: 16px 36px;
    }

    &__footer {
      background: #fff;
      padding: 16px 36px;
      display: flex;
      align-items: center;
      justify-content: flex-end;
      border-top: 1px solid #e4e4e4;
      border-radius: 0px 0px 4px 4px;
    }

    &__cancel,
    &__save {
      width: 140px;
      height: 40px;
      background: #ed484f;
      border-radius: 24px;
      font-weight: normal;
      font-size: 14px;
      line-height: 140%;
      display: flex;
      align-items: center;
      text-align: center;
      justify-content: center;
      color: #ffffff;
      cursor: pointer;
      outline: none;
      padding: 0;
      border: 0;
    }

    &__cancel {
      background: transparent;
      color: #333333;
    }
  }
`;

export const WarningMessage = styled.div`
  color: #ed484f;
  font-size: 14px;
`;

export const Resend = styled.button`
  color: #1890ff;
  cursor: pointer;
`;

export const CheckboxWrapper = styled.div`
  padding: 14px 0;

  > p {
    margin-bottom: 0;
    padding-left: 30px;
    font-size: 12px;
  }
`;

export const RewardsBlock = styled.div`
  padding: 16px 0 0 0;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;

  & .ant-form-item {
    width: 200px;
    margin: 0 12px 0 0;
  }

  & .ant-form-item-label {
  }

  & button {
    border-radius: 24px;
    border: 1px solid #ed484f;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    color: #ed484f;
    padding: 5px 19px;
    margin: 0 0 7px 12px;
  }

  & h3 {
    font-size: 16px !important;
  }
`;

export const Divider = styled.div`
  height: 1px;
  width: calc(100% - 48px);
  background: #e4e4e4;
  margin: 0 24px;
`;

export const RewardsAdditionalTitle = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #545454;
  margin-top: 8px;
  flex-grow: 1;

  i {
    position: relative;
    left: 3px;
    top: 1px;
  }
`;

export const PaymentBlockWrapper = styled.div`
  border: 1px solid #e4e4e4;
  border-radius: 3px;
  padding: 24px;
`;

export const PaymentSubTitle = styled.div`
  font-weight: 600;
  font-size: 16px;
  color: #000;
  padding: 0;
  margin-bottom: 24px;
`;

export const RewardsBlockColumn = styled(RewardsBlock)`
  flex-direction: column;
  align-items: flex-start;
  padding: 16px 30px 0;
`;

export const CardWrapper = styled.div`
  padding: 16px 33px 0 15px;
`;

export const SubmitButton = styled.button`
  margin-top: 26px;
  margin-bottom: 35px;
  display: flex;
  height: 40px;
  align-items: center;
  margin-right: auto;
  padding: 10px 74px;
  background: #ed484f;
  border-radius: 24px;
  font-weight: 500;
  font-size: 18px;
  line-height: 140%;
  border: 1px solid transparent;
  color: #fff;
  transition: ease 0.4s;

  &:disabled {
    cursor: not-allowed;
  }

  &:hover:not([disabled]) {
    background-color: transparent;
    color: #ed484f;
    border: 1px solid #ed484f;
  }
`;

export const StyledFormItem = styled(antd.Form.Item)`
  .ant-form-explain {
    font-size: 12px;
  }
`;

export const InputNumber = styled(antd.Input)`
  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  -moz-appearance: textfield; /* Firefox */
`;
InputNumber.defaultProps = {
  type: 'number'
};

export const MobileInput = styled(antd.Input)`
  &&& {
    .ant-input-prefix {
      left: 6px;
    }

    .ant-input-group-addon {
      padding: 0 6px;
    }

    input {
      padding-left: 70px !important;
    }

    .mobile-code {
      width: 57px;
      background: #4a90e2;
      border-radius: 4px;
      line-height: 140%;
      color: #ffffff;
      padding: 4px 6.3px;
    }
  }
`;

export const SecurityBtn = styled.button`
  display: flex;
  align-items: center;
  font-weight: 500;
  font-size: 14px;
  color: #ed494f;
  transition: ease 0.4s;
  cursor: pointer;

  i {
    margin-left: 10px;

    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    color: #000;

    i {
      svg {
        path {
          stroke: #000;
        }
      }
    }
  }
`;
