import image1 from '../images/image1.png';
import image2 from '../images/image2.png';
import image3 from '../images/image3.png';
import image4 from '../images/image4.png';

const chosenProduct = [
  {
    id: 0,
    brand: 'Zara',
    rating: '4.8',
    followers: '6.2',
    currency: 'bd',
    products: [
      {
        id: 0,
        name: 'Wide Detailed Hoop Earrings Gold',
        image: image1,
        size: 'UK 9',
        color: 'yellow',
        quantity: '1',
        price: '132.000',
        oldPrice: '243.000'
      },
      {
        id: 1,
        name: 'Circle Hair Pin Gold',
        image: image2,
        size: 'UK 8',
        color: 'red',
        quantity: '3',
        price: '521.000',
        oldPrice: '925.000'
      }
    ]
  },
  {
    id: 1,
    brand: 'Bosa Living',
    rating: '4.1',
    followers: '19.7',
    currency: 'bd',
    products: [
      {
        id: 3,
        name: 'Crochet Detail Pleated Dress White',
        image: image3,
        size: 'UK 10',
        color: 'green',
        quantity: '2',
        price: '99.000',
        oldPrice: ''
      },
      {
        id: 4,
        name: 'Pleat Detail Suit Pants Black',
        image: image4,
        size: 'UK 9',
        color: 'yellow',
        quantity: '1',
        price: '325.000',
        oldPrice: ''
      }
    ]
  }
];

const country = {
  Afghanistan: {
    name: 'Afghanistan',
    dialCode: '+93',
    code: 'AF'
  },
  'Aland Islands': {
    name: 'Aland Islands',
    dialCode: '+358',
    code: 'AX'
  },
  Albania: {
    name: 'Albania',
    dialCode: '+355',
    code: 'AL'
  },
  Algeria: {
    name: 'Algeria',
    dialCode: '+213',
    code: 'DZ'
  },
  AmericanSamoa: {
    name: 'AmericanSamoa',
    dialCode: '+1 684',
    code: 'AS'
  },
  Andorra: {
    name: 'Andorra',
    dialCode: '+376',
    code: 'AD'
  },
  Angola: {
    name: 'Angola',
    dialCode: '+244',
    code: 'AO'
  },
  Anguilla: {
    name: 'Anguilla',
    dialCode: '+1 264',
    code: 'AI'
  },
  Antarctica: {
    name: 'Antarctica',
    dialCode: '+672',
    code: 'AQ'
  },
  'Antigua and Barbuda': {
    name: 'Antigua and Barbuda',
    dialCode: '+1268',
    code: 'AG'
  },
  Argentina: {
    name: 'Argentina',
    dialCode: '+54',
    code: 'AR'
  },
  Armenia: {
    name: 'Armenia',
    dialCode: '+374',
    code: 'AM'
  },
  Aruba: {
    name: 'Aruba',
    dialCode: '+297',
    code: 'AW'
  },
  Australia: {
    name: 'Australia',
    dialCode: '+61',
    code: 'AU'
  },
  Austria: {
    name: 'Austria',
    dialCode: '+43',
    code: 'AT'
  },
  Azerbaijan: {
    name: 'Azerbaijan',
    dialCode: '+994',
    code: 'AZ'
  },
  Bahamas: {
    name: 'Bahamas',
    dialCode: '+1 242',
    code: 'BS'
  },
  Bahrain: {
    name: 'Bahrain',
    dialCode: '+973',
    code: 'BH'
  },
  Bangladesh: {
    name: 'Bangladesh',
    dialCode: '+880',
    code: 'BD'
  },
  Barbados: {
    name: 'Barbados',
    dialCode: '+1 246',
    code: 'BB'
  },
  Belarus: {
    name: 'Belarus',
    dialCode: '+375',
    code: 'BY'
  },
  Belgium: {
    name: 'Belgium',
    dialCode: '+32',
    code: 'BE'
  },
  Belize: {
    name: 'Belize',
    dialCode: '+501',
    code: 'BZ'
  },
  Benin: {
    name: 'Benin',
    dialCode: '+229',
    code: 'BJ'
  },
  Bermuda: {
    name: 'Bermuda',
    dialCode: '+1 441',
    code: 'BM'
  },
  Bhutan: {
    name: 'Bhutan',
    dialCode: '+975',
    code: 'BT'
  },
  'Bolivia, Plurinational State of': {
    name: 'Bolivia, Plurinational State of',
    dialCode: '+591',
    code: 'BO'
  },
  'Bosnia and Herzegovina': {
    name: 'Bosnia and Herzegovina',
    dialCode: '+387',
    code: 'BA'
  },
  Botswana: {
    name: 'Botswana',
    dialCode: '+267',
    code: 'BW'
  },
  Brazil: {
    name: 'Brazil',
    dialCode: '+55',
    code: 'BR'
  },
  'British Indian Ocean Territory': {
    name: 'British Indian Ocean Territory',
    dialCode: '+246',
    code: 'IO'
  },
  'Brunei Darussalam': {
    name: 'Brunei Darussalam',
    dialCode: '+673',
    code: 'BN'
  },
  Bulgaria: {
    name: 'Bulgaria',
    dialCode: '+359',
    code: 'BG'
  },
  'Burkina Faso': {
    name: 'Burkina Faso',
    dialCode: '+226',
    code: 'BF'
  },
  Burundi: {
    name: 'Burundi',
    dialCode: '+257',
    code: 'BI'
  },
  Cambodia: {
    name: 'Cambodia',
    dialCode: '+855',
    code: 'KH'
  },
  Cameroon: {
    name: 'Cameroon',
    dialCode: '+237',
    code: 'CM'
  },
  Canada: {
    name: 'Canada',
    dialCode: '+1',
    code: 'CA'
  },
  'Cape Verde': {
    name: 'Cape Verde',
    dialCode: '+238',
    code: 'CV'
  },
  'Cayman Islands': {
    name: 'Cayman Islands',
    dialCode: '+ 345',
    code: 'KY'
  },
  'Central African Republic': {
    name: 'Central African Republic',
    dialCode: '+236',
    code: 'CF'
  },
  Chad: {
    name: 'Chad',
    dialCode: '+235',
    code: 'TD'
  },
  Chile: {
    name: 'Chile',
    dialCode: '+56',
    code: 'CL'
  },
  China: {
    name: 'China',
    dialCode: '+86',
    code: 'CN'
  },
  'Christmas Island': {
    name: 'Christmas Island',
    dialCode: '+61',
    code: 'CX'
  },
  'Cocos (Keeling) Islands': {
    name: 'Cocos (Keeling) Islands',
    dialCode: '+61',
    code: 'CC'
  },
  Colombia: {
    name: 'Colombia',
    dialCode: '+57',
    code: 'CO'
  },
  Comoros: {
    name: 'Comoros',
    dialCode: '+269',
    code: 'KM'
  },
  Congo: {
    name: 'Congo',
    dialCode: '+242',
    code: 'CG'
  },
  'Congo, The Democratic Republic of the Congo': {
    name: 'Congo, The Democratic Republic of the Congo',
    dialCode: '+243',
    code: 'CD'
  },
  'Cook Islands': {
    name: 'Cook Islands',
    dialCode: '+682',
    code: 'CK'
  },
  'Costa Rica': {
    name: 'Costa Rica',
    dialCode: '+506',
    code: 'CR'
  },
  "Cote d'Ivoire": {
    name: "Cote d'Ivoire",
    dialCode: '+225',
    code: 'CI'
  },
  Croatia: {
    name: 'Croatia',
    dialCode: '+385',
    code: 'HR'
  },
  Cuba: {
    name: 'Cuba',
    dialCode: '+53',
    code: 'CU'
  },
  Cyprus: {
    name: 'Cyprus',
    dialCode: '+357',
    code: 'CY'
  },
  'Czech Republic': {
    name: 'Czech Republic',
    dialCode: '+420',
    code: 'CZ'
  },
  Denmark: {
    name: 'Denmark',
    dialCode: '+45',
    code: 'DK'
  },
  Djibouti: {
    name: 'Djibouti',
    dialCode: '+253',
    code: 'DJ'
  },
  Dominica: {
    name: 'Dominica',
    dialCode: '+1 767',
    code: 'DM'
  },
  'Dominican Republic': {
    name: 'Dominican Republic',
    dialCode: '+1 849',
    code: 'DO'
  },
  Ecuador: {
    name: 'Ecuador',
    dialCode: '+593',
    code: 'EC'
  },
  Egypt: {
    name: 'Egypt',
    dialCode: '+20',
    code: 'EG'
  },
  'El Salvador': {
    name: 'El Salvador',
    dialCode: '+503',
    code: 'SV'
  },
  'Equatorial Guinea': {
    name: 'Equatorial Guinea',
    dialCode: '+240',
    code: 'GQ'
  },
  Eritrea: {
    name: 'Eritrea',
    dialCode: '+291',
    code: 'ER'
  },
  Estonia: {
    name: 'Estonia',
    dialCode: '+372',
    code: 'EE'
  },
  Ethiopia: {
    name: 'Ethiopia',
    dialCode: '+251',
    code: 'ET'
  },
  'Falkland Islands (Malvinas)': {
    name: 'Falkland Islands (Malvinas)',
    dialCode: '+500',
    code: 'FK'
  },
  'Faroe Islands': {
    name: 'Faroe Islands',
    dialCode: '+298',
    code: 'FO'
  },
  Fiji: {
    name: 'Fiji',
    dialCode: '+679',
    code: 'FJ'
  },
  Finland: {
    name: 'Finland',
    dialCode: '+358',
    code: 'FI'
  },
  France: {
    name: 'France',
    dialCode: '+33',
    code: 'FR'
  },
  'French Guiana': {
    name: 'French Guiana',
    dialCode: '+594',
    code: 'GF'
  },
  'French Polynesia': {
    name: 'French Polynesia',
    dialCode: '+689',
    code: 'PF'
  },
  Gabon: {
    name: 'Gabon',
    dialCode: '+241',
    code: 'GA'
  },
  Gambia: {
    name: 'Gambia',
    dialCode: '+220',
    code: 'GM'
  },
  Georgia: {
    name: 'Georgia',
    dialCode: '+995',
    code: 'GE'
  },
  Germany: {
    name: 'Germany',
    dialCode: '+49',
    code: 'DE'
  },
  Ghana: {
    name: 'Ghana',
    dialCode: '+233',
    code: 'GH'
  },
  Gibraltar: {
    name: 'Gibraltar',
    dialCode: '+350',
    code: 'GI'
  },
  Greece: {
    name: 'Greece',
    dialCode: '+30',
    code: 'GR'
  },
  Greenland: {
    name: 'Greenland',
    dialCode: '+299',
    code: 'GL'
  },
  Grenada: {
    name: 'Grenada',
    dialCode: '+1 473',
    code: 'GD'
  },
  Guadeloupe: {
    name: 'Guadeloupe',
    dialCode: '+590',
    code: 'GP'
  },
  Guam: {
    name: 'Guam',
    dialCode: '+1 671',
    code: 'GU'
  },
  Guatemala: {
    name: 'Guatemala',
    dialCode: '+502',
    code: 'GT'
  },
  Guernsey: {
    name: 'Guernsey',
    dialCode: '+44',
    code: 'GG'
  },
  Guinea: {
    name: 'Guinea',
    dialCode: '+224',
    code: 'GN'
  },
  'Guinea-Bissau': {
    name: 'Guinea-Bissau',
    dialCode: '+245',
    code: 'GW'
  },
  Guyana: {
    name: 'Guyana',
    dialCode: '+595',
    code: 'GY'
  },
  Haiti: {
    name: 'Haiti',
    dialCode: '+509',
    code: 'HT'
  },
  'Holy See (Vatican City State)': {
    name: 'Holy See (Vatican City State)',
    dialCode: '+379',
    code: 'VA'
  },
  Honduras: {
    name: 'Honduras',
    dialCode: '+504',
    code: 'HN'
  },
  'Hong Kong': {
    name: 'Hong Kong',
    dialCode: '+852',
    code: 'HK'
  },
  Hungary: {
    name: 'Hungary',
    dialCode: '+36',
    code: 'HU'
  },
  Iceland: {
    name: 'Iceland',
    dialCode: '+354',
    code: 'IS'
  },
  India: {
    name: 'India',
    dialCode: '+91',
    code: 'IN'
  },
  Indonesia: {
    name: 'Indonesia',
    dialCode: '+62',
    code: 'ID'
  },
  'Iran, Islamic Republic of Persian Gulf': {
    name: 'Iran, Islamic Republic of Persian Gulf',
    dialCode: '+98',
    code: 'IR'
  },
  Iraq: {
    name: 'Iraq',
    dialCode: '+964',
    code: 'IQ'
  },
  Ireland: {
    name: 'Ireland',
    dialCode: '+353',
    code: 'IE'
  },
  'Isle of Man': {
    name: 'Isle of Man',
    dialCode: '+44',
    code: 'IM'
  },
  Israel: {
    name: 'Israel',
    dialCode: '+972',
    code: 'IL'
  },
  Italy: {
    name: 'Italy',
    dialCode: '+39',
    code: 'IT'
  },
  Jamaica: {
    name: 'Jamaica',
    dialCode: '+1 876',
    code: 'JM'
  },
  Japan: {
    name: 'Japan',
    dialCode: '+81',
    code: 'JP'
  },
  Jersey: {
    name: 'Jersey',
    dialCode: '+44',
    code: 'JE'
  },
  Jordan: {
    name: 'Jordan',
    dialCode: '+962',
    code: 'JO'
  },
  Kazakhstan: {
    name: 'Kazakhstan',
    dialCode: '+7 7',
    code: 'KZ'
  },
  Kenya: {
    name: 'Kenya',
    dialCode: '+254',
    code: 'KE'
  },
  Kiribati: {
    name: 'Kiribati',
    dialCode: '+686',
    code: 'KI'
  },
  "Korea, Democratic People's Republic of Korea": {
    name: "Korea, Democratic People's Republic of Korea",
    dialCode: '+850',
    code: 'KP'
  },
  'Korea, Republic of South Korea': {
    name: 'Korea, Republic of South Korea',
    dialCode: '+82',
    code: 'KR'
  },
  Kuwait: {
    name: 'Kuwait',
    dialCode: '+965',
    code: 'KW'
  },
  Kyrgyzstan: {
    name: 'Kyrgyzstan',
    dialCode: '+996',
    code: 'KG'
  },
  Laos: {
    name: 'Laos',
    dialCode: '+856',
    code: 'LA'
  },
  Latvia: {
    name: 'Latvia',
    dialCode: '+371',
    code: 'LV'
  },
  Lebanon: {
    name: 'Lebanon',
    dialCode: '+961',
    code: 'LB'
  },
  Lesotho: {
    name: 'Lesotho',
    dialCode: '+266',
    code: 'LS'
  },
  Liberia: {
    name: 'Liberia',
    dialCode: '+231',
    code: 'LR'
  },
  'Libyan Arab Jamahiriya': {
    name: 'Libyan Arab Jamahiriya',
    dialCode: '+218',
    code: 'LY'
  },
  Liechtenstein: {
    name: 'Liechtenstein',
    dialCode: '+423',
    code: 'LI'
  },
  Lithuania: {
    name: 'Lithuania',
    dialCode: '+370',
    code: 'LT'
  },
  Luxembourg: {
    name: 'Luxembourg',
    dialCode: '+352',
    code: 'LU'
  },
  Macao: {
    name: 'Macao',
    dialCode: '+853',
    code: 'MO'
  },
  Macedonia: {
    name: 'Macedonia',
    dialCode: '+389',
    code: 'MK'
  },
  Madagascar: {
    name: 'Madagascar',
    dialCode: '+261',
    code: 'MG'
  },
  Malawi: {
    name: 'Malawi',
    dialCode: '+265',
    code: 'MW'
  },
  Malaysia: {
    name: 'Malaysia',
    dialCode: '+60',
    code: 'MY'
  },
  Maldives: {
    name: 'Maldives',
    dialCode: '+960',
    code: 'MV'
  },
  Mali: {
    name: 'Mali',
    dialCode: '+223',
    code: 'ML'
  },
  Malta: {
    name: 'Malta',
    dialCode: '+356',
    code: 'MT'
  },
  'Marshall Islands': {
    name: 'Marshall Islands',
    dialCode: '+692',
    code: 'MH'
  },
  Martinique: {
    name: 'Martinique',
    dialCode: '+596',
    code: 'MQ'
  },
  Mauritania: {
    name: 'Mauritania',
    dialCode: '+222',
    code: 'MR'
  },
  Mauritius: {
    name: 'Mauritius',
    dialCode: '+230',
    code: 'MU'
  },
  Mayotte: {
    name: 'Mayotte',
    dialCode: '+262',
    code: 'YT'
  },
  Mexico: {
    name: 'Mexico',
    dialCode: '+52',
    code: 'MX'
  },
  'Micronesia, Federated States of Micronesia': {
    name: 'Micronesia, Federated States of Micronesia',
    dialCode: '+691',
    code: 'FM'
  },
  Moldova: {
    name: 'Moldova',
    dialCode: '+373',
    code: 'MD'
  },
  Monaco: {
    name: 'Monaco',
    dialCode: '+377',
    code: 'MC'
  },
  Mongolia: {
    name: 'Mongolia',
    dialCode: '+976',
    code: 'MN'
  },
  Montenegro: {
    name: 'Montenegro',
    dialCode: '+382',
    code: 'ME'
  },
  Montserrat: {
    name: 'Montserrat',
    dialCode: '+1664',
    code: 'MS'
  },
  Morocco: {
    name: 'Morocco',
    dialCode: '+212',
    code: 'MA'
  },
  Mozambique: {
    name: 'Mozambique',
    dialCode: '+258',
    code: 'MZ'
  },
  Myanmar: {
    name: 'Myanmar',
    dialCode: '+95',
    code: 'MM'
  },
  Namibia: {
    name: 'Namibia',
    dialCode: '+264',
    code: 'NA'
  },
  Nauru: {
    name: 'Nauru',
    dialCode: '+674',
    code: 'NR'
  },
  Nepal: {
    name: 'Nepal',
    dialCode: '+977',
    code: 'NP'
  },
  Netherlands: {
    name: 'Netherlands',
    dialCode: '+31',
    code: 'NL'
  },
  'Netherlands Antilles': {
    name: 'Netherlands Antilles',
    dialCode: '+599',
    code: 'AN'
  },
  'New Caledonia': {
    name: 'New Caledonia',
    dialCode: '+687',
    code: 'NC'
  },
  'New Zealand': {
    name: 'New Zealand',
    dialCode: '+64',
    code: 'NZ'
  },
  Nicaragua: {
    name: 'Nicaragua',
    dialCode: '+505',
    code: 'NI'
  },
  Niger: {
    name: 'Niger',
    dialCode: '+227',
    code: 'NE'
  },
  Nigeria: {
    name: 'Nigeria',
    dialCode: '+234',
    code: 'NG'
  },
  Niue: {
    name: 'Niue',
    dialCode: '+683',
    code: 'NU'
  },
  'Norfolk Island': {
    name: 'Norfolk Island',
    dialCode: '+672',
    code: 'NF'
  },
  'Northern Mariana Islands': {
    name: 'Northern Mariana Islands',
    dialCode: '+1 670',
    code: 'MP'
  },
  Norway: {
    name: 'Norway',
    dialCode: '+47',
    code: 'NO'
  },
  Oman: {
    name: 'Oman',
    dialCode: '+968',
    code: 'OM'
  },
  Pakistan: {
    name: 'Pakistan',
    dialCode: '+92',
    code: 'PK'
  },
  Palau: {
    name: 'Palau',
    dialCode: '+680',
    code: 'PW'
  },
  'Palestinian Territory, Occupied': {
    name: 'Palestinian Territory, Occupied',
    dialCode: '+970',
    code: 'PS'
  },
  Panama: {
    name: 'Panama',
    dialCode: '+507',
    code: 'PA'
  },
  'Papua New Guinea': {
    name: 'Papua New Guinea',
    dialCode: '+675',
    code: 'PG'
  },
  Paraguay: {
    name: 'Paraguay',
    dialCode: '+595',
    code: 'PY'
  },
  Peru: {
    name: 'Peru',
    dialCode: '+51',
    code: 'PE'
  },
  Philippines: {
    name: 'Philippines',
    dialCode: '+63',
    code: 'PH'
  },
  Pitcairn: {
    name: 'Pitcairn',
    dialCode: '+872',
    code: 'PN'
  },
  Poland: {
    name: 'Poland',
    dialCode: '+48',
    code: 'PL'
  },
  Portugal: {
    name: 'Portugal',
    dialCode: '+351',
    code: 'PT'
  },
  'Puerto Rico': {
    name: 'Puerto Rico',
    dialCode: '+1 939',
    code: 'PR'
  },
  Qatar: {
    name: 'Qatar',
    dialCode: '+974',
    code: 'QA'
  },
  Romania: {
    name: 'Romania',
    dialCode: '+40',
    code: 'RO'
  },
  Russia: {
    name: 'Russia',
    dialCode: '+7',
    code: 'RU'
  },
  Rwanda: {
    name: 'Rwanda',
    dialCode: '+250',
    code: 'RW'
  },
  Reunion: {
    name: 'Reunion',
    dialCode: '+262',
    code: 'RE'
  },
  'Saint Barthelemy': {
    name: 'Saint Barthelemy',
    dialCode: '+590',
    code: 'BL'
  },
  'Saint Helena, Ascension and Tristan Da Cunha': {
    name: 'Saint Helena, Ascension and Tristan Da Cunha',
    dialCode: '+290',
    code: 'SH'
  },
  'Saint Kitts and Nevis': {
    name: 'Saint Kitts and Nevis',
    dialCode: '+1 869',
    code: 'KN'
  },
  'Saint Lucia': {
    name: 'Saint Lucia',
    dialCode: '+1 758',
    code: 'LC'
  },
  'Saint Martin': {
    name: 'Saint Martin',
    dialCode: '+590',
    code: 'MF'
  },
  'Saint Pierre and Miquelon': {
    name: 'Saint Pierre and Miquelon',
    dialCode: '+508',
    code: 'PM'
  },
  'Saint Vincent and the Grenadines': {
    name: 'Saint Vincent and the Grenadines',
    dialCode: '+1 784',
    code: 'VC'
  },
  Samoa: {
    name: 'Samoa',
    dialCode: '+685',
    code: 'WS'
  },
  'San Marino': {
    name: 'San Marino',
    dialCode: '+378',
    code: 'SM'
  },
  'Sao Tome and Principe': {
    name: 'Sao Tome and Principe',
    dialCode: '+239',
    code: 'ST'
  },
  'Saudi Arabia': {
    name: 'Saudi Arabia',
    dialCode: '+966',
    code: 'SA'
  },
  Senegal: {
    name: 'Senegal',
    dialCode: '+221',
    code: 'SN'
  },
  Serbia: {
    name: 'Serbia',
    dialCode: '+381',
    code: 'RS'
  },
  Seychelles: {
    name: 'Seychelles',
    dialCode: '+248',
    code: 'SC'
  },
  'Sierra Leone': {
    name: 'Sierra Leone',
    dialCode: '+232',
    code: 'SL'
  },
  Singapore: {
    name: 'Singapore',
    dialCode: '+65',
    code: 'SG'
  },
  Slovakia: {
    name: 'Slovakia',
    dialCode: '+421',
    code: 'SK'
  },
  Slovenia: {
    name: 'Slovenia',
    dialCode: '+386',
    code: 'SI'
  },
  'Solomon Islands': {
    name: 'Solomon Islands',
    dialCode: '+677',
    code: 'SB'
  },
  Somalia: {
    name: 'Somalia',
    dialCode: '+252',
    code: 'SO'
  },
  'South Africa': {
    name: 'South Africa',
    dialCode: '+27',
    code: 'ZA'
  },
  'South Georgia and the South Sandwich Islands': {
    name: 'South Georgia and the South Sandwich Islands',
    dialCode: '+500',
    code: 'GS'
  },
  Spain: {
    name: 'Spain',
    dialCode: '+34',
    code: 'ES'
  },
  'Sri Lanka': {
    name: 'Sri Lanka',
    dialCode: '+94',
    code: 'LK'
  },
  Sudan: {
    name: 'Sudan',
    dialCode: '+249',
    code: 'SD'
  },
  Suriname: {
    name: 'Suriname',
    dialCode: '+597',
    code: 'SR'
  },
  'Svalbard and Jan Mayen': {
    name: 'Svalbard and Jan Mayen',
    dialCode: '+47',
    code: 'SJ'
  },
  Swaziland: {
    name: 'Swaziland',
    dialCode: '+268',
    code: 'SZ'
  },
  Sweden: {
    name: 'Sweden',
    dialCode: '+46',
    code: 'SE'
  },
  Switzerland: {
    name: 'Switzerland',
    dialCode: '+41',
    code: 'CH'
  },
  'Syrian Arab Republic': {
    name: 'Syrian Arab Republic',
    dialCode: '+963',
    code: 'SY'
  },
  Taiwan: {
    name: 'Taiwan',
    dialCode: '+886',
    code: 'TW'
  },
  Tajikistan: {
    name: 'Tajikistan',
    dialCode: '+992',
    code: 'TJ'
  },
  'Tanzania, United Republic of Tanzania': {
    name: 'Tanzania, United Republic of Tanzania',
    dialCode: '+255',
    code: 'TZ'
  },
  Thailand: {
    name: 'Thailand',
    dialCode: '+66',
    code: 'TH'
  },
  'Timor-Leste': {
    name: 'Timor-Leste',
    dialCode: '+670',
    code: 'TL'
  },
  Togo: {
    name: 'Togo',
    dialCode: '+228',
    code: 'TG'
  },
  Tokelau: {
    name: 'Tokelau',
    dialCode: '+690',
    code: 'TK'
  },
  Tonga: {
    name: 'Tonga',
    dialCode: '+676',
    code: 'TO'
  },
  'Trinidad and Tobago': {
    name: 'Trinidad and Tobago',
    dialCode: '+1 868',
    code: 'TT'
  },
  Tunisia: {
    name: 'Tunisia',
    dialCode: '+216',
    code: 'TN'
  },
  Turkey: {
    name: 'Turkey',
    dialCode: '+90',
    code: 'TR'
  },
  Turkmenistan: {
    name: 'Turkmenistan',
    dialCode: '+993',
    code: 'TM'
  },
  'Turks and Caicos Islands': {
    name: 'Turks and Caicos Islands',
    dialCode: '+1 649',
    code: 'TC'
  },
  Tuvalu: {
    name: 'Tuvalu',
    dialCode: '+688',
    code: 'TV'
  },
  Uganda: {
    name: 'Uganda',
    dialCode: '+256',
    code: 'UG'
  },
  Ukraine: {
    name: 'Ukraine',
    dialCode: '+380',
    code: 'UA'
  },
  'United Arab Emirates': {
    name: 'United Arab Emirates',
    dialCode: '+971',
    code: 'AE'
  },
  'United Kingdom': {
    name: 'United Kingdom',
    dialCode: '+44',
    code: 'GB'
  },
  'United States': {
    name: 'United States',
    dialCode: '+1',
    code: 'US'
  },
  Uruguay: {
    name: 'Uruguay',
    dialCode: '+598',
    code: 'UY'
  },
  Uzbekistan: {
    name: 'Uzbekistan',
    dialCode: '+998',
    code: 'UZ'
  },
  Vanuatu: {
    name: 'Vanuatu',
    dialCode: '+678',
    code: 'VU'
  },
  'Venezuela, Bolivarian Republic of Venezuela': {
    name: 'Venezuela, Bolivarian Republic of Venezuela',
    dialCode: '+58',
    code: 'VE'
  },
  Vietnam: {
    name: 'Vietnam',
    dialCode: '+84',
    code: 'VN'
  },
  'Virgin Islands, British': {
    name: 'Virgin Islands, British',
    dialCode: '+1 284',
    code: 'VG'
  },
  'Virgin Islands, U.S.': {
    name: 'Virgin Islands, U.S.',
    dialCode: '+1 340',
    code: 'VI'
  },
  'Wallis and Futuna': {
    name: 'Wallis and Futuna',
    dialCode: '+681',
    code: 'WF'
  },
  Yemen: {
    name: 'Yemen',
    dialCode: '+967',
    code: 'YE'
  },
  Zambia: {
    name: 'Zambia',
    dialCode: '+260',
    code: 'ZM'
  },
  Zimbabwe: {
    name: 'Zimbabwe',
    dialCode: '+263',
    code: 'ZW'
  }
};

export default {chosenProduct, country};
