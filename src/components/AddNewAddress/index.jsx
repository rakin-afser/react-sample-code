import React from 'react';
import {bool, func, objectOf, any, string} from 'prop-types';

import {Form, Select, Button, Input} from 'antd';

import {EditLocationModal, CheckoutForm, StyledFormItem, MobileInput, InputNumber} from './styled';

import exampleData from './assets/data/exampleData';
import StyledSelect from './components/Select';
import StyledButton from './components/Button';
import cities from './assets/data/cities';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import {useUser} from 'hooks/reactiveVars';
import {useMutation} from '@apollo/client';
import {ADD_SHIPPING_ADDRESS} from 'mutations';
import {ShippingAddressFragment} from 'fragments';

const {Option} = Select;
const {country} = exampleData;

const AddNewAddress = ({visible, close, form, shippingAddressChanged = () => {}}) => {
  const {getFieldDecorator, getFieldValue} = form;
  const [user] = useUser();
  const [addAddress, {data: dataEdit}] = useMutation(ADD_SHIPPING_ADDRESS, {
    onCompleted() {}
  });

  const onSubmitHandler = (e) => {
    e.preventDefault();
    form.validateFields(async (err, values) => {
      const address = {
        addDirections: values?.addrAddDirections,
        address1: values?.addrAddress1,
        addressNickname: values?.addrAddressNickname,
        apartment: parseInt(values?.addrApartment),
        block: parseInt(values?.addrBlock),
        city: values?.addrCity,
        country: country?.[values?.addrCountry]?.code,
        district: values?.addrDistrict,
        firstName: values?.addrFirstName,
        floor: parseInt(values?.addrFloor),
        map: values?.addrMap,
        phone: values?.addrPhone,
        road: parseInt(values?.addrRoad),
        searchRoad: values?.addrSearchRoad,
        type: values?.addrType,
        build: values?.addrBuild
      };
      if (!err) {
        if (!user) {
          shippingAddressChanged(address);
          localStorage.setItem('gAddress', JSON.stringify(address));
        }
        if (user) {
          await addAddress({
            variables: {
              input: {
                ...values,
                addrCountry: country?.[values?.addrCountry]?.code,
                addrBlock: parseInt(values?.addrBlock),
                addrRoad: parseInt(values?.addrRoad),
                addrFloor: parseInt(values?.addrFloor),
                addrApartment: parseInt(values?.addrApartment)
              }
            },
            update(cache, {data}) {
              cache.modify({
                fields: {
                  testSampleAddresses(existing) {
                    if (data?.editShippingAddress?.shippingAddresses?.find((addr) => addr?.id === visible)?.selected) {
                      shippingAddressChanged(address);
                    }
                    const shipping = data?.addShippingAddress?.shippingAddresses?.map((addr) =>
                      cache.writeFragment({
                        id: cache.identify(addr),
                        data: addr,
                        fragment: ShippingAddressFragment
                      })
                    );
                    return {...existing, shipping};
                  }
                }
              });
            }
          });
        }
        close();
      }
    });
  };

  const handleCancel = () => {
    close();
  };

  let savedData = {};

  const countryName = getFieldValue('addrCountry') || getCountryFromCode(savedData?.country);

  const prefixModilePhone = (
    <div className="mobile-code">{country?.[countryName] ? country?.[countryName].dialCode : '+'}</div>
  );

  const numberFieldRules = {
    pattern: /^\d+$/,
    message: 'Numbers only'
  };

  return (
    <EditLocationModal
      afterClose={() => form.resetFields()}
      visible={visible}
      footer={false}
      onCancel={close}
      width="430px"
    >
      <h3 className="title">Edit Address</h3>
      <CheckoutForm>
        <Form onSubmit={onSubmitHandler}>
          <div className="grid-2">
            <StyledFormItem label="Country">
              {getFieldDecorator('addrCountry', {
                rules: [{required: true, message: 'Enter a valid country'}],
                initialValue: countryName
              })(
                <StyledSelect placeholder="Select Your Country" showSearch allowClear>
                  {Object.keys(country).map((key) => (
                    <Option value={key} key={key}>
                      {key}
                    </Option>
                  ))}
                </StyledSelect>
              )}
            </StyledFormItem>
            <StyledFormItem label="Mobile Number">
              {getFieldDecorator('addrPhone', {
                rules: [{required: true, message: 'Enter a valid phone number'}],
                initialValue: savedData?.phone
              })(<MobileInput prefix={prefixModilePhone} style={{width: '100%'}} />)}
            </StyledFormItem>
          </div>

          <StyledFormItem label="Full Name (First and Last name)">
            {getFieldDecorator('addrFirstName', {
              rules: [{required: true, message: 'Enter a valid Full Name'}],
              initialValue: savedData?.firstName
            })(<Input placeholder="" />)}
          </StyledFormItem>

          <StyledFormItem label="District">
            {getFieldDecorator('addrDistrict', {
              rules: [{required: true, message: 'Enter a valid district'}],
              initialValue: savedData?.district
            })(<Input placeholder="" />)}
          </StyledFormItem>

          <StyledFormItem label="City">
            {getFieldDecorator('addrCity', {
              rules: [{required: true, message: 'Enter a valid City'}],
              initialValue: savedData?.city
            })(
              <StyledSelect placeholder="Select Your City" showSearch allowClear>
                {cities?.[countryName] &&
                  cities?.[countryName].map((city, key) => (
                    <Option value={city} key={key}>
                      {city}
                    </Option>
                  ))}
              </StyledSelect>
            )}
          </StyledFormItem>
          <StyledFormItem label="Street / Building Name">
            {getFieldDecorator('addrAddress1', {
              rules: [{required: true, message: 'Enter a valid street or building name'}],
              initialValue: savedData?.address1
            })(<Input placeholder="" />)}
          </StyledFormItem>
          {/* <div className="grid-2">
            <StyledFormItem label="Address Type">
              {getFieldDecorator('addrType', {
                rules: [{required: true, message: 'Select Address Type'}],
                initialValue: savedData?.type
              })(
                <StyledSelect allowClear>
                  <Option value="Apartment">Apartment</Option>
                  <Option value="Office">Office</Option>
                </StyledSelect>
              )}
            </StyledFormItem>
            <StyledFormItem label="House/Builiding. No.">
              {getFieldDecorator('addrBuild', {
                rules: [{required: true, message: 'Enter a valid No.'}],
                initialValue: savedData?.build
              })(<Input placeholder="" />)}
            </StyledFormItem>
          </div> */}
          <div className="grid-3">
            <StyledFormItem label="Block No.">
              {getFieldDecorator('addrBlock', {
                rules: [{required: true, message: 'Required'}, numberFieldRules],
                initialValue: savedData?.block
              })(<InputNumber type="number" placeholder="" />)}
            </StyledFormItem>
            <StyledFormItem label="Road No.">
              {getFieldDecorator('addrRoad', {
                rules: [{required: true, message: 'Required'}, numberFieldRules],
                initialValue: savedData?.road
              })(<InputNumber type="number" placeholder="" />)}
            </StyledFormItem>
            {/* <StyledFormItem label="Floor No.">
              {getFieldDecorator('addrFloor', {
                rules: [numberFieldRules],
                initialValue: savedData?.floor
              })(<InputNumber type="number" placeholder="" />)}
            </StyledFormItem> */}
            <StyledFormItem label="Apt./House No.">
              {getFieldDecorator('addrApartment', {
                rules: [numberFieldRules],
                initialValue: savedData?.apartment
              })(<InputNumber type="number" placeholder="" />)}
            </StyledFormItem>
          </div>
          <StyledFormItem label="Address Nickname">
            {getFieldDecorator('addrAddressNickname', {
              rules: [],
              initialValue: savedData.addressNickname
            })(<Input placeholder="" />)}
          </StyledFormItem>
          <div className="actions" key="">
            <Button type="link" onClick={handleCancel} className="cancel">
              Cancel
            </Button>
            <StyledButton htmlType="submit" styles="font-family: Helvetica;">
              Save
            </StyledButton>
          </div>
        </Form>
      </CheckoutForm>
    </EditLocationModal>
  );
};

AddNewAddress.propTypes = {
  form: objectOf(any).isRequired,
  visible: bool.isRequired,
  close: func.isRequired,
  location: objectOf(string).isRequired
};

const WrappedEditLocation = Form.create({
  name: 'add-new-address'
})(AddNewAddress);

export default WrappedEditLocation;
