import styled from 'styled-components';
import {primaryColor} from '../../constants/colors';

export const Wrapper = styled.div`
  .ScrollbarsCustom-Scroller {
    padding: 0 !important;
    margin: 0 !important;
  }
  .TrackX {
    height: 0px !important;

    .ScrollbarsCustom-ThumbX {
      height: 0px !important;
    }
  }
`;

export const List = styled.div`
  display: flex;
  text-align: center;
  white-space: nowrap;
`;

export const Item = styled.div`
  ${({active}) =>
    active &&
    `
      border-color: #ed484f;
      color: #000 !important;
      font-weight: 700;
    `}
  font-family: Helvetica Neue, sans-serif;
  line-height: 16.7px;
  color: #999;
  padding: 10px ${({horizontalPadding, isAll}) => (isAll ? horizontalPadding * 2 + 'px' : horizontalPadding + 'px')};
  height: 16px !important;

  &:hover {
    cursor: pointer;
  }
`;

export const Marker = styled.div`
  position: absolute;
  z-index: 1;
  display: block;
  background-color: ${primaryColor};
  height: 2px;
  width: ${({width}) => width + 'px'};
  left: ${({left}) => left + 'px'};
  top: 36px;
  -webkit-transition: width 0.7s, left 0.7s;
  -moz-transition: width 0.7s, left 0.3s;
  -ms-transition: width 0.7s, left 0.7s;
  -o-transition: width 0.7s, left 0.7s;
  transition: width 0.7s, left 0.7s;
`;

export const FullWidthMarker = styled.div`
  position: absolute;
  height: 2px;
  width: 100vh;
  top: 36px;
  background-color: #efefef;
  width: ${({width}) => width + 'px'};
`;
