import React, {useRef, useEffect, useState} from 'react';
import Scrollbar from 'react-scrollbars-custom';
import {Wrapper, List, Item, Marker, FullWidthMarker} from './styled';
const horizontalPadding = 20;

const CategoriesMobile = ({onClick, categories, active}) => {
  const activeRef = useRef();
  const containerRef = useRef();
  const [containerWidth, setContainerWidth] = useState('100%');
  const [position, setPosition] = useState({
    width: 0,
    left: 0
  });

  useEffect(() => {
    if (activeRef && activeRef.current) {
      setPosition({
        width: activeRef.current.clientWidth + horizontalPadding / 2,
        left: activeRef.current.offsetLeft - horizontalPadding / 3
      });
    }
  }, [active]);

  useEffect(() => {
    if (containerRef && containerRef.current) {
      setContainerWidth(containerRef.current.clientWidth + categories.length * horizontalPadding * 2);
    }
  }, [containerRef, categories]);

  const renderCategories = () => {
    return categories.map((item, key) => {
      return (
        <Item
          key={key}
          ref={item.slug === active ? activeRef : null}
          active={item.slug === active}
          onClick={() => onClick(item)}
          horizontalPadding={horizontalPadding}
          isAll={item.slug === 'all'}
        >
          {item.name}
        </Item>
      );
    });
  };

  return (
    <Wrapper ref={containerRef}>
      {categories && categories.length ? (
        <Scrollbar
          disableTracksWidthCompensation
          style={{height: 38}}
          trackXProps={{
            renderer: (props) => {
              const {elementRef, ...restProps} = props;
              return <span {...restProps} ref={elementRef} className="TrackX" />;
            }
          }}
        >
          <List>{renderCategories()}</List>
          <Marker width={position.width} left={position.left} />
          <FullWidthMarker width={containerWidth} />
        </Scrollbar>
      ) : null}
    </Wrapper>
  );
};

CategoriesMobile.defaultProps = {
  title: 'Categories'
};

export default CategoriesMobile;
