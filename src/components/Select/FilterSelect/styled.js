import styled from 'styled-components';
import {Select} from 'antd';

const liHover = `
  background: #EFEFEF;
  span {
    color: #000000;
  };
  i {
    display: inline-flex;
  }
`;

export const StyledSelect = styled.div`
  padding: 6px 0;
  border-bottom: 1px solid #c3c3c3;
  width: 100%;
  max-width: 167px;
  cursor: pointer;
  display: flex;
  align-content: center;
  justify-content: space-between;
  margin-left: 12px;
  font-weight: 500;
  font-size: 14px;
  color: #000000;
  position: relative;
`;

export const ClearBlock = styled.div`
  display: flex;
  align-items: center;
  i {
    margin-left: 5px;
  }
`;

export const TopBlock = styled.div`
  padding: 13px 13px 0 8px;
`;

export const DropdownList = styled.div`
  position: absolute;
  top: -6px;
  z-index: 10;
  left: 0;
  right: 0;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  background-color: white;
  border-radius: 4px;
`;

export const StyledOption = styled(Select.Option)`
  &&& {
    display: flex !important;
    align-items: center;
    font-family: 'Helvetica', sans-serif;
    font-weight: 400;
    font-size: 144px;
    line-height: 140%;
    color: #545454;

    &::before {
      content: '';
      display: block;
      width: 12px;
      height: 12px;
      border-radius: 50%;
      background: #db95d1;
      margin: 0 8px 0 0;
    }
  }
`;

export const SelectTitle = styled.div`
  font-weight: 500;
  line-height: 132%;
`;

export const ClearAll = styled.div`
  font-size: 10px;
  line-height: 140%;
  color: #ed494f;
  ${({isAvailable}) => (isAvailable ? {color: '#ed494f'} : {color: '#CCCCCC'})}
`;

export const Header = styled.div`
  border-bottom: 1px solid #c3c3c3;
  border-radius: 0;
  margin-bottom: 10px;
`;

export const TopHeaderBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  background: #ffffff;
  box-sizing: border-box;
  padding-bottom: 6px;
`;

export const SelectedNumber = styled.p`
  font-size: 9px;
  color: #999999;
  margin-bottom: 0;
  text-align: left;
`;

export const Menu = styled.ul`
  padding: 0;
  scrollbar-width: none;
  -ms-overflow-style: none;
  &::-webkit-scrollbar {
    display: none;
  }
`;

export const MenuItem = styled.li`
  font-size: 14px;
  line-height: 140%;
  padding-top: 3px;
  padding-bottom: 3px;
  margin-bottom: 3px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px 16px;
  color: #545454;
  i {
    display: none;
  }
  ${(props) => (props.isSelected ? liHover : null)}
  &:hover {
    ${liHover}
  }
`;

export const SearchInput = styled.input`
  outline: none;
  border: none;
  box-sizing: border-box;
  border-bottom: 1px solid #c3c3c3;
  width: 100%;
  font-size: 12px;
  line-height: 132%;
  padding-bottom: 5px;
  margin-bottom: 10px;
  padding-left: 2px;
`;

export const RedCircle = styled.span`
  height: 6px;
  width: 6px;
  border-radius: 50%;
  background-color: #ed494f;
  position: absolute;
  right: 25px;
  top: 50%;
  transform: translateY(-50%);
`;
