import React, {useState, useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import PriceSelect from 'components/PriceSelect';
import Icon from 'components/Icon';
import {
  StyledSelect,
  StyledOption,
  ClearAll,
  SelectTitle,
  Header,
  Menu,
  SearchInput,
  DropdownList,
  ClearBlock,
  TopBlock,
  MenuItem,
  RedCircle,
  SelectedNumber,
  TopHeaderBlock
} from './styled';

const useOutsideAlerter = (ref, changeAction) => {
  const handleClickOutside = (event) => {
    if (ref.current && !ref.current.contains(event.target)) {
      changeAction(false);
    }
  };

  useEffect(() => {
    // Bind the event listener
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });
};

const FilterSelect = ({
  selectedValues = [],
  selection = '',
  list = ['Ab', '2', '3', '4', '5'],
  filterBy = (f) => f,
  clearAll = (f) => f,
  isSearchable = true,
  onChange = () => {}
}) => {
  const [searchQuery, setSearchQuery] = useState('');
  const [listDisplaying, setListDisplaying] = useState(false);

  const onSelect = (clickedValue) => {
    const newArray = [...selectedValues];
    if (selectedValues.includes(clickedValue)) {
      const indexToDelete = selectedValues.indexOf(clickedValue);
      newArray.splice(indexToDelete, 1);
    } else {
      newArray.push(clickedValue);
    }
    filterBy(newArray);
  };

  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef, setListDisplaying);

  const listToDisplay = searchQuery
    ? list.filter((el) => el.label.toLowerCase().includes(searchQuery.toLowerCase()))
    : list;

  return (
    <StyledSelect ref={wrapperRef} onClick={() => setListDisplaying(true)}>
      <span>{selection}</span>
      {selectedValues.length ? <RedCircle /> : null}
      <Icon type="arrowDown" width="20" height="20" color="#000000" />
      {listDisplaying && (
        <DropdownList>
          {selection === 'Price' ? (
            <PriceSelect onChange={onChange} currentPrice={selectedValues}/>
          ) : (
            <div>
              <TopBlock>
                <Header>
                  <TopHeaderBlock>
                    <SelectTitle>{selection}</SelectTitle>
                    <ClearBlock>
                      <ClearAll isAvailable={selectedValues.length} onClick={() => clearAll()}>
                        Clear All
                      </ClearAll>
                      <Icon type="arrowDown" width="20" height="20" color="#000000" />
                    </ClearBlock>
                  </TopHeaderBlock>
                  {selectedValues.length ? <SelectedNumber>{selectedValues.length} Selected</SelectedNumber> : null}
                </Header>

                {isSearchable && (
                  <SearchInput
                    placeholder="Search"
                    value={searchQuery}
                    onChange={(e) => setSearchQuery(e.target.value)}
                  />
                )}
              </TopBlock>
              <Menu>
                {listToDisplay.map((el, i) => (
                  <MenuItem key={i} isSelected={selectedValues.includes(el.value)} onClick={() => onSelect(el.value)}>
                    <span>{el.label}</span> <Icon type="checkbox" width="20" height="20" color="#000000" />
                  </MenuItem>
                ))}
              </Menu>
            </div>
          )}
        </DropdownList>
      )}
    </StyledSelect>
  );
};

FilterSelect.defaultProps = {};

FilterSelect.propTypes = {};

export default FilterSelect;
