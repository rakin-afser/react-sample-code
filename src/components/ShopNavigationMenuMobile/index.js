import React from 'react';
import {useParams, useHistory} from 'react-router-dom';
import {v1} from 'uuid';

import useGlobal from 'store';

import HomeStore from 'assets/Home';
import Feed from 'assets/FeedNew';
import {useUser} from 'hooks/reactiveVars';

import Posts from 'assets/Posts';
import Categories from 'assets/Categories';
import Info from 'assets/Info';
import Notification from 'assets/Notification';
import {ReactComponent as Profile} from 'images/svg/profile.svg';
import {ReactComponent as Messages} from 'images/svg/messages.svg';
import {ReactComponent as Union} from 'images/svg/union.svg';

import {Container, NavLink, Avatar, AddPost, AddButton} from './styled';

const ShopNavigationMenuMobile = ({data, isOwner = false}) => {
  let pages = [];
  const [, {setIsRequireAuth}] = useGlobal();
  const {shopName, shopUrl, page: currentPage} = useParams();
  const [user] = useUser();
  const [, setGlobalState] = useGlobal();
  const history = useHistory();

  const name = shopName || shopUrl;

  const renderProfileIcon = () => {
    if (user && user?.email && user?.avatar && user?.avatar?.url) {
      return <Avatar src={user.avatar.url} />;
    }

    return <Profile />;
  };

  if (isOwner) {
    pages = [
      {
        title: 'Feed',
        icon: <Feed color="#999" />,
        to: '/',
        slug: 'feed'
      },
      {
        title: user?.email && user.firstName ? user.firstName : 'My Profile',
        icon: renderProfileIcon(),
        to: '/profile/posts',
        id: 'profile'
      },
      {
        title: 'Add post',
        slug: 'add-posts'
      },
      {
        title: 'Messages',
        icon: <Messages color="#999" />,
        to: '/profile/messages',
        id: 'messages'
      },
      {
        title: 'Notifications',
        icon: <Notification fill="#999" />,
        to: '/profile/notifications',
        slug: 'notifications'
      }
    ];
  } else {
    pages = [
      {
        title: 'Store Home',
        icon: <HomeStore color="#999" />,
        to: `/shop/${name}/products`,
        slug: 'products'
      },
      {
        title: 'Category',
        icon: <Categories color="#999" />,
        to: `/shop/${name}/categories`,
        slug: 'categories'
      },
      {
        title: 'Post',
        icon: <Posts color="#999" />,
        to: `/shop/${name}/posts`,
        slug: 'posts'
      },
      {
        title: 'About',
        icon: <Info color="#999" />,
        to: `/shop/${name}/about`,
        slug: 'about'
      },
      {
        title: 'Chat',
        icon: <Messages color="#999" />,
        to: ''
      }
    ];
  }

  const onSendMessage = (e) => {
    e.preventDefault();
    setGlobalState.setMessenger({
      data: {
        databaseId: data?.store?.id,
        username: data?.store?.name,
        photoUrl: data?.store?.gravatar
      }
    });
    history.push('/profile/messages');
  };

  async function onChange(e) {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      try {
        const medias = await Promise.all(
          files.map((file) => {
            return new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.addEventListener('load', (ev) => {
                resolve({
                  id: v1(),
                  file,
                  result: ev.target.result
                });
              });
              reader.addEventListener('error', reject);
              reader.readAsDataURL(file);
            });
          })
        );
        history.push('/post/new', {medias});
      } catch (error) {
        console.log(`error`, error);
      }
    }
  }

  const onAddButton = user?.email ? (e) => e.currentTarget.previousSibling.click() : () => setIsRequireAuth(true);

  return (
    <Container>
      {pages.map((page) => {
        if (page.title === 'Chat') {
          return (
            <NavLink onClick={onSendMessage} current={currentPage === page.slug ? 'true' : undefined} key={page.slug}>
              {page.icon}
              {page?.title}
            </NavLink>
          );
        }

        if (page.title === 'Add post') {
          return (
            <AddPost key={page.id}>
              <input onChange={onChange} type="file" style={{display: 'none'}} multiple accept="image/*, video/*" />
              <AddButton onClick={onAddButton}>
                <Union />
              </AddButton>
              Add Post
            </AddPost>
          );
        }

        return (
          <NavLink current={currentPage === page.slug ? 'true' : undefined} key={page.slug} to={page.to}>
            {page.icon}
            {page?.title}
          </NavLink>
        );
      })}
    </Container>
  );
};

export default ShopNavigationMenuMobile;
