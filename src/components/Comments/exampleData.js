import avatar2 from './img/Avatar2.png';
import avatar from './img/avatar.png';
import oval from './img/Oval.png';

export const exampleData = [
  {
    ava: avatar,
    name: 'James Charles',
    text: 'Colors will depend heavily on the setting the character who wears.',
    likes: '3k',
    repliesNumber: '2 Replies',
    date: '3 months ago',
    replies: [
      {
        ava: avatar2,
        name: 'Charles James',
        replyTo: 'James Charles',
        text: 'Colors will depend heavily on the setting the character who wears.',
        likes: '5k',
        repliesNumber: '4 Replies',
        date: '2 months ago'
      }
    ]
  },
  {
    ava: avatar,
    name: 'James Charles',
    text: 'Colors will depend heavily on the setting the character who wears.',
    likes: '3k',
    repliesNumber: '2 Replies',
    date: '3 months ago',
    replies: []
  },
  {
    ava: avatar,
    name: 'James Charles',
    text: 'Colors will depend heavily on the setting the character who wears.',
    likes: '3k',
    repliesNumber: '2 Replies',
    date: '3 months ago',
    replies: []
  },
  {
    ava: avatar,
    name: 'James Charles',
    text: 'Colors will depend heavily on the setting the character who wears.',
    likes: '3k',
    repliesNumber: '2 Replies',
    date: '3 months ago',
    replies: []
  },
  {
    ava: avatar,
    name: 'James Charles',
    text: 'Colors will depend heavily on the setting the character who wears.',
    likes: '3k',
    repliesNumber: '2 Replies',
    date: '3 months ago',
    replies: []
  },
  {
    ava: avatar,
    name: 'James Charles',
    text: 'Colors will depend heavily on the setting the character who wears.',
    likes: '3k',
    repliesNumber: '2 Replies',
    date: '3 months ago'
  }
];

export const hashTags = [
  {name: 'BillieElish', subtitle: '63541 times'},
  {name: 'popularshops', subtitle: '631 times'},
  {name: 'topartists', subtitle: '16351 times'},
  {name: 'popmusic', subtitle: '96354 times'},
  {name: 'ilovemusic', subtitle: '26354 times'}
];

export const users = [
  {avatar: oval, name: 'BillieElish', subtitle: 'whereisavocado | Following'},
  {avatar: oval, name: 'A', subtitle: 'whereisavocado | Following'},
  {avatar: oval, name: 'Ab', subtitle: 'whereisavocado | Following'},
  {avatar: oval, name: 'asdasd as2d', subtitle: 'whereisavocado | Following'},
  {avatar: oval, name: 'aaxorp', subtitle: 'whereisavocado | Following'}
];
