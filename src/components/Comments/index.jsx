import React from 'react';
import {useWindowSize} from '@reach/window-size';
import CommentsDesktop from './CommentsDesktop';
import CommentsMobile from './CommentsMobile';

const MyProfile = (props) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return isMobile ? <CommentsMobile {...props} /> : <CommentsDesktop {...props} />;
};

export default MyProfile;
