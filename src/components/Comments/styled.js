import styled, {css} from 'styled-components/macro';
import Icon from 'components/Icon';

import {blue, grayBackroundColor, primaryColor} from 'constants/colors';
import {mainFont} from 'constants/fonts';

export const Item = styled.div`
  position: relative;
  display: flex;
  padding: 16px;
  flex-wrap: wrap;

  ${({bordered}) =>
    bordered
      ? css`
          border-bottom: 1px solid #e4e4e4;
        `
      : null}

  &:last-child {
    border-bottom: none;
  }
`;

export const List = styled.div`
  border-top: 1px solid #e4e4e4;
`;

export const ListMobile = styled.div`
  padding-bottom: 8px;

  & ${Item}:last-child {
    border-bottom: none;
  }
`;

export const Name = styled.span`
  font-family: Helvetica Neue, sans-serif;
  font-weight: bold;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  max-width: 145px;
  align-items: center;
  color: #000;
  margin: 4px 0;
`;

// export const ShowAll = styled.button`
//   color: #ed484f;
//   margin: 0 0 0 85px;
//   border: none;
//   background: transparent;
//   text-align: left;
//   padding: 0;
// `;

export const Message = styled.div`
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #000;
  ${({cutWidth}) => (cutWidth ? 'max-width: 224px' : 'max-width: 388px')}
`;

export const Date = styled.span`
  position: absolute;
  top: 22px;
  right: 22px;
  font-family: Helvetica Neue, sans-serif;
  font-size: 12px;
  line-height: 140%;
  color: #999999;
`;

export const Activity = styled.div`
  display: flex;
  align-items: center;

  span {
    cursor: pointer;
  }
`;

export const Likes = styled.span`
  font-family: ${mainFont}, sans-serif;
  display: inline-flex;
  align-items: center;
  margin: 0 16px 0 0;
  font-size: 12px;
  line-height: 132%;
  color: #333;

  svg {
    margin-right: 5px;
  }
`;

export const Shares = styled.span`
  font-family: ${mainFont}, sans-serif;
  font-size: 12px;
  line-height: 132%;
  color: #000;
`;

export const AddComment = styled.div`
  display: flex;
  align-items: center;
  padding: 9px 18px 9px 2px;
  position: relative;
  border: 1px solid #efefef;
  border-radius: 4px;
`;

export const UserPicHolder = styled.div`
  width: ${({small}) => (small ? '28px' : '40px')};
  height: ${({small}) => (small ? '28px' : '40px')};
  min-width: ${({small}) => (small ? '28px' : '40px')};
  min-height: ${({small}) => (small ? '28px' : '40px')};
  border-radius: 50%;
  overflow: hidden;
  margin: ${({comment}) => (comment ? '0 8px 0 21px' : '0 12px 0 0')};
`;

export const UserPic = styled.img`
  display: block;
  object-fit: cover;
  width: 100%;
  height: 100%;
`;

export const InputMessage = styled.input`
  height: 100%;
  flex-grow: 1;
  background: transparent;
  border: none;
  outline: none;
  color: black;
`;

export const Send = styled.button`
  margin-left: auto;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  background: ${grayBackroundColor};
  border: none;
  outline: none;
  cursor: pointer;
  transition: background-color 0.3s;

  &.active {
    background: ${primaryColor};
  }

  &:hover {
    background-color: ${primaryColor};
  }
`;

export const CloseCommentsContainer = styled.p`
  margin-bottom: 0;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  cursor: pointer;
`;

export const HorizontalDivider = styled.i`
  display: block;
  width: 100%;
  height: 1px;
  background: #e4e4e4;
`;

export const Reply = styled.span`
  font-family: ${mainFont}, sans-serif;
  color: #666666;
  font-size: 12px;
  margin-right: 3px;
  line-height: 132%;
  transition: ease 0.4s;
  display: inline-flex;
  align-items: center;

  span {
    margin-right: 8px;
    margin-left: 8px;
  }

  &:hover {
    color: ${blue};
    text-decoration: underline;
  }
`;

export const Report = styled.span`
  display: inline-block;
  margin-left: 18px;
  color: #999;
  transition: color 0.3s ease-in;

  &:hover {
    color: #ed484f;
  }
`;

export const ReplyItem = styled.div`
  margin: ${({isModal}) => (isModal ? '0 0 0 42px;' : ' 0 47px 0 85px')};
`;

export const ReplyBlock = styled.div`
  border-top: 1px solid #ececec;
  padding: 12px 32px 12px 16px;
  font-size: 14px;
  line-height: 140%;
  color: #999;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const CloseReply = styled(Icon)`
  cursor: pointer;
`;

export const To = styled.span`
  ${({isReplyTo}) =>
    isReplyTo
      ? {
          fontWeight: 'bold',
          fontSize: '14px',
          lineHeight: '140%',
          color: '#000000'
        }
      : {color: '#1b5dab'}};
`;

export const Container = styled.div`
  background: #fff;
  flex: 1 0 100%;
`;

export const SeeBlockMobile = styled.p`
  width: 100%;
  max-width: 414px;
  margin: 0 auto;
  padding: 10px 0;
  text-align: center;
  font-weight: normal;
  font-size: 11px;
  color: #353535;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const FlagBtn = styled.button`
  margin-left: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 25px;
  height: 25px;
  transition: ease 0.4s;
  cursor: pointer;
  background-color: transparent;
  border-radius: 50%;

  svg {
    transition: ease 0.4s;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    background-color: #d6d6d6;

    svg {
      fill: ${primaryColor};

      path {
        fill: ${primaryColor};
        stroke: ${primaryColor};
      }
    }
  }
`;
