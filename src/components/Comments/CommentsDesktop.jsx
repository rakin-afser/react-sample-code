import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

import Icon from 'components/Icon';
import FindPopUp from './components/FindPopUp';
import {Container, List, AddComment, Send, CloseCommentsContainer, ReplyBlock, To, CloseReply} from './styled';
import Scrollbar from 'components/Scrollbar';
import Comment from './components/Comment';
import {useCreateCommentDesktopMutation} from 'hooks/mutations';
import {useCommentsOfDesktopPostQuery, useRelatedUsersLazyQuery} from 'hooks/queries';
import {endElement} from 'util/elements';
import Replies from './components/Replies';
import CustomMentions from 'components/CustomMentions';

const Comments = ({
  postId,
  isModal = true,
  hideComments = (f) => f,
  showReportPopup = () => {},
  quickView = false,
  notExpandable = false,
  maxHeight = 342,
  commentOn
}) => {
  const [onUpdate, {data, loading}] = useCommentsOfDesktopPostQuery({
    variables: {
      id: postId,
      first: 10
    }
  });
  const {hasNextPage} = data?.post?.comments?.pageInfo || {};

  const [commentText, setCommentText] = useState('');
  const [selectedItem, setSelectedItem] = useState(false);
  const [isFocused, setIsFocused] = useState(false);
  const [reply, setReply] = useState(null);
  const replyTo = reply?.parent ? `@${reply?.author?.node?.username} ` : null;
  const [getRelatedUsers, {data: dataRelated, loading: loadingRelated}] = useRelatedUsersLazyQuery();
  const {followers, following} = dataRelated?.user || {};
  const relatedUsers = [...(followers || []), ...(following || [])]?.filter(
    (p, i, a) => a.findIndex((pr) => pr.id === p.id) === i
  );

  useEffect(() => {
    setCommentText(replyTo || '');
  }, [replyTo]);

  const commentId = reply?.parent ? reply?.parent?.node?.id : reply?.id;

  const [createComment, {loading: createCommentLoading}] = useCreateCommentDesktopMutation({
    onCompleted() {
      setCommentText('');
      setReply(null);
    },
    postId,
    commentId
  });

  const onCreateComment = (content, rpl) => {
    const parent = rpl?.parent ? rpl?.parent?.node?.databaseId : rpl?.databaseId;

    createComment({
      variables: {input: {content, commentOn, parent}}
    });
  };

  let searchType = '';
  if (commentText.charAt(0) === '#') {
    searchType = '#';
  } else if (commentText.charAt(0) === '@') {
    searchType = '@';
  }

  const selectItem = (val) => {
    setSelectedItem(true);
    setCommentText(val);
  };

  const onInputMessageFocus = () => {
    setIsFocused(true);
  };

  const onInputMessageBlur = () => {
    if (commentText === '') {
      setIsFocused(false);
    }
  };

  useEffect(() => {
    if (!commentText.length && selectedItem) {
      setSelectedItem(false);
    }
  }, [commentText, selectedItem]);

  return (
    <>
      <Container isModal={isModal}>
        {/* {isModal && !notExpandable && (
          <CloseCommentsContainer onClick={() => hideComments(false)}>
            Close Comments <Icon type="close" />
          </CloseCommentsContainer>
        )} */}
        <List>
          <Scrollbar onUpdate={onUpdate} autoHeight autoHeightMax={maxHeight}>
            {data?.post?.comments?.nodes?.map((comment) => (
              <Comment
                setReply={setReply}
                key={comment?.id}
                comment={comment}
                showReportPopup={showReportPopup}
                commentOn={commentOn}
                renderReplies={
                  <Replies
                    setReply={setReply}
                    comment={comment}
                    commentOn={commentOn}
                    showReportPopup={showReportPopup}
                  />
                }
                bordered
              />
            ))}
            {endElement(hasNextPage, loading)}
          </Scrollbar>
        </List>
        {!createCommentLoading && reply ? (
          <ReplyBlock>
            <span>
              Reply to <To>{reply?.author?.node?.name}</To>
            </span>
            <CloseReply onClick={() => setReply(null)} type="cross" width={13} height={13} fill="#666" />
          </ReplyBlock>
        ) : null}
        <AddComment bt={1}>
          {/* {commentText && !selectedItem && (
            <FindPopUp select={selectItem} query={commentText.substring(1)} type={searchType} />
          )} */}
          <CustomMentions
            loading={loadingRelated}
            value={createCommentLoading ? '' : commentText}
            onFocus={onInputMessageFocus}
            onBlur={onInputMessageBlur}
            onChange={(v) => setCommentText(v)}
            onSearch={(v) => getRelatedUsers({variables: {search: v}})}
            placeholder={!createCommentLoading && reply ? 'Add a reply' : 'Add a comment'}
            relations={relatedUsers}
          />
          <Send onClick={() => onCreateComment(commentText, reply)} className={isFocused ? 'active' : ''}>
            {createCommentLoading ? (
              <Icon type="loader" color="#000" width={20} height={20} />
            ) : (
              <Icon type="send" width={13} height={13} />
            )}
          </Send>
        </AddComment>
      </Container>
    </>
  );
};

Comments.propTypes = {
  quickView: PropTypes.bool
};

export default Comments;
