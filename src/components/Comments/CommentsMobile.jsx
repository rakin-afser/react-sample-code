import React, {useState, useRef} from 'react';
import parse from 'html-react-parser';
import moment from 'moment';

import Icon from 'components/Icon';
import Popup from 'components/MobileCommentsPopup';
import {ReactComponent as ArrowRight} from 'images/svg/arrow-right.svg';
import useGlobal from 'store';
import {
  Activity,
  CloseCommentsContainer,
  Container,
  Date,
  HorizontalDivider,
  Item,
  Likes,
  ListMobile,
  Message,
  Name,
  Reply,
  ReplyItem,
  Report,
  SeeBlockMobile,
  Shares,
  To,
  UserPic,
  UserPicHolder
} from './styled';
import userPlaceholder from 'images/placeholders/user.jpg';
import {useUser} from 'hooks/reactiveVars';
import BackPage from 'components/Modals/mobile/BackPage';

const Comments = ({
  data,
  postId,
  commentOn,
  isModal = true,
  hideComments = (f) => f,
  showCommentsPopup,
  setShowCommentsPopup,
  commentCount
}) => {
  const [, {setIsRequireAuth, setReportPopup}] = useGlobal();
  const contentRef = useRef(null);
  const [user] = useUser();
  const [reply, setReply] = useState(null);
  const [showMore] = useState(false);
  const [isLiked, setIsLiked] = useState(false);

  const commentsData = data?.filter((el) => el.parent === null);

  function onLike() {
    if (!user?.email) {
      setIsRequireAuth(true);
    } else {
      setIsLiked(!isLiked);
    }
  }

  function onReply(comment) {
    if (!user?.databaseId) {
      setIsRequireAuth(true);
    } else {
      setReply({...comment, active: true});
      setShowCommentsPopup(true);
    }
  }

  function onReport() {
    setReportPopup({
      active: true,
      subjectId: commentsData?.[0]?.databaseId,
      subjectSource: 'comment_report'
    });
  }

  // function renderLastComment() {
  //   if (!commentsData?.length) return null;
  // }

  function renderComments() {
    if (!commentsData?.length) return null;

    return (
      <>
        {isModal && (
          <CloseCommentsContainer onClick={() => hideComments(false)}>
            Close Comments <Icon type="close" />
          </CloseCommentsContainer>
        )}
        <HorizontalDivider />
        <ListMobile>
          {isModal || (!isModal && showMore) ? (
            <>
              {commentsData.map((el, i) => (
                <>
                  <Item isModal={isModal} key={i}>
                    <UserPicHolder comment>
                      <UserPic src={el?.author?.node?.avatar?.url || userPlaceholder} />
                    </UserPicHolder>
                    <div>
                      <Name>{el.name}</Name>
                      <Message cutWidth={isModal}>{el.text}</Message>
                      <Activity>
                        <Likes>
                          <Icon width={13} height={13} type="like" color="#333" />
                          {el.totalLikes}
                        </Likes>
                        <Shares>{el.replies?.nodes?.length}</Shares>
                        <Reply onClick={() => setReply(true)}>Reply</Reply>
                      </Activity>
                    </div>
                    <Date>{el.date}</Date>
                  </Item>
                  {/* {el.replies
                    ? el.replies.map((reply, replyIndex) => (
                        <ReplyItem isModal={isModal} key={replyIndex}>
                          <Item isModal={isModal} key={i}>
                            <UserPicHolder comment>
                              <UserPic src={reply.ava} />
                            </UserPicHolder>
                            <div>
                              <Name>{reply.name}</Name>
                              <Message cutWidth={isModal}>
                                <To>@{reply.replyTo}</To> {reply.text}
                              </Message>
                              <Activity>
                                <Likes>
                                  <Icon width={13} height={13} type="like" color="#333" />
                                  {reply.likes}
                                </Likes>
                                <Shares>{reply.repliesNumber}</Shares>
                              </Activity>
                            </div>
                            <Date>{reply.date}</Date>
                          </Item>
                        </ReplyItem>
                      ))
                    : null} */}
                </>
              ))}
            </>
          ) : (
            <Item>
              <UserPicHolder comment>
                <UserPic src={commentsData[0]?.author?.node?.avatar?.url || userPlaceholder} />
              </UserPicHolder>
              <div>
                <Name>{commentsData[0]?.author?.node?.name}</Name>
                <Message cutWidth={isModal}>{parse(commentsData?.[0]?.content || '')}</Message>
                <Activity>
                  <Likes onClick={onLike}>
                    <Icon width={13} height={13} type={isLiked ? 'liked' : 'like'} color="#333" />
                    {commentsData?.[0]?.totalLikes || ''}
                  </Likes>
                  <Reply onClick={() => onReply(commentsData?.[0])}>
                    <Icon width={13} height={13} type="message" color="#333" />
                    <span />
                    Reply
                  </Reply>
                  <Report onClick={onReport}>
                    <Icon type="flag" />
                  </Report>
                </Activity>
              </div>
              <Date>{moment(commentsData?.[0]?.date).format('DD MMM YYYY')}</Date>
            </Item>
          )}
        </ListMobile>
        <SeeBlockMobile onClick={() => setShowCommentsPopup(true)}>
          Show all comments
          <ArrowRight />
        </SeeBlockMobile>
      </>
    );
  }

  const options = reply?.active
    ? {
        type: 'replies',
        title: 'Replies',
        setActive: (active) => setReply({...reply, active}),
        onClose: (active) => {
          setShowCommentsPopup(active);
          setTimeout(() => {
            setReply({...reply, active});
          }, 300);
        }
      }
    : {
        type: 'comments',
        title: `<span class="text">Comments</span> ${commentCount ? `<span class="count">${commentCount}</span>` : ''}`,
        setActive: setShowCommentsPopup,
        onClose: setShowCommentsPopup,
        contentStyle: {
          height: 'calc(100% - 123px)'
        }
      };

  return (
    <Container>
      {renderComments()}
      <BackPage {...options} active={showCommentsPopup} ref={contentRef}>
        <Popup
          contentRef={contentRef}
          postId={postId}
          commentOn={commentOn}
          showCommentsPopup={showCommentsPopup}
          setShowCommentsPopup={setShowCommentsPopup}
          data={commentsData}
          reply={reply}
          setReply={setReply}
        />
      </BackPage>
    </Container>
  );
};

export default Comments;
