import React from 'react';
import Icon from 'components/Icon';
import {Item, UserPicHolder, UserPic, Name, Message, Activity, Likes, Reply, FlagBtn, Date} from '../../styled';
import userPlaceholder from 'images/placeholders/user.jpg';
import moment from 'moment';
import parse from 'html-react-parser';
import {useAddUpdateLikedCommentMutation} from 'hooks/mutations';

const Comment = (props) => {
  const {comment, showReportPopup, isModal, renderReplies, small, bordered, setReply} = props || {};
  const {id, databaseId, author, replies, date, content, isLiked, totalLikes} = comment || {};
  const [triggerLike] = useAddUpdateLikedCommentMutation({variables: {input: {id: databaseId}}, comment});

  return (
    <Item key={id} bordered={bordered}>
      <UserPicHolder small={small ? 1 : 0}>
        <UserPic src={author?.node?.avatar?.url || userPlaceholder} />
      </UserPicHolder>
      <div>
        <Name>{author?.node?.name}</Name>
        <Message cutWidth={isModal}>{parse(content)}</Message>
        <Activity>
          <Likes onClick={() => triggerLike()}>
            <Icon type={isLiked ? 'liked' : 'like'} color="#8F8F8F" width={13} height={13} /> {totalLikes || ''}
          </Likes>
          <Reply onClick={() => setReply(comment)}>
            {
            replies?.nodes?.length
              ?
              <>
                <Icon type="message" width={13} height={13} color="#8F8F8F" />
                <span>{replies?.nodes?.length}</span>
              </>
              : null
            }
            Reply
          </Reply>
          <FlagBtn onClick={() => showReportPopup(true)}>
            <Icon type="flag" />
          </FlagBtn>
        </Activity>
      </div>
      <Date>{moment(date).format('DD MMM YYYY')}</Date>
      {replies?.nodes?.length ? renderReplies : null}
    </Item>
  );
};

Comment.defaultProps = {
  setReply: () => {}
};

export default Comment;
