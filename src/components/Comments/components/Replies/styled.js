import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';

export const Wrapper = styled.div`
  flex: 1 0 100%;
  background-color: #fff;
`;

export const ShowMore = styled.button`
  padding: 5px 0;
  font-family: ${secondaryFont};
  font-weight: 500;
  font-size: 12px;
  line-height: 1.333;
  color: #000;
`;

export const RepliesHolder = styled.div`
  width: 100%;
  padding-left: 54px;
`;

export const ToggleReplies = styled.button`
  font-family: ${secondaryFont};
  font-weight: 500;
  font-size: 12px;
  line-height: 1.333;
  color: #000;
  padding: 5px 0 5px 15px;
  position: relative;

  &::before {
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 5px 5px 0 5px;
    border-color: currentColor transparent transparent transparent;
    position: absolute;
    left: 0;
    top: 9px;
    transform: ${({active}) => (active ? 'rotate(180deg)' : 'rotate(0)')};
    transition: transform 0.3s ease;
  }
`;
