import React from 'react';
import {useQuery} from '@apollo/client';
import Loader from 'components/Loader';
import {REPLY} from 'queries';
import Comment from '../Comment';
import {Wrapper, ShowMore} from './styled';

const List = (props) => {
  const {comment, commentOn, showReportPopup, setReply} = props;
  const {id} = comment || {};
  const {data, loading, fetchMore} = useQuery(REPLY, {
    variables: {id},
    notifyOnNetworkStatusChange: true
  });
  const {replies} = data?.comment || {};
  const {hasNextPage, endCursor} = replies?.pageInfo || {};

  // const [commentText, setCommentText] = useState('');
  // const [isFocused, setIsFocused] = useState(false);

  // const onCreateComment = (content) => {
  //   createComment({
  //     variables: {input: {content, parent: databaseId, commentOn}}
  //   });
  // };

  // const [createComment, {loading: createCommentLoading}] = useCreateCommentMutation({
  //   commentId: id,
  //   onCompleted() {
  //     setCommentText('');
  //   }
  // });

  function onShowMore() {
    fetchMore({
      updateQuery(prev, {fetchMoreResult}) {
        if (!fetchMoreResult) return prev;
        const prevNodes = prev?.comment?.replies?.nodes || [];
        const newNodes = fetchMoreResult?.comment?.replies?.nodes || [];
        const nodes = [...prevNodes, ...newNodes];

        return {
          ...prev,
          comment: {
            ...prev?.comment,
            replies: {
              ...prev?.comment?.replies,
              nodes,
              pageInfo: fetchMoreResult?.comment?.replies?.pageInfo
            }
          }
        };
      },
      variables: {
        first: 5,
        after: endCursor
      }
    });
  }

  return (
    <Wrapper>
      {!replies?.nodes?.length && !loading ? 'There are no replies yet.' : ''}
      {replies?.nodes?.map((rpl) => (
        <Comment
          key={rpl?.id}
          comment={rpl}
          showReportPopup={showReportPopup}
          commentOn={commentOn}
          setReply={setReply}
        />
      ))}
      {!loading && hasNextPage ? <ShowMore onClick={onShowMore}>Show more replies</ShowMore> : null}
      {loading ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
    </Wrapper>
  );
};

export default List;
