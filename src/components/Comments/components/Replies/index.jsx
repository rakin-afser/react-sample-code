import React, {useState} from 'react';
import {RepliesHolder, ToggleReplies} from './styled';
import List from './List';

const Replies = (props) => {
  const [active, setActive] = useState(null);
  return (
    <RepliesHolder>
      <ToggleReplies active={active ? 1 : 0} onClick={() => setActive(!active)}>
        Replies
      </ToggleReplies>
      {active ? <List {...props} small /> : null}
    </RepliesHolder>
  );
};

export default Replies;
