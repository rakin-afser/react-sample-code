import styled from 'styled-components';

export const Wrapper = styled.div`
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.28);
  background: #ffffff;
  border-radius: 8px;
  padding-top: 12px;
  position: absolute;
  left: 17px;
  bottom: 56px;
  z-index: 10;
  .ScrollbarsCustom-Wrapper {
    right: 0 !important;
  }
  ${({customStyles}) => (customStyles ? {...customStyles} : null)}
`;

export const Title = styled.h3`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const TopContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 16px;
  margin-bottom: 20px;
`;

export const ItemsList = styled.ul`
  max-height: 224px;
  -ms-overflow-style: none; // IE 10+
  overflow: -moz-scrollbars-none; // Firefox
  overflow-y: auto;
  &::-webkit-scrollbar {
    display: none; // Safari and Chrome
  }
`;

export const Item = styled.li`
  padding: 10px 5px 10px 16px;
  border-bottom: 1px solid #efefef;
  width: 100%;
  cursor: pointer;
  display: flex;
`;

export const Name = styled.h3`
  font-weight: 500;
  font-size: 12px;
  margin-bottom: 2px;
`;

export const Subtitle = styled.p`
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #a7a7a7;
  margin: 0;
`;

export const Avatar = styled.img`
  width: 36px;
  height: 36px;
  margin-right: 12px;
`;
