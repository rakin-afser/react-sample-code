import React, {useState, useEffect} from 'react';
import Grid from 'components/Grid';
import Breadcrumbs from 'components/Breadcrumbs';
import {Col, Row} from 'antd';
import FilterSelect from 'components/Select/FilterSelect';
import Icon from 'components/Icon';
import {
  Title,
  Filter,
  FilterOption,
  SelectedFilter,
  Clear,
  AppliedFilters,
  PostSearchFilters,
  GetPostFilters,
  BreadcrumbsContainer
} from '../styled';
import {brandData, colorData, sizeData, priceData, countryData} from './exampleData';

const filters = ['Location', 'Category', 'Brand', 'Price', 'Sort by'];
const selectedTags = ["Women's clothing   X", 'All brands   X', 'Off White   X'];

function getFilters(options = filters) {
  return options.map((item) => (
    <Filter defaultValue={item} clearIcon="Clear All">
      <FilterOption key="option" value="option">
        <span>Option</span>
      </FilterOption>
    </Filter>
  ));
}

function getAppliedFilters(tags, onClear, additionaLFilters) {
  const allTags = tags || selectedTags;
  if (additionaLFilters) {
    allTags.push(additionaLFilters);
  }
  return (
    <AppliedFilters>
      <Row>
        {allTags.map((el, i) => (
          <SelectedFilter key={i}>
            {el} <Icon type="close" width={20} height={20} color="##000000" />
          </SelectedFilter>
        ))}
      </Row>
      <Clear onClick={() => onClear()}>Clear All</Clear>
    </AppliedFilters>
  );
}

const Filters = ({isPostSearch, title, additionalTags = [], deleteAdditionalTag = (f) => f, filters, setFilters}) => {
  const [sizes, setSizes] = useState([]);
  const [colors, setColors] = useState([]);
  const [brands, setBrands] = useState([]);
  const [prices, setPrices] = useState(filters?.price || []);
  const [locations, setLocations] = useState([]);

  // useEffect(() => {

  // }, [additionalTags])

  const stateChanging = (selectedFunction, index) => {
    selectedFunction((oldArray) => {
      const newArray = [...oldArray];
      newArray.splice(index, 1);
      return newArray;
    });
  };

  const deleteItem = (indexOfState, valueIndex) => {
    switch (indexOfState) {
      case 0:
        stateChanging(setSizes, valueIndex);
        break;
      case 1:
        stateChanging(setColors, valueIndex);
        break;
      case 2:
        stateChanging(setBrands, valueIndex);
        break;
      case 3:
        stateChanging(setPrices, valueIndex);
        break;
      case 4:
        stateChanging(setLocations, valueIndex);
        break;
      case 5:
        stateChanging(deleteAdditionalTag, valueIndex);
        break;
      default:
        break;
    }
  };

  const deleteAll = () => {
    setSizes([]);
    setColors([]);
    setBrands([]);
    setPrices([]);
    setLocations([]);
    deleteAdditionalTag([]);
    setFilters();
  };

  const allValues = [sizes, colors, brands, prices, locations, additionalTags];
  const isSelected =
    sizes.length || colors.length || brands.length || prices.length || locations.length || additionalTags.length;

  return (
    <Row>
      {title ? (
        <Col span={6}>
          <Grid aic>
            <Title>{title}</Title>
          </Grid>
        </Col>
      ) : null}

      {!isPostSearch ? (
        <div>
          <Row style={{display: 'flex', marginLeft: '3px', justifyContent: 'flex-start'}}>
            <FilterSelect
              selection="Size"
              list={sizeData}
              selectedValues={sizes}
              filterBy={setSizes}
              clearAll={() => setSizes([])}
            />
            <FilterSelect
              selection="Color"
              list={colorData}
              selectedValues={colors}
              filterBy={setColors}
              clearAll={() => setColors([])}
            />
            <FilterSelect
              selection="Brand"
              list={brandData}
              selectedValues={brands}
              filterBy={setBrands}
              clearAll={() => setBrands([])}
            />
            <FilterSelect
              selection="Price"
              list={priceData}
              selectedValues={prices}
              filterBy={setPrices}
              clearAll={() => setPrices([])}
              onChange={(value) => setFilters({price: value})}
            />
            <FilterSelect
              selection="Location"
              list={countryData}
              selectedValues={locations}
              filterBy={setLocations}
              clearAll={() => setLocations([])}
            />
          </Row>
          {isSelected ? (
            <AppliedFilters>
              <div>
                {allValues.map((state, stateIndex) =>
                  state.map((el, i) => (
                    <SelectedFilter key={i}>
                      {el}{' '}
                      <Icon
                        onClick={() => deleteItem(stateIndex, i)}
                        type="close"
                        width={20}
                        height={20}
                        color="#000000"
                      />
                    </SelectedFilter>
                  ))
                )}
              </div>
              <Clear onClick={() => deleteAll()}>Clear All</Clear>
            </AppliedFilters>
          ) : null}
        </div>
      ) : null}
    </Row>
  );
};

export default Filters;
