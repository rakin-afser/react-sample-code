import React, {useState} from 'react';
import {Col, Row} from 'antd';
import PropTypes from 'prop-types';

import Grid from 'components/Grid';
import FilterSelect from 'components/Select/FilterSelect';
import Icon from 'components/Icon';
import {Title, SelectedFilter, Clear, AppliedFilters} from '../styled';
import {brandData, colorData, sizeData, priceData} from '../ProductFilter/exampleData';

const Filters = ({isPostSearch, title, additionalTags, deleteAdditionalTag, type}) => {
  const [sizes, setSizes] = useState([]);
  const [colors, setColors] = useState([]);
  const [brands, setBrands] = useState([]);
  const [prices, setPrices] = useState([]);

  const stateChanging = (selectedFunction, index) => {
    selectedFunction((oldArray) => {
      const newArray = [...oldArray];
      newArray.splice(index, 1);
      return newArray;
    });
  };

  const deleteItem = (indexOfState, valueIndex) => {
    switch (indexOfState) {
      case 0:
        stateChanging(setSizes, valueIndex);
        break;
      case 1:
        stateChanging(setColors, valueIndex);
        break;
      case 2:
        stateChanging(setBrands, valueIndex);
        break;
      case 3:
        stateChanging(setPrices, valueIndex);
        break;
      case 4:
        stateChanging(deleteAdditionalTag, valueIndex);
        break;
      default:
        break;
    }
  };

  const deleteAll = () => {
    setSizes([]);
    setColors([]);
    setBrands([]);
    setPrices([]);
    deleteAdditionalTag([]);
  };

  const allValues = [sizes, colors, brands, prices, additionalTags];
  const isSelected = sizes.length || colors.length || brands.length || prices.length || additionalTags.length;

  return (
    <Row>
      {title ? (
        <Col span={6}>
          <Grid aic>
            <Title>{title}</Title>
          </Grid>
        </Col>
      ) : null}

      {!isPostSearch ? (
        <div>
          <Row style={{display: 'flex', marginLeft: '3px', justifyContent: 'flex-end'}}>
            {type !== 'lists' && (
              <FilterSelect
                selection="Category"
                list={sizeData}
                selectedValues={sizes}
                filterBy={setSizes}
                clearAll={() => setSizes([])}
              />
            )}
            {type === 'posts' && (
              <FilterSelect
                selection="Post Type"
                list={colorData}
                selectedValues={colors}
                filterBy={setColors}
                clearAll={() => setColors([])}
              />
            )}
            {type === 'stores' && (
              <FilterSelect
                selection="Other preferences"
                list={colorData}
                selectedValues={colors}
                filterBy={setColors}
                clearAll={() => setColors([])}
              />
            )}
            <FilterSelect
              selection="Hashtags"
              list={brandData}
              selectedValues={brands}
              filterBy={setBrands}
              clearAll={() => setBrands([])}
            />
            <FilterSelect
              selection="Sort By"
              list={priceData}
              selectedValues={prices}
              filterBy={setPrices}
              clearAll={() => setPrices([])}
            />
          </Row>
          {isSelected ? (
            <AppliedFilters style={{maxWidth: 882, marginLeft: 'auto'}}>
              <div>
                {allValues.map((state, stateIndex) =>
                  state.map((el, i) => (
                    <SelectedFilter key={i}>
                      {el}{' '}
                      <Icon
                        onClick={() => deleteItem(stateIndex, i)}
                        type="close"
                        width={20}
                        height={20}
                        color="#000000"
                      />
                    </SelectedFilter>
                  ))
                )}
              </div>
              <Clear onClick={() => deleteAll()}>Clear All</Clear>
            </AppliedFilters>
          ) : null}
        </div>
      ) : null}
    </Row>
  );
};

Filters.defaultProps = {
  isPostSearch: false,
  title: '',
  additionalTags: [],
  deleteAdditionalTag: (f) => f
};

Filters.propTypes = {
  isPostSearch: PropTypes.bool,
  title: PropTypes.string,
  additionalTags: PropTypes.arrayOf(PropTypes.any),
  deleteAdditionalTag: PropTypes.func,
  type: PropTypes.string.isRequired
};

export default Filters;
