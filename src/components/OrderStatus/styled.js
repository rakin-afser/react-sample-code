import styled from 'styled-components';

const color = {
  delivered: '#2cb767',
  inProgress: '#666',
  returned: '#ed484f'
};

export const Wrap = styled.span`
  display: inline-flex;
  align-items: center;
  font-weight: 500;
  color: ${({status}) => color[status]};
`;

export const IconWrapper = styled.span`
  margin-right: 6px;
  display: inline-flex;
`;
