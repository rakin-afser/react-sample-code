import React from 'react';
import {Wrap, IconWrapper} from './styled';

import Icon from 'components/Icon';
import {primaryColor} from 'constants/colors';

const statuses = {
  COMPLETED: {
    icon: 'checkCircle',
    iconColor: '#2ECC71',
    text: 'Delivered'
  },
  REFUNDED: {
    icon: 'closeCircle',
    iconColor: primaryColor,
    text: 'Returned'
  },
  PROCESSING: {
    text: 'In Progress'
  },
  CANCELLED: {
    icon: 'closeCircle',
    iconColor: primaryColor,
    text: 'Cancelled'
  }
};

const OrderStatus = ({status = ''}) => {
  return (
    <Wrap status={status}>
      {statuses[status]?.icon && (
        <IconWrapper>
          <Icon type={statuses[status]?.icon} svgStyle={{width: 17, height: 17, color: statuses[status]?.iconColor}} />
        </IconWrapper>
      )}
      {statuses[status]?.text}
    </Wrap>
  );
};

export default OrderStatus;
