import styled from 'styled-components/macro';

export const Card = styled.div`
  position: relative;
  width: 200px;
  height: auto;
  border: 1px solid #eeeeee;
  box-sizing: border-box;
  border-radius: 4px;
  margin: ${({margin}) => (margin ? `${margin}` : 0)};
`;

export const Poster = styled.div`
  width: 100%;
  position: relative;
  height: 200px;
  cursor: pointer;
`;

export const PosterPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Avatar = styled.div`
  width: 22px;
  height: 22px;
`;

export const About = styled.div`
  margin: 4px 0 0 8px;
`;

export const AvatarLogo = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 50%;
`;

export const Name = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 17px;
  display: flex;
  align-items: center;
  color: #000000;
`;

export const Time = styled.div`
  width: 100%;
  padding-top: 0;
  padding-left: 9px;
  padding-bottom: 4px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  text-align: left;
  font-size: 10px;
  line-height: 13px;
  color: #8f8f8f;
`;

export const PostName = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #000000;
  margin: 12px 0 10px;
  padding: 0 8px;
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const PostFeedback = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 8px 16px;
`;

export const Viewers = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
`;

export const ViewerImg = styled.img`
  width: 18px;
  height: 18px;
  margin-right: 1px;
  border-radius: 10px;
  &:last-child {
    margin-right: 0;
  }
`;

export const ViewersCount = styled.span`
  display: inline-block;
  margin-left: 4px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 17px;
  color: #4a90e2;
`;

export const Counts = styled.div`
  display: flex;
  align-self: center;
  justify-content: flex-end;
  align-items: baseline;
  flex: 2;
  font-family: Helvetica Neue;
  font-style: normal;
  font-size: 10px;
  line-height: 13px;
  color: #333333;
  mix-blend-mode: normal;
  border-radius: 3px;
`;

export const ViewersTotal = styled.span`
  font-weight: bold;
  display: inline-block;
  margin-right: 3px;
`;

export const LikesTotal = styled.span`
  font-weight: bold;
  display: inline-block;
  margin-right: 3px;
  margin-left: 8px;
`;
