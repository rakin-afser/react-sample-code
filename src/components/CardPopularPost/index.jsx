import React, {useState} from 'react';
import moment from 'moment';

import {
  Card,
  Avatar,
  About,
  AvatarLogo,
  Name,
  Time,
  Poster,
  PosterPic,
  PostName,
  PostFeedback,
  // Viewers,
  // ViewerImg,
  // ViewersCount,
  Counts,
  ViewersTotal,
  LikesTotal
} from './styled';

import {PosterWrapper, PosterOverlay, VideoWrapper} from 'components/Cards/Post/styled';
import Video from 'components/Cards/Video';
import Grid from 'components/Grid';
import userPlaceholder from 'images/placeholders/user.jpg';
import ProductsCounter from 'components/ProductsCounter';
// import useGlobal from 'store';
import Icon from 'components/Icon';
import productPlaceholder from 'images/placeholders/product.jpg';
import PostView from 'components/Modals/PostView';

const CardPopularPost = ({content = {}, margin}) => {
  // const [globalState, setGlobalState] = useGlobal();
  const [visible, setVisible] = useState(false);
  const {databaseId, author, title, date, relatedProducts, galleryImages, commentCount, totalLikes, viewed} = content;

  const posterMedia = galleryImages?.nodes?.[0];

  const onClick = () => {
    setVisible(true);
  };

  const showPoster = (media) => {
    switch (media?.mimeType?.split('/')?.[0]) {
      case 'video':
        return (
          <VideoWrapper>
            <Video borderRadius={0} play={false} onClick={onClick} src={media?.mediaItemUrl} />
          </VideoWrapper>
        );
      case 'image':
        return (
          <PosterWrapper onClick={onClick}>
            <PosterOverlay>
              <Icon type="lens" />
            </PosterOverlay>
            <PosterPic src={media?.mediaItemUrl} alt={media?.altText} />
          </PosterWrapper>
        );
      default:
        return (
          <PosterWrapper onClick={onClick}>
            <PosterOverlay>
              <Icon type="lens" />
            </PosterOverlay>
            <PosterPic src={productPlaceholder} alt="post" />
          </PosterWrapper>
        );
    }
  };

  return (
    <Card margin={margin}>
      <Grid aic height={29} padding="0 0 0 8px" margin="0 0 4px 0">
        <Avatar>
          <AvatarLogo src={author?.node?.avatar?.url || userPlaceholder} />
        </Avatar>
        <About>
          <Name>{author?.node?.name}</Name>
        </About>
      </Grid>
      <Time>{moment(date).format('DD MMM YYYY')}</Time>
      <Poster>
        <ProductsCounter top="4px" right="4px" bottom="unset" left="unset" count={relatedProducts?.nodes?.length} />
        {showPoster(posterMedia)}
      </Poster>
      <PostName>{title || 'Untitled'}</PostName>
      <PostFeedback>
        {/* <Viewers>
          <ViewerImg src={viewed?.viewer1} alt="avatar" />
          <ViewerImg src={viewed?.viewer2} alt="avatar" />
          <ViewersCount>+{viewed?.moreViews}</ViewersCount>
        </Viewers> */}
        <Counts>
          <ViewersTotal>{commentCount || '0'} </ViewersTotal> Comments
          <LikesTotal>{totalLikes || '0'} </LikesTotal> Likes
        </Counts>
      </PostFeedback>
      <PostView visible={visible} setVisible={setVisible} postId={databaseId} />
    </Card>
  );
};

export default CardPopularPost;
