import React from 'react';
import {Product, Img, Info, Txt, ItemPrice} from './styled';
import product from 'images/products/product17.jpg';

const ProdDetail = ({data}) => {
  return (
    <Product>
      <Img src={data?.product?.image?.sourceUrl} />
      <Info>
        <Txt>{data?.product?.name}</Txt>
        <ItemPrice>
          Item price: <b>{data?.product?.currencySymbol}</b>&nbsp;
          <span>{data?.total}</span>
        </ItemPrice>
      </Info>
    </Product>
  );
};

export default ProdDetail;
