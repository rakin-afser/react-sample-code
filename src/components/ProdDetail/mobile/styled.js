import styled from 'styled-components';

export const Product = styled.div`
  display: flex;
  padding: 16px 0;

  img {
    object-fit: cover;
  }
`;

export const Img = styled.img`
  border-radius: 4px;
  width: 90px;
  height: 90px;
  border: 1px solid #efefef;
`;

export const Info = styled.div`
  padding-left: 8px;
`;

export const Txt = styled.div`
  margin-bottom: 3px;

  &:last-child {
    margin-bottom: 0;
  }

  span {
    font-size: 14px;
    font-weight: 500;
    color: #000;
  }
`;

export const ItemPrice = styled.p`
  margin-bottom: 0;
  color: #404040;

  span {
    font-size: 12px;
    font-weight: 500;
    color: #000;
  }

  b {
    font-weight: 500;
    font-size: 10px;
    color: #000;
  }
`;
