import styled from 'styled-components';
import {mainFont} from 'constants/fonts';

export const Container = styled.div`
  border: 1px solid #e4e4e4;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.1);
  width: 100%;
  height: ${({height}) => (height ? `${height}` : 'auto')};
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  height: 60px;
  padding: 19px 0 19px 44px;
`;

export const Heading = styled.span`
  font-family: ${mainFont};
  letter-spacing: -0.024em;
  font-size: 18px;
  line-height: 140%;
  font-weight: 500;
  color: #000;
`;
