import React from 'react';
import {Container, Header, Heading} from './styled';

function AccountContent({heading, children, height, ...props}) {
  return (
    <Container height={height} {...props}>
      <Header>
        <Heading>{heading}</Heading>
      </Header>
      {children}
    </Container>
  );
}

export default AccountContent;
