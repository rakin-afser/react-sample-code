import {Link} from 'react-router-dom';
import styled from 'styled-components/macro';

export const Container = styled.div`
  margin-top: auto;
  width: 100%;
  display: flex;
  padding: 0 16px 12px;
  z-index: 11;
`;

export const Wrap = styled.div`
  width: 100%;
  background: #ffffff;

  .attributes {
    display: flex;
    align-items: center;
    padding: 12px 10.5px 0;
  }

  .attribute {
    display: inline-flex;
    padding: 5px 8px;
    border: 0.5px solid #e8e1e1;
    border-radius: 22px;
    margin: 0;
    line-height: 1;
    white-space: nowrap;
    font-family: 'Helvetica Neue';
    font-style: normal;
    font-weight: normal;
    font-size: 12px;

    &__wrapper {
      margin: 0 auto;
      padding: 0 5.5px;

      &:first-child {
        margin-left: 0 !important;
      }

      &:last-child {
        margin-right: 0;
      }
    }

    &--rating {
      i:not(:last-child) {
        margin-right: 2px;
      }
    }

    &--delivery {
      span {
        padding-left: 4px;
        padding-top: 1px;
      }
    }
  }
`;

export const MainImageBlock = styled.div`
  width: calc(100% - 32px);
  margin: 12px 16px 0;
  height: 19.4vh;
  border-radius: 16px;
  background-image: url('${({image}) => image}');
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  position: relative;
  display: flex;
  flex-wrap: wrap;

  &:before {
    content: '';
    position: absolute;
    top: 23px;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 1;
    background: linear-gradient(180deg, rgba(255, 255, 255, 0) 7.08%, rgba(61, 59, 59, 0.356546) 54.2%, #141414 124.06%);
    border-radius: 0px 0px 16px 16px;
  }
`;

export const InfoBlock = styled.div`
  width: 100%;
  margin-top: 7px;
`;

export const FlexBox = styled.div`
  display: flex;
  align-items: center;
`;

export const Avatar = styled.img`
  border-radius: 50%;
  width: 50px;
  height: 50px;
  margin-right: 10px;
  margin-top: 0;
  border: 1px solid #ffffff;
  background-color: #fff;
`;

export const InfoBlockRight = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const Title = styled.h3`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 700;
  font-size: 18px;
  line-height: 21px;
  color: #000;
  display: block;
  margin-bottom: 4px;
`;

export const RatingWrap = styled.div`
  display: flex;
  justify-content: center;
`;

export const LikesNumber = styled.div`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #7a7a7a;
  margin-left: 9px;
`;

export const FollowInfo = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const Follow = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-start;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  color: #000;
  & span {
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: 600;
    font-size: 12px;
    line-height: 17.71px;
    letter-spacing: 0.019em;
    color: #000;
  }
`;

export const Followers = styled.div`
  display: flex;
  align-items: center;
`;

export const FollowersNumber = styled.span`
  margin-right: 8px;
`;

export const Following = styled.div`
  display: flex;
  align-items: center;
  &:first-child {
    margin-right: 12px;
  }
`;

export const FollowingNumber = styled.span`
  margin-right: 6px;
`;

export const Action = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 100%;
  margin-left: auto;
`;

export const ButtonWrap = styled.div`
  margin-left: auto;

  button {
    border-color: #ed484f;
    background: transparent;
    color: #ed484f;
    padding-top: 2px;
  }

  &:first-child {
    margin-right: 18px;
  }
`;

export const ExpandedTextWrap = styled.div`
  & div:first-child {
    margin-top: 12px;
    padding: 0;
    max-width: 767px;
    text-align: left;
  }
`;

export const Tags = styled.div`
  margin: 0 16px;
  display: flex;
  white-space: nowrap;
  text-overflow: ellipsis;
  position: relative;
  overflow: hidden;
`;

export const Tag = styled(Link)`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #4a90e2;
  line-height: 1.4;
  margin-bottom: 0;
  display: inline-block;
  margin-right: 7px;
  text-overflow: ellipsis;
`;
