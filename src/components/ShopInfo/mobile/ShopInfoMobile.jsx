import React from 'react';

import {
  Wrap,
  MainImageBlock,
  InfoBlock,
  FlexBox,
  Container,
  Avatar,
  Title,
  LikesNumber,
  RatingWrap,
  InfoBlockRight,
  FollowInfo,
  Follow,
  Followers,
  FollowersNumber,
  Following,
  FollowingNumber,
  Tags,
  Tag,
  Action,
  ButtonWrap
} from './styled';
import Settings from 'components/Buttons';
import userPlaceholder from 'images/placeholders/user.jpg';
import storePlaceholderImg from 'images/placeholder.png';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';
import Btn from 'components/Btn';
import {Rate} from 'antd';
import Icon from 'components/Icon';

const ShopInfoMobile = ({isOwner, setRating, onSettingsClick, data = {}}) => {
  const {id, followed, rating} = data?.store || {};
  const [followUnfollowStore, {loading}] = useFollowUnFollowStoreMutation({followed});

  return (
    <Wrap>
      <MainImageBlock image={data?.store?.banner || storePlaceholderImg}></MainImageBlock>

      <Container>
        <InfoBlock>
          <FlexBox>
            <Avatar src={data?.store?.gravatar || userPlaceholder} />
            <InfoBlockRight>
              <Title>{data?.store?.name}</Title>
              <FollowInfo>
                <Follow>
                  <Following>
                    <FollowingNumber>{data?.store?.totalFollowing}</FollowingNumber>
                    Following
                  </Following>
                  <Followers>
                    <FollowersNumber>{data?.store?.totalFollowers}</FollowersNumber>
                    Followers
                  </Followers>
                </Follow>
              </FollowInfo>
            </InfoBlockRight>
            {isOwner ? (
              <></>
              // <Action>
              //   <Settings type="settings" props={{onClick: onSettingsClick}} />
              // </Action>
            ) : (
              <ButtonWrap>
                <Btn
                  kind={followed ? 'following' : 'follow'}
                  loading={loading}
                  onClick={() => followUnfollowStore({variables: {input: {id: parseInt(id)}}})}
                />
              </ButtonWrap>
            )}
          </FlexBox>
        </InfoBlock>
      </Container>

      {data?.store?.storeTags ? (
        <Tags>
          {data?.store?.storeTags?.map((tag) => (
            <Tag to={`/search/stores?search=%23${tag}`} key={tag}>
              #{tag}
            </Tag>
          ))}
        </Tags>
      ) : null}

      <div className="attributes">
        <div className="attribute__wrapper">
          <div className="attribute attribute--rating">
            <Icon type="star" fill={rating >= 1 ? '#FFC131' : '#ddd'} width={9} height={9} />
            <Icon type="star" fill={rating >= 2 ? '#FFC131' : '#ddd'} width={9} height={9} />
            <Icon type="star" fill={rating >= 3 ? '#FFC131' : '#ddd'} width={9} height={9} />
            <Icon type="star" fill={rating >= 4 ? '#FFC131' : '#ddd'} width={9} height={9} />
            <Icon type="star" fill={rating >= 5 ? '#FFC131' : '#ddd'} width={9} height={9} />
          </div>
        </div>
        
        <div className="attribute__wrapper">
          <div className="attribute attribute--accepts-returns">
            Accepts Returns
          </div>
        </div>

        <div className="attribute__wrapper">
          <div className="attribute attribute--delivery">
            <strong>FREE </strong> <span>Local Delivery</span>
          </div>
        </div>
      </div>
    </Wrap>
  );
};

export default ShopInfoMobile;
