import React from 'react';
import {useHistory} from 'react-router-dom';
import Star from 'assets/ProductPage/Star';
import {Tag, SocialLink, SocialImage} from './desktop/styled.js';
import facebook from './img/facebook.svg';
import instagram from './img/instagram.svg';
import twitter from './img/twitter.svg';
import linked from './img/linked.svg';
import ShopInfoDesktop from './desktop/ShopInfoDesktop';
import ShopInfoMobile from './mobile/ShopInfoMobile';

const ShopInfo = ({isOwner, isMobile, data}) => {
  const history = useHistory();
  const {social, storeTags} = data?.store || {};

  const socialLinks = [
    {name: facebook, link: social?.fb, icon: facebook},
    {name: instagram, link: social?.instagram, icon: instagram},
    {name: twitter, link: social?.twitter, icon: twitter},
    {name: linked, link: social?.linkedin, icon: linked}
  ];

  const tags = [{value: '#Cream'}, {value: '#Canvas'}, {value: '#Big'}, {value: '#tags'}];

  const redirectToSettings = () => {
    history.push('/account-settings');
  };

  const descriptionText =
    'Trendy overview for last season, tenden collaboration, FW The magazine covers international, national and local fashion and beauty trends and news.';

  const getTags = () => {
    return storeTags.map((tag, i) => (
      <Tag to={`/search/stores?search=%23${tag}`} key={i}>
        #{tag}
      </Tag>
    ));
  };

  const getSocialLinks = () => {
    return socialLinks.map((socialLink, i) => {
      return (
        <SocialLink key={i} href={socialLink.link}>
          <SocialImage src={socialLink.icon} alt={socialLink.name} />
        </SocialLink>
      );
    });
  };

  const setRating = (value) => {
    const Rating = [];
    for (let i = 0; i < 5; i++) {
      Rating.push(<Star key={i} />);
    }
    if (value < 5) {
      for (let i = 0; i < 5 - value; i++) {
        Rating.push(<Star key={i} color="#efefef" />);
      }
    }
    return Rating;
  };

  return isMobile ? (
    <ShopInfoMobile setRating={setRating} isOwner={isOwner} onSettingsClick={redirectToSettings} data={data} />
  ) : (
    <ShopInfoDesktop
      data={data}
      setRating={setRating}
      descriptionText={descriptionText}
      getTags={getTags}
      getSocialLinks={getSocialLinks}
      isOwner={isOwner}
      onSettingsClick={redirectToSettings}
    />
  );
};

export default ShopInfo;
