import React from 'react';

import Avatar from 'components/Avatar';
import {Row, Column, Name} from 'components/UserInfo/styled';
import {Text} from 'components/ShopInfo/desktop/styled';
import FollowButton from 'components/FollowButton';
import StarRating from 'components/StarRating';

const ShopInfo = ({src, type, location, followers, shopName, isOnline, isFollowed}) => {
  return (
    <Row>
      <Avatar style={{cursor: 'pointer'}} width={60} height={60} isOnline={isOnline} img={src} />
      <Column padding="0 17px">
        <Text>
          {type} {location ? `| ${location}` : ''}
        </Text>
        <Name margin={'4px 0'} size={16}>
          {shopName}
        </Name>
        <Row>
          <StarRating />
          <Text margin={'0 5px'}>(335)</Text>
          <Text color="#464646">{followers} followers</Text>
        </Row>
      </Column>
      <FollowButton isFollowed={isFollowed} />
    </Row>
  );
};

export default ShopInfo;
