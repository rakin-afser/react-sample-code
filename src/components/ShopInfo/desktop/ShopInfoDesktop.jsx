import React from 'react';
import featuredImage from '../img/kylie-skin.jpg';
import avatarImage from '../img/beauty-avatar.png';
import storePlaceholderImg from 'images/placeholder.png';
import mapPin from '../img/mappin.svg';
import Icons from 'components/Icon';
import {
  Wrap,
  ImageBlock,
  ImagePic,
  Title,
  RatingWrap,
  LikesNumber,
  Location,
  LocationItem,
  LocationPic,
  Follow,
  Followers,
  FollowersNumber,
  Following,
  FollowingNumber,
  Action,
  ButtonWrap,
  Tags,
  Social,
  AvatarPic,
  GoToDashboard
} from './styled.js';
import Button from 'components/Buttons';
import ExpandedText from 'components/ExpandedText';
import userPlaceholder from 'images/placeholders/user.jpg';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import Btn from 'components/Btn';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';
import {Rate} from 'antd';

const ShopInfoDesktop = ({data, isOwner, setRating, descriptionText, getTags, getSocialLinks, onSettingsClick}) => {
  const [, setGlobalStore] = useGlobal();
  const [user] = useUser();
  const isSeller = user?.capabilities?.includes('seller');
  const {id, followed} = data?.store || {};
  const [followUnfollowStore, {loading}] = useFollowUnFollowStoreMutation({followed});

  const dataForMessenger = {
    databaseId: data.store.id,
    username: data.store.name,
    photoUrl: data.store.gravatar
  };

  const onShowMessenger = () => {
    setGlobalStore.setMessenger({
      visible: true,
      minify: true,
      data: isSeller ? null : dataForMessenger
    });
  };

  return (
    <Wrap>
      <ImageBlock>
        <ImagePic src={data?.store?.banner || storePlaceholderImg} />
        <AvatarPic src={data?.store?.gravatar || userPlaceholder} />
      </ImageBlock>
      <Title>{data?.store?.name}</Title>
      <RatingWrap>
        <Rate disabled value={5} character={<Icons fill="currentColor" width={15} height={15} type="star" />} />
      </RatingWrap>
      <Location>
        <LocationPic src={mapPin} />
        <LocationItem>
          {data?.store?.address?.city}, {data?.store?.address?.countryName}
        </LocationItem>
      </Location>
      <Follow>
        <Followers>
          <FollowersNumber>{data?.store?.totalFollowers}</FollowersNumber>
          Followers
        </Followers>
        <Following>
          <FollowingNumber>{data?.store?.totalFollowing}</FollowingNumber>
          Following
        </Following>
      </Follow>
      {isOwner ? (
        <Action>
          <Button type="settings" props={{onClick: onSettingsClick}} />
          <GoToDashboard href="https://seller.testSample.net/dashboard/">
            <Icons type="tile" color="#333" height={12} width={12} />
            Dashboard
          </GoToDashboard>
        </Action>
      ) : (
        <Action>
          <ButtonWrap>
            <Btn
              kind={followed ? 'following' : 'follow'}
              loading={loading}
              onClick={() => followUnfollowStore({variables: {input: {id: parseInt(id)}}})}
            />
          </ButtonWrap>
          <ButtonWrap>
            <Button onClick={onShowMessenger} type="sendMessage" />
          </ButtonWrap>
        </Action>
      )}
      <ExpandedText text={data?.store?.vendorBiography || 'no biography yet'} divider={57} />
      <Tags>{getTags()}</Tags>
      <Social>{getSocialLinks()}</Social>
    </Wrap>
  );
};

export default ShopInfoDesktop;
