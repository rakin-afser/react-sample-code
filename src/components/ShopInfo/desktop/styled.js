import {Link} from 'react-router-dom';
import styled from 'styled-components/macro';

export const Wrap = styled.div`
  width: 100%;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  padding-bottom: 21px;
`;

export const ImageBlock = styled.div`
  width: 100%;
  height: 144px;
  position: relative;
`;

export const ImagePic = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const AvatarPic = styled.img`
  width: 72px;
  height: 72px;
  border-radius: 50%;
  object-fit: cover;
  bottom: 0;
  left: 50%;
  position: absolute;
  transform: translate(-50%, 50%);
  background: #f8f8f8;
  border: 1px solid #ddd;
`;

export const StoreImage = styled.img`
  position: absolute;
  bottom: 0;
  left: 24px;
  transform: translate(0, 50%);
  width: 72px;
  background-color: #fff;
  border-radius: 50%;
`;

export const Title = styled.h3`
  text-align: center;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 124%;
  margin-top: 42px;
  letter-spacing: 0.019em;
  color: #464646;
`;

export const Rating = styled.div`
  display: flex;
  align-items: center;

  & svg {
    margin-right: 4px;

    &:last-child {
      margin-right: 0;
    }
  }
`;

export const RatingWrap = styled.div`
  display: flex;
  justify-content: center;
`;

export const LikesNumber = styled.div`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #7a7a7a;
  margin-left: 7px;
`;

export const Location = styled.div`
  margin-top: 27px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const LocationPic = styled.img`
  margin-right: 7px;
`;

export const LocationItem = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #666666;
`;

export const Follow = styled.div`
  margin-top: 17px;
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #545454;

  & span {
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: 600;
    font-size: 18px;
    line-height: 124%;
    text-align: right;
    letter-spacing: 0.019em;
    color: #000000;
  }
`;

export const Followers = styled.div`
  display: flex;
  align-items: center;
  margin: 0 9px;
`;

export const FollowersNumber = styled.span`
  margin-right: 7px;
`;

export const Following = styled.div`
  display: flex;
  align-items: center;
  margin: 0 9px;
`;

export const FollowingNumber = styled.span`
  margin-right: 7px;
`;

export const Action = styled.div`
  margin-top: 15px;
  display: flex;
  justify-content: center;
  width: 100%;
`;

export const ButtonWrap = styled.div`
  margin: 0 11px;
`;

export const Tags = styled.div`
  margin-top: 16px;
  width: 100%;
  padding: 0 35px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  text-align: center;
  color: #4a90e2;
`;

export const Tag = styled(Link)`
  margin-right: 6px;
  cursor: pointer;

  &:last-child {
    margin-right: 0;
  }
`;

export const Social = styled.div`
  margin-top: 29px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const SocialLink = styled.a``;

export const SocialImage = styled.img`
  fill: #bec0c3;
  margin: 0 3px;
`;

export const SettingsButtonWrapper = styled.div`
  text-align: center;
  margin-top: 17px;
  margin-bottom: 7px;
`;

export const Text = styled.span`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: ${({color}) => color || '#8F8F8F'};
  margin: ${({margin}) => (margin ? margin : '0')};
`;

export const GoToDashboard = styled.a`
  padding: 0 6px;
  margin-left: 10px;
  display: flex;
  align-items: center;
  min-width: 100px;
  height: 30px;
  font-size: 14px;
  line-height: 140%;
  color: #666666;
  border: 1px solid #8f8f8f;
  border-radius: 24px;

  & svg {
    margin-right: 4px;
  }

  &:hover {
    background: #8f8f8f;
    color: #ffffff;

    & svg path {
      fill: #ffffff;
    }
  }
`;
