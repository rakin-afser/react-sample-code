import axios from 'axios';

const axiosClient = axios.create({
  baseURL: `${process.env.REACT_APP_testSample_DOMAIN}wp-json/wp/v2/media`,
  responseType: 'json'
});

axiosClient.interceptors.request.use((config) => {
  const token = localStorage.getItem('token');
  const newConfig = {
    ...config,
    headers: {
      ...config.headers,
      authorization: `Bearer ${token}`
    }
  };
  return newConfig;
});

export default axiosClient;
