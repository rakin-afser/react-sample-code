import Pusher from 'pusher-js';

const init = () => {
  // Pusher.logToConsole = true;

  const pusher = new Pusher('', {
    cluster: 'eu'
  });
  window.pusherJs = pusher;
};

const bind = (channel, event, callback) => {
  const pusherClient = window.pusherJs;
  const pusherChannel = pusherClient.channels.find(channel) || pusherClient.subscribe(channel);
  pusherChannel.bind(event, callback);
};

const unbind = (channel, event, callback) => {
  const pusherClient = window.pusherJs;
  const pusherChannel = pusherClient.channels.find(channel);

  if (pusherChannel) {
    pusherChannel.unbind(event, callback);
  }
};

export default {
  init,
  bind,
  unbind
};
