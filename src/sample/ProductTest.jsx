import React from 'react';
import { useQuery, gql } from '@apollo/client';

const GET_CURRENCY = gql`
  {
    currency {
      available_currency_codes
    }
  }
`;

const ProductTest = () => {
  // const {loading, error, data} = useQuery(GET_CURRENCY);
  const { loading, error, data } = useQuery(GET_CURRENCY, {
    context: { clientType: 'magento' }
  });
  console.log('ProductTest=?', loading, error, data);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <select name="dog">
      {data.currency.available_currency_codes.map((currency) => (
        <option key={currency} value={currency}>
          {currency}
        </option>
      ))}
    </select>
  );
};

export default ProductTest;
