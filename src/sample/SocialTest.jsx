import React from 'react';
import { useQuery, gql } from '@apollo/client';

const GET_COUNTRY = gql`
  {
    Countries {
      CountryName
    }
  }
`;

const SocialTest = () => {
  const { loading, error, data } = useQuery(GET_COUNTRY);
  // console.log('SocialTest=?', loading, error, data);

  if (loading) return 'Loading...';
  if (error) return `Error! ${error.message}`;

  return (
    <select name="dog">
      {data.Countries.map((country) => (
        <option key={country.CountryName} value={country.CountryName}>
          {country.CountryName}
        </option>
      ))}
    </select>
  );
};

export default SocialTest;
