import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import detector from 'i18next-browser-languagedetector';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

import TranslationEn from './dictionary/locales/en/TranslationEn.json';
import TranslationAr from './dictionary/locales/ar/TranslationAr.json';

import sizeGuideEn from './dictionary/locales/en/sizeGuideEn.json';
import sizeGuideAr from './dictionary/locales/ar/sizeGuideAr.json';

import sellerLandingEn from './dictionary/locales/en/sellerLandingEn.json';
import sellerLandingAr from './dictionary/locales/ar/sellerLandingAr.json';

i18n
  .use(initReactI18next)
  .use(Backend)
  .use(LanguageDetector)
  .use(detector)
  .use(initReactI18next)
  .init({
    resources: {
      English: {
        translation: TranslationEn,
        sizeGuide: sizeGuideEn,
        sellerLanding: sellerLandingEn
      },
      Arabic: {
        translation: TranslationAr,
        sizeGuide: sizeGuideAr,
        sellerLanding: sellerLandingAr
      }
    },
    lng: 'English',
    fallbackLng: 'English',
    interpolation: {
      escapeValue: false
    }
  });

export default i18n;
