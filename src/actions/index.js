export const setArabic = (store, value) => {
  store.setState({isArabic: value});
};

export const setActionsPopup = (store, value, data) => {
  store.setState({actionsPopup: value, actionsPopupData: data});
};

export const setProductFiltersPopup = (store, value) => {
  const productFiltersPopup = value;
  store.setState({productFiltersPopup});
};

export const setHeaderBackButtonLink = (store, value) => {
  const headerBackButtonLink = value;
  store.setState({headerBackButtonLink});
};

export const setIsModalOpened = (store, value) => {
  const isModalOpened = value;

  store.setState({isModalOpened});
};

export const setActionsCartProduct = (store, value) => {
  const actionsCartProduct = value;

  store.setState({actionsCartProduct});
};

export const setQuickView = (store, product) => {
  if (product) {
    store.setState({quickView: product});
  } else {
    store.setState({quickView: null});
  }
};

export const setUser = (store, data) => {
  const user = data;

  store.setState({user});
};

// for test need to delete
export const setTestUser = (store, value) => {
  store.setState({testUser: value});
};
// for test need to delete

export const setLeaveFeedback = (store, value) => {
  store.setState({leaveFeedback: value});
};

export const setOrderDetails = (store, value) => {
  store.setState({orderDetails: value});
};

export const setActionAlert = (store, value) => {
  store.setState({actionAlert: value});
};

export const setReportPopup = (store, value) => {
  store.setState({reportPopup: {...store.state.reportPopup, ...value}});
};

export const setOptionsPopup = (store, value) => {
  store.setState({optionsPopup: {...store.state.optionsPopup, ...value}});
};

export const setIsRequireAuth = (store, value) => {
  store.setState({isRequireAuth: value});
};

export const setShowVariationsPopup = (store, value) => {
  store.setState({showVariationsPopup: value});
};

export const setShowSizeGuide = (store, value) => {
  store.setState({showSizeGuide: value});
};

export const setShowProductAdded = (store, value) => {
  store.setState({showProductAdded: value});
};

export const setIsMuted = (store, value) => {
  store.setState({isMuted: value});
};

export const setIsAutoplay = (store, value) => {
  store.setState({isAutoplay: value});
};

export const setReturnRequest = (store, value) => {
  store.setState({returnRequest: value});
};

export const setSellerContact = (store, value) => {
  store.setState({sellerContact: value});
};

export const setFollowersModal = (store, value) => {
  store.setState({followersModal: value});
};

export const setAuthPopup = (store, value) => {
  const authPopup = value;
  store.setState({authPopup});
};

export const setReportModal = (store, value, props) => {
  store.setState({reportModal: value, reportModalProps: props});
};

export const setPostActions = (store, value, props) => {
  store.setState({postActionsModal: value, postActionsProps: props});
};

export const setPostShare = (store, value, props) => {
  store.setState({postShareModal: value, postShareProps: props});
};

export const setFilterModal = (store, value, props) => {
  store.setState({filterModal: value, filterModalProps: props});
};

export const setInfluencer = (store, value) => {
  store.setState({influencer: value});
};

export const setMessenger = (store, value) => {
  store.setState({messenger: value});
};
export const setSignUpInfo = (store, value) => {
  store.setState({signUpInfo: value});
};

export const setPlayInCardVideo = (store, value) => {
  store.setState({playInCardVideo: value});
};

export const setFeedback = (store, value) => {
  store.setState({feedback: value});
};

export const setProductPopup = (store, value, props) => {
  store.setState({productPopup: value, productPopupProps: props});
};
