import {gql} from '@apollo/client';
import {
  CartFragment,
  ReviewsFragment,
  testSampleSubcartsFragment,
  ShippingAddressFragment,
  PostFragment,
  CommentFragment,
  FlashPostFragment,
  MediumMediaItemFragment,
  CommentDesktopFragment
} from 'fragments';

export const CATEGORIES = gql`
  query Categories {
    productCategories(where: {parent: 0, hideEmpty: true}, first: 100) {
      nodes {
        id
        totalFollowers
        name
        slug
        image {
          id
          sourceUrl(size: MEDIUM)
        }
        count
        children {
          nodes {
            id
            totalFollowers
            name
            slug
            count
            image {
              id
              sourceUrl(size: MEDIUM)
            }
            children {
              nodes {
                id
                totalFollowers
                name
                slug
                count
                image {
                  id
                  sourceUrl(size: MEDIUM)
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const POPULAR_CATEGORIES = gql`
  query Categories($first: Int) {
    productCategories(where: {parent: 0, hideEmpty: true}, first: $first) {
      nodes {
        id
        databaseId
        isFollowed
        totalFollowers
        name
        slug
        image {
          id
          sourceUrl(size: MEDIUM)
        }
      }
    }
  }
`;

export const NAV_CATEGORIES = gql`
  query NavCategories {
    productCategories(where: {parent: 0, hideEmpty: true}, first: 30) {
      nodes {
        id
        name
        slug
        children(where: {hideEmpty: true}) {
          nodes {
            id
            name
            slug
            children(where: {hideEmpty: true}) {
              nodes {
                id
                name
                slug
              }
            }
          }
        }
      }
    }
  }
`;

export const VariableProductFragment = gql`
  fragment VariableProductFragment on VariableProduct {
    id
    currencySymbol
    databaseId
    name
    price
    salePrice
    regularPrice
    shippingType
    slug
    isLiked
    totalLikes
    product_video_url
    productVideoPoster: product_video_poster
    variations {
      nodes {
        id
        price
        salePrice
        regularPrice
      }
    }
    productCategories {
      nodes {
        id
        slug
        name
        parentDatabaseId
      }
    }
    seller {
      name
      id
      gravatar
      followed
    }
    image {
      id
      sourceUrl(size: LARGE)
      altText
    }
  }
`;

export const SimpleProductFragment = gql`
  fragment SimpleProductFragment on SimpleProduct {
    id
    currencySymbol
    databaseId
    name
    price
    salePrice
    regularPrice
    shippingType
    slug
    shortDescription
    description
    isLiked
    totalLikes
    onSale
    product_video_url
    productCategories {
      nodes {
        id
        slug
        name
        parentDatabaseId
      }
    }
    seller {
      name
      id
      gravatar
      followed
    }
    image {
      id
      sourceUrl(size: LARGE)
      altText
    }
  }
`;

export const PRODUCTS = gql`
  query Products(
    $categorySlug: String
    $categoryIn: [String]
    $search: String
    $type: ProductTypesEnum
    $cursor: String
    $first: Int
    $minPrice: Float
    $maxPrice: Float
    $onSale: Boolean
    $vendorId: Int
    $orderBy: [ProductsOrderbyInput]
    $tagIn: [String]
    $featured: Boolean
    $filterBrand: [String]
    $filterColor: [String]
    $filterSize: [String]
    $shippingType: String
    $attributeFilters: [ProductTaxonomyFilterInput]
    $isDealProduct: Boolean
    $shopcoins_available: Boolean
    $recentlyViewed: Boolean
  ) {
    products(
      where: {
        is_a_deal_product: $isDealProduct
        vendor: $vendorId
        category: $categorySlug
        search: $search
        type: $type
        minPrice: $minPrice
        maxPrice: $maxPrice
        onSale: $onSale
        orderby: $orderBy
        tagIn: $tagIn
        featured: $featured
        categoryIn: $categoryIn
        brands: $filterBrand
        filterColor: $filterColor
        filterSize: $filterSize
        shippingType: $shippingType
        status: "publish"
        shopcoins_available: $shopcoins_available
        taxonomyFilter: {and: $attributeFilters}
        recently_viewed: $recentlyViewed
      }
      first: $first
      after: $cursor
    ) {
      nodes {
        id
        currencySymbol
        databaseId
        shippingType
        slug
        name
        description
        type
        averageRating
        isLiked
        totalLikes
        midCoins {
          id
          qty
        }
        reviewCount
        product_video_url
        productVideoPoster: product_video_poster
        seller {
          id
          name
          storeUrl
          followed
          rating
          location
          gravatar
          totalFollowers
        }
        image {
          id
          sourceUrl(size: LARGE)
          altText
          mimeType
        }
        galleryImages {
          nodes {
            id
            sourceUrl(size: LARGE)
            altText
            mimeType
          }
        }
        productTags {
          nodes {
            id
            name
          }
        }
        ... on SimpleProduct {
          id
          currencySymbol
          price
          regularPrice
          salePrice
          commentCount
          product_video_url
        }
        ... on VariableProduct {
          id
          currencySymbol
          price
          regularPrice
          salePrice
          commentCount
          product_video_url
          variations {
            nodes {
              id
              databaseId
              price
              salePrice
              regularPrice
              thumbnail: image {
                id
                sourceUrl(size: THUMBNAIL)
                altText
              }
              image {
                id
                sourceUrl(size: LARGE)
                altText
              }
              attributes {
                nodes {
                  label
                  name
                  value
                  attributeId
                  id
                }
              }
            }
          }
        }
        productCategories {
          nodes {
            id
            slug
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
        hasPreviousPage
        startCursor
        total
      }
    }
  }
`;

export const USERS_SEARCH = gql`
  query UsersSearch($first: Int, $after: String, $where: RootQueryToUserConnectionWhereArgs) {
    users(first: $first, after: $after, where: $where) {
      nodes {
        id
        databaseId
        slug
        name
        isFollowed
        totalFollowers
        username
        avatar {
          url
        }
      }
      pageInfo {
        endCursor
        hasNextPage
        total
      }
    }
  }
`;

export const PRODUCTS_SEARCH = gql`
  query ProductsSearch($where: RootQueryToProductConnectionWhereArgs, $first: Int, $after: String) {
    products(where: $where, first: $first, after: $after) {
      nodes {
        id
        databaseId
        slug
        name
        product_video_url
        isLiked
        totalLikes
        seller {
          id
          name
          followed
          gravatar
          totalFollowers
        }
        image {
          id
          sourceUrl(size: LARGE)
          altText
          mimeType
        }
        galleryImages {
          nodes {
            id
            sourceUrl(size: LARGE)
            altText
            mimeType
          }
        }
        productTags {
          nodes {
            id
            name
          }
        }
        ... on SimpleProduct {
          price
          salePrice
          regularPrice
        }

        ... on VariableProduct {
          price
          regularPrice
          salePrice
          variations {
            nodes {
              id
              databaseId
              price
              salePrice
              regularPrice
              image {
                id
                sourceUrl(size: LARGE)
                altText
              }
              attributes {
                nodes {
                  label
                  name
                  value
                  attributeId
                  id
                }
              }
            }
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
        total
      }
    }
  }
`;

export const PRODUCTS_FEED_FEATURED = gql`
  query ProductsFeedFeatured($where: RootQueryToProductConnectionWhereArgs, $first: Int) {
    products(where: $where, first: $first) {
      nodes {
        id
        name
        slug
        image {
          id
          sourceUrl(size: MEDIUM)
          altText
        }
        shippingType
        ... on SimpleProduct {
          salePrice
          regularPrice
        }
        ... on VariableProduct {
          salePrice
          regularPrice
        }
      }
    }
  }
`;

export const SPECIFIC_PRODUCTS = gql`
  query SpecificProducts($first: Int, $where: RootQueryToProductConnectionWhereArgs) {
    products(where: $where, first: $first) {
      nodes {
        id
        databaseId
        slug
        name
        isLiked
        totalLikes
        shippingType
        product_video_url
        productVideoPoster: product_video_poster
        image {
          id
          sourceUrl(size: MEDIUM)
          altText
          mimeType
        }
        ... on SimpleProduct {
          id
          price
          regularPrice
          salePrice
          product_video_url
        }
        ... on VariableProduct {
          id
          product_video_url
          variations {
            nodes {
              id
              price
              salePrice
              regularPrice
            }
          }
        }
      }
      pageInfo {
        total
      }
    }
  }
`;

export const USER = gql`
  query User($id: ID!) {
    user(id: $id, idType: DATABASE_ID) {
      id
      databaseId
      nickname
      name
      capabilities
      description
      username
      email
      url
      timezone
      locale
      language
      currency
      date_of_birth
      userId
      profile_picture
      gender
      firstName
      lastName
      education
      url
      company
      job_title
      country
      noti_publish_posts
      noti_post_gets_like
      noti_post_gets_comment
      noti_someone_reacts_to_comment
      noti_list_followed
      noti_list_gets_a_like
      noti_follows_me
      noti_send_message
      noti_reward_earns
      noti_reward_coins_change
      noti_received_message
      noti_joins_by_my_link
      noti_upcoming_birthdays_of_my_friends
      availableMidcoins
      availableShopcoins
      roles {
        nodes {
          id
          name
        }
      }
      avatar {
        url
        scheme
        default
        foundAvatar
        forceDefault
        extraAttr
        rating
      }
    }
  }
`;

export const USER_PROFILE = gql`
  query UserProfile($id: ID!) {
    user(id: $id, idType: DATABASE_ID) {
      id
      databaseId
      username
      profile_picture
      isFollowed
      totalFollowers
      totalFollowing
      description
      firstName
      lastName
      email
      job_title
      url
      locale
      gender
      avatar {
        url
      }
    }
  }
`;
export const CART_MINI = gql`
  ${MediumMediaItemFragment}
  query Cart {
    cart {
      contents {
        nodes {
          total
          key
          quantity
          variation {
            node {
              id
              databaseId
              regularPrice
              salePrice
              image {
                ...MediumMediaItemFragment
              }
            }
            attributes {
              id
              label
              value
            }
          }
          product {
            node {
              id
              databaseId
              name
              slug
              ... on SimpleProduct {
                regularPrice
                salePrice
              }
              ... on VariableProduct {
                regularPrice
                salePrice
              }
              image {
                id
                sourceUrl(size: LARGE)
                altText
              }
              databaseId
            }
          }
          midCoins
        }
      }
      subcarts {
        subcarts {
          shipping {
            chosenMethodId
            availableMethods {
              cost
              id
              label
              taxes
            }
          }
        }
      }
    }
  }
`;

export const CART = gql`
  ${CartFragment}
  ${testSampleSubcartsFragment}
  query Cart {
    cart {
      totalMidCoins
      totalShopCoins
      ...CartFragment
      subcarts {
        ...testSampleSubcartsFragment
      }
    }
  }
`;

export const CART_PRODUCTS = gql`
  query CartProducts {
    cart {
      totalQuantity
      contents {
        nodes {
          key
          product {
            node {
              id
              slug
            }
          }
        }
      }
      subcarts {
        subcarts {
          shipping {
            chosenMethodId
            availableMethods {
              cost
              id
              label
              taxes
            }
          }
        }
      }
    }
  }
`;

export const PRODUCTS_BY_CATEGORY = gql`
  ${VariableProductFragment}
  ${SimpleProductFragment}
  query ProductsByCategory($categoryIn: [String], $exclude: [Int]) {
    products(first: 7, where: {categoryIn: $categoryIn, exclude: $exclude}) {
      edges {
        node {
          ...VariableProductFragment
          ...SimpleProductFragment
        }
      }
    }
  }
`;

export const POSTS = gql`
  ${PostFragment}
  query Posts(
    $authorName: String
    $authorId: Int
    $first: Int
    $after: String
    $search: String
    $hashtags: [String]
    $orderby: [PostObjectsConnectionOrderbyInput]
    $isFeedbackPost: Boolean
    $notIn: [ID]
    $postGeo: PostGeoEnum
    $isFollowing: Boolean
  ) {
    posts(
      where: {
        authorName: $authorName
        author: $authorId
        search: $search
        tagSlugAnd: $hashtags
        orderby: $orderby
        isFeedbackPost: $isFeedbackPost
        notIn: $notIn
        postGeo: $postGeo
        isFollowing: $isFollowing
      }
      first: $first
      after: $after
    ) {
      nodes {
        ...PostFragment
      }
      pageInfo {
        hasNextPage
        endCursor
        hasPreviousPage
        startCursor
        total
      }
    }
  }
`;

export const POSTS_SEARCH = gql`
  query PostsSearch($first: Int, $after: String, $where: RootQueryToPostConnectionWhereArgs) {
    posts(first: $first, after: $after, where: $where) {
      nodes {
        id
        databaseId
        date
        totalLikes
        isLiked
        author {
          node {
            id
            name
            avatar {
              url
            }
          }
        }
        galleryImages(first: 1) {
          nodes {
            id
            mimeType
            altText
            sourceUrl(size: MEDIUM)
            mediaItemUrl
            poster {
              id
              mimeType
              altText
              sourceUrl(size: MEDIUM)
            }
          }
        }
        relatedProducts {
          nodes {
            id
            ... on SimpleProduct {
              regularPrice
            }
            ... on VariableProduct {
              regularPrice
            }
          }
        }
      }
      pageInfo {
        total
        endCursor
        hasNextPage
      }
    }
  }
`;

export const POSTS_SEARCH_INPUT = gql`
  query PostsSearchInput($first: Int, $after: String, $where: RootQueryToPostConnectionWhereArgs) {
    posts(first: $first, after: $after, where: $where) {
      nodes {
        id
        title
        databaseId
        slug
        totalLikes
        galleryImages(first: 1) {
          nodes {
            id
            altText
            sourceUrl(size: MEDIUM)
            mediaItemUrl
            poster {
              id
              mimeType
              altText
              sourceUrl(size: MEDIUM)
            }
          }
        }
      }
    }
  }
`;

export const POST = gql`
  ${PostFragment}
  query Posts($postId: ID!) {
    post(id: $postId, idType: SLUG) {
      ...PostFragment
    }
  }
`;

export const STORES = gql`
  query Stores {
    stores(first: 10, where: {sortBy: newStores}) {
      nodes {
        id
        name
        storeUrl
        gravatar
        banner
        followed
        rating
      }
    }
  }
`;

export const PRODUCT_QUICKVIEW = gql`
  query ProductQuickView($id: ID!) {
    product(id: $id, idType: SLUG) {
      id
      currencySymbol
      databaseId
      shippingType
      slug
      name
      description
      type
      averageRating
      isLiked
      totalLikes
      availableShipping {
        city
        available
      }
      midCoins {
        id
        qty
      }
      reviewCount
      product_video_url
      seller {
        id
        name
        followed
        rating
        location
        gravatar
        totalFollowers
      }
      image {
        id
        sourceUrl(size: LARGE)
        altText
        mimeType
      }
      galleryImages {
        nodes {
          id
          sourceUrl(size: LARGE)
          altText
          mimeType
        }
      }
      productTags {
        nodes {
          id
          name
        }
      }
      ... on SimpleProduct {
        id
        currencySymbol
        price
        regularPrice
        salePrice
        commentCount
        product_video_url
      }
      ... on VariableProduct {
        id
        currencySymbol
        price
        regularPrice
        salePrice
        commentCount
        product_video_url
        variations {
          nodes {
            id
            databaseId
            price
            salePrice
            regularPrice
            thumbnail: image {
              id
              sourceUrl(size: THUMBNAIL)
              altText
            }
            image {
              id
              sourceUrl(size: LARGE)
              altText
            }
            attributes {
              nodes {
                label
                name
                value
                attributeId
                id
              }
            }
          }
        }
      }
      productCategories {
        nodes {
          id
          slug
        }
      }
    }
  }
`;

export const ADDRESSES = gql`
  ${ShippingAddressFragment}
  query Addresses {
    testSampleAddresses {
      shipping {
        ...ShippingAddressFragment
      }
    }
  }
`;

export const GET_BANNER = gql`
  query GetBanner($banner_type: BannerTypeEnum!) {
    getBanner(banner_type: $banner_type) {
      title
      image
      linkText
      description
      canDownloadForAndroid
      canDownloadForIOS
    }
  }
`;

export const PRIVACY_POLICY_CONTENT = gql`
  query Page {
    page(idType: DATABASE_ID, id: "5447") {
      content
      title
    }
  }
`;

export const TERMS_OF_USE_CONTENT = gql`
  query Page {
    page(idType: DATABASE_ID, id: "5697") {
      content
      title
    }
  }
`;

export const COOKIE_POLICY_CONTENT = gql`
  query Page {
    page(idType: DATABASE_ID, id: "11537") {
      content
      title
    }
  }
`;

export const GET_TAP_ORDER = gql`
  query GetTapOrder($tapId: String) {
    testSampleTapPayment(tapId: $tapId) {
      amount
      currency
      error
      date
      orderId
      status
      type
      midcoinsAmount
      shopcoinsAmount
      success
      orders {
        nodes {
          id
          status
          subtotal
          total
          currency
          date
          dateCompleted
          datePaid
          discountTotal
          needsPayment
          needsProcessing
          orderNumber
          paymentMethod
          shippingTotal
          totalTax
          shippingTax
          shipping {
            address1
            address2
            city
            company
            country
            firstName
            email
            lastName
            phone
            postcode
            state
          }
          createdVia
          orderKey
          billing {
            address1
            address2
            city
            company
            country
            email
            firstName
            lastName
            phone
            postcode
            state
          }
        }
      }
    }
  }
`;

export const testSample_COD_PAYMENT = gql`
  query testSampleCodPayment($orderId: ID!) {
    testSampleCodPayment(orderId: $orderId) {
      orderId
      amount
      currency
      orders {
        nodes {
          id
          status
          subtotal
          total
          currency
          date
          dateCompleted
          datePaid
          discountTotal
          needsPayment
          needsProcessing
          orderNumber
          paymentMethod
          shippingTotal
          totalTax
          shippingTax
          shipping {
            address1
            address2
            city
            company
            country
            firstName
            email
            lastName
            phone
            postcode
            state
          }
          createdVia
          orderKey
          billing {
            address1
            address2
            city
            company
            country
            email
            firstName
            lastName
            phone
            postcode
            state
          }
        }
      }
    }
  }
`;

export const testSample_TAP_PAY_PAYMENT = gql`
  query testSampleTapPayment($orderId: String!) {
    testSampleTapPayment(tapId: $orderId) {
      orderId
      amount
      currency
      orders {
        nodes {
          id
          status
          subtotal
          total
          currency
          date
          dateCompleted
          datePaid
          discountTotal
          needsPayment
          needsProcessing
          orderNumber
          paymentMethod
          shippingTotal
          totalTax
          shippingTax
          shipping {
            address1
            address2
            city
            company
            country
            firstName
            email
            lastName
            phone
            postcode
            state
          }
          createdVia
          orderKey
          billing {
            address1
            address2
            city
            company
            country
            email
            firstName
            lastName
            phone
            postcode
            state
          }
        }
      }
    }
  }
`;

export const GET_SELLER_PRODUCTS = gql`
  ${VariableProductFragment}
  ${SimpleProductFragment}
  query SellerProducts($id: Int) {
    products(first: 7, where: {vendor: $id}) {
      nodes {
        ...VariableProductFragment
        ...SimpleProductFragment
      }
    }
  }
`;

export const RECENTLY_VIEWED = gql`
  ${VariableProductFragment}
  ${SimpleProductFragment}
  query RecentlyViewed {
    products(first: 7, where: {recently_viewed: true}) {
      nodes {
        ...VariableProductFragment
        ...SimpleProductFragment
      }
    }
  }
`;

export const RECENTLY_VIEWED_BY_ID = gql`
  ${VariableProductFragment}
  ${SimpleProductFragment}
  query RecentlyViewed($viewed: [ID]) {
    products(where: {include: $viewed}) {
      nodes {
        ...VariableProductFragment
        ...SimpleProductFragment
      }
    }
  }
`;
export const DealProductsHome = gql`
  ${VariableProductFragment}
  ${SimpleProductFragment}
  query DealsQuery {
    products(first: 7, where: {is_a_deal_product: true}) {
      edges {
        node {
          ...VariableProductFragment
          ...SimpleProductFragment
        }
      }
      pageInfo {
        total
      }
    }
  }
`;

export const GET_ORDERS = gql`
  query GetOrders($customerId: Int, $statuses: [OrderStatusEnum], $dateQuery: DateQueryInput) {
    orders(where: {customerId: $customerId, statuses: $statuses, dateQuery: $dateQuery}) {
      nodes {
        id
        date
        databaseId
        status
        total
        dateCompleted
        transactionId
        lineItems {
          nodes {
            databaseId
            variationId
            orderId
            leftfeedback
            product {
              id
              slug
              databaseId
              seller {
                id
                name
                storesCount
              }
              image {
                id
                sourceUrl
                title
              }
              currencySymbol
              name
              shopCoins {
                id
                qty
              }
              midCoins {
                id
                qty
              }
              ... on VariableProduct {
                id
                databaseId
                seller {
                  id
                  name
                  storesCount
                }
                image {
                  id
                  sourceUrl
                  title
                }
                currencySymbol
                name
                shopCoins {
                  id
                  qty
                }
                midCoins {
                  id
                  qty
                }
              }
              ... on SimpleProduct {
                id
                databaseId
                seller {
                  id
                  name
                  storesCount
                }
                image {
                  id
                  sourceUrl
                  title
                }
                currencySymbol
                name
                shopCoins {
                  id
                  qty
                }
                midCoins {
                  id
                  qty
                }
              }
            }
            total
          }
        }
        stores {
          nodes {
            name
            rating
            id
            storeUrl
            followed
            gravatar
          }
        }
        billing {
          address1
          address2
          city
          company
          country
          email
          firstName
          lastName
          phone
          postcode
          state
        }
        shipping {
          address1
          address2
          city
          company
          country
          email
          firstName
          lastName
          phone
          postcode
          state
        }
        shippingLines {
          nodes {
            shippingMethod {
              description
              title
            }
          }
        }
      }
    }
  }
`;

export const GET_TAGS = gql`
  query MyTags {
    tags {
      edges {
        node {
          id
          databaseId
          name
        }
      }
    }
  }
`;

export const CART_QUANTITY = gql`
  query CartQuantity {
    cart {
      totalQuantity
    }
  }
`;

export const ORDERS = gql`
  query Orders {
    orders {
      nodes {
        id
        status
        date
        lineItems {
          nodes {
            variation {
              variationId: id
              price
              salePrice
              regularPrice
              image {
                id
                sourceUrl
                altText
              }
            }
            product {
              id
              slug
              name
              databaseId
              ... on SimpleProduct {
                price
                salePrice
                regularPrice
              }
              image {
                id
                sourceUrl
                altText
              }
            }
          }
        }
      }
    }
  }
`;

export const FEED_VARIATIONS_POPUP = gql`
  query FeedVariationsPopup($id: ID!) {
    product(id: $id) {
      id
      name
      databaseId
      ... on SimpleProduct {
        salePrice
        regularPrice
        image {
          id
          sourceUrl
          altText
        }
      }
      ... on VariableProduct {
        variations {
          nodes {
            id
            salePrice
            regularPrice
            image {
              id
              sourceUrl
              altText
            }
            attributes {
              nodes {
                label
                name
                value
                attributeId
                id
              }
            }
          }
        }
      }
    }
  }
`;

export const GET_INTERESTS = gql`
  query GetInterests {
    interests {
      nodes {
        id
        databaseId
        title
        totalFollowers
        slug
        isFollowed
        featuredImage {
          node {
            id
            sourceUrl(size: THUMBNAIL)
            altText
          }
        }
      }
    }
  }
`;

export const COMMENTS_OF_POST = gql`
  ${CommentFragment}
  query CommentsOfPost($id: ID!, $after: String, $first: Int) {
    post(id: $id, idType: ID) {
      id
      comments(where: {parent: 0}, first: $first, after: $after) {
        nodes {
          ...CommentFragment
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;

export const COMMENTS_OF_POST_DESKTOP = gql`
  ${CommentDesktopFragment}
  query CommentsOfPost($id: ID!, $after: String, $first: Int) {
    post(id: $id, idType: ID) {
      id
      comments(where: {parent: 0}, first: $first, after: $after) {
        nodes {
          ...CommentDesktopFragment
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;

export const REPLY = gql`
  ${CommentFragment}
  query Comments($id: ID!, $after: String, $first: Int) {
    comment(id: $id) {
      id
      replies(first: $first, after: $after) {
        nodes {
          ...CommentFragment
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;

export const GET_REVIEWS = gql`
  ${ReviewsFragment}
  query getReviews($contentId: ID, $userId: [ID]) {
    comments(where: {commentType: "review", contentId: $contentId, authorIn: $userId}) {
      nodes {
        ...ReviewsFragment
      }
    }
  }
`;

export const RELATED_USERS = gql`
  query RelatedUsers($id: ID!, $search: String) {
    user(id: $id, idType: DATABASE_ID) {
      id
      followers(search: $search, limit: 3) {
        id
        name
        username
        avatar {
          url
        }
      }
      following(search: $search, limit: 3) {
        id
        name
        username
        avatar {
          url
        }
      }
    }
  }
`;

export const GET_CURRENCIES = gql`
  query GetCurrecies($id: ID!) {
    user(id: $id, idType: DATABASE_ID) {
      id
      currency
      coinsConversionRate
      availableMidcoins
    }
  }
`;

export const GET_COINS = gql`
  query GetCoins($id: ID!) {
    user(id: $id, idType: DATABASE_ID) {
      id
      currency
      coinsConversionRate
      availableMidcoins
    }
  }
`;

export const TRENDS = gql`
  ${FlashPostFragment}
  query Trends($first: Int, $where: RootQueryToPostConnectionWhereArgs, $after: String) {
    posts(first: $first, where: $where, after: $after) {
      nodes {
        ...FlashPostFragment
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`;

export const GET_POPULAR_TAGS = gql`
  query Tags($where: RootQueryToTagConnectionWhereArgs, $first: Int) {
    tags(where: $where, first: $first) {
      nodes {
        id
        slug
        name
      }
    }
  }
`;

export const testSample_CHECKOUT = gql`
  query testSampleCheckout {
    testSampleCheckout {
      checkoutAuthorize
      checkoutPublicKey
      checkoutEnable
      tapPublicKey
      tapEnable
    }
  }
`;

export const APPLIED_MIDCOINS = gql`
  query AppliedMidcoins {
    cart {
      appliedMidCoins {
        discountAmount
        qty
      }
    }
  }
`;

export const APPLIED_COUPONS = gql`
  query AppliedCoupons {
    cart {
      appliedCoupons {
        discountAmount
        code
      }
    }
  }
`;

export const testSample_GET_SAVED_CARDS = gql`
  query GetSavedCards($id: ID!) {
    user(id: $id, idType: DATABASE_ID) {
      id
      savedCards {
        brand
        exp_month
        exp_year
        first_six
        id
        last_four
      }
    }
  }
`;

export const FEED_POSTS_MOBILE = gql`
  ${PostFragment}
  query FeedPosts($where: RootQueryToPostConnectionWhereArgs, $first: Int, $after: String) {
    posts(where: $where, first: $first, after: $after) {
      nodes {
        ...PostFragment
      }
      pageInfo {
        hasNextPage
        endCursor
        hasPreviousPage
        startCursor
        total
      }
    }
  }
`;

export const FEED_POSTS = gql`
  query FeedPosts($where: RootQueryToPostConnectionWhereArgs, $first: Int, $after: String) {
    posts(where: $where, first: $first, after: $after) {
      nodes {
        id
        title
        content
        date
        commentCount
        isFeedbackPost
        rating
        databaseId
        slug
        isLiked
        totalLikes
        testSampleType
        ratio
        tags {
          nodes {
            id
            name
            slug
          }
        }
        author {
          node {
            id
            nicename
            isFollowed
            name
            username
            databaseId
            slug
            avatar {
              url
            }
            roles {
              nodes {
                name
                id
              }
            }
          }
        }
        galleryImages {
          nodes {
            id
            altText
            sourceUrl(size: MEDIUM_LARGE)
            mediaDetails {
              height
              width
            }
            mediaItemUrl
            mimeType
            poster {
              id
              mediaItemUrl
              sourceUrl(size: MEDIUM_LARGE)
              altText
            }
          }
        }
        geo {
          country_name
          city
        }
        relatedProducts {
          nodes {
            id
            databaseId
            name
            image {
              id
              altText
              sourceUrl(size: THUMBNAIL)
              mimeType
            }
            ... on SimpleProduct {
              salePrice
              regularPrice
            }
            ... on VariableProduct {
              salePrice
              regularPrice
              variations(first: 1) {
                nodes {
                  id
                  databaseId
                  regularPrice
                  salePrice
                  image {
                    id
                    sourceUrl(size: LARGE)
                    altText
                  }
                }
              }
            }
          }
        }
      }
      pageInfo {
        hasNextPage
        endCursor
        hasPreviousPage
        startCursor
        total
      }
    }
  }
`;

export const SHOP_FEATURED_PRODUCTS = gql`
  query shopFeaturedProducts($where: RootQueryToProductConnectionWhereArgs, $first: Int, $after: String) {
    products(where: $where, first: $first, after: $after) {
      nodes {
        id
        currencySymbol
        databaseId
        shippingType
        slug
        name
        description
        type
        averageRating
        isLiked
        totalLikes
        midCoins {
          id
          qty
        }
        reviewCount
        product_video_url
        productVideoPoster: product_video_poster
        seller {
          id
          name
          storeUrl
          followed
          rating
          location
          gravatar
          totalFollowers
        }
        image {
          id
          sourceUrl(size: LARGE)
          altText
          mimeType
        }
        galleryImages {
          nodes {
            id
            sourceUrl(size: LARGE)
            altText
            mimeType
          }
        }
        productTags {
          nodes {
            id
            name
          }
        }
        ... on SimpleProduct {
          id
          currencySymbol
          price
          regularPrice
          salePrice
          commentCount
          product_video_url
        }
        ... on VariableProduct {
          id
          currencySymbol
          price
          regularPrice
          salePrice
          commentCount
          product_video_url
          variations {
            nodes {
              id
              databaseId
              price
              salePrice
              regularPrice
              thumbnail: image {
                id
                sourceUrl(size: THUMBNAIL)
                altText
              }
              image {
                id
                sourceUrl(size: LARGE)
                altText
              }
              attributes {
                nodes {
                  label
                  name
                  value
                  attributeId
                  id
                }
              }
            }
          }
        }
        productCategories {
          nodes {
            id
            slug
          }
        }
      }
      pageInfo {
        hasNextPage
        endCursor
        hasPreviousPage
        startCursor
        total
      }
    }
  }
`;

export const FEED_POST = gql`
  query FeedPost($id: ID!) {
    post(id: $id, idType: DATABASE_ID) {
      id
      databaseId
      title
      content
      commentCount
      isLiked
      totalLikes
      slug
      geo {
        country_name
        city
      }
      author {
        node {
          id
          databaseId
          name
          totalFollowers
          isFollowed
          nicename
          avatar {
            url
          }
          roles {
            nodes {
              id
              name
            }
          }
        }
      }
      tags {
        nodes {
          id
          name
          slug
        }
      }
      galleryImages {
        nodes {
          id
          altText
          sourceUrl(size: LARGE)
          mediaItemUrl
          mimeType
          poster {
            id
            altText
            sourceUrl(size: THUMBNAIL)
          }
        }
      }
      relatedProducts {
        nodes {
          id
          databaseId
          name
          image {
            id
            altText
            sourceUrl(size: THUMBNAIL)
            mimeType
          }
          ... on SimpleProduct {
            salePrice
            regularPrice
          }
          ... on VariableProduct {
            salePrice
            regularPrice
            variations(first: 1) {
              nodes {
                id
                regularPrice
                salePrice
                image {
                  id
                  sourceUrl(size: LARGE)
                  altText
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const FEED_POST_SHORT = gql`
  query FeedPostShort($id: ID!) {
    post(id: $id, idType: DATABASE_ID) {
      id
      databaseId
      commentCount
      author {
        node {
          id
          databaseId
          name
          nicename
          totalFollowers
          isFollowed
          roles {
            nodes {
              id
              name
            }
          }
        }
      }
      relatedProducts {
        nodes {
          id
          databaseId
          image {
            id
            altText
            sourceUrl(size: THUMBNAIL)
            mimeType
          }
        }
      }
      galleryImages(first: 1) {
        nodes {
          id
          altText
          sourceUrl(size: THUMBNAIL)
          mediaItemUrl
          mimeType
          poster {
            id
            altText
            sourceUrl(size: THUMBNAIL)
          }
        }
      }
      tags {
        nodes {
          id
          name
          slug
        }
      }
    }
  }
`;

export const PRODUCT_POST_QUICKVIEW = gql`
  query ProductPostQuickView($id: ID!) {
    product(id: $id, idType: DATABASE_ID) {
      id
      databaseId
      slug
      name
      description
      isLiked
      totalLikes
      product_video_url
      availableShipping {
        city
        available
      }
      midCoins {
        id
        qty
      }
      seller {
        id
        name
        followed
        gravatar
        totalFollowers
      }
      image {
        id
        sourceUrl(size: LARGE)
        altText
        mimeType
      }
      galleryImages {
        nodes {
          id
          sourceUrl(size: LARGE)
          altText
          mimeType
        }
      }
      productTags {
        nodes {
          id
          name
        }
      }
      ... on SimpleProduct {
        price
        regularPrice
        salePrice
        commentCount
        product_video_url
      }
      ... on VariableProduct {
        price
        regularPrice
        salePrice
        commentCount
        variations {
          nodes {
            id
            databaseId
            price
            salePrice
            regularPrice
            image {
              id
              sourceUrl(size: LARGE)
              altText
              mimeType
            }
            attributes {
              nodes {
                label
                name
                value
                attributeId
                id
              }
            }
          }
        }
      }
    }
  }
`;

export const CHECKOUTDOTCOM_PAYMENT_HANDLER = gql`
  query testSampleCheckoutDotComPaymentHandler($ckoId: String) {
    testSampleCheckoutDotComPayment(ckoId: $ckoId) {
      success
      status
    }
  }
`;

export const testSample_CHECKOUTDOTCOM_PAYMENT = gql`
  query testSampleCheckoutDotComPayment($ckoId: String) {
    testSampleCheckoutDotComPayment(ckoId: $ckoId) {
      amount
      currency
      error
      date
      orderId
      status
      type
      midcoinsAmount
      shopcoinsAmount
      success
      orders {
        nodes {
          id
          status
          subtotal
          total
          currency
          date
          dateCompleted
          datePaid
          discountTotal
          needsPayment
          needsProcessing
          orderNumber
          paymentMethod
          shippingTotal
          totalTax
          shippingTax
          shipping {
            address1
            address2
            city
            company
            country
            firstName
            email
            lastName
            phone
            postcode
            state
          }
          createdVia
          orderKey
          billing {
            address1
            address2
            city
            company
            country
            email
            firstName
            lastName
            phone
            postcode
            state
          }
        }
      }
    }
  }
`;

export const testSample_BALANCE = gql`
  query testSampleBalanceData($after: String, $before: String, $first: Int, $last: Int, $where: BalanceSearchWhere) {
    testSampleBalanceData(after: $after, before: $before, first: $first, last: $last, where: $where) {
      nodes {
        id
        date
        storeName
        type
        awardedQty
        status
      }
      pageInfo {
        endCursor
        hasNextPage
        total
      }
    }
  }
`;

export const WALLET_BALANCE = gql`
  query WalletBalance($id: ID!) {
    user(id: $id, idType: DATABASE_ID) {
      id
      walletBalance
    }
  }
`;

export const AFFILIATE_LINK = gql`
  query AffiliateLink($id: ID!) {
    user(id: $id, idType: DATABASE_ID) {
      id
      affiliateLink
    }
  }
`;

export const GET_LISTS = gql`
  query TestLists($first: Int, $after: String, $where: ListSearchWhere) {
    lists(first: $first, after: $after, where: $where) {
      nodes {
        id
        user_id
        list_name
        list_description
        list_id
        is_private
        hashtags
        isFollowed
        totalFollowers
        addedProducts {
          nodes {
            name
            slug
            image {
              altText
              sourceUrl
            }
          }
        }
      }
      pageInfo {
        total
        endCursor
        hasNextPage
      }
    }
  }
`;

export const SEARCH_INPUT = gql`
  query SearchInput($search: String) {
    products(where: {search: $search}, first: 4) {
      nodes {
        id
        slug
        name
        shippingType
        totalLikes
        image {
          id
          altText
          sourceUrl
        }
      }
    }
    stores(first: 2, where: {search: $search}) {
      nodes {
        id
        storeUrl
        name
        followed
        totalFollowers
        gravatar
      }
    }
    users(where: {search: $search}, first: 2) {
      nodes {
        id
        databaseId
        slug
        username
        totalFollowers
        isFollowed
        avatar {
          url
        }
      }
    }
    lists(first: 2, where: {list_name: $search, is_private: false}) {
      nodes {
        id
        list_name
        list_id
        user_id
        totalFollowers
        isFollowed
      }
    }
    posts(where: {search: $search}, first: 2) {
      nodes {
        id
        databaseId
        slug
        title
        galleryImages(first: 1) {
          nodes {
            id
            sourceUrl(size: THUMBNAIL)
            poster {
              id
              sourceUrl(size: THUMBNAIL)
            }
          }
        }
      }
    }
  }
`;

export const SEARCH_INPUT_PRODUCTS = gql`
  query SearchInputProducts($where: RootQueryToProductConnectionWhereArgs, $first: Int, $after: String) {
    products(where: $where, first: $first, after: $after) {
      nodes {
        id
        databaseId
        slug
        name
        totalLikes
        image {
          id
          sourceUrl(size: LARGE)
          altText
          mimeType
        }
        galleryImages(first: 1) {
          nodes {
            id
            sourceUrl(size: LARGE)
            altText
            mimeType
          }
        }
      }
    }
  }
`;
