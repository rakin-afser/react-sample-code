import React, {useEffect, useState} from 'react';
import {arrayOf, shape, bool} from 'prop-types';
import Grid from 'components/Grid';
import PostsContent from 'components/PostsContent/mobile';
import {ContentMobile, MessageBoxMobile, NavMobile, TabMobile} from './styled';
import Trends from 'components/Trends/mobile';

const MyProfileMobile = ({data, loadingPosts, stores, endRefEl, tabs, activeTabIndex, setActiveTabIndex}) => {
  // TODO these constant using for demonstration different content

  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, []);

  return (
    <Grid pageContainer {...{padding: '0'}}>
      <ContentMobile>
        <Trends />
        <NavMobile>
          {tabs.map((tab, i) => (
            <TabMobile active={activeTabIndex === i} key={tab} onClick={() => setActiveTabIndex(i)}>
              {tab}
            </TabMobile>
          ))}
        </NavMobile>
        <PostsContent
          data={data}
          stores={stores}
          loading={loadingPosts}
          withPopularProducts={false}
          endRefEl={endRefEl}
        />
      </ContentMobile>
    </Grid>
  );
};

MyProfileMobile.defaultProps = {
  data: [],
  loading: false
};

MyProfileMobile.propTypes = {
  data: arrayOf(shape({})),
  loading: bool
};

export default MyProfileMobile;
