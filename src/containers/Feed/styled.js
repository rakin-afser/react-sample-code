import styled, {css} from 'styled-components/macro';

import {menuTitleColor, textGray, primaryColor, blue} from 'constants/colors';

export const Content = styled.div`
  flex-grow: 1;
  //max-width: calc(100% - 591px);
  padding: 0 16px;
  max-width: 502px;
`;

export const ContentMobile = styled.div`
  flex-grow: 1;
  max-width: 100%;
`;

export const Aside = styled.aside`
  position: sticky;
  top: 10px;
  width: ${({right}) => (right ? '292px' : '270px')};
  flex-shrink: 0;
`;

export const Nav = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
  background: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin-bottom: 8px;
`;

export const NavMobile = styled.div`
  font-size: 14px;
  display: flex;
  flex: 50%;
  justify-content: center;
  align-items: center;
  background: #fff;
  margin-bottom: 8px;
`;

export const Tab = styled.button`
  position: relative;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 40px;
  background: transparent;
  border: none;
  cursor: pointer;
  font-family: Helvetica Neue, sans-serif;
  font-weight: 500;
  font-size: 14px;
  outline: none;
  padding: 0;
  margin: 0 16px;
  color: ${({active}) => (active ? '#000' : `${menuTitleColor}`)};
  transition: color 0.3s;

  &:hover {
    color: #000;

    &:before {
      height: 2px;
    }
  }

  &:before {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: ${({active}) => (active ? '2px' : `2px`)};
    background: currentColor;
    transition: ease 0.3s;
    opacity: ${({active}) => (active ? '1' : `0`)};
    border-radius: 2px;
  }
`;

export const TabMobile = styled.button`
  width: 50%;
  position: relative;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 40px;
  background: transparent;
  border: none;
  cursor: pointer;
  font-family: Helvetica Neue, sans-serif;
  font-weight: 700;
  font-size: 14px;
  outline: none;
  padding: 0;
  color: ${({active}) => (active ? '#000' : `${menuTitleColor}`)};
  transition: color 0.3s;

  &:hover {
    color: #000;

    &:before {
      height: 2px;
    }
  }

  &:before {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: ${({active}) => (active ? '2px' : `0`)};
    background: currentColor;
    transition: height 0.3s;
    border-radius: 2px;
  }
`;

export const MessageBox = styled.div`
  padding: 15px 78px;
  background: #fff;
  border: 1px solid #ececec;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  color: ${textGray};
  margin-bottom: 18px;

  .link {
    cursor: pointer;
    color: ${primaryColor};
    font-weight: 700;
  }

  button {
    transition: ease 0.4s;
    cursor: pointer;

    &:hover {
      color: ${blue};
      transform: scale(1.1);
    }
  }
`;

export const MessageBoxMobile = styled.div`
  padding: 15px 16px;
  background: #fff;
  border: 1px solid #ececec;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  color: ${textGray};
  margin-top: 18px;
  margin-bottom: 18px;
  text-align: center;

  .link {
    cursor: pointer;
    color: ${primaryColor};
    font-weight: 700;
  }
`;

export const EndRef = styled.div`
  ${({order}) =>
    order
      ? css`
          order: ${order};
        `
      : null}
`;
