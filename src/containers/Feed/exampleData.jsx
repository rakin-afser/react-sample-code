import React from 'react';

import product1 from 'images/gallery/gallery1.jpg';
import product2 from 'images/gallery/gallery2.jpg';
import avatar from './img/avatar.png';
import avatar2 from './img/avatar2.png';
import avatar3 from './img/avatar3.png';
import avatar4 from './img/avatar4.png';
import avatar5 from './img/avatar5.png';
import avatar6 from './img/avatar6.png';
import avatar7 from './img/avatar7.png';
import preview from './img/preview.png';
import preview1 from './img/preview1.png';
import preview2 from './img/preview2.png';
import preview3 from './img/preview3.png';
import preview4 from './img/preview4.png';
import preview5 from './img/preview5.png';
import preview6 from './img/preview6.png';
import preview7 from './img/preview7.png';

export default [
  {
    avatar,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: null,
    date: '12 Oct',
    preview
  },
  {
    avatar: avatar2,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'It’s just wonderful!',
    date: '12 Oct',
    preview: preview1
  },
  {
    avatar: avatar2,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'Wow!',
    date: '12 Oct',
    preview: preview2
  },
  {
    avatar: avatar3,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'I used to be here!',
    date: '12 Oct',
    preview: preview3
  },
  {
    avatar: avatar4,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'It’s just wonderful!',
    date: '12 Oct',
    preview: preview4
  },
  {
    avatar: avatar5,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'It’s just wonderful!',
    date: '12 Oct',
    preview: preview5
  },
  {
    avatar: avatar6,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'It’s just wonderful!',
    date: '12 Oct',
    preview: preview6
  },
  {
    avatar: avatar7,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'It’s just wonderful!',
    date: '12 Oct',
    preview: preview7
  },
  {
    avatar: avatar6,
    name: 'Juanita Black',
    info: 'replied to your post',
    message: 'It’s just wonderful!',
    date: '12 Oct',
    preview: preview6
  }
];

export const linksList = [
  {href: '#', text: 'About'},
  {href: '#', text: 'Sell on testSample'},
  {href: '#', text: 'Get The App'},
  {href: '#', text: 'Contact'},
  {href: '#', text: 'Privacy Policy'},
  {href: '#', text: 'Terms of Service'}
];

export const newShops = [
  {avatar, title: 'Luis Vuitton & ...', isFollowing: false},
  {avatar: avatar2, title: 'Luis Vuitton & ...', isFollowing: false},
  {avatar: avatar3, title: 'Luis Vuitton & ...', isFollowing: false}
];

export const peopleToFollow = [
  {avatar, title: 'anna_stone', isFollowing: false, followers: 100000},
  {avatar: avatar2, title: 'anna_stone', isFollowing: true, followers: 80000},
  {avatar: avatar3, title: 'anna_stone', isFollowing: false, followers: 120000}
];

export const mostLiked = [
  {
    action: 'commented on',
    userLink: '@chanel',
    user: {
      name: 'Rihannaofficial',
      img: {
        src: avatar,
        description: 'image description'
      }
    },
    time: '1h',
    postContent: (
      <>
        <p>Post: Tati Beauty started selling a new pallette...</p>
        <img src={product1} alt="product" />
      </>
    )
  },
  {
    type: 'following',
    action: 'started following',
    userLink: '@guessoffical',
    user: {
      name: 'Jms_Charles',
      img: {
        src: avatar2,
        description: 'image description'
      }
    },
    time: '1d'
  },
  {
    action: 'commented on',
    userLink: '@iamtati',
    user: {
      name: 'Sherylinfenn',
      img: {
        src: avatar3,
        description: 'image description'
      }
    },
    time: '1h',
    postContent: (
      <>
        <img src={product2} alt="product" />
        <p>Post: Tati Beauty started selling a new pallette...</p>
      </>
    )
  },
  {
    type: 'following',
    action: 'started following',
    userLink: '@jimmychoo',
    user: {
      name: 'Jms_Charles',
      img: {
        src: avatar2,
        description: 'image description'
      }
    },
    time: '1d'
  }
];

export const recentComments = [
  {
    action: 'left a comment',
    userLink: '@JSJK',
    user: {
      name: 'Lovelytunes',
      img: {
        src: avatar,
        description: 'image description'
      }
    },
    time: '3h'
  },
  {
    action: 'commented on',
    userLink: '@AntimoDA',
    user: {
      name: 'Mango Official',
      img: {
        src: avatar2,
        description: 'image description'
      }
    },
    time: '1d',
    postContent: (
      <>
        <p>Post: Tati Beauty started selling a new pallette...</p>
        <img src={product2} alt="product" />
      </>
    )
  },
  {
    action: 'commented on',
    userLink: '@AntimoDA',
    user: {
      name: 'Mango Official',
      img: {
        src: avatar2,
        description: 'image description'
      }
    },
    time: '1d',
    postContent: (
      <>
        <p>Post: Tati Beauty started selling a new pallette...</p>
        <img src={product2} alt="product" />
      </>
    )
  }
];
