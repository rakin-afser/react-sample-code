import React, {useState} from 'react';
import 'intersection-observer';
import {useWindowSize} from '@reach/window-size';
import {useQuery} from '@apollo/client';
import {Helmet} from 'react-helmet';
import {useUser} from 'hooks/reactiveVars';

import Error from 'components/Error';
import FeedDesktop from './FeedDesktop';
import FeedMobile from './FeedMobile';
import Layout from 'containers/Layout';
import {STORES, FEED_POSTS, FEED_POSTS_MOBILE} from 'queries';
import {EndRef} from './styled';
import {useOnScreen} from 'hooks';

const Feed = () => {
  const [user] = useUser();
  const {data, loading, error} = useQuery(STORES);
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const isUserLogeIn = user?.username;
  const query = isMobile ? FEED_POSTS_MOBILE : FEED_POSTS;

  const tabs = isUserLogeIn ? ['Local', 'Global', 'Following'] : ['Local', 'Global'];

  const variables = {
    first: 7,
    where: {
      postGeo: 'LOCAL',
      orderby: [{field: 'DATE', order: 'DESC'}]
    }
  };

  switch (activeTabIndex) {
    case 0:
      variables.where = {
        postGeo: 'LOCAL',
        orderby: [{field: 'DATE', order: 'DESC'}]
      };
      break;
    case 1:
      variables.where = {
        postGeo: 'GLOBAL',
        orderby: [{field: 'DATE', order: 'DESC'}]
      };
      break;
    case 2:
      variables.where = {
        isFollowing: true,
        orderby: [{field: 'DATE', order: 'DESC'}]
      };
      break;
    default:
      break;
  }

  const {data: dataPosts, loading: loadingPosts, error: errorPosts, fetchMore} = useQuery(query, {
    variables,
    notifyOnNetworkStatusChange: true
  });
  const ref = useOnScreen({
    doAction() {
      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.posts?.nodes || [];
          const newNodes = fetchMoreResult?.posts?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            posts: {
              ...fetchMoreResult?.posts,
              nodes
            }
          };
        },
        variables: {
          ...variables,
          after: endCursor
        }
      });
    }
  });

  const {endCursor, hasNextPage} = dataPosts?.posts?.pageInfo || {};
  const endRefEl = hasNextPage && !loadingPosts ? <EndRef ref={ref} /> : null;
  const {stores} = data || {};

  const renderContent = () => {
    if (error || errorPosts) {
      return <Error />;
    }

    return isMobile ? (
      <FeedMobile
        data={dataPosts?.posts?.nodes}
        stores={stores}
        loadingPosts={loadingPosts}
        endRefEl={endRefEl}
        tabs={tabs}
        activeTabIndex={activeTabIndex}
        setActiveTabIndex={setActiveTabIndex}
      />
    ) : (
      <FeedDesktop
        data={dataPosts?.posts?.nodes}
        loading={loading}
        loadingPosts={loadingPosts}
        endRefEl={endRefEl}
        tabs={tabs}
        activeTabIndex={activeTabIndex}
        setActiveTabIndex={setActiveTabIndex}
      />
    );
  };

  return (
    <Layout showBottomNav={isMobile} isFooterShort>
      <Helmet>
        <title>testSample - Shop Share Earn</title>
        <meta
          name="description"
          content="Post a video, add products, share with the community & start earning, its easy!"
        />
      </Helmet>
      {renderContent()}
    </Layout>
  );
};

export default Feed;
