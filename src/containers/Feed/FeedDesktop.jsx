import React from 'react';
import PropTypes from 'prop-types';
import PostContent from 'containers/ShopPage/desktop/PostsPage/PostsContent';
import Grid from 'components/Grid';
import ShopsToFollow from 'components/ShopsToFollow';
import useGlobal from 'store';
import ToFollow from './components/ToFollow';
import ActionBar from './components/ActionBar';
import HashtagsList from './components/HashtagsList';
import RecentNews from './components/RecentNews';
import LinksList from './components/LinksList';
import {Content, Aside, Nav, Tab, MessageBox} from './styled';
// import {linksList, newShops, peopleToFollow, mostLiked, recentComments} from './exampleData';
import peopleToFollow from './exampleData';
import SidebarMostLIked from 'containers/Feed/components/SidebarMostLiked';
import SidebarRecentComments from 'containers/Feed/components/SidebarRecentComments';
import {useUser} from 'hooks/reactiveVars';

const MyProfileDesktop = ({data, loading, loadingPosts, endRefEl, tabs, activeTabIndex, setActiveTabIndex}) => {
  const [user] = useUser();
  const [_, setGlobalState] = useGlobal();
  const isUserLogeIn = user?.username?.length > 0;

  const userType = 'buyer';

  const showReportedData = (reportedData) => console.log('Report Submitted', reportedData);

  const showReportModal = () => {
    setGlobalState.setReportModal(true, {
      type: 'list',
      onSubmit: showReportedData
    });
  };

  const renderLeftSidebarContent = () => {
    if (isUserLogeIn && userType === 'seller') {
      return (
        <>
          <ToFollow data={peopleToFollow} title="People to follow " type="person" />
        </>
      );
    }

    return (
      <>
        <ShopsToFollow forFeed />
        <HashtagsList />
        <LinksList />
      </>
    );
  };

  return (
    <Grid pageContainer padding="4px 0 0 0" customStyles={{maxWidth: '815px', transform: 'translate(-50px)'}}>
      <Grid sb>
        <Aside>{renderLeftSidebarContent()}</Aside>
        <Content>
          {isUserLogeIn ? <ActionBar /> : null}
          <Nav>
            {tabs.map((tab, i) => (
              <Tab active={activeTabIndex === i} key={i} onClick={() => setActiveTabIndex(i)}>
                {tab}
              </Tab>
            ))}
          </Nav>
          <PostContent
            forFeed
            data={data}
            showReportPopup={showReportModal}
            withPopularProducts={false}
            isMyProfile
            isUserLogeIn={isUserLogeIn}
            loading={loadingPosts}
            endRefEl={endRefEl}
          />
        </Content>
        {/* <Aside right> */}
        {/*   {(!isUserLogeIn || (!!isUserLogeIn && userType === 'buyer')) && <SidebarMostLIked />} */}
        {/*   {!!isUserLogeIn && userType === 'seller' && ( */}
        {/*     <RecentNews title="Recent Stories" news={recentComments} loading={loading} /> */}
        {/*   )} */}
        {/*   {!!isUserLogeIn && userType === 'buyer' && <SidebarRecentComments />} */}
        {/* </Aside> */}
      </Grid>
    </Grid>
  );
};

MyProfileDesktop.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
  loading: PropTypes.bool
};

MyProfileDesktop.defaultProps = {
  data: [],
  loading: false
};

export default MyProfileDesktop;
