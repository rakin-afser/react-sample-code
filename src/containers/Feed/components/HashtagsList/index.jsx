import React from 'react';
import {useQuery} from '@apollo/client';
import {Button} from 'antd';
import LogoIcon from 'assets/LogoIcon';

import {Wrap, Title, List, Divivder, LogoBox} from './styled';
import Error from 'components/Error';
import {GET_POPULAR_TAGS} from 'queries';
import {Link} from 'react-router-dom';
import qs from 'qs';

const HashtagsList = () => {
  const {data, error} = useQuery(GET_POPULAR_TAGS, {
    variables: {where: {hideEmpty: true, order: 'DESC', orderby: 'COUNT'}, first: 10}
  });

  if (error) {
    return (
      <Wrap>
        <Error />
      </Wrap>
    );
  }

  return (
    <Wrap>
      <Divivder />
      <Title style={{color: '#000'}}>Popular hashtags</Title>
      <List>
        {data?.tags?.nodes?.map((tag) => (
          <Link to={`/search/posts/all?${qs.stringify({filters: {hashtags: [tag?.slug]}})}`} key={tag?.id}>
            #{tag?.name}
          </Link>
        ))}
      </List>
      <Divivder />
      <Title>Open Your Shop With testSample</Title>
      <LogoBox>
        <LogoIcon />
        <Button href="/" type="danger" shape="round">
          Start a Shop
        </Button>
      </LogoBox>
    </Wrap>
  );
};

export default HashtagsList;
