import styled from 'styled-components/macro';
import {secondaryTextColor, primaryColor, grayTextColor, blueColor} from 'constants/colors';

export const Wrap = styled.div`
  padding: 27px 0 21px;
`;

export const Title = styled.span`
  display: block;
  font-weight: 700;
  color: ${secondaryTextColor};
  margin-bottom: 12px;
`;

export const List = styled.div`
  margin-bottom: -5px;

  a {
    color: ${grayTextColor};
    margin: 0 5px 0 0;
    display: inline-block;
    vertical-align: top;

    &:hover {
      color: ${blueColor};
    }
  }
`;

export const Divivder = styled.hr`
  margin: 13px 0;
  border-color: rgba(122, 122, 122, 0.3);
  border-width: 0 0 1px;
`;

export const LogoBox = styled.div`
  display: flex;
  justify-content: space-between;

  svg {
    width: 100px;
    height: 28px;
  }

  .ant-btn {
    position: relative;
    background: #000;
    border: none;
    height: 28px;
    font-size: 10px;
    font-weight: 700;
    line-height: 26px;
    transition: ease 0.4s;
    box-shadow: none;

    &:hover {
      background-color: ${primaryColor};
      color: #fff;
      box-shadow: none;
    }
  }
`;
