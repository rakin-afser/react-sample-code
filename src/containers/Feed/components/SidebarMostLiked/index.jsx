import React, {memo} from 'react';
import RecentNews from 'containers/Feed/components/RecentNews';
import {useQuery, gql} from '@apollo/client';
import Error from 'components/Error';

const GET_MOST_LIKED = gql`
  query getMostLiked {
    popularPosts {
      nodes {
        id
        title
        postId
        databaseId
        content
        date
        author {
          node {
            id
            username
            userId
            profile_picture
          }
        }
        galleryImages {
          nodes {
            id
            sourceUrl(size: MEDIUM)
          }
        }
      }
    }
  }
`;

const SidebarMostLIked = memo(() => {
  const {data, loading, error} = useQuery(GET_MOST_LIKED);

  if (error) {
    return <Error />;
  }

  return (
    <RecentNews
      actionTitle="liked on"
      title="Most Liked"
      news={data?.popularPosts?.nodes?.slice(0, 4)}
      loading={loading}
    />
  );
});

SidebarMostLIked.propTypes = {};

export default SidebarMostLIked;
