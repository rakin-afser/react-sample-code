import React, {memo} from 'react';
import {useQuery, gql} from '@apollo/client';
import Error from 'components/Error';
import RecentNews from 'containers/Feed/components/RecentNews';

const GET_LAST_POSTS_COMMENTS = gql`
  query GetLatPostsComments {
    comments(where: {contentStatus: PUBLISH, contentType: POST}, first: 4) {
      nodes {
        author {
          node {
            ... on User {
              id
              profile_picture
              username
              databaseId
            }
          }
        }
        commentedOn {
          node {
            ... on Post {
              id
              title
              galleryImages {
                nodes {
                  id
                  sourceUrl(size: MEDIUM)
                }
              }
            }
          }
        }
        content
        date
        id
      }
    }
  }
`;

const SidebarRecentComments = memo(() => {
  const {data, loading, error} = useQuery(GET_LAST_POSTS_COMMENTS);

  const newData = data?.comments?.nodes?.reduce((acc, item) => {
    return acc.concat({...item.commentedOn.node, content: item.content, date: item.date, author: item.author});
  }, []);

  if (error) {
    return <Error />;
  }

  return <RecentNews actionTitle="commented on" title="Recent Comments" news={newData} loading={loading} />;
});

SidebarRecentComments.propTypes = {};

export default SidebarRecentComments;
