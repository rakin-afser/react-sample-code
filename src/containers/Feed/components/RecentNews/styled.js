import styled from 'styled-components/macro';

import {textGray, primaryColor, blue} from 'constants/colors';

export const Wrap = styled.div`
  background: #fff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
`;

export const Title = styled.span`
  display: block;
  font-size: 16px;
  font-weight: 500;
  padding: 16px 16px 0 16px;
  color: #000;
`;

export const Post = styled.div`
  padding: 16px;

  &:not(:last-child) {
    border-bottom: 1px solid #eee;
  }
`;

export const Heading = styled.div`
  display: flex;
  align-items: center;
`;

export const Avatar = styled.div`
  width: 44px;
  height: 44px;
  border-radius: 50%;
  overflow: hidden;
  flex-shrink: 0;
  margin-right: 18px;
  transition: ease 0.4s;

  img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  &:hover {
    box-shadow: 0 0 8px rgba(000, 000, 000, 0.5);
    cursor: pointer;
  }
`;

export const UserInfo = styled.div`
  flex-grow: 1;

  p {
    margin: 0;
    font-size: 12px;
  }

  a {
    color: ${primaryColor};
  }
`;

export const UserName = styled.div`
  display: flex;

  strong {
    font-weight: 500;
    color: #000;
    flex-grow: 1;
    padding-right: 10px;
    transition: ease 0.4s;

    &:hover {
      cursor: pointer;
      color: ${blue};
    }
  }

  span {
    font-size: 12px;
    color: ${textGray};
  }
`;

export const PostContent = styled.div`
  color: rgba(0, 0, 0, 0.8);
  margin-top: 8px;

  p {
    margin: 0;

    & + img {
      margin-top: 5px;
    }
  }

  img {
    & + p {
      margin-top: 10px;
    }
  }
`;

export const PostImg = styled.div`
  margin-top: 10px;
  width: 91px;
  height: 91px;
  overflow: hidden;
  border-radius: 6px;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;
