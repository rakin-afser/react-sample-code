import React, {memo} from 'react';
import {arrayOf, string, bool, shape} from 'prop-types';
import {useHistory} from 'react-router-dom';
import defaultAvatar from 'images/avatarPlaceholder.png';
import parse from 'html-react-parser';
import moment from 'moment';

import {Wrap, Title, Post, Heading, Avatar, UserInfo, UserName, PostContent, PostImg} from './styled';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';

const RecentNews = memo(({news, title, loading, actionTitle}) => {
  const history = useHistory();

  return (
    <Wrap>
      <Title>{title}</Title>
      {news?.map((post, index) => {
        if (loading) {
          return (
            <SkeletonTheme style={{marginTop: 30, width: '100%'}}>
              <Skeleton style={{marginBottom: 30, width: '100%'}} height={50} />
              <Skeleton style={{marginBottom: 30, width: '100%'}} height={50} />
              <Skeleton style={{marginBottom: 30, width: '100%'}} height={50} />
              <Skeleton style={{marginBottom: 30, width: '100%'}} height={50} />
            </SkeletonTheme>
          );
        }

        return (
          <Post key={index}>
            <Heading>
              <Avatar onClick={() => history.push(`/profile/${post?.author?.node?.userId}/posts`)}>
                <img src={post?.author?.node?.profile_picture || defaultAvatar} alt={post?.author?.node?.username} />
              </Avatar>
              <UserInfo>
                <UserName>
                  <strong onClick={() => history.push(`/profile/${post?.author?.node?.userId}/posts`)}>
                    {post?.author?.node?.username}
                  </strong>
                  <span>{moment(post?.date).fromNow()}</span>
                </UserName>
                <p style={post?.type === 'following' ? {color: '#000'} : {color: '#545454'}}>
                  {actionTitle} <a href={post?.postId}>@{post?.title}</a>
                </p>
              </UserInfo>
            </Heading>
            {post?.content && <PostContent>{parse(post?.content)}</PostContent>}
            {post?.galleryImages?.nodes[0]?.sourceUrl && (
              <PostImg>
                <img src={post?.galleryImages?.nodes[0]?.sourceUrl} />
              </PostImg>
            )}
          </Post>
        );
      })}
    </Wrap>
  );
});

RecentNews.defaultProps = {
  news: [],
  title: 'Most Liked',
  loading: false,
  actionTitle: ''
};

RecentNews.propTypes = {
  news: arrayOf(shape({})),
  title: string,
  loading: bool,
  actionTitle: string
};

export default RecentNews;
