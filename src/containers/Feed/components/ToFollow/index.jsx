import React from 'react';
import {arrayOf, string, bool, oneOf, shape} from 'prop-types';

import Icon from 'components/Icon';
import StarRating from 'components/StarRating';
import Button from 'components/Buttons';
import EmptyNewShop from 'components/EmptyNewShop';

import {Shops, Line, Heading, View, FollowingBtn, Followers, RatingWrapper} from './styled';

const ToFollow = ({data, title, loading, type}) => {
  const genericFollowersQty = (qty) => {
    if (qty < 1000) return qty;
    return `${qty / 1000}K`;
  };

  return (
    <Shops>
      <Line>
        <Heading>{title}</Heading>
        <View>
          <div>View All</div>
          <Icon type="arrow" width={15} height={12} color="inherit" />
        </View>
      </Line>
      {data.slice(0, 3).map((el, index) => {
        // if (loading) return <EmptyNewShop />;

        return (
          <Line key={index}>
            <div style={{display: 'flex'}}>
              <img src={el.avatar} alt="avatar" />
              <div>
                <h3>{el.title}</h3>
                {type === 'shop' ? (
                  <RatingWrapper style={{display: 'flex', alignItems: 'center'}}>
                    <StarRating starWidth="8px" starHeight="8px" />
                    <span>(335)</span>
                  </RatingWrapper>
                ) : (
                  <Followers>
                    {genericFollowersQty(el.followers)} {el.followers > 1 ? 'followers' : 'follower'}
                  </Followers>
                )}
              </div>
            </div>
            {el.isFollowing ? (
              <FollowingBtn>
                <Icon type="checkmark" color="currentColor" /> Following
              </FollowingBtn>
            ) : (
              <Button type="follow" />
            )}
          </Line>
        );
      })}
    </Shops>
  );
};

ToFollow.defaultProps = {
  data: [],
  title: 'To Follow',
  type: 'shop',
  loading: false
};

ToFollow.propTypes = {
  data: arrayOf(shape({})),
  title: string,
  type: oneOf(['shop', 'person']),
  loading: bool
};

export default ToFollow;
