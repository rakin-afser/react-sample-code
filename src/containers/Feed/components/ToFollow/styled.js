import styled from 'styled-components/macro';

import {blue, transparentTextColor} from 'constants/colors';

export const Shops = styled.div`
  padding-bottom: 30px;
  background: #fff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
`;

export const Line = styled.div`
  position: relative;
  padding: 24px 16px 6px;
  display: flex;
  align-items: center;
  justify-content: ${({flexStart}) => (flexStart ? 'flex-start' : 'space-between')};
  margin-bottom: 0 !important;

  span {
    font-weight: normal;
    font-size: 12px;
    color: #464646;
    margin-left: 8px;
  }

  img {
    width: 40px;
    height: 40px;
    margin-right: 16px;
  }

  h3 {
    font-weight: normal;
    font-size: 14px;
    margin-bottom: 2px;
  }

  button {
    width: auto;
    min-width: 76px;
    padding: 0 8px;
    text-align: center;
    height: 22px;
    line-height: 20px;
    font-size: 12px;
    flex-shrink: 0;
  }

  &:first-child {
    margin-bottom: 40px;
  }

  &:last-child {
    margin-bottom: 40px;
  }
`;

export const Heading = styled.h2`
  font-family: Helvetica Neue, sans-serif;
  font-weight: 700;
  font-size: 18px;
  line-height: 140%;
  color: #a7a7a7;
  margin: 0;
`;

export const View = styled.div`
  font-family: Helvetica Neue, sans-serif;
  font-size: 12px;
  color: #000;
  fill: #000;
  cursor: pointer;
  display: flex;
  transition: ease 0.4s;

  svg {
    transition: ease 0.4s;
    position: relative;
    top: -1px;

    path {
      fill: #000;
      transition: ease 0.4s;
    }
  }

  &:hover {
    color: ${blue};

    svg {
      transform: scale(1.2) translateX(4px);

      path {
        fill: ${blue};
      }
    }
  }

  i {
    margin: 0 3px;
  }
`;

export const FollowingBtn = styled.button`
  color: ${transparentTextColor};
  border: 1px solid ${transparentTextColor};
  background: #fff;
  cursor: pointer;
  transition: 0.3s;
  border-radius: 22px;

  &:hover {
    background: ${transparentTextColor};
    color: #fff;
  }

  i {
    vertical-align: middle;
    margin-top: -2px;
  }
`;

export const Followers = styled.p`
  font-family: Helvetica Neue, sans-serif;
  font-size: 12px;
  color: #656565;
  margin: 0;
`;

export const RatingWrapper = styled.div`
  svg {
    margin: 0;
  }
`;
