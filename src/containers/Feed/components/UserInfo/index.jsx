import React, {useState} from 'react';

import truncateParagraph from 'util/truncateParagraph';

import Grid from 'components/Grid';
import Icon from 'components/Icon';

import {userExampleData} from 'containers/MyProfile/exampleData';
import EditProfile from 'components/Modals/EditProfile';
import {
  About,
  Avatar,
  AvatarPic,
  Location,
  Name,
  Statistic,
  StatisticCount,
  Description,
  Profession,
  EditAvatar,
  AvatarWrap,
  ButtonsArea,
  Button
} from './styled';

const UserInfo = (data) => {
  const [isLess, setIsLess] = useState(true);
  const [edit, setEdit] = useState(false);
  const [userInfo, setUserInfo] = useState(userExampleData);

  const profile = data.profile;

  return (
    <>
      <About>
        <AvatarWrap>
          <Avatar>
            <AvatarPic src={profile.avatar} />
          </Avatar>
          {profile.editAvatar && (
            <EditAvatar onClick={() => setEdit(true)}>
              <button type="submit">
                <Icon type="pencil" />
              </button>
            </EditAvatar>
          )}
        </AvatarWrap>
        <Name>{profile.name}</Name>
        {profile.location && (
          <Location>
            <Icon type="marker" />
            {profile.location}
          </Location>
        )}
        <Grid sb margin="16px 0 -4px 0">
          <Statistic margin>
            <StatisticCount>{profile.followers}</StatisticCount>
            followers
          </Statistic>
          <Statistic>
            <StatisticCount>{profile.following}</StatisticCount>
            following
          </Statistic>
        </Grid>
        {profile.type === 'seller' ? (
          <ButtonsArea>
            <Button>Go to Store</Button>
          </ButtonsArea>
        ) : (
          <ButtonsArea>
            <Button>Go to profile</Button>
          </ButtonsArea>
        )}
        {profile.profession && <Profession>{profile.profession}</Profession>}
        <Description less={isLess} onClick={() => setIsLess(!isLess)}>
          {isLess ? truncateParagraph(profile.description, 57) : profile.description}
        </Description>
        <EditProfile edit={edit} setEdit={setEdit} userInfo={userInfo} setUserInfo={setUserInfo} />
      </About>
    </>
  );
};

export default UserInfo;
