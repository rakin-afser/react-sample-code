import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const About = styled.div`
  background: #fff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 32px 31px 36px 31px;
`;

export const AvatarWrap = styled.div`
  position: relative;
`;

export const Avatar = styled.div`
  margin-bottom: 16px;
  width: 128px;
  height: 128px;
  border-radius: 50%;
  overflow: hidden;
`;

export const AvatarPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const EditAvatar = styled.i`
  position: absolute;
  bottom: 14px;
  right: 7px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 33px;
  height: 33px;
  background: #ed484f;
  border-radius: 50%;
  border: 3px solid #ffffff;
  transition: ease 0.4s;

  i {
    position: relative;
    left: 1px;
    top: 3px;
  }

  &:hover {
    cursor: pointer;
    background-color: #000;
  }
`;

export const Name = styled.span`
  font-family: SF Pro Display, sans-serif;
  font-weight: 600;
  font-size: 20px;
  line-height: 124%;
  text-align: center;
  letter-spacing: 0.019em;
  color: #000;
  margin: 16px 0 18px 0;
`;

export const Location = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: SF Pro Display, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #ed484f;
  margin: 0 0 26px 0;

  & svg {
    width: 18px;
    height: 20px;
    margin-right: 8px;
  }

  & svg path {
    fill: #ed484f;
  }
`;

export const Statistic = styled.span`
  font-family: SF Pro Display, Regular;
  font-size: 14px;
  line-height: 132%;
  color: #545454;
  margin: ${({margin}) => (margin ? '0 24px 0 0' : '0')};
`;

export const StatisticCount = styled.span`
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  text-align: right;
  letter-spacing: 0.019em;
  color: #000;
  margin: 0 4px 0 0;
`;

export const Profession = styled.span`
  font-family: SF Pro Display, sans-serif;
  font-weight: 600;
  font-size: 16px;
  color: #000;
  margin: 0 0 8px 0;
  text-transform: uppercase;
`;

export const Description = styled.p`
  font-family: SF Pro Display, sans-serif;
  font-size: 13px;
  line-height: 140%;
  text-align: center;
  color: #000;
  cursor: pointer;

  &::after {
    content: ${({less}) => (less ? `'More'` : `'Less'`)};
    color: #ed484f;
    margin-left: 4px;
  }
`;

export const ButtonsArea = styled.div`
  padding: 25px 0;
`;

export const Button = styled.button`
  padding: 2px 27px;
  border: 1px solid #c3c3c3;
  box-sizing: border-box;
  border-radius: 24px;
  font-size: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  color: #545454;
  transition: ease 0.4s;
  background-color: transparent;
  cursor: pointer;

  &:hover {
    background-color: ${primaryColor};
    color: #fff;
  }
`;
