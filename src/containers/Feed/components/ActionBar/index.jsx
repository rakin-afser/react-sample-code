import React, {useState} from 'react';

import CreatePost from 'components/CreatePost';

import {Wrap, AddButton} from './styled';
import Icon from 'components/Icon';

const ActionBar = () => {
  const [visible, setVisible] = useState(false);

  return (
    <>
      <Wrap>
        <AddButton onClick={() => setVisible(true)}>
          <Icon type="plus" color="#fff" width={20} height={20} /> Add a Post
        </AddButton>
      </Wrap>
      <CreatePost visible={visible} onClose={() => setVisible(false)} />
    </>
  );
};

export default ActionBar;
