import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';
import {Button} from 'antd';

export const Wrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  border-radius: 8px;
  margin-bottom: 20px;
  padding: 18px 8px 0;
  width: 100%;

  .ant-btn {
    color: rgba(0, 0, 0, 0.6);
  }
`;

export const AddButton = styled.button`
  padding: 0 20px 0 8px;
  background: ${primaryColor};
  border: none;
  cursor: pointer;
  color: #fff;
  transition: color 0.3s;
  font-size: 14px;
  font-weight: 500;
  border-radius: 17px;
  height: 36px;
  display: flex;
  align-items: center;
  filter: drop-shadow(0px 1.35px 6.75px rgba(237, 72, 79, 0.4));

  svg {
    vertical-align: middle;
    margin-right: 16px;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    color: #000;

    svg {
      path {
        fill: #000;
      }
    }
  }
`;
