import React from 'react';
import {List} from './styled';

const LinksList = () => {
  const links = [
    {id: 1, text: 'About', href: '/about/about-us'},
    {id: 2, text: 'Sell on testSample', href: 'https://seller.testSample.net/my-account/'},
    {id: 3, text: 'Contact', href: '/about/contact-us'},
    {id: 4, text: 'Privacy Policy', href: '/privacy-policy'},
    {id: 5, text: 'Terms', href: '/about/payments'}
  ];

  return (
    <List>
      {links.map((link, index) => (
        <li key={index}>
          <a href={link.href}>{link.text}</a>
        </li>
      ))}
    </List>
  );
};

LinksList.defaultProps = {};

LinksList.propTypes = {};

export default LinksList;
