import styled from 'styled-components';

import {bookmarkFillColor, primaryColor} from 'constants/colors';

export const List = styled.ul`
  li {
    display: inline-block;
    vertical-align: middle;
    margin: 0 6px 6px 0;

    &:not(:first-child) {
      &:before {
        content: '';
        width: 4px;
        height: 4px;
        background: #000;
        border-radius: 50%;
        display: inline-block;
        vertical-align: middle;
        margin: -3px 6px 0 0;
      }
    }

    a {
      color: ${bookmarkFillColor};

      &:hover {
        color: ${primaryColor};
      }
    }
  }
`;
