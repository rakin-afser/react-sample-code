import styled from 'styled-components/macro';
import * as antd from 'antd';
import {mainFont, secondaryFont} from 'constants/fonts';
import {midCoinsColor, primaryColor} from 'constants/colors';
import media from '../../constants/media';

export const StyledModal = styled(antd.Modal)`
  top: 0 !important;

  & .ant-modal-content {
    border-radius: 8px;
  }

  @media (max-width: ${media.mobileMax}) {
    padding: 0 !important;
    margin: 0 !important;
    width: 100% !important;
    top: 0 !important;
    max-width: 100% !important;

    & .ant-modal-content {
      border-radius: 0;
      overflow: hidden;
    }

    & .ant-modal-body {
      padding: 0 !important;
      overflow: hidden !important;
      min-height: 100vh !important;
      height: 100vh !important;
    }
  }
`;

export const Wrapper = styled.div`
  position: relative;

  @media (max-width: ${media.mobileMax}) {
    width: 100%;
  }
`;

export const WrapperInner = styled.div`
  @media (max-width: ${media.mobileMax}) {
    position: fixed;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    display: flex;
    flex-direction: column;
  }
`;

export const WrapperContent = styled.div`
  overflow: auto;
  width: 100%;
`;

export const Header = styled.div`
  margin: 0 auto;
  max-width: 882px;
  width: 100%;
`;

export const Title = styled.div`
  text-align: center;
  color: #000000;
  margin-top: 0;

  h3 {
    margin-bottom: 7px;
    font-size: 18px;
    font-weight: 500;
    line-height: 120%;
    letter-spacing: 0.013em;
  }

  h5 {
    font-size: 14px;
    line-height: 140%;
    color: #666666;
    margin-bottom: 0;
    font-weight: 400;
  }

  @media (max-width: ${media.mobileMax}) {
    padding-top: 12px;

    h3 {
      margin-bottom: 12px;
      letter-spacing: 0;
    }

    h5 {
      display: none;
    }
  }
`;

export const MobileTitle = styled.div`
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  text-align: center;
  color: #666666;

  @media (min-width: 768px) {
    display: none;
  }
`;

export const Step1Wrapper = styled.div`
  transition: ease 0.4s;

  &.exit {
    transform: translateX(-100%);
  }
`;

export const Step2Wrapper = styled.div`
  transition: ease 0.4s;
`;

export const Step = styled.div`
  position: relative;
  text-align: ${({center}) => (center ? 'center' : 'right')};
  color: #000000;
  margin: 17px 0 4px;

  small {
    position: absolute;
    right: -5px;
    top: -43px;
    font-family: ${mainFont};
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    color: #000000;

    span {
      font-size: 18px;
    }

    @media (max-width: ${media.mobileMax}) {
      right: 24px;
      top: -36px;
    }
  }

  div {
    height: 4px;
    background: #efefef;
    border-radius: 4px;
    position: relative;

    &:after {
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      background: ${({background}) => background || '#208C4E'};
      width: ${({progress}) => progress}%;
      border-radius: 4px;
      transition: ease 0.4s;
    }
  }

  @media (max-width: ${media.mobileMax}) {
    margin: 14px 4px 17px;
  }
`;

export const CardGroup = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, ${({width}) => width || '135px'});
  grid-gap: ${({gap}) => gap || '7px 48px'};
  align-self: center;
  justify-content: center;
  margin: 23px 0 0;

  @media (max-width: ${media.mobileMax}) {
    padding: 16px 8px 86px;
    grid-template-columns: repeat(auto-fit, 80px);
    margin: 0;
  }
`;

export const CardGroupStores = styled.div`
  padding-top: 25px;
  padding-bottom: 25px;
  display: grid;
  grid-template-columns: repeat(auto-fit, 191px);
  grid-gap: 36px;
  justify-content: center;

  @media (max-width: ${media.mobileMax}) {
    grid-template-columns: repeat(auto-fit, 152px);
    grid-gap: 30px;
    padding-bottom: 86px;
  }
`;

export const CardOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: ${({selected}) => (selected ? 1 : 0)};
  transition: opacity ease 0.3s;
  background-color: rgba(0, 0, 0, 0.44);
  z-index: 0;

  i {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
`;

export const CardWrap = styled.div`
  width: 145px;
  margin-bottom: 20px;
  border: none;
  background: #ffffff;
  border-radius: 0;

  @media (min-width: 768px) {
    &:hover {
      cursor: pointer;

      ${CardOverlay} {
        opacity: 1;
      }
    }
  }

  @media (max-width: ${media.mobileMax}) {
    width: 85px;
  }
`;

export const CardImageWrapper = styled.div`
  position: relative;
  margin-bottom: 10px;
  width: 100%;
  height: 130px;
  background: #ededed;
  border-radius: 12px;
  overflow: hidden;
  z-index: 1;

  @media (max-width: ${media.mobileMax}) {
    height: 85px;
  }
`;

export const CardImage = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const CardTitle = styled.h4`
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  color: #000000;
  margin: 0;
`;

export const CardFollowers = styled.h5`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #8f8f8f;
  flex: none;
  order: 0;
  align-self: center;
  margin: 6px 0 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Actions = styled.div`
  padding-top: 16px;
  margin: 0 auto;
  max-width: 882px;
  display: flex;
  justify-content: space-between;

  @media (max-width: ${media.mobileMax}) {
    width: 100%;
    position: fixed;
    margin: 0 0 25px 0;
    justify-content: center;
    bottom: 0;
    left: 0;
    right: 0;
  }
`;

export const Button = styled.button`
  margin-left: auto;
  padding: 0 24px 0 44px;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  display: flex;
  height: 48px;
  align-items: center;
  text-align: center;
  letter-spacing: 0.019em;
  color: #ffffff;
  background: #000;
  border-radius: 24px;
  transition: ease 0.4s;

  &:disabled {
    background: #cccccc;
    border-radius: 24px;
  }

  &:not(:disabled) {
    &:hover {
      background-color: ${midCoinsColor};
    }
  }

  i {
    margin-left: 13px;
  }

  @media (max-width: ${media.mobileMax}) {
    width: 319px;
    height: 51px;
    text-align: center;
    justify-content: center;
    margin: 0 auto;
  }
`;

export const BackBtn = styled.button`
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  display: flex;
  align-items: center;
  text-align: center;
  letter-spacing: 0.019em;
  color: #666666;
  transition: ease 0.4s;

  i {
    margin-right: 10px;
    transform: rotate(-180deg);

    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    color: ${primaryColor};

    i {
      svg {
        path {
          fill: ${primaryColor};
        }
      }
    }
  }

  @media (max-width: ${media.mobileMax}) {
    display: none;
  }
`;

export const MobileBackBtn = styled.button`
  position: absolute;
  left: 10px;
  top: 8px;
`;

export const NextButton = styled(antd.Button)`
  width: 72px;
  height: auto !important;
  font-weight: 500 !important;
  font-size: 16px !important;
  line-height: 140% !important;
  color: #000000 !important;
  padding: 0 !important;
  justify-self: right;

  div {
    border: 3px solid #ed484f;
    box-sizing: border-box;
    border-radius: 100%;
    width: 47px;
    height: 47px;
    display: flex;
    justify-content: center;
    align-items: center;
    margin: 0 auto 10px;
  }

  svg {
    transform: ${({next}) => (next ? 'rotate(180deg)' : '')};
  }
`;

export const MobileInterestsCard = styled.div`
  display: flex;
  align-items: center;
  background: ${({follow}) => (follow ? '#efefef' : '#fff')};
  border-radius: 4px;
  margin: 0px 29px 15px 16px;
  border: ${({follow}) => (follow ? '1px solid #efefef' : '1px solid #E4E4E4')};
  box-sizing: border-box;

  .img {
    margin: 6px;
    z-index: 100000000;
  }

  .desc {
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: 3px;

    strong {
      font-family: SF Pro Display;
      font-weight: 500;
      line-height: 140%;
      color: #000000;
    }

    small {
      font-size: 10px;
      line-height: 132%;
      color: #8f8f8f;
    }
  }

  .follow {
    fill: ${({follow}) => (follow ? '#ED484F' : 'none')};
    margin: 24px 14px 24px auto;
    stroke: ${({follow}) => (follow ? 'none' : '#8F8F8F')};
  }
`;
