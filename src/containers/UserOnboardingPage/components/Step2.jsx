import React from 'react';
import PropTypes from 'prop-types';

import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';
import Error from 'components/Error';
import Card from 'components/Cards/Store';

import {Step2Wrapper, CardGroupStores} from '../styled';

const Step2 = ({isMobile, data, loading, error}) => {
  const renderSkeleton = (count) => {
    return (
      <>
        {Array.from(Array(count).keys()).map((elm, idx) => (
          <SkeletonTheme key={idx}>
            <Skeleton height={60} />
            <p style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
              <Skeleton circle width={50} height={50} />
              <Skeleton width={90} height={20} />
            </p>
            <p>
              <Skeleton count={2} />
            </p>
          </SkeletonTheme>
        ))}
      </>
    );
  };

  if (error) {
    return <Error />;
  }

  return (
    <Step2Wrapper>
      <div>
        <CardGroupStores>
          {loading
            ? renderSkeleton(8)
            : data?.stores?.nodes?.map((item) => <Card mini overlay key={item.id} data={item} />)}
        </CardGroupStores>
      </div>
    </Step2Wrapper>
  );
};

Step2.defaultProps = {
  isMobile: false
};

Step2.propTypes = {
  isMobile: PropTypes.bool
};

export default Step2;
