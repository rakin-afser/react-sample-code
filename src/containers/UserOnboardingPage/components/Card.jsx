import React from 'react';
import {CardWrap, CardImageWrapper, CardImage, CardOverlay, CardTitle, CardFollowers} from '../styled';
import Icon from 'components/Icon';
import {useFollowUnfollowInterestMutation} from 'hooks/mutations';

const Card = (props) => {
  const {id, databaseId, featuredImage, title, isFollowed, totalFollowers} = props;
  const [followInterest] = useFollowUnfollowInterestMutation({id, isFollowed});
  return (
    <CardWrap onClick={() => followInterest({variables: {input: {id: databaseId}}})} key={databaseId}>
      <CardImageWrapper>
        <CardImage src={featuredImage?.node?.sourceUrl} />
        <CardOverlay selected={isFollowed}>
          <Icon type="checkmark" color="#fff" height={32} width={32} />
        </CardOverlay>
      </CardImageWrapper>
      <CardTitle>{title}</CardTitle>
      <CardFollowers>{totalFollowers} followers</CardFollowers>
    </CardWrap>
  );
};

export default Card;
