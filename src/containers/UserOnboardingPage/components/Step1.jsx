import React from 'react';
import {func} from 'prop-types';
import {useWindowSize} from '@reach/window-size';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';
import Error from 'components/Error';
import {CardGroup, Step1Wrapper, CardWrap} from '../styled';
import Card from './Card';

const Step1 = ({interestsData, interestsError, interestsLoading}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  const renderSkeleton = (quantity) => {
    return Array.from(Array(quantity).keys()).map((elm, idx) => (
      <SkeletonTheme key={idx}>
        <CardWrap>
          <Skeleton width={isMobile ? 80 : 140} height={isMobile ? 80 : 120} />
          <Skeleton width={isMobile ? 80 : 140} height={isMobile ? 10 : 20} />
          <Skeleton width={isMobile ? 80 : 140} height={isMobile ? 10 : 20} />
        </CardWrap>
      </SkeletonTheme>
    ));
  };

  const renderContent = () => {
    return interestsData?.interests?.nodes.map((item) => {
      return <Card key={item?.id} {...item} />;
    });
  };

  if (interestsError) {
    return <Error />;
  }

  return (
    <Step1Wrapper>
      <div>
        <CardGroup>{interestsLoading ? renderSkeleton(10) : renderContent()}</CardGroup>
      </div>
    </Step1Wrapper>
  );
};

Step1.propTypes = {
  onFollowInterest: func.isRequired
};

export default Step1;
