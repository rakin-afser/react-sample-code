import React, {useState} from 'react';
import {bool, func} from 'prop-types';
import Scrollbars from 'react-scrollbars-custom';
import {useQuery, gql} from '@apollo/client';
import Icons from 'components/Icon';
import Step1 from './components/Step1';
import Step2 from './components/Step2';
import {GET_INTERESTS} from 'queries';
import {
  Wrapper,
  Actions,
  Button,
  BackBtn,
  MobileBackBtn,
  Title,
  Step,
  Header,
  MobileTitle,
  WrapperInner,
  WrapperContent
} from './styled';
import FadeSwitch from 'containers/Animations/FadeSwitch';

const GET_STORES = gql`
  query getStores {
    stores {
      nodes {
        gravatar
        banner
        id
        totalFollowers
        followed
        name
        storeUrl
        rating
      }
    }
  }
`;

const UserOnboarding = ({isMobile, onClose}) => {
  const {data: storesData, loading: storesLoading, error: storesError} = useQuery(GET_STORES);
  const {data: interestsData, loading: interestsLoading, error: interestsError} = useQuery(GET_INTERESTS);
  const [step, setStep] = useState(1);

  const nextStep = () => {
    if (step === 1) {
      setStep(2);
    } else {
      onClose();
    }
  };

  const getFollowedStores = () => {
    let totalFollowed = 0;

    if (storesData) {
      storesData.stores.nodes.map((el) => {
        if (el?.followed) {
          totalFollowed += 1;
        }

        return null;
      });
    }

    return totalFollowed;
  };

  const getSelectecInterests = () => {
    let total = 0;

    if (interestsData) {
      interestsData.interests.nodes.map((el) => {
        if (el?.isFollowed) {
          total += 1;
        }

        return null;
      });
    }

    return total;
  };

  const disabledContinueBtn = () => {
    if ((step === 1 && getSelectecInterests() >= 3) || (step === 2 && getFollowedStores() >= 3)) {
      return false;
    }
    return true;
  };

  function renderContent() {
    const markUp = (
      <>
        <MobileTitle>This will help us build a custom feed for you</MobileTitle>
        <FadeSwitch state={step}>
          {step === 1 ? (
            <Step1 interestsData={interestsData} interestsLoading={interestsLoading} />
          ) : (
            <Step2
              data={storesData}
              loading={storesLoading}
              error={storesError}
              isMobile={isMobile}
              onClose={onClose}
            />
          )}
        </FadeSwitch>
      </>
    );

    if (isMobile) {
      return <WrapperContent>{markUp}</WrapperContent>;
    }

    return (
      <Scrollbars
        clientWidth={4}
        noDefaultStyles={false}
        noScroll={false}
        style={{height: '470px', width: '100%'}}
        thumbYProps={{className: 'thumbY'}}
      >
        {markUp}
      </Scrollbars>
    );
  }

  return (
    <Wrapper>
      <WrapperInner>
        <Header>
          {isMobile && step === 2 && (
            <MobileBackBtn onClick={() => setStep(1)}>
              <Icons type="arrowBack" color="#000" width={26} height={26} />
            </MobileBackBtn>
          )}
          <Title>
            <h3>{step === 1 ? 'Pick at least 3 Interests' : 'Follow at least 3 Stores'}</h3>
            <h5>This will help us build a custom feed for you</h5>
          </Title>
          <Step progress={step === 1 ? 50 : 100} background="#000000" center>
            <small>
              <span>{step === 1 ? 1 : 2}</span>/2
            </small>
            <div />
          </Step>
        </Header>
        {renderContent()}
      </WrapperInner>
      <Actions>
        {step === 2 && (
          <BackBtn onClick={() => setStep(1)}>
            <Icons type="arrow" color="#000" width={15} height={15} />
            Back
          </BackBtn>
        )}
        <Button onClick={nextStep} disabled={disabledContinueBtn()}>
          Continue
          <Icons type="arrow" color="#fff" width={15} height={15} />
        </Button>
      </Actions>
    </Wrapper>
  );
};

UserOnboarding.propTypes = {
  isMobile: bool.isRequired,
  onClose: func.isRequired
};

export default UserOnboarding;
