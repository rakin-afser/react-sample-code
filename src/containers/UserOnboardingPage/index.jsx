import React, {useState} from 'react';
import {useWindowSize} from '@reach/window-size';

import HomePage from 'containers/HomePage';
import UserOnboarding from './UserOnboarding';
import {StyledModal} from './styled';
import {useHistory} from 'react-router';

const UserOnboardingPage = () => {
  const [displaying, setDisplaying] = useState(true);
  const {width} = useWindowSize();
  const {push} = useHistory();
  const isMobile = width <= 767;

  const onClose = () => {
    setDisplaying(false);
    if (isMobile) {
      push('/');
    }
  };

  return (
    <>
      {isMobile ? (
        <UserOnboarding onClose={onClose} isMobile={isMobile} />
      ) : (
        <>
          <HomePage />
          <StyledModal
            afterClose={() => push('/')}
            visible={displaying}
            onCancel={onClose}
            width={1010}
            centered
            footer={null}
            destroyOnClose
            bodyStyle={{
              padding: '22px 0 22px',
              minHeight: '451px'
            }}
            closeIcon={
              <div
                style={{
                  display: 'none'
                }}
              >
                &nbsp;
              </div>
            }
          >
            <UserOnboarding onClose={onClose} isMobile={isMobile} />
          </StyledModal>
        </>
      )}
    </>
  );
};

export default UserOnboardingPage;
