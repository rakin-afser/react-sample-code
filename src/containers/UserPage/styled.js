import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';

export const PageWrapper = styled.div`
  max-width: 100vw;
  display: flex;
  align-items: flex-start;

  @media (max-width: 1000px) {
    padding: 0 10px;
  }
`;

export const Content = styled.div`
  width: 576px;
  margin: 0 16px;

  @media (max-width: 1200px) {
    width: 100%;
  }
`;

export const LeftSide = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: sticky;
  top: 34px;
  width: 300px;
  height: fit-content;
  display: flex;
  flex-direction: column;
  align-items: stretch;
  min-width: 300px;

  & > div {
    width: 100%;
    margin-bottom: 24px;
  }
`;

export const RightSide = styled.div`
  min-width: 290px;
  position: sticky;
  top: 34px;
`;

export const Nav = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
`;

export const Tab = styled(Link)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  border: none;
  cursor: pointer;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 16px;
  color: #7a7a7a;
  box-sizing: border-box;
  outline: none;
  padding: 12px 0 4px;
  margin: 0 65px;
  border-bottom: 2px solid transparent;
  transition: color ease-in-out 0.4s, border-color ease-in-out 0.4s;

  ${({active}) =>
    active && {
      color: '#000000',
      borderBottom: '2px solid #000000'
    }}

  &:hover {
    color: #000000;
    border-bottom: 2px solid #000000;
  }
`;
