import React from 'react';
import {useParams} from 'react-router-dom';
import {useWindowSize} from '@reach/window-size';

import shopImage from 'images/shops/shop2.jpg';
import UserPageDesktop from 'containers/UserPage/UserPageDesktop';
import UserPageMobile from './containers/UserPage/mobile';
import Layout from 'containers/Layout';
import userAvatar from 'components/Post/img/kathryn_mccoy.png';

const user = {
  avatar: userAvatar,
  firstName: 'Ariana',
  lastName: 'Grande',
  alias: 'kathryn_mccoy',
  shopName: 'Hazel & Glory',
  shopPic: shopImage,
  seller: true,
  followers: '3k',
  following: 999,
  location: 'Kiev, Ukraine',
  profession: 'singer',
  description: `Trendy overview for last season, tenden collaboration,
    FW The magazine covers international, national and local
    fashion and beauty trends and news. Lorem ipsum dolor sit
    amet, consectetur adipiscing elit. Nulla nec nibh sit amet
    augue sollicitudin facilisis sit amet in elit. Aliquam erat
    volutpat. Ut nulla erat, malesuada non nunc et, cursus consequat
    ligula. Maecenas eu mattis ex, eget fringilla sem.`
};

const UserPage = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const {userName} = useParams();

  return (
    <Layout isFooterShort showBottomNav={isMobile} hideFooter={isMobile} showBasketButton={false}>
      {isMobile ? <UserPageMobile /> : <UserPageDesktop user={user} user={{...user, alias: userName}} page={'lists'} />}
    </Layout>
  );
};

export default UserPage;
