import React from 'react';
import {useParams} from 'react-router-dom';
import {useWindowSize} from '@reach/window-size';

import Grid from 'components/Grid';
import LeaveFeedback from 'components/Modals/LeaveFeedback';
import useGlobal from 'store';
import List from 'containers/MyProfile/components/List';
import UserInfo from 'components/UserInfo/GuestView';
import Lists from 'containers/MyProfile/components/Lists';
import ToFollow from 'containers/Feed/components/ToFollow';
import {Content as ContentStyled, Nav, Tab, PageWrapper, LeftSide, RightSide} from './styled';

import avatar from 'images/avatar.png';
import avatar1 from 'images/avatar2.png';
import avatar2 from 'images/avatar3.png';

const peopleToFollow = [
  {avatar: avatar, title: 'anna_stone', isFollowing: false, followers: 100000},
  {avatar: avatar1, title: 'simona_sarabu', isFollowing: true, followers: 80000},
  {avatar: avatar2, title: 'selia_smith', isFollowing: false, followers: 120000}
];

const shopsToFollow = [
  {avatar: avatar, title: 'Luis Vuitton & ...', isFollowing: false},
  {avatar: avatar1, title: 'Dimino Shop', isFollowing: false},
  {avatar: avatar2, title: 'Bell & D', isFollowing: false}
];

const UserPageDesktop = ({page = 'lists', user}) => {
  const {width} = useWindowSize();
  const [globalState] = useGlobal();
  const activeTab = page;
  const {userName, slug} = useParams();

  return (
    <Grid pageContainer>
      <PageWrapper>
        <LeftSide>
          <UserInfo user={user} />
          {!slug && <ToFollow data={peopleToFollow} title="People to follow " type="person" />}
          {(width <= 1200 || !!slug) && <ToFollow data={shopsToFollow} title="Shops to follow" type="shop" />}
        </LeftSide>
        {!slug && (
          <ContentStyled>
            <Nav>
              <Tab active={activeTab === 'posts'} to={`/user/${userName}/lists${!!slug ? `/${slug}` : ''}`}>
                Posts
              </Tab>
              <Tab active={activeTab === 'lists'} to={`/user/${userName}/lists${!!slug ? `/${slug}` : ''}`}>
                Lists
              </Tab>
            </Nav>
            {activeTab === 'lists' && <Lists isGuest />}
          </ContentStyled>
        )}
        {!!slug && <List isGuest />}
        {width > 1200 && !slug && (
          <RightSide>
            <ToFollow data={shopsToFollow} title="Shops to follow" type="shop" />
          </RightSide>
        )}
      </PageWrapper>
      <LeaveFeedback displaying={globalState.leaveFeedback.open} />
    </Grid>
  );
};

export default UserPageDesktop;
