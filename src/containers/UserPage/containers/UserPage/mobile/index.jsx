import React from 'react';
import AccountHeader from 'components/AccountHeader';
import {userProfile} from 'constants/staticData';
import ShopsToFollow from 'components/ShopsToFollow/mobile';
import {Wrap, StyledTabs} from './styled';
import List from 'components/List';
import {Tabs} from 'antd';

const {TabPane} = Tabs;

const UserPage = () => {
  return (
    <>
      <AccountHeader user={userProfile} />
      <StyledTabs defaultActiveKey="2">
        <TabPane tab="Posts" key="1">
          Posts
        </TabPane>
        <TabPane tab="Lists" key="2">
          <Wrap>
            {Array(3)
              .fill()
              .map(() => (
                <List />
              ))}
            <ShopsToFollow />
            <List />
          </Wrap>
        </TabPane>
      </StyledTabs>
    </>
  );
};

export default UserPage;
