import styled from 'styled-components';
import {Tabs} from 'antd';

export const Wrap = styled.div`
  padding: 8px 16px 0;
`;

export const StyledTabs = styled(Tabs)`
  &.ant-tabs-top {
    margin-top: 24px;
    border-top: 8px solid #fafafa;
  }

  .ant-tabs {
    &-tabpane {
      background-color: #fafafa;
    }

    &-bar {
      margin-bottom: 0;
    }

    &-ink-bar {
      background-color: #ed484f;
    }

    &-bar {
      border: none;
      position: relative;

      &:after {
        content: '';
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        border-bottom: 2px solid #efefef;
      }
    }

    &-nav {
      min-width: 100vw;
    }
  }

  &&& {
    .ant-tabs {
      &-tab {
        margin-right: 0;
        min-width: 94px;
        text-align: center;
        padding: 9.5px 0;
        width: 33.333vw;
        color: #999;

        &.ant-tabs-tab-active {
          color: #000;
          font-weight: 700;
        }
      }
    }
  }
`;
