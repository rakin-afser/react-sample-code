import styled from 'styled-components';

export const Wrap = styled.div`
  padding: 16px 0;
  border: 1px solid #eeeeee;
  margin-bottom: 76px;
  border-radius: 4px;
  background-color: #fff;
`;

export const InnerWrap = styled.div`
  padding: 12px 16px 0;
  position: relative;
`;

export const Options = styled.div`
  position: absolute;
  right: 18px;
  top: 18px;
  display: flex;
  align-items: center;

  i + i {
    margin-left: 10px;
  }
`;

export const ItemsWrap = styled.div`
  display: flex;
  overflow: auto;
  padding-left: 16px;
  padding-right: 16px;
  max-width: 100%;
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */

  &::-webkit-scrollbar {
    display: none;
  }
`;

export const Items = styled.div`
  display: flex;
`;

export const Item = styled.div`
  background-size: cover;
  border-radius: 8px;
  border: 1px solid #efefef;
  min-width: 82px;
  height: 135px;
  margin-right: 8px;

  &:last-child {
    margin-right: 16px;
  }

  &:nth-child(3n + 2) {
    background-color: #d2e4e6;
  }
  &:nth-child(3n) {
    background-color: #efe7ec;
  }
  &:nth-child(3n + 1) {
    background-color: #e9e7ef;
  }
`;

export const Title = styled.h4`
  font-size: 18px;
  color: #000000;
  margin-bottom: 10px;
`;

export const Stats = styled.p`
  color: #333;
  line-height: 1.333;
  margin-bottom: 0;

  span {
    font-weight: 500;
  }

  & + & {
    margin-left: 20px;
  }
`;
