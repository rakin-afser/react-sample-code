import React from 'react';
import {Wrap, ItemsWrap, Items, Item, Title, InnerWrap, Stats, Options} from './styled';
import Grid from 'components/Grid';
import product1 from 'images/products/product1.jpg';
import Icon from 'components/Icon';
import useGlobal from 'store';

const List = () => {
  const [, globalActions] = useGlobal();

  const buttons = [
    {
      title: 'Save'
    },
    {
      title: 'Share',
      onClick: async () => {
        const shareData = {
          title: 'testSample',
          text: 'Share',
          url: window.location.href
        };

        try {
          if (navigator.share) {
            await navigator.share(shareData);
          }
        } catch (err) {
          console.log(err);
        }
      }
    },
    {
      title: 'Report',
      warn: true,
      onClick: () => {
        globalActions.setOptionsPopup({active: false});
        globalActions.setReportPopup({active: true});
      }
    }
  ];

  return (
    <Wrap>
      <ItemsWrap>
        <Items>
          {Array(10)
            .fill(1)
            .map((_, i) => (
              <Item style={i === 0 ? {backgroundImage: `url(${product1})`} : {}}></Item>
            ))}
        </Items>
      </ItemsWrap>
      <InnerWrap>
        <Options>
          <Icon type="eye" />
          <Icon onClick={() => globalActions.setOptionsPopup({active: true, buttons})} type="dots" />
        </Options>
        <Title>MakeUp</Title>
        <Grid>
          <Stats>
            <span>3k</span> Followers
          </Stats>
          <Stats>
            <span>340</span> Views
          </Stats>
        </Grid>
      </InnerWrap>
    </Wrap>
  );
};

export default List;
