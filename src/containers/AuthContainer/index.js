import React, {useEffect} from 'react';
import {Redirect} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';
import {useLocation, useHistory} from 'react-router-dom';
import {useWindowSize} from '@reach/window-size';
import SignInPage from '../SignInPage';
import SignUpPage from '../SignUpPage';
import AuthPopupContainer from '../../components/UserAuth/components/AuthPopupContainer';
import HomePage from '../HomePage';
import useGlobal from 'store';
import ResetPasswordPage from '../ResetPasswordPage';
import RecoverUsernamePage from '../RecoverUsernamePage';
import VerifyOPTPage from '../VerifyOPTPage';

const childRoutes = [
  {path: '/auth/sign-in', name: 'SignIn', Component: SignInPage},
  {path: '/auth/sign-up', name: 'SignUp', Component: SignUpPage},
  {path: '/auth/reset-password', name: 'Reset Password', Component: ResetPasswordPage},
  {path: '/auth/recover-username', name: 'Recover Username', Component: RecoverUsernamePage},
  {path: '/auth/otp-verification', name: 'VerifyOTP', Component: VerifyOPTPage}
];

const AuthContainer = () => {
  const [, globalActions] = useGlobal();
  const {pathname} = useLocation();
  const {push} = useHistory();
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  useEffect(() => {
    globalActions.setAuthPopup(true);
  }, []);

  useEffect(() => {
    if (pathname === '/auth') push('/auth/sign-in');
  }, [pathname]);

  const onClickClose = () => {
    globalActions.setAuthPopup(false);
  };

  const generateChildRoutes = (routes) => (
    <Switch>
      <Redirect exact path="/auth" />
      {routes.map((route) => (
        <Route exact path={route.path} key={route.name}>
          {<route.Component onClickClose={onClickClose} />}
        </Route>
      ))}
    </Switch>
  );

  return (
    <>
      <HomePage />
      <AuthPopupContainer showBackDrop={!isMobile}>{generateChildRoutes(childRoutes)}</AuthPopupContainer>
    </>
  );
};

export default AuthContainer;
