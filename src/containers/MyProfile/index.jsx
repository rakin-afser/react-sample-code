import React, {useEffect, lazy} from 'react';
import {useWindowSize} from '@reach/window-size';
import {useHistory, useParams, useLocation, Redirect} from 'react-router-dom';

// import Wishlist from 'containers/MyProfile/MyProfileMobile/components/Wishlist';
import Layout from 'containers/Layout';
import {useUser} from 'hooks/reactiveVars';

const MyProfileDesktop = lazy(() => import('containers/MyProfile/Buyer/MyProfileDesktop'));
const Profile = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Profile'));
const Orders = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Orders'));
const Settings = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Settings'));
const Edit = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Edit'));
const Rewards = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Rewards'));
const Messages = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Messages'));
const Posts = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Posts'));
const Promote = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Promote'));
const Likes = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Likes'));
const ProfileInfluencer = lazy(() => import('containers/MyProfile/Influencer'));
const Guest = lazy(() => import('containers/MyProfile/MyProfileMobile/components/Guest'));

const withoutHeader = ['messages', 'notifications', 'promote', 'mylikes'];
const withBack = ['settings', 'edit', 'rewards', 'orders', 'account-settings'];
const withHamburger = ['lists', 'posts'];
const withTitle = {
  posts: 'My Profile',
  lists: 'My Profile',
  settings: 'My Account',
  edit: 'Edit profile',
  orders: 'My Orders',
  rewards: 'My Rewards',
  statistic: 'Rewards', // ToDo: Find out better way to add title for slug
  transactions: 'Transactions',
  shopcoins: 'Use Your Shop Coins',
  'what-is': 'What is MidCoins',
  referral: 'Referral'
};

const MyProfile = () => {
  const history = useHistory();
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const {pathname} = useLocation();
  const page = useParams()?.page || pathname?.split('/')?.pop();
  const {slug, userParam} = useParams();
  const influencer = pathname.match('influencer');
  const [user] = useUser();

  useEffect(() => {
    if (!user?.loading && !user?.databaseId && !userParam) {
      // if user unauthorized and try to get access to profile
      // !fix src/containers/AuthProvider.jsx for get loading!
      // history.push('/auth');
    }
  }, [user, userParam]);

  const getMyProfileType = () => {
    if (influencer) {
      return <ProfileInfluencer page={page || 'products'} />;
    }

    return <MyProfileDesktop page={page || 'profile'} />;
  };

  const getMobilePage = () => {
    switch (page) {
      case 'settings':
        return <Settings />;
      case 'edit':
        return <Edit />;
      case 'orders':
        return <Orders />;
      case 'rewards':
        return <Rewards />;
      case 'messages':
      case 'notifications':
        return <Messages page={page} />;
      case 'posts':
      case 'lists':
        // if (slug === 'wishlist') {
        //   return <Wishlist />;
        // }
        return <Posts page={page} user={user} />;
      case 'guest':
        return <Guest />;
      case 'promote':
        return <Promote />;
      case 'mylikes':
        return <Likes />;
      case 'profile':
      default:
        return <Profile user={user} />;
    }
  };

  const showBottomNav = () => {
    switch (true) {
      case isMobile && page === 'messages':
      case isMobile && page === 'notifications':
      case isMobile && page === 'posts' && slug === 'new':
        return false;
      case isMobile:
        return true;
      default:
        return false;
    }
  };

  return (
    <Layout
      title={withTitle[slug || page]}
      showBasketButton={false}
      showSearchButton={false}
      showBackButton={isMobile && withBack.includes(page)}
      showBurgerButton={isMobile && withHamburger.includes(page)}
      isFooterShort
      showBottomNav={showBottomNav()}
      hideFooter={isMobile}
      hideHeader={isMobile && withoutHeader.includes(page)}
    >
      {isMobile ? getMobilePage() : getMyProfileType()}
    </Layout>
  );
};

export default MyProfile;
