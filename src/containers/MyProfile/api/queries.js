import {gql} from '@apollo/client';

export const ListFragment = gql`
  fragment ListFragment on ListType {
    user_id
    totalFollowers
    list_name
    list_id
    list_description
    is_private
    # hashtags #disable for now
    addedProducts {
      nodes {
        onSale
        image {
          altText
          sourceUrl
        }
        ... on VariableProduct {
          databaseId
          name
          price
          salePrice
          regularPrice
          shippingType
          slug
          isLiked
          totalLikes
        }
        ... on SimpleProduct {
          databaseId
          name
          price
          salePrice
          regularPrice
          shippingType
          slug
          isLiked
          totalLikes
        }
      }
    }
  }
`;

export const GET_LIST = gql`
  ${ListFragment}
  query GetList($id: Int!) {
    getList(list_id: $id) {
      ...ListFragment
    }
  }
`;

export const GET_LISTS = gql`
  ${ListFragment}
  query GetLists($where: ListSearchWhere) {
    lists(first: 1, where: $where) {
      nodes {
        ...ListFragment
      }
    }
  }
`;

export const GET_REWARDS = gql`
  query RewardsData(
    $userId: Int!
    $search: String
    $sortBy: RewardOrderBy
    $orderBy: RewardOrderBySort
    $offset: Int
    $limit: Int
  ) {
    testSampleRewardData(
      user_id: $userId
      searchBy: $search
      sortBy: $sortBy
      orderBy: $orderBy
      offset: $offset
      limit: $limit
    ) {
      total
      nodes {
        id
        date
        status
        type
        awarded_qty
        order_id
        store_id
        expiry_date
        awardedProduct {
          nodes {
            name
            slug
            id
            databaseId
            currencySymbol
            image {
              sourceUrl(size: THUMBNAIL)
              id
            }
            seller {
              name
              id
            }
          }
        }
        product_id
      }
    }
  }
`;

export const GET_REWARDS_STATISTICS = gql`
  query GetRewards($userId: Int!) {
    rewardsData(user_id: $userId) {
      earnedMidcoinsInMonth {
        total
        onReviews
        onReferrals
        onLists
        inDays {
          date
          midcoins
        }
      }
      earnedMidcoinsInWeek {
        total
        onReferrals
        onReviews
        inDays {
          date
          midcoins
        }
        onLists
      }
      earnedMidcoinsInYear {
        total
        onLists
        onReferrals
        onReviews
        inDays {
          date
          midcoins
        }
      }
    }
  }
`;
