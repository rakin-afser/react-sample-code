import React, {useState} from 'react';
import Dashboard from 'containers/MyProfile/Influencer/InfluencerDashboard/components/Dashboard';
import Activity from 'containers/MyProfile/components/Activity';
import TransactionsTable from 'containers/MyProfile/components/TransactionsTable';
import {Content, TabsWrapper, Nav, Tab} from './styled';

const InfluencerDashboard = () => {
  const tabs = ['Dashboard', 'Transactions', 'Activity'];
  const [activeTabIndex, setActiveTabIndex] = useState(0);

  const getActiveTab = (name) => {
    switch (name) {
      case 0:
        return <Dashboard />;
      case 1:
        return <TransactionsTable />;
      case 2:
        return <Activity />;
      default:
        return <Dashboard />;
    }
  };

  return (
    <Content>
      <TabsWrapper>
        <Nav>
          {tabs.map((tab, i) => (
            <Tab active={activeTabIndex === i} key={i} onClick={() => setActiveTabIndex(i)}>
              {tab}
            </Tab>
          ))}
        </Nav>
      </TabsWrapper>
      {getActiveTab(activeTabIndex)}
    </Content>
  );
};

export default InfluencerDashboard;
