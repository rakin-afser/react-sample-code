import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Icons from 'components/Icon';
import numberWithCommas from 'util/heplers';
import {Wrapper, Title, Header, HeaderInner, Button, List, ListItem, ListItemInner, Footer} from './styled';

const StatsPanel = ({data, title, footerTitle, withList}) => {
  const [sortAsc, setSortAcs] = useState(true);
  const [list, setList] = useState(() => sortAscFnc(data));

  function sortAscFnc(array) {
    return array.sort((a, b) => (a.count > b.count ? 1 : -1));
  }

  function sortDescFnc(array) {
    return array.sort((a, b) => (a.count < b.count ? 1 : -1));
  }

  const onListSort = () => {
    setSortAcs((prev) => !prev);

    !sortAsc ? setList(sortAscFnc(list)) : setList(sortDescFnc(list));
  };

  const getSum = (array) => {
    let newArr = 0;

    array.map((el) => {
      newArr += parseFloat(el.count);
      return null;
    });

    return newArr;
  };

  return (
    <Wrapper>
      <Header>
        <HeaderInner>
          <Title>{title}</Title>
          <Button sortAsc={sortAsc} onClick={onListSort}>
            Last 7 Days
            <Icons type="arrow" width={10} height={10} color="#000" />
          </Button>
        </HeaderInner>
      </Header>
      <List reversed={!sortAsc} withList={withList}>
        {list?.map((el, key) => (
          <ListItem withList={withList} key={key}>
            <ListItemInner>
              {el.title}{' '}
              <span>
                {withList && 'BD'} {numberWithCommas(el.count.toFixed(2))}
              </span>
            </ListItemInner>
          </ListItem>
        ))}
      </List>
      <Footer>
        <Title>{footerTitle}</Title>
        <Title>
          {withList && <span>BD </span>}
          {numberWithCommas(getSum(data).toFixed(2))}
        </Title>
      </Footer>
    </Wrapper>
  );
};

StatsPanel.defaultProps = {
  withList: false
};

StatsPanel.propTypes = {
  title: PropTypes.string.isRequired,
  footerTitle: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  withList: PropTypes.bool
};

export default StatsPanel;
