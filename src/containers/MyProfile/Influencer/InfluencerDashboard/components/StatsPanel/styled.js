import styled from 'styled-components';
import {midCoinsColor, secondaryTextColor} from 'constants/colors';

export const Wrapper = styled.div`
  max-width: 426px;
  width: 100%;
  background: #ffffff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
`;

export const Title = styled.h4`
  font-weight: 500;
  font-size: 18px;
  letter-spacing: -0.024em;
  color: #000000;

  span {
    font-weight: 400;
    font-size: 14px;
    color: #464646;
  }
`;

export const Header = styled.header`
  padding: 15px 26px;
`;

export const HeaderInner = styled.div`
  border-bottom: 1px solid #000;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Button = styled.button`
  font-size: 12px;
  padding: 0;
  transition: ease 0.6s;

  i {
    position: relative;
    top: 2px;
    margin-left: 4px;
    transform: ${({sortAsc}) => (sortAsc ? 'rotate(90deg)' : 'rotate(270deg)')};
  }

  &:hover {
    color: #000;
    text-decoration: underline;
  }
`;

export const List = styled.ol`
  padding: 4px 26px 1px;

  ${({withList}) =>
    withList &&
    `
    padding: 4px 26px 1px 40px;
  `};
`;

export const ListItem = styled.li`
  margin-bottom: 15px;

  ${({withList}) =>
    withList &&
    `
    list-style: unset;
  `};
`;

export const ListItemInner = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-size: 14px;
  color: ${secondaryTextColor};

  span {
    font-weight: 500;
    color: #000;
  }
`;

export const Footer = styled.footer`
  padding: 15px 26px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #f7f7f7;
`;
