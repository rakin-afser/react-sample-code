import styled from 'styled-components';
import {secondaryFont} from 'constants/fonts';

export const Wrapper = styled.div`
  margin-right: 22px;
  padding: 12px 15px;
  flex-basis: 280px;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 8px;

  &:last-child {
    margin-right: 0;
  }
`;

export const Title = styled.h4`
  padding: 0;
  margin: 0 0 14px;
  font-weight: 500;
  font-size: 12px;
  color: #000;
`;

export const Count = styled.h5`
  padding: 0;
  margin: 0;
  font-family: ${secondaryFont};
  font-weight: 600;
  text-align: right;
  font-size: 30px;
  letter-spacing: -0.024em;
  color: #000000;

  span {
    margin-right: 10px;
    font-weight: 500;
    font-size: 22px;
  }
`;
