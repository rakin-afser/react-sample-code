import React from 'react';
import PropTypes from 'prop-types';
import numberWithCommas from 'util/heplers';
import {Wrapper, Title, Count} from './styled';

const EarningsInfo = ({title, count}) => {
  return (
    <Wrapper>
      <Title>{title}</Title>
      <Count>
        <span>BD</span>
        {numberWithCommas(count.toFixed(2))}
      </Count>
    </Wrapper>
  );
};

EarningsInfo.propTypes = {
  title: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired
};

export default EarningsInfo;
