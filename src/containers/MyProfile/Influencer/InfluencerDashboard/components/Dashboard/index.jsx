import React, {useEffect, useState} from 'react';
import StatsPanel from 'containers/MyProfile/Influencer/InfluencerDashboard/components/StatsPanel';
import {statsPanel1, statsPanel2, earnings} from 'containers/MyProfile/Influencer/InfluencerDashboard/exampleDtata';
import {exampleDataEarn} from 'containers/MyProfile/components/MyRewards/components/Rewards/exampleData';
import Chart from 'containers/MyProfile/components/MyRewards/components/Chart';
import EarningsInfo from 'containers/MyProfile/Influencer/InfluencerDashboard/components/EarningsInfo';
import {BottomPanels, ChartWrapper, EarningsWrapper, ChartTabs, Tab} from './styled';
import FadeOnMount from 'components/Transitions';

const Dashboard = () => {
  const tabs = [
    {id: 'yesterday', name: 'Yeasterday'},
    {id: 'lastWeek', name: 'Last Week'},
    {id: 'last2Weeks', name: 'Last 2 Weeks'},
    {id: 'lastMonth', name: 'Last Month'}
  ];

  const [chartData, setChartData] = useState([]);
  const [activeTab, setActiveTab] = useState(tabs[3].id);

  useEffect(() => {
    if (activeTab === 'yesterday') {
      setChartData(getLastDatePeriod(exampleDataEarn, 2));
    }

    if (activeTab === 'lastWeek') {
      setChartData(getLastDatePeriod(exampleDataEarn, 7));
    }

    if (activeTab === 'last2Weeks') {
      setChartData(getLastDatePeriod(exampleDataEarn, 14));
    }

    if (activeTab === 'lastMonth') {
      setChartData(getLastDatePeriod(exampleDataEarn, 30));
    }
  }, [activeTab]);

  useEffect(() => {
    setChartData(getLastDatePeriod(exampleDataEarn, 30));
  }, []);

  function getLastDatePeriod(array, period) {
    let newArr = [];

    // eslint-disable-next-line array-callback-return
    exampleDataEarn.reverse().map((el, idx) => {
      if (idx >= period) {
        return;
      }
      newArr = [...newArr, el];
    });

    return newArr;
  }

  const onTabChange = (id) => {
    setActiveTab(id);
  };

  return (
    <FadeOnMount>
      <EarningsWrapper>
        {earnings.map((el) => (
          <EarningsInfo key={el.id} count={el.count} title={el.title} />
        ))}
      </EarningsWrapper>
      <ChartWrapper>
        <ChartTabs>
          {tabs.map((el, idx) => (
            <Tab onClick={() => onTabChange(el.id)} active={el.id === activeTab} key={idx}>
              {el.name}
            </Tab>
          ))}
        </ChartTabs>
        <Chart data={chartData} width="100%" height="80%" />
      </ChartWrapper>
      <BottomPanels>
        <StatsPanel footerTitle="Total Campaigns" title="All Campaigns" data={statsPanel1} />
        <StatsPanel footerTitle="Total Earnings" title="Top 5 Campaigns" data={statsPanel2} withList />
      </BottomPanels>
    </FadeOnMount>
  );
};

export default Dashboard;
