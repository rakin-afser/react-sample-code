import styled from 'styled-components';

export const ChartWrapper = styled.div`
  padding: 29px 0 24px 0;
  background: #ffffff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin-bottom: 18px;
  height: 360px;
`;

export const BottomPanels = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: space-between;
`;

export const EarningsWrapper = styled.div`
  margin-bottom: 20px;
  display: flex;
  justify-content: space-between;
`;

export const ChartTabs = styled.div`
  padding-left: 32px;
  margin-bottom: 46px;
  display: flex;
  align-items: center;
`;

export const Tab = styled.button`
  margin-right: 35px;
  padding: 0;
  font-weight: ${({active}) => (active ? '700' : '500')};
  font-size: 12px;
  color: ${({active}) => (active ? '#000' : '#7a7a7a')};
  border-bottom: ${({active}) => (active ? '2px solid #000' : '2px solid transparent')};
`;
