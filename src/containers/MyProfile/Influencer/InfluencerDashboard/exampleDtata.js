export const statsPanel1 = [
  {id: 1, title: 'Views', count: 2472},
  {id: 2, title: 'Orders', count: 758},
  {id: 3, title: 'Saves to List', count: 159},
  {id: 4, title: 'Likes', count: 1350},
  {id: 5, title: 'Shares', count: 553}
];

export const statsPanel2 = [
  {id: 1, title: 'Off White', count: 8749.0},
  {id: 2, title: 'Nike Offical', count: 5434.0},
  {id: 3, title: 'Reebok Crossfit', count: 3159.0},
  {id: 4, title: 'Kenzo', count: 1385.0},
  {id: 5, title: 'Gucci', count: 553.0}
];

export const earnings = [
  {id: 1, title: 'Total Earnings', count: 15046.0},
  {id: 2, title: 'Total Paid', count: 12761.5},
  {id: 3, title: 'Total Comission', count: 1124.65}
];

export const transactionsBrands = [
  {
    id: 1,
    name: 'John Doe',
    status: 'active',
    startDate: '14.08.2018',
    endData: '14.10.2020',
    totalSales: '546',
    amountEarned: 'BD1.00',
    amountPending: 'BD6300.00'
  }
];

export const transactionsProducts = [
  {
    id: 1,
    name: 'Green Sweater',
    image: '',
    startDate: '14.08.2018',
    endData: '14.10.2020',
    totalSales: '546',
    amountEarned: 'BD1.00',
    amountPending: 'BD6300.00'
  }
];
