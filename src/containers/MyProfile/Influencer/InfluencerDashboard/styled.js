import styled from 'styled-components';
import {mainFont} from 'constants/fonts';

export const Content = styled.div`
  margin-left: 15px;
  width: 100%;
`;

export const TabsWrapper = styled.div`
  margin-bottom: 48px;
  z-index: 10;
`;

export const Nav = styled.div`
  display: flex;
  align-items: flex-end;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
`;

export const Tab = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  border: none;
  cursor: pointer;
  font-family: ${mainFont};
  font-weight: 700;
  font-size: 14px;
  color: #7a7a7a;
  box-sizing: border-box;
  outline: none;
  padding: 12px 0 4px;
  margin: 0 65px;
  border-bottom: 2px solid transparent;
  transition: color ease-in-out 0.4s, border-color ease-in-out 0.4s;

  ${({active}) =>
    active && {
      color: '#000000',
      borderBottom: '2px solid #000000'
    }}
  &:hover {
    color: #000000;
    border-bottom: 2px solid #000000;
  }
`;
