import React from 'react';
import {useParams} from 'react-router-dom';
import {string} from 'prop-types';

import Grid from 'components/Grid';
import Chat from 'components/Chat';
import LeaveFeedback from 'components/Modals/LeaveFeedback';
import useGlobal from 'store';
import {Content as ContentStyled, Nav, Tab} from 'containers/MyProfile/styled';
import SideBar from 'containers/MyProfile/components/SIdebar';

import Notifications from 'containers/MyProfile/components/Notifications';
import List from 'containers/MyProfile/components/List';
import AccountSettings from 'containers/AccountSettings';
import OrdersPage from 'containers/OrdersPage';
import MyRewards from 'containers/MyProfile/components/MyRewards';
import Promote from 'containers/MyProfile/components/Promote';
import PostsGridView from 'containers/MyProfile/components/PostsGridView';
import Products from 'containers/MyProfile/Influencer/Products';
import Activity from 'containers/MyProfile/components/Activity';
import InfluencerDashboard from 'containers/MyProfile/Influencer/InfluencerDashboard';
import {userId} from 'util/heplers';

const ProfileInfluencer = ({page}) => {
  const {slug, userParam} = useParams();
  const [globalState] = useGlobal();
  const activeTab = ['products', 'posts', 'activity'].includes(page) ? page : 'posts';

  const menuLinks = [
    {id: 'profile', href: `/influencer/profile/${userParam ? `${userParam}/` : ''}posts`, text: 'My Profile'},
    {id: 'orders', href: `/influencer/profile/${userParam ? `${userParam}/` : ''}orders`, text: 'My Orders'},
    {id: 'rewards', href: `/influencer/profile/${userParam ? `${userParam}/` : ''}rewards`, text: 'My Rewards'},
    {id: 'messages', href: `/influencer/profile/${userParam ? `${userParam}/` : ''}messages`, text: 'Messages'},
    {
      id: 'notifications',
      href: `/influencer/profile/${userParam ? `${userParam}/` : ''}notifications`,
      text: 'Notifications'
    },
    {id: 'promote', href: `/influencer/profile/${userParam ? `${userParam}/` : ''}promote`, text: 'Promote Lists'},
    {id: 'dashboard', href: `/influencer/profile/${userParam ? `${userParam}/` : ''}dashboard`, text: 'Insights'},
    {id: 'settings', href: `/influencer/profile/${userParam ? `${userParam}/` : ''}settings`, text: 'Settings'}
  ];

  const getPage = () => {
    switch (page) {
      case 'orders':
        return <OrdersPage />;
      case 'rewards':
        return <MyRewards />;
      case 'messages':
        return <Chat />;
      case 'notifications':
        return <Notifications />;
      case 'settings':
        return <AccountSettings />;
      case 'promote':
        return <Promote />;
      case 'dashboard':
        return <InfluencerDashboard />;
      case 'profile':
      case 'posts':
      case 'lists':
      default:
        return null;
    }
  };

  return (
    <Grid pageContainer>
      <Grid>
        <SideBar
          influencer
          menuLinks={menuLinks}
          selected={['products', 'posts', 'activity'].includes(page) ? 'profile' : page}
        />

        {['profile', 'products', 'posts', 'activity'].includes(page) ? (
          <div style={{width: '100%'}}>
            {!slug && (
              <ContentStyled>
                <Nav paddingLeft>
                  <Tab
                    active={activeTab === 'products' ? 1 : 0}
                    to={`/influencer/profile/${userParam ? `${userParam}/` : ''}products`}
                  >
                    Products
                  </Tab>
                  <Tab
                    active={activeTab === 'posts' ? 1 : 0}
                    to={`/influencer/profile/${userParam ? `${userParam}/` : ''}posts`}
                  >
                    Posts
                  </Tab>
                  {userId === +userParam && (
                    <Tab
                      active={activeTab === 'activity' ? 1 : 0}
                      to={`/influencer/profile/${userParam ? `${userParam}/` : ''}activity`}
                    >
                      Activity
                    </Tab>
                  )}
                </Nav>
                {activeTab === 'products' && <Products />}
                {activeTab === 'posts' && <PostsGridView />}
                {activeTab === 'activity' && <Activity />}
              </ContentStyled>
            )}
            {!!slug && activeTab === 'lists' && <List />}
          </div>
        ) : null}
        {getPage()}
      </Grid>
      <LeaveFeedback displaying={globalState.leaveFeedback.open} />
    </Grid>
  );
};

ProfileInfluencer.propTypes = {
  page: string.isRequired
};

export default ProfileInfluencer;
