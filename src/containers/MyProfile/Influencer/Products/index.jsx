import React from 'react';
import Grid from 'components/Grid';
import {promoteList1} from 'containers/MyProfile/components/Promote/staticData';
import PromoteListItem from 'containers/MyProfile/components/PromoteListItem';
import {Hr, TagsTitle} from 'containers/MyProfile/components/PostsGridView/styled';
import PopularTags from 'components/PopularTags';
import FadeOnMount from 'components/Transitions';

const tagsList = [
  '#Cream',
  '#canvas',
  '#Big',
  '#tags',
  '#Cream',
  '#canvas',
  '#Big',
  '#tags',
  '#Big',
  '#tags',
  '#Cream',
  '#canvas',
  '#Big',
  '#tags',
  '#Cream',
  '#canvas',
  '#Big',
  '#tags'
];

const Products = () => {
  function getCards() {
    return promoteList1.items.map((item, i) => (
      <PromoteListItem noSelectedFilter style={{marginBottom: 40}} {...item} key={i} />
    ));
  }

  return (
    <FadeOnMount>
      <Grid sb wrap margin="48px 0 0 0" padding="0 55px 0 59px">
        {getCards()}
      </Grid>
      <Grid sb wrap margin="0" padding="0 55px 0 59px">
        <div>
          <TagsTitle>Popular tags</TagsTitle>
        </div>
        <Hr />
        <PopularTags data={tagsList} />
      </Grid>
    </FadeOnMount>
  );
};

Products.propTypes = {};

export default Products;
