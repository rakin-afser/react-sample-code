import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {primaryColor} from 'constants/colors';

export const Content = styled.div`
  position: relative;
  //width: 576px;
  margin: 0 0 0 16px;
`;

export const Nav = styled.div`
  padding-left: ${({paddingLeft}) => (paddingLeft ? '60px' : '18px')};
  display: flex;
  align-items: flex-end;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
`;

export const Tab = styled(Link)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background: transparent;
  border: none;
  cursor: pointer;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 14px;
  color: #7a7a7a;
  box-sizing: border-box;
  outline: none;
  padding: 12px 0 4px;
  border-bottom: 2px solid transparent;
  transition: color ease-in-out 0.4s, border-color ease-in-out 0.4s;
  margin-right: 112px;

  &:last-child {
    margin-right: 0;
  }

  ${({active}) =>
    active && {
      color: '#000000',
      borderBottom: '2px solid #000000'
    }}
  &:hover {
    color: #000000;
    border-bottom: 2px solid #000000;
  }
`;

export const AddPostBtn = styled.button`
  position: relative;
  top: -5px;
  margin-right: 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 28px;
  height: 28px;
  border-radius: 50%;
  transition: ease 0.4s;
  background-color: ${primaryColor};

  &:hover {
    background-color: #000;
  }
`;
