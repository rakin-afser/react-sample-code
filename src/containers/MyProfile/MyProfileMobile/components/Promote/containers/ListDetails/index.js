import React, {useState} from 'react';
import {FormControlLabel, Checkbox} from '@material-ui/core';
import {withStyles} from '@material-ui/core/styles';
import IconArrowBack from 'assets/ArrowBack';
import {mainBlackColor as black, primaryColor as primary} from 'constants/colors';
import {truncateText} from 'util/heplers';
import ActiveItems from '../../components/ActiveItems';
import Search from '../../components/Search';
import Item from '../../components/Item';
import {
  Header,
  Back,
  Flex,
  OpenedPromoteTitle,
  PromoteLength,
  Main,
  SelectAllLabel,
  SendRequestButton,
  Items
} from './styled';
import {filters} from '../../staticData';

const checkboxStyles = () => ({
  root: {
    '&$checked': {
      color: primary
    }
  },
  checked: {}
});

const CustomCheckbox = withStyles(checkboxStyles)(Checkbox);

const ListDetails = ({listData, closeListDetails}) => {
  const [searchValue, setSearchValue] = useState('');
  const [isAllSelected, setIsAllSelected] = useState(false);
  const [searchFilters, setSearchFilters] = useState(filters);
  const [items, setItems] = useState(
    listData.items.map((item) => ({
      ...item,
      isSelected: false
    }))
  );

  const clearFilter = (id) => {
    setSearchFilters(searchFilters.filter((filter) => filter.id !== id));
  };

  const toggleItemIsSelected = (itemId) => {
    setItems(
      items.map((item) => {
        if (item.id === itemId) {
          item.isSelected = !item.isSelected;
        }
        return item;
      })
    );
    setIsAllSelected(items.every((item) => item.isSelected));
  };

  const toggleIsAllSelected = () => {
    setItems(
      items.map((item) => ({
        ...item,
        isSelected: !isAllSelected
      }))
    );
    setIsAllSelected(!isAllSelected);
  };

  const canSendRequest = items.some((item) => item.isSelected);

  return (
    <>
      <Header>
        <Back onClick={closeListDetails}>
          <IconArrowBack height={26} stroke={black} />
        </Back>
        <Flex>
          <OpenedPromoteTitle>
            {truncateText(listData.brandName, 15)}
            <PromoteLength>({items.length})</PromoteLength>
          </OpenedPromoteTitle>
          <ActiveItems activeItems={listData.activeItems} />
        </Flex>
      </Header>
      <Main>
        <Search
          searchValue={searchValue}
          setSearchValue={setSearchValue}
          filters={searchFilters}
          clearFilter={clearFilter}
        />
        <Flex>
          <FormControlLabel
            label={<SelectAllLabel>Select All</SelectAllLabel>}
            control={<CustomCheckbox checked={isAllSelected} onChange={toggleIsAllSelected} />}
          />
          <SendRequestButton $isDisabled={!canSendRequest}>Send Request</SendRequestButton>
        </Flex>
        <Items>
          {items.map((item) => (
            <Item key={item.id} item={item} toggleIsSelected={toggleItemIsSelected} />
          ))}
        </Items>
      </Main>
    </>
  );
};

export default ListDetails;
