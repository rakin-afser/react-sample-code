import styled from 'styled-components';
import {
  mainWhiteColor as white,
  mainBlackColor as black,
  primaryColor as primary,
  bgLightGray as gray200
} from 'constants/colors';

export const Header = styled.header`
  background-color: ${white};
  display: flex;
  flex-direction: row;
  justify-content: center;
  position: relative;
  padding: 12px 16px 12px 16px;
`;

export const Back = styled.div`
  position: absolute;
  top: 50%;
  left: 16px;
  transform: translateY(-40%);
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const Title = styled.h2`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: ${black};
  margin-top: -2px;
  margin-bottom: 0;
`;

export const OpenedPromoteTitle = styled(Title)`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-left: 35px;
`;

export const PromoteLength = styled.p`
  color: #999999;
  margin-bottom: 0;
  margin-left: 3px;
`;

export const Main = styled.div`
  overflow-x: hidden;
  padding: 16px;
  background-color: #fafafa;
`;

export const SelectAllLabel = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${black};
  margin-bottom: -2px;
`;

export const SendRequestButton = styled.button`
  padding: 5px 20px 3px 20px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${white};
  border-radius: 24px;
  background-color: ${({$isDisabled}) => ($isDisabled ? gray200 : primary)};
  pointer-events: ${({$isDisabled}) => ($isDisabled ? 'none' : 'auto')};
`;

export const Items = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 10px;
  margin-top: 25px;
`;

export const SelectedFilter = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  max-height: 168px;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.4);
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  visibility: hidden;
`;

export const Item = styled.div`
  background: ${white};
  mix-blend-mode: normal;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  width: 100%;
  position: relative;

  ${SelectedFilter} {
    visibility: ${({$isSelected}) => ($isSelected ? 'visible' : 'hidden')};
  }
`;

export const Image = styled.img`
  width: 100%;
  max-height: 168px;
  min-height: 168px;
  object-fit: cover;
`;

export const Body = styled.div`
  background-color: ${white};
  padding: 6px 13px 8px 13px;
  position: relative;
`;

export const Price = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 7px;
`;

export const Currency = styled.span`
  font-size: 12px;
  color: #999999;
`;

export const PriceValue = styled.span`
  font-size: 14px;
  color: ${black};
`;

export const Name = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  color: ${black};
  margin-bottom: 7px;
`;

export const Label = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  color: #999999;
  margin-bottom: 0;
`;

export const Actions = styled.div`
  position: absolute;
  right: 7px;
  bottom: 7px;

  .iconFilledPromote {
    display: ${({$isActive}) => ($isActive ? 'block' : 'none')};
  }

  .iconPromote {
    display: ${({$isActive}) => ($isActive ? 'none' : 'block')};
  }
`;
