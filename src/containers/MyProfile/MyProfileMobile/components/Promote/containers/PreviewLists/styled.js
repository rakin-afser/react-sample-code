import styled, {css} from 'styled-components';
import {mainWhiteColor as white, mainBlackColor as black} from 'constants/colors';

export const Header = styled.header`
  background-color: ${white};
  border-bottom: 3px solid #e5e5e5;
  display: flex;
  flex-direction: row;
  justify-content: center;
  position: relative;
  padding: 12px 16px 12px 16px;
`;

export const Back = styled.div`
  position: absolute;
  top: 50%;
  left: 16px;
  transform: translateY(-40%);
`;

export const Title = styled.h2`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: ${black};
  margin-bottom: 0;
`;

export const PromoteLength = styled.p`
  color: #999999;
  margin-bottom: 0;
  margin-left: 3px;
`;

export const Main = styled.div`
  overflow-x: hidden;
`;

export const Lists = styled.div`
  margin: 0 auto;
  padding: 8px 16px;
  background: #fafafa;
`;

export const searchFiltersStyles = css`
  padding: 15px 0 15px 15px;
`;
