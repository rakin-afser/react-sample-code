import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import IconArrowBack from 'assets/ArrowBack';
import {mainBlackColor as black} from 'constants/colors';
import SearchFilters from '../../components/SearchFilters';
import PreviewList from '../../components/PreviewList';
import {Header, Back, Title, Main, Lists, searchFiltersStyles} from './styled';
import {filters} from '../../staticData';

const PreviewLists = ({lists, setOpenedList}) => {
  const [searchFilters, setSearchFilters] = useState(filters);

  const history = useHistory();

  const clearFilter = (id) => {
    setSearchFilters(searchFilters.filter((filter) => filter.id !== id));
  };

  const redirectToProfile = () => history.push('/profile/posts');

  const getDisplayedItems = (index) => {
    if (index % 4 === 0) return 2;
    else if (index % 3 === 0) return 4;
    else if (index % 2 === 0) return 3;
    else return 3;
  };

  return (
    <>
      <Header>
        <Back onClick={redirectToProfile}>
          <IconArrowBack height={26} stroke={black} />
        </Back>
        <Title>Promote List</Title>
      </Header>
      <Main>
        <SearchFilters filters={searchFilters} clearFilter={clearFilter} customStyles={searchFiltersStyles} />
        <Lists>
          {lists.map((list, index) => (
            <PreviewList
              key={list.id}
              list={list}
              displayedItems={getDisplayedItems(index + 1)}
              openListDetails={() => setOpenedList(list)}
            />
          ))}
        </Lists>
      </Main>
    </>
  );
};

export default PreviewLists;
