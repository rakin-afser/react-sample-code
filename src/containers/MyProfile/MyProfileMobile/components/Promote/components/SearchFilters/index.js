import React from 'react';
import IconClose from 'assets/CloseIcon';
import {Filters, Filter, Text, Close} from './styled';

const SearchFilters = ({filters, clearFilter, customStyles}) => {
  if (!filters.length) {
    return null;
  }

  return (
    <Filters $customStyles={customStyles}>
      {filters.map((filter) => (
        <Filter key={filter.id}>
          <Text>{filter.value}</Text>
          <Close onClick={() => clearFilter(filter.id)}>
            <IconClose color="#666" width={18} height={18} />
          </Close>
        </Filter>
      ))}
    </Filters>
  );
};

export default SearchFilters;
