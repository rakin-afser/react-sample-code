import styled from 'styled-components';
import {bookmarkFillColor as gray900, bgLightGray as gray200} from 'constants/colors';

export const Filters = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  background-color: transparent;
  overflow-x: auto;
  ${({$customStyles}) => $customStyles || ''};
`;

export const Filter = styled.div`
  border: 1px solid ${gray200};
  box-sizing: border-box;
  border-radius: 22px;
  padding: 7px 14px;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  margin-right: 8px;
`;

export const Text = styled.span`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  color: ${gray900};
  white-space: nowrap;
  margin-right: 8px;
`;

export const Close = styled.div`
  height: 18px;
`;
