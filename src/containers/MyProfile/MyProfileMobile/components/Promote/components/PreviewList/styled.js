import styled from 'styled-components';
import {mainWhiteColor as white, mainBlackColor as black} from 'constants/colors';

export const StyledPreviewList = styled.div`
  background-color: ${white};
  border: 1px solid #eeeeee;
  border-radius: 3px;
  padding: 10px;
  margin-bottom: 10px;

  &:hover {
    cursor: pointer;
  }
`;

export const Header = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 11px;
`;

export const Brand = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const BrandLogo = styled.img`
  width: 28px;
  height: 28px;
  object-fit: cover;
  margin-right: 7px;
`;

export const BrandName = styled.p`
  margin-bottom: 0;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: ${black};
`;

export const Gallery = styled.div`
  ${({theme: {displayedItems}}) => {
    switch (displayedItems) {
      case 2:
      case 3:
        return `
          display: flex;
          flex-direction: row;
          justify-content: space-between;
        `;
      case 4:
        return `
          display: grid;
          grid-template-columns: repeat(2, 1fr);
          grid-gap: 15px;
        `;
    }
  }}
`;

export const GalleryImage = styled.img`
  object-fit: cover;
  border-radius: 3px;

  ${({theme: {displayedItems}}) => {
    switch (displayedItems) {
      case 2:
        return `
          width: 48%;
          height: auto;
        `;
      case 3:
        return `
          width: 31%;
          height: auto;
        `;
      case 4:
        return `
          width: 100%;
          height: 100%;
        `;
    }
  }}
`;
