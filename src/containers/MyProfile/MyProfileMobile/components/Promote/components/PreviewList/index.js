import React from 'react';
import {ThemeProvider} from 'styled-components';
import {truncateText} from 'util/heplers';
import ActiveItems from '../ActiveItems';
import {Header, Brand, BrandLogo, BrandName, StyledPreviewList, Gallery, GalleryImage} from './styled';

const PreviewList = ({list: {brandLogo, brandName, activeItems, items}, displayedItems = 3, openListDetails}) => (
  <StyledPreviewList onClick={openListDetails}>
    <ThemeProvider theme={{displayedItems}}>
      <Header>
        <Brand>
          {brandLogo && <BrandLogo src={brandLogo} />}
          <BrandName>{truncateText(brandName, 15)}</BrandName>
        </Brand>
        <ActiveItems activeItems={activeItems} withoutInactive />
      </Header>
      <Gallery>
        {items.slice(0, displayedItems).map(({id, img}) => (
          <GalleryImage key={id} src={img} />
        ))}
      </Gallery>
    </ThemeProvider>
  </StyledPreviewList>
);

export default PreviewList;
