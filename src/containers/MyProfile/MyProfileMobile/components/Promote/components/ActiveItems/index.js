import React from 'react';
import IconPromote from 'assets/PromoteIcon';
import {transparentTextColor as gray300, lightGreen} from 'constants/colors';
import {ActiveItems, ActiveItemsText, TextBlack} from './styled';

export default ({activeItems, withoutInactive = false}) => (
  <ActiveItems>
    <IconPromote fillColor={activeItems ? lightGreen : gray300} />
    <ActiveItemsText>
      {activeItems ? (
        <>
          <TextBlack>Active</TextBlack> {activeItems} Product
        </>
      ) : (
        !withoutInactive && <TextBlack>Inactive</TextBlack>
      )}
    </ActiveItemsText>
  </ActiveItems>
);
