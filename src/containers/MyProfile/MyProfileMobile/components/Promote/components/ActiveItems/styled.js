import styled from 'styled-components';
import {mainBlackColor as black, transparentTextColor as gray300} from 'constants/colors';

export const ActiveItems = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const ActiveItemsText = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  margin-left: 6px;
  color: ${gray300};
`;

export const TextBlack = styled.span`
  color: ${black};
`;
