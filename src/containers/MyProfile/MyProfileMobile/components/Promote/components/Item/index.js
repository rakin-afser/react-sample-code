import React from 'react';
import IconPromote from 'assets/PromoteIcon';
import {ReactComponent as IconCheck} from 'containers/MyProfile/icons/check.svg';
import {transparentTextColor as gray300, lightGreen} from 'constants/colors';
import {truncateText} from 'util/heplers';
import {StyledItem, Image, SelectedFilter, Body, Price, Currency, PriceValue, Name, Label, Actions} from './styled';

const Item = ({item, toggleIsSelected}) => (
  <StyledItem $isSelected={item.isSelected} onClick={() => toggleIsSelected(item.id)}>
    <Image src={item.img} />
    <SelectedFilter>
      <IconCheck />
    </SelectedFilter>
    <Body>
      <Price>
        <Currency>{item.currency}</Currency>
        <PriceValue>{item.price}</PriceValue>
      </Price>
      <Name>{truncateText(item.name)}</Name>
      <Label>Free Shipping</Label>
      <Actions $isActive={item.isActive}>
        <IconPromote classes="iconPromote" fillColor={gray300} />
        <IconPromote classes="iconFilledPromote" fillColor={lightGreen} />
      </Actions>
    </Body>
  </StyledItem>
);

export default Item;
