import styled from 'styled-components';
import {mainWhiteColor as white, mainBlackColor as black} from 'constants/colors';

export const SelectedFilter = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  max-height: 168px;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.4);
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  visibility: hidden;
`;

export const StyledItem = styled.div`
  background: ${white};
  mix-blend-mode: normal;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  width: 100%;
  position: relative;

  ${SelectedFilter} {
    visibility: ${({$isSelected}) => ($isSelected ? 'visible' : 'hidden')};
  }
`;

export const Image = styled.img`
  width: 100%;
  max-height: 168px;
  min-height: 168px;
  object-fit: cover;
`;

export const Body = styled.div`
  background-color: ${white};
  padding: 6px 13px 8px 13px;
  position: relative;
`;

export const Price = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 7px;
`;

export const Currency = styled.span`
  font-size: 12px;
  color: #999999;
`;

export const PriceValue = styled.span`
  font-size: 14px;
  color: ${black};
`;

export const Name = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  color: ${black};
  margin-bottom: 7px;
`;

export const Label = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  color: #999999;
  margin-bottom: 0;
`;

export const Actions = styled.div`
  position: absolute;
  right: 7px;
  bottom: 7px;

  .iconFilledPromote {
    display: ${({$isActive}) => ($isActive ? 'block' : 'none')};
  }

  .iconPromote {
    display: ${({$isActive}) => ($isActive ? 'none' : 'block')};
  }
`;
