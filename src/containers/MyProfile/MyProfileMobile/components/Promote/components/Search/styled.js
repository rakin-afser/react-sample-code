import styled, {css} from 'styled-components';
import {mainBlackColor as black, mainWhiteColor as white, primaryColor as primary} from 'constants/colors';

export const InputGroup = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 15px;
`;

export const InputWrapper = styled.div`
  width: 100%;
  position: relative;
  flex: 1;
`;

export const InputSearch = styled.input`
  flex: 1;
  border: none;
  background: #efefef;
  border-radius: 4px;
  padding: 13px 35px 10px 45px;
  color: ${black};
  width: 100%;
  line-height: 1;

  &:focus,
  &:active {
    outline: none;
  }
`;

export const IconSearchWrapper = styled.div`
  position: absolute;
  top: 8px;
  left: 10px;
`;

export const IconCloseWrapper = styled.div`
  position: absolute;
  top: 13px;
  right: 10px;
`;

export const ButtonCancel = styled.button`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: ${primary};
  background-color: ${white};
  margin-left: 10px;
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin: ${({$margin}) => $margin || ''};
`;

export const searchFiltersStyles = css`
  margin: 0 -15px 15px 0;
`;

export const ResultsNumber = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${black};
`;

export const ButtonSortFilter = styled.button`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: ${black};
`;

export const ButtonAdvancedSearch = styled.button`
  margin-bottom: -5px;
  margin-left: 50px;
`;
