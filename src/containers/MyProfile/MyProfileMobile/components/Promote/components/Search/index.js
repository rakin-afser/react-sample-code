import React from 'react';
import IconSearch from 'assets/SearchIcon';
import IconFilter from 'assets/Filter';
import SearchFilters from '../SearchFilters';
import {ReactComponent as IconClose} from 'images/icons/closeInCircle.svg';
import {
  InputGroup,
  InputWrapper,
  InputSearch,
  IconSearchWrapper,
  IconCloseWrapper,
  ButtonCancel,
  Flex,
  searchFiltersStyles,
  ResultsNumber,
  ButtonSortFilter,
  ButtonAdvancedSearch
} from './styled';

const Search = ({searchValue, setSearchValue, filters, clearFilter}) => {
  const handleChange = (event) => setSearchValue(event.target.value);

  return (
    <>
      <InputGroup>
        <InputWrapper>
          <InputSearch value={searchValue} onChange={handleChange} placeholder="Search" />
          <IconSearchWrapper>
            <IconSearch fill="#999" />
          </IconSearchWrapper>
          <IconCloseWrapper onClick={() => setSearchValue('')}>
            <IconClose />
          </IconCloseWrapper>
        </InputWrapper>
        {searchValue && <ButtonCancel onClick={() => setSearchValue('')}>Cancel</ButtonCancel>}
      </InputGroup>
      {searchValue && (
        <>
          <Flex $margin="0 0 15px 0">
            <ResultsNumber>3 Results</ResultsNumber>
            <Flex>
              <ButtonSortFilter>Sort &#38; Filter</ButtonSortFilter>
              <ButtonAdvancedSearch>
                <IconFilter />
              </ButtonAdvancedSearch>
            </Flex>
          </Flex>
          <SearchFilters filters={filters} customStyles={searchFiltersStyles} clearFilter={clearFilter} />
        </>
      )}
    </>
  );
};

export default Search;
