import React, {useState} from 'react';
import PreviewLists from './containers/PreviewLists';
import ListDetails from './containers/ListDetails';
import {Section} from './styled';
import {lists} from './staticData';

const Promote = () => {
  const [openedList, setOpenedList] = useState(null);

  return (
    <Section>
      {openedList ? (
        <ListDetails listData={openedList} closeListDetails={() => setOpenedList(null)} />
      ) : (
        <PreviewLists lists={lists} setOpenedList={setOpenedList} />
      )}
    </Section>
  );
};

export default Promote;
