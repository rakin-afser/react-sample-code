import brandLogo1 from './img/brandLogo1.png';
import brandLogo2 from './img/brandLogo2.jpg';
import productItem1 from './img/productItem1.png';
import productItem2 from './img/productItem2.png';
import productItem3 from './img/productItem3.png';
import productItem4 from './img/productItem4.png';
import productItem5 from './img/productItem5.png';
import productItem6 from './img/productItem6.png';
import productItem7 from './img/productItem7.png';
import productItem8 from './img/productItem8.png';
import productItem9 from './img/productItem9.png';

export const filters = [
  {id: 1, value: "Woman's clothings"},
  {id: 2, value: "Kid's clothings"},
  {id: 3, value: 'Off White clothings'},
  {id: 4, value: 'Lux'}
];

export const lists = [
  {
    id: 1,
    brandLogo: brandLogo1,
    brandName: 'Karl Lagerfeld',
    activeItems: 1,
    items: [
      {id: 1, img: productItem1, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 2, img: productItem2, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: true},
      {id: 3, img: productItem3, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 4, img: productItem1, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 5, img: productItem2, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: true},
      {id: 6, img: productItem3, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false}
    ]
  },
  {
    id: 2,
    brandLogo: brandLogo1,
    brandName: 'Karl Lagerfeld',
    activeItems: 1,
    items: [
      {id: 1, img: productItem1, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: true},
      {id: 2, img: productItem2, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 3, img: productItem3, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false}
    ]
  },
  {
    id: 3,
    brandLogo: brandLogo2,
    brandName: 'Burberry',
    activeItems: 0,
    items: [
      {id: 1, img: productItem3, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 2, img: productItem1, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 3, img: productItem5, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 4, img: productItem4, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 5, img: productItem6, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false}
    ]
  },
  {
    id: 4,
    brandLogo: null,
    brandName: 'Off White',
    activeItems: 0,
    items: [
      {id: 1, img: productItem7, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 2, img: productItem8, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 3, img: productItem1, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
      {id: 4, img: productItem2, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: true},
      {id: 5, img: productItem3, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false}
    ]
  }
];

export const foundItems = [
  {id: 1, img: productItem5, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
  {id: 2, img: productItem9, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false},
  {id: 3, img: productItem9, price: 132.98, currency: 'BD', name: 'Face Brush Kabuki', isActive: false}
];
