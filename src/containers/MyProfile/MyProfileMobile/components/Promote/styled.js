import styled from 'styled-components';
import {mainWhiteColor as white, mainBlackColor as black, transparentTextColor as gray300} from 'constants/colors';

export const Section = styled.section``;

export const Header = styled.header`
  background-color: ${white};
  border-bottom: 3px solid #e5e5e5;
  display: flex;
  flex-direction: row;
  justify-content: center;
  position: relative;
  padding: 12px 16px 12px 16px;
`;

export const Back = styled.div`
  position: absolute;
  top: 50%;
  left: 16px;
  transform: translateY(-40%);
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;

export const Title = styled.h2`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: ${black};
  margin-bottom: 0;
`;

export const OpenedPromoteTitle = styled(Title)`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-left: 30px;
`;

export const PromoteLength = styled.p`
  color: #999999;
  margin-bottom: 0;
  margin-left: 3px;
`;

export const Main = styled.div`
  overflow-x: hidden;
`;

export const PreviewLists = styled.div`
  margin: 0 auto;
  padding: 8px 16px;
  background: #fafafa;
`;
