import styled from 'styled-components/macro';
import {headerShadowColor, bookmarkFillColor, bgSuperLightGray} from 'constants/colors';
import {mainFont, secondaryFont} from 'constants/fonts';
import {Button} from 'antd';
import {ReactComponent as Dots} from 'images/svg/dots.svg';

export const ItemWrapper = styled.div`
  background: #fff;
  padding: 12px 15px;
  margin-bottom: 8px;
`;

export const Product = styled.div`
  width: 65px;
  height: 65px;
  border-radius: 4px;
  overflow: hidden;
  margin-right: 10px;
  flex-shrink: 0;
  border: 1px solid #efefef;
`;

export const ProductPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Title = styled.span`
  max-width: 200px;
  display: block;
  font-size: 16px;
  font-weight: 500;
  margin-top: 1px;
  margin-bottom: 6px;
  color: #000;
`;

export const OrderDate = styled.span`
  font-size: 12px;
  color: ${bookmarkFillColor};
`;

export const UnlockBtn = styled.button`
  margin-left: auto;
  margin-right: auto;
  color: #fff;
  border: #666666;
  border-radius: 24px;
  height: 24px;
  display: block;
  vertical-align: top;
  text-align: center;
  transition: 0.3s;
  padding: 0 14px;
  background-color: #666666;
  font-size: 12px;
  font-weight: 500;
  cursor: pointer;

  &:active {
    background-color: #fff;
    color: #666666;

    svg {
      stroke: #666666;
    }
  }

  i {
    vertical-align: middle;
    margin: -2px 0 0 4px;
  }

  svg {
    stroke: #fff;
    transition: stroke 0.3s;
  }
`;

export const MoreInfo = styled.ul`
  position: absolute;
  top: 0;
  right: 100%;
  background: #fff;
  max-width: 200px;
  box-shadow: 0 2px 9px rgba(0, 0, 0, 0.28);
  border-radius: 8px;
  white-space: nowrap;
  font-family: Helvetica, sans-serif;

  li {
    padding: 8px 24px;

    &:not(:last-child) {
      border-bottom: 1px solid ${bgSuperLightGray};
    }
  }

  span,
  a {
    color: ${bookmarkFillColor};
    cursor: pointer;

    &:hover {
      color: #000;
    }
  }
`;

export const MoreBtn = styled.button`
  color: #7c7e82;
  border: none;
  padding: 0;
  line-height: 1;
  text-align: center;
  cursor: pointer;
  transition: 0.3s;
  background-color: transparent;

  @media (hover: hover) {
    opacity: 0;
  }

  &.ant-dropdown-open {
    opacity: 1;
  }

  &:hover {
    color: #000;
  }

  svg {
    vertical-align: top;
  }

  i {
    transform: rotate(90deg);
  }
`;

export const CoinsRow = styled.ul`
  margin-bottom: 12px;
  display: flex;
  align-items: center;

  li {
    margin-right: 7px;
    display: flex;
    align-items: center;
    font-family: ${secondaryFont};
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 132%;
    text-align: right;
    color: #999999;

    &:last-child {
      margin-right: 0;
    }

    span {
      display: flex;
      align-items: center;
      font-family: ${mainFont};
      font-weight: 500;

      i {
        margin-left: 2px;
      }
    }
  }
`;

export const PriceInfo = styled.div`
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #000000;

  span {
    margin-right: 2px;
    font-size: 10px;
  }
`;

export const Footer = styled.div`
  max-width: 211px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-left: 88px;
  margin-top: 17px;
  & span {
    font-size: 13px;
  }
`;

const CustomButton = styled(Button)`
  &&& {
    border-color: currentColor;
    border-radius: 100px;
    font-size: 12px;
    height: 28px;
    min-width: 92px;
    & span {
      font-size: 12px;
    }
  }
`;

export const Buy = styled(CustomButton)`
  &&& {
    color: #ed484f;
  }
`;

export const Unlock = styled(CustomButton)`
  &&& {
    color: #398287;
  }
`;

export const OptionsBtn = styled(Dots)`
  position: absolute;
  top: 0;
  right: 0;
  z-index: 1;
  width: 24px;
  height: 24px;
`;
