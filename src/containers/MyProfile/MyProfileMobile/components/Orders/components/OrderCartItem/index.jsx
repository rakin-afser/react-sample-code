import React, {useState} from 'react';
import useGlobal from 'store';
import PropTypes from 'prop-types';
import moment from 'moment';
import ClampLines from 'react-clamp-lines';
import parse from 'html-react-parser';
import {useQuery} from '@apollo/client';
import _ from 'lodash';

import {
  ItemWrapper,
  Product,
  ProductPic,
  Title,
  OrderDate,
  UnlockBtn,
  MoreInfo,
  MoreBtn,
  CoinsRow,
  PriceInfo,
  Footer,
  Buy,
  Unlock,
  OptionsBtn
} from './styled';
import Grid from 'components/Grid';
import Icons from 'components/Icon';
import RequestAReturn from 'components/Modals/RequestAReturn';
import {renderCoinsByVariation} from 'util/heplers';
// import {useUser} from 'hooks/reactiveVars';
// import {REVIEW_CHECK} from 'containers/OrdersPage/api/queries';

const OrderCartItem = ({cartItemInfo, date, status}) => {
  const [, globalActions] = useGlobal();
  const [returnModal, showReturnModal] = useState(false);
  const {variationId, product, leftfeedback} = cartItemInfo;
  const variationToPass = {
    databaseId: variationId
  };

  const buttons = [
    {
      title: 'Request a Return',
      onClick() {
        globalActions.setOptionsPopup({active: false});
        globalActions.setReturnRequest({open: true, data: cartItemInfo});
      }
    },
    {
      title: 'Share',
      onClick() {
        globalActions.setOptionsPopup({active: false});
        globalActions.setLeaveFeedback(true);
      }
    },
    {
      title: 'Contact Seller',
      onClick() {
        globalActions.setOptionsPopup({active: false});
        globalActions.setSellerContact(true);
      }
    }
  ];

  const unlockMidCoins = () => {
    globalActions.setLeaveFeedback({
      open: true,
      data: cartItemInfo
    });
  };

  const midcoins = renderCoinsByVariation(product?.midCoins, variationToPass);
  const shopcoins = renderCoinsByVariation(product?.shopCoins, variationToPass);

  function renderCoinButtons() {
    if (status === 'COMPLETED' && (shopcoins || midcoins) && !leftfeedback) {
      return <Unlock onClick={unlockMidCoins}>Unlock Coins</Unlock>;
    }

    if (status === 'COMPLETED' && (shopcoins || midcoins) && leftfeedback) {
      return <span>Coins Unlocked</span>;
    }

    return null;
  }

  return (
    <>
      <ItemWrapper>
        <Grid wrap width="100%" margin="0 0 13px 0">
          <Product>
            <ProductPic src={cartItemInfo?.product?.image?.sourceUrl} />
          </Product>
          <Grid sb customStyles="align-items: stretch; flex: 1;">
            <Grid column padding="0 10px 0 0" customStyles="flex-grow: 1;">
              <Title>
                <ClampLines text={cartItemInfo?.product?.name} lines={1} buttons={false} />
              </Title>
              <CoinsRow>
                <li>
                  Mid Coins:{' '}
                  <span>
                    +{midcoins} <Icons type="coins" />
                  </span>
                </li>
                <li>
                  Shop*Coins:{' '}
                  <span>
                    +{shopcoins} <Icons type="coins" />
                  </span>
                </li>
              </CoinsRow>
              <Grid aic sb width="100%">
                <PriceInfo>
                  <span>{parse(cartItemInfo?.product?.currencySymbol)}</span>
                  {cartItemInfo?.total}
                </PriceInfo>
                <OrderDate>Ordered on {moment(date).format('MMM D, YYYY')}</OrderDate>
              </Grid>
            </Grid>
            <Grid column sb aife padding="0" customStyles="flex-shrink: 0;">
              {status !== 'CANCELLED' && (
                <OptionsBtn onClick={() => globalActions.setOptionsPopup({active: true, buttons})} />
              )}
            </Grid>
          </Grid>
        </Grid>
        {status === 'COMPLETED' && (
          <Footer>
            <Buy>Buy Again</Buy>
            {renderCoinButtons()}
          </Footer>
        )}
      </ItemWrapper>
      <RequestAReturn reqReturnModal={returnModal} setReqReturnModal={showReturnModal} />
    </>
  );
};

OrderCartItem.defaultProps = {
  cartItemInfo: {}
};

OrderCartItem.propTypes = {
  cartItemInfo: PropTypes.shape()
};

export default OrderCartItem;
