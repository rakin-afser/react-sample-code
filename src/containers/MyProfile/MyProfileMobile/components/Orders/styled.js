import {Tabs} from 'antd';
import styled from 'styled-components/macro';

export const Wrapper = styled.div``;

export const StyledTabs = styled(Tabs)`
  .ant-tabs-bar {
    margin-bottom: 0;
  }

  .ant-tabs-ink-bar {
    background-color: #ed484f;
  }

  .ant-tabs-bar {
    border: none;
    position: relative;

    &:after {
      content: '';
      position: absolute;
      left: 0;
      right: 0;
      bottom: 0;
      border-bottom: 2px solid #efefef;
    }
  }

  &&& {
    .ant-tabs-nav {
      &-container {
        padding: 0;
      }

      &-scroll {
        overflow: auto;
      }
    }
    .ant-tabs-tab {
      margin-right: 0;
      min-width: 94px;
      text-align: center;
      padding: 9.5px 0;

      &-prev,
      &-next {
        display: none;
      }

      &.ant-tabs-tab-active {
        color: #000;
        font-weight: 700;
      }
    }
  }
`;
