import React, {useEffect, useState} from 'react';
import {Tabs} from 'antd';
import {useParams} from 'react-router-dom';
import {useLazyQuery} from '@apollo/client';
import moment from 'moment';
import FilterButtons from 'components/FilterButtons';
import {Wrapper, StyledTabs} from './styled';
import Orders from 'components/Orders/mobile';
import {GET_ORDERS} from 'queries';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import {userId} from 'util/heplers';

const {TabPane} = Tabs;

const OrdersPage = () => {
  const [, globalActions] = useGlobal();
  const {userParam} = useParams();
  const [user] = useUser();
  const id = user?.userId;

  const [activeLink, setActiveLink] = useState('All Orders');
  const [activeDate, setActiveDate] = useState('Last year');

  useEffect(() => {
    globalActions.setHeaderBackButtonLink(`/profile/${userParam ? `${userParam}/` : ''}settings`);
  }, []);

  const getStatus = () => {
    switch (activeLink) {
      case 'All Orders':
        return null;
      case 'Processing':
        return 'PROCESSING';
      case 'Delivered':
        return 'COMPLETED';
      case 'Returned':
        return 'REFUNDED';
      case 'Cancelled':
        return 'CANCELLED';
      default:
        return null;
    }
  };

  const getByDate = () => {
    const year = parseInt(moment().format('YYYY'));
    const month = moment().format('M') - 3;
    const week = moment().format('w') - 1;

    switch (activeDate) {
      case 'Last Year':
        return {year};
      case 'Last 3 Months':
        return {after: {month}};
      case 'Last Week':
        return {week};
      default:
        return {year};
    }
  };

  const [getOrders, {data, error, loading}] = useLazyQuery(GET_ORDERS, {
    variables: {
      customerId: id,
      statuses: getStatus(),
      dateQuery: getByDate('activeDate')
    }
  });

  useEffect(() => {
    if (id) {
      getOrders();
    }
  }, [getOrders, id]);

  function changeContent(activeTab) {
    setActiveLink(activeTab);
    getOrders();
  }

  const onChangeTab = (tabName) => {
    changeContent(tabName);
  };

  const tabs = [
    {
      title: 'All'
    },
    {
      title: 'Processing'
    },
    {
      title: 'Delivered'
    },
    {
      title: 'Returned'
    },
    {
      title: 'Cancelled'
    }
  ];

  function renderTabBar(props, DefaultTabBar) {
    return (
      <>
        <DefaultTabBar {...props} />
        <FilterButtons setActiveDate={setActiveDate} />
      </>
    );
  }

  return (
    <Wrapper>
      <StyledTabs onChange={onChangeTab} defaultActiveKey="1" renderTabBar={renderTabBar}>
        {tabs.map((tab) => (
          <TabPane tab={tab.title} key={tab.title}>
            <Orders data={data} loading={loading} error={error} />
          </TabPane>
        ))}
      </StyledTabs>
    </Wrapper>
  );
};

export default OrdersPage;
