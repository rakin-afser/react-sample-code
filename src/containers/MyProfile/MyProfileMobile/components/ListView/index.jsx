import React, {useState} from 'react';
import {Link, useParams, useHistory} from 'react-router-dom';
import {useQuery} from '@apollo/client';

import ArrowBack from 'assets/ArrowBack';
import {Wrapper, BackTo} from 'containers/MyProfile/MyProfileMobile/components/Posts/styled';
import {ListContent, ListHeader, ListDescription, Line} from './styled';
import {useUser} from 'hooks/reactiveVars';
import {GET_LIST, GET_LISTS} from 'containers/MyProfile/api/queries';
import ProductGridAsymmetrical from 'components/ProductGridAsymmetrical/mobile';
import Skeleton from 'containers/SearchResult/components/Skeleton';
import ListFollowers from 'components/ListFollowers/mobile';
import Button from 'components/Buttons';

const ListView = () => {
  const {slug, userParam} = useParams();
  const [user] = useUser();
  const history = useHistory();
  const isGuest = userParam && +userParam !== user?.databaseId;
  const isWishlist = slug?.toLowerCase() === 'wishlist';
  const currentQuery = isWishlist ? GET_LISTS : GET_LIST;
  const variables = isWishlist ? {where: {user_id: user?.databaseId, list_name: slug}} : {id: +slug};
  const {data, loading, error} = useQuery(currentQuery, {
    variables
  });

  const listData = (isWishlist ? data?.lists?.nodes?.[0] : data?.getList) || {};
  const {user_id: userId, is_private: isPrivate} = listData || {};
  const productsData = listData?.addedProducts?.nodes || [];

  const goBack = () => {
    history.goBack();
  };

  const params = user?.databaseId === userId || isPrivate ? '' : `?listId=${slug}`;
  const onProductClick = (productSlug) => history.push(`/product/${productSlug}${params}`);

  return (
    <Wrapper>
      <BackTo>
        <Link onClick={goBack} className="back">
          <ArrowBack stroke="#000" />
        </Link>
        {listData.list_name}
      </BackTo>
      <ListHeader>
        <ListDescription>
          {listData.list_description || 'No description yet'}
          {listData?.is_private ? null : (
            <Line>
              <ListFollowers />
              <Button type="follow" title={isGuest ? 'Follow' : 'Invite'} />
            </Line>
          )}
        </ListDescription>
      </ListHeader>
      <ListContent>
        {loading ? <Skeleton /> : <ProductGridAsymmetrical onProductClick={onProductClick} data={productsData} />}
      </ListContent>
    </Wrapper>
  );
};

export default ListView;
