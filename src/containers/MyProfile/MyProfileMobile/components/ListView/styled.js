import styled from 'styled-components/macro';

export const ListContent = styled.div`
  padding: 8px 16px 60px;
  background-color: #fafafa;
  min-height: 80vh;
  flex-grow: 2;
`;

export const ListHeader = styled.div`
  padding: 24px 16px;
`;

export const ListDescription = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #000000;
`;

export const Line = styled.div`
  display: flex;
  align-items: center;
  margin-top: 24px;
`;
