import styled from 'styled-components';

export const Header = styled.div`
  padding: 14px 16px 0;
  display: flex;
`;

export const FollowText = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #666666;
  margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 18px' : '0 18px 0 0')};
`;

export const FollowBlock = styled.div`
  display: flex;
  margin: ${({theme: {isArabic}}) => (isArabic ? '14px 0 8px' : '11px 0 14px')};
`;

export const FollowNumber = styled(FollowText)`
  color: #000000;
  font-weight: 500;
  margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 8px' : '0 8px 0 0')};
`;

export const UserInfo = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: flex-start;
  flex-direction: column;
`;

export const Name = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  color: #000;
  line-height: normal;

  & span {
    margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 10px' : '0 10px 0 0')};
  }

  & svg {
    transform: rotate(90deg) translateX(2px);
    path {
      fill: #000;
    }
  }
`;

export const ButtonWrap = styled.div`
  display: flex;

  button + button {
    margin-left: 16px;
  }
`;

export const Profession = styled.p`
  font-weight: 500;
  margin-top: 13px;
  margin-bottom: 4px;
  color: #000;
`;

export const Desc = styled.p`
  color: #000;
`;
