import React from 'react';
import {Header, FollowBlock, FollowNumber, FollowText, UserInfo, Name, ButtonWrap, Profession, Desc} from './styled';
import UserAvatar from 'containers/MyProfile/components/SIdebar/Components/Avatar';
import userAvatar from 'images/followers-avatar/avatar8.png';
import {useTranslation} from 'react-i18next';
import Button from 'components/Buttons';
import Grid from 'components/Grid';

const user = {
  avatar: userAvatar,
  firstName: 'Kylie',
  lastName: 'Jenner',
  followers: '3k',
  following: 999,
  profession: 'Model, blogger, CEO of Kylie Skin',
  description: `Romanian born and Parisian based model, journalist and fashion influencer.`
};

const Guest = () => {
  const {t} = useTranslation();
  return (
    <>
      <Header>
        <UserAvatar img={user.avatar} isMobile />
        <UserInfo>
          <Name>
            <span>
              {user.firstName} {user.lastName}
            </span>
          </Name>
          <FollowBlock>
            <FollowNumber>{user.followers}</FollowNumber>
            <FollowText>{t('myProfilePage.followers')}</FollowText>
            <FollowNumber>{user.following}</FollowNumber>
            <FollowText>{t('myProfilePage.following')}</FollowText>
          </FollowBlock>
          <ButtonWrap>
            <Button type="follow" />
            <Button type="sendMessage" />
          </ButtonWrap>
        </UserInfo>
      </Header>
      <Grid padding="0 16px" column>
        <Profession>{user.profession}</Profession>
        <Desc>{user.description}</Desc>
      </Grid>
    </>
  );
};

export default Guest;
