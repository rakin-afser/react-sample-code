import React, {useEffect, useState} from 'react';
import qs from 'qs';
import {useLocation, useHistory} from 'react-router-dom';
import Layout from 'containers/Layout';
import IconArrowBack from 'assets/ArrowBack';
import {ReactComponent as IconArrowDown} from 'images/icons/arrowDown.svg';
import {mainBlackColor as black} from 'constants/colors';
import Categories from './components/Categories';
import ProductItem from './components/ProductItem';
import {
  Header,
  ButtonBack,
  Title,
  Tools,
  ItemsCount,
  ButtonSort,
  ButtonSortText,
  ItemsGrid,
  Left,
  Right
} from './styled';
import {getItems} from './staticData';

const categories = [
  {id: 1, title: 'All', name: undefined},
  {id: 2, title: 'Clothing', name: 'clothing'},
  {id: 3, title: 'Shoes', name: 'shoes'},
  {id: 4, title: 'Bags & Accessoires', name: 'bagsAccessoires'},
  {id: 5, title: 'Beauty & Health', name: 'beautyHealth'},
  {id: 6, title: 'Electronics', name: 'electronics'}
];

const Wishlist = () => {
  const location = useLocation();
  const history = useHistory();

  const selectedCategory = qs.parse(location.search, {ignoreQueryPrefix: true}).category;

  const [items, setItems] = useState(getItems(selectedCategory));

  useEffect(() => {
    setItems(getItems(selectedCategory));
  }, [selectedCategory]);

  const goBack = () => history.push('/profile/lists');

  const onUserLike = (itemId) =>
    setItems(
      items.map((item) => {
        if (item.id === itemId) {
          item.isLiked = !item.isLiked;
        }
        return item;
      })
    );

  const itemsLeftCol = [];
  const itemsRightCol = [];

  items.forEach((item, index) => {
    let currentNumber = index + 1;

    if (currentNumber % 2 === 0) {
      itemsRightCol.push(item);
    } else {
      itemsLeftCol.push(item);
    }
  });

  return (
    <Layout hideHeader hideFooter>
      <Header>
        <ButtonBack onClick={goBack}>
          <IconArrowBack height={26} stroke={black} />
        </ButtonBack>
        <Title>My Wishlist</Title>
      </Header>
      <Categories categories={categories} selectedCategory={selectedCategory} />
      <Tools>
        <ItemsCount>{items.length} Items</ItemsCount>
        <ButtonSort>
          <ButtonSortText>Sort</ButtonSortText>
          <IconArrowDown />
        </ButtonSort>
      </Tools>
      <ItemsGrid>
        <Left>
          {itemsLeftCol.map((item, index) => (
            <ProductItem key={item.id} item={item} isBigImage={(index + 1) % 2 === 0} onUserLike={onUserLike} />
          ))}
        </Left>
        <Right>
          {itemsRightCol.map((item, index) => (
            <ProductItem key={item.id} item={item} isBigImage={(index + 1) % 2 !== 0} onUserLike={onUserLike} />
          ))}
        </Right>
      </ItemsGrid>
    </Layout>
  );
};

export default Wishlist;
