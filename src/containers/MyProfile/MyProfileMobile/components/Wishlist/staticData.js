import sliderImg1 from './img/sliderImg1.png';
import sliderImg2 from './img/sliderImg2.png';
import sliderImg3 from './img/sliderImg3.png';

import clothingsImg1 from './img/clothingsImg1.png';
import clothingsImg2 from './img/clothingsImg2.png';
import clothingsImg3 from './img/clothingsImg3.png';
import clothingsImg4 from './img/clothingsImg4.png';
import clothingsImg5 from './img/clothingsImg5.png';
import clothingsImg6 from './img/clothingsImg6.png';
import clothingsImg7 from './img/clothingsImg7.png';
import clothingsImg8 from './img/clothingsImg8.png';

import shoesImg1 from './img/shoesImg1.png';
import shoesImg2 from './img/shoesImg2.png';
import shoesImg3 from './img/shoesImg3.png';

import bagsAccessoriesImg1 from './img/bagsAccessoriesImg1.png';
import bagsAccessoriesImg2 from './img/bagsAccessoriesImg2.png';
import bagsAccessoriesImg3 from './img/bagsAccessoriesImg3.png';
import bagsAccessoriesImg4 from './img/bagsAccessoriesImg4.png';
import bagsAccessoriesImg5 from './img/bagsAccessoriesImg5.png';
import bagsAccessoriesImg6 from './img/bagsAccessoriesImg6.png';
import bagsAccessoriesImg7 from './img/bagsAccessoriesImg7.png';
import bagsAccessoriesImg8 from './img/bagsAccessoriesImg8.png';

import beautyHealthImg1 from './img/beautyHealthImg1.png';
import beautyHealthImg2 from './img/beautyHealthImg2.png';
import beautyHealthImg3 from './img/beautyHealthImg3.png';
import beautyHealthImg4 from './img/beautyHealthImg4.png';
import beautyHealthImg5 from './img/beautyHealthImg5.png';
import beautyHealthImg6 from './img/beautyHealthImg6.png';
import beautyHealthImg7 from './img/beautyHealthImg7.png';
import beautyHealthImg8 from './img/beautyHealthImg8.png';

import electronicsImg1 from './img/electronicsImg1.png';
import electronicsImg2 from './img/electronicsImg2.png';
import electronicsImg3 from './img/electronicsImg3.png';
import electronicsImg4 from './img/electronicsImg4.png';
import electronicsImg5 from './img/electronicsImg5.png';
import electronicsImg6 from './img/electronicsImg6.png';
import electronicsImg7 from './img/electronicsImg7.png';
import electronicsImg8 from './img/electronicsImg8.png';

export const navLinks = [
  {id: 1, href: '#clothings', title: 'Clothings'},
  {id: 2, href: '#shoes', title: 'Shoes'},
  {id: 3, href: '#bags-accessories', title: 'Bags & Accessories'},
  {id: 4, href: '#beauty-health', title: 'Beauty & Health'},
  {id: 5, href: '#electronics', title: 'Electronics'}
];

export const sliderImages = [
  {id: 1, img: sliderImg1},
  {id: 2, img: sliderImg2},
  {id: 3, img: sliderImg3}
];

export const clothingsData = [
  {
    id: 1,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg1,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: true,
    isWished: false
  },
  {
    id: 2,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg8,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: true,
    isWished: false
  },
  {
    id: 3,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg3,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 4,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg4,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 5,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg5,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 6,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg6,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 7,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg7,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 8,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: clothingsImg8,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  }
];

export const shoesData = [
  {
    id: 9,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: shoesImg1,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 10,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: shoesImg2,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 11,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: shoesImg3,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  }
];

export const bagsAccessoriesData = [
  {
    id: 12,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg1,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 13,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg2,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 14,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg3,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 15,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg4,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 16,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg5,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 17,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg6,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 18,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg7,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 19,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: bagsAccessoriesImg8,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  }
];

export const beautyHealthData = [
  {
    id: 20,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg1,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 21,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg2,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 22,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg3,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 23,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg4,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 24,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg5,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 25,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg6,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 26,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg7,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 27,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: beautyHealthImg8,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  }
];

export const electronicsData = [
  {
    id: 28,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg1,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 29,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg2,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 30,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg3,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 31,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg4,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 32,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg5,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 33,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg6,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 34,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg7,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  },
  {
    id: 35,
    name: 'Queen Sweatshirt Funny Lou Bega ...',
    description: 'Free Shipping',
    img: electronicsImg8,
    price: 120,
    currency: 'BD',
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isLiked: false,
    isWished: false
  }
];

export const allData = [
  ...shoesData,
  ...clothingsData,
  ...bagsAccessoriesData,
  ...beautyHealthData,
  ...electronicsData
];

export const getItems = (category) => {
  switch (category) {
    case 'clothing':
      return clothingsData;
    case 'shoes':
      return shoesData;
    case 'bagsAccessoires':
      return bagsAccessoriesData;
    case 'beautyHealth':
      return beautyHealthData;
    case 'electronics':
      return electronicsData;
    default:
      return allData;
  }
};
