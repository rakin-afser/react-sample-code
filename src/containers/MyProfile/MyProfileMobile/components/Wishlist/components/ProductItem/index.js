import React from 'react';
import {ThemeProvider} from 'styled-components';
import IconMore from 'assets/More';
import IconHeart from 'assets/Heart';
import IconFilledHeart from 'assets/FilledHeart';
import {truncateText} from 'util/heplers';
import {
  Item,
  Image,
  Body,
  Price,
  CurrentPrice,
  CurrentPriceCurrency,
  CurrentPriceValue,
  PriceWithoutDiscount,
  Name,
  Label,
  DiscountLabel,
  ButtonMore,
  Likes,
  ButtonLike,
  LikesNumber
} from './styled';

const ProductItem = ({
  item: {id, name, img, price, currency, isDiscount, discountPercent, priceWithoutDiscount, isLiked},
  isBigImage,
  onUserLike
}) => (
  <Item>
    <Image src={img} $isBigImage={isBigImage} />
    <Body>
      <ThemeProvider theme={{isDiscount}}>
        <Price>
          <CurrentPrice>
            <CurrentPriceCurrency>{currency}</CurrentPriceCurrency>
            <CurrentPriceValue>{price.toFixed(2)}</CurrentPriceValue>
          </CurrentPrice>
          {isDiscount && (
            <PriceWithoutDiscount>
              {currency}
              {priceWithoutDiscount.toFixed(2)}
            </PriceWithoutDiscount>
          )}
        </Price>
      </ThemeProvider>
      <Name>{truncateText(name, 20)}</Name>
      <Label>Free Shipping</Label>
      <Likes>
        <ButtonLike $isLiked={isLiked} onClick={() => onUserLike(id)}>
          <IconHeart width={14} height={11} isLiked={false} />
          <IconFilledHeart width={14} height={11} />
        </ButtonLike>
        <LikesNumber>127</LikesNumber>
      </Likes>
    </Body>
    <DiscountLabel>-{discountPercent}%</DiscountLabel>
    <ButtonMore>
      <IconMore />
    </ButtonMore>
  </Item>
);

export default ProductItem;
