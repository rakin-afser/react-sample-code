import styled from 'styled-components';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  primaryColor as primary,
  bookmarkFillColor as gray900,
  secondaryColor as secondary
} from 'constants/colors';

export const Item = styled.div`
  position: relative;
`;

export const Image = styled.img`
  width: 100%;
  height: ${({$isBigImage}) => ($isBigImage ? '190px' : '168px')};
  object-fit: cover;
  border-radius: 4px;
`;

export const Body = styled.div`
  padding: 5px 0 10px;
  position: relative;
`;

export const Price = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  margin-bottom: 5px;
`;

export const CurrentPrice = styled.p`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  color: ${({theme: {isDiscount}}) => (isDiscount ? primary : gray900)};
  margin-bottom: 0;
  line-height: 1;
`;

export const CurrentPriceCurrency = styled.span`
  font-size: 10px;
`;

export const CurrentPriceValue = styled.span`
  font-size: 14px;
  font-weight: 600;
  letter-spacing: -0.8px;
`;

export const PriceWithoutDiscount = styled.p`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  color: ${gray900};
  text-decoration: line-through;
  line-height: 1;
  margin: 0 0px 1px 5px;
`;

export const Name = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: ${black};
`;

export const Label = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 300;
  font-size: 10px;
  line-height: 140%;
  color: ${gray900};
`;

export const DiscountLabel = styled.div`
  background-color: ${secondary};
  color: ${white};
  position: absolute;
  top: 3px;
  right: 3px;
  border-radius: 2px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  padding: 6px 2px;
`;

export const ButtonMore = styled.button`
  transform: rotate(90deg);
  position: absolute;
  right: 5px;
  bottom: 10px;
`;

export const Likes = styled.div`
  background: transparent;
  position: absolute;
  right: 8px;
  top: -12px;
  text-align: center;
`;

export const ButtonLike = styled.button`
  border-radius: 50%;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.08);
  background: ${white};
  width: 24px;
  height: 24px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  ${({$isLiked}) =>
    $isLiked
      ? `
    svg.iconHeart {
      display: none;
    }

    svg.iconFilledHeart {
      display: block;
    }
  `
      : `
    svg.iconHeart {
      display: block;
    }

    svg.iconFilledHeart {
      display: none;
    }
  `}
`;

export const LikesNumber = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  color: ${black};
  margin-top: 2px;
`;
