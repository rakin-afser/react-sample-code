import React from 'react';
import qs from 'qs';
import {Wrapper, Categories, Category} from './styled';

export default ({categories, selectedCategory}) => (
  <Wrapper>
    <Categories>
      {categories.map((category) => {
        let to = '/profile/lists/wishlist';

        if (category.name) {
          to += qs.stringify({category: category.name}, {addQueryPrefix: true});
        }

        return (
          <Category key={category.id} to={to} $isActive={category.name === selectedCategory}>
            {category.title}
          </Category>
        );
      })}
    </Categories>
  </Wrapper>
);
