import React, {useEffect, useState} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import {Select} from 'antd';

import {Wrap, Header, RowFields, Radio, GroupTitle, Cancel, Save, Footer, Photo} from './styled';
import Input from 'components/Input';
import SelectField from 'components/SelectField';
import AreaField from 'components/AreaField';
import InputFile from 'components/InputFile';
import {country, days, months, years} from 'constants/staticData';
import {useMutation} from '@apollo/client';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import {UpdatedUser} from 'mutations';
import axiosClient from 'axiosClient';
import avatarPlaceholder from 'images/avatarPlaceholder.png';

const {Option} = Select;

const Edit = () => {
  const [, globalActions] = useGlobal();
  const {userParam} = useParams();
  const [user, setUser] = useUser();
  const {push} = useHistory();
  useEffect(() => {
    globalActions.setHeaderBackButtonLink(`/profile/${userParam ? `${userParam}/` : ''}settings`);
  }, [globalActions, userParam]);

  const [updateUser] = useMutation(UpdatedUser);

  const [loading, setLoading] = useState(false);
  const [photo, setPhoto] = useState('');
  const [countryName, setCountryName] = useState(user?.country);
  const [description, setDescription] = useState(user?.description);
  const [date, setDate] = useState('');
  const [month, setMonth] = useState('');
  const [year, setYear] = useState('');
  const [formDataEdit, setFormDataEdit] = useState({
    avatar: user?.avatar?.url || avatarPlaceholder,
    firstName: user?.firstName || '',
    lastName: user?.lastName || '',
    username: user?.email || '',
    gender: user?.gender || '',
    job_title: user?.job_title || '',
    company: user?.company || '',
    education: user?.education || ''
  });

  const {avatar, firstName, lastName, username, gender, job_title, company, education} = formDataEdit;

  const dateOfBirth = `${date}/${month}/${year}`;

  const onInputChange = (event) => {
    setFormDataEdit({...formDataEdit, [event.target.name]: event.target.value});
  };

  const onPhotoChange = (e) => {
    const reader = new FileReader();
    const file = e.target.files[0];

    const formDataImage = new FormData();
    formDataImage.append('file', e.target.files[0]);

    reader.onloadend = () => {
      setFormDataEdit({...formDataEdit, avatar: reader.result});
      setPhoto(formDataImage);
    };

    reader.readAsDataURL(file);
  };

  const updateUserFunc = (response) => {
    updateUser({
      variables: {
        input: {
          id: user?.userId,
          firstName,
          lastName,
          nickname: username,
          gender,
          company,
          education,
          job_title,
          country: countryName,
          description,
          date_of_birth: dateOfBirth,
          profile_picture: response?.data?.source_url
        }
      },
      update(cache, {data}) {
        cache.modify({
          fields: {
            user() {
              setUser(data.updateUser.user);
              return data.updateUser.user;
            }
          }
        });
      }
    });
  };

  const onSubmitUserUpdate = (e) => {
    e.preventDefault();
    setLoading(true);

    if (photo) {
      axiosClient({
        method: 'post',
        headers: {
          'Content-Disposition': `filename=${photo.name}`
        },
        data: photo
      })
        .then((response) => {
          if (response) {
            updateUserFunc(response);
            setLoading(false);

            if (!loading) {
              push(`/profile/${userParam ? `${userParam}/` : ''}posts`);
            }
          }
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.log(error);
          setLoading(false);
        });
    } else {
      updateUserFunc();
      if (!loading) {
        push(`/profile/${userParam ? `${userParam}/` : ''}posts`);
      }
    }
  };

  return (
    <Wrap>
      <form>
        <Header>
          <Photo src={avatar} alt="user avatar" />
          <InputFile onChange={onPhotoChange}>Change Photo</InputFile>
        </Header>
        <Input key="first" name="firstName" type="text" label="First Name" value={firstName} onChange={onInputChange} />
        <Input key="last" name="lastName" type="text" label="Last Name" value={lastName} onChange={onInputChange} />
        <Input
          key="user"
          name="username"
          type="text"
          label="Username"
          value={username}
          onChange={onInputChange}
          readOnly
        />
        <RowFields bottom>
          <SelectField key="day" label="Birthday" onChange={(value) => setDate(value)}>
            {days.map((key) => (
              <Option value={key} key={key}>
                {key}
              </Option>
            ))}
          </SelectField>
          <SelectField onChange={(value) => setMonth(value)}>
            {months.map((key) => (
              <Option value={key} key={key}>
                {key}
              </Option>
            ))}
          </SelectField>
          <SelectField onChange={(value) => setYear(value)}>
            {years.map((key) => (
              <Option value={key} key={key}>
                {key}
              </Option>
            ))}
          </SelectField>
        </RowFields>
        <GroupTitle>Gender</GroupTitle>
        <RowFields>
          <Radio>
            <Input
              type="radio"
              name="gender"
              id="male"
              value="male"
              label="Male"
              checked={gender === 'male' ? 'male' : ''}
              onChange={onInputChange}
            />
          </Radio>
          <Radio>
            <Input
              type="radio"
              name="gender"
              id="female"
              value="female"
              label="Female"
              checked={gender === 'female' ? 'female' : ''}
              onChange={onInputChange}
            />
          </Radio>
          <Radio>
            <Input
              type="radio"
              name="gender"
              id="others"
              value="others"
              label="Others"
              checked={gender === 'others' ? 'others' : ''}
              onChange={onInputChange}
            />
          </Radio>
        </RowFields>
        <SelectField label="Country" onChange={(value) => setCountryName(value)} defaultValue={user?.country}>
          {Object.keys(country).map((key) => (
            <Option value={key} key={key}>
              {key}
            </Option>
          ))}
        </SelectField>
        <AreaField
          label="About me"
          placeholder="Write something about yourself"
          name="description"
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
        <Input
          key="job"
          name="job_title"
          type="text"
          label="Job Title"
          placeholder="Placeholder"
          value={job_title}
          onChange={onInputChange}
        />
        <Input
          key="company"
          name="company"
          type="text"
          label="Company"
          placeholder="Placeholder"
          value={company}
          onChange={onInputChange}
        />
        <Input
          key="university"
          name="education"
          type="text"
          label="University"
          placeholder="Placeholder"
          value={education}
          onChange={onInputChange}
        />
        <Footer>
          <Cancel>Cancel</Cancel>
          <Save onClick={onSubmitUserUpdate} shape="round" loading={loading}>
            Save
          </Save>
        </Footer>
      </form>
    </Wrap>
  );
};

export default Edit;
