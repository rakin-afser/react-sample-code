import {Button} from 'antd';
import styled from 'styled-components';

export const Wrap = styled.div`
  padding: 16px 16px;
`;

export const Header = styled.div`
  margin-bottom: 24px;
  display: flex;
  align-items: center;
`;

export const Photo = styled.img`
  width: 90px;
  height: 90px;
  border-radius: 90px;
`;

export const RowFields = styled.div`
  display: flex;
  margin: 0 -4px 16px;
  ${({bottom}) => (bottom ? `align-items: flex-end` : '')}

  > div {
    margin: 0 4px;
    flex: 1 1 0%;

    &:nth-child(2) {
      flex-grow: 1.5;
    }
  }
`;

export const Radio = styled.div`
  > div {
    height: 48px;
    margin-bottom: 0;
  }

  .radio {
    display: flex !important;
    color: #000;
    flex-direction: row-reverse;
    justify-content: flex-end;

    label {
      margin-left: 18px;
    }
  }
`;

export const GroupTitle = styled.p`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  margin-bottom: 6px;
  display: block;
`;

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 8px;

  &:hover {
    border-color: #4a90e2;
  }
`;

export const Cancel = styled(Button)`
  &&& {
    font-size: 14px;
    background-color: transparent;
    border-color: transparent;
    color: #000000;
    box-shadow: none;
    max-width: 120px;
    width: 100%;
    margin-left: 16px;
    height: 40px;
    border-radius: 40px;
  }
`;

export const Save = styled(Button)`
  &&& {
    font-size: 14px;
    background-color: #ed484f;
    border-color: #ed484f;
    max-width: 186px;
    width: 100%;
    height: 40px;
    border-radius: 40px;
    color: #fff;
  }
`;
