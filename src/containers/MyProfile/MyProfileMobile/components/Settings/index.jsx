import Icon from 'components/Icon';
import React, {useEffect} from 'react';
import useGlobal from 'store';
import {Wrapper, PageLink} from './styled';

const links = [
  {
    name: 'Edit profile',
    to: '/profile/edit'
  },
  {
    name: 'My Orders',
    to: '/profile/orders'
  },
  {
    name: 'My Rewards',
    to: '/profile/rewards'
  },
  {
    name: 'Messages',
    to: '/profile/messages'
  },
  {
    name: 'Notifications',
    to: '/profile/notifications'
  },
  {
    name: 'Account Settings',
    to: '/account-settings'
  }
];

const SettingsPage = ({}) => {
  const [, globalActions] = useGlobal();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/profile/posts');
  }, []);

  return (
    <Wrapper>
      {links.map((link) => (
        <PageLink key={link.name} to={link.to}>
          {link.name}
          <Icon type="chevronRight" fill="#999" />
        </PageLink>
      ))}
    </Wrapper>
  );
};

export default SettingsPage;
