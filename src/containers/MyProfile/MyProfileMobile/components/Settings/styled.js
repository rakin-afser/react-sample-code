import {secondaryTextColor} from 'constants/colors';
import {Link} from 'react-router-dom';
import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  width: 100%;
  overflow: hidden;
  display: flex;
  align-items: stretch;
  justify-content: flex-start;
  flex-direction: column;
  background-color: #fafafa;
  padding-bottom: 54px;
`;

export const PageLink = styled(Link)`
  font-size: 18px;
  line-height: 1;
  color: ${secondaryTextColor};
  display: flex;
  border-left: 5px solid transparent;
  transition: border-color 0.3s ease-in;
  padding: 21px 30px;
  border-top: 1px solid #efefef;
  position: relative;
  background-color: #fff;

  &:last-child {
    border-bottom: 1px solid #efefef;
  }

  &:hover {
    color: #666666;
    border-left-color: #ed494f;
  }

  i {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 26px;
  }
`;
