import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import Arrow from 'assets/Arrow';
import ShopIcon from 'assets/Shop';
import {useApolloClient} from '@apollo/client';
import {primaryColor} from 'constants/colors';
import {
  Wrapper,
  MenuLink,
  LinkText,
  StartShopBlock,
  ShopText,
  ShopButton,
  Dropdowns,
  DropdownWrapper,
  Profile,
  Avatar,
  Name
} from './styled';
import SelectNative from 'components/SelectNative';
import Icon from 'components/Icon';
import ProfileSwitcher from '../ProfileSwitcher';
import BottomPopup from 'components/Modals/mobile/BottomPopup';

const menuLinks = [
  {id: 'rewards', href: '/profile/rewards', text: 'myProfilePage.rewards', icon: 'coins'},
  {id: 'orders', href: '/profile/orders', text: 'myProfilePage.recentOrders', icon: 'orders'},
  {id: 'activity', href: '/profile/notifications', text: 'My Activity', icon: 'like'},
  {id: 'profile', href: '/profile/posts', text: 'general.myProfile', icon: 'user'},
  {id: 'wishlist', href: '/profile/lists/wishlist', text: 'My Wishlist', icon: 'starOutlined'},
  {id: 'settings', href: '/profile/settings', text: 'myProfilePage.settings', icon: 'gear'}
  // {id: 'logOut', href: '/', text: 'general.logOut', icon: 'logout', withoutArrow: true}
];

const countries = [
  {value: 'bahrain', label: 'Bahrain / BHD'},
  {value: 'ukraine', label: 'Ukraine'},
  {
    value: 'usa',
    label: 'United States of America'
  }
];

const languages = [
  {value: 'en', label: 'English'},
  {value: 'ar', label: 'Arabic'},
  {value: 'ru', label: 'Russian'}
];

const ProfilePage = ({user}) => {
  const [selectedLanguage, setSelectedLanguage] = useState('en');
  const [selectedCountry, setSelectedCountry] = useState('bahrain');
  const [activeProfile, setActiveProfile] = useState('user');
  const [active, setActive] = useState(false);
  const {t} = useTranslation();
  const client = useApolloClient();
  function onSetActiveProfile(profile) {
    setActiveProfile(profile);
    setTimeout(() => {
      setActive(false);
    }, 500);
  }
  const handleLogout = async (e) => {
    e.preventDefault();
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    localStorage.removeItem('woo-session');
    client.resetStore();
    window.location.href = '/';
  };

  return (
    <>
      <Wrapper>
        <Profile onClick={() => setActive(true)}>
          <Avatar>
            <img src={user?.avatar?.url} />
          </Avatar>
          <Name>
            {user?.firstName} {user?.lastName}
            <span>{t('myProfilePage.userProfile')}</span>
          </Name>
          <Icon type="checkmark" />
        </Profile>
        {menuLinks?.map((item) => (
          <MenuLink key={`myProfile-${item.id}`} to={item.href}>
            <Icon type={item.icon} color={'#828282'} fill={'#828282'} stroke={'#828282'} width={15} height={15} />
            <LinkText>{t(item.text)}</LinkText>
            {!item.withoutArrow ? <Icon type="chevronRight" height={13} fill="#BDBDBD" /> : null}
          </MenuLink>
        ))}
        <MenuLink onClick={handleLogout}>
          <Icon type="logout" color={'#828282'} fill={'#828282'} stroke={'#828282'} width={15} height={15} />
          <LinkText>Log Out</LinkText>
          <Icon type="chevronRight" height={13} fill="#BDBDBD" />
        </MenuLink>
        {user?.seller && (
          <StartShopBlock>
            <ShopIcon />
            <ShopText>{t('general.startAShop')}</ShopText>
            <ShopButton>
              {t('general.now')} <Arrow height={8} color={primaryColor} />
            </ShopButton>
          </StartShopBlock>
        )}
        <Dropdowns>
          <DropdownWrapper>
            <SelectNative
              label={t('general.country')}
              options={countries}
              value={selectedCountry}
              onChange={(value) => setSelectedCountry(value)}
            />
          </DropdownWrapper>
          <DropdownWrapper>
            <SelectNative
              label={t('general.language')}
              options={languages}
              value={selectedLanguage}
              onChange={(value) => setSelectedLanguage(value)}
            />
          </DropdownWrapper>
        </Dropdowns>
      </Wrapper>
      {/* disabled at the moment */}
      {/* <BottomPopup active={active} setActive={setActive}>
        <ProfileSwitcher
          user={user}
          setActive={() => setActive(false)}
          activeProfile={activeProfile}
          onSetActiveProfile={onSetActiveProfile}
        />
      </BottomPopup> */}
    </>
  );
};

export default ProfilePage;
