import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';

import {primaryColor, midCoinsColor, bookmarkFillColor} from 'constants/colors';

export const Wrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  padding-bottom: 100px;
  overflow: hidden;
  display: flex;
  align-items: stretch;
  justify-content: flex-start;
  flex-direction: column;
  background-color: #fafafa;
  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const MenuLink = styled(Link)`
  padding: 22px 28px 22px 26px;
  display: flex;
  align-items: center;
  background-color: #fff;
  border-bottom: 1px solid #efefef;
  color: #000;

  &:first-of-type {
    color: #398287;

    i:first-child svg {
      stroke: currentColor;
    }
  }

  & svg {
    flex-shrink: 0;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    & svg {
      transform: scale(-1,1);
    }
  `}
`;

export const LinkText = styled.div`
  line-height: 1.1;
  margin: 0 12px;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  flex-grow: 1;
`;

export const StartShopBlock = styled.div`
  margin-top: 8px;
  padding: 14px 26px 14px 24px;
  display: flex;
  align-items: center;
  background-color: #fff;

  & svg {
    flex-shrink: 0;
  }
`;

export const ShopText = styled(LinkText)`
  font-weight: 700;
  margin: 0 13px;
  line-height: 1.5;
`;

export const ShopButton = styled.button`
  color: ${primaryColor};
  border: 1px solid ${primaryColor};
  border-radius: 24px;
  padding: 7px 20px 7px 26px;
  line-height: 1;
  font-weight: 500;
  font-size: 12px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    padding: 7px 26px 7px 20px;
    & svg {
      transform: scale(-1,1);
    }
  `}
`;

export const Dropdowns = styled.div`
  margin-top: 24px;
`;

export const DropdownWrapper = styled.div`
  padding: 0 16px;
  margin-bottom: 16px;

  & .ant-select-selection {
    display: flex;
    align-items: center;

    &.ant-select-selection--single {
      color: #000;
      font-weight: bold;
      font-size: 14px;
      border: 1px solid #a7a7a7;
      border-radius: 2px;

      &:active,
      &:focus,
      &:hover {
        border: 1px solid #a7a7a7;
        box-shadow: none;
      }

      ${({theme: {isArabic}}) =>
        isArabic &&
        `
        padding-right: 8px;
      `}
    }
  }

  & .ant-select-arrow {
    path {
      fill: ${bookmarkFillColor};
    }
  }
`;

export const DropdownLabel = styled.div`
  font-weight: 500;
  font-size: 14px;
  color: #000;
  margin-bottom: 9px;
`;

export const Profile = styled.div`
  padding: 13px 20px;
  background-color: #fff;
  display: flex;
  align-items: center;
  transition: opacity 0.4s;
  opacity: 1;
  cursor: pointer;
  margin-bottom: 8px;

  & svg {
    width: 25px;
    height: auto;
    transition: opacity 0.4s;
    opacity: 1;

    path {
      fill: ${primaryColor};
    }
  }
`;

export const Avatar = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
  width: 56px;
  height: 56px;
  margin-right: 24px;
  border-radius: 50%;

  & img {
    border-radius: 50%;
    width: 52px;
    height: 52px;
  }

  background: linear-gradient(152.45deg, rgba(244, 77, 84, 0.75) 24.39%, rgba(52, 149, 155, 0.75) 75.89%);
  box-shadow: 0px 4px 4px rgba(237, 72, 79, 0.06);

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-right: 0;
    margin-left: 24px;
  `}
`;

export const Name = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #000;
  flex-grow: 1;

  & span {
    display: block;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    color: #999;
    margin-top: 2px;
  }
`;
