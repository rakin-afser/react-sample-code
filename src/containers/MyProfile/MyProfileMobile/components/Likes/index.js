import React, {useEffect, useState} from 'react';
import qs from 'qs';
import {useHistory, useLocation} from 'react-router-dom';
import IconArrowBack from 'assets/ArrowBack';
import {mainBlackColor as black} from 'constants/colors';
import Categories from './components/Categories';
import ProductItem from './components/ProductItem';
import {Header, ButtonBack, Title, Tools, ItemsCount, ItemsCards} from './styled';
import {itemsData} from './staticData';

const categories = [
  {id: 1, title: 'All', name: undefined},
  {id: 2, title: 'Clothing', name: 'clothing'},
  {id: 3, title: 'Shoes', name: 'shoes'},
  {id: 4, title: 'Bags & Accessoires', name: 'bagsAccessoires'},
  {id: 5, title: 'Beauty & Health', name: 'beautyHealth'},
  {id: 6, title: 'Electronics', name: 'electronics'}
];

const Likes = () => {
  const history = useHistory();
  const location = useLocation();

  const selectedCategory = qs.parse(location.search, {ignoreQueryPrefix: true}).category;

  const [items, setItems] = useState(itemsData);

  useEffect(() => {
    setItems(itemsData);
  }, [selectedCategory]);

  const goBack = () => history.push('/profile');

  const onUserLike = (itemId) => setItems(items.filter((item) => item.id !== itemId));

  return (
    <>
      <Header>
        <ButtonBack onClick={goBack}>
          <IconArrowBack height={26} stroke={black} />
        </ButtonBack>
        <Title>My Likes</Title>
      </Header>
      <Categories categories={categories} selectedCategory={selectedCategory} />
      <Tools>
        <ItemsCount>{items.length} Items</ItemsCount>
      </Tools>
      <ItemsCards>
        {items.map((itemData) => (
          <ProductItem key={itemData.id} itemData={itemData} onUserLike={onUserLike} />
        ))}
      </ItemsCards>
    </>
  );
};

export default Likes;
