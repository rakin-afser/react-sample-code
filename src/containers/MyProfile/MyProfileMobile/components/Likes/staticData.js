import productImg1 from './img/product1.jpeg';
import productImg2 from './img/product2.jpeg';
import productImg3 from './img/product3.jpeg';
import productImg4 from './img/product4.png';
import productImg5 from './img/product5.jpeg';
import productImg6 from './img/product6.jpeg';

export const itemsData = [
  {
    id: 1,
    brand: 'Chanel',
    name: 'Chanel Le Fit Creame',
    price: 132.98,
    currency: 'BD',
    isDiscount: true,
    priceWithoutDiscount: 243.99,
    discountPercent: 40,
    image: productImg1,
    isLiked: true,
    likesNumber: 128
  },
  {
    id: 2,
    brand: 'Chanel',
    name: 'Chanel Le Fit Creame',
    price: 132.98,
    currency: 'BD',
    isDiscount: false,
    priceWithoutDiscount: null,
    discountPercent: null,
    image: productImg2,
    isLiked: true,
    likesNumber: 1294
  },
  {
    id: 3,
    brand: 'Chanel',
    name: 'Chanel Le Fit Creame',
    price: 132.98,
    currency: 'BD',
    isDiscount: true,
    priceWithoutDiscount: 243.99,
    discountPercent: 40,
    image: productImg3,
    isLiked: true,
    likesNumber: 128
  },
  {
    id: 4,
    brand: 'Chanel',
    name: 'Chanel Le Fit Creame',
    price: 132.98,
    currency: 'BD',
    isDiscount: false,
    priceWithoutDiscount: null,
    discountPercent: null,
    image: productImg4,
    isLiked: true,
    likesNumber: 1294
  },
  {
    id: 5,
    brand: 'Chanel',
    name: 'Chanel Le Fit Creame',
    price: 132.98,
    currency: 'BD',
    isDiscount: true,
    priceWithoutDiscount: 243.99,
    discountPercent: 40,
    image: productImg5,
    isLiked: true,
    likesNumber: 128
  },
  {
    id: 6,
    brand: 'Chanel',
    name: 'Chanel Le Fit Creame',
    price: 132.98,
    currency: 'BD',
    isDiscount: false,
    priceWithoutDiscount: null,
    discountPercent: null,
    image: productImg6,
    isLiked: true,
    likesNumber: 1294
  }
];
