import React from 'react';
import styled from 'styled-components';
import {Link} from 'react-router-dom';
import {mainBlackColor as black, bgLightGray as gray200} from 'constants/colors';

export const Wrapper = styled.div`
  overflow-x: hidden;
`;

export const Categories = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  margin: 20px 0;
  padding: 0 15px;
  overflow-x: auto;
`;

export const Category = styled(({$isActive, ...rest}) => <Link {...rest} />)`
  padding: 5px 10px;
  white-space: nowrap;
  border: 1px solid ${gray200};
  border-radius: 24px;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  margin-right: 8px;
  color: ${({$isActive}) => ($isActive ? black : '#666666')};
  background-color: ${({$isActive}) => ($isActive ? gray200 : 'transparent')};

  &:focus,
  &:hover {
    color: ${black};
    background-color: ${gray200};
  }
`;
