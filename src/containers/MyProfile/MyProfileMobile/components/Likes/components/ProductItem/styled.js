import styled from 'styled-components';
import {
  mainBlackColor as black,
  bookmarkFillColor as gray900,
  headerShadowColor as gray400,
  primaryColor as primary
} from 'constants/colors';

export const Item = styled.div`
  display: flex;
  flex-direction: row;
  padding: 15px 0;
  position: relative;

  &:before {
    position: absolute;
    content: '';
    height: 1px;
    width: 100%;
    left: 0;
    top: 0;
    background-color: ${gray400};
  }

  p {
    margin-bottom: 0;
  }
`;

export const Image = styled.img`
  width: 80px;
  height: 80px;
  object-fit: cover;
`;

export const Info = styled.div`
  flex: 1;
  position: relative;
  padding: 5px 0 0 20px;
`;

export const Brand = styled.h5`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: ${gray900};
  margin-bottom: 5px;
`;

export const Name = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${black};
  margin-bottom: 10px;
`;

export const Price = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
`;

export const CurrentPrice = styled.p`
  font-weight: 500;
`;

export const CurrentPriceCurrency = styled.span`
  font-size: 12px;
  color: #999999;
`;

export const CurrentPriceValue = styled.span`
  font-size: 14px;
  color: ${primary};
`;

export const PriceWithoutDiscount = styled.p`
  font-size: 12px;
  font-weight: 500;
  margin-left: 7px;
  text-decoration: line-through;
  color: #999999;
`;

export const DiscountPercent = styled.p`
  font-size: 12px;
  font-weight: 500;
  margin-left: 7px;
  color: ${primary};
`;

export const Likes = styled.div`
  position: absolute;
  top: 0;
  right: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const ButtonLike = styled.button``;

export const LikesNumber = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: ${black};
`;

export const ButtonShowDetails = styled.button`
  position: absolute;
  bottom: 2px;
  right: 13px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;
