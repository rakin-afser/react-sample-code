import React from 'react';
import IconArrowRight from 'assets/Arrow';
import IconFilledHeart from 'assets/FilledHeart';
import {
  Item,
  Image,
  Info,
  Brand,
  Name,
  Price,
  CurrentPrice,
  CurrentPriceCurrency,
  CurrentPriceValue,
  PriceWithoutDiscount,
  DiscountPercent,
  Likes,
  ButtonLike,
  LikesNumber,
  ButtonShowDetails
} from './styled';

const ProductItem = ({
  itemData: {
    id,
    brand,
    name,
    price,
    currency,
    isDiscount,
    priceWithoutDiscount,
    discountPercent,
    image,
    isLiked,
    likesNumber
  },
  onUserLike
}) => (
  <Item>
    <Image src={image} />
    <Info>
      <Brand>{brand}</Brand>
      <Name>{name}</Name>
      <Price>
        <CurrentPrice>
          <CurrentPriceCurrency>{currency}</CurrentPriceCurrency>
          <CurrentPriceValue $isDiscount={isDiscount}>{price}</CurrentPriceValue>
        </CurrentPrice>
        {isDiscount && (
          <>
            <PriceWithoutDiscount>
              {currency}
              {priceWithoutDiscount}
            </PriceWithoutDiscount>
            <DiscountPercent>-{discountPercent}%</DiscountPercent>
          </>
        )}
      </Price>
      <Likes>
        <ButtonLike onClick={() => onUserLike(id)} $isLiked={isLiked}>
          <IconFilledHeart />
        </ButtonLike>
        <LikesNumber>{likesNumber}</LikesNumber>
      </Likes>
      <ButtonShowDetails>
        <IconArrowRight color="#CCCCCC" height={15} width={9} />
      </ButtonShowDetails>
    </Info>
  </Item>
);

export default ProductItem;
