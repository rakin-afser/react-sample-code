import styled from 'styled-components';
import {mainWhiteColor as white, mainBlackColor as black, transparentTextColor as gray300} from 'constants/colors';

export const Header = styled.header`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding: 10px 25px;
  border-bottom: 1px solid #efefef;
  background-color: ${white};
  box-shadow: 0px 2px 13px rgba(0, 0, 0, 0.08);
`;

export const ButtonBack = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.div`
  flex: 1;
  margin-left: 30px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: ${black};
`;

export const Tools = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 15px;
  margin-bottom: 10px;
`;

export const ItemsCount = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: ${gray300};
`;

export const ItemsCards = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  max-width: 450px;
  margin: 0 auto;
  padding: 0 15px;
`;
