import React from 'react';
import {useTranslation} from 'react-i18next';
import Div100vh from 'react-div-100vh';
import moment from 'moment';
import {useQuery} from '@apollo/client';

import ArrowBack from 'assets/ArrowBack';
import {TabsWrapper, Tab} from 'containers/MyProfile/MyProfileMobile/components/Posts/styled';
import templateData from 'components/NotificationBar/templateData';
import Search from 'assets/Search';
import AddMessage from 'assets/AddMessage';
import {
  Wrapper,
  BackTo,
  BackIcon,
  AddIcon,
  SearchField,
  SearchWrapper,
  ScrolledContainer,
  NotificationsItem,
  AvatarWrap,
  Avatar,
  ItemDescription,
  ItemText,
  FollowButton,
  ItemPhoto,
  UserStatus,
  MessageDate,
  MessageCounter
} from 'containers/MyProfile/MyProfileMobile/components/Messages/styled';
import Checkmark from 'assets/Checkmark';
import Plus from 'assets/Plus';
import Messenger from 'components/Messenger/index';
import userPlaceholder from 'images/placeholders/user.jpg';
import {GET_NOTIFICATIONS} from 'components/NotificationBar/api/queries';
import {useUser} from 'hooks/reactiveVars';
import Loader from 'components/Loader';

const Messages = ({data}) => {
  const {t} = useTranslation();

  return (
    <Wrapper>
      <SearchWrapper>
        <SearchField placeholder={t('messagesPage.searchInMessages')} />
        <Search color="#999" />
      </SearchWrapper>
      <Messenger />
    </Wrapper>
  );
};

const Notifications = ({}) => {
  const [user] = useUser();
  const {data, loading, error} = useQuery(GET_NOTIFICATIONS, {variables: {id: user?.databaseId}});

  return (
    <ScrolledContainer>
      {loading && <Loader wrapperWidth="100%" wrapperHeight="100px" />}
      {!loading && !data?.notifications?.length && <NotificationsItem>You have no notifications yet</NotificationsItem>}
      {data?.notifications?.map((item, i) => (
        <NotificationsItem key={`notifications-${item.id}`}>
          <Avatar src={item.avatar || userPlaceholder} />
          <ItemDescription>
            <ItemText name>!username!</ItemText>
            {` `}
            <ItemText gray>{moment(item.notification_time).fromNow()}</ItemText>
            {` `}
            <ItemText>{item.message}</ItemText>
          </ItemDescription>
          {item.type === 'follow' || item.type === 'following' ? (
            <FollowButton transparent={item.type === 'following'}>
              {item.type === 'follow' ? <Plus /> : <Checkmark width={15} />}
              {item.type}
            </FollowButton>
          ) : (
            <ItemPhoto src={item.postPreview} />
          )}
        </NotificationsItem>
      ))}
    </ScrolledContainer>
  );
};

const Page = ({page}) => {
  const {t} = useTranslation();

  return (
    <Div100vh>
      <Wrapper>
        <BackTo>
          <BackIcon to="/profile/posts">
            <ArrowBack stroke="#000" />
          </BackIcon>
          {t('general.messages')} & {t('general.notifications')}
          {page === 'messages' && (
            <AddIcon to="#">
              <AddMessage width={22} stroke="#000" />
            </AddIcon>
          )}
        </BackTo>
        <TabsWrapper activeTab={page === 'messages' ? 1 : 2}>
          <Tab isActive={page === 'messages'} to="/profile/messages">
            {t('general.messages')}
          </Tab>
          <Tab isActive={page === 'notifications'} to="/profile/notifications">
            {t('general.notifications')}
          </Tab>
        </TabsWrapper>
        {page === 'notifications' ? (
          <Notifications forMobile data={templateData.notifications} />
        ) : (
          <Messages forMobile data={templateData.message} />
        )}
      </Wrapper>
    </Div100vh>
  );
};

export default Page;
