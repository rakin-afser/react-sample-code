import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';

import {primaryColor, grayTextColor} from 'constants/colors';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  padding-bottom: 8px;
  overflow: hidden;
  display: flex;
  align-items: stretch;
  justify-content: flex-start;
  flex-direction: column;
  background-color: #fafafa;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const BackIcon = styled(Link)`
  position: absolute;
  left: 10px;
  transform: translateY(-2px);

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
      left: unset;
      right: 10px;
      transform: scale(-1,1) translateY(-2px);
      `}
`;

export const AddIcon = styled(Link)`
  position: absolute;
  right: 16px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    right: unset;
    left: 16px;
  `}
`;

export const BackTo = styled.div`
  padding: 12px 14px;
  border-bottom: 1px solid #efefef;
  font-style: normal;
  font-weight: 500;
  line-height: 120%;
  font-size: 18px;
  color: #000;
  position: relative;
  text-align: center;
`;

export const SearchWrapper = styled.div`
  width: calc(100% - 32px);
  position: relative;
  margin: 16px auto;

  & svg {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    left: 18px;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    & svg {
      right: 18px;
      left: unset;
    }
  `}
`;

export const SearchField = styled.input`
  width: 100%;
  outline: none;
  background: #efefef;
  border-radius: 4px;
  border: none;
  padding: 10px 18px 10px 49px;
  font-size: 14px;
  color: #000;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
    padding: 10px 49px 10px 18px;
  `}

  &:placeholder {
    color: #999;
  }
`;

export const ScrolledContainer = styled.div`
  overflow-y: auto;
  //padding-bottom: 20px;
`;

export const NotificationsItem = styled.div`
  position: relative;
  display: flex;
  align-items: flex-start;
  border-top: 1px solid #efefef;
  padding: 16px;
  transition: box-shadow ease 0.4s;
  font-weight: 400;

  &:first-child {
    border-top: none;
  }

  ${({message}) =>
    message &&
    `
    padding: 12px 16px;
    align-items: center;
    &:hover {
      cursor: pointer;
      box-shadow: 0 0 11px rgba(0,0,0,0.2);
    }
  `}
`;

export const ItemDescription = styled.div`
  margin: 0 10px;
  flex-grow: 1;
`;

export const Avatar = styled.img`
  display: block;
  flex-shrink: 0;
  object-fit: cover;
  border-radius: 50%;
  width: ${({message}) => (message ? '55px' : '40px')};
  height: ${({message}) => (message ? '55px' : '40px')};
  margin: 0 0 auto 0;
`;

export const ItemText = styled.span`
  font-size: 14px;
  color: ${({gray}) => (gray ? '#999' : '#000')};

  ${({message}) =>
    message &&
    `
      display: block;
      text-overflow: ellipsis;
      white-space: nowrap;
      max-width: 60vw;
      overflow: hidden;
      margin: 0;
      color: #666666;
  `}
  ${({title}) =>
    title &&
    `
      font-weight: 500;
      display: block;
      margin: 0 0 6px 0;
  `}
  ${({name}) =>
    name &&
    `
    font-weight: 500;
    `}

  ${({theme: {isArabic}, message, title}) =>
    isArabic &&
    !message &&
    !title &&
    `
    display: inline-block;
  `}
`;

export const ItemPhoto = styled.img`
  display: block;
  max-width: 100%;
  margin-left: 10px;
  flex-shrink: 0;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-left: 0;
    margin-right: 10px;
  `}
`;

export const FollowButton = styled.div`
  font-size: 12px;
  border-radius: 24px;
  color: #fff;
  background: ${primaryColor};
  text-transform: capitalize;
  padding: 4px 15px;
  display: flex;
  cursor: pointer;
  min-width: 86px;
  align-items: center;
  justify-content: center;
  transition: box-shadow ease 0.4s;

  & svg {
    margin-right: 11px;
    flex-shrink: 0;
    path {
      fill: #fff;
    }
  }

  &:hover {
    box-shadow: 0 3px 4px rgba(0, 0, 0, 0.2);
  }

  ${({transparent}) =>
    transparent &&
    `
      padding: 3px 6px;
      min-width: unset;
      background: transparent;
      color: ${grayTextColor};
      border: 1px solid ${grayTextColor};
      & svg{
        margin-right: 7px;
        path{
          fill: ${grayTextColor};
        }
      }
    `};

  ${({theme: {isArabic}, transparent}) =>
    isArabic &&
    `
    & svg {
      margin-right: 0;
      margin-left: ${transparent ? '7' : '11'}px;
    }
  `}
`;

export const MessageDate = styled.span`
  position: absolute;
  right: 16px;
  top: 12px;
  font-size: 14px;
  font-weight: 300;
  color: #999;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    right: unset;
    left: 16px;
  `}
`;

export const MessageCounter = styled.i`
  position: absolute;
  right: 14px;
  bottom: 17px;
  display: flex;
  justy-content: center;
  align-items: center;
  border-radius: 50%;
  background: #ed484f;
  color: #fff;
  font-weight: 300;
  font-size: 12px;
  padding: 0 5px;
  font-style: normal;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    right: unset;
    left: 14px;
  `}
`;

export const UserStatus = styled.i`
  display: block;
  position: absolute;
  bottom: 0;
  right: 0;
  width: 13px;
  height: 13px;
  border-radius: 50%;
  border: 2px solid #fff;
  background: #2ecc71;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    right: unset;
    left: 0;
  `}
`;

export const AvatarWrap = styled.div`
  position: relative;
`;
