import React, {useState} from 'react';
import {useParams} from 'react-router';
import {isEmpty} from 'lodash';

import Rewards from './components/Rewards';
import Statistic from './components/Statistic';
import Transactions from './components/Transactions';
// import ShopCoins from './components/ShopCoins';
import WhatIs from './components/WhatIs';
import Referral from './components/Referral';
import Balance from './components/Balance';
import {useUser} from 'hooks/reactiveVars';
import useRewards from 'hooks/useRewards';
import Pagination from 'components/PagePagination';
import useDebounce from 'hooks/useDebounce';
import {PaginationWrapper} from 'containers/MyProfile/MyProfileMobile/components/Rewards/styled';

const RewardsPage = () => {
  const {slug} = useParams();
  const [user] = useUser();
  const [inputValue, setInputValue] = useState('');
  const debouncedSearchTerm = useDebounce(inputValue, 500);

  const [sort, setSort] = useState({
    sortBy: 'AwardedDate', // sort by date on default
    orderBy: 'DESC'
  });

  const {currentData, loading, error, rewardsPagination} = useRewards({
    variables: {search: debouncedSearchTerm, ...sort}
  });

  const onSearch = () => {
    rewardsPagination.resetPagination();
  };

  const getPage = () => {
    switch (slug) {
      case 'statistic':
        return <Statistic />;
      case 'transactions':
        return (
          <>
            <Transactions
              onSearch={onSearch}
              setSearch={setInputValue}
              search={inputValue}
              loading={loading}
              data={currentData}
              error={error}
            />
            {!(isEmpty(currentData) && rewardsPagination?.currentPage === 1) && (
              <PaginationWrapper>
                <Pagination
                  totalPages={rewardsPagination?.totalPages}
                  prevPage={rewardsPagination?.prevPage}
                  nextPage={rewardsPagination?.nextPage}
                  currentPage={rewardsPagination?.currentPage}
                />
              </PaginationWrapper>
            )}
          </>
        );
      // case 'shopcoins':
      //   return <ShopCoins />;
      case 'balance':
        return <Balance />;
      case 'what-is':
        return <WhatIs />;
      case 'referral':
        return <Referral />;
      case 'rewards':
      default:
        return <Rewards />;
    }
  };

  return getPage();
};

export default RewardsPage;
