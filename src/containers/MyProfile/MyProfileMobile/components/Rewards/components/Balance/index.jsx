import React from 'react';
import {Col, Row, Table} from 'antd';
import moment from 'moment';
import {usetestSampleBalanceQuery, useWalletBalanceQuery} from 'hooks/queries';
import {Heading, Title, Desc, Date, Amount, Status, TableWrapper, BalanceAmount, BalanceWrapper} from './styled';
import Btn from 'components/Btn';
import {useUser} from 'hooks/reactiveVars';
import {useOnScreen} from 'hooks';

const Balance = () => {
  const [user] = useUser();
  const {data: dataWallet} = useWalletBalanceQuery({variables: {id: user?.databaseId}});
  const {walletBalance} = dataWallet?.user || {};

  const columns = [
    {
      title: 'Date',
      dataIndex: 'date-storeName',
      render: (a) => {
        return (
          <>
            <Date>{moment(a?.date).format('DD/MM/YYYY')}</Date>
            <div>{a?.storeName}</div>
          </>
        );
      },
      width: '37.7%'
    },
    {
      title: 'Type',
      dataIndex: 'type-awardedQty',
      render: (a) => {
        return (
          <>
            <div>{a?.type}</div>
            <Amount>US ${a?.awardedQty?.toFixed(2)}</Amount>
          </>
        );
      },
      width: '37.5%'
    },
    {
      title: 'Status',
      dataIndex: 'status',
      render: (r) => <Status>{r}</Status>,
      width: '25%'
    }
  ];

  const {data, loading, error, fetchMore} = usetestSampleBalanceQuery({
    variables: {first: 10},
    notifyOnNetworkStatusChange: true
  });
  const {endCursor, hasNextPage} = data?.testSampleBalanceData?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first: 10,
          after: endCursor
        }
      });
    }
  });
  const endRefEl = hasNextPage && !loading ? <div ref={ref} /> : null;

  const dataSourceFormatted = data?.testSampleBalanceData?.nodes?.map((c) => ({
    id: c?.id,
    'date-storeName': {date: c?.date, storeName: c?.storeName},
    'type-awardedQty': {
      type: c?.type,
      awardedQty: c?.awardedQty
    },
    status: c?.status
  }));

  const settings = {
    rowKey: (r) => r?.id,
    showHeader: false,
    pagination: false,
    columns,
    loading,
    dataSource: error ? null : dataSourceFormatted
  };

  return (
    <>
      <Heading>
        <Row>
          <Col span={12}>
            <Title>Your Balance</Title>
            <Desc>
              You must have a minimum balance of <strong>USD 50</strong> to make a withdrawal
            </Desc>
          </Col>
          <Col span={12}>
            <BalanceWrapper>
              {walletBalance ? <BalanceAmount>USD {walletBalance}</BalanceAmount> : null}
              <Btn kind="pr-bare" ml="auto">
                Withdraw
              </Btn>
            </BalanceWrapper>
          </Col>
        </Row>
      </Heading>
      <TableWrapper>
        <Table {...settings} />
        {endRefEl}
      </TableWrapper>
    </>
  );
};

export default Balance;
