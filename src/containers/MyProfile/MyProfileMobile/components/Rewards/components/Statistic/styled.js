import {Link} from 'react-router-dom';
import styled from 'styled-components';
import {Tab} from 'containers/MyProfile/styled';

export const Wrap = styled.div`
  padding: 24px 16px 0;
`;

export const WrapChart = styled(Wrap)`
  height: 340px;
`;

export const SortTabList = styled.div`
  display: flex;
  text-align: center;
`;

export const SortTab = styled.div`
  font-size: 14px;
  border-bottom: 2px solid #efefef;
  transition: border-color 0.3s ease-in, color 0.3s ease-in;
  padding-top: 10px;
  padding-bottom: 10px;
  flex: 1 0 0%;
  color: #999;

  ${({active}) =>
    active &&
    `
      border-color: #ED484F;
      font-weight: 700;
      color: #000;
    `};
`;

export const Rewards = styled.div`
  display: flex;
  justify-content: space-between;
  max-width: 300px;
  padding: 0 16px;
  margin: 18px auto 0;
`;

export const Reward = styled.div`
  font-size: 14px;
  border-bottom: 2px solid transparent;
  transition: color 0.3s ease-in;
  padding-top: 6px;
  padding-bottom: 10px;
  color: #7a7a7a;
  font-weight: 700;
  position: relative;

  &::after {
    content: '';
    height: 2px;
    border-radius: 5px;
    width: 100%;
    position: absolute;
    left: 0;
    bottom: 0;
    right: 0;
    background-color: transparent;
    transition: background-color 0.3s ease-in;
  }

  ${({active}) =>
    active &&
    `
      color: #000;

      &::after {
        background-color: #000;
      }
    `};
`;

export const DashboardTitle = styled.h5`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: 700;
  font-size: 16px;
  line-height: 150%;
  color: #3a3a3a;
  margin-bottom: 1px;

  span {
    color: #398287;
  }
`;

export const DashboardSubtitle = styled(DashboardTitle)`
  color: #545454;
  margin-bottom: 14px;
`;

export const DashboardStats = styled.ul`
  display: flex;
  justify-content: space-between;

  li {
    color: #656565;
    font-weight: 500;
    font-size: 12px;
  }
`;

export const PageLink = styled(Link)`
  display: flex;
  border-left: 5px solid transparent;
  transition: border-color 0.3s ease-in;
  padding: 11px 43px 11px 30px;
  border-top: 1px solid #efefef;
  position: relative;
  background-color: #fff;
  height: 56px;
  align-items: center;
  font-size: 16px;
  color: #000;
  line-height: 1;
  font-weight: 500;

  &:last-of-type {
    border-bottom: 1px solid #efefef;
  }

  &:hover {
    color: #666666;
    border-left-color: #ed494f;
  }

  i {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 26px;
  }
`;

export const Hr = styled.hr`
  border: 4px solid #fafafa;
  margin-top: ${({mt}) => mt && `${mt}px`};
  margin-bottom: ${({mb}) => mb && `${mb}px`};
`;
