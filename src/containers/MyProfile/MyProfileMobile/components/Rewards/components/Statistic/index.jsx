import React, {useEffect, useState} from 'react';
import {v1} from 'uuid';
import {useQuery} from '@apollo/client';
import moment from 'moment';

import {
  Hr,
  PageLink,
  Wrap,
  DashboardTitle,
  DashboardSubtitle,
  DashboardStats,
  WrapChart,
  SortTab,
  SortTabList,
  Rewards,
  Reward
} from './styled';
import numberWithCommas from 'util/heplers';
import Icon from 'components/Icon';
import Chart from 'containers/MyProfile/components/MyRewards/components/Chart/mobile';
import useGlobal from 'store';
import {GET_REWARDS_STATISTICS} from 'containers/MyProfile/api/queries';
import {useUser} from 'hooks/reactiveVars';

const tabs = ['Week', 'Month', 'Year'];
const rewards = [
  {id: v1(), title: 'Review'},
  {id: v1(), title: 'Lists'},
  {id: v1(), title: 'Referral'}
];

const Statistic = () => {
  const [, globalActions] = useGlobal();
  const [user] = useUser();
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const [activeInnerTabIndex, setActiveInnerTabIndex] = useState(0);
  const {data: rewardsData} = useQuery(GET_REWARDS_STATISTICS, {variables: {userId: user?.databaseId}});

  const earnedMidcoinsInMonth = rewardsData?.rewardsData?.earnedMidcoinsInMonth;
  const earnedMidcoinsInWeek = rewardsData?.rewardsData?.earnedMidcoinsInWeek;
  const earnedMidcoinsInYear = rewardsData?.rewardsData?.earnedMidcoinsInYear;
  const currentDates =
    activeTabIndex === 0 ? earnedMidcoinsInWeek : activeTabIndex === 1 ? earnedMidcoinsInMonth : earnedMidcoinsInYear;
  const currentPeriod = activeTabIndex === 0 ? 7 : activeTabIndex === 1 ? 30 : 365;

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/profile/rewards');
  }, []);

  const smallLinks = [
    {
      name: 'What is MidCoins?',
      to: '/profile/rewards/what-is'
    },
    {
      name: 'Share the Referral',
      to: '/profile/rewards/referral'
    }
  ];

  const getDates = (items = [], timeLine) => {
    const dates = items?.map((item) => ({date: moment(item?.date).format('DD MMM YYYY'), amt: item?.midcoins}));

    return timeLine
      ? timeLine?.map((item) => ({
          date: item,
          amt:
            dates?.find((i) => {
              return i.date === item;
            })?.amt || 0
        }))
      : dates;
  };

  const getTimeLine = (days = 7) => {
    return [...Array(days).keys()]
      .map((item) =>
        moment()
          .subtract(item, 'days')
          .format('DD MMM YYYY')
      )
      .reverse();
  };

  const currentDate = getDates(currentDates?.inDays, getTimeLine(currentPeriod)) || [];

  return (
    <>
      <Wrap>
        <DashboardTitle>
          You have earned <span>{user?.availableMidcoins} Midcoins.</span>
        </DashboardTitle>
        <DashboardSubtitle>Start shopping and redeem your points !</DashboardSubtitle>
        <DashboardStats>
          <li>List: {0}</li>
          <li>Review: {user?.availableMidcoins}</li>
          <li>Referrals: {0}</li>
          <li>Total: {user?.availableMidcoins}</li>
        </DashboardStats>
      </Wrap>
      <Hr mt={16} mb={16} />
      <SortTabList>
        {tabs.map((tab, i) => (
          <SortTab active={activeTabIndex === i} key={i} onClick={() => setActiveTabIndex(i)}>
            {tab}
          </SortTab>
        ))}
      </SortTabList>
      <Rewards>
        {rewards.map((reward, i) => (
          <Reward active={activeInnerTabIndex === i} onClick={() => setActiveInnerTabIndex(i)} key={reward.id}>
            {reward.title}
          </Reward>
        ))}
      </Rewards>
      <WrapChart>
        <Chart data={currentDate} />
      </WrapChart>
      <Hr mt={16} mb={0} />
      {smallLinks.map((link) => (
        <PageLink key={link.name} to={link.to}>
          {link.name}
          <Icon type="chevronRight" fill="currentColor" />
        </PageLink>
      ))}
    </>
  );
};

export default Statistic;
