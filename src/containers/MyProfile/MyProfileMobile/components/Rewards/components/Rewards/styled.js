import {Link} from 'react-router-dom';
import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding-bottom: 54px;
`;

export const PageLink = styled(Link)`
  display: flex;
  border-left: 5px solid transparent;
  transition: border-color 0.3s ease-in;
  padding: 11px 43px 11px 30px;
  border-top: 1px solid #efefef;
  position: relative;
  background-color: #fff;
  height: ${({small}) => `${small ? 56 : 105}px`};
  align-items: center;

  &:last-child {
    border-bottom: 1px solid #efefef;
  }

  &:hover {
    color: #666666;
    border-left-color: #ed494f;
  }

  i {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    right: 26px;
  }
`;

export const Title = styled.p`
  font-size: ${({small}) => `${small ? 16 : 18}px`};
  color: #000;
  line-height: 1;
  margin-bottom: ${({small}) => `${small ? 0 : 8}px`};
  font-weight: 500;
`;

export const Desc = styled.p`
  color: #666;
  margin-bottom: 0;
  font-size: 14px;
  line-height: 1.2;
  max-width: 264px;
`;
