import React, {useEffect} from 'react';
import useGlobal from 'store';
import {Wrapper, PageLink, Desc, Title} from './styled';
import Icon from 'components/Icon';

const links = [
  {
    name: 'Rewards',
    to: '/profile/rewards/statistic',
    desc: 'Here you can find statistics in week, month or year and all the information about your rewards'
  },
  {
    name: 'Transactions',
    to: '/profile/rewards/transactions',
    desc: 'Find more actions and open new options to unlock and review the products'
  },
  {
    name: 'Shop Coins',
    to: '/profile/rewards/shopcoins',
    desc: 'See your recent products and new stores to use your Shop Coins on amazing items'
  }
];

const smallLinks = [
  {
    name: 'What is MidCoins?',
    to: '/profile/rewards/what-is'
  },
  {
    name: 'Share the Referral',
    to: '/profile/rewards/referral'
  }
];

const RewardsPage = () => {
  const [, globalActions] = useGlobal();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/profile/settings');
  }, []);

  return (
    <Wrapper>
      {links.map((link) => (
        <PageLink key={link.name} to={link.to}>
          <div>
            <Title>{link.name}</Title>
            {link.desc && <Desc>{link.desc}</Desc>}
          </div>
          <Icon type="chevronRight" fill="#999" />
        </PageLink>
      ))}
      {smallLinks.map((link) => (
        <PageLink small="small" key={link.name} to={link.to}>
          <Title small="small">{link.name}</Title>
        </PageLink>
      ))}
    </Wrapper>
  );
};

export default RewardsPage;
