import React, {useState, useEffect} from 'react';
import moment from 'moment';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import Skeleton from 'react-loading-skeleton';
import NothingFound from 'components/NothingFound';

import {
  Wrap,
  StyledSearch,
  TransactionList,
  Transaction,
  Img,
  Main,
  Heading,
  Title,
  Type,
  Status,
  Info,
  Date,
  Unlock
} from './styled';
import useGlobal from 'store';
import SideDrawer from './components/SideDrawer';
import productPlaceholder from 'images/placeholders/product.jpg';
import FilterBarMobile from 'components/FilterBarMobile';
import Icon from 'components/Icon';

const Transactions = ({data, loading, setSearch, search, onSearch}) => {
  const [, globalActions] = useGlobal();
  const [drawer, setDrawer] = useState(false);

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/profile/rewards');
  }, [globalActions]);

  const unlockMidCoins = (productData) => {
    globalActions.setLeaveFeedback({
      open: true,
      data: productData
    });
  };

  const getDates = (items = []) => {
    return [...new Set(items?.map((item) => item?.date)?.sort((a, b) => (a < b ? 1 : -1)))];
  };

  function renderInfo(tr) {
    const product = tr?.awardedProduct?.nodes?.[0] || {};

    if (tr.status === 'Pending') {
      return (
        <Unlock onClick={() => unlockMidCoins({product, orderId: tr?.order_id})}>
          Unlock <Icon type="coins" />
        </Unlock>
      );
    }
    if (tr.status === 'Unlocked') {
      return (
        <Status status={tr.status}>
          <div>Valid Until</div>
          {tr.expiry_date}
        </Status>
      );
    }

    return (
      <>
        <Status>{tr.status}</Status>
        <Date status={tr.status}>{tr.rewardDate}</Date>
      </>
    );
  }

  return (
    <>
      <Wrap>
        <FilterBarMobile results="42" onClick={() => setDrawer(true)} />
        <StyledSearch value={search} onSearch={onSearch} onChange={(e) => setSearch(e.target.value)} placeholder="Type something to search..." />
        <TransactionList>
          {loading && (
            <div style={{margin: '15px 0 0 0'}}>
              {[...Array(10).keys()].map((i) => (
                <Skeleton key={i} style={{marginBottom: 5, width: '100%'}} height={70} />
              ))}
            </div>
          )}
          {_.isEmpty(data) && !loading && <NothingFound hideHelpText hideAdditionalText/>}
          {getDates(data)?.map((el) => (
            <React.Fragment key={el}>
              <Transaction>
                <div>{moment(el).format('DD MMM YYYY')}</div>
              </Transaction>
              {data
                ?.filter((i) => i?.date === el)
                ?.map((item, index) => {
                  const {
                    order_id,
                    store_id,
                    id,
                    awardedProduct,
                    awarded_qty,
                    type,
                    date,
                    status,
                  } = item;
                  const product = awardedProduct?.nodes?.[0] || {};
                  const seller = product?.seller;

                  // if (!awarded_qty) return null;

                  return (
                    <Transaction key={id}>
                      <Main>
                        <Link to={`/product/${product?.slug}`}>
                          <Img src={product?.image?.sourceUrl || productPlaceholder} />
                        </Link>
                        <Heading>
                          <Date>{moment(date).format('DD MMM YYYY')}</Date>
                          <Link to={`/shop/${store_id}/products`}>
                            <Title>{seller?.name || '(no name)'}</Title>
                          </Link>
                        </Heading>
                      </Main>
                      <Info>
                        <Type status={status}>
                          {type} <span>+{awarded_qty}</span>
                        </Type>
                        {renderInfo(item)}
                      </Info>
                    </Transaction>
                  );
                })}
            </React.Fragment>
          ))}
        </TransactionList>
      </Wrap>
      <SideDrawer visible={drawer} onClose={() => setDrawer(false)} />
    </>
  );
};

export default Transactions;
