import {Button} from 'antd';
import styled from 'styled-components';

export const TitleContainer = styled.div`
  position: relative;
`;

export const Done = styled(Button)`
  &&& {
    border: none;
    background-color: transparent;
    color: #ed484f;
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);
    padding: 5px;
    height: auto;
  }
`;

export const Title = styled.h4`
  font-size: 18px;
  margin-bottom: 0;
  line-height: 1.2;
  padding-right: ${({px}) => `${px ? px : 0}px`};
  padding-left: ${({px}) => `${px ? px : 0}px`};
  margin-top: ${({my}) => `${my ? my : 0}px`};
  margin-bottom: ${({my}) => `${my ? my : 0}px`};

  ${({center}) =>
    center &&
    `
    text-align: center;
  `};
`;

export const CheckboxGroup = styled.div`
  .ant-checkbox-wrapper {
    padding: 8px 16px 10px;
    position: relative;
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0;
  }

  .ant-checkbox {
    z-index: 2;

    & + span {
      z-index: 1;
      &::before {
        content: '';
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: -1;
        background-color: transparent;
        transition: background-color 0.3s ease-in;
      }
    }

    &-checked {
      & + span {
        &::before {
          background-color: #efefef;
        }
      }
    }
  }
`;
