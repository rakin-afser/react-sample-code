import {v1} from 'uuid';
import img1 from './img/1.jpg';
import img2 from './img/2.jpg';
import img3 from './img/3.jpg';
import img4 from './img/4.jpg';

export const exampleData = [
  {
    id: v1(),
    image: img1,
    store: 'Fenty Beauty Make UP Beauty',
    type: 'MidCoins',
    amount: '8',
    status: false,
    rewardDate: '24 Dec 2019',
    expired: false,
    date: '12 Dec 2019'
  },
  {
    id: v1(),
    image: img2,
    store: 'Jeffree Star Cosmetics',
    type: 'Referral',
    amount: '98',
    status: 'Available',
    rewardDate: '24 Dec 2019',
    expired: false,
    date: '12 Dec 2019'
  },
  {
    id: v1(),
    image: img3,
    store: 'Fenty Beauty',
    type: 'List',
    amount: '16',
    status: 'Valid Until',
    rewardDate: '24 Dec 2019',
    expired: false,
    date: '12 Dec 2019'
  },
  {
    id: v1(),
    image: img4,
    store: 'Tati Beauty',
    type: 'MidCoins',
    amount: '87',
    status: 'Expiring soon',
    rewardDate: '24 Dec 2019',
    expired: true,
    date: '12 Dec 2019'
  },
  {
    id: v1(),
    image: img4,
    store: 'Tati Beauty',
    type: 'ShopCoins',
    amount: '22',
    status: 'Expiring soon',
    rewardDate: '24 Dec 2019',
    expired: true,
    date: '12 Dec 2019'
  },
  {
    id: v1(),
    image: img3,
    store: 'Fenty Beauty',
    type: 'List',
    amount: '16',
    status: 'Valid Until',
    rewardDate: '24 Dec 2019',
    expired: false,
    date: '12 Dec 2019'
  },
  {
    id: v1(),
    image: img3,
    store: 'Fenty Beauty',
    type: 'List',
    amount: '16',
    status: 'Valid Until',
    rewardDate: '24 Dec 2019',
    expired: false,
    date: '12 Dec 2019'
  }
];
