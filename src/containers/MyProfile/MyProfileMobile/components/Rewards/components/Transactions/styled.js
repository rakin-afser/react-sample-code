import styled from 'styled-components';
import {Input, Button} from 'antd';
import {midCoinsColor} from 'constants/colors';

const {Search} = Input;

export const Wrap = styled.div`
  padding: 16px 16px 0;
`;

export const StyledSearch = styled(Search)`
  .anticon svg {
    width: 20px;
    height: 20px;
  }

  &&& {
    margin-top: 16px;
    margin-bottom: 4px;

    .ant-input {
      background-color: #efefef;
      border-radius: 4px;
      border: none;
      box-shadow: none !important;
      padding-left: 49px;
      padding-right: 10px;
      color: #000;

      &::placeholder {
        color: #999999;
      }

      &-suffix {
        left: 18px;
        right: auto;
      }

      &-group-addon {
        background-color: transparent;
        border: none;

        i {
          vertical-align: middle;
        }
      }
    }
  }
`;

export const TransactionList = styled.div`
  font-size: 12px;
  padding-bottom: 24px;
`;

export const Transaction = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 12px;
  border-top: 1px solid #e4e4e4;
  padding-top: 12px;
`;

export const Main = styled.div`
  display: flex;
`;

export const Heading = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 89px;
  margin-left: 32px;
  padding-top: 6px;
`;

export const Info = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 109px;
  width: 100%;
  text-align: center;
  padding-top: 6px;
  align-items: center;
`;

export const Img = styled.img`
  width: 66px;
  height: 66px;
  border-radius: 4px;
  border: 1px solid #e4e4e4;
`;

export const Date = styled.span`
  color: #000;
  line-height: 1.32;
  font-weight: ${({status}) => (status === 'Valid Until' ? '400' : '500')};
`;

export const Title = styled.p`
  margin-bottom: 0;
  color: #464646;
  line-height: 1.4;
  max-height: 32px;
  overflow: hidden;
  text-overflow: ellipsis;
  -webkit-line-clamp: 2;
  white-space: normal;
  -webkit-box-orient: vertical;
  display: -webkit-box;
`;

export const Type = styled.p`
  margin-bottom: 5px;
  color: #545454;

  span {
    font-weight: 700;
    color: ${({status}) => (status === 'Expiring soon' ? midCoinsColor : '#26A95E')};
  }
`;

export const Status = styled.span`
  color: #000
    ${({status}) =>
      status === 'Available' &&
      `
      font-weight: 700;
      `};
`;

export const Unlock = styled(Button)`
  &&& {
    padding-right: 16px;
    padding-left: 19px;
    background-color: #666666;
    border-color: #666666;
    color: #fff;
    border-radius: 100px;
    line-height: 1;
    height: 24px;
    display: flex;
    align-items: center;
    font-size: inherit;
    width: 92px;

    svg {
      margin-left: 7px;
    }
  }
`;
