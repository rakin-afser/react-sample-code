import React from 'react';
import {Drawer} from 'antd';
import {Done, Title, TitleContainer, CheckboxGroup} from './styled';
import Checkbox from 'components/Checkbox';

const SideDrawer = (props) => {
  const types = ['Review', 'List', 'Referral', 'Shop Coins'];
  const statuses = ['Available', 'Unlock', 'Referal'];

  const {visible, onClose} = props;
  function setTitle() {
    return (
      <TitleContainer>
        <Title center>Sort &amp; Filter</Title>
        <Done onClick={() => onClose(false)}>Done</Done>
      </TitleContainer>
    );
  }

  return (
    <Drawer
      destroyOnClose
      width={335}
      headerStyle={{padding: '13px 11px 16px'}}
      bodyStyle={{padding: '0 0 16px'}}
      title={setTitle()}
      visible={visible}
      onClose={onClose}
      closable={false}
    >
      <Title px={16} my={16}>
        Type:
      </Title>
      <CheckboxGroup>
        {types.map((type) => (
          <Checkbox key={type}>{type}</Checkbox>
        ))}
      </CheckboxGroup>
      <Title px={16} my={16}>
        Status:
      </Title>
      <CheckboxGroup>
        {statuses.map((status) => (
          <Checkbox key={status}>{status}</Checkbox>
        ))}
      </CheckboxGroup>
    </Drawer>
  );
};

export default SideDrawer;
