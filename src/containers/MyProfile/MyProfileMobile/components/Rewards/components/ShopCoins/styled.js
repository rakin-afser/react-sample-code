import {Button} from 'antd';
import styled from 'styled-components';

export const AfterTitle = styled.div`
  color: #398287;
  font-size: 16px;
  font-weight: 500;
  display: inline-flex;
  align-items: center;
  margin-left: 12px;

  i {
    margin-right: 5px;
  }
`;

export const SliderContainer = styled.div`
  padding: 16px 0;
`;

export const View = styled.div`
  padding: 16px 11px 16px 16px;
  color: #000;
  position: relative;
  font-weight: 500;
  margin-bottom: 0;
`;

export const ViewModes = styled.div`
  display: flex;
  position: absolute;
  top: 50%;
  right: 11px;
  transform: translateY(-50%);
`;

export const ViewBtn = styled(Button)`
  &&& {
    color: ${({active}) => (active ? '#000' : '#ccc')};
    padding: 5px;
    border: none;
    height: auto;
    line-height: 1;

    > i {
      display: inline-flex;
    }
  }

  & + & {
    margin-left: 19px;
  }
`;
