import WithScroll from 'components/WithScroll';
import Icon from 'components/Icon';
import CardNewArrival from 'components/CardNewArrival';
import React, {useEffect, useState} from 'react';
import {deals, featuredProducts, newArrivals} from 'constants/staticData';
import {SliderContainer, AfterTitle, View, ViewModes, ViewBtn} from './styled';
import useGlobal from 'store';

const ShopCoins = () => {
  const [active, setActive] = useState(0);

  const [, globalActions] = useGlobal();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/profile/rewards');
  }, []);

  const views = ['tile', 'list'];

  function renderAfterTitle() {
    return (
      <AfterTitle>
        <Icon width={21} height={21} type="coins" />
        298
      </AfterTitle>
    );
  }

  return (
    <>
      <View>
        Show as:
        <ViewModes>
          {views.map((view, i) => (
            <ViewBtn key={view} active={active === i ? 1 : 0} onClick={() => setActive(i)}>
              <Icon fill="currentColor" type={view} />
            </ViewBtn>
          ))}
        </ViewModes>
      </View>
      <SliderContainer>
        <WithScroll
          marginTop={0}
          title="Recently Viewed"
          withSeeMore
          height={255}
          seeMoreText=""
          seeMoreCounter=""
          showSeeMoreArrow={false}
        >
          {featuredProducts.map((product, index) => (
            <CardNewArrival key={index} {...product} />
          ))}
        </WithScroll>
      </SliderContainer>

      <SliderContainer>
        <WithScroll
          afterTitle={renderAfterTitle()}
          marginTop={0}
          title="Chanel Beauty"
          withSeeMore
          height={255}
          seeMoreText="Visit Store"
          seeMoreCounter=""
        >
          {deals.map((product, index) => (
            <CardNewArrival key={index} {...product} />
          ))}
        </WithScroll>
      </SliderContainer>

      <SliderContainer>
        <WithScroll
          afterTitle={renderAfterTitle()}
          marginTop={0}
          title="Kylie Skin"
          withSeeMore
          height={255}
          seeMoreText="Visit Store"
          seeMoreCounter=""
        >
          {newArrivals.map((product, index) => (
            <CardNewArrival key={index} {...product} />
          ))}
        </WithScroll>
      </SliderContainer>
    </>
  );
};

export default ShopCoins;
