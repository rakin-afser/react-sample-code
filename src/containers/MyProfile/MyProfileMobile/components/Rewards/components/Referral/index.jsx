import Loader from 'components/Loader';
import {useAffiliateLinkQuery} from 'hooks/queries';
import {useUser} from 'hooks/reactiveVars';
import React, {useEffect, useState} from 'react';
import {SwitchTransition, CSSTransition} from 'react-transition-group';
import useGlobal from 'store';
import {Container, Label, ReferralLink, Row, StyledButton, Text} from './styled';

const Referral = () => {
  const [, globalActions] = useGlobal();

  const [user] = useUser();
  const {data, loading} = useAffiliateLinkQuery({variables: {id: user?.databaseId}});
  const {affiliateLink} = data?.user || {};

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/profile/rewards');
  }, []);

  const [copy, setCopy] = useState(false);

  function onCopy() {
    navigator.clipboard.writeText(`${window.location.origin}/ref/${affiliateLink}`);
    setCopy(true);
    setTimeout(() => {
      setCopy(false);
    }, 5000);
  }

  async function onShare() {
    if (navigator.share) {
      await navigator.share(`${window.location.origin}/ref/${affiliateLink}`);
    }
  }

  function render() {
    if (loading) return <Loader wrapperWidth="auto" wrapperHeight="auto" />;

    return (
      <>
        <Text>Share this link to your friend, earn coins for both!</Text>
        <Label>Your Referral Link</Label>
        <ReferralLink>{affiliateLink}</ReferralLink>
        <Row>
          <StyledButton onClick={onCopy}>
            <SwitchTransition mode="out-in">
              <CSSTransition
                key={copy}
                addEndListener={(node, done) => {
                  node.addEventListener('transitionend', done, false);
                }}
                classNames="fade"
              >
                <span>{copy ? 'Copied!' : 'Copy'}</span>
              </CSSTransition>
            </SwitchTransition>
          </StyledButton>
          <StyledButton onShare={onShare}>Share</StyledButton>
        </Row>
      </>
    );
  }

  return <Container>{render()}</Container>;
};

export default Referral;
