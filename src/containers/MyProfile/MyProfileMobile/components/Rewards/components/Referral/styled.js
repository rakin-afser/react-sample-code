import {Button} from 'antd';
import styled from 'styled-components';

export const Container = styled.div`
  padding: 24px 16px;
  color: #666;
`;

export const Text = styled.p`
  font-size: 16px;
  margin-bottom: 36px;
`;

export const Label = styled.p`
  color: #000;
  margin-bottom: 9px;
`;

export const ReferralLink = styled.p`
  color: #000;
  font-size: 12px;
  padding: 12px 16px;
  border: 1px solid #cccccc;
  line-height: 1;
  text-overflow: ellipsis;
  overflow: hidden;
  padding-right: 82px;
  margin-bottom: 24px;
  border-radius: 2px;
`;

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const StyledButton = styled(Button)`
  &&& {
    font-size: 12px;
    height: 28px;
    width: 100%;
    max-width: 144px;
    border-radius: 100px;
    color: #666;
    border-color: #666;

    .fade-enter {
      opacity: 0;
    }
    .fade-enter-active {
      opacity: 1;
    }
    .fade-exit {
      opacity: 1;
    }
    .fade-exit-active {
      opacity: 0;
    }
    .fade-enter-active,
    .fade-exit-active {
      transition: opacity 200ms;
    }
  }
`;
