import {Button} from 'antd';
import styled from 'styled-components';

export const Wrap = styled.div`
  padding: ${({py}) => (py ? py : 24)}px 16px;
`;

export const Row = styled.div`
  display: flex;
  flex-wrap: wrap;
  border-top: 1px solid #efefef;
  color: #464646;
  line-height: 1;

  &:last-of-type {
    border-bottom: 1px solid #efefef;
    color: #000;
  }
`;

export const Col = styled.div`
  flex: ${({flex}) => (flex ? `0 0 ${flex}px` : '1 1 auto')};
  ${({right}) =>
    right &&
    `
    text-align: right;
    font-weight: 500;
    color: #000;
  `}
  padding: 16px 12px;
`;

export const Text = styled.p`
  color: #666;

  span {
    font-weight: 500;
    color: #398287;
  }
`;

export const Title = styled.h4`
  font-size: 18px;
  line-height: 1.33;
  letter-spacing: -2.4%;
  color: #398287;
  position: relative;
  margin-bottom: 0;

  i {
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
  }
`;

export const Hr = styled.hr`
  margin: 0;
  border: 4px solid #fafafa;
`;

export const Start = styled(Button)`
  &&& {
    height: 42px;
    background-color: transparent;
    color: #398287;
    border-color: currentColor;
    width: 100%;
    border-radius: 100px;
    font-weight: 500;
  }
`;
