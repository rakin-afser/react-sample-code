import React, {useEffect} from 'react';
import {Wrap, Row, Col, Text, Hr, Title, Start} from './styled';
import Icon from 'components/Icon';
import useGlobal from 'store';

const WhatIs = () => {
  const [, globalActions] = useGlobal();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/profile/rewards');
  }, []);

  return (
    <>
      <Wrap>
        <Text>
          <span>Midcoins</span> - is an international not-for-profit organisation that started in the 1980s as an effort
          to "unify" the "codes" for textual characters used in the computing industry. By "code", I just mean a number.
          Computers only understand numbers, visualise them on which character.
        </Text>
      </Wrap>
      <Hr />
      <Wrap>
        <Title>
          How to Use Midcoins <Icon type="chevronRight" fill="#000" />
        </Title>
      </Wrap>
      <div>
        <Row>
          <Col flex={100}>+1</Col>
          <Col right>For each spent £10.00</Col>
        </Row>
        <Row>
          <Col flex={100}>+10</Col>
          <Col right>Purchase through your list</Col>
        </Row>
        <Row>
          <Col flex={100}>+5</Col>
          <Col right>Join through your refferal</Col>
        </Row>
        <Row>
          <Col flex={155}>10 midcoins = £1.00</Col>
          <Col right>(Redeem)</Col>
        </Row>
      </div>
      <Wrap>
        <Start>Start MidCoins Now</Start>
      </Wrap>
    </>
  );
};

export default WhatIs;
