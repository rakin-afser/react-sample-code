import styled from 'styled-components/macro';
import {mainBlackColor, primaryColor} from 'constants/colors';
import Icon from 'components/Icon';

export const SwitcherWrapper = styled.div`
  background-color: #fff;
  border-radius: 12px;
  position: relative;
`;

export const Profile = styled.div`
  padding: 16px 24px;
  display: flex;
  align-items: center;
  transition: opacity 0.4s;
  cursor: pointer;

  &:first-child {
    border-bottom: 1px solid #efefef;
  }

  & svg {
    width: 25px;
    height: auto;
    transition: opacity 0.4s;
    opacity: ${({active}) => (active ? 1 : 0)};

    path {
      fill: ${primaryColor};
    }
  }
`;

export const Avatar = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-shrink: 0;
  width: 56px;
  height: 56px;
  margin-right: 24px;
  border-radius: 50%;
  overflow: hidden;
  background: #efefef;
  position: relative;

  & img {
    border-radius: 50%;
    width: 52px;
    height: 52px;
    position: relative;
    z-index: 1;
  }

  &::before {
    content: '';
    z-index: 0;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    opacity: ${({active}) => (active ? 1 : 0)};
    transition: opacity 0.3s ease-in;
    background: linear-gradient(152.45deg, rgba(244, 77, 84, 0.75) 24.39%, rgba(52, 149, 155, 0.75) 75.89%);
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-right: 0;
    margin-left: 24px;
  `}
`;

export const Name = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  transition: color 0.3s ease-in;
  color: ${({active}) => (active ? '#000' : '#666')};
  flex-grow: 1;
  transition: color 0.3s ease-in;

  & span {
    display: block;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    color: #999;
    margin-top: 2px;
  }
`;

export const Title = styled.h4`
  font-size: 18px;
  color: ${mainBlackColor};
  text-align: center;
  padding-top: 13px;
  margin-bottom: 2px;
`;

export const Close = styled(Icon)`
  position: absolute;
  top: 17px;
  left: 17px;
  z-index: 1;
`;
