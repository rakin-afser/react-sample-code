import React from 'react';
import {useTranslation} from 'react-i18next';

import Checkmark from 'assets/Checkmark';
import {SwitcherWrapper, Profile, Avatar, Name, Title, Close} from './styled';

const ProfileSwitcher = ({user, onSetActiveProfile, activeProfile, setActive}) => {
  const {t} = useTranslation();

  return (
    <SwitcherWrapper>
      <Close onClick={setActive} type="cross" fill="#000" width={14} height={14} />
      <Title>Change Profile</Title>
      <Profile active={activeProfile === 'user'} onClick={() => onSetActiveProfile('user')}>
        <Avatar active={activeProfile === 'user'}>
          <img src={user?.avatar?.url} />
        </Avatar>
        <Name active={activeProfile === 'user'}>
          {user?.firstName} {user?.lastName}
          <span>{t('myProfilePage.userProfile')}</span>
        </Name>
        <Checkmark />
      </Profile>
      {user?.seller ? (
        <Profile active={activeProfile === 'shop'} onClick={() => onSetActiveProfile('shop')}>
          <Avatar active={activeProfile === 'shop'}>
            <img src={user?.avatar?.url} />
          </Avatar>
          <Name active={activeProfile === 'shop'}>
            {user?.shopName}
            <span>{t('myProfilePage.shopProfile')}</span>
          </Name>
          <Checkmark />
        </Profile>
      ) : null}
    </SwitcherWrapper>
  );
};

export default ProfileSwitcher;
