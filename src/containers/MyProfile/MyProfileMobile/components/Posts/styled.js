import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';

import {primaryColor} from 'constants/colors';
import {secondaryFont} from 'constants/fonts';
import {Button} from 'antd';
import {mobileHeaderHeight} from 'components/Header/constants';

export const Wrapper = styled.div`
  width: 100%;
  overflow: hidden;
  display: flex;
  align-items: stretch;
  justify-content: flex-start;
  flex-direction: column;
  position: relative;
  height: 100%;

  ${({popupOpened}) =>
    popupOpened &&
    `
    max-height: 100vh;
  `}

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const FlexBlock = styled.div`
  padding: 0 16px;
  display: flex;
`;

export const BackTo = styled.div`
  height: ${mobileHeaderHeight};
  line-height: ${mobileHeaderHeight};
  border-bottom: 1px solid #efefef;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #000;
  position: relative;
  text-align: center;

  a {
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    line-height: 1;
    font-size: 0;
  }

  .settings {
    color: #999;
    right: 18px;
  }

  .back {
    left: 10px;
  }
`;

export const Header = styled(FlexBlock)`
  padding-top: 14px;
  display: flex;
`;

export const UserInfo = styled.div`
  display: flex;
  flex-grow: 1;
  align-items: flex-start;
  flex-direction: column;
`;

export const Name = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  color: #000;
  line-height: normal;
  display: flex;
  align-items: center;

  & span {
    margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 10px' : '0 10px 0 0')};
  }
`;

export const FollowText = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #666666;
  margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 18px' : '0 18px 0 0')};
`;

export const FollowBlock = styled.div`
  display: flex;
  margin: ${({theme: {isArabic}}) => (isArabic ? '14px 0 8px' : '11px 0 14px')};
`;

export const FollowNumber = styled(FollowText)`
  color: #000000;
  font-weight: 500;
  margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 8px' : '0 8px 0 0')};
`;

export const EditButton = styled(Link)`
  color: #666;
  font-size: 12px;
  border: 1px solid #666666;
  border-radius: 24px;
  padding: 5px 15px;
  line-height: 1;
`;

export const Description = styled.span`
  flex-shrink: 0;
  margin: 3px 0 16px;
  padding: 0 16px;
  font-family: Helvetica Neue;
  font-size: 14px;
  color: #000;
  display: block;
  overflow: hidden;
  line-height: 1.2;

  &::after {
    content: 'see more';
    color: ${primaryColor};
    margin-left: 4px;
  }

  ${({expanded}) =>
    expanded &&
    `
      &::after {
        content: 'less';
      }
    `}
`;

export const Profession = styled(FlexBlock)`
  margin-top: 11px;
  font-family: ${secondaryFont};
  letter-spacing: 3px;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #000;
  text-transform: uppercase;
`;

export const TabsWrapper = styled.div`
  position: relative;
  display: flex;
  border-top: 8px solid #fafafa;
  border-bottom: 2px solid #efefef;

  &::before {
    content: '';
    position: absolute;
    width: 50%;
    height: 2px;
    background-color: ${primaryColor};
    left: ${({theme: {isArabic}}) => (isArabic ? '50%' : '0')};
    bottom: -2px;
    transition: left 0.4s;
  }

  ${({activeTab, theme: {isArabic}}) =>
    activeTab === 2 &&
    `
    &::before {
      left: ${isArabic ? '0' : '50%'};
    }
  `}
`;

export const Tab = styled(Link)`
  padding: 9.5px 0 7.5px 0;
  font-weight: 400;
  font-size: 14px;
  color: #999;
  width: 50%;
  display: flex;
  justify-content: center;
  cursor: pointer;
  transition: color 0.4s;

  &:hover {
    color: #000;
  }

  ${({isActive}) =>
    isActive &&
    `
    font-weight: 700;
    color: #000;
  `}
`;

export const PostWrapper = styled.div`
  border: 1px solid #ececec;
  margin: 16px 0;
  ${(p) =>
    p.first &&
    `
    margin-top: 0;
    border-top: none;
  `}
`;

export const PostHeader = styled.div`
  padding: 17px 17px 13px 17px;
  display: flex;
  align-items: center;
`;

export const Avatar = styled.img`
  flex-shrink: 0;
  margin: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 8px' : '0 8px 0 0')};
  border-radius: 50%;
  width: 50px;
  height: 50px;
`;

export const Author = styled.div`
  font-family: SF Pro Display;
  font-weight: bold;
  font-size: 16px;
  color: #000;
  flex-grow: 1;
  padding: ${({theme: {isArabic}}) => (isArabic ? '0 0 0 20px' : '0 20px 0 0')};

  & span {
    display: block;
    font-weight: normal;
    font-size: 12px;
  }
`;

export const PostImage = styled.img`
  width: 100%;
  height: auto;
`;

export const Dots = styled.div`
  margin: ${({theme: {isArabic}}) => (isArabic ? '4px 0 0 4px' : '4px 4px 0 0')};
  align-self: self-start;
  direction: ltr;

  & span,
  &::before,
  &::after {
    display: inline-block;
    content: '';
    border-radius: 50%;
    background-color: #7c7e82;
    width: 4px;
    height: 4px;
  }

  &::after {
    margin-left: 3.8px;
  }

  &::before {
    margin-right: 3.8px;
  }
`;

export const AddPostButton = styled.button`
  position: fixed;
  width: 50px;
  height: 50px;
  right: 20px;
  bottom: 74px;
  border-radius: 50%;
  background: ${primaryColor};
  z-index: 999;
  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.15);
  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    right: 0;
    left: 20px;
  `}
`;

export const PopupWrapper = styled.div`
  position: absolute;
  width: 0;
  height: 100vh;
  background: rgba(0, 0, 0, 0.6);
  overflow: hidden;
  display: flex;
  justify-content: flex-end;
  flex-direction: column;
  z-index: 1000;
  opacity: 0;
  transition: all 0.6s;

  ${(p) =>
    p.isOpen &&
    `
    opacity: 1;
    width: 100%;
  `}
`;

export const PopupContent = styled.div`
  margin: 0 10px 64px 10px;
  background: #fff;
  border-radius: 12px;
  overflow: hidden;
  min-width: calc(100vw - 20px);
`;

export const PopUpHeader = styled.div`
  padding: 13px 17px 8px 17px;
  font-weight: 500;
  font-size: 18px;
  color: #000;
  position: relative;
  text-align: center;

  & svg {
    position: absolute;
    left: 10px;
  }
  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    & svg {
      left: unset;
      right: 10px;
    }
  `}
`;

export const PostImageWrapper = styled.div`
  position: relative;
`;

export const PostLabel = styled.div`
  position: absolute;
  top: 16px;
  right: 16px;
  padding: 4px 10px;
  background-color: #ffffff;
  opacity: 0.9;
  box-shadow: 1.42857px 1.42857px 2.85714px rgba(0, 0, 0, 0.16);
  border-radius: 13px;
  font-size: 12px;
  letter-spacing: -0.016em;
  color: #7a7a7a;
  display: flex;
  line-height: 120%;
  align-items: flex-end;

  & svg {
    width: 20px;
    height: 17px;
    margin-right: 8px;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    left: 16px;
    right: unset;

    & svg {
      margin-left: 8px;
      margin-right: 0;
    }
  `}
`;
