import video1 from 'videos/videoplayback.mp4';
import video2 from 'videos/videoplayback_2.mp4';
import userAvatar from 'images/followers-avatar/avatar8.png';
import post1 from 'images/posts/post1.jpg';
import post2 from 'images/posts/post2.jpg';
import post3 from 'images/posts/post3.jpg';
import post4 from 'images/posts/post4.jpg';
import product from 'images/products/product24.jpg';
import {v1} from 'uuid';

export default [
  {
    type: 'post',
    id: v1(),
    store: {
      name: 'arianagrande',
      avatar: userAvatar
    },
    date: '10m ago',
    posters: [post1, post1, post1],
    productsCount: 1,
    products: [
      {
        name: 'Chanel Lipstick',
        image: product,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 252,
    comments: 65
  },
  {
    type: 'post',
    id: v1(),
    store: {
      name: 'arianagrande',
      avatar: userAvatar
    },
    description: {
      title: 'New colors - new life!',
      text:
        'Cream canvas Big Sad Wolf Tote Bag. Feature large placement print to one side and a small slogan print to the other. '
    },
    date: '5m ago',
    tags: ['#Cream', '#canvas', '#Big', '#tags'],
    posters: [post2],
    likes: 635,
    comments: 324
  },
  {
    id: v1(),
    store: {
      name: 'gigihadid',
      avatar: userAvatar
    },
    stars: 5,
    date: '15w ago',
    description: {
      title: 'I just love it <3 <3',
      text:
        'New collection is coming in spring 2020 and we created so many things which are really awesome and new, i love it so much and i recommend it!'
    },
    tags: ['#topproducts', '#electronics', '#headphones'],
    posters: [post2, post2, post2],
    productsCount: 1,
    products: [
      {
        name: 'Beats Solo Pro',
        image: product,
        price: 490,
        delivery: 'Free Shipping',
        sale: null
      }
    ],
    likes: 653,
    comments: 546
  }
];
