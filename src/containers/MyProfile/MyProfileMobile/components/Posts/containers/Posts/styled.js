import {bookmarkFillColor} from 'constants/colors';
import {Link} from 'react-router-dom';
import styled from 'styled-components/macro';

export const Title = styled.h4`
  color: ${bookmarkFillColor};
  margin-bottom: 0;
`;

export const CreateButton = styled.div`
  border-radius: 50%;
  border-color: #ed484f;
  background-color: #ed484f;
  height: 50px;
  width: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  right: 16px;
  bottom: 74px;
  z-index: 10;
  box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.15);

  & > input {
    position: absolute;
    z-index: 11;
    width: 100%;
    height: 100%;
    opacity: 0;
  }
`;

export const CardsList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-auto-flow: row;
  grid-gap: 2px;
  padding: 9px;
  background-color: rgba(196, 196, 196, 0.13);
  border-radius: 8px;
  margin: 0 9px;
`;
