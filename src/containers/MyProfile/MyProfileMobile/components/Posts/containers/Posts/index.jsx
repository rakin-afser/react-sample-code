import React, {useEffect, useState} from 'react';
import {useParams, useHistory} from 'react-router-dom';

import {useUser} from 'hooks/reactiveVars';
import Icon from 'components/Icon';
import {CreateButton, Title, CardsList} from './styled';
import Post from 'components/Post';
import Grid from 'components/Grid';
import FadeSwitch from 'containers/Animations/FadeSwitch';
import Loader from 'components/Loader';
import PostCardView from 'components/Cards/Post/mobile/CardView';
import {v1} from 'uuid';
import {useQuery} from '@apollo/client';
import {FEED_POSTS} from 'queries';
import {useOnScreen} from 'hooks';

const Posts = () => {
  const [feedView, setFeedView] = useState(false);
  const {userParam} = useParams();
  const {push} = useHistory();
  const [user] = useUser();
  const [clickedPost, setClickedPost] = useState(null);

  const [visible, setVisible] = useState([]);
  const active = visible?.length === 0 ? null : Math.min(...visible);

  const {data, loading, fetchMore} = useQuery(FEED_POSTS, {
    variables: {
      first: 9,
      where: {
        author: +userParam || user?.databaseId
      }
    },
    notifyOnNetworkStatusChange: true
  });
  const {posts} = data || {};
  const {endCursor, hasNextPage} = data?.posts?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.posts?.nodes || [];
          const newNodes = fetchMoreResult?.posts?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            posts: {
              ...fetchMoreResult?.posts,
              nodes
            }
          };
        },
        variables: {
          first: 9,
          where: {
            author: +userParam || user?.databaseId
          },
          after: endCursor
        }
      });
    }
  });

  const postCardClick = (databaseId) => {
    setClickedPost(databaseId);
    setFeedView(true);
  };

  useEffect(() => {
    document.querySelector('html').classList.add('scroll-behavior-auto');
    return () => {
      document.querySelector('html').classList.remove('scroll-behavior-auto');
    };
  });

  async function onChange(e) {
    if (e.target.files) {
      const files = Array.from(e.target.files);
      try {
        const medias = await Promise.all(
          files.map((file) => {
            return new Promise((resolve, reject) => {
              const reader = new FileReader();
              reader.addEventListener('load', (ev) => {
                resolve({
                  id: v1(),
                  file,
                  result: ev.target.result
                });
              });
              reader.addEventListener('error', reject);
              reader.readAsDataURL(file);
            });
          })
        );
        push('/post/new', {medias});
      } catch (error) {
        console.log(`error`, error);
      }
    }
  }

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  if (loading && !posts?.nodes?.length) return <Loader wrapperHeight="200px" wrapperWidth="100%" />;

  return (
    <div style={{paddingBottom: '60px'}}>
      <Grid sb padding="20px 28px 8px 18px">
        <Title>Recent</Title>
        <FadeSwitch state={feedView}>
          <Icon type={feedView ? 'feed' : 'cards'} onClick={() => setFeedView(!feedView)} />
        </FadeSwitch>
      </Grid>
      <CreateButton>
        <input onChange={onChange} type="file" multiple accept="image/*, video/*" />
        <Icon width={14} height={14} type="plus" color="#fff" />
      </CreateButton>
      <FadeSwitch state={feedView}>
        {feedView ? (
          posts?.nodes?.map((item, i) => (
            <Post
              scrollToThisPost={item?.databaseId === clickedPost}
              active={i === active}
              visible={visible}
              setVisible={setVisible}
              order={i}
              key={item.id}
              data={item}
              isUserLogeIn
            />
          ))
        ) : (
          <CardsList style={{margin: '20px 9px'}}>
            {posts?.nodes?.map((item) => (
              <PostCardView data={item} onClick={() => postCardClick(item?.databaseId)} />
            ))}
          </CardsList>
        )}
      </FadeSwitch>

      {loading && posts?.nodes?.length ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
      {endRef}
    </div>
  );
};

export default Posts;
