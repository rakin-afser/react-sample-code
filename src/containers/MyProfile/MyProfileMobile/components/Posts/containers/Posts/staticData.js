import prod1 from 'images/products/product23.png';
import prod2 from 'images/products/product22.jpg';
import prod3 from 'images/products/product21.jpg';
import prod4 from 'images/products/product20.jpg';
import prod5 from 'images/products/product19.jpg';
import prod6 from 'images/products/product18.jpg';
import {v1} from 'uuid';

export const cards = [
  {id: v1(), type: 'post', img: prod1},
  {id: v1(), type: 'post', img: prod2},
  {id: v1(), type: 'post', img: prod3},
  {id: v1(), type: 'post', img: prod4},
  {id: v1(), type: 'post', img: prod5, price: 292},
  {id: v1(), type: 'post', img: prod6},
  {id: v1(), type: 'post', img: prod1},
  {id: v1(), type: 'post', img: prod2},
  {id: v1(), type: 'post', img: prod3}
];
