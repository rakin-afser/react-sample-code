import styled from 'styled-components';
import {mobileHeaderHeight} from 'constants/mobileSizes';
import {Progress} from 'antd';
import Icon from 'components/Icon';

export const Back = styled(Icon)`
  position: absolute;
  left: 10px;
  top: 50%;
  transform: translateY(-50%);
`;

export const Container = styled.div`
  position: relative;
  padding-bottom: ${({pb}) => (pb ? pb : 0)}px;
  padding-top: 47px;
`;

export const Header = styled.div`
  background-color: #fff;
  height: ${mobileHeaderHeight};
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  z-index: 1001;
  border-bottom: 1px solid #efefef;
`;

export const PageTitle = styled.h3`
  font-size: 18px;
  line-height: 1.22;
  margin-bottom: 0;
`;

export const StyledProgress = styled(Progress)`
  &&& {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    font-size: 0;

    .ant-progress-inner {
      border-radius: 0;
    }

    .ant-progress-bg {
      border-radius: 0;
    }
  }
`;

StyledProgress.defaultProps = {
  strokeColor: '#ED484F',
  trailColor: '#EFEFEF',
  strokeWidth: 3,
  showInfo: false,
  strokeLinecap: 'square'
};
