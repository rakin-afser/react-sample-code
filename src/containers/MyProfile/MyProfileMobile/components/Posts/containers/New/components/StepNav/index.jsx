import React from 'react';
import {Wrap, Text, Next, Cancel, SimpleButton} from './styled';

const StepNav = ({text = '', next = 'Next', onNext = () => {}, onCancel, simple, disable}) => {
  return simple ? (
    <SimpleButton disable={disable} onClick={onNext}>
      Next
    </SimpleButton>
  ) : (
    <Wrap>
      {text ? <Text>{text}</Text> : null}
      {onCancel ? <Cancel onClick={onCancel}>Cancel</Cancel> : null}
      <Next onClick={onNext}>{next}</Next>
    </Wrap>
  );
};

export default StepNav;
