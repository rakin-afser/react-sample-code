import styled, {css} from 'styled-components';
import {Button} from 'antd';

export const Wrap = styled.div`
  position: fixed;
  left: 16px;
  right: 16px;
  bottom: 20px;
  padding: 16px 26px 16px 20px;
  border-radius: 12px;
  background-color: #fff;
  box-shadow: 0px 4px 7px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  z-index: 10;
  border: 1px solid #efefef;
`;

export const Next = styled(Button)`
  &&& {
    height: 30px;
    border-radius: 30px;
    border-color: #666666;
    color: #000;
    margin-left: auto;
  }
`;

export const Text = styled.p`
  max-width: 170px;
  margin-bottom: 0;
  color: #a7a7a7;
  line-height: 1.2;
`;

export const Cancel = styled(Button)`
  &&& {
    font-size: 14px;
    font-weight: 400;
    color: #000;
    height: 30px;
    border-radius: 30px;
    border: none;
    padding: 5px;
  }
`;

export const SimpleButton = styled.div`
  position: fixed;
  left: 16px;
  right: 16px;
  bottom: 20px;
  padding: 10px 24px;
  width: 100%;
  max-width: 246px;
  border: 1px solid #666666;
  box-sizing: border-box;
  border-radius: 24px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  color: #000000;
  line-height: 17px;
  z-index: 5;
  background: #fff;
  display: flex;
  margin: auto;
  justify-content: center;

  ${({disable}) =>
    disable &&
    css`
      background: #f4f4f4;
      color: #999;
      border: 1px solid #999;
    `}
`;
