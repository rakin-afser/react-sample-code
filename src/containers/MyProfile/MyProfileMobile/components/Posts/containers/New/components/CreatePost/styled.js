import styled, {css} from 'styled-components/macro';
import Slider from 'react-slick';
import {Button} from 'antd';
import Icon from 'components/Icon';

export const Wrap = styled.div`
  padding: ${({py}) => py || 0}px 16px;
`;

export const IframeWrap = styled.div`
  position: relative;
  padding-bottom: calc(56.25% + 32px);
  height: 0;
  overflow: hidden;
  margin: 0 -16px 50px;

  .react-player {
    position: absolute;
    top: 0;
    left: 0;
  }
`;

export const StyledSlider = styled(Slider)`
  .slick-slide > div {
    width: 100%;
    font-size: 0;
  }

  .slick-dots {
    bottom: 32px !important;
    font-size: 0;
    left: 0;
    right: 0;
    max-width: 250px;
    margin: 0 auto;

    li {
      width: auto;
      height: auto;
      margin: 0;

      &.slick-active {
        button {
          &::before {
            background-color: #ea2c34;
          }
        }
      }

      button {
        padding: 6px;
        display: flex;
        justify-content: center;
        align-items: center;

        &::before {
          width: 8px;
          height: 8px;
          content: '';
          background-color: #e4e4e4;
          border-radius: 50%;
          margin: auto;
          right: 0;
          bottom: 0;
          opacity: 1;
        }
      }
    }
  }
`;

export const Slide = styled.div`
  width: 100%;
  background-color: #fafafa;
  position: relative;
`;

export const Image = styled.div`
  background-size: cover;
  background-position: center center;
  background-repeat: no-repeat;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
`;

export const SlideVideo = styled.video`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const EmbedWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;

  &:before {
    content: '';
    padding-top: 56.25%;
    display: block;
  }
`;

export const SlidePlaceholder = styled.div`
  padding-bottom: ${({ratio}) => (ratio ? ratio * 100 : 100)}%;
  transition: padding-bottom 0.3s ease;
`;

export const DeleteBtn = styled(Button)`
  &&& {
    position: absolute;
    right: 16px;
    bottom: 13px;
    background-color: rgba(255, 255, 255, 0.3);
    border: none;
    color: #fff;
    width: 44px;
    height: 44px;
    z-index: 1;
  }
`;

DeleteBtn.defaultProps = {
  shape: 'circle',
  icon: 'delete'
};

export const SliderNavWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -3px;
`;

export const SliderNavImage = styled.img`
  border-radius: 4px;
  object-fit: cover;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  width: 100%;
  height: 100%;
`;

export const SliderNavItem = styled.div`
  margin: 2.5px 3px;
  position: relative;
  flex: 0 0 auto;
  width: calc(20% - 6px);
  padding-bottom: calc(20% - 6px);
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);

  &:before {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    background-color: #fff;
    opacity: ${({active}) => (active ? 0.54 : 0)};
    transition: opacity 0.3s ease-in;
    z-index: 1;
    border: 1px solid rgba(237, 72, 79, 0.8);
    border-radius: 4px;
  }
`;

export const SliderNewItem = styled.div`
  margin: 2.5px 3px;
  position: relative;
  flex: 0 0 auto;
  width: calc(20% - 6px);
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  padding-bottom: calc(20% - 6px);

  input {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    opacity: 0;
    width: 100%;
    height: 100%;
  }

  i {
    margin-bottom: 5px;
  }
`;

export const SliderNewItemWrap = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  font-size: 12px;
  color: #000;
`;

export const Play = styled.div`
  padding-left: 7px;
  width: 50px;
  height: 50px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.3);
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 10;
`;

export const IconPlay = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 1;
  width: ${({circleSize}) => circleSize || 68}px;
  height: ${({circleSize}) => circleSize || 68}px;
  border-radius: 50%;
  background-color: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.3);

  ${({previewVideoStart}) =>
    previewVideoStart &&
    `
    opacity: 0;
  `};

  svg {
    margin-left: 3px;
  }
`;

export const RatioList = styled.ul`
  display: flex;
  justify-content: space-around;
  padding-top: 8px;
  padding-bottom: 8px;
`;

export const RatioItem = styled.li`
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  font-weight: 500;
  color: ${({active}) => (active ? '#ed484f' : '#c3c3c3')};
  transition: color 0.3s ease;
`;

export const RatioSymbol = styled.span`
  display: block;
  width: ${({width, height}) => (width / height) * 20}px;
  height: ${({width, height}) => (height / width) * 20}px;
  border: 2px solid currentColor;
`;

export const PostTypeSwitcher = styled.div`
  background: #fff;
  border-radius: 24px;
  border: 1px solid #ccc;
  position: fixed;
  bottom: 68px;
  display: flex;
  left: 50%;
  transform: translateX(-50%);
  height: 34px;
  width: 127px;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 5;

  & button {
    position: relative;
    color: #8f8f8f;
    padding: 0;
    &:first-child {
      margin: 0 15px 0;
    }
    &:last-child {
      margin: 0 11px 0;
    }
  }

  &::before {
    content: '';
    position: absolute;
    left: 2px;
    top: 2px;
    background: #dcebfb;
    border-radius: 24px;
    width: 65px;
    height: 28px;
    transition: all 0.4s;
  }

  ${({type}) =>
    type === 'flash'
      ? css`
          &::before {
            left: 58px;
          }
          & button:last-child {
            color: #464646;
          }
        `
      : css`
          & button:first-child {
            color: #464646;
          }
        `}
`;
