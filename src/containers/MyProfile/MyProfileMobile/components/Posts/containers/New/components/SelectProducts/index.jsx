import React, {useState, useEffect} from 'react';
import {
  Container,
  Header,
  Title,
  Selected,
  Search,
  SearchWrapper,
  SearchIcon,
  Cancel,
  List,
  Prod,
  ProdWrap,
  ProdImg,
  ProdTitle,
  Price,
  Currency,
  Shipping,
  Info,
  PlusIcon,
  CheckWrap,
  Check
} from './styled';
import {useQuery} from '@apollo/client';
import {GET_ORDERS} from 'queries';
import {SELLER_PRODUCTS} from 'components/CreatePost/api/queries';
import Loader from 'components/Loader';
import {useUser} from 'hooks/reactiveVars';

const SelectProducts = ({setStep, selectedProducts, setSelectedProducts}) => {
  const [user] = useUser();
  const [search, setSearch] = useState('');
  const query = user?.isSeller ? SELLER_PRODUCTS : GET_ORDERS;
  const variables = user?.isSeller ? {vendorId: user?.databaseId, first: 12, search} : {customerId: user?.databaseId};
  const {data, loading} = useQuery(query, {
    variables,
    onCompleted(dataItems) {
      const {products, orders} = dataItems || {};
      const isNoItems = user?.isSeller ? !products?.nodes?.length : !orders?.nodes?.length;
      if (isNoItems) {
        setStep(2);
      }
    }
  });
  const [searchedData, setSearchedData] = useState([]);

  const products = user?.isSeller
    ? data?.products?.nodes
    : data?.orders?.nodes
        ?.reduce(
          (accOrder, currOrder) => [
            ...accOrder,
            ...currOrder?.lineItems?.nodes?.map((item) => ({...item?.product, ...item?.variation}))
          ],
          []
        )
        ?.filter((p, i, a) => a.findIndex((pr) => pr.id === p.id) === i);

  useEffect(() => {
    if (products?.length && !loading) {
      setSearchedData(products);
    }
  }, [loading]);

  function onSelect(prod) {
    if (selectedProducts?.findIndex((p) => p?.id === prod?.id) >= 0) {
      setSelectedProducts(selectedProducts?.filter((p) => p?.id !== prod?.id));
      return null;
    }

    if (selectedProducts?.length >= 5) {
      return null;
    }

    setSelectedProducts([...selectedProducts, prod]);
    return null;
  }

  const onSearch = (e) => {
    const {
      target: {value}
    } = e;

    setSearch(value);

    if (user?.isSeller) return;

    if (value) {
      const searchValue = value.toLowerCase();
      setSearchedData(products?.filter((item) => item.name.toLowerCase().includes(searchValue)));
    } else {
      setSearchedData(products);
    }
  };

  function renderOrders() {
    return (
      searchedData &&
      searchedData?.map((prod) => {
        const {id, name} = prod || {};
        const {price, image} = prod || {};
        const {sourceUrl, altText} = image || {};

        return (
          <Prod key={id} onClick={() => onSelect(prod)}>
            <ProdWrap>
              <CheckWrap selected={selectedProducts?.map((i) => i?.databaseId)?.includes(prod?.databaseId)}>
                <Check type="checkbox" color="#fff" width="27" height="20" />
              </CheckWrap>
              <ProdImg src={sourceUrl} alt={altText} />
            </ProdWrap>
            <Info>
              <ProdTitle>{name}</ProdTitle>
              <Price>
                {/* <Currency>BD</Currency> */}
                {price}
              </Price>
              <Shipping>{}</Shipping>
            </Info>
            <PlusIcon
              selected={selectedProducts?.findIndex((p) => p?.id === id) >= 0 ? 1 : 0}
              type="plus"
              width={16}
              height={16}
            />
          </Prod>
        );
      })
    );
  }

  return (
    <Container>
      <Header>
        <Title>Search Products</Title>
        <Selected>{selectedProducts?.length || 0} Selected</Selected>
      </Header>
      <SearchWrapper>
        <SearchIcon type="search" />
        <Search type="text" value={search} onChange={onSearch} placeholder="Search Products" />
        <Cancel>Cancel</Cancel>
      </SearchWrapper>
      <List>{loading ? <Loader /> : renderOrders()}</List>
    </Container>
  );
};

export default SelectProducts;
