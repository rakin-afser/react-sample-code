import React, {useEffect, useRef, useState} from 'react';
import {
  DeleteBtn,
  EmbedWrapper,
  IconPlay,
  IframeWrap,
  Image,
  Slide,
  SlidePlaceholder,
  SliderNavImage,
  SliderNavItem,
  SliderNavWrapper,
  SliderNewItem,
  SliderNewItemWrap,
  SlideVideo,
  StyledSlider,
  Wrap,
  RatioList,
  RatioItem,
  RatioSymbol,
  PostTypeSwitcher
} from './styled';
import Icon from 'components/Icon';
import Input from 'components/Input';
import AddNewPostPopup from 'components/Modals/mobile/AddNewPostPopup';
import ReactPlayer from 'react-player';
import Ratios from 'components/Ratios';

const CreatePost = ({
  ratio,
  setRatio,
  uploads,
  setUploads,
  type,
  setType,
  youtubeLink,
  setYoutubeLink,
  noYouTubeLink
}) => {
  const sliderRef = useRef(null);
  const [slideIndex, setSlideIndex] = useState(0);
  const videoRef = useRef(null);
  const [previewVideoStart, setPreviewVideoStart] = useState(false);
  const [active, setActive] = useState(false);

  const hasImages = uploads.some((a) => a?.file?.type?.split('/')?.[0] === 'image');

  const canBeFlash = uploads?.length === 0 || uploads?.length > 1 || hasImages;

  useEffect(() => {
    if (type === 'flash') {
      setRatio('2:3');
    }
  }, [type, setRatio]);

  useEffect(() => {
    if (hasImages || !uploads?.length) {
      setType('');
    }
  }, [hasImages, uploads, setType]);

  const options = {
    arrows: false,
    infinite: false,
    dots: true,
    beforeChange: (_current, next) => setSlideIndex(next)
  };

  function renderSlide(slide) {
    const onVideoPlay = () => {
      if (previewVideoStart) {
        setPreviewVideoStart(false);
        videoRef.current.pause();
      } else {
        setPreviewVideoStart(true);
        videoRef.current.play();
      }
    };

    switch (slide?.file?.type?.split('/')?.[0]) {
      case 'video':
        return (
          <EmbedWrapper>
            <IconPlay previewVideoStart={previewVideoStart} onClick={onVideoPlay} type="play" width={30} height={30} />
            <SlideVideo ref={videoRef} controls={false} src={slide.result} />
          </EmbedWrapper>
        );
      case 'image':
        return <Image style={{backgroundImage: `url(${slide.result})`}} />;
      default:
        return null;
    }
  }

  function renderDotSlide(slide) {
    switch (slide?.file?.type?.split('/')?.[0]) {
      case 'video':
        return <IconPlay circleSize={50} type="play" width={30} height={30} />;
      case 'image':
        return <SliderNavImage src={slide.result} alt={slide.id} />;
      default:
        return null;
    }
  }

  return (
    <>
      <StyledSlider ref={sliderRef} {...options}>
        {uploads.map((slide) => {
          const [width, height] = ratio?.split(':') || [];
          return (
            <Slide key={slide.id}>
              <SlidePlaceholder ratio={height / width} />
              {renderSlide(slide)}
              <DeleteBtn onClick={() => setUploads(uploads?.filter((sl) => sl.id !== slide.id))} />
            </Slide>
          );
        })}
      </StyledSlider>
      {type !== 'flash' && (
        <Wrap>
          <Ratios ratio={ratio} setRatio={setRatio} />
        </Wrap>
      )}

      <PostTypeSwitcher type={type}>
        <button type="button" onClick={() => setType('')}>
          Post
        </button>
        <button type="button" onClick={() => setType(canBeFlash ? '' : 'flash')}>
          Flash
        </button>
      </PostTypeSwitcher>
      {type === '' ? (
        <Wrap py={9}>
          <SliderNavWrapper>
            {uploads.map((slide, i) => (
              <SliderNavItem
                key={slide.id}
                active={slideIndex === i ? 1 : 0}
                onClick={() => sliderRef.current.slickGoTo(i)}
              >
                {renderDotSlide(slide)}
              </SliderNavItem>
            ))}
            <SliderNewItem>
              <SliderNewItemWrap onClick={() => setActive(true)}>
                <Icon type="plus" width={16} height={16} color="#000" />
                Add More
              </SliderNewItemWrap>
            </SliderNewItem>
          </SliderNavWrapper>
        </Wrap>
      ) : null}

      {!noYouTubeLink && (
        <Wrap py={24}>
          <Input
            value={youtubeLink}
            onChange={(e) => setYoutubeLink(e.target.value)}
            label="Add YouTube Link"
            placeholder="Here you can put your YouTube link"
          />
        </Wrap>
      )}
      {!noYouTubeLink && youtubeLink ? (
        <Wrap>
          <IframeWrap>
            <ReactPlayer playsinline url={youtubeLink} width="100%" height="100%" className="react-player" />
          </IframeWrap>
        </Wrap>
      ) : null}
      <AddNewPostPopup active={active} setActive={setActive} setUploads={setUploads} uploads={uploads} />
    </>
  );
};

export default CreatePost;
