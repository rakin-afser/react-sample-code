import styled from 'styled-components';
import Icon from 'components/Icon';
import {Button} from 'antd';
import {mainFont} from 'components/Header/constants';

export const Container = styled.div`
  padding: 0 16px;
`;

export const Header = styled.div`
  display: flex;
  color: #666;
  align-items: flex-end;
  padding-top: 16px;
  padding-bottom: 16px;
`;

export const Selected = styled.p`
  font-size: 12px;
  margin-left: auto;
  margin-bottom: 0;
`;

export const Title = styled.h5`
  font-size: 18px;
  margin-bottom: 0;
`;

export const Search = styled.input`
  height: 40px;
  border: none;
  border-radius: 4px;
  background-color: #efefef;
  width: 100%;
  outline: none;
  padding-right: 16px;
  padding-left: 50px;
  margin-bottom: 0;
  caret-color: #ed484f;

  &::placeholder {
    color: #999;
  }
`;

export const SearchIcon = styled(Icon)`
  position: absolute;
  left: 14px;
  top: 50%;
  transform: translateY(-50%);
`;

export const SearchWrapper = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  margin-bottom: 16px;
`;

export const Cancel = styled(Button)`
  &&& {
    font-weight: 500;
    color: #ed484f;
    border: none;
    padding: 5px;
    margin-left: 6px;
  }
`;

export const List = styled.div`
  max-height: calc(100vh - 54px - 59px - 47px - 56px);
  overflow: auto;
  padding-bottom: 84px;
`;

export const Prod = styled.div`
  font-family: ${mainFont};
  display: flex;
  align-items: center;
  padding: 2px 22px 2px 2px;
  border: 1px solid #efefef;
  border-radius: 2px;
  width: 100%;

  & + & {
    margin-top: 7px;
  }
`;

export const ProdWrap = styled.div`
  margin-right: 16px;
  position: relative;
  border-radius: 1px;
  overflow: hidden;
`;

export const CheckWrap = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  opacity: ${({selected}) => (selected ? 1 : 0)};
  background-color: rgba(0, 0, 0, 0.44);
  transition: opacity 0.2s ease-in;
`;

export const Check = styled(Icon)`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const ProdImg = styled.img`
  width: 72px;
  height: 72px;
  object-fit: cover;
`;

export const ProdTitle = styled.h4`
  color: #000;
  margin-bottom: 3px;
  width: 100%;
  min-width: 0;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

export const Info = styled.div`
  width: calc(100% - 72px - 16px - 23px);
`;

export const Price = styled.p`
  color: #000;
  margin-bottom: 8px;
`;

export const Currency = styled.span`
  color: #666;
  font-size: 10px;
  padding-right: 2px;
`;

export const Shipping = styled.p`
  font-size: 12px;
  line-height: 1;
  margin-bottom: 0;
`;

export const PlusIcon = styled(Icon)`
  margin-left: auto;
  transform: rotate(${({selected}) => (selected ? 225 : 0)}deg);
  transition: transform 0.2s ease-in;
`;
