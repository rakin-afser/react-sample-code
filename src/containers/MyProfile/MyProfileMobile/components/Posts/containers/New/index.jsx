import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router';

import {Container, Header, PageTitle, StyledProgress, Back} from './styled';
import StepNav from './components/StepNav';
import CreatePost from './components/CreatePost';
import SelectProducts from './components/SelectProducts';
import Details from './components/Details';
import Preview from './components/Preview';
import {useUser} from 'hooks/reactiveVars';

const New = () => {
  const [step, setStep] = useState(0);
  const {push, goBack, location, replace} = useHistory();
  const [uploads, setUploads] = useState(location?.state?.medias || []);
  const [youtubeLink, setYoutubeLink] = useState('');
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [user] = useUser();
  const [tags, setTags] = useState([]);
  const [type, setType] = useState('');
  const [ratio, setRatio] = useState('1:1');

  // useEffect(() => {
  //   if (!location?.state) {
  //     push('/feed');
  //   }
  // }, []);

  useEffect(() => {
    if (location?.state) {
      replace('/post/new', null);
    }
  }, [location, replace]);

  const data = {
    author: {
      node: {
        ...user
      }
    },
    ratio,
    title,
    content,
    galleryImages: {
      nodes: uploads?.map((upload) => ({
        mediaItemUrl: URL.createObjectURL(upload?.file),
        mimeType: upload?.file?.type?.split('/')?.[0] === 'video' ? 'default-video' : upload?.file?.type
      }))
    },
    tags: {nodes: tags?.map((i) => ({name: i}))},
    featuredImage: {node: {sourceUrl: uploads?.[0]?.result, altText: title}},
    relatedProducts: {nodes: selectedProducts},
    video: youtubeLink,
    testSampleType: type
  };

  const addTag = (tag) => {
    if (!tag) return;
    if (!tags.includes(tag)) {
      setTags([...tags, tag]);
    }
  };

  const removeTag = (tag) => {
    setTags(tags.filter((item) => item !== tag));
  };

  function renderSteps() {
    switch (step) {
      case 0:
        return (
          <CreatePost
            ratio={ratio}
            setRatio={setRatio}
            noYouTubeLink
            uploads={uploads}
            setUploads={setUploads}
            type={type}
            setType={setType}
            youtubeLink={youtubeLink}
            setYoutubeLink={setYoutubeLink}
          />
        );
      case 1:
        return (
          <SelectProducts
            setStep={setStep}
            selectedProducts={selectedProducts}
            setSelectedProducts={setSelectedProducts}
          />
        );
      case 2:
        return (
          <Details
            removeTag={removeTag}
            tags={tags}
            addTag={addTag}
            title={title}
            setTitle={setTitle}
            content={content}
            setContent={setContent}
          />
        );
      case 3:
        return <Preview data={data} onBack={setStep} uploads={uploads} />;
      default:
        return null;
    }
  }

  function onBack() {
    if (step > 0) {
      setStep(step - 1);
    }
    if (step === 0) {
      goBack();
    }
  }

  const pb = [50, 0, 80, 0, 0];

  const pageTitle = [type === 'flash' ? 'Create Flash' : 'Create Post', 'Select Products', 'Details', 'Post Preview'];

  const stepNavOptions = [
    {
      simple: true,
      onNext: () => {
        if (uploads?.length > 0) setStep(1);
      },
      disable: uploads?.length === 0
    },
    {text: 'Add details about your product, photo/video', onNext: () => setStep(2)},
    {onNext: () => setStep(3), next: 'Preview', onCancel: () => setStep(1)}
  ];

  return (
    <Container pb={pb[step]}>
      <Header>
        <Back color="#000" type="arrowBack" onClick={onBack} />
        <PageTitle>{pageTitle[step]}</PageTitle>
        {step !== 3 ? <StyledProgress percent={((step + 1) / 3) * 100} /> : null}
      </Header>
      {renderSteps()}
      {step !== 3 ? <StepNav {...stepNavOptions[step]} /> : null}
    </Container>
  );
};

export default New;
