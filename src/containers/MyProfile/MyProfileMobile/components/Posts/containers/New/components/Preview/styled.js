import {Button} from 'antd';
import styled from 'styled-components';

export const Back = styled(Button)`
  &&& {
    border: none;
    color: #ed484f;
    padding: 5px;
    height: 36px;
    border-radius: 36px;
  }
`;
