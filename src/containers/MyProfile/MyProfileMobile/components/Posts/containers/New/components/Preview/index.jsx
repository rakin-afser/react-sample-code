import React, {useState} from 'react';
import Post from 'components/Post';
import Grid from 'components/Grid';
import {Back} from './styled';
import {useHistory} from 'react-router';
import axiosClient from 'axiosClient';
import axios from 'axios';
import Btn from 'components/Btn';
import {useCreateFlashPostMutation, useCreatePostMutation} from 'hooks/mutations';

const Preview = ({onBack, data, uploads}) => {
  const [loading, setLoading] = useState(false);
  const {push} = useHistory();
  const [createPost, {loading: loadingPost}] = useCreatePostMutation();

  const [createFlashPost, {loading: loadingFlashPost}] = useCreateFlashPostMutation({
    onCompleted() {
      push('/');
    }
  });

  async function onPost() {
    const {title, content, video, relatedProducts, tags, testSampleType, ratio} = data;

    const filesToUpload = uploads?.map((upload) => {
      const formData = new FormData();
      formData.append('file', upload?.file);
      return axiosClient.post('/', formData);
    });
    try {
      setLoading(true);
      const res = await axios.all(filesToUpload);
      const galleryId = res?.map((item) => item.data.id);
      setLoading(false);

      if (!testSampleType) {
        createPost({
          variables: {
            input: {
              status: 'PUBLISH',
              ratio,
              title,
              content,
              video,
              tags,
              relatedProducts: relatedProducts?.nodes?.map((p) => p.databaseId),
              galleryId,
              testSampleType
            }
          }
        });
        return;
      }

      createFlashPost({
        variables: {
          input: {
            status: 'PUBLISH',
            ratio,
            title,
            content,
            video,
            tags,
            relatedProducts: relatedProducts?.nodes?.map((p) => p.databaseId),
            galleryId,
            testSampleType
          }
        }
      });
    } catch (error) {
      setLoading(false);
      console.log(`error`, error);
    }
  }

  return (
    <>
      <div>
        <Post data={data} showActions={false} showMoreButton={false} isStandStill />
      </div>
      <Grid sb aic padding="0 16px 16px">
        <Back onClick={() => onBack(2)}>Back to Edit</Back>
        <Btn kind="primary-small" loading={loading || loadingPost} onClick={() => onPost()}>
          Post
        </Btn>
      </Grid>
    </>
  );
};

export default Preview;
