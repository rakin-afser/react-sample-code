import {Button, Mentions} from 'antd';
import styled from 'styled-components';

export const Container = styled.div`
  padding: 0 16px;
`;

export const Title = styled.input`
  margin-top: 24px;
  margin-bottom: 12px;
  font-size: 18px;
  color: #000;
  font-weight: 500;
  border: none;
  width: 100%;
  outline: none;
  padding: 0;

  &::placeholder {
    padding-top: 4px;
    color: #999;
    font-size: 14px;
    font-weight: 400;
  }
`;

export const Desc = styled(Mentions)`
  &&& {
    margin-bottom: 24px;
    font-size: 18px;
    color: #000;
    border: none;
    width: 100%;
    outline: none;
    padding: 0;
    height: 228px;
    line-height: 1.333;
    resize: none;
    box-shadow: none;

    &::placeholder {
      padding-top: 4px;
      color: #999;
      font-size: 14px;
      font-weight: 400;
    }

    > textarea {
      padding: 0;
    }
  }
`;

export const Share = styled.h4`
  font-size: 16px;
  font-weight: 500;
  line-height: 1.25;
  margin-bottom: 24px;
`;

export const Social = styled.p`
  color: #999;
  font-size: 16px;
  line-height: 1.18;
  margin-bottom: 0;
`;

export const Connect = styled(Button)`
  &&& {
    height: 27px;
    border: 1px solid #cccccc;
    border-radius: 30px;
    color: #999999;
    padding: 0 8px;
  }
`;

export const Tags = styled.div`
  min-width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-grow: 2;
  max-width: 80%;
  margin: -10px 0 20px -18px;
`;

export const Tag = styled.div`
  font-family: SF Pro Display;
  color: #464646;
  cursor: pointer;
  display: flex;
  align-items: baseline;
  background: #e4e4e4;
  border-radius: 24px;
  font-size: 14px;
  padding: 5px 27px 5px 12px;
  position: relative;
  margin: 10px 0 0 15px;
  display: flex;
  align-items: center;

  & svg {
    width: 18px;
    height: 18px;
    right: 7px;
    position: absolute;
    transition: transform 0.3s;
    &:hover {
      transform: scale(1.2);
    }
  }
`;
