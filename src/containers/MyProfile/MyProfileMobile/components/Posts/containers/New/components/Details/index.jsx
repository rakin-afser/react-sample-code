import React, {useEffect, useState} from 'react';
import {Mentions} from 'antd';
import {useQuery} from '@apollo/client';

import {Container, Title, Desc, Share, Social, Connect, Tags, Tag} from './styled';
import Grid from 'components/Grid';
import {GET_TAGS} from 'components/CreatePost/api/queries';
import Icon from 'components/Icon';

const {Option} = Mentions;

const Details = ({tags = [], title, setTitle, content, setContent, addTag = () => {}, removeTag = () => {}}) => {
  const [currentTag, setCurrentTag] = useState('');
  const [currentOptions, setCurrentOptions] = useState([]);
  const {data: tagsData, loading: tagsLoading, error: tagsError} = useQuery(GET_TAGS, {variables: {search: currentTag}});

  useEffect(() => {
    const options = tagsData?.tags?.nodes?.map((i) => i.name) || [];
    if (!tagsLoading) {
      if (options.includes(currentTag)) {
        setCurrentOptions(options);
      } else {
        setCurrentOptions([...options, currentTag]);
      }
    }
  }, [currentTag, tagsData, tagsLoading]);

  const onSelect = (tag) => {
    addTag(tag?.value);

    if (tag?.value) {
      const newContent = content.replace(`#${tag?.value}`, '');
      
      setContent(newContent.replace('#', ''));
    }
  };

  const onChange = (value) => {
    setContent(value);
  };

  const onSearch = (value) => {
    setCurrentTag(value);
  };

  return (
    <Container>
      <Title type="text" placeholder="Type your title" onChange={(e) => setTitle(e.target.value)} value={title} />
      <Desc
        onSelect={onSelect}
        rows={9}
        prefix="#"
        placeholder="Describe your post"
        value={content}
        onChange={onChange}
        onSearch={onSearch}
      >
        {currentOptions.map((option) => (
          <Option key={option} value={option}>
            #{option}
          </Option>
        ))}
      </Desc>
      {tags?.length ? <Share>Tags</Share> : null}
      <Tags>
        {tags?.map((tag, i) => (
          <Tag key={i}>
            #{tag} <Icon onClick={() => removeTag(tag)} type="close" color="#464646" />
          </Tag>
        ))}
      </Tags>
      <Share>Share</Share>
      <Grid sb aic>
        <Social>Facebook</Social>
        <Connect>Connect</Connect>
      </Grid>
    </Container>
  );
};

export default Details;
