import styled, {css} from 'styled-components/macro';

export const ListsWrapper = styled.div`
  background: #fafafa;
  padding: 8px 0 32px;
  display: flex;
  flex-direction: column;
  align-items: center;
  min-height: 80vh;

  ${({isEmpty}) =>
    isEmpty &&
    css`
      background: #fff;
    `}
`;
