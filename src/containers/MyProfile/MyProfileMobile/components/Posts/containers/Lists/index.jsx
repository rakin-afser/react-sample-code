import React from 'react';
import {useParams, useHistory} from 'react-router-dom';

import List from 'components/List';
import {useUser} from 'hooks/reactiveVars';
import {ListsWrapper} from 'containers/MyProfile/MyProfileMobile/components/Posts/containers/Lists/styled';
import Loader from 'components/Loader';
import {useListsQuery} from 'hooks/queries';
import {useOnScreen} from 'hooks';

const Lists = () => {
  const {userParam} = useParams();
  const [user] = useUser();
  const history = useHistory();
  const isGuest = userParam && +userParam !== user?.databaseId;
  const first = 5;
  const {data, loading, fetchMore} = useListsQuery({
    variables: {
      first,
      where: {
        user_id: isGuest ? +userParam : user?.databaseId,
        is_private: isGuest ? false : null
      }
    },
    notifyOnNetworkStatusChange: true
  });

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first,
          where: {
            user_id: isGuest ? +userParam : user?.databaseId,
            is_private: isGuest ? false : null
          },
          after: endCursor
        }
      });
    }
  });

  const {hasNextPage, endCursor} = data?.lists?.pageInfo || {};
  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  if (!data?.lists?.nodes?.length && loading) return <Loader wrapperWidth="100%" wrapperHeight="200px" />;

  return (
    <ListsWrapper isEmpty={!data?.lists?.nodes?.length}>
      {data?.lists?.nodes?.map((item) => (
        <List
          key={item?.id}
          title={item.list_name}
          onClick={() => history.push(`/profile/${userParam ? `${userParam}/` : ''}lists/${item?.list_id}`)}
          totalFollowers={item.totalFollowers}
          isPrivate={item.is_private}
          products={item?.addedProducts?.nodes || []}
        />
      ))}
      {data?.lists?.nodes?.length && loading ? (
        <Loader wrapperWidth="100%" wrapperHeight="100px" dotsSize="10px" />
      ) : null}
      {endRef}
    </ListsWrapper>
  );
};

export default Lists;
