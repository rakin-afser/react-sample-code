import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useParams} from 'react-router-dom';
import {useQuery} from '@apollo/client';

import ArrowBack from 'assets/ArrowBack';
import ProfileSwitcher from 'containers/MyProfile/MyProfileMobile/components/ProfileSwitcher';
import {Wrapper, BackTo, TabsWrapper, Tab, PopupWrapper, PopupContent, PopUpHeader} from './styled';
import AccountHeader from 'components/AccountHeader';
import Posts from './containers/Posts';
import Lists from './containers/Lists';
import {USER_PROFILE} from 'queries';
import Loader from 'components/Loader';
import Error from 'components/Error';
import ListView from 'containers/MyProfile/MyProfileMobile/components/ListView';

const Popup = ({isOpen, onClose, user, selected, onChange, title}) => {
  return (
    <PopupWrapper isOpen={isOpen}>
      <PopupContent onClick={onClose}>
        {/* <PopUpHeader> */}
        {/*  <div onClick={onClose}> */}
        {/*    <Close width={28} height={28} color="#000" /> */}
        {/*  </div> */}
        {/*  {title} */}
        {/* </PopUpHeader> */}
        <ProfileSwitcher isPopUp user={user} activeProfile={selected} onChange={onChange} />
      </PopupContent>
    </PopupWrapper>
  );
};

const Page = ({page, user}) => {
  const {t} = useTranslation();
  const [popupOpened, setPopupOpened] = useState(false);
  const {slug, userParam} = useParams();
  const {data, loading, error} = useQuery(USER_PROFILE, {
    variables: {id: userParam || user?.databaseId}
  });

  const isGuest = userParam && +userParam !== user?.databaseId;

  if (isGuest && loading) return <Loader />;
  if (isGuest && error) return <Error />;

  if (slug) return <ListView />;

  const userData = data?.user;

  return (
    <Wrapper popupOpened={popupOpened}>
      {/* !isGuest && (
        <BackTo>
          {/* disabled at the moment */}
      {/* <Link to="/" className="back">
            <ArrowBack stroke="#000" />
          </Link> */}
      {/* t('myProfilePage.myAccount')}
        </BackTo>
      ) */}
      <AccountHeader isOwner={!isGuest} user={userData} onSwitchUser={() => setPopupOpened(true)} />
      <TabsWrapper activeTab={page === 'posts' ? 1 : 2}>
        <Tab isActive={page === 'posts'} to={`/profile/${userParam ? `${userParam}/` : ''}posts`}>
          {t('myProfilePage.posts')}
        </Tab>
        <Tab isActive={page === 'lists'} to={`/profile/${userParam ? `${userParam}/` : ''}lists`}>
          {t('myProfilePage.lists')}
        </Tab>
      </TabsWrapper>
      {page === 'lists' ? <Lists /> : <Posts />}
      {/* disabled at the moment */}
      {/* <Popup
        title={t('myProfilePage.changeProfile')}
        isOpen={popupOpened}
        onClose={() => setPopupOpened(false)}
        user={userData}
        selected={activeProfile}
        onChange={setActiveProfile}
      /> */}
    </Wrapper>
  );
};

export default Page;
