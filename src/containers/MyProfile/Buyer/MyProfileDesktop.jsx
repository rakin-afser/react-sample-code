import React, {useState} from 'react';
import {useParams, Redirect} from 'react-router-dom';
import {string} from 'prop-types';
import {userId} from 'util/heplers';

import Grid from 'components/Grid';
import Chat from 'components/Chat';
import LeaveFeedback from 'components/Modals/LeaveFeedback';
import useGlobal from 'store';
import PostsWrapper from 'containers/MyProfile/components/PostsWrapper';
import {Content as ContentStyled, Nav, Tab, AddPostBtn} from 'containers/MyProfile/styled';
import SideBar from 'containers/MyProfile/components/SIdebar';
import Lists from 'containers/MyProfile/components/Lists';

import Notifications from 'containers/MyProfile/components/Notifications';
import List from 'containers/MyProfile/components/List';
import Activity from 'containers/MyProfile/components/Activity';
import AccountSettings from 'containers/AccountSettings';
import OrdersPage from 'containers/OrdersPage';
import MyRewards from 'containers/MyProfile/components/MyRewards';
import Promote from 'containers/MyProfile/components/Promote';
import Icons from 'components/Icon';
import CreatePost from 'components/CreatePost';
import {useUser} from 'hooks/reactiveVars';

const MyProfileDesktop = ({page}) => {
  const {slug, userParam} = useParams();
  const [globalState] = useGlobal();
  const [user] = useUser();
  const [visible, setVisible] = useState(false);
  const activeTab = ['lists', 'posts', 'activity'].includes(page) ? page : 'posts';
  const isGuest = userParam && userParam !== user?.databaseId;

  const menuLinks = [
    {id: 'profile', href: `/profile/${userParam ? `${userParam}/` : ''}posts`, text: 'My Profile'},
    {id: 'orders', href: `/profile/${userParam ? `${userParam}/` : ''}orders`, text: 'My Orders'},
    {id: 'rewards', href: `/profile/${userParam ? `${userParam}/` : ''}rewards`, text: 'My Rewards'},
    {id: 'messages', href: `/profile/${userParam ? `${userParam}/` : ''}messages`, text: 'Messages'},
    {id: 'notifications', href: `/profile/${userParam ? `${userParam}/` : ''}notifications`, text: 'Notifications'},
    {id: 'settings', href: `/profile/${userParam ? `${userParam}/` : ''}settings`, text: 'Settings'}
  ];

  const getPage = () => {
    switch (page) {
      case 'orders':
        return <OrdersPage />;
      case 'rewards':
        return <MyRewards />;
      case 'messages':
        return <Chat />;
      case 'notifications':
        return <Notifications />;
      case 'settings':
        return <AccountSettings />;
      case 'promote':
        return <Promote />;
      case 'profile':
      case 'posts':
      case 'lists':
      default:
        return null;
    }
  };

  if (user?.capabilities?.includes('seller') && userParam === user?.databaseId) {
    return <Redirect to={`/shop/${user?.databaseId}/products`} />;
  }

  if (user?.capabilities?.includes('seller') && !userParam) {
    return <Redirect to={`/shop/${user?.databaseId}/products`} />;
  }

  return (
    <Grid pageContainer>
      <Grid>
        <SideBar menuLinks={menuLinks} selected={['lists', 'posts'].includes(page) ? 'profile' : page} />

        {['profile', 'lists', 'posts', 'activity'].includes(page) ? (
          <div style={{width: '100%'}}>
            {!slug && (
              <ContentStyled>
                <Nav>
                  {!isGuest ? (
                    <AddPostBtn onClick={() => setVisible(true)}>
                      <Icons type="plus" color="#fff" />
                    </AddPostBtn>
                  ) : null}

                  <Tab active={activeTab === 'posts'} to={`/profile/${userParam ? `${userParam}/` : ''}posts`}>
                    Posts
                  </Tab>
                  <Tab active={activeTab === 'lists'} to={`/profile/${userParam ? `${userParam}/` : ''}lists`}>
                    Lists
                  </Tab>
                  {/** temporary hide activity tab */}
                  {/* {!isGuest && ( */}
                  {/*   <Tab active={activeTab === 'activity'} to={`/profile/${userParam ? `${userParam}/` : ''}activity`}> */}
                  {/*     Activity */}
                  {/*   </Tab> */}
                  {/* )} */}
                </Nav>
                {activeTab === 'lists' && <Lists />}
                {activeTab === 'posts' && <PostsWrapper />}
                {activeTab === 'activity' && <Activity />}
              </ContentStyled>
            )}
            {!!slug && activeTab === 'lists' && <List />}
          </div>
        ) : null}
        {getPage()}
      </Grid>
      <LeaveFeedback displaying={globalState.leaveFeedback.open} />
      <CreatePost visible={visible} onClose={() => setVisible(false)} />
    </Grid>
  );
};

MyProfileDesktop.propTypes = {
  page: string.isRequired
};

export default MyProfileDesktop;
