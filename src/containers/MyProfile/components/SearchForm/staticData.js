export const categories = [
  {id: 1, value: 'fashion', title: 'Fashion'},
  {id: 2, value: 'beauty', title: 'Beauty'},
  {id: 3, value: 'hoodie', title: 'Hoodie'}
];

export const sortVariants = [
  {id: 1, value: 'name', title: 'Name'},
  {id: 2, value: 'date', title: 'Date'},
  {id: 3, value: 'price_low_to_high', title: 'Price (Low to High)'},
  {id: 4, value: 'price_high_to_low', title: 'Price (High to Low)'}
];

export const selectedCategories = [
  {id: 1, title: 'Fashion'},
  {id: 2, title: 'Beauty'},
  {id: 3, title: 'Hoodie'}
];
