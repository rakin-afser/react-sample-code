import styled from 'styled-components';
import {
  mainBlackColor as black,
  secondaryTextColor as gray800,
  headerShadowColor as gray400,
  transparentTextColor as gray300,
  primaryColor as primary
} from 'constants/colors';

export const Form = styled.form`
  margin-bottom: 30px;
  width: 600px;
`;

export const SearchInputWrapper = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 25px;
`;

export const SearchInput = styled.input`
  background: rgba(228, 228, 228, 0.32);
  border: none;
  line-height: 1;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  padding: 10px 15px;
  width: 100%;

  &:focus {
    outline: none;
  }

  &::placeholder {
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    color: ${gray300};
  }
`;

export const SearchButton = styled.button`
  background-color: ${primary};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
`;

export const Filter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Text = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 22px;
  color: ${black};
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`;

export const SelectedCategories = styled.div`
  display: flex;
  flex-direction: row;
`;

export const SelectedCategory = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: ${gray800};
  background-color: ${gray400};
  border-radius: 24px;
  padding: 6px 12px;
  cursor: pointer;

  & + & {
    margin-left: 16px;
  }
`;

export const ClearAllButton = styled.button`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${primary};

  &:hover {
    text-decoration: underline;
  }
`;
