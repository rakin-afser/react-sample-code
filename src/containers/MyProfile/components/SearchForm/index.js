import React, {useState} from 'react';
import {
  Form,
  SearchInputWrapper,
  SearchInput,
  SearchButton,
  Filter,
  Text,
  Flex,
  SelectedCategories,
  SelectedCategory,
  ClearAllButton
} from './styled';
import {FormControl, Select, MenuItem} from '@material-ui/core';
import {makeStyles} from '@material-ui/core/styles';
import SearchIcon from 'assets/SearchIcon';
import {categories, sortVariants, selectedCategories} from './staticData';

const useStyles = makeStyles({
  formControl: {
    flex: 1,
    marginLeft: 20
  },
  select: {
    '&::before': {
      borderColor: '#C3C3C3'
    },
    '&$focus': {
      backgroundColor: 'transparent'
    }
  }
});

const SearchForm = () => {
  const [searchText, setSearchText] = useState('');
  const [category, setCategory] = useState('');
  const [sortVariant, setSortVariant] = useState('');
  const [showExtraParams, setShowExtraParams] = useState(false);

  const styles = useStyles();

  const handleSearchTextChange = (event) => setSearchText(event.target.value);
  const handleCategoryChange = (event) => setCategory(event.target.value);
  const handleSortVariantChange = (event) => setSortVariant(event.target.value);

  const onSearchClick = (event) => {
    event.preventDefault();

    if (searchText) {
      setShowExtraParams(true);
    }
  };

  const onClearAllClick = (event) => {
    event.preventDefault();

    setCategory(false);
    setSortVariant(false);
  };

  return (
    <Form>
      <SearchInputWrapper>
        <SearchInput
          type="text"
          placeholder="Search for Products &#38; Stores"
          value={searchText}
          onChange={handleSearchTextChange}
        />
        <SearchButton onClick={onSearchClick}>
          <SearchIcon />
        </SearchButton>
      </SearchInputWrapper>
      {showExtraParams && (
        <>
          <Filter>
            <Text>Filter</Text>
            <FormControl className={styles.formControl}>
              <Select className={styles.select} value={category} onChange={handleCategoryChange} displayEmpty>
                <MenuItem value="" disabled>
                  Category
                </MenuItem>
                {categories.map(({id, value, title}) => (
                  <MenuItem key={id} value={value}>
                    {title}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <FormControl className={styles.formControl}>
              <Select value={sortVariant} onChange={handleSortVariantChange} displayEmpty className={styles.select}>
                <MenuItem value="" disabled>
                  Sort By
                </MenuItem>
                {sortVariants.map(({id, value, title}) => (
                  <MenuItem key={id} value={value}>
                    {title}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Filter>
          <Flex>
            <SelectedCategories>
              {selectedCategories.map(({id, title}) => (
                <SelectedCategory key={id}>{title}</SelectedCategory>
              ))}
            </SelectedCategories>
            <ClearAllButton onClick={onClearAllClick}>Clear All</ClearAllButton>
          </Flex>
        </>
      )}
    </Form>
  );
};

export default SearchForm;
