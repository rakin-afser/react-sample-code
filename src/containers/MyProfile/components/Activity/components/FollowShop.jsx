import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Grid from 'components/Grid';
import CardShop from 'components/CardShop';
import useGlobal from 'store';
import {AvatarsHolder, Circle, Panel, Text, ShowFollowers} from '../styled';

const FollowShop = ({data, userAvatar}) => {
  const [, setGlobalState] = useGlobal();

  const followersList = () => {
    setGlobalState.setFollowersModal(true);
  };

  const RenderForMultiple = () => {
    return (
      <>
        and <ShowFollowers onClick={followersList}>{data.items.length} more</ShowFollowers>
      </>
    );
  };

  return (
    <Panel>
      <Grid>
        <AvatarsHolder>
          <Circle>
            <img src={userAvatar} alt="user avatar" />
          </Circle>
        </AvatarsHolder>

        <div>
          <Text>
            You started follow <strong>{data.items[0].name}</strong> {data.items.length > 1 && <RenderForMultiple />}
          </Text>
          <Grid wrap margin="4px 0 12px -8px" padding="0">
            {data.items?.map((shop, index) => (
              <CardShop
                key={index}
                title={shop.name}
                user={shop?.isUser}
                imgSrc={shop.img}
                location={shop?.location}
                rating={5}
                isFollowing={false}
              />
            ))}
          </Grid>
          <Text>{moment(data.date).fromNow()}</Text>
        </div>
      </Grid>
    </Panel>
  );
};

FollowShop.propTypes = {
  data: PropTypes.shape().isRequired,
  userAvatar: PropTypes.string.isRequired
};

export default FollowShop;
