import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Grid from 'components/Grid';
import {
  AvatarsHolder,
  Circle,
  Square,
  Panel,
  Text,
  ListWrapper,
  ListFollowers,
  ListName,
  ListFooter,
  ListImages
} from '../styled';

const ListWasLiked = ({data, userAvatar}) => {
  return (
    <Panel>
      <Grid>
        <AvatarsHolder>
          <Circle>
            <img src={userAvatar} alt="user avatar" />
          </Circle>
        </AvatarsHolder>

        <div>
          <Text>
            Your list <strong>{data.items[0].name}</strong> get {100 * Math.floor(data.items[0].listData.likes / 100)}+
            Likes
          </Text>
          <div>
            <ListWrapper>
              <ListImages>
                {data.items?.map((item) =>
                  item.listData.images.map((image, index) => (
                    <Square key={index} big>
                      <img src={image} alt="" />
                    </Square>
                  ))
                )}
              </ListImages>
              <ListFooter>
                <ListName>{data.items[0].listData.title}</ListName>
                <ListFollowers>{data.items[0].listData.followers} Followers</ListFollowers>
              </ListFooter>
            </ListWrapper>
          </div>
          <Text>{moment(data.date).fromNow()}</Text>
        </div>
      </Grid>
    </Panel>
  );
};

ListWasLiked.propTypes = {
  data: PropTypes.shape().isRequired,
  userAvatar: PropTypes.string.isRequired
};

export default ListWasLiked;
