import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Grid from 'components/Grid';
import useGlobal from 'store';
import {AvatarsHolder, Circle, Panel, Text, ShowFollowers} from '../styled';

const FollowUser = ({data}) => {
  const [, setGlobalState] = useGlobal();
  const followersList = () => {
    setGlobalState.setFollowersModal(true);
  };

  const RenderForMultiple = () => {
    return (
      <>
        and <ShowFollowers onClick={followersList}>{data.items.length} more</ShowFollowers>
      </>
    );
  };

  return (
    <Panel>
      <Grid>
        <AvatarsHolder>
          {data.items?.map((item, key) => (
            <Circle key={key} multiple={data.items.length > 1}>
              <img src={item.img} alt={item.name} />
            </Circle>
          ))}
        </AvatarsHolder>

        <div>
          <Text>
            You started follow <strong>{data.items[0].name}</strong> {data.items.length > 1 && <RenderForMultiple />}
          </Text>
          <Text>{moment(data.date).fromNow()}</Text>
        </div>
      </Grid>
    </Panel>
  );
};

FollowUser.propTypes = {
  data: PropTypes.shape().isRequired
};

export default FollowUser;
