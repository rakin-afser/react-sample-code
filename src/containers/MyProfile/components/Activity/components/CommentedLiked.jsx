import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Grid from 'components/Grid';
import {AvatarsHolder, Panel, Square, Text} from '../styled';

const CommentedLiked = ({data}) => {
  const RenderItems = () => {
    if (data.items.length > 1) {
      return (
        <>
          You {data.type} <strong>{data.items.length} post</strong>
        </>
      );
    }

    return (
      <>
        You {data.type} on <strong>{data.items[0].name}'s</strong> post{' '}
      </>
    );
  };

  return (
    <Panel>
      <Grid>
        <AvatarsHolder>
          {data.items?.map((item, key) => (
            <Square key={key} multiple={data.items.length > 1}>
              <img src={item.img} alt={item.name} />
            </Square>
          ))}
        </AvatarsHolder>

        <div>
          <Text>{RenderItems()}</Text>
          <Text>{moment(data.date).fromNow()}</Text>
        </div>
      </Grid>
    </Panel>
  );
};

CommentedLiked.propTypes = {
  data: PropTypes.shape().isRequired
};

export default CommentedLiked;
