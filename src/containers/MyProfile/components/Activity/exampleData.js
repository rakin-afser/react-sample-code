import avatar1 from './img/avatar1.png';
import avatar2 from './img/avatar2.png';
import card1 from './img/card1.png';
import card2 from './img/card2.png';
import card3 from './img/card3.png';
import shop1 from './img/shop1.jpg';
import shop2 from './img/shop2.jpg';
import shop3 from './img/shop3.jpg';

const exampleData = [
  {
    id: '1',
    type: 'followUser',
    items: [
      {id: 1, name: 'Scarlett Sane', img: avatar1},
      {id: 2, name: 'Scarlett Sane', img: avatar2},
      {id: 3, name: 'Scarlett Sane', img: avatar1}
    ],
    date: 'March 8, 2021 17:00:00'
  },
  {
    id: '2',
    type: 'followUser',
    items: [{id: 1, name: 'Jane Rose', img: avatar1}],
    date: 'March 18, 2021 17:00:00'
  },
  {
    id: '3',
    type: 'followShop',
    items: [
      {id: 1, name: 'Royal MADE official', img: shop1},
      {id: 2, name: 'Royal MADE official', img: shop2},
      {id: 3, name: 'Royal MADE official', img: shop3},
      {id: 4, name: 'Kathryn Mccoy', img: avatar2, isUser: true, location: 'Kiev, Ukraine'}
    ],
    date: 'March 18, 2021 17:00:00'
  },
  {
    id: '4',
    type: 'listWasLiked',
    items: [
      {
        id: 1,
        name: 'Autumn and Winter Collection 2020',
        listData: {images: [card1, card2, card3], likes: 1009, followers: 701, title: 'Fashion Ideas'}
      }
    ],
    date: 'March 20, 2021 00:00:00'
  },
  {
    id: '5',
    type: 'commented',
    items: [{id: 1, name: 'Jane Rose', img: card1}],
    date: 'March 18, 2021 17:00:00'
  },
  {
    id: '7',
    type: 'liked',
    items: [
      {id: 1, name: 'Jane Rose', img: card1},
      {id: 1, name: 'Jane Rose', img: card2},
      {id: 1, name: 'Jane Rose', img: card3}
    ],
    date: 'March 19, 2021 22:00:00'
  }
];

export default exampleData;
