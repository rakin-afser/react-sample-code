import React from 'react';
import FollowUser from 'containers/MyProfile/components/Activity/components/FollowUser';
import FollowShop from 'containers/MyProfile/components/Activity/components/FollowShop';
import ListWasLiked from 'containers/MyProfile/components/Activity/components/ListWasLiked';
import CommentedLiked from 'containers/MyProfile/components/Activity/components/CommentedLiked';
import FadeOnMount from 'components/Transitions';
import userAvatar from './img/avatar2.png';
import {Wrapper} from './styled';
import exampleData from './exampleData';

const Activity = () => {
  const renderByType = (data, type) => {
    switch (type) {
      case 'followUser':
        return <FollowUser key={data.id} data={data} />;
      case 'followShop':
        return <FollowShop key={data.id} userAvatar={userAvatar} data={data} />;
      case 'listWasLiked':
        return <ListWasLiked key={data.id} userAvatar={userAvatar} data={data} />;
      case 'commented':
      case 'liked':
        return <CommentedLiked key={data.id} data={data} />;
      default:
        return null;
    }
  };

  return (
    <FadeOnMount>
      <Wrapper>{exampleData?.map((el) => renderByType(el, el.type))}</Wrapper>
    </FadeOnMount>
  );
};
Activity.propTypes = {};

export default Activity;
