import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  margin-top: 16px;
`;

export const Panel = styled.div`
  margin-bottom: 18px;
  padding: 15px;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
`;

export const Text = styled.div`
  font-size: 14px;
  color: #545454;

  strong {
    color: #000;
  }
`;

export const AvatarsHolder = styled.div`
  margin-right: 16px;
  display: flex;
`;

export const Circle = styled.div`
  width: ${({multiple}) => (multiple ? '32px' : '48px')};
  height: ${({multiple}) => (multiple ? '32px' : '48px')};
  min-width: ${({multiple}) => (multiple ? '32px' : '48px')};
  min-height: ${({multiple}) => (multiple ? '32px' : '48px')};
  background-color: #9b9b9b;
  border-radius: 50%;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
  }

  &:not(:first-child) {
    margin-left: -10px;
  }
`;

export const Square = styled.div`
  width: ${({big}) => (big ? '80px' : '50px')};
  height: ${({big}) => (big ? '80px' : '50px')};
  background-color: #9b9b9b;
  border: 2px solid #ffffff;
  border-radius: 4px;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
  }

  &:not(:first-child) {
    margin-left: ${({big}) => (big ? '0' : '-10px')};
  }
`;

export const ListWrapper = styled.div`
  margin-top: 14px;
  margin-bottom: 8px;
  padding: 6px 6px 9px;
  max-width: 254px;
  background: #ffffff;
  border: 1px solid #eeeeee;
  border-radius: 4px;
  overflow: hidden;
`;

export const ListImages = styled.div`
  margin-bottom: 9px;
  display: flex;
`;

export const ListFollowers = styled.div`
  font-size: 12px;
  color: #464646;
`;

export const ListName = styled.div`
  font-weight: 500;
  font-size: 14px;
  color: #000;
`;

export const ListFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ShowFollowers = styled.button`
  font-weight: 700;
  color: #000;
  transition: ease 0.4s;

  &:hover {
    color: ${primaryColor};
    text-decoration: underline;
    cursor: pointer;

    strong {
      color: inherit;
    }
  }
`;
