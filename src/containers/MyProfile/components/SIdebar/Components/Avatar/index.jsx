import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icon';
import {Avatar, AvatarPic, AvatarWrap, EditAvatar} from './styled';

const UserAvatar = ({img, setEdit, influencer, isMobile, logged}) => {
  return (
    <AvatarWrap isMobile={isMobile}>
      <Avatar big={influencer} isMobile={isMobile}>
        <AvatarPic src={img} alt="user avatar" />
      </Avatar>
      {logged && setEdit ? (
        <EditAvatar influencer={influencer} isMobile={isMobile} onClick={() => setEdit(true)}>
          <Icon type="pencil" />
        </EditAvatar>
      ) : null}
    </AvatarWrap>
  );
};

UserAvatar.defaultProps = {
  influencer: false,
  logged: false,
  isMobile: false
};

UserAvatar.propTypes = {
  img: PropTypes.node.isRequired,
  setEdit: PropTypes.func.isRequired,
  influencer: PropTypes.bool,
  logged: PropTypes.bool,
  isMobile: PropTypes.bool
};

export default UserAvatar;
