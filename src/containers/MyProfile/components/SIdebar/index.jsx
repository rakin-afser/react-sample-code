import React, {useState, useRef, useEffect} from 'react';
import {useQuery, gql, useMutation} from '@apollo/client';
import {useHistory, useParams} from 'react-router-dom';
import PropTypes from 'prop-types';

import avatar from 'components/Post/img/kathryn_mccoy.png';
import Icon from 'components/Icon';
import EditProfile from 'components/Modals/EditProfile';
import UserAvatar from 'containers/MyProfile/components/SIdebar/Components/Avatar';
import useGlobal from 'store';
import numberWithCommas from 'util/heplers';
import Grid from 'components/Grid';
import InstagramWidget from 'components/InstagramWidget';
import Error from 'components/Error';
import Loader from 'components/Loader';
import {useUser} from 'hooks/reactiveVars';
import ProfileMenu from '../ProfileMenu';
import {
  Aside,
  About,
  BookMark,
  Location,
  Name,
  Statistic,
  StatisticCount,
  DescriptionWrap,
  Description,
  Profession,
  InfluencerStatus,
  FollowersWrap,
  Title,
  Title2,
  ButtonsWrap,
  Button,
  Countries,
  Socials,
  Categories,
  CategoriesIcon,
  Tag
} from './styled';
import MyShop from '../MyShop';
import userPlaceholder from 'images/placeholders/user.jpg';
import {FOLLOW_UNFOLLOW_USER} from 'mutations';
import Btn from 'components/Btn';
import {useFollowUnfollowUserMutation} from 'hooks/mutations';

const profile = {
  avatar,
  name: 'kathryn_mccoy',
  location: 'Kiev, Ukraine',
  followers: 234,
  following: 345,
  profession: 'stylist',
  description:
    'Trendy overview for last season, tenden collaboration, FW The magazine covers international, national and local fashion and beauty trends and news.',
  followerRegion: 'GCC',
  totalPosts: 2384,
  totalLikes: 10253,
  countries: ['U.A.E', 'Bahrain', 'Qatar', 'K.S.A', 'Kuwait', 'Oman'],
  socials: [
    {id: 1, link: 'https://twitter.com/?lang=en', icon: 'socialTwitter'},
    {id: 2, link: 'https://www.instagram.com/', icon: 'socialInstagram'},
    {id: 3, link: 'https://www.snapchat.com/', icon: 'socialSnapChat'},
    {id: 4, link: 'https://www.youtube.com/', icon: 'socialYouTube'},
    {id: 5, link: 'https://www.facebook.com/', icon: 'socialFacebook'},
    {id: 6, link: 'https://www.linkedin.com', icon: 'socialLinkedIn'},
    {id: 7, link: 'https://www.tiktok.com/', icon: 'socialTikTok'}
  ],
  tags: ['Fashion', 'Beauty', 'Business', 'Cosmetics', 'Makeup']
};

const GET_USER_DATA = gql`
  query GetUser($userId: ID!) {
    user(id: $userId, idType: DATABASE_ID) {
      id
      name
      slug
      isFollowed
      totalFollowers
      avatar {
        url
      }
      following {
        id
        userId
        name
        username
        profile_picture
        followers {
          id
        }
        following {
          id
        }
      }
      followers {
        id
        userId
        name
        username
        profile_picture
        followers {
          id
        }
        following {
          id
        }
      }
      nickname
      name
      capabilities
      databaseId
      description
      username
      email
      url
      id
      timezone
      locale
      language
      currency
      date_of_birth
      show_date_of_birth
      userId
      profile_picture
      gender
      firstName
      lastName
      education
      url
      company
      job_title
      country
      noti_follows_me
      noti_list_followed
    }
  }
`;

function cutWords(word) {
  if (!word) {
    return null;
  }

  const text = word
    .split('')
    .map((oneWord, i) => (i < 57 ? oneWord : null))
    .join()
    .replace(/,/gi, '');
  return `${text}...`;
}

const SideBar = ({influencer, menuLinks, selected}) => {
  const [global, setGlobal] = useGlobal();
  const history = useHistory();
  const [isLess, setIsLess] = useState(true);
  const [edit, setEdit] = useState(false);
  const [sidebarPosition, setSidebarPosition] = useState(null);
  const {userParam} = useParams();
  const asideRef = useRef(null);
  const [user] = useUser();
  const isGuest = userParam && userParam != user?.databaseId;

  const {data, loading, error} = useQuery(GET_USER_DATA, {
    variables: {userId: isGuest ? +userParam : user?.databaseId}
  });
  const {id, databaseId, isFollowed} = data?.user || {};
  const [followUser] = useFollowUnfollowUserMutation({id, isFollowed});

  useEffect(() => {
    if (asideRef) {
      const sidebarHeight = asideRef.current.getBoundingClientRect().height;
      const bodyHeight = document.body.getBoundingClientRect().height;

      if (bodyHeight <= sidebarHeight) {
        setSidebarPosition(sidebarHeight - bodyHeight);
      }
    }
  }, []);

  const onShowFollowers = (type) => {
    setGlobal.setFollowersModal({open: true, data: data?.user?.followers, type});
  };

  const onShowFollowing = (type) => {
    setGlobal.setFollowersModal({open: true, data: data?.user?.following, type});
  };

  if (error)
    return (
      <Aside ref={asideRef} sidebarPosition={sidebarPosition}>
        <Error />
      </Aside>
    );
  if (loading)
    return (
      <Aside ref={asideRef} sidebarPosition={sidebarPosition}>
        <Loader wrapperWidth="auto" wrapperHeight="695px" />
      </Aside>
    );

  const onShowMessenger = () => {
    setGlobal.setMessenger({visible: true, minified: false, data: data?.user});
  };

  return (
    <Aside ref={asideRef} sidebarPosition={sidebarPosition}>
      <About influencer={influencer}>
        {isGuest && influencer && (
          <BookMark>
            <Icon type="bookmarkIcon" color="#000" width={16} height={14} />
          </BookMark>
        )}
        <UserAvatar
          logged={!isGuest}
          influencer={influencer}
          img={data?.user?.profile_picture || data?.user?.avatar?.url || userPlaceholder}
          setEdit={setEdit}
        />
        <Name influencer={influencer}>{data?.user?.name}</Name>
        <Profession>
          {!influencer ? (
            data?.user?.job_title
          ) : (
            <InfluencerStatus>
              <Icon type="star" width={12} height={12} />
              Top Rate Influencer
            </InfluencerStatus>
          )}
        </Profession>
        {data?.user?.country && (
          <Location>
            <Icon type="marker" />
            {data?.user?.country}
          </Location>
        )}
        {isGuest && user?.databaseId && (
          <ButtonsWrap>
            <Btn
              kind={isFollowed ? 'following' : 'follow'}
              full
              mb="10px"
              onClick={() => followUser({variables: {input: {id: databaseId}}})}
            />
            <Btn kind="bare-sm" full onClick={onShowMessenger}>
              Message
            </Btn>
            {influencer && <Button black>Hire Now</Button>}
          </ButtonsWrap>
        )}

        {!isGuest && influencer && (
          <ButtonsWrap>
            <Button onClick={setEdit} black>
              Edit Your Profile
            </Button>
          </ButtonsWrap>
        )}

        <FollowersWrap>
          {!user?.databaseId && influencer && (
            <Statistic margin>
              <Title>Posts</Title>
              <StatisticCount>{numberWithCommas(profile.totalPosts)}</StatisticCount>
            </Statistic>
          )}

          {!user?.databaseId && influencer && (
            <Statistic margin>
              <Title>Likes</Title>
              <StatisticCount>{numberWithCommas(profile.totalLikes)}</StatisticCount>
            </Statistic>
          )}

          <Statistic margin>
            <Title onClick={() => onShowFollowers('followers')}>Followers</Title>
            <StatisticCount onClick={() => onShowFollowers('followers')}>
              {numberWithCommas(data?.user?.totalFollowers)}
            </StatisticCount>
          </Statistic>

          <Statistic>
            <Title onClick={() => onShowFollowing('following')}>Following</Title>
            <StatisticCount onClick={() => onShowFollowing('following')}>
              {numberWithCommas(data?.user?.following?.length || 0)}
            </StatisticCount>
          </Statistic>

          {!user?.databaseId && influencer && (
            <Statistic>
              <Title>Follower Region</Title>
              <StatisticCount>{profile.followerRegion}</StatisticCount>
            </Statistic>
          )}
        </FollowersWrap>

        {!user?.databaseId && influencer && (
          <Countries>
            {profile.countries?.map((item, index) => (
              <span key={index}>{item}</span>
            ))}
          </Countries>
        )}

        {influencer && (
          <Socials>
            {profile.socials?.map((item, index) => (
              <a rel="noreferrer" key={index} href={item.link} target="_blank">
                <Icon width={18} height={18} type={item.icon} />
              </a>
            ))}
          </Socials>
        )}
      </About>

      <DescriptionWrap>
        <Title2>{influencer ? 'Profile Info' : 'About Me'}</Title2>
        <Description less={isLess} onClick={() => setIsLess(!isLess)}>
          {isLess ? cutWords(data?.user?.description) : data?.user?.description}
        </Description>
      </DescriptionWrap>

      {!user?.databaseId && influencer && (
        <Categories>
          <Grid sb>
            <Title>Category</Title>
            <CategoriesIcon>
              <Icon type="questionCircle" width={18} height={18} />
            </CategoriesIcon>
          </Grid>
          <Grid wrap>
            {profile.tags?.map((el, idx) => (
              <Tag key={idx}>{el}</Tag>
            ))}
          </Grid>
        </Categories>
      )}

      {influencer && !user?.databaseId && <InstagramWidget noWrapperStyles />}

      {!isGuest && <ProfileMenu items={menuLinks} selected={selected} />}
      {/* user shop no need more */}
      {/* {data?.user?.capabilities?.includes('seller') && (
        <MyShop
          name={data?.user?.name}
          image={data?.user?.avatar?.url || userPlaceholder}
          onClick={() => history.push(`/shop/${data?.user?.databaseId}/products`)}
        />
      )} */}
      <EditProfile edit={edit} setEdit={setEdit} data={data} />
    </Aside>
  );
};

SideBar.defaultProps = {
  influencer: false,
  menuLinks: [],
  selected: 'profile'
};

SideBar.propTypes = {
  menuLinks: PropTypes.arrayOf(PropTypes.any),
  selected: PropTypes.string,
  influencer: PropTypes.bool
};

export default SideBar;
