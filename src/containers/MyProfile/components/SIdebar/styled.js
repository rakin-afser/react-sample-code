import styled from 'styled-components/macro';
import media from 'constants/media';

import {secondaryTextColor, primaryColor, midCoinsColor, blue} from 'constants/colors';
import {mainFont, secondaryFont} from 'constants/fonts';

export const Aside = styled.aside`
  position: sticky;
  top: 34px;
  width: 300px;
  min-width: 300px;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  transition: top ease 0.4s;

  @media (max-width: ${media.mobileMax}) {
    position: static;
    top: auto;
  }
`;

export const About = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: ${({influencer}) => (influencer ? '23px 23px 10px 23px' : '44px 23px 10px 23px')};
  border-bottom: 1px solid #eee;
`;

export const BookMark = styled.div`
  position: absolute;
  top: 24px;
  right: 24px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 28px;
  height: 28px;
  background: #ffffff;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.13);
  border-radius: 50%;
  transition: ease 0.4s;
  cursor: pointer;

  svg {
    path {
      transition: ease 0.4s;
      fill: #000;
    }
  }

  &:hover {
    box-shadow: 0 4px 6px rgba(0, 0, 0, 0.3);

    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const AvatarWrap = styled.div`
  position: relative;

  ${(p) =>
    p.isMobile &&
    `
    margin-right: ${p.theme?.isArabic ? '0' : '16'}px;
    margin-left: ${p.theme?.isArabic ? '16' : '0'}px;
    `}
`;

export const Avatar = styled.div`
  width: ${({big}) => (big ? '150px' : '120px')};
  height: ${({big}) => (big ? '150px' : '120px')};
  border-radius: 50%;
  overflow: hidden;

  ${(p) =>
    p.isMobile &&
    `
    flex-shrink: 0;
    width: 90px;
    height: 90px;
  `}
`;

export const AvatarPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const EditAvatar = styled.i`
  position: absolute;
  bottom: ${({influencer}) => (influencer ? '3px' : '-3px')};
  right: 2px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 33px;
  height: 33px;
  background: ${({influencer}) => (influencer ? '#ED484F' : '#c3c3c3')};
  border-radius: 50%;
  border: 3px solid #ffffff;
  transition: ease 0.3s;

  &:hover {
    cursor: pointer;
    background: ${secondaryTextColor};
  }

  ${(p) =>
    p.isMobile &&
    `
    bottom: 1px;
    right: 4px;
    background: ${primaryColor};
    width: 22px;
    height: 22px;
    border: 2px solid #ffffff;

    & svg {
      width: 9px;
    }
  `}
`;

export const Name = styled.span`
  font-family: SF Pro Display, sans-serif;
  font-weight: ${({influencer}) => (influencer ? '700' : '600')};
  font-size: 20px;
  line-height: 124%;
  text-align: center;
  letter-spacing: 0.019em;
  color: #000;
  margin: 16px 0 3px 0;
`;

export const Location = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: SF Pro Display, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  margin: 0 0 30px 0;

  & svg {
    position: relative;
    left: -10px;
    width: 18px;
    height: 20px;
    margin-right: 0;
  }

  & svg path {
    fill: #464646;
  }
`;

export const FollowersWrap = styled.div`
  width: 100%;
`;

export const Title = styled.h4`
  font-family: ${mainFont};
  font-weight: 500;
  font-size: 16px;
  margin-bottom: 16px;
  transition: ease 0.4s;

  &:hover {
    cursor: pointer;
    color: ${blue};
  }
`;

export const Title2 = styled(Title)`
  margin-bottom: 10px;
  font-family: ${secondaryFont};
`;

export const Statistic = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 14px;
  line-height: 132%;
`;

export const StatisticCount = styled.span`
  font-size: 16px;
  text-align: right;
  letter-spacing: 0.019em;
  color: #000;
  margin: 0 4px 0 0;
  transition: ease 0.4s;

  &:hover {
    cursor: pointer;
    color: ${blue};
    text-decoration: underline;
  }
`;

export const Profession = styled.span`
  font-size: 14px;
  letter-spacing: -0.024em;
  color: #7a7a7a;
  margin: 0 0 17px 0;
`;

export const InfluencerStatus = styled.span`
  display: flex;
  align-items: center;

  i {
    position: relative;
    top: -1px;
  }

  svg {
    margin-right: 6px;

    path {
      fill: #f38388;
    }
  }
`;

export const DescriptionWrap = styled.div`
  padding: 26px 23px 16px;
  border-bottom: 1px solid #eee;
`;

export const Description = styled.p`
  font-family: SF Pro Display, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  cursor: pointer;

  &::after {
    content: ${({less}) => (less ? `'More'` : `'Less'`)};
    color: #ed484f;
    margin-left: 4px;
  }
`;

export const ButtonsWrap = styled.div`
  margin-bottom: 32px;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  max-width: 175px;
`;

export const Button = styled.button`
  margin-bottom: 8px;
  padding: 3px 56px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  font-size: 14px;
  border-radius: 24px;
  color: #000;
  border: 1px solid #000;
  transition: ease 0.4s;

  i {
    margin-right: 8px;
    position: relative;
    top: -1px;
    display: flex;
    align-items: center;
  }

  svg path {
    transition: ease 0.4s;
  }

  &:hover {
    background-color: #202020;
    border-color: #202020;
    color: #fff;

    svg path {
      fill: #fff;
    }
  }

  ${({pink}) =>
    pink &&
    `
    color: ${primaryColor};
    border-color: ${primaryColor};
  `};

  ${({black}) =>
    black &&
    `
    color: #fff;
    border-color: #000;
    background-color: #000;
  `};
`;

export const Countries = styled.div`
  padding-top: 3px;
  padding-bottom: 33px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  font-size: 12px;
  color: #545454;
`;

export const Socials = styled.div`
  padding-bottom: 15px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;

  a {
    svg {
      transition: ease 0.5s;

      path {
        transition: ease 0.4s;
      }
    }

    &:hover {
      svg {
        transform: scale(1.3);

        path {
          fill: ${primaryColor};
        }
      }
    }
  }
`;

export const Categories = styled.div`
  padding: 25px 23px 29px 23px;
  border-bottom: 1px solid #eee;
`;

export const CategoriesIcon = styled.div`
  svg {
    cursor: pointer;

    circle {
      transition: ease 0.4s;
    }

    &:hover {
      circle {
        fill: ${primaryColor};
      }
    }
  }
`;

export const Tag = styled.div`
  margin-bottom: 10px;
  margin-right: 11px;
  padding: 2px 10px;
  font-weight: 500;
  color: #545454;
  background: #efefef;
  border-radius: 100px;
`;
