import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  width: 576px;
  margin-left: 16px;
`;

export const Title = styled.h3`
  margin: 0;
  padding: 24px 24px 31px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: #000000;
`;
export const Item = styled.div`
  border-top: 1px solid #e4e4e4;
  padding: 16px;
  display: flex;
  align-items: center;
`;

export const Description = styled.div`
  display: grid;
  flex-grow: 1;
  grid-gap: 4px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #000;
  .action-mes {
    color: #656565;
  }
  .date {
    font-size: 12px;
    line-height: 132%;
    color: #656565;
  }
  .message {
    font-size: 14px;
    line-height: 140%;
    color: #3a3a3a;
  }
`;

export const Image = styled.img``;
