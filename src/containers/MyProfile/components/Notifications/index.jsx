import React from 'react';
import moment from 'moment';
import {useQuery} from '@apollo/client';

import FadeOnMount from 'components/Transitions';
import {AvatarStyled} from '../List/styled';
import {Wrapper, Item, Title, Description, Image} from './styled';
import {GET_NOTIFICATIONS} from 'components/NotificationBar/api/queries';
import {useUser} from 'hooks/reactiveVars';
import Loader from 'components/Loader';
import userPlaceholder from 'images/placeholders/user.jpg';

const Notifications = () => {
  const [user] = useUser();
  const {data, loading, error} = useQuery(GET_NOTIFICATIONS, {
    variables: {id: user?.databaseId}
  });

  return (
    <FadeOnMount>
      <Wrapper>
        <Title>Notifications</Title>
        {loading && <Loader wrapperWidth="100%" wrapperHeight="100px" />}
        {!loading && !data?.notifications?.length && <Item>You have no notifications yet</Item>}
        {data?.notifications?.map(({avatar, notification_type, notification_time, thumb, message}, key) => {
          return (
            <Item key={key}>
              <AvatarStyled
                icon="user"
                src={avatar || userPlaceholder}
                custom="width: 48px; height: 48px; margin-right: 12px;"
              />
              <Description>
                <div>
                  <strong>!username!</strong>
                  {` `}
                  <span className="action-mes">!{notification_type}!</span>
                </div>
                <span className="message">{message}</span>
                <span className="date">{moment(notification_time).format('Do MMM')}</span>
              </Description>
              <Image src={thumb} />
            </Item>
          );
        })}
      </Wrapper>
    </FadeOnMount>
  );
};

export default Notifications;
