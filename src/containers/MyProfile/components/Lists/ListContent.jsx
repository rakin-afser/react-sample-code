import React, {useState, useRef, useEffect} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import useOutsideClick from 'util/useOutsideClick';

import {
  Table,
  Item,
  Title,
  Pic,
  PicPlaceholder,
  Group,
  Details,
  Icon,
  Settings,
  Bold,
  EditList,
  SettingsContainer,
  Function,
  AdditionalInfo
} from './styled';
import photo1 from './img/photo1.png';
import photo2 from './img/photo2.png';
import photo3 from './img/photo3.png';
import eye from './img/eye.svg';
import Lock from 'assets/Lock';
import EditListModal from 'containers/MyProfile/components/List/components/EditListModal';
import ShareModal from 'containers/MyProfile/components/List/components/Share';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import Loader from 'components/Loader';
import {useListsQuery} from 'hooks/queries';
import {useOnScreen} from 'hooks';

const lists = [
  {
    slug: 'fancy-trends-2020',
    name: 'Fancy Trends 2020',
    images: [photo1, photo2, photo3],
    description: 'Last month I bought these Makeup Pallette.',
    views: 1111,
    followers: 967,
    type: 'public'
  },
  {
    slug: 'woman',
    name: 'Woman',
    images: [photo3, photo2, photo1],
    views: 75,
    description: 'Last month I bought these Makeup Pallette.',
    followers: 12,
    type: 'private'
  },
  {
    slug: 'make-up',
    name: 'Make Up',
    images: [photo3, photo2],
    views: 914,
    description: 'Last month I bought these Makeup Pallette.',
    followers: 147
  },
  {
    slug: 'twin-peaks',
    name: 'Twin Peaks',
    images: [photo3],
    views: 45,
    followers: 9
  },
  {
    slug: 'personal',
    name: 'Personal',
    images: [],
    views: 814,
    followers: 457,
    type: 'public'
  },
  {
    slug: 'ariana-grande',
    name: 'Ariana Grande',
    description: 'Last month I bought these Makeup Pallette.',
    images: [photo3, photo1, photo1],
    views: 1234,
    followers: 457
  },
  {
    slug: 'lipsticks-mac',
    name: 'Lipsticks - MAC',
    images: [photo2, photo3, photo1],
    views: 857,
    followers: 12
  }
];

const placeholderColors = ['#F8EBE2', '#E6EDF3', '#F0D9D9'];

function ListsContent() {
  const [, setGlobalState] = useGlobal();
  const settingRef = useRef();
  const history = useHistory();
  const {userParam} = useParams();
  const [editList, setEditList] = useState(null);
  const [viewModal, setViewModal] = useState(false);
  const [shareModal, setShareModal] = useState(false);
  const [user] = useUser();
  const isGuest = userParam && userParam != user?.databaseId;
  const first = 6;
  const {data, loading, fetchMore} = useListsQuery({
    variables: {
      first,
      where: {
        user_id: isGuest ? +userParam : user?.databaseId,
        is_private: isGuest ? false : null
      }
    },
    notifyOnNetworkStatusChange: true
  });

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first,
          where: {
            user_id: isGuest ? +userParam : user?.databaseId,
            is_private: isGuest ? false : null
          },
          after: endCursor
        }
      });
    }
  });

  const {hasNextPage, endCursor} = data?.lists?.pageInfo || {};
  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  useEffect(() => {
    if (viewModal || shareModal) {
      setEditList(null);
    }
  }, [viewModal, shareModal]);

  const showReportModal = () => {
    setGlobalState.setReportModal(true, {type: 'list', onSubmit: (data) => console.log('Report Submitted', data)});
  };

  const openListView = (listId) => {
    history.push(`/profile/${userParam ? `${userParam}/` : ''}lists/${listId}`);
  };

  useOutsideClick(settingRef, () => {
    if (editList) setEditList(null);
  });

  if (!data?.lists?.nodes?.length && loading) return <Loader wrapperWidth="auto" wrapperHeight="896px" />;
  return (
    <>
      <Table isGuest={isGuest}>
        {data?.lists?.nodes?.map((item, i) => {
          const productsData = item?.addedProducts?.nodes || [];
          return (
            <Item key={item.list_id}>
              <Group styles={{marginBottom: '8px'}} onClick={() => openListView(item.list_id)}>
                {[...Array(3).keys()].map((index) =>
                  productsData?.[index] ? (
                    <Pic key={index} src={productsData?.[index]?.image?.sourceUrl} />
                  ) : (
                    <PicPlaceholder key={index} color={placeholderColors[index]} />
                  )
                )}
              </Group>
              <Group styles={{borderTop: '1px solid #E4E4E4', paddingTop: '5px'}}>
                <Title onClick={() => openListView(item.list_id)}>{item.list_name}</Title>
                {item.is_private ? (
                  <Icon>
                    <Lock />
                  </Icon>
                ) : (
                  <Icon>
                    <img src={eye} />
                  </Icon>
                )}
                <SettingsContainer onClick={() => setEditList(item.list_id)}>
                  <Settings>
                    <div />
                  </Settings>
                  {editList === item.list_id && (
                    <EditList ref={settingRef}>
                      {isGuest ? (
                        <>
                          <Function onClick={showReportModal}>Report</Function>
                          <Function onClick={() => setShareModal(true)}>Share</Function>
                        </>
                      ) : (
                        <>
                          <Function onClick={() => setViewModal(true)}>Edit List</Function>
                          <Function onClick={() => setShareModal(true)}>Share / Invite</Function>
                          <Function color="#ed494f">Delete List</Function>
                        </>
                      )}
                    </EditList>
                  )}
                </SettingsContainer>
              </Group>
              <AdditionalInfo>
                <Details>
                  <Bold>{item.totalFollowers}</Bold> Followers
                </Details>
                <Details purchased>
                  <Bold>{productsData?.length}</Bold> Products
                </Details>
              </AdditionalInfo>
            </Item>
          );
        })}
        <EditListModal
          isOpen={viewModal}
          onClose={() => setViewModal(false)}
          onSubmit={(data) => console.log(data)}
          data={lists[0]}
        />
        <ShareModal shareModal={shareModal} setShareModal={setShareModal} />
      </Table>
      {data?.lists?.nodes?.length && loading ? <Loader wrapperWidth="auto" wrapperHeight="auto" /> : null}
      {endRef}
    </>
  );
}

export default ListsContent;
