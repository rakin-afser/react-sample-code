import React from 'react';
import ShopsToFollow from 'components/ShopsToFollow';
import FadeOnMount from 'components/Transitions';
import {Wrap, Left, Right} from './styled';
import ListContent from './ListContent';

function Lists({isGuest = false}) {
  return (
    <FadeOnMount>
      <Wrap>
        <Left>
          <ListContent />
        </Left>
        <Right>
          <ShopsToFollow />
        </Right>
      </Wrap>
    </FadeOnMount>
  );
}

export default Lists;
