import styled from 'styled-components/macro';

export const AdditionalInfo = styled.div`
  margin: 8px 0 0;
`;

export const Wrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;

export const Left = styled.div`
  max-width: 575px;
  flex-grow: 2;
`;

export const Right = styled.div`
  position: sticky;
  top: 28px;
  margin-top: 28px;
  margin-left: 16px;
  flex: 1;
`;

export const Content = styled.div`
  width: 576px;
  margin: 0 0 0 16px;
`;

export const Table = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  margin: 12px 0 0;

  ${({isGuest}) =>
    isGuest &&
    `
    @media (max-width: 1200px) {
      justify-content: space-around;
    }
  `}
  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const Item = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding: 16px;
  width: 280px;
  justify-content: flex-start;
  border-radius: 3px;
  margin-bottom: 25px;
`;

export const PicPlaceholder = styled.div`
  width: 33%;
  height: 176px;
  margin: 0 1px;
  border-radius: 8px;
  cursor: pointer;
  background-color: ${({color}) => color};
`;

export const Pic = styled.img`
  width: 33%;
  margin: 0 1px;
  min-width: 10%;
  height: 176px;
  border-radius: 8px;
  cursor: pointer;
  object-fit: cover;
  transition: all 0.5s;

  &:hover {
    width: 70%;
  }
`;

export const Create = styled.span`
  display: inline-flex;
  justify-content: center;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
`;

export const Title = styled.span`
  display: block;
  flex-grow: 1;
  font-family: Helvetica Neue;
  font-weight: 500;
  font-size: 16px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: #000000;
  cursor: pointer;
`;

export const Group = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  justify-content: space-between;

  div {
    display: flex;
  }

  ${({styles}) => (styles ? {...styles} : null)}
`;

export const PicGroup = styled.div`
  display: flex;
`;

export const Details = styled.div`
  display: inline-block;
  font-family: Helvetica Neue;
  font-size: 12px;
  font-weight: 400;
  line-height: 120%;
  color: #333333;
  margin-right: ${({purchased}) => (purchased ? '46px' : '13px')};

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-left: 13px;
    margin-right: 0;
  `}
`;

export const Bold = styled(Details)`
  font-weight: 700;
  margin: 0;
  color: #464646;
  letter-spacing: 0.06em;
`;

export const Icon = styled.div`
  cursor: pointer;

  & svg {
    path {
      stroke: #545454;
    }
  }
`;

export const Settings = styled.button`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 24px;
  background: transparent;
  border: none;
  cursor: pointer;
  margin-left: 10px;
  outline: none;

  &:before,
  &:after,
  & div {
    transition: background 0.3s;
    content: '';
    width: 4px;
    height: 4px;
    background: #7c7e82;
    border-radius: 50%;
    margin: 0 2px;
  }

  &:hover {
    &:before,
    &:after,
    & div {
      background: #000;
    }
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-right: 10px;
    margin-left: 0;
  `}
`;

export const Dot = styled.span`
  display: block;
  width: 4px;
  height: 4px;
  background: #7c7e82;
  border-radius: 50%;
`;

export const EditList = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  transform: translateY(-100%);
  display: flex;
  flex-direction: column;
  border-radius: 8px 8px 0px 8px;
  margin-left: 3px;
  background: #ffffff;
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.28);
  border-radius: 8px 8px 0px 8px;
  z-index: 99;
`;

export const Shops = styled.div`
  width: 292px;
  height: 309px;
  background: #ffffff;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin-left: 14px;
  padding: 24px 8px 24px 16px;
`;

export const SettingsContainer = styled.div`
  position: relative;
`;

export const Function = styled.div`
  cursor: pointer;
  min-width: 144px;
  white-space: nowrap;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: ${({color}) => (color ? color : '#464646')};
  padding: 8px 23px;
  border-bottom: 1px solid #fafafa;
  transition: color 0.3s;

  &:last-child {
    border-bottom: none;
  }

  &:hover {
    color: #000000;
  }
`;

export const Heading = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 18px;
  line-height: 140%;
  color: #a7a7a7;
`;

export const View = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #000000;
`;

export const Line = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: ${({flexStart}) => (flexStart ? 'flex-start' : 'space-between')};
  margin-bottom: ${({short}) => (short ? '16px' : '32px')};

  &:first-child {
    margin-bottom: 40px;
  }

  &:last-child {
    margin-bottom: 40px;
  }
`;
