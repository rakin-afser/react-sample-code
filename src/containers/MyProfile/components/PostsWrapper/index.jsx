import React, {useState} from 'react';
import {useParams} from 'react-router-dom';
import _ from 'lodash';

import {useUser} from 'hooks/reactiveVars';
import ShopsToFollow from 'components/ShopsToFollow';
import FadeOnMount from 'components/Transitions';
import PostsGridView from '../PostsGridView';
import Posts from '../Posts';
import {ReactComponent as IconViewGrid} from './img/iconViewGrid.svg';
import {ReactComponent as IconViewFeed} from './img/iconViewFeed.svg';
import {Controls, FeedView, Left, Right, IconViewGridWrap, IconViewFeedWrap} from './styled';
import Loader from 'components/Loader';
import {useQuery} from '@apollo/client';
import {FEED_POSTS} from 'queries';
import {useOnScreen} from 'hooks';

const PostsWrapper = () => {
  const [feedView, setFeedView] = useState(false);
  const {userParam} = useParams();
  const [user] = useUser();
  const {data, loading, fetchMore} = useQuery(FEED_POSTS, {
    variables: {
      first: 9,
      where: {
        author: +userParam || user?.databaseId
      }
    },
    notifyOnNetworkStatusChange: true
  });

  const {endCursor, hasNextPage} = data?.posts?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.posts?.nodes || [];
          const newNodes = fetchMoreResult?.posts?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            posts: {
              ...fetchMoreResult?.posts,
              nodes
            }
          };
        },
        variables: {
          first: 9,
          where: {
            author: +userParam || user?.databaseId
          },
          after: endCursor
        }
      });
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;
  const {posts} = data || {};

  return (
    <>
      <Controls feedView={feedView}>
        {feedView ? (
          <IconViewGridWrap feedView={feedView} onClick={() => setFeedView(false)}>
            <IconViewGrid />
          </IconViewGridWrap>
        ) : (
          <IconViewFeedWrap feedView={feedView} onClick={() => setFeedView(true)}>
            <IconViewFeed />
          </IconViewFeedWrap>
        )}
      </Controls>
      {feedView ? (
        <FeedView>
          <Left>
            <Posts data={posts?.nodes} loading={!posts?.nodes?.length && loading} />
            {loading && posts?.nodes?.length ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
            {endRef}
          </Left>
          <Right>
            <FadeOnMount>
              <ShopsToFollow />
            </FadeOnMount>
          </Right>
        </FeedView>
      ) : (
        <>
          <FadeOnMount>
            {!posts?.nodes?.length && loading ? (
              <Loader wrapperWidth="100%" wrapperHeight="400px" />
            ) : (
              <PostsGridView data={posts?.nodes} />
            )}
          </FadeOnMount>
          {loading && posts?.nodes?.length ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
          {endRef}
        </>
      )}
    </>
  );
};

export default PostsWrapper;
