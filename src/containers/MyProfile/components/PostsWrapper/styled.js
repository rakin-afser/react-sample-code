import styled from 'styled-components';
import {secondaryColor} from 'constants/colors';

export const Controls = styled.div`
  position: relative;
  right: 38px;
  top: 13px;
  display: flex;
  justify-content: flex-end;
  cursor: pointer;
`;

export const IconViewGridWrap = styled.button`
  svg {
    rect {
      fill: #000;
      transition: ease 0.4s;
    }

    &:hover {
      rect {
        fill: ${secondaryColor};
      }
    }

    ${({feedView}) =>
      !feedView &&
      `
      rect {
      fill: #000;
      }
    `}
  }
`;

export const IconViewFeedWrap = styled.button`
  svg {
    rect {
      transition: ease 0.4s;
      stroke: #000;
    }
  }

  &:hover {
    svg {
      rect {
        stroke: ${({feedView}) => (feedView ? '#000' : secondaryColor)};
      }
    }
  }

  ${({feedView}) =>
    feedView &&
    `
    rect {
      fill: #000;
      stroke: #000;
    }
  `}
`;

export const FeedView = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;

export const Left = styled.div`
  max-width: 575px;
  flex-grow: 1;
`;

export const Right = styled.div`
  position: sticky;
  top: 24px;
  margin-top: 24px;
  margin-left: 16px;
`;
