import styled from 'styled-components';

export const Nav = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: #ffffff;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
`;

export const Tab = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  height: 36px;
  background: transparent;
  border: none;
  cursor: pointer;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 16px;
  line-height: 20px;
  color: #7a7a7a;
  box-sizing: border-box;
  outline: none;
  padding: none;
  ${({active}) =>
    active
      ? {
          color: '#000000',
          borderBottom: '2px solid #000000'
        }
      : null}
  &:first-child {
    margin: 4px 124px 0 auto;
  }
  &:last-child {
    margin: 4px auto 0 124px;
  }
  &:hover {
    color: #000000;
    border-bottom: 2px solid #000000;
  }
`;
