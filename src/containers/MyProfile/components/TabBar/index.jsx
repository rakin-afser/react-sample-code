import React from 'react';
import {Nav, Tab} from './styled';

const TabBar = ({tabs = [], activeTabIndex, selectTab = (f) => f}) => (
  <Nav>
    {tabs.map((tab, i) => (
      <Tab active={activeTabIndex === i} key={i} onClick={() => setActiveTabIndex(i)}>
        {tab}
      </Tab>
    ))}
  </Nav>
);

export default TabBar;
