import styled from 'styled-components/macro';
import {secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding-top: 34px;
  padding-left: 56px;
  padding-bottom: 20px;
  border: 1px solid #e4e4e4;
  border-radius: 4px;
`;

export const Inner = styled.div`
  max-width: 600px;
`;

export const BtnBack = styled.button`
  margin-bottom: 15px;
  padding: 6px 13px 6px 9px;
  display: flex;
  align-items: center;
  background: #efefef;
  border-radius: 20px;
  font-size: 11px;
  transition: ease 0.8s;

  i {
    margin-right: 3px;
    position: relative;
    top: -1px;
    transform: rotate(-180deg);

    svg {
      path {
        transition: ease 0.8s;
      }
    }
  }

  &:hover {
    background: #6b6b6b;
    color: #fff;

    i {
      svg {
        path {
          fill: #fff;
        }
      }
    }
  }
`;

export const Title = styled.h2`
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 24px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const Form = styled.form``;

export const Row = styled.div`
  margin-bottom: 33px;
`;

export const Tabs = styled.div`
  display: flex;
  align-items: center;
`;

export const Tab = styled.div`
  margin-right: 25px;
  position: relative;
  font-size: 12px;
  line-height: 140%;
  text-align: center;
  color: ${({active}) => (active ? '#000' : '#7a7a7a')};
  cursor: pointer;
  transition: ease 0.4s;
  font-weight: ${({active}) => (active ? '700' : '400')};

  &::after {
    content: '';
    position: relative;
    display: block;
    height: 2px;
    width: 100%;
    border-radius: 10px;
    transition: ease 0.4s;
    background-color: ${({active}) => (active ? '#000' : 'transparent')};
  }
`;

export const Hr = styled.hr`
  margin-bottom: 21px;
  border: none;
  width: 100%;
  height: ${({black}) => (black ? '2px' : '1px')};
  background: ${({black}) => (black ? '#000000' : '#e4e4e4')};
`;

export const SecondTitle = styled.h3`
  margin-bottom: 20px;
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 20px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const FieldName = styled.h4`
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  color: #000000;
`;

export const ProposalsWrap = styled.div`
  display: flex;
`;

export const Proposal = styled.div`
  margin-top: auto;
  margin-right: 13px;

  &:last-child {
    margin-right: 0;
  }
`;

export const Text = styled.p`
  margin: 0;
  padding: 0;
  font-size: 14px;
  line-height: 140%;
  text-align: center;
  color: #7a7a7a;
`;

export const SubmitBtn = styled.button`
  margin: 0 auto;
  padding: 6px 28px;
  background: #000000;
  border-radius: 24px;
  font-weight: 500;
  font-size: 14px;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  color: #ffffff;
  transition: ease 0.6s;

  &:hover {
    background-color: ${primaryColor};
  }
`;
