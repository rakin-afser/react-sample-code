import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import promoteLists from 'containers/MyProfile/components/Promote/staticData';
import {
  Header,
  Title,
  TotalProducts,
  ImagesHolder,
  Card,
  CloseCardBtn,
  ShowMoreBtn
} from 'containers/MyProfile/components/Campaign/components/ProductsBePromoted/styled';
import Icons from 'components/Icon';
import Expanded from 'components/Expanded';

const ProductsBePromoted = ({selectedItems}) => {
  const [data, setData] = useState([]);

  useEffect(() => {
    if (selectedItems.length) {
      const itemsFromData = promoteLists.find((el) => el.title === selectedItems[0].listId).items;
      setData(itemsFromData.filter((item) => selectedItems.some((item2) => item2.id === item.id)));
    }
  }, [selectedItems]);

  const onCardClose = (e, id) => {
    e.preventDefault();
    setData(data.filter((el) => el.id !== id));
  };

  const ShowMore = () => {
    return (
      <ShowMoreBtn>
        Show More
        <Icons type="arrow" width={10} height={10} color="#000" />
      </ShowMoreBtn>
    );
  };

  const ShowLess = () => {
    return (
      <ShowMoreBtn less>
        Show Less
        <Icons type="arrow" width={10} height={10} color="#000" />
      </ShowMoreBtn>
    );
  };

  const renderCards = (start, end) => {
    return data.map((el, idx) => {
      if (idx >= start && idx < end + 1) {
        return (
          <Card key={idx}>
            <CloseCardBtn onClick={(e) => onCardClose(e, el.id)}>
              <Icons type="cross" fill="#000" />
            </CloseCardBtn>
            <img src={el.img} alt={el.name} />
          </Card>
        );
      }

      return null;
    });
  };

  return (
    <>
      <Header>
        <Title>Products to be promoted</Title>
        <TotalProducts>{data.length} products</TotalProducts>
      </Header>
      <ImagesHolder>
        {renderCards(0, 12)}

        {data.length > 12 && (
          <Expanded
            stylesForHidden={{width: '100%', display: 'flex'}}
            style={{width: '100%'}}
            contentVisible=""
            hideBtn={<ShowLess />}
            showBtn={<ShowMore />}
          >
            {renderCards(12, data.length)}
          </Expanded>
        )}
      </ImagesHolder>
    </>
  );
};

ProductsBePromoted.defaultProps = {
  selectedItems: []
};

ProductsBePromoted.propTypes = {
  selectedItems: PropTypes.arrayOf(PropTypes.shape())
};

export default ProductsBePromoted;
