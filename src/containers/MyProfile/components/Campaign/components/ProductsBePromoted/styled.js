import styled from 'styled-components';
import {secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const Header = styled.header`
  margin-bottom: 23px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Title = styled.h3`
  font-weight: bold;
  font-size: 20px;
  line-height: 24px;
  color: #000000;
`;

export const TotalProducts = styled.span`
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #b3b3b3;
`;

export const ImagesHolder = styled.div`
  margin-left: -5px;
  margin-right: -10px;
  display: flex;
  flex-wrap: wrap;
`;

export const Card = styled.div`
  position: relative;
  margin: 5px;
  width: 92px;
  height: 92px;
  background: #c4c4c4;
  border-radius: 4px;
  overflow: hidden;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;

export const CloseCardBtn = styled.button`
  position: absolute;
  right: 6px;
  top: 6px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 18px;
  height: 18px;
  background-color: #e7e7e7;
  border-radius: 50%;
  transition: ease 0.6s;

  i {
    svg {
      path {
        transition: ease 0.6s;
      }
    }
  }

  &:hover {
    background-color: #3c3c3c;

    i {
      svg {
        path {
          fill: #fff;
        }
      }
    }
  }
`;

export const ShowMoreBtn = styled.div`
  margin-top: 35px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: ease 0.4s;
  font-family: ${secondaryFont};
  font-weight: 500;
  font-size: 14px;
  color: #000000;

  i {
    margin-left: 6px;
    transform: ${({less}) => (less ? 'rotate(-90deg)' : 'rotate(90deg)')};

    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    color: ${primaryColor};

    i {
      svg {
        path {
          fill: ${primaryColor};
        }
      }
    }
  }
`;
