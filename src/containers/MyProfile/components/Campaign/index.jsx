import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useForm, Controller} from 'react-hook-form';
import {DatePicker} from 'antd';
import {
  Wrapper,
  Inner,
  BtnBack,
  Title,
  Form,
  Row,
  Tabs,
  Tab,
  Hr,
  SecondTitle,
  FieldName,
  ProposalsWrap,
  Proposal,
  Text,
  SubmitBtn
} from 'containers/MyProfile/components/Campaign/styled';
import Input from 'components/Input';
import ProductsBePromoted from 'containers/MyProfile/components/Campaign/components/ProductsBePromoted';
import FadeOnMount from 'components/Transitions';
import Textarea from 'components/Textarea';
import Icons from 'components/Icon';
import ActionAlert from 'components/Modals/ActionAlert';

const Campaign = ({setShowCampaign, selectedItems}) => {
  const {RangePicker} = DatePicker;
  const {register, handleSubmit, control} = useForm();
  const [activeTab, setActiveTab] = useState('product');
  const [modal, setModal] = useState(false);

  const tabItems = [
    {id: 'product', title: 'Per Product'},
    {id: 'fixed', title: 'Fixed Price'}
  ];

  const onTabClick = (id) => {
    setActiveTab(id);
  };

  const onSubmit = (data) => {
    setModal(true);
    return data;
  };

  const onModalSubmit = () => {
    setShowCampaign(false);
    setModal(false);
  };

  return (
    <>
      <FadeOnMount>
        <Wrapper>
          <Inner>
            <BtnBack onClick={() => setShowCampaign(false)}>
              <Icons type="arrow" color="#545454" width={12} height={12} />
              Back
            </BtnBack>
            <Title>Request a Campaign</Title>
            <Hr black />

            <Form onSubmit={handleSubmit(onSubmit)}>
              <Row>
                <FieldName>Title</FieldName>
                <Input placeholder="Put a contract name " name="contactName" ref={register} />
              </Row>
              <Row>
                <FieldName>Description</FieldName>
                <Textarea placeholder="Your text" rows={4} name="description" ref={register} />
              </Row>
              <Hr />
              <SecondTitle>Campaign Date</SecondTitle>
              <Row>
                <Controller
                  render={(props) => (
                    <RangePicker
                      {...props}
                      onChange={(e) => {
                        // eslint-disable-next-line react/prop-types
                        props.onChange(e);
                      }}
                    />
                  )}
                  control={control}
                  name="startDate"
                  rules={{required: false}}
                  defaultValue=""
                />
              </Row>
              <Hr />
              <Row>
                <SecondTitle>Proposal</SecondTitle>
                <Tabs>
                  {tabItems.map((el) => (
                    <Tab active={el.id === activeTab} onClick={() => onTabClick(el.id)} key={el.id}>
                      {el.title}
                    </Tab>
                  ))}
                </Tabs>
              </Row>
              <Row>
                <ProposalsWrap>
                  <Proposal>
                    <FieldName>Expected Orders (#)</FieldName>
                    <Input placeholder="10" name="expectedOrders" ref={register} />
                  </Proposal>
                  <Proposal>
                    <FieldName>Days</FieldName>
                    <Input placeholder="100" name="days" ref={register} />
                  </Proposal>
                  <Proposal>
                    <FieldName>
                      Commission
                      <br />
                      (Per item)
                    </FieldName>
                    <Input placeholder="5%" name="commission" ref={register} />
                  </Proposal>
                  <Proposal>
                    <FieldName>
                      Coupon Discount
                      <br />
                      (For customers)
                    </FieldName>
                    <Input placeholder="5%" name="couponDiscount" ref={register} />
                  </Proposal>
                </ProposalsWrap>
              </Row>
              <Hr />
              <Row>
                <ProductsBePromoted selectedItems={selectedItems} />
              </Row>
              <Row>
                <Text>You will receive an influencer code when the seller accepts your campaign request.</Text>
              </Row>
              <Row>
                <SubmitBtn type="submit">Send Request </SubmitBtn>
              </Row>
            </Form>
          </Inner>
        </Wrapper>
      </FadeOnMount>
      <ActionAlert
        content="Your request has been sent successfully."
        headerTitle="Request sent"
        okBtnName="Back to Promote List"
        onClose={() => setModal(false)}
        open={modal}
        onSubmit={onModalSubmit}
        cancelBtnName="Close"
      />
    </>
  );
};

Campaign.defaultProps = {
  selectedItems: []
};

Campaign.propTypes = {
  setShowCampaign: PropTypes.func.isRequired,
  selectedItems: PropTypes.arrayOf(PropTypes.shape())
};

export default Campaign;
