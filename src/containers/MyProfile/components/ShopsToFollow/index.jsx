import React from 'react';
import Icon from 'components/Icon';
import { Shops, Line, Heading, View, RightSidebar } from './styled';
import avatar from 'images/avatar.png';
import StarRating from 'components/StarRating';
import Button from 'components/Buttons';

const data = [
  {avatar: avatar, title: 'Luis Vuitton'},
  {avatar: avatar, title: 'Luis Vuitton'},
  {avatar: avatar, title: 'Luis Vuitton'}
];

const ShopsToFollow = () => {
  return (
    <RightSidebar>
      <Shops>
        <Line>
          <Heading>Shops to follow</Heading>
          <View>
            View All <Icon type="arrow" width={15} height={12} color="#666666"/>
          </View>
        </Line>
        {data.map((el, index) => (
          <Line key={index}>
            <div style={{display: 'flex'}}>
              <img src={el.avatar} alt="avatar"/>
              <div>
                <h3>{el.title}</h3>
                <div style={{display: 'flex'}}>
                  <StarRating/>
                  <span>(335)</span>
                </div>
              </div>
            </div>
            <Button type="follow"/>
          </Line>
        ))}
      </Shops>
    </RightSidebar>
  )
}

export default ShopsToFollow;
