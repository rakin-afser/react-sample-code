import styled from 'styled-components';
import media from 'constants/media';

export const RightSidebar = styled.div`
  position: sticky;
  top: 34px;
  width: 292px;

  @media (max-width: ${media.mobileMax}) {
    position: static;
    top: auto;
  }
`;

export const Shops = styled.div`
  width: 100%;
  max-height: 309px;
  background: #ffffff;
  box-shadow: 0 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  padding: 0 0 24px 0;
`;

export const Line = styled.div`
  position: relative;
  padding: 16px;
  display: flex;
  align-items: center;
  justify-content: ${({flexStart}) => (flexStart ? 'flex-start' : 'space-between')};
  margin-bottom: 0 !important;

  span {
    font-weight: normal;
    font-size: 12px;
    color: #464646;
    margin-left: 8px;
  }

  img {
    width: 40px;
    height: 40px;
    margin-right: 16px;
  }

  h3 {
    font-weight: bold;
    font-size: 14px;
    margin-bottom: 4px;
  }

  button {
    width: 76px;
    height: 22px;
    font-size: 12px;
  }

  &:first-child {
    margin-bottom: 40px;
  }

  &:last-child {
    margin-bottom: 40px;
  }
`;

export const Heading = styled.h2`
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 18px;
  line-height: 140%;
  color: #a7a7a7;
  margin: 0;
`;

export const View = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #000000;

  i {
    margin-left: 5px;
  }
`;
