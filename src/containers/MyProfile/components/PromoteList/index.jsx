import React, {useState} from 'react';
import {number, shape, string, bool, func, arrayOf} from 'prop-types';
import {Checkbox} from '@material-ui/core';
import PromoteIcon from 'assets/PromoteIcon';
import {lightGreen, transparentTextColor as gray300} from 'constants/colors';
import {ReactComponent as ShowAllIcon} from '../../icons/showAll.svg';
import {ReactComponent as ShowLessIcon} from '../../icons/showLess.svg';
import {
  Container,
  Top,
  Left,
  Right,
  Title,
  ActiveItems,
  ActiveItemsText,
  SelectAll,
  SelectAllLabel,
  SendRequestButton,
  PromoteListItems,
  PaginationButton,
  PaginationButtonText
} from './styled';
import PromoteListItem from '../PromoteListItem';

const DISPLAYED_ITEMS = 3;

const PromoteList = ({title, items, setShowCampaign, setSelectedItems}) => {
  const [selectedLists, setSelectedLists] = useState([]);
  const [renderedItems, setRenderedItems] = useState(items.slice(0, DISPLAYED_ITEMS));
  const [isAllItemsSelected, setIsAllItemsSelected] = useState(false);

  const itemsLength = items.length;
  const renderedItemsLength = renderedItems.length;

  const activeItemsNumber = items.reduce((count, {isActive}) => (isActive ? count + 1 : count), 0);
  const hasPagination = itemsLength > DISPLAYED_ITEMS;

  const onSelectAllItems = (event) => setIsAllItemsSelected(event.target.checked);

  const onPaginationClick = () => {
    if (renderedItemsLength > DISPLAYED_ITEMS) {
      setRenderedItems(items.slice(0, DISPLAYED_ITEMS));
    } else {
      setRenderedItems(items);
    }
  };

  const onCardSelect = (id, listId) => {
    setSelectedLists([
      ...selectedLists,
      {
        id,
        listId
      }
    ]);
  };

  const onCardDeselect = (id, listId) => {
    setSelectedLists(selectedLists.filter((el) => el.id !== id));
  };

  const onSendRequest = () => {
    setShowCampaign(true);
    setSelectedItems(selectedLists);
  };

  // const onCardSelectAll = (id, listId) => {
  //   const allItems = [...renderedItems.map((el) => el.id)];
  //   setSelectedLists([
  //     ...selectedLists,
  //     {
  //       id,
  //       listId
  //     }
  //   ]);
  // };

  const paginationButton = (
    <PaginationButton onClick={onPaginationClick}>
      {itemsLength > renderedItemsLength ? (
        <>
          <PaginationButtonText>
            Show All <span>{itemsLength}</span>
          </PaginationButtonText>
          <ShowAllIcon />
        </>
      ) : (
        <>
          <PaginationButtonText>Show Less</PaginationButtonText>
          <ShowLessIcon />
        </>
      )}
    </PaginationButton>
  );

  return (
    <Container>
      <Top>
        <Left>
          <Title>{title}</Title>
          <ActiveItems>
            <PromoteIcon fillColor={activeItemsNumber ? lightGreen : gray300} />
            <ActiveItemsText>
              {activeItemsNumber ? (
                <>
                  <span>Active</span> {activeItemsNumber} Product
                </>
              ) : (
                <span>Inactive</span>
              )}
            </ActiveItemsText>
          </ActiveItems>
        </Left>
        <Right>
          <SelectAll>
            <Checkbox checked={isAllItemsSelected} onChange={onSelectAllItems} style={{color: '#707070'}} />
            <SelectAllLabel>Select All</SelectAllLabel>
          </SelectAll>
          <SendRequestButton onClick={onSendRequest}>Send Request</SendRequestButton>
        </Right>
      </Top>
      <PromoteListItems>
        {renderedItems.map((item) => (
          <PromoteListItem
            selected={selectedLists.some((el) => el.id === item.id)}
            onCardSelect={onCardSelect}
            onCardDeselect={onCardDeselect}
            onSelectAll={isAllItemsSelected}
            promoteListId={title}
            key={item.id}
            {...item}
          />
        ))}
      </PromoteListItems>
      {hasPagination && paginationButton}
    </Container>
  );
};

PromoteList.propTypes = {
  title: string,
  items: arrayOf(
    shape({
      id: number,
      price: number,
      name: string,
      isLiked: bool,
      isActive: bool,
      img: string
    })
  ),
  setShowCampaign: func.isRequired,
  setSelectedItems: func.isRequired
};

export default PromoteList;
