import styled from 'styled-components';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  secondaryTextColor as gray800,
  bookmarkFillColor as gray500,
  headerShadowColor as gray400,
  transparentTextColor as gray300,
  primaryColor as primary
} from 'constants/colors';

export const Container = styled.div`
  border: 1px solid ${gray400};
  padding: 30px 55px 20px;
  width: 100%;
  margin-bottom: 25px;
`;

export const Top = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  position: relative;
  padding-bottom: 7px;
  justify-content: space-between;

  &:after {
    position: absolute;
    content: '';
    left: 0;
    bottom: 0;
    width: 100%;
    height: 2px;
    background-color: ${primary};
  }
`;

export const Left = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Right = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const Title = styled.h2`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 28px;
  letter-spacing: 0.013em;
  color: ${gray800};
  margin-bottom: 0;
  margin-right: 22px;
`;

export const ActiveItems = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const ActiveItemsText = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  margin-left: 6px;
  color: ${gray300};

  span {
    color: ${black};
  }
`;

export const SelectAll = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const SelectAllLabel = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${gray800};
`;

export const SendRequestButton = styled.button`
  background-color: ${primary};
  color: ${white};
  border-radius: 24px;
  padding: 5px 25px;
  margin-left: 35px;
  align-self: center;
`;

export const PromoteListItems = styled.div`
  padding-top: 30px;
  display: grid;
  grid-template-columns: repeat(3, 228px);
  justify-content: space-between;
  grid-row-gap: 45px;
  margin-bottom: 25px;
`;

export const PaginationButton = styled.button`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 0 auto;

  svg {
    margin-right: 8px;
  }
`;

export const PaginationButtonText = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${gray500};
  margin-right: 8px;

  span {
    color: ${black};
  }
`;
