import React, {useRef, useState} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import {useQuery} from '@apollo/client';
import {useTranslation} from 'react-i18next';

import OwnerViewCard from 'components/Cards/OwnerViewCard';
import useOutsideClick from 'util/useOutsideClick';
import {
  Wrap,
  BackLink,
  Title,
  Text,
  Avatar,
  Follow,
  Number,
  Views,
  Line,
  FollowBtn,
  GridContent,
  ButtonMore,
  EditList,
  Function,
  Private
} from './styled';
import EditListModal from './components/EditListModal';
import ShareModal from './components/Share';
import Lock from 'assets/Lock';
import {useUser} from 'hooks/reactiveVars';
import {Settings, SettingsContainer} from '../Lists/styled';
import {ReactComponent as ArrowLeftIcon} from '../../icons/arrowLeft.svg';
import avatar1 from '../../img/avatar5.png';
import avatar2 from '../../img/avatar7.png';
import FadeOnMount from 'components/Transitions';
import useGlobal from 'store';
import {GET_LIST, GET_LISTS} from 'containers/MyProfile/api/queries';
import productPlaceholder from 'images/placeholders/product.jpg';
import {getDiscountPercent} from 'util/heplers';

function List({isGuest}) {
  const [, setGlobalState] = useGlobal();
  const {t} = useTranslation();
  const history = useHistory();
  const [user] = useUser();
  const settingRef = useRef();
  const [settingPopUp, setSettingPopUp] = useState(false);
  const [viewModal, setViewModal] = useState(false);
  const [shareModal, setShareModal] = useState(false);
  const {userParam, slug} = useParams();
  const isWishlist = slug?.toLowerCase() === 'wishlist';
  const currentQuery = isWishlist ? GET_LISTS : GET_LIST;
  const variables = isWishlist ? {where: {user_id: user?.databaseId, list_name: slug}} : {id: +slug};
  const {data, loading, error} = useQuery(currentQuery, {
    variables
  });

  const listData = (isWishlist ? data?.lists?.nodes?.[0] : data?.getList) || {};
  const {user_id: userId, is_private: isPrivate} = listData || {};
  const productsData = listData?.addedProducts?.nodes || [];

  const openModal = () => {
    setViewModal(true);
    setSettingPopUp(false);
  };

  const shareProduct = () => {
    setShareModal(true);
  };

  const showReportModal = () => {
    setGlobalState.setReportModal(true, {type: 'list', onSubmit: (data) => console.log('Report Submitted', data)});
  };

  const dotList = isGuest ? (
    <EditList>
      <Function onClick={showReportModal}>Report</Function>
      <Function onClick={shareProduct}>Share</Function>
    </EditList>
  ) : (
    <EditList>
      <Function onClick={openModal}>Edit List</Function>
      <Function onClick={shareProduct}>Share</Function>
      <Function color="#b91319">Delete List</Function>
    </EditList>
  );

  useOutsideClick(settingRef, () => {
    setSettingPopUp(false);
  });

  const followersList = () => {
    setGlobalState.setFollowersModal(true);
  };

  const actions = {
    owner: [
      {title: 'Share', handleClick: shareProduct},
      {
        title: 'Delete Product',
        handleClick: () => {}
      }
    ],
    guest: [
      {title: 'Share', handleClick: shareProduct},
      {
        title: 'Report',
        handleClick: showReportModal
      }
    ]
  };

  const goBack = () => {
    const ownerId = userParam ? `${userParam}/` : '';
    history.push(`/profile/${ownerId}lists`);
  };

  const params = user?.databaseId === userId || isPrivate ? '' : `?listId=${slug}`;

  return (
    <FadeOnMount>
      <Wrap>
        <BackLink onClick={goBack}>
          <ArrowLeftIcon /> Back to Lists
        </BackLink>
        <Line short>
          <Title>
            {listData.list_name}
            {listData?.is_private && (
              <Private>
                <Lock />
                {t('general.private')}
              </Private>
            )}
          </Title>
          <SettingsContainer onClick={() => setSettingPopUp(true)} ref={settingRef}>
            <Settings>
              <div />
            </Settings>
            {settingPopUp && dotList}
          </SettingsContainer>
        </Line>
        <Text>{listData.list_description} </Text>
        {listData?.is_private ? null : (
          <Line flexStart>
            <Avatar src={avatar2} />
            <Avatar last src={avatar1} />
            <Follow>Scarlett Robertson</Follow>
            <Follow thin>and</Follow>
            <Number onClick={followersList} style={{fontSize: 12}}>
              7 more
            </Number>
            <Follow thin>follow this list.</Follow>
            <Views thin>
              <Number style={{color: '#000'}}>{productsData?.length}</Number>
              Products
            </Views>
            <FollowBtn>
              + <span>{isGuest ? 'Follow' : 'Invite'}</span>
            </FollowBtn>
          </Line>
        )}

        <GridContent>
          {productsData.map((item) => (
            <OwnerViewCard
              // productCount={}
              key={item?.id}
              databaseId={item.databaseId}
              onClick={() => history.push(`/product/${item?.slug}${params}`)}
              onSale={item?.onSale}
              sale={getDiscountPercent(item.regularPrice, item.salePrice)}
              productImage={item.image?.sourceUrl || productPlaceholder}
              price={item.onSale ? item.salePrice : item.price}
              oldPrice={item.onSale ? item.regularPrice : null}
              title={item.name}
              isLiked={item.isLiked}
              actions={isGuest ? actions.guest : actions.owner}
            />
          ))}
        </GridContent>
        {/* <ButtonMore>Load More</ButtonMore> */}
        <EditListModal
          isOpen={viewModal}
          onClose={() => setViewModal(false)}
          onSubmit={(data) => console.log(data)}
          data={listData}
        />
        <ShareModal shareModal={shareModal} setShareModal={setShareModal} />
      </Wrap>
    </FadeOnMount>
  );
}

export default List;
