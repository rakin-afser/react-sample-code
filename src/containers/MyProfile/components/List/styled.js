import styled from 'styled-components/macro';
import {Modal, Input, Button, Avatar as AvatarAntd, Carousel} from 'antd';
import {blue} from 'constants/colors';

export const Wrap = styled.div`
  width: 100%;
  margin: 0 0 0 36px;
  max-width: calc(100% - 50px);

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const ModaTitle = styled.h3`
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
  margin: 0;
`;

export const BackLink = styled.div`
  display: flex;
  align-items: center;
  background: #efefef;
  border-radius: 20px;
  width: 117px;
  height: 27px;
  font-family: 'Helvetica', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  color: #545454;
  cursor: pointer;

  svg {
    margin-left: 3px;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    svg {
      margin-left: 0;
      margin-right: 3px;
      transform: scale(-1,1);
    }
  `}
`;

export const Link = styled.a`
  font-family: 'Helvetica', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 120%;
  letter-spacing: 0.06em;
`;

export const Title = styled.span`
  display: flex;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
  line-height: 120%;
  letter-spacing: 0.011em;
  color: #000000;
`;

export const Heading = styled.span`
  display: block;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 18px;
  line-height: 140%;
  color: #7a7a7a;
  padding-left: 31px;
`;

export const Text = styled.span`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 170%;
  color: #545454;
  display: grid;
  grid-template-columns: 2fr 1fr;
`;

export const Line = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  justify-content: ${({flexStart}) => (flexStart ? 'flex-start' : 'space-between')};
  margin-bottom: 24px;
  ${({short}) =>
    short
      ? `margin: 16px 0 21px;`
      : `padding-bottom: 25px;
        margin: 17px 0 21px;
        border-bottom: 1px solid #d6d6d6;`}
`;

export const Avatar = styled.img`
  width: 25px;
  height: 25px;
  background: #fff;
  padding: 2px;
  margin-left: -8px;
  border-radius: 100%;
  margin-right: ${({last}) => (last ? '8' : '0')}px;

  &:first-child {
    margin-left: 0;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-left: 0;
    margin-right: -8px;
    &:first-child {
      margin-right: 0;
    }
  `}
`;

export const Follow = styled.div`
  display: inline-block;
  font-family: Helvetica Neue, sans-serif;
  font-style: normal;
  font-weight: ${({thin}) => (thin ? '300' : '500')};
  font-size: 12px;
  line-height: 120%;
  color: ${({thin}) => (thin ? '#545454' : '#000')};
  letter-spacing: 0.06em;
  margin-right: 2px;
`;

export const Number = styled.a`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #4a90e2;
  margin-right: 5px;
`;

export const Views = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: ${({thin}) => (thin ? '300' : '500')};
  font-size: 14px;
  line-height: 17px;
  color: ${({thin}) => (thin ? '#545454' : '#000')};
  margin-left: ${({shortPadding}) => (shortPadding ? '24px' : '37px')};

  ${({theme: {isArabic}, shortPadding}) =>
    isArabic &&
    `
    margin-left: 0;
    margin-right: ${shortPadding ? '24px' : '37px'};
  `}
`;

export const Pic = styled.img`
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
`;

export const Sale = styled.div`
  position: absolute;
  width: 45px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 12px;
  right: 7px;
  background: #f0646a;
  border-radius: 2px;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 12px;
  line-height: 15px;
  color: #ffffff;
`;

export const Product = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 276px;
  height: 373px;
  background: #ffffff;
  border-radius: 4px;
  border: ${({empty}) => (empty ? '1px solid #E4E4E4;' : 'none')};
  margin-bottom: 24px;
`;

export const Information = styled.div`
  width: 100%;
  height: 97px;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  padding: 12px 12px 23px;
  border-bottom-left-radius: 4px;
  border-bottom-right-radius: 4px;
`;

export const Price = styled.span`
  display: inline-block;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: ${({little}) => (little ? '12px' : '24px')};
  line-height: 132%;
  letter-spacing: -0.024em;
  color: ${({red}) => (red ? '#ED494F' : '#000000')};
  padding-right: ${({red}) => (red ? '5px' : '0')};
`;

export const OldPrice = styled(Price)`
  font-size: 12px;
  color: ${({red}) => (red ? '#ED494F' : ' #999999')};
  padding-right: 0;
  padding-left: ${({red}) => (red ? '0' : '5px')};
  text-decoration: ${({red}) => (red ? 'none' : 'line-through')};
`;

export const Name = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const EditList = styled.div`
  position: absolute;
  width: 160px;
  top: -10px;
  transform: translateX(-100%);
  left: 0;
  background: #ffffff;
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.28);
  border-radius: 8px;
  z-index: 1000;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    left: -10px;
  `}
`;

export const MoveList = styled(EditList)`
  width: 343px;
  height: auto;
`;

export const Function = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  border-bottom: ${({boldBorder}) => (boldBorder ? '1px solid #C3C3C3' : '1px solid #FAFAFA')};
  background: transparent;
  cursor: pointer;
  color: ${({color}) => color || '#464646'};
  padding: 11px 16px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  letter-spacing: 0.2px;

  &:hover {
    color: #000000;
  }

  &:last-child {
    border-bottom: none;
  }

  &:first-child {
    &::after {
      content: '';
      position: absolute;
      right: -22px;
      border: 15px solid transparent;
      border-left: 15px solid #fff;
    }
  }
`;

export const MoveBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 99px;
  height: 28px;
  border: none;
  font-family: Helvetica, sans-serif;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  cursor: pointer;
  color: ${({checked}) => (checked ? '#ffffff' : '#7A7A7A')};
  background: ${({checked}) => (checked ? '#ED484F' : '#E4E4E4')};
  border-radius: 24px;
  margin-right: 4px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-left: 4px;
    margin-right: 0;
  `}
`;

export const MoveLine = styled.div`
  display: flex;
  align-items: center;
  background: #fafafa;
`;

export const Shopping = styled.span`
  display: inline-flex;
  align-items: center;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #000000;
`;

export const FollowBtn = styled.button`
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 75px;
  height: 28px;
  background: transparent;
  border: 1px solid #ed484f;
  border-radius: 24px;
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #ed484f;
  outline: none;
  cursor: pointer;
  margin-left: auto;

  &:hover {
    color: #ffffff;
    background-color: #ed484f;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-left: unset;
    margin-right: auto;
  `}
`;

export const GridContent = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(255px, 1fr));
  grid-gap: 38px;
  justify-items: center;
`;

export const ButtonMore = styled.div`
  background: #fff;
  border: 1px solid #666;
  border-radius: 24px;
  width: 140px;
  height: 36px;
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 58px auto 0;
  color: #666;
  cursor: pointer;

  &:hover {
    color: #ffffff;
    background-color: #666666;
  }
`;

export const ModalStyled = styled(Modal)`
  && {
    ${({custom}) => custom || null}
  }

  .ant-modal-header {
    padding: 24px;
  }

  .ant-modal-footer {
    padding: 16px 24px;
  }

  .ant-modal-close {
    right: -55px;
    top: -5px;
    position: absolute;
  }

  .ant-form-item-label {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #464646;
    margin-bottom: 7px;
  }

  .checkbox {
    margin: 0;
  }
`;

export const TextAreaStyled = styled(Input.TextArea)`
  &&& {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    border-color: #a7a7a7;

    .ant-input {
      background: #ffffff;
      border: 1px solid #a7a7a7;
      box-sizing: border-box;
      border-radius: 2px;
    }
  }
`;
export const InputStyled = styled(Input)`
  &&& {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    border-color: #a7a7a7;

    .ant-input {
      background: #ffffff;
      border: 1px solid #a7a7a7;
      box-sizing: border-box;
      border-radius: 2px;
    }
  }
`;

export const MessageButton = styled.button`
  padding: 3px 10px 2px 7px;
  border: 1px solid #8f8f8f;
  box-sizing: border-box;
  border-radius: 24px;
  font-family: Helvetica Neue;
  font-weight: 300;
  font-size: 10px;
  line-height: 140%;
  color: #666666;
  margin-right: 20px;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  transition: all 0.4s;
  min-width: 76px;

  & svg {
    margin-right: 3px;
  }

  &:hover {
    color: #000;
    border: 1px solid #000;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    padding: 3px 7px 2px 10px;
    margin-left: 20px;
    margin-right: 0;
    & svg {
      margin-left: 3px;
      margin-right: 0;
    }
  `}
`;

export const FollowButton = styled.button`
  border: 1px solid #ed484f;
  box-sizing: border-box;
  border-radius: 24px;
  font-family: Helvetica Neue;
  font-weight: 300;
  padding: 2px 10px 1px 7px;
  color: #ed484f;
  min-width: 76px;
  font-size: 12px;
  margin-right: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.4s;

  svg {
    margin-right: 5px;

    path {
      transition: all 0.4s;
    }
  }

  &:hover {
    background-color: #ed484f;
    color: #fff;

    svg path {
      fill: #fff;
    }
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
  padding: 2px 7px 1px 10px;
  margin-left: 20px;
  margin-right: 0;
  & svg {
    margin-left: 5px;
    margin-right: 0;
  }
`}
`;

export const ShareButton = styled.button`
  background: #ffffff;
  border: 1px solid;
  color: #000000;
  width: 80px;
  height: 28px;
  box-sizing: border-box;
  border-radius: 24px;
  font-weight: 500;
  font-size: 12px;
  line-height: 140%;
  ${({isShared}) =>
    isShared ? {borderColor: '#999999', color: '#999999'} : {borderColor: '#000000', color: '#000000'}}
`;

export const RedCloseButton = styled(Button)`
  &&& {
    width: 16px;
    padding: 0;
    display: flex;
    align-items: center;
    transition: ease 0.4s;

    svg {
      path {
        transition: ease 0.4s;
      }
    }

    &:hover {
      svg {
        path {
          fill: #000;
        }
      }
    }
  }
`;

export const ActionsWrapper = styled.div`
  display: flex;
  align-items: center;
`;

export const Description = styled.div`
  flex-grow: 1;
  font-family: Helvetica Neue;
  font-style: normal;

  strong {
    display: block;
    font-weight: bold;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    margin-bottom: 6px;
    transition: ease 0.4s;

    &:hover {
      cursor: pointer;
      color: ${blue};
    }
  }

  small {
    display: block;
    font-weight: normal;
    font-size: 10px;
    line-height: 132%;
    color: #7a7a7a;
  }
`;

export const AvatarStyled = styled(AvatarAntd)`
  &&& {
    ${({custom}) => custom || null};
    flex-shrink: 0;
  }

  && {
    width: 50px;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-right: 8px;

    i {
      font-size: 30px;
    }

    ${({theme: {isArabic}}) =>
      isArabic &&
      `
    margin-left: 8px;
    margin-right: 0;
  `}

    transition: ease .4s;

    &:hover {
      cursor: pointer;
      box-shadow: 0 0 5px rgba(000, 000, 000, 0.5);
    }
  }
`;

export const Follower = styled.div`
  display: flex;
  align-items: center;
  margin: 0px 16px 16px 24px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `} ${({styles}) => (styles ? {...styles} : null)}
`;

export const ShareUserList = styled.div`
  padding-bottom: 1px;
`;

export const SearchInput = styled(Input)`
  &&&& {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 19px;
    color: #000000;
    border-color: #e4e4e4;
    margin: 1px 0 16px;

    .ant-input {
      padding: 9px 24px 8px;
      background: #ffffff;
      border: 1px solid #e4e4e4;
      box-sizing: border-box;
      border-radius: 2px;
      color: #000000;
    }
  }
`;

export const TitleShare = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  padding: 12px 16px 15px;
  margin: 0;
`;

export const CarouselStyled = styled(Carousel)`
  &&&&& {
    .slick-slider {
      padding: 0 20px;
    }
  }
`;

export const SliderWrapper = styled.div`
  position: relative;
`;

export const NextButton = styled(Button)`
  &&& {
    position: absolute;

    right: 0;
    top: 0;
    padding: 0;
  }
`;
export const PrevButton = styled(Button)`
  &&& {
    position: absolute;
    left: 0;
    top: 0;
    padding: 0;
  }
`;

export const SliderButton = styled.button`
  border: none;
  background: none;
  display: flex;
  align-items: center;
  position: absolute;
  top: 10px;
  padding: 0;
  box-shadow: 0px 2px 9px rgba(0, 0, 0, 0.06);
  border-radius: 3px;
  z-index: 9999;
  ${({isRight}) => (isRight ? {right: 4, transform: 'rotate(180deg)'} : {left: 4})}
`;

export const ShareLink = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  line-height: 140%;
  text-align: center;
  color: #666666;
  width: 100%;
  display: grid;
  grid-gap: 9px;
  justify-items: center;
  cursor: pointer;
`;

export const SearchWrapper = styled.div`
  padding: 9px 16px;

  .title {
    font-family: Helvetica;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 140%;
    color: #464646;
    margin-bottom: 7px;
  }
`;

export const TopEmailBlock = styled.div`
  padding: 0 16px 8px;
  border-bottom: 1px solid #e4e4e4;

  p {
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    margin-bottom: 7px;
    color: #464646;
  }
`;

export const BottomTextAreaBlock = styled.div`
  padding: 24px 16px;

  p {
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    margin-bottom: 7px;
    color: #464646;
  }

  textarea {
    background: #ffffff;
    border: 1px solid #e4e4e4;
    box-sizing: border-box;
    border-radius: 2px;
    resize: none;
    width: 358px;
    height: 80px;
    padding: 10px 16px;
    color: #000;
  }
`;

export const ButtonContainer = styled.div`
  margin-top: 21px;
  text-align: right;
`;

export const Private = styled.div`
  display: inline-block;
  margin: 0 26px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  display: flex;
  align-items: center;
  color: #545454;

  & svg {
    margin: 0 15px;

    path {
      stroke: #545454;
    }
  }
`;
