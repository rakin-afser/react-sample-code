import styled from 'styled-components/macro';

export const ModalFooter = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding: 16px 24px;
  border-top: 1px solid #e4e4e4;
`;

export const Title = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const ModalBlock = styled.div`
  padding: ${({checkbox}) => (checkbox ? `30px 24px` : `16px 24px 0 24px`)};
  position: relative;

  &::after {
    ${({textarea, symbols}) => textarea && `content: '${symbols || '0'}/400 symbols';`}
    position: absolute;
    right: 40px;
    bottom: 8px;
    opacity: 0.6;
    color: #545454;
    font-family: SF Pro Display;
    font-size: 12px;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    &::after {
      right: unset;
      left: 40px;
    }
  `}
`;

export const Input = styled.input`
  width: 100%;
  height: 40px;
  border: ${({isError}) => (isError ? `1px solid #ed484f` : `1px solid #a7a7a7`)};
  box-sizing: border-box;
  border-radius: 2px;
  transition: border 0.5s;

  &::placeholder {
    color: #ed484f;
    opacity: 0.6;
  }
`;

export const Textarea = styled.textarea`
  width: 100%;
  height: 120px;
  border: 1px solid #a7a7a7;
  box-sizing: border-box;
  border-radius: 2px;
  resize: none;
  padding: 10px;
`;

export const Label = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  margin-bottom: 7px;

  & span {
    color: red;
  }
`;

export const CheckboxLabel = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-size: 14px;
  line-height: 140%;
  color: #a7a7a7;

  & span {
    color: #000;
    font-weight: 500;
  }
`;

export const Cancel = styled.div`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  transition: color 0.3s;

  &:hover {
    cursor: pointer;
    color: #464646;
  }
`;

export const Submit = styled.div`
  border: 1px solid #000000;
  box-sizing: border-box;
  border-radius: 24px;
  padding: 6px 34px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 1;
  margin-left: 24px;
  color: #000;
  transition: 0.4s;

  &:hover {
    cursor: pointer;
    color: #fff;
    border: 1px solid #ed484f;
    background-color: #ed484f;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin-right: 24px;
    margin-left: 0;
  `}
`;

export const SearchField = styled.input`
  width: 100%;
  outline: none;
  line-height: 140%;
  background: #ffffff;
  border: 1px solid #e4e4e4;
  box-sizing: border-box;
  border-radius: 2px;
  padding: 9px 24px 8px 24px;
  font-family: Helvetica Neue;
  font-size: 16px;
  color: #000;
  margin-bottom: 16px;

  &::placeholder {
    color: #8f8f8f;
  }
`;
