import React, {useState, useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import Scrollbars from 'react-scrollbars-custom';
import defaultAvatar from 'images/avatarPlaceholder.png';
import useGlobal from 'store';
import {useHistory} from 'react-router-dom';
import ModalContainer from 'components/Modals/ModalContainer';
import {Title, ModalBlock, SearchField} from 'containers/MyProfile/components/List/components/styled';
import {
  MessageButton,
  FollowButton,
  ActionsWrapper,
  Description,
  AvatarStyled,
  Follower,
  RedCloseButton
} from 'containers/MyProfile/components/List/styled';
import Icon from 'components/Icon';
import {ReactComponent as RedCloseIcon} from 'images/svg/close-red.svg';
import {ReactComponent as ChatIcon} from 'images/svg/chat.svg';
import {useMutation} from '@apollo/client';
import {FOLLOW_UNFOLLOW_USER} from 'mutations';
import {userId} from 'util/heplers';

const FollowersModal = ({isOpen, onClose}) => {
  const history = useHistory();
  const [globalStore, setGlobalStore] = useGlobal();
  const [followers, setFollowers] = useState(null);
  const {t} = useTranslation();
  const [followUser] = useMutation(FOLLOW_UNFOLLOW_USER);

  useEffect(() => {
    setFollowers(globalStore.followersModal.data);
  }, [globalStore.followersModal.data]);

  const onFollowUser = (id) => {
    followUser({
      variables: {userId: id, followerId: userId},
      update(cache, {data}) {
        cache.modify({
          fields: {
            user() {
              return {data};
            }
          }
        });
      }
    });
  };

  const handleSearch = ({target: {value}}) => {
    if (value.length) {
      const searchResult = globalStore?.followersModal?.data?.filter(
        ({username}) => username.toLowerCase().indexOf(value.toLowerCase()) > -1 && username
      );

      setFollowers(searchResult);
    } else {
      setFollowers(globalStore.followersModal.data);
    }
  };

  const onGoToUser = (userId) => {
    setGlobalStore.setFollowersModal(false);
    history.push(`/profile/${userId}/posts`);
  };

  return (
    <ModalContainer
      title={
        <Title>{globalStore.followersModal.type === 'followers' ? t('myProfilePage.followers') : 'Following'}</Title>
      }
      onClose={onClose}
      isOpen={isOpen}
    >
      <ModalBlock>
        <SearchField onChange={handleSearch} placeholder={t('listsPage.searchForUsername')} />
      </ModalBlock>
      <Scrollbars
        rtl={false}
        clientWidth={4}
        noDefaultStyles={false}
        noScroll={false}
        style={{height: '400px', width: '100%'}}
        thumbYProps={{className: 'thumbY'}}
      >
        {followers &&
          followers.map(({followers, following, profile_picture, id, userId, username}) => {
            return (
              <Follower key={id}>
                <AvatarStyled onClick={() => onGoToUser(userId)} icon="user" src={profile_picture || defaultAvatar} />
                <Description>
                  <strong onClick={() => onGoToUser(userId)}>{username}</strong>
                  <small>{`${followers?.length} ${t('myProfilePage.followers')} | ${following?.length} ${t(
                    'myProfilePage.following'
                  )}`}</small>
                </Description>
                <ActionsWrapper>
                  {globalStore.followersModal.type !== 'followers' && (
                    <MessageButton>
                      <ChatIcon />
                      <span>{t('general.message')}</span>
                    </MessageButton>
                  )}
                  {globalStore.followersModal.type !== 'following' && (
                    <FollowButton onClick={() => onFollowUser(userId)}>
                      <Icon type="plus" width="9px" height="9px" />
                      {t('general.follow')}
                    </FollowButton>
                  )}
                  {globalStore.followersModal.type !== 'followers' && (
                    <RedCloseButton onClick={() => onFollowUser(userId)} type="link">
                      <RedCloseIcon />
                    </RedCloseButton>
                  )}
                </ActionsWrapper>
              </Follower>
            );
          })}
      </Scrollbars>
    </ModalContainer>
  );
};

export default FollowersModal;
