import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';

import ModalContainer from 'components/Modals/ModalContainer';
import Checkbox from 'components/Checkbox';
import {
  ModalFooter,
  Title,
  ModalBlock,
  Input,
  Textarea,
  Label,
  CheckboxLabel,
  Cancel,
  Submit
} from 'containers/MyProfile/components/List/components/styled';

const EditListModal = ({isOpen, data, onSubmit, onClose}) => {
  const {t} = useTranslation();
  const [error, setError] = useState({
    name: null,
    description: null,
    hashtags: null
  });
  const [formData, setFormData] = useState({
    name: data?.name || '',
    description: data?.description || '',
    hashtags: data?.hashtags || '',
    private: data?.private || false
  });

  const isFormValid = () => {
    if (!formData.name?.length) return false;
    return true;
  };

  const onSubmitHandler = (e) => {
    if (isFormValid()) {
      onSubmit(formData);
      onClose();
    } else {
      setError({...error, name: 'Please add some name'});
    }
  };

  const onChange = (e) => {
    setFormData({...formData, [e.target.name]: e.target.name === 'private' ? e.target.checked : e.target.value});
    setError({
      name: null,
      description: null,
      hashtags: null
    });
  };

  return (
    <ModalContainer title={<Title>{t('listsPage.editList')}</Title>} onClose={onClose} isOpen={isOpen}>
      <ModalBlock>
        <Label>
          {t('general.name')} <span>*</span>
        </Label>
        <Input placeholder={error.name} isError={!!error.name} name="name" onChange={onChange} value={formData.name} />
      </ModalBlock>
      <ModalBlock textarea symbols={formData.description?.length}>
        <Label>{t('listsPage.addDescription')}</Label>
        <Textarea name="description" onChange={onChange} value={formData.description} />
      </ModalBlock>
      <ModalBlock>
        <Label>{t('general.hashtags')}</Label>
        <Input name="hashtags" onChange={onChange} value={formData.hashtags} />
      </ModalBlock>
      <ModalBlock checkbox>
        <Checkbox name="private" onChange={onChange} checked={formData.private}>
          <CheckboxLabel>
            <span>{t('general.private')}</span> {t('listsPage.privateDescription')}
          </CheckboxLabel>
        </Checkbox>
      </ModalBlock>
      <ModalFooter>
        <Cancel onClick={onClose}>{t('general.cancel')}</Cancel>
        <Submit onClick={onSubmitHandler}>{t('general.save')}</Submit>
      </ModalFooter>
    </ModalContainer>
  );
};

export default EditListModal;
