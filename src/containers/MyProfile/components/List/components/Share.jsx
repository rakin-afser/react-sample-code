import React from 'react';
import PropTypes from 'prop-types';

import ShareBlock from 'components/ShareBlock';
import {ReactComponent as CloseIcon} from 'images/svg/modal-close.svg';
import {ModalStyled} from '../styled';

const ShareModal = ({shareModal, setShareModal}) => {
  const hideModal = () => {
    setShareModal(false);
  };

  return (
    <ModalStyled
      closeIcon={<CloseIcon className="close-icon" />}
      width="384px"
      visible={shareModal}
      footer={null}
      onCancel={hideModal}
      custom=".ant-modal-body{padding: 0px;}"
    >
      <ShareBlock title="Share" noCloseBtn />
    </ModalStyled>
  );
};

ShareModal.propTypes = {
  shareModal: PropTypes.bool.isRequired,
  setShareModal: PropTypes.func.isRequired
};

export default ShareModal;
