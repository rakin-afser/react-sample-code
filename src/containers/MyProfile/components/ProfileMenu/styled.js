import styled from 'styled-components/macro';

import {Link as LinkAtom} from 'react-router-dom';
import {secondaryFont} from 'constants/fonts';

export const List = styled.ul`
  list-style: none;
  margin: 0;
  border-bottom: 1px solid #eeeeee;
`;

export const Item = styled.li`
  padding-right: 25px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: ease 0.4s;
  background-color: ${({active}) => (active ? '#f7f7f7' : 'transparent')};

  svg {
    transition: ease 0.4s;
    opacity: ${({active}) => (active ? '1' : '0')};

    path {
      fill: #666666;
    }
  }

  i {
    svg {
      path {
        fill: ${({active}) => (active ? '#000' : '#666')};
      }
    }
  }

  &:hover {
    background-color: #f7f7f7;
  }
`;

export const Link = styled(LinkAtom)`
  display: flex;
  align-items: center;
  width: 100%;
  height: 44px;
  font-family: ${secondaryFont};
  font-weight: ${({active}) => (active ? '500' : '400')};
  font-size: 16px;
  line-height: 140%;
  color: ${({active}) => (active ? '#000' : '#7a7a7a')};
  padding: 0 0 0 23px;
  ${({active}) => (active ? '' : null)};
  transition: ease 0.4s;
  letter-spacing: -0.016em;

  i {
    position: relative;
    top: -1px;
    margin-right: 10px;

    svg {
      opacity: 1;
    }
  }

  &:hover {
    color: #000;
  }
`;
