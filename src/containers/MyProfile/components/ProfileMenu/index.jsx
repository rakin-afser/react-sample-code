import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icon';
import {List, Item, Link} from './styled';

const ProfileMenu = ({items, selected}) => {
  return (
    <List>
      {items.map((item) => (
        <Item active={item.id === selected} key={item.id}>
          <Link active={item.id === selected ? 1 : 0} to={item.href}>
            {item.id === 'promote' && (
              <Icon type="speaker" width={18} height={18} color={item.id === selected ? '#000' : '#666'} />
            )}
            {item.id === 'dashboard' && (
              <Icon type="tileOutlined" width={14} height={14} color={item.id === selected ? '#000' : '#666'} />
            )}
            {item.text}
          </Link>
          <Icon active={item.id === selected} type="chevronRight" width="9" height="15" color="#000000" />
        </Item>
      ))}
    </List>
  );
};

ProfileMenu.defaultProps = {
  items: [],
  selected: 'profile'
};

ProfileMenu.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any),
  selected: PropTypes.string
};

export default ProfileMenu;
