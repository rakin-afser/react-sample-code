import React, {useState, useRef, useEffect} from 'react';
import ClampLines from 'react-clamp-lines';
import PromoteIcon from 'assets/PromoteIcon';
import HeartIcon from 'assets/Heart';
import FilledHeartIcon from 'assets/FilledHeart';
import {lightGreen, transparentTextColor as gray300} from 'constants/colors';
import {ReactComponent as CheckIcon} from '../../icons/check.svg';
import {Card, Image, SelectedFilter, Body, Top, Price, Currency, Text, Title, Actions, Like, Active} from './styled';

const PromoteListItem = ({
  id,
  promoteListId,
  price,
  currency = 'BD',
  name,
  isLiked,
  isActive,
  img,
  noSelectedFilter,
  onCardSelect,
  onCardDeselect,
  selected,
  onSelectAll,
  ...props
}) => {
  const [selectedAdd, setSelectedAll] = useState(false);
  const ref = useRef(null);
  const onCardClick = (cardId, listId) => {
    if (!selected) {
      onCardSelect(cardId, listId);
    } else {
      onCardDeselect(cardId, listId);
    }
  };

  useEffect(() => {
    if (onSelectAll && !selectedAdd) {
      setTimeout(() => {
        ref.current.click();
      }, 200);
      setSelectedAll(true);
    }

    if (!onSelectAll && selectedAdd) {
      setTimeout(() => {
        ref.current.click();
      }, 200);
      setSelectedAll(false);
    }
  }, [onSelectAll, selectedAdd]);

  return (
    <Card ref={ref} selected={selected} onClick={() => onCardClick(id, promoteListId)} {...props}>
      <Image src={img} />
      <SelectedFilter>{!noSelectedFilter && <CheckIcon />}</SelectedFilter>
      <Body>
        <Top>
          <Price>
            <Currency>{currency}</Currency>
            <span>{price.toFixed(2)}</span>
          </Price>
          <Text>Free Shipping</Text>
        </Top>
        <Title>
          <ClampLines id={`promote-item-title-${id}`} text={name} lines={1} buttons={false} />
        </Title>
        <Actions>
          <Like isLiked={isLiked}>
            <HeartIcon width={20} height={18} isLiked={false} />
            <FilledHeartIcon width={20} height={18} />
          </Like>
          <Active isActive={isActive}>
            <PromoteIcon classes="iconPromote" fillColor={gray300} />
            <PromoteIcon classes="iconFilledPromote" fillColor={lightGreen} />
          </Active>
        </Actions>
      </Body>
    </Card>
  );
};

export default PromoteListItem;
