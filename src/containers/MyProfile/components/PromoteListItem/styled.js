import styled from 'styled-components';
import {mainBlackColor as black, transparentTextColor as gray300} from 'constants/colors';

export const SelectedFilter = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 228px;
  background: rgba(0, 0, 0, 0.4);
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  opacity: 0;
  transition: ease 0.7s;
`;

export const Card = styled.div`
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  position: relative;
  overflow: hidden;

  &:hover {
    cursor: pointer;

    ${SelectedFilter} {
      opacity: 1;
    }
  }

  ${({selected}) =>
    selected &&
    `${SelectedFilter} {
        opacity: 1;
    }
   `}
`;

export const Image = styled.img`
  width: 228px;
  height: 228px;
  object-fit: contain;
`;

export const Body = styled.div`
  padding: 9px 10px 10px 10px;
`;

export const Top = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Price = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  color: ${black};
  margin-bottom: 12px;

  span {
    font-weight: 500;
  }
`;

export const Currency = styled.span`
  font-size: 12px;
  margin-right: 2px;
`;

export const Text = styled.p`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${gray300};
`;

export const Title = styled.h4`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${black};
  margin-bottom: 15px;
`;

export const Actions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Like = styled.div`
  .iconFilledHeart {
    display: ${({isLiked}) => (isLiked ? 'block' : 'none')};
  }

  .iconHeart {
    display: ${({isLiked}) => (isLiked ? 'none' : 'block')};
  }

  &:hover {
    cursor: pointer;

    .iconFilledHeart {
      display: ${({isLiked}) => (isLiked ? 'none' : 'block')};
    }

    .iconHeart {
      display: ${({isLiked}) => (isLiked ? 'block' : 'none')};
    }
  }
`;

export const Active = styled.div`
  .iconFilledPromote {
    display: ${({isActive}) => (isActive ? 'block' : 'none')};
  }

  .iconPromote {
    display: ${({isActive}) => (isActive ? 'none' : 'block')};
  }

  &:hover {
    cursor: pointer;

    .iconFilledPromote {
      display: ${({isActive}) => (isActive ? 'none' : 'block')};
    }

    .iconPromote {
      display: ${({isActive}) => (isActive ? 'block' : 'none')};
    }
  }
`;
