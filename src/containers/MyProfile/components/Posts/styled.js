import styled from 'styled-components';
import {ReactComponent as AddPostIcon} from './img/addPost.svg';
import {ReactComponent as PhotoVideoBtn} from './img/photoVideo.svg';
import {ReactComponent as ProductBtn} from './img/product.svg';
import {primaryColor} from 'constants/colors';

export const AddNewBlock = styled.div`
  padding: 15px 5px 15px 17px;
  margin: 24px 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #ffffff;
  box-sizing: border-box;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  p {
    cursor: pointer;
    color: #ed484f;
    font-weight: 500;
    font-size: 14px;
    text-align: center;
    margin: 0;
    i {
      margin-right: 20px;
    }
  }
`;

export const AddNewPost = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  transition: color ease-in-out 0.4s;

  &:hover {
    color: ${primaryColor};
  }
`;

export const AddPostIconStyled = styled(AddPostIcon)`
  margin-right: 36px;
`;

export const ButtonsHolder = styled.div`
  display: flex;
  align-items: center;
`;

export const AddPostIconBtn = styled.button`
  margin-right: 10px;
  display: flex;
  align-items: center;
  transition: color ease-in-out 0.4s;
  font-size: 13px;

  & span {
    position: relative;
    top: 3px;
    opacity: 0.6;
    transition: opacity ease-in-out 0.4s;
  }

  &:hover {
    color: ${primaryColor};

    & span {
      opacity: 1;
    }

    & svg {
      fill: ${primaryColor};
    }
  }
`;

export const PhotoVideoBtnStyled = styled(PhotoVideoBtn)`
  margin-right: 9px;
  font-size: inherit;
  fill: #000;
`;

export const ProductBtnStyled = styled(ProductBtn)`
  margin-right: 9px;
  font-size: inherit;
`;
