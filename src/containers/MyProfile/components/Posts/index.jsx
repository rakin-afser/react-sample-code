import React from 'react';
import FadeOnMount from 'components/Transitions';
import PostContent from 'containers/ShopPage/desktop/PostsPage/PostsContent';

const Posts = ({data, loading}) => {
  return (
    <FadeOnMount>
      <div style={{paddingTop: 24}}>
        <PostContent data={data} withPopularProducts={false} loading={loading} isMyProfile />
      </div>
    </FadeOnMount>
  );
};

export default Posts;
