import styled from 'styled-components';
import {primaryColor} from 'constants/colors';

export const Hr = styled.hr`
  border: none;
  height: 2px;
  width: 100%;
  background-color: ${primaryColor};
`;

export const TagsTitle = styled.h2`
  font-weight: bold;
  font-size: 32px;
  letter-spacing: 0.013em;
  color: #343434;
`;

export const NoPostsTitle = styled.h3`
  font-size: 22px;
`;

export const Grid = styled.div`
  margin: 24px 0 0 0;
  padding: 0 55px 0 59px;
  grid-template-columns: max-content max-content max-content;
  gap: 0 44px;
  display: grid;
`;
