import React from 'react';

import Post from 'components/Cards/Post';
// import Grid from 'components/Grid';
import PopularTags from 'components/PopularTags';
import {Hr, TagsTitle, NoPostsTitle, Grid} from './styled';
import FadeOnMount from 'components/Transitions';
import Pagination from 'components/PagePagination';

const PostsGridView = ({data}) => {
  return (
    <FadeOnMount>
      {/* <Grid sb wrap margin="24px 0 0 0" padding="0 55px 0 59px"> */}
      <Grid>
        {data?.length ? (
          data?.map((item, index) => (
            <Post style={{maxWidth: 228}} noHeader key={index} index={index} content={item} margin="0 0 49px 0" />
          ))
        ) : (
          <NoPostsTitle>No posts yet</NoPostsTitle>
        )}
      </Grid>
      {/* </Grid> */}
      {/* {data?.length ? (
        <Pagination totalPages={totalPages} prevPage={prevPage} nextPage={nextPage} currentPage={currentPage} />
      ) : null} */}

      {/** popular tags temporary hidden */}
      {/* <Grid sb wrap margin="0" padding="0 55px 0 59px"> */}
      {/*   <div> */}
      {/*     <TagsTitle>Popular tags</TagsTitle> */}
      {/*   </div> */}
      {/*   <Hr /> */}
      {/*   <PopularTags /> */}
      {/* </Grid> */}
    </FadeOnMount>
  );
};

PostsGridView.propTypes = {};

export default PostsGridView;
