import styled from 'styled-components';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  grayTextColor as gray700,
  headerShadowColor as gray400
} from 'constants/colors';

export const Section = styled.section`
  margin-left: 15px;
  width: 100%;
`;

export const LoadMore = styled.div`
  text-align: center;
  position: relative;
  margin-top: 40px;
  padding-bottom: 35px;

  &:after {
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
    content: '';
    width: 500px;
    height: 8px;
    background: ${gray400};
  }
`;

export const LoadMoreButton = styled.button`
  color: ${black};
  border: 1px solid ${black};
  margin: 0 auto;
  border-radius: 4px;
  padding: 13px 55px 8px 55px;
  line-height: 1;

  &:hover {
    background: ${black};
    color: ${white};
  }
`;

export const ProductsNumber = styled.p`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  text-align: center;
  color: ${gray700};
  margin-top: 8px;
`;
