import React, {useState} from 'react';
import Campaign from 'containers/MyProfile/components/Campaign';
import FadeOnMount from 'components/Transitions';
import SearchForm from '../SearchForm';
import PromoteList from '../PromoteList';
import {Section, LoadMore, LoadMoreButton, ProductsNumber} from './styled';
import promoteLists from './staticData';

const Promote = () => {
  const [showCampaign, setShowCampaign] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);

  const ListsView = () => {
    return (
      <FadeOnMount>
        <SearchForm />
        {promoteLists.map((list, index) => (
          <PromoteList setSelectedItems={setSelectedItems} setShowCampaign={setShowCampaign} {...list} key={index} />
        ))}
        <LoadMore>
          <LoadMoreButton>Load more products</LoadMoreButton>
        </LoadMore>
        <ProductsNumber>36 of 14 678 products</ProductsNumber>
      </FadeOnMount>
    );
  };

  return (
    <Section>
      {showCampaign ? <Campaign selectedItems={selectedItems} setShowCampaign={setShowCampaign} /> : <ListsView />}
    </Section>
  );
};

export default Promote;
