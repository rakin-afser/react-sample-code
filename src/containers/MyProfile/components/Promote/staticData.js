import promoteItem1 from '../../img/promoteItem1.png';
import promoteItem2 from '../../img/promoteItem2.png';
import promoteItem3 from '../../img/promoteItem3.png';
import promoteItem4 from '../../img/promoteItem4.png';
import promoteItem5 from '../../img/promoteItem5.png';
import promoteItem6 from '../../img/promoteItem6.png';
import promoteItem7 from '../../img/promoteItem7.png';
import promoteItem8 from '../../img/promoteItem8.png';
import promoteItem9 from '../../img/promoteItem9.png';
import promoteItem10 from '../../img/promoteItem10.png';
import promoteItem11 from '../../img/promoteItem11.png';
import promoteItem12 from '../../img/promoteItem12.png';

export const promoteList1 = {
  title: 'Chanel Beauty',
  items: [
    {
      id: 1,
      price: 998,
      name: 'Fenty Beauty Primer',
      isLiked: false,
      isActive: false,
      img: promoteItem1
    },
    {
      id: 2,
      price: 76,
      name: 'Fenty Red Lipstick',
      isLiked: false,
      isActive: true,
      img: promoteItem2
    },
    {
      id: 3,
      price: 4738,
      name: 'Nike Air New Collection',
      isLiked: true,
      isActive: false,
      img: promoteItem3
    },
    {
      id: 4,
      price: 4738,
      name: 'Oversized Hoodie',
      isLiked: false,
      isActive: false,
      img: promoteItem4
    },
    {
      id: 5,
      price: 4738,
      name: 'Red Classic Bag',
      isLiked: false,
      isActive: false,
      img: promoteItem5
    },
    {
      id: 6,
      price: 4738,
      name: 'White Socks 3 Pairs',
      isLiked: false,
      isActive: false,
      img: promoteItem6
    },
    {
      id: 7,
      price: 4738,
      name: 'Leather Jacket',
      isLiked: false,
      isActive: false,
      img: promoteItem7
    },
    {
      id: 8,
      price: 4738,
      name: 'Black Shorts',
      isLiked: false,
      isActive: false,
      img: promoteItem8
    },
    {
      id: 9,
      price: 4738,
      name: 'White Shirt',
      isLiked: false,
      isActive: false,
      img: promoteItem9
    },
    {
      id: 10,
      price: 4738,
      name: 'Brown Sweater',
      isLiked: false,
      isActive: false,
      img: promoteItem10
    },
    {
      id: 11,
      price: 4738,
      name: 'Gold Earrings',
      isLiked: false,
      isActive: false,
      img: promoteItem11
    },
    {
      id: 12,
      price: 4738,
      name: 'Beige Coat',
      isLiked: false,
      isActive: false,
      img: promoteItem12
    }
  ]
};

export const promoteList2 = {
  title: 'Burberry',
  items: [
    {
      id: 1,
      price: 4738,
      name: 'Oversized Hoodie',
      isLiked: false,
      isActive: false,
      img: promoteItem4
    },
    {
      id: 2,
      price: 4738,
      name: 'Red Classic Bag',
      isLiked: false,
      isActive: false,
      img: promoteItem5
    },
    {
      id: 3,
      price: 4738,
      name: 'White Socks 3 Pairs',
      isLiked: false,
      isActive: false,
      img: promoteItem6
    },
    {
      id: 4,
      price: 998,
      name: 'Fenty Beauty Primer',
      isLiked: false,
      isActive: false,
      img: promoteItem1
    },
    {
      id: 5,
      price: 76,
      name: 'Fenty Red Lipstick',
      isLiked: false,
      isActive: false,
      img: promoteItem2
    },
    {
      id: 6,
      price: 4738,
      name: 'Nike Air New Collection',
      isLiked: false,
      isActive: false,
      img: promoteItem3
    }
  ]
};

export const promoteList3 = {
  title: 'Off White',
  items: [
    {
      id: 1,
      price: 4738,
      name: 'Leather Jacket',
      isLiked: false,
      isActive: false,
      img: promoteItem7
    },
    {
      id: 2,
      price: 4738,
      name: 'Black Shorts',
      isLiked: false,
      isActive: false,
      img: promoteItem8
    },
    {
      id: 3,
      price: 4738,
      name: 'White Shirt',
      isLiked: false,
      isActive: false,
      img: promoteItem9
    },
    {
      id: 4,
      price: 998,
      name: 'Fenty Beauty Primer',
      isLiked: false,
      isActive: false,
      img: promoteItem1
    },
    {
      id: 5,
      price: 76,
      name: 'Fenty Red Lipstick',
      isLiked: false,
      isActive: false,
      img: promoteItem2
    },
    {
      id: 6,
      price: 4738,
      name: 'Nike Air New Collection',
      isLiked: false,
      isActive: false,
      img: promoteItem3
    },
    {
      id: 7,
      price: 4738,
      name: 'Oversized Hoodie',
      isLiked: false,
      isActive: false,
      img: promoteItem4
    },
    {
      id: 8,
      price: 4738,
      name: 'Red Classic Bag',
      isLiked: false,
      isActive: false,
      img: promoteItem5
    },
    {
      id: 9,
      price: 4738,
      name: 'White Socks 3 Pairs',
      isLiked: false,
      isActive: false,
      img: promoteItem6
    }
  ]
};

export default [promoteList1, promoteList2, promoteList3];
