import React, {useState} from 'react';
import {useParams} from 'react-router';
import {isEmpty} from 'lodash';

import {Nav, Tab} from 'containers/MyProfile/styled';
import {TabsWrapper, Content, ContentMain} from 'containers/MyProfile/components/MyRewards/styled';
import Transaction from 'containers/MyProfile/components/MyRewards/components/Transaction';
import Rewards from 'containers/MyProfile/components/MyRewards/components/Rewards';
import RightSidebar from 'containers/MyProfile/components/MyRewards/components/RightSidebar';
import ShopCoins from 'containers/MyProfile/components/MyRewards/components/ShopCoins';
import {useUser} from 'hooks/reactiveVars';
import useRewards from 'hooks/useRewards';
import Pagination from 'components/PagePagination';
import useDebounce from 'hooks/useDebounce';
import Balance from './components/Balance';

const tabs = [
  {name: 'Rewards', slug: 'rewards'},
  {name: 'Transaction', slug: 'transactions'},
  // {name: 'Shop Coins', slug: 'shopcoins'},
  {name: 'Balance', slug: 'balance'}
];

const MyRewards = () => {
  const {slug} = useParams();
  const [user] = useUser();
  const [inputValue, setInputValue] = useState('');
  const debouncedSearchTerm = useDebounce(inputValue, 500);
  const [sort, setSort] = useState({
    sortBy: 'AwardedDate', // sort by date on default
    orderBy: 'DESC'
  });
  const {currentData, loading, error, rewardsPagination} = useRewards({
    variables: {search: debouncedSearchTerm, ...sort}
  });

  const onSearch = () => {
    rewardsPagination.resetPagination();
  };

  const getPage = () => {
    switch (slug) {
      case 'transactions':
        return (
          <>
            <Transaction
              onSearch={onSearch}
              setSearch={setInputValue}
              search={inputValue}
              loading={loading}
              data={currentData}
              error={error}
              setSort={setSort}
              sort={sort}
            />
            {!(isEmpty(currentData) && rewardsPagination?.currentPage === 1) && (
              <Pagination
                totalPages={rewardsPagination?.totalPages}
                prevPage={rewardsPagination?.prevPage}
                nextPage={rewardsPagination?.nextPage}
                currentPage={rewardsPagination?.currentPage}
              />
            )}
          </>
        );
      // case 'shopcoins':
      //   return <ShopCoins />;
      case 'balance':
        return <Balance />;
      case 'rewards':
      default:
        return <Rewards loading={loading} data={currentData} error={error} />;
    }
  };

  return (
    <Content>
      <ContentMain>
        <TabsWrapper>
          <Nav>
            {tabs.map((tab, i) => (
              <Tab
                active={slug ? tab.slug === slug : tab.slug === 'rewards'}
                key={i}
                to={`/profile/rewards${tab.slug === 'rewards' ? '' : `/${tab.slug}`}`}
              >
                {tab.name}
              </Tab>
            ))}
          </Nav>
        </TabsWrapper>
        {getPage()}
      </ContentMain>
      <RightSidebar />
    </Content>
  );
};

export default MyRewards;
