import React from 'react';

// eslint-disable-next-line react/prop-types
const CustomizedDot = ({cx, cy}) => (
  <circle cx={cx - 0} cy={cy - 0} r={2} stroke="#61BF9D" strokeWidth={3} fill="#61BF9D" />
);

CustomizedDot.propTypes = {};

export default CustomizedDot;
