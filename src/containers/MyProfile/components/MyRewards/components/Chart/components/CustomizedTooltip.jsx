import React from 'react';
import {TooltipStyled} from 'containers/MyProfile/components/MyRewards/components/Chart/styled';

// eslint-disable-next-line react/prop-types
const CustomizedTooltip = ({active, payload, label}) => {
  if (active) {
    return (
      <TooltipStyled>
        Midcoins $<span>{payload && payload[0]?.payload?.amt}</span>
      </TooltipStyled>
    );
  }

  return null;
};

CustomizedTooltip.propTypes = {};

export default CustomizedTooltip;
