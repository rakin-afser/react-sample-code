import React from 'react';
import {AreaChart, ReferenceDot, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer} from 'recharts';
import PropTypes from 'prop-types';
// import CustomizedDot from 'containers/MyProfile/components/MyRewards/components/Chart/components/CustomizedDot';
import CustomizedTooltip from 'containers/MyProfile/components/MyRewards/components/Chart/components/CustomizedTooltip';
import moment from 'moment';

const Chart = ({data, width, height}) => {
  const dateFormatter = (date) => {
    return moment(date).format('D MMM');
  };

  data &&
    data.forEach((d) => {
      // eslint-disable-next-line no-param-reassign
      d.date = moment(d.date).valueOf();
    });

  return (
    <ResponsiveContainer width={width} height={height}>
      <AreaChart data={data} margin={{top: 10, right: 70, left: 0, bottom: 0}}>
        <CartesianGrid verticalPoints={2} horizontalPoints={0} vertical={false} horizontal={false} />
        <YAxis dataKey="amt" stroke="#464646" type="number" axisLine={false} tickLine={false} />
        <XAxis
          dataKey="date"
          stroke="#464646"
          scale="time"
          type="time"
          tickFormatter={dateFormatter}
          domain={data && data.length && [data[0].data, data[data.length - 1].data]}
        />
        <ReferenceDot />
        <Tooltip content={<CustomizedTooltip />} />
        <Area
          dataKey="amt"
          stroke="rgba(97, 191, 157, 0.49)"
          strokeWidth={3}
          fill="rgba(97, 191, 157, 0.08)"
          // dot={<CustomizedDot />}
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};

Chart.defaultProps = {
  width: 600,
  height: 200
};

Chart.propTypes = {
  data: PropTypes.arrayOf(PropTypes.any).isRequired,
  width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string])
};

export default Chart;
