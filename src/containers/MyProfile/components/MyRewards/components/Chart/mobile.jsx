import React from 'react';
import {AreaChart, ReferenceDot, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer} from 'recharts';
// import CustomizedDot from 'containers/MyProfile/components/MyRewards/components/Chart/components/CustomizedDot';
import CustomizedTooltip from 'containers/MyProfile/components/MyRewards/components/Chart/components/CustomizedTooltip';
import moment from 'moment';

const Chart = ({data}) => {
  const dateFormatter = (date) => {
    return moment(date).format('D MMM');
  };

  const formattedData = data?.map((d) => {
    return {...d, date: moment(d.date).valueOf()};
  });

  return (
    <ResponsiveContainer width="100%">
      <AreaChart data={formattedData}>
        <YAxis width={30} dataKey="amt" stroke="#464646" type="number" axisLine={false} tickLine={false} />
        <XAxis
          tick={{fill: '#464646'}}
          strokeDashoffset={8}
          strokeDasharray={8}
          strokeWidth={2}
          dataKey="date"
          tickLine={false}
          stroke="#E4E4E4"
          scale="time"
          type="time"
          tickFormatter={dateFormatter}
          domain={data && [data[0].data, data[data.length - 1].data]}
        />
        <ReferenceDot />
        <Tooltip content={<CustomizedTooltip value="text" />} />
        <Area
          dataKey="amt"
          stroke="rgba(97, 191, 157, 0.49)"
          strokeWidth={3}
          fill="rgba(97, 191, 157, 0.08)"
          // dot={<CustomizedDot />}
        />
      </AreaChart>
    </ResponsiveContainer>
  );
};

export default Chart;
