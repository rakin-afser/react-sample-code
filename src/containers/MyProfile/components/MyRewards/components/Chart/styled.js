import styled from 'styled-components';

export const TooltipStyled = styled.div`
  position: relative;
  left: -10px;
  padding: 7px;
  background: #e6f1ff;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.04);
  border-radius: 4px;
  color: #464646;
  font-size: 10px;
  text-align: center;
  transform: translateX(-50%);

  &::after {
    content: '';
    position: absolute;
    left: 50%;
    bottom: -5px;
    transform: translateX(-50%);
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 5px 6px 0 6px;
    border-color: #e6f1ff transparent transparent transparent;
  }
`;

export const DotStyled = styled.div`
  transition: ease 0.4s;
`;
