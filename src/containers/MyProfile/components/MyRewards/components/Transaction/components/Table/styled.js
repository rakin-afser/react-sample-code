import styled from 'styled-components/macro';
import {headerShadowColor, midCoinsColor, primaryColor, secondaryTextColor} from 'constants/colors';
import {ReactComponent as ToggleIcon} from './img/toggleArrows.svg';
import {ReactComponent as PopupBtnIcon} from './img/popupBtn.svg';
import {Link} from 'react-router-dom';

export const Table = styled.div`
  table-layout: fixed;
  width: 100%;
`;

export const Thead = styled.div`
  display: flex;
`;

export const TheadTh = styled.div`
  position: relative;
  text-align: center;
  padding-bottom: 24px;
  color: ${secondaryTextColor};
  transition: all ease 0.3s;

  &:nth-child(1) {
    width: 14%;
    padding-left: 14px;
    text-align: left;
    cursor: pointer;
    font-weight: 500;
    color: #000;

    &:hover {
      color: ${primaryColor};
    }
  }

  &:nth-child(2) {
    width: 19%;
  }

  &:nth-child(3) {
    width: 19%;
    cursor: pointer;

    &:hover {
      color: ${primaryColor};
    }
  }

  &:nth-child(4) {
    width: 18%;
    cursor: pointer;

    &:hover {
      color: ${primaryColor};
    }
  }

  &:nth-child(5) {
    margin-left: auto;
    padding-right: 64px;
    text-align: right;
  }
`;

export const IconStyled = styled(ToggleIcon)`
  position: relative;
  top: 2px;
  margin-left: 4px;

  path:nth-child(1) {
    fill: ${({active}) => (active ? '#000000' : '#a7a7a7')};
  }

  path:nth-child(2) {
    fill: ${({active}) => (!active ? '#000000' : '#a7a7a7')};
  }
`;

export const Tr = styled.div`
  border-bottom: 1px solid ${headerShadowColor};
  display: flex;
  justify-content: flex-start;
  align-items: center;

  & > div {
    position: relative;
    padding-bottom: 0;
    text-align: center;
  }
`;

export const TrTransactionDate = styled.div`
  border-bottom: 1px solid ${headerShadowColor};
  padding-bottom: 5px;

  &:not(:first-child) {
    padding-top: 24px;
  }
`;

export const Image = styled(Link)`
  margin: 11px;
  width: 50px;
  height: 50px;
  border: 1px solid #e4e4e4;
  border-radius: 4px;
  overflow: hidden;

  img {
    display: block;
    width: 100%;
    height: auto;
    object-fit: cover;
  }
`;

export const TextColumn = styled.div`
  padding: 0 1px;
  width: 124px;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: ${secondaryTextColor};

  & a {
    color: ${secondaryTextColor};

    &:hover {
      color: #111;
    }
  }
`;

export const TextColumn2 = styled(TextColumn)`
  width: 80px;
  padding: 0;
  color: #545454;
`;

export const Amount = styled.div`
  width: 103px;
  letter-spacing: 0.06em;
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 120%;
  color: ${({index}) => (index % 2 === 0 ? midCoinsColor : '#26A95E')};
`;

export const UnlockBtn = styled.button`
  position: relative;
  margin: 0 auto;
  padding: 14px 18px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 28px;
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  color: ${primaryColor};
  border: 1px solid ${primaryColor};
  border-radius: 24px;
  transition: all ease 0.4s;

  svg {
    position: relative;
    left: 5px;

    path {
      transition: all ease 0.4s;
    }
  }

  &:hover {
    color: #fff;
    background-color: ${primaryColor};

    svg {
      path {
        stroke: #fff;
      }
    }
  }
`;

export const RewardDate = styled.div`
  color: #464646;
  font-weight: ${({expired}) => (expired ? 700 : 'normal')};
`;

export const PopupWrapper = styled.div`
  opacity: 0;
  position: absolute;
  top: calc(50% + 3px);
  right: 16px;
  transform: translateY(-50%);
  z-index: 1;
  transition: opacity ease 0.6s;
`;

export const Status = styled.div`
  position: relative;
  margin-left: auto;
  width: 160px;
  font-size: 12px;
  line-height: 130%;
  color: #000;

  &:hover {
    ${PopupWrapper} {
      opacity: 1;
    }
  }
`;

export const PopupHint = styled.div`
  position: absolute;
  left: -165px;
  bottom: -46px;
  opacity: ${({hint}) => (hint ? 1 : 0)};
  padding: 8px 7px;
  width: 188px;
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  line-height: 140%;
  text-align: left;
  color: #000000;
  background: #e5f1ff;
  box-shadow: 0 4px 4px rgba(0, 0, 0, 0.08);
  border-radius: 4px;
  transition: opacity ease 0.6s;
`;

export const PopupBtn = styled(PopupBtnIcon)`
  &:hover {
    cursor: pointer;
  }
`;
