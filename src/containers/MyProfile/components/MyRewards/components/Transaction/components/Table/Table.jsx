import React, {useState} from 'react';
import {arrayOf, shape, oneOfType} from 'prop-types';
// import Scrollbars from 'react-scrollbars-custom';
import moment from 'moment';
import {Link} from 'react-router-dom';
import Skeleton from 'react-loading-skeleton';
import _ from 'lodash';
import NothingFound from 'components/NothingFound';

import {
  Amount,
  IconStyled,
  Image,
  PopupBtn,
  PopupHint,
  Table,
  TextColumn,
  Tr,
  TheadTh,
  Thead,
  TrTransactionDate,
  Status,
  PopupWrapper,
  TextColumn2,
  UnlockBtn,
  RewardDate
} from 'containers/MyProfile/components/MyRewards/components/Transaction/components/Table/styled';
import useGlobal from 'store';
import {ReactComponent as IconUnlockBtn} from 'containers/MyProfile/components/MyRewards/components/Transaction/img/iconUnlockBtn.svg';
import productPlaceholder from 'images/placeholders/product.jpg';

const TransactionTable = ({data, loading, sort, setSort}) => {
  const [, globalActions] = useGlobal();
  const [state, setState] = useState(data);
  const [hint, setHint] = useState(false);

  const sortBy = (key) => {
    if (key === 'type1') {// date
      setSort({
        sortBy: 'AwardedDate',
        orderBy: sort?.orderBy === 'DESC' ? 'ASC' : 'DESC'
      });
    }
  };

  const unlockMidCoins = (productData) => {
    globalActions.setLeaveFeedback({
      open: true,
      data: productData
    });
  };

  const getDates = (items = []) => {
    return [...new Set(items?.map((item) => item?.date)?.sort((a, b) => (a < b ? 1 : -1)))];
  };

  return (
    <Table>
      <Thead>
        <TheadTh onClick={() => sortBy('type1')}>
          Date
          <IconStyled active={+sort.type1} />
        </TheadTh>
        <TheadTh>Store</TheadTh>
        <TheadTh onClick={() => sortBy('type2')}>
          Type
          <IconStyled active={+sort.type2} />
        </TheadTh>
        <TheadTh onClick={() => sortBy('type3')}>
          Amount
          <IconStyled active={+sort.type3} />
        </TheadTh>
        <TheadTh>Status</TheadTh>
        <TheadTh>&nbsp;</TheadTh>
      </Thead>
      {/* <Scrollbars
        clientwidth={4}
        noDefaultStyles={false}
        noScroll={false}
        style={{height: '720px'}}
        thumbYProps={{className: 'thumbY'}}
      > */}
      {loading && (
        <div style={{margin: '0 15px 0 0'}}>
          {[...Array(9).keys()].map((i) => (
            <Skeleton key={i} style={{marginBottom: 5, width: '100%'}} height={70} />
          ))}
        </div>
      )}
      {_.isEmpty(data) && !loading && <NothingFound hideHelpText hideAdditionalText />}
      {getDates(data)?.map((el) => (
        <React.Fragment key={el}>
          <TrTransactionDate>
            <div>{moment(el).format('DD MMM YYYY')}</div>
          </TrTransactionDate>
          {data
            ?.filter((i) => i?.date === el)
            ?.map((innerEl, idx) => {
              const {order_id, store_id, id, awardedProduct, awarded_qty, expiry_date, type, status, expired} = innerEl;
              const product = awardedProduct?.nodes?.[0] || {};
              const seller = product?.seller;

              // if (!awarded_qty) return null;

              return (
                <React.Fragment key={id}>
                  <Tr>
                    <Image to={`/product/${product?.slug}`}>
                      <img src={product?.image?.sourceUrl || productPlaceholder} alt="" />
                    </Image>
                    <TextColumn>
                      <Link to={`/shop/${store_id}/products`}>{seller?.name || '(no name)'}</Link>
                    </TextColumn>
                    <TextColumn2>
                      <div>{type}</div>
                      {/* <div>{type}</div> */}
                    </TextColumn2>
                    <Amount index={idx}>+{awarded_qty}</Amount>
                    <Status>
                      {status === 'Pending' ? (
                        <UnlockBtn
                          onClick={() =>
                            unlockMidCoins({
                              databaseId: product?.databaseId,
                              name: product?.name,
                              image: product?.image?.sourceUrl,
                              price: 0, // need to fix
                              currencySymbol: product?.currencySymbol,
                              orderId: order_id,
                              storeId: store_id,
                              midcoins: awarded_qty, // need to fix
                              shopcoins: awarded_qty // need to fix
                            })
                          }
                        >
                          Unlock
                          <IconUnlockBtn />
                        </UnlockBtn>
                      ) : (
                        <>
                          <div>Valid Until</div>
                          <RewardDate expired={expired}>{expiry_date}</RewardDate>
                          <PopupWrapper>
                            <PopupBtn onClick={() => setHint(!hint)} />
                            <PopupHint hint={hint}>Available for redemption during This period of time.</PopupHint>
                          </PopupWrapper>
                        </>
                      )}
                    </Status>
                  </Tr>
                </React.Fragment>
              );
            })}
        </React.Fragment>
      ))}
      {/* </Scrollbars> */}
    </Table>
  );
};

TransactionTable.defaultProps = {};

TransactionTable.propTypes = {
  data: arrayOf(oneOfType([shape([])])).isRequired
};

export default TransactionTable;
