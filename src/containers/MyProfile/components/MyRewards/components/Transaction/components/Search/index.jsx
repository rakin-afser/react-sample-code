import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Tags from 'containers/MyProfile/components/MyRewards/components/Transaction/components/Tags/Tags.jsx';
import {Wrapper, Input, SearchBtn, AdvancedBtn, InputHolder, InputWrapper, TagsHolder} from './styled';

const Search = ({placeholder, setSearch, search, onSearch = () => {}}) => {
  const [advancedSearch, setAdvancedSearch] = useState(false);

  const onChange = (e) => {
    setSearch(e.target.value);
  };

  return (
    <Wrapper>
      <InputHolder>
        <InputWrapper>
          <Input placeholder={placeholder} value={search} onChange={onChange} />
          <SearchBtn onClick={onSearch} />
        </InputWrapper>
        <AdvancedBtn onClick={() => setAdvancedSearch(!advancedSearch)}>{advancedSearch ? 'Hide' : 'Show'}</AdvancedBtn>
      </InputHolder>
      <TagsHolder advancedSearch={advancedSearch}>
        <Tags />
      </TagsHolder>
    </Wrapper>
  );
};

Search.defaultProps = {
  placeholder: 'Type something to search...',
  onSearch: ''
};

Search.propTypes = {
  placeholder: PropTypes.string,
  onSearch: PropTypes.func
};

export default Search;
