import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 10px;
`;

export const Row = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  margin-bottom: 24px;

  span {
    width: 67px;
    font-weight: 500;
    font-size: 14px;
    color: #464646;
  }
`;

export const Tag = styled.button`
  margin-right: 25px;
  padding: 0 22px;
  font-size: 12px;
  font-weight: normal;
  border: 1px solid #cccccc;
  box-sizing: border-box;
  border-radius: 24px;
  height: 28px;
  color: #545454;
  transition: all ease 0.6s;

  &:last-child {
    margin-right: 0;
  }

  &:hover {
    background: rgba(228, 228, 228, 0.48);
  }

  &.active {
    background: #e4e4e4;
    color: #464646;
    font-weight: 500;
  }
`;
