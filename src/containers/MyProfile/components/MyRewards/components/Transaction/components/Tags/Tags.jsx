import React from 'react';
import {Row, Tag, Wrapper} from './styled';

const types = ['Review', 'List', 'Referral', 'Shop Coins'];
const statuses = ['Available', 'Unlock', 'Expire'];

const Tags = () => {
  const getTagData = (e) => {
    e.target.classList.toggle('active');
  };

  return (
    <Wrapper>
      <Row>
        <span>Type:</span>
        {types.map((el, i) => (
          <Tag onClick={getTagData} key={i}>
            {el}
          </Tag>
        ))}
      </Row>
      <Row>
        <span>Status:</span>
        {statuses.map((el, i) => (
          <Tag onClick={getTagData} key={i}>
            {el}
          </Tag>
        ))}
      </Row>
    </Wrapper>
  );
};

export default Tags;
