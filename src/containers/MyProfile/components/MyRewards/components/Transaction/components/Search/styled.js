import styled from 'styled-components/macro';
import {ReactComponent as LensIcon} from './img/lensIcon.svg';

export const Wrapper = styled.div`
  margin-bottom: 16px;
  padding: 10px 16px 15px;
  background: #ffffff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.03);
  border-radius: 4px;
`;

export const InputHolder = styled.div`
  padding-left: 9px;
  padding-right: 25px;
  display: flex;
  justify-content: space-between;
`;

export const InputWrapper = styled.div`
  position: relative;
  width: 84%;
`;

export const Input = styled.input`
  padding: 0 56px 0 16px;
  font-family: 'SF Pro Display', sans-serif;
  width: 100%;
  height: 40px;
  background: #ffffff;
  border: 1px solid rgba(84, 84, 84, 0.32);
  border-radius: 4px;
  font-size: 14px;
  line-height: 140%;
  color: #a7a7a7;
  transition: all ease 1.5s;
`;

export const SearchBtn = styled(LensIcon)`
  position: absolute;
  right: 16px;
  top: 50%;
  transform: translateY(-50%);
  cursor: pointer;
  transition: all ease 0.4s;
`;

export const AdvancedBtn = styled.button`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  text-align: center;
  color: #ea2c34;
  transition: ease color 0.6s;

  &:hover {
    color: #000000;
  }
`;

export const TagsHolder = styled.div`
  padding-top: 14px;
  max-height: 0;
  overflow: hidden;
  transition: all ease 0.8s;

  ${({advancedSearch}) =>
    advancedSearch && {
      maxHeight: '1000px'
    }}
`;
