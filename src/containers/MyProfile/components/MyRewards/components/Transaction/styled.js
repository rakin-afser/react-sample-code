import styled from 'styled-components/macro';
import {mainWhiteColor} from 'constants/colors';

export const Wrapper = styled.div`
  padding: 26px 3px 26px 18px;
  background: ${mainWhiteColor};
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
`;
