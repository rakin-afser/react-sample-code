import React from 'react';
import FadeOnMount from 'components/Transitions';
import {Wrapper} from 'containers/MyProfile/components/MyRewards/components/Transaction/styled';
import TransactionTable from 'containers/MyProfile/components/MyRewards/components/Transaction/components/Table/Table';
import Search from 'containers/MyProfile/components/MyRewards/components/Transaction/components/Search';

const Transaction = ({data, loading, setSearch, search, onSearch, ...props}) => {
  return (
    <FadeOnMount>
      <Search onSearch={onSearch} setSearch={setSearch} search={search} />
      <Wrapper>
        <TransactionTable search={search} data={data} loading={loading} {...props}/>
      </Wrapper>
    </FadeOnMount>
  );
};

export default Transaction;
