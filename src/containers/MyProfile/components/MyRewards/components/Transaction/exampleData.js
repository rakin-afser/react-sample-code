import img1 from './img/1.jpg';
import img2 from './img/2.jpg';
import img3 from './img/3.jpg';
import img4 from './img/4.jpg';
import img5 from './img/5.jpg';
import img6 from './img/6.jpg';

const exampleData = [
  {
    transactionDate: '24 Dec 2019',
    data: [
      {
        id: 0,
        image: img1,
        store: 'Fenty Beauty',
        type: 'MidCoins',
        amount: '8',
        status: false,
        rewardDate: '24 Dec 2019',
        expired: false
      },
      {
        id: 1,
        image: img2,
        store: 'Jeffree Star Cosmetics',
        type: 'Referral',
        amount: '98',
        status: 'Valid Until',
        rewardDate: '24 Dec 2019',
        expired: false
      },
      {
        id: 2,
        image: img3,
        store: 'Fenty Beauty',
        type: 'List',
        amount: '16',
        status: 'Valid Until',
        rewardDate: '24 Dec 2019',
        expired: false
      },
      {
        id: 3,
        image: img4,
        store: 'Tati Beauty',
        type: 'MidCoins',
        amount: '87',
        status: 'Expiring soon',
        rewardDate: '24 Dec 2019',
        expired: true
      },
      {
        id: 4,
        image: img4,
        store: 'Tati Beauty',
        type: 'ShopCoins',
        amount: '22',
        status: 'Expiring soon',
        rewardDate: '24 Dec 2019',
        expired: true
      }
    ]
  },
  {
    transactionDate: '23 Dec 2019',
    data: [
      {
        id: 0,
        image: img5,
        store: 'Chanel',
        type: 'MidCoins',
        amount: '8',
        status: false,
        rewardDate: '24 Dec 2019',
        expired: false
      },
      {
        id: 1,
        image: img6,
        store: 'Chanel',
        type: 'ShopCoins',
        amount: '16',
        status: false,
        rewardDate: '24 Dec 2019',
        expired: false
      },
      {
        id: 2,
        image: img3,
        store: 'Tati Beauty',
        type: 'List',
        amount: '38',
        status: 'Expiring soon',
        rewardDate: '24 Dec 2019',
        expired: true
      }
    ]
  },
  {
    transactionDate: '22 Dec 2019',
    data: [
      {
        id: 0,
        image: img5,
        store: 'Chanel',
        type: 'MidCoins',
        amount: '8',
        status: false,
        rewardDate: '24 Dec 2019'
      },
      {
        id: 1,
        image: img6,
        store: 'Chanel',
        type: 'ShopCoins',
        amount: '16',
        status: false,
        rewardDate: '24 Dec 2019',
        expired: true
      },
      {
        id: 2,
        image: img3,
        store: 'Tati Beauty',
        type: 'List',
        amount: '38',
        status: 'Expiring soon',
        rewardDate: '24 Dec 2019',
        expired: true
      }
    ]
  },
  {
    transactionDate: '21 Dec 2019',
    data: [
      {
        id: 0,
        image: img5,
        store: 'Chanel',
        type: 'MidCoins',
        amount: '8',
        status: false,
        rewardDate: '24 Dec 2019',
        expired: false
      },
      {
        id: 1,
        image: img6,
        store: 'Chanel',
        type: 'ShopCoins',
        amount: '16',
        status: false,
        rewardDate: '24 Dec 2019',
        expired: false
      },
      {
        id: 2,
        image: img3,
        store: 'Tati Beauty',
        type: 'List',
        amount: '38',
        status: 'Expiring soon',
        rewardDate: '24 Dec 2019',
        expired: false
      }
    ]
  }
];

export default exampleData;
