import React from 'react';
import {Link} from 'react-router-dom';
import Content from 'containers/MyProfile/components/MyRewards/components/ShopCoins/styled';
import {recentlyViewed, deals, defaultShops} from 'constants/staticData';
import Slider from 'containers/MyProfile/components/MyRewards/components/ShopCoins/Components/Slider';
import FadeOnMount from 'components/Transitions';
import CardNewArrival from 'components/CardNewArrival';

const ShopCoins = () => {
  function getSlides(data) {
    return data.map((arrival, index) => (
      <Link to="/product">
        <CardNewArrival key={index} isLiked={false} isWished={false} {...arrival} />
      </Link>
    ));
  }

  return (
    <FadeOnMount>
      <Content>
        {/* It should be removed */}
        {/*<Slider title="Recently Viewed">{getSlides(deals)}</Slider>*/}
        <Slider title="Chanel Beauty" coins={298} linkName="Visit Store" linkPath="/shop">
          {getSlides(recentlyViewed)}
        </Slider>
        <Slider title="Escada" coins={1234} linkName="Visit Store" linkPath="/shop">
          {getSlides(defaultShops)}
        </Slider>
      </Content>
    </FadeOnMount>
  );
};

export default ShopCoins;
