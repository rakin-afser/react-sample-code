import React from 'react';
import {node, string, number} from 'prop-types';
import {
  Row,
  Header,
  Title,
  WithSliderStyled,
  LinkStyled,
  HeaderLeft,
  Coins
} from 'containers/MyProfile/components/MyRewards/components/ShopCoins/Components/styled';
import {ReactComponent as LinkArrow} from './img/arrowRight.svg';
import Icons from 'components/Icon';

const Slider = ({children, title, coins, linkName, linkPath}) => {
  return (
    <Row>
      <Header>
        <HeaderLeft>
          <Title>{title}</Title>
          {coins && <Icons type="coinsProfile" width={28} height={28} />}
          <Coins>{coins}</Coins>
        </HeaderLeft>
        {linkPath && (
          <LinkStyled to={linkPath}>
            {linkName}
            <LinkArrow />
          </LinkStyled>
        )}
      </Header>
      <WithSliderStyled withHeader={false} marginTop={0} slidesToScroll={2} infinite={false} slidesToShow={2} dots>
        {children}
      </WithSliderStyled>
    </Row>
  );
};

Slider.propTypes = {
  children: node.isRequired,
  title: string.isRequired,
  coins: number,
  linkName: string,
  linkPath: string
};

Slider.defaultProps = {
  coins: null,
  linkName: null,
  linkPath: null
};

export default Slider;
