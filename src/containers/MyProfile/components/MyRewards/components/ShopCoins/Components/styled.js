import styled from 'styled-components/macro';
import WithSlider from 'components/WithSlider';
import {Link} from 'react-router-dom';
import {mainWhiteColor, primaryColor, secondaryTextColor} from 'constants/colors';
import {FlexContainer} from 'globalStyles';
import media from 'constants/media';

export const Row = styled.div`
  margin-bottom: 47px;
  background: ${mainWhiteColor};
  padding: 16px 0 0 0;
`;

export const Header = styled(FlexContainer)`
  margin-left: 57px;
  margin-right: 55px;
  padding-bottom: 7px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 2px ${primaryColor} solid;

  @media (max-width: ${media.mobileMax}) {
    border-bottom: 0;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    padding: 0 16px 21px;
  }
`;

export const HeaderLeft = styled.div`
  display: flex;
  align-items: center;
`;

export const Coins = styled.div`
  margin-left: 4px;
  font-weight: 500;
  font-size: 16px;
  letter-spacing: 0.016em;
  color: #398287;
`;

export const LinkStyled = styled(Link)`
  display: flex;
  align-items: center;
  font-size: 14px;
  text-align: right;
  color: #333333;

  & svg {
    margin-left: 10px;

    path {
      transition: fill 0.3s;
    }
  }

  &:hover {
    color: ${primaryColor};

    svg path {
      fill: ${primaryColor};
    }
  }
`;

export const Title = styled.div`
  margin-right: 18px;
  margin-bottom: 4px;
  font-size: 24px;
  line-height: 1;
  letter-spacing: 0.5px;
  color: ${secondaryTextColor};
  font-weight: bold;

  @media (max-width: ${media.mobileMax}) {
    font-style: normal;
    font-weight: 700;
    font-size: 18px;
    line-height: 21px;
    color: #343434;
  }
`;

export const WithSliderStyled = styled(WithSlider)`
  & .slick-prev {
    left: -24px;
  }

  & .slick-next {
    right: -14px;
  }

  & .slick-slide {
    & .CardNewArrival__card {
      width: 200px;
      box-shadow: 0 2px 6px rgb(0 0 0 / 10%);

      img {
        width: 200px;
        height: 200px;
        object-fit: cover;
      }
    }
  }
`;
