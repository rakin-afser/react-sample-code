import styled from 'styled-components';

const Content = styled.div`
  padding-top: 10px;
  width: 100%;
`;

export default Content;
