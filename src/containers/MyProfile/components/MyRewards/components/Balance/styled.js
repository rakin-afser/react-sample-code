import {mainBlackColor, midCoinsColor} from 'constants/colors';
import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';

export const Heading = styled.div`
  padding: 13px 28px;
  background: #ffffff;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.03);
  border-radius: 4px;
`;

export const Title = styled.h3`
  font-weight: 700;
  font-size: 14px;
  line-height: 1.4;
  color: ${mainBlackColor};
  margin-bottom: 9px;
`;

export const Desc = styled.p`
  color: #464646;
  font-size: 11px;
  max-width: 180px;
`;

export const BalanceAmount = styled.span`
  display: block;
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 24px;
  line-height: 1.32;
  letter-spacing: -0.024em;
  color: ${mainBlackColor};
  margin-bottom: 7px;
`;

export const BalanceWrapper = styled.div`
  text-align: right;
`;

export const TableWrapper = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  padding: 17px 28px;
  margin-top: 5px;

  .ant-table {
    color: #545454;

    &-thead > tr > th {
      background-color: transparent;
      font-size: 14px;

      .ant-table-column-sorter .ant-table-column-sorter-inner .ant-table-column-sorter-down.on,
      .ant-table-column-sorter .ant-table-column-sorter-inner .ant-table-column-sorter-up.on {
        color: ${mainBlackColor};
      }
    }

    &-body {
      font-size: 12px;
      line-height: 1.3;
    }

    &-thead > tr > th {
      &:first-child {
        padding-left: 0;
      }

      &:last-child {
        padding-right: 0;
      }
    }

    &-tbody > tr {
      height: 75px;
    }

    &-tbody > tr > td {
      &:first-child {
        padding-left: 0;
      }

      &:last-child {
        padding-right: 0;
      }
    }
  }
`;

export const Amount = styled.strong`
  color: ${midCoinsColor};
  font-size: 14px;
`;

export const Status = styled.strong`
  color: ${mainBlackColor};
`;
