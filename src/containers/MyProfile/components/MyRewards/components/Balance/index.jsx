import React from 'react';
import {Col, Row, Table} from 'antd';
import {usetestSampleBalanceQuery, useWalletBalanceQuery} from 'hooks/queries';
import moment from 'moment';
import {Heading, Title, Desc, Amount, Status, TableWrapper, BalanceAmount, BalanceWrapper} from './styled';
import Btn from 'components/Btn';
import {useUser} from 'hooks/reactiveVars';
import {useOnScreen} from 'hooks';

const Balance = () => {
  const [user] = useUser();
  const {data: dataWallet} = useWalletBalanceQuery({variables: {id: user?.databaseId}});
  const {walletBalance} = dataWallet?.user || {};

  const columns = [
    {
      title: 'Date',
      dataIndex: 'date',
      sorter: (a, b) => moment(a?.date) - moment(b?.date),
      render: (a) => moment(a).format('DD/MM/YYYY')
    },
    {title: 'Store', dataIndex: 'storeName'},
    {title: 'Type', dataIndex: 'type', sorter: (a, b) => a?.type.localeCompare(b?.type)},
    {
      title: 'Amount',
      dataIndex: 'awardedQty',
      sorter: (a, b) => a?.awardedQty - b?.awardedQty,
      render: (a) => <Amount>US ${a?.toFixed(2)}</Amount>
    },
    {title: 'Status', dataIndex: 'status', render: (r) => <Status>{r}</Status>}
  ];

  const {data, loading, error, fetchMore} = usetestSampleBalanceQuery({
    variables: {first: 10},
    notifyOnNetworkStatusChange: true
  });
  const {endCursor, hasNextPage} = data?.testSampleBalanceData?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first: 10,
          after: endCursor
        }
      });
    }
  });
  const endRefEl = hasNextPage && !loading ? <div ref={ref} /> : null;

  const settings = {
    rowKey: (r) => r.id,
    pagination: false,
    fixed: true,
    columns,
    loading,
    dataSource: error ? null : data?.testSampleBalanceData?.nodes
  };

  return (
    <>
      <Heading>
        <Row>
          <Col span={12}>
            <Title>Your Balance</Title>
            <Desc>
              You must have a minimum balance of <strong>USD 50</strong> to make a withdrawal
            </Desc>
          </Col>
          <Col span={12}>
            <BalanceWrapper>
              {walletBalance ? <BalanceAmount>USD {walletBalance}</BalanceAmount> : null}
              <Btn kind="pr-bare" ml="auto">
                Withdraw
              </Btn>
            </BalanceWrapper>
          </Col>
        </Row>
      </Heading>
      <TableWrapper>
        <Table {...settings} />
        {endRefEl}
      </TableWrapper>
    </>
  );
};

export default Balance;
