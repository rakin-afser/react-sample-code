import styled from 'styled-components/macro';
import {Tab} from 'containers/MyProfile/styled';
import {secondaryTextColor} from 'constants/colors';

export const Wrapper = styled.div`
  background-color: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
`;

export const DashboardTitle = styled.h5`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 150%;
  color: #3a3a3a;
  margin-bottom: 1px;

  span {
    color: #398287;
  }
`;

export const DashboardSubtitle = styled(DashboardTitle)`
  color: #545454;
  margin-bottom: 14px;
`;

export const DashboardStats = styled.ul`
  display: flex;
  justify-content: space-between;

  li {
    color: #656565;
    font-weight: 500;
    font-size: 14px;

    &:last-child {
      font-weight: 700;
    }
  }
`;

export const DashboardRow1 = styled.div`
  padding: 24px 27px 11px;
`;

export const DashboardRow2 = styled.div`
  padding: 16px 0 11px;
  background-color: rgba(#efefef, 0.3);
`;

export const DashboardRow3 = styled.div`
  padding-bottom: 20px;
`;

export const TabsLevel3Wrapper = styled.div`
  margin-bottom: 20px;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  width: 282px;
  margin-left: 77px;
`;

export const TabLevel2 = styled(Tab)`
  font-size: 14px;
  &:first-child,
  &:last-child {
    margin: 0;
  }

  &:hover {
    border-bottom-color: #ed484f;
  }

  ${({active}) =>
    active
      ? {
          borderBottom: '2px solid #ED484F'
        }
      : null}
`;

export const TabLevel3 = styled(Tab)`
  margin: 4px 0 0 0;
  font-size: 14px;

  &:first-child,
  &:last-child {
    margin: 0;
  }
`;

export const TotalEarned = styled.div`
  margin-right: 23px;
  margin-bottom: 9px;
  font-weight: bold;
  font-size: 10px;
  text-align: right;
  color: ${secondaryTextColor};
`;
