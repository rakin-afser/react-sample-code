import React, {useState} from 'react';
import {useQuery} from '@apollo/client';
import moment from 'moment';

import {
  DashboardRow1,
  DashboardRow2,
  DashboardRow3,
  DashboardStats,
  DashboardSubtitle,
  DashboardTitle,
  TabLevel2,
  TabLevel3,
  TabsLevel3Wrapper,
  TotalEarned,
  Wrapper
} from 'containers/MyProfile/components/MyRewards/components/Rewards/styled';
import {Nav} from 'containers/MyProfile/styled';
import Chart from 'containers/MyProfile/components/MyRewards/components/Chart';
import FadeOnMount from 'components/Transitions';
import {useUser} from 'hooks/reactiveVars';
import {GET_REWARDS_STATISTICS} from 'containers/MyProfile/api/queries';

const tabs = ['Week', 'Month', 'Year'];
const innerTabs = ['Review', 'Lists', 'Referral'];


const Rewards = ({data, loading, error}) => {
  const [user] = useUser();
  const [activeTabIndex, setActiveTabIndex] = useState(0);
  const [activeInnerTabIndex, setActiveInnerTabIndex] = useState(0);
  const {data: rewardsData} = useQuery(GET_REWARDS_STATISTICS, {variables: {userId: user?.databaseId}});

  const earnedMidcoinsInMonth = rewardsData?.rewardsData?.earnedMidcoinsInMonth;
  const earnedMidcoinsInWeek = rewardsData?.rewardsData?.earnedMidcoinsInWeek;
  const earnedMidcoinsInYear = rewardsData?.rewardsData?.earnedMidcoinsInYear;
  const currentDates =
    activeTabIndex === 0 ? earnedMidcoinsInWeek : activeTabIndex === 1 ? earnedMidcoinsInMonth : earnedMidcoinsInYear;
  const currentPeriod = activeTabIndex === 0 ? 7 : activeTabIndex === 1 ? 30 : 365;

  const getDates = (items = [], timeLine) => {
    const dates = items?.map((item) => ({date: moment(item?.date).format('DD MMM YYYY'), amt: item?.midcoins}));

    return timeLine
      ? timeLine?.map((item) => ({
          date: item,
          amt:
            dates?.find((i) => {
              return i.date === item;
            })?.amt || 0
        }))
      : dates;
  };

  const getTimeLine = (days = 7) => {
    return [...Array(days).keys()]
      .map((item) =>
        moment()
          .subtract(item, 'days')
          .format('DD MMM YYYY')
      )
      .reverse();
  };

  const currentDate = getDates(currentDates?.inDays, getTimeLine(currentPeriod)) || [];

  return (
    <FadeOnMount>
      <Wrapper>
        <DashboardRow1>
          <DashboardTitle>
            You have earned <span>{user?.availableMidcoins} Midcoins.</span>
          </DashboardTitle>
          <DashboardSubtitle>Start shopping and redeem your points !</DashboardSubtitle>
          <DashboardStats>
            <li>List: {0}</li>
            <li>Review: {user?.availableMidcoins}</li>
            <li>Referrals: {0}</li>
            <li>Total: {user?.availableMidcoins}</li>
          </DashboardStats>
        </DashboardRow1>
        <DashboardRow2>
          <Nav
            style={{
              boxShadow: 'none',
              backgroundColor: 'rgba(239, 239, 239, 0.3)',
              justifyContent: 'space-around'
            }}
          >
            {tabs.map((tab, i) => (
              <TabLevel2 active={activeTabIndex === i} key={i} onClick={() => setActiveTabIndex(i)}>
                {tab}
              </TabLevel2>
            ))}
          </Nav>
        </DashboardRow2>
        <DashboardRow3>
          <TabsLevel3Wrapper>
            {innerTabs.map((tab, i) => (
              <TabLevel3 active={activeInnerTabIndex === i} key={i} onClick={() => setActiveInnerTabIndex(i)}>
                {tab}
              </TabLevel3>
            ))}
          </TabsLevel3Wrapper>
          <TotalEarned>Total Midcoins Earned ${currentDates?.total}</TotalEarned>
          <Chart data={currentDate} />
        </DashboardRow3>
      </Wrapper>
    </FadeOnMount>
  );
};

export default Rewards;
