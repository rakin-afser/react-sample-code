import React from 'react';
import {RightPanel, Wrapper} from 'containers/MyProfile/components/MyRewards/components/RightSidebar/styled';
import ExpansionPanel from 'containers/MyProfile/components/MyRewards/components/RightSidebar/components/ExpansionPanel';
import ReferralPanel from 'containers/MyProfile/components/MyRewards/components/RightSidebar/components/ReferralPanel';
import InformPanel from 'containers/MyProfile/components/MyRewards/components/RightSidebar/components/InformPanel';
import {useUser} from 'hooks/reactiveVars';
import {useAffiliateLinkQuery} from 'hooks/queries';
import Loader from 'components/Loader';

const informPanelData = [
  {
    id: 1,
    count: 1,
    text: 'For each $10 spent on testSample'
  },
  {
    id: 2,
    count: 10,
    text: 'When someone purchases made  through your list'
  },
  {
    id: 3,
    count: 5,
    text: 'When someone joins through your referral link'
  }
];

const RightSidebar = () => {
  const [user] = useUser();
  const {data, loading} = useAffiliateLinkQuery({variables: {id: user?.databaseId}});
  const {affiliateLink} = data?.user || {};

  function renderAffiliate() {
    if (loading) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

    return (
      <ReferralPanel
        title="Referral"
        description="Share this to your friend, earn coins for both!"
        link={affiliateLink}
      />
    );
  }

  return (
    <Wrapper>
      <RightPanel>
        <ExpansionPanel title="What is Midcoins?">
          <p>
            Unicode is an international not-for-profit organisation that started in the 1980s as an effort to "unify"
            the "codes" for textual characters used in the computing industry. By "code", I just mean a number.
            Computers only understand numbers, visualise them on which character.
          </p>
        </ExpansionPanel>
      </RightPanel>
      <RightPanel>
        <InformPanel title="What is Midcoin?" footer="10 midcoins = £1.00" data={informPanelData} />
      </RightPanel>
      <RightPanel>{renderAffiliate()}</RightPanel>
    </Wrapper>
  );
};

export default RightSidebar;
