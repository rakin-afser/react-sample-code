import React from 'react';
import PropTypes from 'prop-types';
import {Title, Item, ItemNum, ItemTxt, Footer} from './styled';

const InformPanel = ({title, footer, data}) => {
  return (
    <>
      <Title>{title}</Title>
      <div>
        {data.map((el, idx) => (
          <Item key={idx}>
            <ItemNum>+{el.count}</ItemNum>
            <ItemTxt>{el.text}</ItemTxt>
          </Item>
        ))}
      </div>
      <Footer>{footer}</Footer>
    </>
  );
};

InformPanel.propTypes = {
  title: PropTypes.string.isRequired,
  footer: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default InformPanel;
