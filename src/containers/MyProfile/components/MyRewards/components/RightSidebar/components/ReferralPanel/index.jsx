import React, {useState} from 'react';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import PropTypes from 'prop-types';
import {Button, Input, PanelTitle} from 'containers/MyProfile/components/MyRewards/components/RightSidebar/styled';

const ReferralPanel = ({title, description, link}) => {
  const [copied, setCopied] = useState(false);

  const onMouseLeave = () => {
    setTimeout(() => setCopied(false), 3000);
  };

  return (
    <>
      <PanelTitle>{title}</PanelTitle>
      <p>{description}</p>
      <Input value={link} disabled />
      <CopyToClipboard onCopy={() => setCopied(true)} text={`${window.location.origin}/ref/${link}`}>
        <Button onMouseLeave={onMouseLeave}>{copied ? `Copied` : `Copy`}</Button>
      </CopyToClipboard>
    </>
  );
};

ReferralPanel.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired
};

export default ReferralPanel;
