import styled from 'styled-components';
import {headerShadowColor, midCoinsColor} from 'constants/colors';

export const Title = styled.div`
  margin-bottom: 6px;
  font-weight: 500;
  font-size: 18px;
  letter-spacing: -0.024em;
  color: ${midCoinsColor};
`;

export const Item = styled.div`
  margin-left: -21px;
  margin-right: -21px;
  padding-left: 21px;
  padding-right: 21px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  min-height: 52px;
  border-bottom: 1px solid ${headerShadowColor};
`;

export const ItemNum = styled.div`
  font-size: 14px;
  line-height: 140%;
  color: #464646;
`;

export const ItemTxt = styled.div`
  max-width: 192px;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  text-align: right;
  color: #000000;
`;

export const Footer = styled.div`
  padding-top: 16px;
  padding-bottom: 5px;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;
