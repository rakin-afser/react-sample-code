import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {
  ArrowIconWrapper,
  PanelHidden,
  PanelTitle,
  PanelVisible
} from 'containers/MyProfile/components/MyRewards/components/RightSidebar/styled';
import {ReactComponent as ArrowIcon} from 'containers/MyProfile/components/MyRewards/components/RightSidebar/img/arrow.svg';

const ExpansionPanel = ({title, children}) => {
  const [openPanel, setOpenPanel] = useState(false);

  const showMore = () => {
    setOpenPanel(!openPanel);
  };

  return (
    <>
      <PanelVisible onClick={showMore}>
        <PanelTitle>{title}</PanelTitle>
        <ArrowIconWrapper openPanel={openPanel}>
          <ArrowIcon />
        </ArrowIconWrapper>
      </PanelVisible>
      <PanelHidden openPanel={openPanel}>{children}</PanelHidden>
    </>
  );
};

ExpansionPanel.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default ExpansionPanel;
