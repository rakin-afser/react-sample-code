import styled from 'styled-components/macro';
import {grayBackroundColor, grayTextColor, mainBlackColor, secondaryTextColor} from 'constants/colors';

export const Wrapper = styled.div`
  position: sticky;
  top: 34px;
  width: 292px;
`;

export const RightPanel = styled.div`
  margin-bottom: 16px;
  padding: 23px 21px 17px;
  background: #ffffff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;

  p {
    margin-bottom: 12px;
    color: #8f8f8f;
    font-size: 12px;
  }
`;

export const PanelTitle = styled.h5`
  margin-bottom: 0;
  font-weight: 500;
  font-size: 18px;
  letter-spacing: -0.024em;
  color: ${secondaryTextColor};
`;

export const ArrowIconWrapper = styled.div`
  position: relative;
  top: 0;
  right: -7px;
  transform: rotate(0);
  transition: all ease 0.2s;

  ${({openPanel}) =>
    openPanel && {
      transform: 'rotate(90deg)',
      right: '-9px',
      top: '-4px'
    }}
`;

export const PanelVisible = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`;

export const PanelHidden = styled.div`
  max-height: 0;
  transition: max-height ease-out 0.8s;
  overflow: hidden;

  p {
    padding: 14px 3px 0;
    font-family: 'Helvetica Neue', sans-serif;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 17px;
    color: #666666;
  }

  ${({openPanel}) =>
    openPanel && {
      maxHeight: '1000px',
      transition: 'max-height ease-in 0.6s'
    }}
`;

export const Input = styled.input`
  width: 100%;
  margin-bottom: 16px;
  padding: 9px;
  background: #ffffff;
  border: 1px solid #c3c3c3;
  border-radius: 5px;
  //font-family:' Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #464646;
`;

export const Button = styled.div`
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  width: 99px;
  height: 32px;
  font-size: 14px;
  line-height: 140%;
  color: ${grayTextColor};
  border: 1px solid #c3c3c3;
  box-sizing: border-box;
  border-radius: 24px;
  border: 1px solid ${grayBackroundColor};
  overflow: hidden;
  transition: all ease 0.4s;
  cursor: pointer;

  &:hover {
    color: ${mainBlackColor};
    border-color: ${secondaryTextColor};
  }
`;
