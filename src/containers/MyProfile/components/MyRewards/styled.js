import styled from 'styled-components/macro';

export const Content = styled.div`
  display: flex;
  align-items: flex-start;
  width: 100%;
  max-width: 898px;
`;

export const ContentMain = styled.div`
  width: 576px;
  margin: 0 15px;
`;

export const TabsWrapper = styled.div`
  margin-bottom: 16px;
  z-index: 10;
`;
