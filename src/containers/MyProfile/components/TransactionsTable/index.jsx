import React from 'react';
import PropTypes from 'prop-types';
import {Table, DatePicker} from 'antd';

import {Wrapper, Header, HeaderLeft, GoBtn, ExportBtn} from './styled';
import Icons from 'components/Icon';

const columns = [
  {
    title: 'Seller Name',
    dataIndex: 'sellerName',
    render: (text) => <a>{text}</a>
  },
  {
    title: 'Campaign Status',
    dataIndex: 'campaignStatus'
  },
  {
    title: 'Start Date',
    dataIndex: 'startDate'
  },
  {
    title: 'End Date',
    dataIndex: 'endDate'
  },
  {
    title: 'Total Sales',
    dataIndex: 'totalSales'
  },
  {
    title: 'Amount Earned',
    dataIndex: 'amountEarned'
  },
  {
    title: 'Amount Pending',
    dataIndex: 'amountPending'
  }
];

const data = [];
for (let i = 0; i < 46; i += 1) {
  data.push({
    key: i,
    sellerName: `Edward King ${i}`,
    campaignStatus: 32,
    startDate: `London, Park Lane no. ${i}`,
    endDate: `London, Park Lane no. ${i}`,
    totalSales: `London, Park Lane no. ${i}`,
    amountEarned: `London, Park Lane no. ${i}`,
    amountPending: `London, Park Lane no. ${i}`
  });
}

const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
  },
  getCheckboxProps: (record) => ({
    disabled: record.name === 'Disabled User',
    // Column configuration not to be checked
    name: record.name
  })
};

const TransactionsTable = () => {
  const {RangePicker} = DatePicker;

  return (
    <Wrapper>
      <Header>
        <HeaderLeft>
          <RangePicker />
          <GoBtn>Go</GoBtn>
        </HeaderLeft>
        <ExportBtn>
          <Icons type="export" />
          Export
        </ExportBtn>
      </Header>
      <Table
        rowSelection={{
          type: 'checkbox',
          ...rowSelection
        }}
        columns={columns}
        dataSource={data}
      />
    </Wrapper>
  );
};

TransactionsTable.propTypes = {};

export default TransactionsTable;
