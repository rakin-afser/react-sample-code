import styled from 'styled-components';

export const Wrapper = styled.div``;

export const Header = styled.div`
  display: flex;
  align-items: center;
`;

export const HeaderLeft = styled.div`
  display: flex;
  align-items: center;
`;

export const GoBtn = styled.button`
  padding: 6px 18px;
  font-weight: 500;
  font-size: 14px;
  display: flex;
  align-items: center;
  color: #ffffff;
  background-color: #000;
  border-radius: 24px;
`;

export const ExportBtn = styled.button`
  margin-left: auto;
  display: flex;
  align-items: center;
  border: 1px solid #666666;
  border-radius: 24px;

  i {
    svg {
      margin-right: 2px;
    }
  }
`;
