import React from 'react';
import Grid from 'components/Grid';
import StarRating from 'components/StarRating';
import {Container, Logo, About, Name, RatingWrapper, Btn} from './styled';
import shopIcon from './img/shop_icon.png';

const exampleData = {
  shopIcon,
  name: 'Ralph Lauren',
  rating: 5,
  rates: '602'
};

export default function({name, image, onClick = () => {}}) {
  return (
    <Container>
      <Grid>
        <Logo>
          <img src={image} alt="shop" />
        </Logo>
        <About>
          <Name>{name}</Name>
          <RatingWrapper>
            <StarRating stars={exampleData.rating} starWidth="12px" starHeight="12px" />
            {exampleData.rates ? <span>({exampleData.rates})</span> : null}
          </RatingWrapper>
        </About>
        <Btn onClick={onClick}>Visit Store</Btn>
      </Grid>
    </Container>
  );
}
