import styled from 'styled-components';
import {secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const Container = styled.div`
  padding: 28px 26px 25px 23px;
`;

export const About = styled.div`
  margin-right: auto;
`;

export const Logo = styled.div`
  background-color: #e4e4e4;
  margin: 0 12px 0 0;
  overflow: hidden;

  & img {
    width: 36px;
    height: 36px;
    border-radius: 50%;
    object-fit: cover;
  }
`;

export const Name = styled.h5`
  display: block;
  font-family: Helvetica, sans-serif;
  font-size: 14px;
  margin: 0;
`;

export const RatingWrapper = styled.div`
  display: flex;

  svg {
    margin: 0 4px 0 0;
  }

  span {
    margin-left: 4px;
    font-family: ${secondaryFont};
    font-size: 12px;
    color: #7a7a7a;
  }
`;

export const Btn = styled.button`
  padding: 1px 9px;
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #545454;
  border-radius: 24px;
  color: #545454;
  font-size: 11px;
  transition: ease 0.4s;

  &:hover {
    background-color: ${primaryColor};
    border-color: ${primaryColor};
    color: #fff;
  }
`;
