import React from 'react';
import parse from 'html-react-parser';
import {useTranslation} from 'react-i18next';
import ArrowIcon from 'assets/Arrow';
import ctaBg from 'assets/sellerLandingImages/ctaMobBg.svg';
import ctaImg from 'assets/sellerLandingImages/ctaImg.svg';
import {mainWhiteColor as white} from 'constants/colors';
import {Section, Content, Title, Text, Button, ButtonText, ButtonIcon, Background, Image} from './styled';

const MCallToAction = () => {
  const {t} = useTranslation('sellerLanding');

  return (
    <Section>
      <Content>
        <Image src={ctaImg} />
        <Title>
          {t('MCallToAction.title')}
          <br />
          {t('MCallToAction.title2')}
        </Title>
        <Text>{parse(t('MCallToAction.text1'))}</Text>
        <Button>
          <ButtonText>{t('MCallToAction.ButtonText')}</ButtonText>
          <ButtonIcon>
            <ArrowIcon width={15} height={16} color={white} />
          </ButtonIcon>
        </Button>
      </Content>
      <Background src={ctaBg} />
    </Section>
  );
};

export default MCallToAction;
