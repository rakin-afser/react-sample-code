import styled from 'styled-components/macro';
import {mainWhiteColor as white} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 32px 24px;
  position: relative;
  height: 515px;
  display: none;
  text-align: center;
  overflow: hidden;

  @media (max-width: 425px) {
    display: block;
  }
`;

export const Content = styled.div`
  padding-top: 50px;
`;

export const Title = styled.h1`
  max-width: 212px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 34px;
  line-height: 120%;
  text-align: left;
  color: #000;
  margin-bottom: 84px;
`;

export const Text = styled.p`
  margin-bottom: 120px;
  max-width: 204px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 160%;
  text-align: left;
  color: #000;
`;

export const Button = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 50px;
  background: #000;
  color: ${white};
  border-radius: 100px;
  margin: 0 auto;
  padding: 0 23px 0 39px;
`;

export const ButtonText = styled.span`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  letter-spacing: 0.3px;
`;

export const ButtonIcon = styled.span`
  margin-top: 4px;
  margin-left: 16px;
`;

export const Background = styled.img`
  position: absolute;
  top: 0;
  left: 50%;
  transform: translateX(-50%);
  z-index: -1;
`;

export const Image = styled.img`
  z-index: 1;
  position: absolute;
  top: 156px;
  right: -24%;
  transform: translateX(-50%);
  width: 181px;
`;
