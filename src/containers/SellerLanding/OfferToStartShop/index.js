import React from 'react';
import {useWindowSize} from '@reach/window-size';
import {useTranslation} from 'react-i18next';
import ArrowIcon from 'assets/Arrow';
import {mainWhiteColor as white} from 'constants/colors';
import background from './images/background.jpg';
import backgroundMobile from './images/bg-mobile.jpg';
import {
  Section,
  Inner,
  Left,
  Right,
  RightTitle,
  RightSubtitle,
  Description,
  Background,
  LeftTitle,
  Button,
  ButtonText,
  ButtonIcon,
  Overlay
} from './styled';

const OfferToStartShop = () => {
  const {t} = useTranslation('sellerLanding');
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return (
    <Section>
      <Inner>
        <Left>
          <Background src={isMobile ? backgroundMobile : background} />
          <Overlay>
            <LeftTitle>{t('OfferToStartShop.leftTitle')}</LeftTitle>
            <Button>
              <ButtonText>{t('OfferToStartShop.buttonText')}</ButtonText>
              <ButtonIcon>
                <ArrowIcon width={11} height={15} color={white} />
              </ButtonIcon>
            </Button>
          </Overlay>
        </Left>
        <Right>
          <RightTitle>{t('OfferToStartShop.rightTitle')}</RightTitle>
          <RightSubtitle>{t('OfferToStartShop.rightSubtitle')}</RightSubtitle>
          <Description>{t('OfferToStartShop.description')}</Description>
        </Right>
      </Inner>
    </Section>
  );
};

export default OfferToStartShop;
