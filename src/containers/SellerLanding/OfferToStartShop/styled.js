import styled from 'styled-components/macro';
import {mainBlackColor as black, mainWhiteColor as white} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  height: 326px;
  background: #f9fbff;

  @media (max-width: 425px) {
    height: auto;
  }
`;

export const Inner = styled.div`
  height: 100%;
  display: flex;
  max-width: 1440px;
  margin: 0 auto;

  @media (max-width: 425px) {
    height: auto;
    flex-direction: column;
  }
`;

export const Left = styled.div`
  display: flex;
  position: relative;
  flex-basis: 604px;

  @media (max-width: 425px) {
    padding: 0 24px;
    height: 307px;
    flex-basis: auto;
    order: 2;
  }
`;

export const Right = styled.div`
  padding-top: 82px;
  max-width: 551px;
  margin-left: auto;
  margin-right: auto;

  @media (max-width: 425px) {
    padding: 46px 24px 25px;
    order: 1;
  }
`;

export const RightTitle = styled.h3`
  margin-bottom: 0;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  color: #000000;

  @media (max-width: 425px) {
    margin-bottom: 6px;
    font-size: 24px;
  }
`;

export const RightSubtitle = styled.p`
  margin-bottom: 31px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 140%;
  color: #000000;

  @media (max-width: 425px) {
    margin-bottom: 28px;
    font-size: 14px;
    font-weight: 500;
  }
`;

export const Description = styled.p`
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 160%;
  color: rgba(0, 0, 0, 0.8);

  @media (max-width: 425px) {
    font-size: 14px;
    line-height: 140%;
  }
`;

export const Background = styled.img`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const LeftTitle = styled.h3`
  margin-bottom: 27px;
  text-align: center;
  position: relative;
  z-index: 1;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  color: #ffffff;

  @media (max-width: 425px) {
    margin-bottom: 57px;
    max-width: 167px;
    font-size: 30px;
    text-align: left;
  }
`;

export const ButtonText = styled.div`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  color: ${white};
  transition: ease 0.4s;
  letter-spacing: 0.3px;

  @media (max-width: 425px) {
    margin-top: 2px;
    font-size: 16px;
    color: #000;
  }
`;

export const Overlay = styled.div`
  padding-top: 77px;
  position: relative;
  z-index: 1;
  margin-left: auto;
  width: 45%;
  background-color: rgba(122, 122, 122, 0.4);

  @media (max-width: 425px) {
    padding-top: 59px;
    width: 100%;
    background-color: rgba(122, 122, 122, 0);
  }
`;

export const ButtonIcon = styled.div`
  margin-top: 5px;

  path {
    transition: ease 0.4s;
  }

  @media (max-width: 425px) {
    margin-top: 6px;
    path {
      fill: #000;
    }
  }
`;

export const Button = styled.button`
  position: relative;
  z-index: 1;
  background: ${black};
  border-radius: 100px;
  min-width: 204px;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  padding: 15px 24px 15px 33px;
  margin-bottom: 27px;
  transition: ease 0.4s;
  border: 1px solid transparent;

  &:hover {
    background-color: #fff;
    border: 1px solid #000;

    ${ButtonText} {
      color: #000;
    }

    ${ButtonIcon} path {
      fill: #000;
    }
  }

  @media (max-width: 425px) {
    min-width: 195px;
    padding: 10px 25px 10px 32px;
    margin-left: 0;
    background: #fff;
  }
`;
