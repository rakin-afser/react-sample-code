import React from 'react';
import parse from 'html-react-parser';
import Icons from 'components/Icon';
import {Section, Wrapper, Title, Button} from './styled';
import {useTranslation} from 'react-i18next';

const Banner = () => {
  const {t} = useTranslation('sellerLanding');

  return (
    <Section>
      <Wrapper>
        <Title>{parse(t('Banner.title'))};</Title>
        <Button>
          {t('Banner.button')}
          <Icons type="arrow" color="#000" />
        </Button>
      </Wrapper>
    </Section>
  );
};

export default Banner;
