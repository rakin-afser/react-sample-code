import styled from 'styled-components/macro';
import {poppins} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const Section = styled.section`
  padding: 76px 20px 70px;
  margin-bottom: -56px;
  background-color: #1b2533;

  @media (max-width: 425px) {
    padding: 44px 20px 105px;
  }
`;

export const Wrapper = styled.div`
  max-width: 435px;
  margin: 0 auto;
`;

export const Title = styled.h3`
  margin-bottom: 48px;
  text-align: center;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: #ffffff;

  strong {
    color: ${primaryColor};
    font-weight: 600;
  }

  @media (max-width: 425px) {
    margin-bottom: 46px;
    font-size: 24px;
    line-height: 120%;
    letter-spacing: 0;
  }
`;

export const Button = styled.button`
  margin: 0 auto;
  padding: 0 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 58px;
  font-family: ${poppins};
  font-weight: bold;
  font-size: 18px;
  letter-spacing: 0.3px;
  color: #000000;
  background: #ffffff;
  border: 1px solid #fff;
  border-radius: 100px;
  transition: ease 0.4s;

  i {
    margin-left: 12px;

    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    background-color: #000;
    border: 1px solid #fff;
    color: #fff;

    svg {
      path {
        fill: #fff;
      }
    }
  }

  @media (max-width: 425px) {
    padding: 0 22px;
    height: 48px;
  }
`;
