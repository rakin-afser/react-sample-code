import React from 'react';
import {useTranslation} from 'react-i18next';
import ArrowIcon from 'assets/Arrow';
import {mainWhiteColor as white} from 'constants/colors';
import opportunityImg1 from './images/opportunity-img-1.png';
import opportunityImg2 from './images/opportunity-img-2.png';
import opportunityImg3 from './images/opportunity-img-3.png';
import opportunityBg1 from './images/opportunity-bg-1.png';
import opportunityBg2 from './images/opportunity-bg-2.png';
import opportunityBg3 from './images/opportunity-bg-3.png';
import {
  Section,
  Wrapper,
  Title,
  Flex,
  SubTitle,
  SubTitleLvl2,
  List,
  ListItem,
  Button,
  ButtonText,
  ButtonIcon,
  First,
  FirstLeft,
  FirstRight,
  Second,
  SecondLeft,
  SecondRight,
  Third,
  ThirdLeft,
  ThirdRight,
  Image1,
  Image3,
  Image5,
  FirstBackground,
  SecondBackground,
  ThirdBackground
} from './styled';

const Opportunities = () => {
  const {t} = useTranslation('sellerLanding');

  return (
    <Section>
      <Wrapper>
        <Title>
          {t('Opportunities.title')} <b>{t('testSample')}</b>
        </Title>
        <First>
          <Flex index={1}>
            <FirstLeft>
              <SubTitle>{t('Opportunities.subTitle1')}</SubTitle>
              <List>
                <ListItem>{t('Opportunities.listItem1-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem1-2-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem1-3-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem1-4')}</ListItem>
              </List>
              <Button>
                <ButtonText>{t('Opportunities.buttonText')}</ButtonText>
                <ButtonIcon>
                  <ArrowIcon width={11} height={15} color={white} />
                </ButtonIcon>
              </Button>
            </FirstLeft>
            <FirstRight>
              <Image1 src={opportunityImg1} />
              <FirstBackground src={opportunityBg1} />
            </FirstRight>
          </Flex>
        </First>
        <Second>
          <Flex index={2}>
            <SecondLeft>
              <Image3 src={opportunityImg2} />
              <SecondBackground src={opportunityBg2} />
            </SecondLeft>
            <SecondRight>
              <SubTitle>{t('Opportunities.subTitle2')}</SubTitle>
              <SubTitleLvl2>{t('Opportunities.subTitleLvl2-2')}</SubTitleLvl2>
              <List>
                <ListItem>{t('Opportunities.listItem2-2-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem2-3-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem2-4-1')}</ListItem>
              </List>
            </SecondRight>
          </Flex>
        </Second>
        <Third>
          <Flex index={3}>
            <ThirdLeft>
              <SubTitle>{t('Opportunities.subTitle3')}</SubTitle>
              <SubTitleLvl2>{t('Opportunities.subTitleLvl2-3')}</SubTitleLvl2>
              <List>
                <ListItem>{t('Opportunities.listItem3-2-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem3-3-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem3-4-1')}</ListItem>
                <ListItem>{t('Opportunities.listItem3-5-1')}</ListItem>
              </List>
            </ThirdLeft>
            <ThirdRight>
              <Image5 src={opportunityImg3} />
              <ThirdBackground src={opportunityBg3} />
            </ThirdRight>
          </Flex>
        </Third>
      </Wrapper>
    </Section>
  );
};

export default Opportunities;
