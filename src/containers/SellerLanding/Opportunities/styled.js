import styled from 'styled-components/macro';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  secondaryTextColor as gray800,
  blueBtn,
  watermelon
} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 115px 20px 194px 20px;
  overflow: hidden;

  @media (max-width: 1024px) {
    padding-top: 5px;
  }

  @media (max-width: 565px) {
    padding: 115px 24px 0 24px;
  }

  @media (max-width: 425px) {
    padding-top: 28px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const Title = styled.h2`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  text-align: center;
  color: ${black};
  margin-bottom: 36px;

  b {
    color: ${watermelon};
    font-size: 34px;
  }

  @media (max-width: 425px) {
    margin-bottom: 0;
    text-align: left;
    font-size: 24px;

    b {
      font-size: 22px;
    }
  }
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;

  @media (max-width: 1024px) {
    flex-direction: ${({index}) => (index === 2 ? 'column-reverse' : 'column')};
  }
`;

export const SubTitle = styled.h4`
  margin-bottom: 0;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 140%;
  color: ${black};

  @media (max-width: 425px) {
    font-size: 18px;
    font-weight: 600;
  }
`;

export const SubTitleLvl2 = styled.h5`
  margin-top: 8px;
  margin-bottom: 2px;
  font-family: ${poppins};
  font-weight: 600;
  font-size: 20px;
  line-height: 140%;
  color: #000000;

  @media (max-width: 425px) {
    margin-bottom: 10px;
    font-size: 14px;
    font-weight: 500;
  }
`;

export const List = styled.ul`
  padding-top: 43px;

  @media (max-width: 425px) {
    padding-top: 29px;
  }
`;

export const ListItem = styled.li`
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: rgba(0, 0, 0, 0.8);
  position: relative;
  padding-left: 10px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    list-style-type: arabic-indic;
  `}
  & + & {
    margin-top: 23px;
  }

  &:before {
    content: '';
    position: absolute;
    top: 8px;
    left: 0;
    width: 3.5px;
    height: 3.5px;
    background: ${gray800};
    border-radius: 50%;

    ${({theme: {isArabic}}) =>
      isArabic &&
      `
    display: none;
  `}
  }

  @media (max-width: 1024px) {
    padding-left: 11px;

    &:before {
    }
  }
`;

export const Button = styled.button`
  display: flex;
  flex-direction: row;
  margin-top: 44px;
  margin-bottom: 2px;
  padding: 15px 19px 15px 36px;
  border-radius: 100px;
  background: #000;
  color: ${white};
  transition: ease 0.4s;

  &:hover {
    background-color: ${blueBtn};
  }

  @media (max-width: 1024px) {
    margin-left: auto;
    margin-right: auto;
  }

  @media (max-width: 425px) {
    position: relative;
    z-index: 2;
    margin-top: 40px;
    padding: 11px 28px 11px 33px;
  }
`;

export const ButtonText = styled.span`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  letter-spacing: 0.3px;

  @media (max-width: 425px) {
    margin-top: 2px;
    font-size: 16px;
  }
`;

export const ButtonIcon = styled.span`
  margin-left: 29px;
  padding-top: 6px;

  @media (max-width: 425px) {
    margin-left: 22px;
  }
`;

export const First = styled.div`
  padding-bottom: 120px;
`;

export const FirstLeft = styled.div`
  flex: 1;
  padding-top: 93px;

  @media (max-width: 1024px) {
    padding-top: 45px;
    text-align: center;
  }

  @media (max-width: 425px) {
    padding-top: 35px;
    text-align: left;
  }
`;

export const FirstRight = styled.div`
  flex: 1;
  position: relative;

  @media (max-width: 1024px) {
    width: 100%;
    min-height: 435px;
  }
`;

export const Second = styled.div`
  padding-bottom: 172px;

  @media (max-width: 425px) {
    padding-bottom: 0;
  }
`;

export const SecondLeft = styled.div`
  flex: 1;
  position: relative;

  @media (max-width: 1024px) {
    min-height: 375px;
  }
`;

export const SecondRight = styled.div`
  flex: 1;
  padding-top: 103px;

  @media (max-width: 1024px) {
    margin-left: 0;
    padding-top: 0;
    text-align: center;
  }

  @media (max-width: 425px) {
    text-align: left;
  }
`;

export const Third = styled.div`
  @media (max-width: 425px) {
    margin-top: -22px;
  }
`;

export const ThirdLeft = styled.div`
  flex: 1;
  padding-top: 99px;

  @media (max-width: 1024px) {
    text-align: center;
    padding-top: 15px;
  }

  @media (max-width: 425px) {
    text-align: left;
  }
`;

export const ThirdRight = styled.div`
  flex: 1;
  position: relative;

  @media (max-width: 1024px) {
    min-height: 300px;
  }
`;

export const Image1 = styled.img`
  position: absolute;
  top: 69px;
  left: 220px;
  z-index: 3;

  @media (max-width: 1024px) {
    left: 30%;
  }

  @media (max-width: 768px) {
    left: 20%;
  }

  @media (max-width: 565px) {
    top: 52px;
    left: 50%;
    transform: ${({theme: {isArabic}}) => (isArabic ? 'translateX(50%)' : 'translateX(-50%)')};
  }
`;

export const Image3 = styled.img`
  position: absolute;
  top: 77px;
  left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '-48px')};
  right: ${({theme: {isArabic}}) => (isArabic ? '-31px' : 'auto')};
  z-index: 3;

  @media (max-width: 1024px) {
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '25%')};
    right: ${({theme: {isArabic}}) => (isArabic ? '25%' : 'auto')};
  }

  @media (max-width: 768px) {
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '14%')};
    right: ${({theme: {isArabic}}) => (isArabic ? '14%' : 'auto')};
  }

  @media (max-width: 565px) {
    top: 10px;
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '48%')};
    right: ${({theme: {isArabic}}) => (isArabic ? '48%' : 'auto')};
    transform: ${({theme: {isArabic}}) => (isArabic ? 'translateX(50%) scale(.78)' : 'translateX(-50%) scale(.78)')};
  }
`;

export const Image5 = styled.img`
  position: absolute;
  top: 101px;
  left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '51px')};
  right: ${({theme: {isArabic}}) => (isArabic ? '51px' : 'auto')};
  z-index: 2;

  @media (max-width: 1024px) {
    top: 72px;
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '50%')};
    right: ${({theme: {isArabic}}) => (isArabic ? '50%' : 'auto')};
    transform: ${({theme: {isArabic}}) => (isArabic ? 'translateX(50%)' : 'translateX(-50%)')};
  }

  @media (max-width: 565px) {
    width: 359px;
    top: 42px;
  }
`;

export const FirstBackground = styled.img`
  position: absolute;
  top: 97px;
  left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '85px')};
  right: ${({theme: {isArabic}}) => (isArabic ? '18px' : 'auto')};
  z-index: 1;

  @media (max-width: 1024px) {
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '50%')};
    right: ${({theme: {isArabic}}) => (isArabic ? '50%' : 'auto')};
    transform: ${({theme: {isArabic}}) => (isArabic ? 'translateX(50%)' : 'translateX(-50%)')};
  }

  @media (max-width: 565px) {
    top: 76px;
    width: 515px;
  }
`;

export const SecondBackground = styled.img`
  position: absolute;
  top: 0;
  left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '-142px')};
  right: ${({theme: {isArabic}}) => (isArabic ? '-98px' : 'auto')};
  z-index: 1;

  @media (max-width: 1024px) {
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '50%')};
    right: ${({theme: {isArabic}}) => (isArabic ? '50%' : 'auto')};
    transform: ${({theme: {isArabic}}) => (isArabic ? 'translateX(50%)' : 'translateX(-50%)')};
  }

  @media (max-width: 565px) {
    width: 428px;
    top: 19px;
  }
`;

export const ThirdBackground = styled.img`
  position: absolute;
  top: 8px;
  left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '100px')};
  right: ${({theme: {isArabic}}) => (isArabic ? '100px' : 'auto')};
  z-index: 1;

  @media (max-width: 1024px) {
    top: -16px;
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '50%')};
    right: ${({theme: {isArabic}}) => (isArabic ? '50%' : 'auto')};
    transform: ${({theme: {isArabic}}) => (isArabic ? 'translateX(50%)' : 'translateX(-50%)')};
  }

  @media (max-width: 565px) {
    width: 351px;
    top: -34px;
    left: 63%;
  }
`;
