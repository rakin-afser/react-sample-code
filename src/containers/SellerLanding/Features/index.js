import React from 'react';
import {useTranslation} from 'react-i18next';
import parse from 'html-react-parser';
import featureImg1 from './images/image1.svg';
import featureImg2 from './images/image2.svg';
import {Section, Wrapper, Feature, ImageWrapper, Image, Title, List, ListItem} from './styled';

const Features = () => {
  const {t} = useTranslation('sellerLanding');

  const data = [
    {
      id: 1,
      img: featureImg1,
      title: t('Features.title1'),
      list: [
        {id: 1, text: t('Features.text1-1')},
        {id: 2, text: t('Features.text1-2')}
      ]
    },
    {
      id: 2,
      img: featureImg2,
      title: t('Features.title2'),
      list: [
        {id: 1, text: t('Features.text2-1')},
        {id: 2, text: t('Features.text2-2')}
      ]
    }
  ];

  return (
    <Section>
      <Wrapper>
        {data.map(({id, img, title, list}) => (
          <Feature key={id}>
            <ImageWrapper index={id}>
              <Image src={img} index={id} />
            </ImageWrapper>
            <Title>{parse(title)}</Title>
            <List>
              {list.map(({id, text}) => (
                <ListItem key={id}>{text}</ListItem>
              ))}
            </List>
          </Feature>
        ))}
      </Wrapper>
    </Section>
  );
};

export default Features;
