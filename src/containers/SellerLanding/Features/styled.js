import styled from 'styled-components/macro';
import {mainBlackColor as black, secondaryTextColor as gray800, bgLightGray as gray200} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 179px 20px 78px 20px;
  position: relative;

  &:after {
    content: '';
    background: ${gray200};
    width: 1040px;
    height: 1px;
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
  }

  @media (max-width: 1040px) {
    &:after {
      width: 100%;
    }
  }

  @media (max-width: 1024px) {
    padding: 25px 20px 85px 20px;
  }

  @media (max-width: 425px) {
    padding-top: 58px;
    padding-bottom: 49px;

    &:after {
      width: calc(100% - 48px);
    }
  }
`;

export const Wrapper = styled.div`
  max-width: 1100px;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `};

  @media (max-width: 1024px) {
    flex-direction: column;
  }
`;

export const Feature = styled.div`
  flex: 1;
  text-align: center;

  & + & {
    margin-left: 99px;
  }

  @media (max-width: 1024px) {
    & + & {
      margin-left: 0;
      margin-top: 43px;
    }
  }
`;

export const Image = styled.img`
  @media (max-width: 1024px) {
    margin-left: ${({index}) => (index === 1 ? '0' : '-70px')};
  }

  @media (max-width: 425px) {
    width: 100%;
    ${({index}) =>
      index === 2 &&
      `
      margin-left: 0;
      width: 150px;
    `};
  }
`;

export const ImageWrapper = styled.div`
  width: 239px;
  height: 194px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-end;
  margin: ${({index}) => (index === 1 ? '0 auto' : '0 auto')};

  @media (max-width: 1024px) {
    margin: 0 auto;
    height: auto;
  }

  @media (max-width: 425px) {
    width: 163px;
  }
`;

export const Title = styled.h4`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 30px;
  line-height: 140%;
  color: ${black};
  margin: 34px 0 37px 0;

  @media (max-width: 425px) {
    font-size: 24px;
    margin: 29px 0 17px 0;
  }
`;

export const List = styled.ul`
  text-align: ${({theme: {isArabic}}) => (isArabic ? 'right' : 'center')};

  @media (max-width: 1024px) {
    text-align: center;
  }

  @media (max-width: 425px) {
    margin: 0 auto;
    max-width: 310px;
  }
`;

export const ListItem = styled.li`
  display: inline-block;
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 160%;
  color: rgba(0, 0, 0, 0.8);
  position: relative;
  text-align: center;
  padding-left: 10px;

  ${({theme: {isArabic}}) => isArabic && `list-style-type: arabic-indic`};

  & + & {
    margin-top: 18px;
  }

  &:before {
    content: '';
    position: absolute;
    top: 10px;
    left: 0;
    width: 3px;
    height: 3px;
    background: ${gray800};
    border-radius: 50%;

    ${({theme: {isArabic}}) => isArabic && `display: none`};
  }

  @media (max-width: 1024px) {
    padding-left: 11px;

    &:before {
      position: relative;
      display: inline-block;
      top: -3px;
      left: -6px;
    }
  }

  @media (max-width: 425px) {
    & + & {
      margin-top: 6px;
    }
  }
`;
