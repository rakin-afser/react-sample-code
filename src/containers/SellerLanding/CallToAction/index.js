import React from 'react';
import {useTranslation} from 'react-i18next';
import useGlobal from 'store';
import ArrowIcon from 'assets/Arrow';
import ctaImg from 'assets/sellerLandingImages/ctaImg.svg';
import {mainWhiteColor as white} from 'constants/colors';
import ctaBg from './images/bg.png';
import {Section, Wrapper, Left, Title, Text, Button, ButtonText, ButtonIcon, Right, Background, Image} from './styled';

const CallToAction = () => {
  const [globalState] = useGlobal();
  const {isArabic} = globalState;
  const {t} = useTranslation('sellerLanding');

  return (
    <Section>
      <Wrapper dir={isArabic ? 'rtl' : null}>
        <Left>
          <Title>
            {t('callToAction.title')}
            <br /> {t('callToAction.title2')}
          </Title>
          <Text>
            {t('callToAction.text1')}
            <br />
            <strong>{t('callToAction.text2')}</strong>
          </Text>
          <Button>
            <ButtonText>{t('callToAction.ButtonText')}</ButtonText>
            <ButtonIcon>
              <ArrowIcon width={11} height={15} color={white} />
            </ButtonIcon>
          </Button>
        </Left>
        <Right>
          <Background src={ctaBg} />
          <Image src={ctaImg} />
        </Right>
      </Wrapper>
    </Section>
  );
};

export default CallToAction;
