import styled from 'styled-components/macro';

import {mainWhiteColor as white, blueBtn} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  @media (max-width: 1280px) {
    text-align: center;
    padding-left: 0;
  }

  @media (max-width: 425px) {
    display: none;
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Left = styled.div`
  padding: 133px 0 86px;
  max-width: 582px;

  @media (max-width: 1280px) {
    text-align: center;
    margin: 0 auto;
    padding-bottom: 25px;
  }

  @media (max-width: 768px) {
    padding-bottom: 0;
  }
`;

export const Title = styled.h1`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 48px;
  line-height: 120%;
  color: #000;
  margin-bottom: 47px;
`;

export const Text = styled.p`
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 22px;
  line-height: 160%;
  color: #000;
  margin-bottom: 31px;
`;

export const Button = styled.button`
  background: #000;
  font-family: ${poppins};
  font-weight: bold;
  color: ${white};
  border-radius: 100px;
  font-size: 18px;
  margin-top: 13px;
  padding: 14px 30px 14px 34px;
  transition: ease 0.4s;
  letter-spacing: 0.3px;

  &:hover {
    background-color: ${blueBtn};
  }
`;

export const ButtonText = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 22px;
`;

export const ButtonIcon = styled.span`
  margin-left: 26px;
`;

export const Background = styled.img`
  width: 995px;
  position: absolute;

  top: -45px;
  right: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '-186px')};
  left: ${({theme: {isArabic}}) => (isArabic ? '-186px' : 'auto')};
  z-index: -1;
`;

export const Right = styled.div`
  flex: 1;
  position: relative;

  @media (max-width: 1280px) {
    display: none;
  }
`;

export const Image = styled.img`
  position: absolute;
  top: 55px;
  right: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '22px')};
  left: ${({theme: {isArabic}}) => (isArabic ? '115px' : 'auto')};
  z-index: -1;
`;
