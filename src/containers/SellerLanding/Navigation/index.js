import React from 'react';
import {useTranslation} from 'react-i18next';
import useGlobal from 'store';
import {Section, NavMenu, NavLink} from './styled';

const Navigation = () => {
  const [globalState] = useGlobal();
  const {isArabic} = globalState;
  const {t} = useTranslation('sellerLanding');

  const links = [
    {id: 1, href: '#features', title: t('Navigation.Features')},
    {id: 2, href: '#explore', title: t('Navigation.Explore')},
    {id: 3, href: '#social', title: t('Navigation.Social')},
    {id: 4, href: '#rewards', title: t('Navigation.Rewards')}
  ];

  return (
    <Section>
      <NavMenu dir={isArabic ? 'rtl' : null}>
        {links.map(({id, href, title}) => (
          <NavLink key={id} href={href}>
            {title}
          </NavLink>
        ))}
      </NavMenu>
    </Section>
  );
};

export default Navigation;
