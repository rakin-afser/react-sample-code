import styled from 'styled-components/macro';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 99px 20px 3px 20px;

  @media (max-width: 768px) {
    padding-bottom: 65px;
  }

  @media (max-width: 425px) {
    padding: 34px 24px 5px 24px;
  }
`;

export const NavMenu = styled.nav`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const NavLink = styled.a`
  position: relative;
  height: 56px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: #7d7d86;
  display: inline-block;

  &::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 3px;
    background: #000000;
    border-radius: 3px;
    transition: ease 0.4s;
    cursor: pointer;
    opacity: 0;
  }

  &:hover {
    color: #000;

    &::after {
      opacity: 1;
    }
  }

  & + & {
    margin-left: 29px;
  }

  @media (max-width: 425px) {
    font-family: ${poppins};
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    line-height: 15px;
    color: #666666;

    & + & {
      margin-left: 21px;
    }
  }
`;
