import styled from 'styled-components/macro';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 82px 20px 73px;

  @media (max-width: 425px) {
    padding: 23px 24px 17px;
  }
`;

export const Wrapper = styled.div`
  margin: 0 auto;
  max-width: 1046px;
`;

export const Title = styled.h3`
  margin-bottom: 55px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  color: #000000;

  @media (max-width: 425px) {
    margin-bottom: 21px;
    font-size: 24px;
  }
`;

export const Tags = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const Tag = styled.div`
  margin-right: 15px;
  padding: 0 22px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 42px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  color: rgba(0, 0, 0, 0.8);
  background: #ffffff;
  border: 1px solid #d4d4d4;
  border-radius: 100px;
  transition: ease 0.4s;
  cursor: pointer;

  &:hover {
    background-color: #000;
    color: #fff;
  }

  @media (max-width: 425px) {
    padding: 0 14px;
    margin-bottom: 12px;
    margin-right: 10px;
    height: 32px;
    font-size: 11px;
  }
`;
