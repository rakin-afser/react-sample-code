import React from 'react';
import {Section, Wrapper, Title, Tags, Tag} from './styled';
import {useTranslation} from 'react-i18next';

const Categories = () => {
  const {t} = useTranslation('sellerLanding');

  const tags = [
    {id: 1, title: t('Categories.tag1')},
    {id: 2, title: t('Categories.tag2')},
    {id: 3, title: t('Categories.tag3')},
    {id: 4, title: t('Categories.tag4')},
    {id: 5, title: t('Categories.tag5')},
    {id: 6, title: t('Categories.tag6')},
    {id: 7, title: t('Categories.tag7')}
  ];

  return (
    <Section>
      <Wrapper>
        <Title>{t('Categories.title')}</Title>
        <Tags>
          {tags.map((el) => (
            <Tag key={el.id}>{el.title}</Tag>
          ))}
        </Tags>
      </Wrapper>
    </Section>
  );
};

export default Categories;
