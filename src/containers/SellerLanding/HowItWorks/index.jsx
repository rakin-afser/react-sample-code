import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import ReactPlayer from 'react-player';
import {useWindowSize} from '@reach/window-size';
import videoDesktop from './videos/video-desktop.mp4';
import videoMobile from './videos/video-mobile.mp4';
import posterDesktop from './img/posterDesktop.png';
import posterMobile from './img/posterMobile.png';
import {Section, SectionInner, Title, VideoWrapper, Poster, PlayBtn} from './styled';
import {ReactComponent as IconPlay} from './img/iconPlay.svg';
import {ReactComponent as IconPause} from './img/iconPause.svg';

const HowItWorks = () => {
  const {t} = useTranslation('sellerLanding');
  const [play, setPlay] = useState(false);
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  const onPlayToggle = () => {
    setPlay((prev) => !prev);
  };

  return (
    <Section>
      <SectionInner>
        <Title>{t('HowItWorks.title')}</Title>

        <VideoWrapper isMobile={isMobile}>
          <ReactPlayer
            playsinline
            onEnded={() => {
              setPlay(false);
            }}
            playing={play}
            controls={false}
            url={isMobile ? videoMobile : videoDesktop}
            width="100%"
            height="100%"
          />
          <Poster play={play}>
            <img src={isMobile ? posterMobile : posterDesktop} alt="poster" />
          </Poster>
          <PlayBtn isMobile={isMobile} play={play} onClick={onPlayToggle}>
            {!play ? <IconPlay /> : <IconPause />}
          </PlayBtn>
        </VideoWrapper>
      </SectionInner>
    </Section>
  );
};

export default HowItWorks;
