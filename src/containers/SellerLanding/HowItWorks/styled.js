import styled from 'styled-components/macro';
import {mainBlackColor} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  margin-bottom: 99px;
  padding-top: 94px;
  padding-bottom: 79px;
  background-color: #f0f2f4;

  @media (max-width: 425px) {
    padding-top: 46px;
    margin-bottom: 45px;
    padding-left: 24px;
    padding-right: 24px;
  }
`;

export const SectionInner = styled.div`
  max-width: 1040px;
  margin: 0 auto;
`;

export const PlayBtn = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  z-index: 3;
  transform: translate(-50%, -50%);
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${({isMobile}) => (isMobile ? '80px' : '100px')};
  height: ${({isMobile}) => (isMobile ? '80px' : '100px')};
  background: #fafafa;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  border-radius: 50%;
  cursor: pointer;
  opacity: ${({play}) => (!play ? 1 : 0)};
  transition: ease 0.4s;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    box-shadow: 0 2px 7px rgba(0, 0, 0, 0.3);

    svg {
      path {
        fill: #464646;
      }
    }
  }
`;

export const Title = styled.h2`
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 66px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 34px;
  line-height: 140%;
  text-align: center;
  color: ${mainBlackColor};

  @media (max-width: 425px) {
    margin-bottom: 36px;
    text-align: left;
    font-size: 24px;
    line-height: 120%;
  }
`;

export const VideoWrapper = styled.div`
  margin: 0 auto;
  position: relative;
  height: ${({isMobile}) => (isMobile ? '500px' : '585px;')};
  width: 100%;
  box-shadow: 0 0 6px rgba(0, 0, 0, 0.2);
  overflow: hidden;
  border-radius: 20px;

  video {
    position: relative;
    z-index: 1;
    object-fit: cover;
  }

  &:hover ${PlayBtn} {
    opacity: 1;

    ${({isMobile, play}) =>
      isMobile &&
      play &&
      `
      opacity: 0
    `}
  }
`;

export const Poster = styled.div`
  position: absolute;
  z-index: 2;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
  transition: ease 0.4s;
  opacity: ${({play}) => (!play ? 1 : 0)};

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;
