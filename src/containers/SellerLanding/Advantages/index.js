import React from 'react';
import parse from 'html-react-parser';
import {useTranslation} from 'react-i18next';

import advantageImg1 from './images/icon1.svg';
import advantageImg2 from './images/icon2.svg';
import advantageImg3 from './images/icon3.svg';
import {Section, Wrapper, Card, Image, Title, Text} from './styled';

const Advantages = () => {
  const {t} = useTranslation('sellerLanding');

  const advantagesData = [
    {
      id: 1,
      title: t('Advantages.title1'),
      text: t('Advantages.text1'),
      img: advantageImg1
    },
    {
      id: 2,
      title: t('Advantages.title2'),
      text: t('Advantages.text2'),
      img: advantageImg2
    },
    {
      id: 3,
      title: t('Advantages.title3'),
      text: t('Advantages.text3'),
      img: advantageImg3
    }
  ];

  return (
    <Section>
      <Wrapper>
        {advantagesData.map(({id, title, text, img}) => (
          <Card key={id}>
            <Image src={img} />
            <Title>{parse(title)}</Title>
            <Text>{parse(text)}</Text>
          </Card>
        ))}
      </Wrapper>
    </Section>
  );
};

export default Advantages;
