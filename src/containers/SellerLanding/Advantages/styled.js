import styled from 'styled-components/macro';
import {mainBlackColor as black} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 33px 20px;

  @media (max-width: 768px) {
    padding-top: 35px;
  }

  @media (max-width: 425px) {
    padding-top: 27px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1100px;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `};

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: center;
  }
`;

export const Card = styled.div`
  padding: 40px 18px 29px 28px;
  text-align: left;
  flex: 1;
  box-shadow: 0 4px 10px rgba(0, 0, 0, 0.07);
  border-radius: 8px;

  & + & {
    margin-left: ${({theme: {isArabic}}) => (isArabic ? '0' : '27px')};
  }

  @media (max-width: 768px) {
    max-width: 400px;

    & + & {
      margin-left: 0;
      margin-top: 20px;
    }
  }

  @media (max-width: 425px) {
    padding: 31px 15px 19px 30px;
  }
`;

export const Image = styled.img`
  @media (max-width: 425px) {
    width: 53px;
  }
`;

export const Title = styled.h3`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 140%;
  color: ${black};
  margin: 46px 0 14px;

  @media (max-width: 425px) {
    margin: 25px 0 14px;
  }
`;

export const Text = styled.p`
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 160%;
  color: rgba(0, 0, 0, 0.8);

  @media (max-width: 425px) {
  }
`;
