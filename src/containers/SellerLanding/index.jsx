import React from 'react';

import Layout from 'containers/Layout';
import CallToAction from './CallToAction';
import Navigation from './Navigation';
import About from './About';
import Advantages from './Advantages';
import Opportunities from './Opportunities';
import AdvantagesMore from './AdvantagesMore';
import Features from './Features';
import Plans from './Plans';
import Questions from './Questions';
import OfferToStartShop from './OfferToStartShop';

import MCallToAction from './MCallToAction';
import HowItWorks from './HowItWorks';
import Categories from './Categories';
import Banner from './Banner';

const SellerLanding = () => (
  <Layout sellerHeader isFooterShort>
    <CallToAction />
    <MCallToAction />
    <Navigation />
    <HowItWorks />
    <About />
    <Advantages />
    <Opportunities />
    <AdvantagesMore />
    <Features />
    <Plans />
    <Questions />
    <OfferToStartShop />
    <Categories />
    <Banner />
  </Layout>
);

export default SellerLanding;
