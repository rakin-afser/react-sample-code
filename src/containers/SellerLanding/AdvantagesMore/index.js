import React from 'react';
import {useTranslation} from 'react-i18next';

import advantageImg1 from './images/icon1.svg';
import advantageImg2 from './images/icon2.svg';
import advantageImg3 from './images/icon3.svg';

import {Section, Wrapper, Card, Image, Title, Text} from './styled';

const AdvantagesMore = () => {
  const {t} = useTranslation('sellerLanding');

  const data = [
    {
      id: 1,
      title: t('AdvantagesMore.title1'),
      text: t('AdvantagesMore.text1'),
      img: advantageImg1
    },
    {
      id: 2,
      title: t('AdvantagesMore.title2'),
      text: t('AdvantagesMore.text2'),
      img: advantageImg2
    },
    {
      id: 3,
      title: t('AdvantagesMore.title3'),
      text: t('AdvantagesMore.text3'),
      img: advantageImg3
    }
  ];

  return (
    <Section>
      <Wrapper>
        {data.map(({id, title, text, img}) => (
          <Card key={id}>
            <Image src={img} index={id} />
            <Title>{title}</Title>
            <Text>{text}</Text>
          </Card>
        ))}
      </Wrapper>
    </Section>
  );
};

export default AdvantagesMore;
