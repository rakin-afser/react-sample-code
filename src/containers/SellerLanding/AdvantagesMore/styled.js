import styled from 'styled-components/macro';
import {mainBlackColor as black} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 95px 20px 81px 20px;
  background: #f0f2f4;

  @media (max-width: 425px) {
    padding: 41px 20px 51px 20px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1166px;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  ${({theme: {isArabic}}) => isArabic && `direction: rtl`};

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Image = styled.img`
  width: 46px;
  height: 45px;
  object-fit: contain;
  margin-left: ${({index}) => (index === 2 ? '9px' : '0')};
`;

export const Title = styled.h4`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 140%;
  text-align: center;
  color: ${black};
  margin: 34px 0 14px 0;

  @media (max-width: 425px) {
    margin-top: 31px;
  }
`;

export const Text = styled.p`
  max-width: 208px;
  margin: 0 auto;
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 160%;
  color: rgba(0, 0, 0, 0.8);
`;

export const Card = styled.div`
  text-align: center;
  flex: 1;

  & + & {
    margin-left: 65px;
  }

  @media (max-width: 768px) {
    max-width: 350px;
    margin: 0 auto;

    & + & {
      margin-left: auto;
      margin-top: 25px;
    }
  }

  &:last-child ${Text} {
    max-width: 270px;
  }

  @media (max-width: 425px) {
    & + & {
      margin-top: 34px;
    }
  }
`;
