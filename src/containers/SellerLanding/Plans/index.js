import React from 'react';
import {useTranslation} from 'react-i18next';
import {ThemeProvider} from 'styled-components';
import {
  Section,
  Wrapper,
  Title,
  Text,
  Cards,
  Card,
  Name,
  Description,
  Price,
  PriceValue,
  List,
  ListItem,
  ButtonWrapper,
  Button
} from './styled';

const Plans = () => {
  const {t} = useTranslation('sellerLanding');

  const data = [
    {
      id: 1,
      name: t('Plans.card1Name'),
      description: t('Plans.card1Description'),
      price: null,
      list: [
        {id: 1, text: t('Plans.card1Item1')},
        {id: 2, text: t('Plans.card1Item2')},
        {id: 3, text: t('Plans.card1Item3')},
        {id: 4, text: t('Plans.card1Item4')},
        {id: 5, text: t('Plans.card1Item5')}
      ],
      buttonText: t('Plans.card1ButtonText')
    },
    {
      id: 2,
      name: t('Plans.card2Name'),
      description: t('Plans.card2Description'),
      price: 19,
      list: [
        {id: 1, text: t('Plans.card2Item1')},
        {id: 2, text: t('Plans.card2Item2')},
        {id: 3, text: t('Plans.card2Item3')},
        {id: 4, text: t('Plans.card2Item4')}
      ],
      buttonText: t('Plans.card2ButtonText')
    },
    {
      id: 3,
      name: t('Plans.card3Name'),
      description: t('Plans.card3Description'),
      price: 9,
      list: [
        {id: 1, text: t('Plans.card3Item1')},
        {id: 2, text: t('Plans.card3Item2')},
        {id: 3, text: t('Plans.card3Item3')}
      ],
      buttonText: t('Plans.card3ButtonText')
    }
  ];

  return (
    <Section>
      <Wrapper>
        <Title>{t('Plans.title')}</Title>
        <Text>
          {t('Plans.text1')} <b>{t('Plans.text2')}</b>
        </Text>
        <Cards>
          {data.map(({id, name, description, price, list, buttonText}) => (
            <ThemeProvider theme={{index: id}} key={id}>
              <Card>
                <Name>{name}</Name>
                {price && (
                  <Price>
                    BD<PriceValue>{price}</PriceValue> &#47;m
                  </Price>
                )}
                <Description>{description}</Description>
                <List>
                  {list.map(({id, text}) => (
                    <ListItem key={id}>{text}</ListItem>
                  ))}
                </List>
                <ButtonWrapper>
                  <Button>{buttonText}</Button>
                </ButtonWrapper>
              </Card>
            </ThemeProvider>
          ))}
        </Cards>
      </Wrapper>
    </Section>
  );
};

export default Plans;
