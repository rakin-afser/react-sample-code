import styled from 'styled-components/macro';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  bgLightGray as gray200,
  primaryColor as primary,
  watermelon
} from 'constants/colors';
import iconCheck from 'assets/sellerLandingImages/iconCheckBlack.svg';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 98px 0 115px;
  position: relative;

  &:after {
    content: '';
    background: ${gray200};
    width: 1040px;
    height: 1px;
    position: absolute;
    bottom: 0;
    left: 50%;
    transform: translateX(-50%);
  }

  @media (max-width: 1040px) {
    &:after {
      width: 100%;
    }
  }

  @media (max-width: 975px) {
    padding: 95px 0;
  }

  @media (max-width: 425px) {
    padding: 55px 0;

    &:after {
      display: none;
    }
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;
`;

export const Title = styled.h3`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  color: ${black};
  text-align: center;
  margin-bottom: 8px;

  @media (max-width: 425px) {
    font-size: 24px;
  }
`;

export const Text = styled.p`
  font-family: ${poppins};
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 140%;
  text-align: center;
  color: ${black};
  margin-bottom: 67px;

  b {
    font-weight: 600;
    color: ${watermelon};
  }

  @media (max-width: 425px) {
    margin-bottom: 37px;
    font-size: 14px;
  }
`;

export const Cards = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 0 33px;

  @media (max-width: 975px) {
    flex-direction: column;
    align-items: center;
  }
`;

export const Card = styled.div`
  display: flex;
  flex-direction: column;
  width: 300px;
  height: 435px;
  box-shadow: 0 4px 10px rgba(0, 0, 0, 0.07);
  border-radius: 10px;
  border: 2px solid transparent;
  transition: ease 0.4s;
  padding: 32px 20px 40px 20px;
  background: ${white};

  @media (max-width: 975px) {
    height: auto;

    & + & {
      margin-top: 20px;
    }
  }

  @media (max-width: 425px) {
    padding-top: 24px;
    padding-bottom: 28px;
    width: 324px;
  }

  &:hover {
    border: 2px solid #eeecec;
    box-shadow: 0 16px 16px rgba(0, 0, 0, 0.07);
  }
`;

export const Name = styled.h4`
  font-family: ${poppins};
  font-style: normal;
  font-weight: 600;
  font-size: 22px;
  line-height: 124%;
  text-align: center;
  color: ${black};
  margin-bottom: 4px;
`;

export const Description = styled.p`
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  text-align: center;
  color: rgba(0, 0, 0, 0.8);
  margin-bottom: 36px;

  @media (max-width: 425px) {
    margin-bottom: 27px;
  }
`;

export const Price = styled.p`
  text-align: center;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  color: #000;
  letter-spacing: 0.019em;
  margin-bottom: 6px;
`;

export const PriceValue = styled.span`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  color: ${primary};
  letter-spacing: 0.019em;
`;

export const List = styled.ul`
  padding: 0 19px;

  @media (max-width: 425px) {
    padding: 0 33px;
  }
`;

export const ListItem = styled.li`
  font-family: ${poppins};
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 140%;
  color: ${black};
  position: relative;
  text-align: ${({theme: {isArabic}}) => (isArabic ? 'right' : 'left')};
  padding-left: ${({theme: {isArabic}}) => (isArabic ? '0' : '29px')};
  padding-right: ${({theme: {isArabic}}) => (isArabic ? '29px' : '0')};

  &:before {
    position: absolute;
    content: '';
    top: 0;
    left: ${({theme: {isArabic}}) => (isArabic ? 'auto' : '0')};
    right: ${({theme: {isArabic}}) => (isArabic ? '0' : 'auto')};
    background: url(${iconCheck});
    width: 18px;
    height: 13px;
  }

  & + & {
    margin-top: 24px;
  }

  @media (max-width: 425px) {
    & + & {
      margin-top: 22px;
    }
  }
`;

export const ButtonWrapper = styled.div`
  margin-top: auto;
  text-align: center;

  @media (max-width: 425px) {
    margin-top: 30px;
  }
`;

export const Button = styled.button`
  width: 160px;
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 140%;
  text-align: center;
  color: #242f3f;
  border: 1px solid #242f3f;
  border-radius: 24px;
  padding: 10px 0;
  letter-spacing: 0.3px;
  transition: ease 0.4s;

  &:hover {
    background-color: #000;
    color: #fff;
  }
`;
