import styled from 'styled-components/macro';
import {mainBlackColor as black} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 120px 20px 104px 20px;

  @media (max-width: 1024px) {
    padding: 65px 20px 150px 20px;
  }

  @media (max-width: 425px) {
    padding: 10px 24px 30px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;

  ${({theme: {isArabic}}) => isArabic && `direction: rtl`};

  @media (max-width: 425px) {
    max-width: none;
    margin: auto;
  }
`;

export const Title = styled.h3`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  color: ${black};
  margin-bottom: 71px;

  @media (max-width: 1024px) {
    text-align: center;
  }

  @media (max-width: 425px) {
    text-align: left;
    font-size: 24px;
    line-height: 140%;
    text-transform: capitalize;
    margin-bottom: 39px;
  }
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  height: 525px;

  @media (max-width: 675px) {
    flex-direction: column;
    height: auto;
  }
`;

export const Left = styled.div`
  flex: 1;
  margin-right: ${({theme: {isArabic}}) => (isArabic ? '0' : '211px')};
  padding-left: ${({theme: {isArabic}}) => (isArabic ? '211px' : '0')};
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media (max-width: 1024px) {
    margin-right: ${({theme: {isArabic}}) => (isArabic ? '0' : '50px')};
    padding-left: 0;
  }

  @media (max-width: 675px) {
    flex-direction: column;
    margin-right: 0;
  }
`;

export const Right = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const Question = styled.div`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 140%;
  color: ${black};

  @media (max-width: 675px) {
    & + & {
      margin-top: 20px;
    }
  }

  @media (max-width: 425px) {
    & + & {
      margin-top: 6px;
    }
  }
`;

export const QuestionTitle = styled.h4`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 140%;
  color: ${black};
  margin-bottom: 19px;

  @media (max-width: 675px) {
    flex-direction: column;
    margin-bottom: 16px;
  }

  @media (max-width: 425px) {
    margin-bottom: 30px;
    font-size: 18px;
    font-weight: 600;
  }
`;

export const QuestionText = styled.p`
  white-space: pre-line;
  font-family: ${poppins};
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 160%;
  color: rgba(0, 0, 0, 0.8);

  @media (max-width: 425px) {
    font-size: 14px;
    line-height: 140%;
  }
`;

export const IconWrapper = styled.div`
  position: absolute;
  right: 0;
  top: -2px;
  transition: ease 0.3s;
  transform: ${({active}) => (active ? 'rotate(270deg)' : 'rotate(90deg)')};
`;
