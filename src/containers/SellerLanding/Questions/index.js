import React from 'react';
import {useWindowSize} from '@reach/window-size';
import {useTranslation} from 'react-i18next';
import Expanded from 'components/Expanded';
import Icons from 'components/Icon';
import {Section, Wrapper, Title, Flex, Left, Right, Question, QuestionTitle, QuestionText, IconWrapper} from './styled';

const Questions = () => {
  const {t} = useTranslation('sellerLanding');
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  const data = [
    {
      id: 1,
      title: t('Questions.item1Title'),
      text: t('Questions.item1Text')
    },
    {
      id: 2,
      title: t('Questions.item2Title'),
      text: t('Questions.item2Text')
    },
    {
      id: 3,
      title: t('Questions.item3Title'),
      text: t('Questions.item3Text')
    },
    {
      id: 4,
      title: t('Questions.item4Title'),
      text: t('Questions.item4Text')
    },
    {
      id: 5,
      title: t('Questions.item5Title'),
      text: t('Questions.item5Text')
    },
    {
      id: 6,
      title: t('Questions.item6Title'),
      text: t('Questions.item6Text')
    }
  ];

  const ShowBtn = ({active}) => {
    return (
      <IconWrapper active={active}>
        <Icons type="arrow" color="#000" width={14} />
      </IconWrapper>
    );
  };

  return (
    <Section>
      <Wrapper>
        <Title>{t('Questions.title')}</Title>
        <Flex>
          <Left>
            {data.slice(0, 3).map(({id, title, text}) => {
              if (isMobile) {
                return (
                  <Question key={id}>
                    <Expanded
                      styles={{position: 'relative'}}
                      contentVisible={<QuestionTitle>{title}</QuestionTitle>}
                      hideBtn={<ShowBtn active />}
                      showBtn={<ShowBtn />}
                    >
                      <QuestionText>{text}</QuestionText>
                    </Expanded>
                  </Question>
                );
              }

              return (
                <Question key={id}>
                  <QuestionTitle>{title}</QuestionTitle>
                  <QuestionText>{text}</QuestionText>
                </Question>
              );
            })}
          </Left>
          <Right>
            {data.slice(3).map(({id, title, text}) => {
              if (isMobile) {
                return (
                  <Question key={id}>
                    <Expanded
                      styles={{position: 'relative'}}
                      contentVisible={<QuestionTitle>{title}</QuestionTitle>}
                      hideBtn={<ShowBtn active />}
                      showBtn={<ShowBtn />}
                    >
                      <QuestionText>{text}</QuestionText>
                    </Expanded>
                  </Question>
                );
              }

              return (
                <Question key={id}>
                  <QuestionTitle>{title}</QuestionTitle>
                  <QuestionText>{text}</QuestionText>
                </Question>
              );
            })}
          </Right>
        </Flex>
      </Wrapper>
    </Section>
  );
};

export default Questions;
