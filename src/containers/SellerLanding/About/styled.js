import styled from 'styled-components';
import {mainBlackColor as black, watermelon} from 'constants/colors';
import {poppins} from 'constants/fonts';

export const Section = styled.section`
  padding: 3px 20px;
`;

export const Title = styled.h2`
  font-family: ${poppins};
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  text-align: center;
  color: ${black};

  b {
    color: ${watermelon};
  }

  @media (max-width: 425px) {
    font-size: 18px;
    line-height: 130%;
  }
`;
