import React from 'react';
import {useTranslation} from 'react-i18next';
import {Section, Title} from './styled';

const About = () => {
  const {t} = useTranslation('sellerLanding');

  return (
    <Section>
      <Title>
        <b>{t('testSample')}</b> {t('About.title')}
        <br />
        {t('About.title2')}
      </Title>
    </Section>
  );
};

export default About;
