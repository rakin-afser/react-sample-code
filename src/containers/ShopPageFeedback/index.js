import React, {useEffect} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import useGlobal from 'store';
import Layout from 'containers/Layout';
import {useWindowSize} from '@reach/window-size';
import ShopPageFeedbackMobile from '../ShopPageFeedbackMobile';

const ShopPageFeedback = () => {
  const history = useHistory();
  const [, globalActions] = useGlobal();
  let {shopName} = useParams();
  const {width} = useWindowSize();

  const isMobile = width <= 767;

  const handleCategoryClick = (categorySlug) => {
    if (categorySlug !== 'all') {
      history.push(`/shop/${shopName}/categories/${categorySlug}`);
    }
  };

  useEffect(() => {
    globalActions.setHeaderBackButtonLink(`/shop/${shopName}/products`);
  }, [shopName]);

  return isMobile ? (
    <Layout
      isFooterShort
      hideHeaderLinks={true}
      showBurgerButton={false}
      showSearchButton={false}
      showBasketButton={false}
      showBackButton={true}
      hideCopyright={true}
      headerShadow={false}
      title="Feedback"
      layOutStyles={{
        paddingTop: 47
      }}
    >
      <ShopPageFeedbackMobile onCategoryClick={handleCategoryClick} />
    </Layout>
  ) : (
    'DESKTOP'
  );
};

export default ShopPageFeedback;
