import React from 'react';
import {useQuery} from '@apollo/client';
import {Helmet} from 'react-helmet';

import {TERMS_OF_USE_CONTENT} from '../../queries';

import Loader from '../../components/Loader';
import Wrapper from './styled';
import Layout from '../Layout';

const TermsConditions = () => {
  const {data, loading} = useQuery(TERMS_OF_USE_CONTENT);

  if (loading !== true) {
    const {
      page: {content, title}
    } = data;

    return (
      <Layout>
        <Wrapper>
          <Helmet>
            <title>{title || 'Terms &  Conditions'}</title>
            <meta name="description" content="testSample Terms & Conditions" />
          </Helmet>
          <div dangerouslySetInnerHTML={{__html: content}} />
        </Wrapper>
      </Layout>
    );
  }

  return <Loader />;
};

export default TermsConditions;
