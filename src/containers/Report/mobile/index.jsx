import React, {useEffect} from 'react';
import BackPage from 'components/Modals/mobile/BackPage';
import {Title, Radio, FieldTitle, AreaField, Container, Cancel} from '../styled';
import useGlobal from 'store';
import {Form, Input, message} from 'antd';
import {useUser} from 'hooks/reactiveVars';
import {useMutation} from '@apollo/client';
import {ADD_REPORT} from 'mutations';
import Btn from 'components/Btn';

const Report = (props) => {
  const [addReport, {data, loading}] = useMutation(ADD_REPORT, {
    onError() {
      message.error('Please try to sign in or check the form data.');
    }
  });

  const [{reportPopup}, {setReportPopup}] = useGlobal();
  const {form} = props;
  const {getFieldDecorator} = form;
  const [user] = useUser();
  const {active, subjectId, subjectSource} = reportPopup || {};

  const reasons = [
    'This content is spam',
    'This content should marked as adult',
    'This content is abusive',
    'This content is violent',
    'This content suggests the author might be risk of hurting themselves',
    'This content contains my private information',
    'Other'
  ];

  useEffect(() => {
    if (data) {
      setReportPopup({active: false});
      message.success('Your report has been sent successfully.');
    }
  }, [data, setReportPopup]);

  function onSubmit(e) {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        addReport({variables: {input: {...values, subjectId, subjectSource}}});
      }
    });
  }

  function onCancel() {
    setReportPopup({active: false});
  }
  return (
    <BackPage active={active} setActive={(v) => setReportPopup({active: v})} title="Report Abuse">
      <Title>Why are you reporting this?</Title>
      <Form onSubmit={onSubmit}>
        <Form.Item>
          {getFieldDecorator('reason', {
            rules: [{required: true, message: 'Please select the reason.'}]
          })(<Radio options={reasons} />)}
        </Form.Item>
        {user?.email ? null : (
          <>
            <FieldTitle>Your Name</FieldTitle>
            {getFieldDecorator('name')(<Input />)}
            <FieldTitle>Your Email</FieldTitle>
            <Form.Item>
              {getFieldDecorator('email', {
                rules: [{required: true, type: 'email', message: 'Please provide us correct email.'}]
              })(<Input />)}
            </Form.Item>
          </>
        )}
        <FieldTitle>Description</FieldTitle>
        {getFieldDecorator('description', {
          rules: [{required: true, message: 'Please select a description.'}]
        })(<AreaField rows="4" />)}
        <Container>
          <Cancel onClick={onCancel}>Cancel</Cancel>
          <Btn kind="primary" loading={loading} type="submit" min={186}>
            Report Abuse
          </Btn>
        </Container>
      </Form>
    </BackPage>
  );
};

export default Form.create({name: 'report'})(Report);
