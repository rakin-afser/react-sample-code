import {Button, Input} from 'antd';
import RadioGroup from 'antd/lib/radio/group';
import styled from 'styled-components';

const {TextArea} = Input;

export const Title = styled.h5`
  margin-top: 10px;
  margin-bottom: 16px;
  font-weight: 500;
  font-size: 18px;
  line-height: 1.22;
  color: #666666;
`;

export const Radio = styled(RadioGroup)`
  display: flex !important;
  flex-direction: column !important;

  .ant-radio-checked {
    .ant-radio-inner {
      border-color: #000 !important;
    }
  }

  .ant-radio-wrapper {
    padding-top: 14px;
    padding-bottom: 14px;
    white-space: normal;

    &-checked {
      font-weight: 700;
    }
  }

  .ant-radio {
    position: absolute;
    left: 0;
    top: 12px;
    color: #000;

    &-inner {
      width: 20px;
      height: 20px;
      border-width: 2px;

      &::after {
        width: 10px;
        height: 10px;
        background-color: #000;
      }
    }

    & + * {
      padding-left: 38px;
      padding-right: 0;
      display: inline-block;
      line-height: 1.3;
      color: #000;
    }
  }
`;

RadioGroup.defaultProps = {
  size: 'large'
};

export const FieldTitle = styled.p`
  margin-top: 16px;
  margin-bottom: 8px;
  color: #000;
  line-height: 1.1;
`;

export const AreaField = styled(TextArea)`
  max-height: 84px;
  resize: none;

  &.ant-input {
    margin-bottom: 40px;
  }
`;

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 27px;
`;

export const Cancel = styled(Button)`
  font-size: 14px !important;
  font-weight: 500;
  border-color: transparent !important;
`;

Cancel.defaultProps = {
  type: 'text',
  shape: 'round',
  size: 'large'
};
