import React from 'react';
import {useWindowSize} from '@reach/window-size';
import VerifyOPTComponent from './VerifyOPTComponent';

const VerifyOPTPage = ({onClickClose}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return <VerifyOPTComponent isMobile={isMobile} />;
};

export default VerifyOPTPage;
