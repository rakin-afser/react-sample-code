import React, {useState, useEffect} from 'react';
import {Auth} from 'aws-amplify';
import {useHistory, useLocation} from 'react-router-dom';
import Verify2 from '../GuestCheckoutPage/components/mobile/Verify2';
import {Wrapper} from './styled';
import {gql, useMutation} from '@apollo/client';

const ResetUserPassword = gql`
  mutation testSampleResetUserPassword($input: testSampleResetUserPasswordInput!) {
    testSampleResetUserPassword(input: $input) {
      code
      message
    }
  }
`;

const ValidateOTP = gql`
  mutation ValidateOTP($input: ValidateOTPInput!) {
    validateOTP(input: $input) {
      message
      code
    }
  }
`;

const SendOTP = gql`
  mutation SendOTP($input: SendOTPInput!) {
    sendOTP(input: $input) {
      message
      code
    }
  }
`;

const Register = gql`
  mutation Register($input: RegisterNewCustomerInput!) {
    registerNewCustomer(input: $input) {
      message
      user_id
    }
  }
`;

const VerifyOTPComponent = ({isMobile}) => {
  const {push} = useHistory();
  const location = useLocation();
  const [isSuccess, setIsSuccess] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isResending, setIsResending] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const [optError, setOptError] = useState('');

  const [validateOtp] = useMutation(ValidateOTP);
  const [sendOTP] = useMutation(SendOTP);
  const [register] = useMutation(Register);
  const [resetUserPassword] = useMutation(ResetUserPassword);

  const resendCode = async () => {
    setIsResending(true);
    setOptError(false);
    try {
      await sendOTP({
        variables: {
          input: {
            email: location?.state?.email
          }
        }
      });
      setErrorMessage({
        type: 'success',
        text: 'A new OTP code has been sent to your email.'
      });
      setIsResending(false);
    } catch (error) {
      console.log(error);
      setIsResending(false);
      setErrorMessage({
        type: 'error',
        title: 'Error',
        text: error.message
      });
    }
  };

  const confirmSignUp = async (code) => {
    setOptError('');
    setIsSubmitting(true);
    try {
      const {data: dataValidate} = await validateOtp({
        variables: {
          input: {
            email: location?.state?.email,
            otp: parseInt(code)
          }
        }
      });

      if (dataValidate?.validateOTP?.code === '404') {
        setErrorMessage({
          type: 'error',
          title: 'Error',
          text: 'Please enter valid OTP'
        });
        setIsSubmitting(false);
      }

      if (dataValidate?.validateOTP?.code === '200') {
        setErrorMessage({
          type: 'success',
          title: 'Success',
          text: 'Your account has been created.'
        });

        await register({
          variables: {
            input: {
              firstName: location?.state?.firstName,
              lastName: location?.state?.lastName,
              email: location?.state?.email,
              username: location?.state?.username,
              password: location?.state?.password
            }
          }
        });
        setIsSuccess(true);
        setIsSubmitting(false);
        setTimeout(() => push('/auth/sign-in'), 10000);
      }
    } catch (error) {
      setIsSubmitting(false);
      if (error.code === 'CodeMismatchException' || error.__type === 'CodeMismatchException') {
        setOptError('Invalid Code');
      } else {
        setErrorMessage({
          type: 'error',
          title: 'Error',
          text: error.message
        });
      }
    }
  };

  // const setNewPassword = async (otpCode, email, password) => {
  //   setOptError('');
  //   setIsSubmitting(true);
  //   try {
  //     const {data} = await resetUserPassword({variables: {input: {password, email, otpCode}}});
  //     if (data?.testSampleResetUserPassword.code === '200') {
  //       setIsSubmitting(false);
  //       setIsSuccess(true);
  //       setErrorMessage({
  //         type: 'success',
  //         title: 'Success!',
  //         text: 'Your password has been successfully reset.'
  //       });
  //       setIsSubmitting(false);
  //       setTimeout(() => push('/auth/sign-in'), 10000);
  //     }
  //     if (data?.testSampleResetUserPassword.code === '404') {
  //       setErrorMessage({
  //         type: 'error',
  //         title: 'Error',
  //         text: 'Please enter a valid OTP'
  //       });
  //       setIsSubmitting(false);
  //     }
  //   } catch (error) {
  //     console.log('setNewPasssword error', error);
  //     setErrorMessage({
  //       type: 'error',
  //       title: 'Error',
  //       text: error.message
  //     });
  //     setIsSubmitting(false);
  //   }
  // };

  const onSubmit = async (otpCode, email, password) => {
    if (location.state.type === 'resetPassword') {
      setOptError('');
      setIsSubmitting(true);
      try {
        const {data} = await resetUserPassword({variables: {input: {password, email, otpCode}}});
        if (data?.testSampleResetUserPassword.code === '200') {
          setIsSubmitting(false);
          setIsSuccess(true);
          setErrorMessage({
            type: 'success',
            title: 'Success!',
            text: 'Your password has been successfully reset.'
          });
          setIsSubmitting(false);
          setTimeout(() => push('/auth/sign-in'), 10000);
        }
        if (data?.testSampleResetUserPassword.code === '404') {
          setErrorMessage({
            type: 'error',
            title: 'Error',
            text: 'Please enter a valid OTP'
          });
          setIsSubmitting(false);
        }
      } catch (error) {
        console.log('setNewPasssword error', error);
        setErrorMessage({
          type: 'error',
          title: 'Error',
          text: error.message
        });
        setIsSubmitting(false);
      }
    } else {
      confirmSignUp(otpCode).then();
    }
  };

  // const resetPassword = async () => {
  //   setIsResending(true);
  //   try {
  //     await Auth.forgotPassword(location.state.email);
  //     setIsResending(false);
  //     setErrorMessage({
  //       type: 'success',
  //       text: 'A new OTP code has been sent to your email.'
  //     });
  //   } catch (error) {
  //     setIsResending(false);
  //     setErrorMessage({
  //       type: 'error',
  //       text: error.message
  //     });
  //   }
  // };

  useEffect(() => {
    if (!location.state || !location.state.email) push('/');
  }, [location.state]);

  return (
    <Wrapper>
      {location.state && location.state.email && (
        <Verify2
          isMobile={isMobile}
          email={location.state.email}
          errorMessage={errorMessage}
          isSubmitting={isSubmitting}
          isResending={isResending}
          onClickBack={() => push(location.state.backUrl ? location.state.backUrl : '/auth/sign-up')}
          onSubmit={onSubmit}
          onResend={resendCode}
          isSuccess={isSuccess}
          title={location.state.title ? location.state.title : 'Verify OTP'}
          type={location.state && location.state.type ? location.state.type : ''}
          optError={optError}
          setOptError={setOptError}
        />
      )}
    </Wrapper>
  );
};

export default VerifyOTPComponent;
