import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import useGlobal from 'store';

import followers1 from './img/followers1.png';
import followers2 from './img/followers2.png';
import defaultAvatar from 'images/avatarPlaceholder.png';
import {
  SellerAbout,
  SellerButtons,
  SellerChatBtn,
  SellerCity,
  SellerFollowBtn,
  SellerFollowers,
  SellerFollowersImg,
  SellerFollowersLink,
  SellerFollowersName,
  SellerInfoItem,
  SellerLogo,
  SellerName,
  SellerPic,
  SellerProfileItem,
  SellerProfileWrap,
  SellerRating,
  UserType
} from './styled';
import {ReactComponent as IconMessage} from './img/messageIcon.svg';
import {ReactComponent as IconPlus} from './img/plusIcon.svg';
import Avatar from 'components/Avatar';
import {useUser} from 'hooks/reactiveVars';

const SellerCard = ({setRating, sellerData}) => {
  const history = useHistory();
  const [, setGlobalStore] = useGlobal();
  const [user] = useUser();
  const isSeller = user?.capabilities?.includes('seller');

  const goShopPage = () => {
    history.push(`/shop/${sellerData?.storeUrl}/products`);
  };

  const dataForMessenger = {
    databaseId: sellerData?.id,
    username: sellerData?.name,
    photoUrl: sellerData?.gravatar
  };

  const onShowMessenger = () => {
    setGlobalStore.setMessenger({
      visible: true,
      minify: true,
      data: isSeller ? null : dataForMessenger
    });
  };

  return (
    <>
      <SellerProfileItem>
        <SellerProfileWrap>
          <SellerAbout>
            <SellerPic onClick={goShopPage}>
              <Avatar isOnline width={60} height={60} img={sellerData?.gravatar || defaultAvatar} />
            </SellerPic>
            <SellerCity>{sellerData?.location}</SellerCity>
          </SellerAbout>
          <SellerAbout>
            <UserType>Seller</UserType>
            <SellerInfoItem>
              <SellerName onClick={goShopPage}>{sellerData?.name}</SellerName>
            </SellerInfoItem>
            <SellerInfoItem>
              <SellerRating>
                {setRating(sellerData?.rating)}
                (602)
              </SellerRating>
              <SellerFollowers>{sellerData?.totalFollowers} followers</SellerFollowers>
            </SellerInfoItem>

            <SellerButtons>
              <SellerChatBtn onClick={onShowMessenger}>
                <IconMessage />
                Message
              </SellerChatBtn>
              <SellerFollowBtn>
                <IconPlus />
                Follow
              </SellerFollowBtn>
            </SellerButtons>
          </SellerAbout>
        </SellerProfileWrap>
        <SellerInfoItem marginTop>
          <SellerFollowersImg src={followers1} alt="img" />
          <SellerFollowersImg src={followers2} alt="img" />
          <SellerFollowersName>Sherylin Fenn and 2 others</SellerFollowersName>
          <SellerFollowersLink>follow that seller</SellerFollowersLink>
        </SellerInfoItem>
      </SellerProfileItem>
    </>
  );
};

SellerCard.propTypes = {
  setRating: PropTypes.func.isRequired,
  sellerData: PropTypes.shape().isRequired
};

export default SellerCard;
