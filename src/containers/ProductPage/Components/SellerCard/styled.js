import styled from 'styled-components/macro';

export const SellerProfileWrap = styled.div`
  display: flex;
`;

export const SellerProfileItem = styled.div`
  padding: 30px 30px 16px 30px;
  width: 383px;
  border: 1px solid #e4e4e4;
  border-radius: 8px;

  ${({alignRight}) =>
    alignRight
      ? `
		display: flex;
		flex-direction: column;
		align-items: flex-end;
		`
      : null};
`;

export const SellerPic = styled.div`
  position: relative;
  margin-right: 25px;
  cursor: pointer;
`;

export const UserType = styled.span`
  margin-top: -6px;
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #8f8f8f;
  padding-bottom: 2px;
`;

export const SellerAbout = styled.div``;

export const SellerInfoItem = styled.div`
  display: flex;
  align-items: center;
  margin-top: ${({marginTop}) => (marginTop ? '36px' : '0')};
  margin-left: ${({marginTop}) => (marginTop ? '20px' : '0')};
`;

export const SellerName = styled.div`
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 18px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
  cursor: pointer;
`;

export const SellerCity = styled.div`
  display: flex;
  justify-content: center;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #a7a7a7;
  margin: 10px 25px 0 0;
`;

export const SellerRating = styled.span`
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #7a7a7a;
  margin-top: 9px;

  & svg {
    margin-right: 4px;

    &:last-child {
      margin-right: 9px;
    }
  }
`;

export const SellerFollowers = styled.span`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  margin: 9px 0 0 17px;
`;

export const SellerButtons = styled.div`
  margin: 26px 0 0 0;
  display: flex;
  justify-content: space-between;
`;

export const SellerChatBtn = styled.button`
  padding: 0 13px;
  margin-right: 9px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  height: 28px;
  border: 1px solid #c3c3c3;
  box-sizing: border-box;
  border-radius: 24px;
  background: transparent;
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #545454;
  transition: ease 0.4s;
  cursor: pointer;

  svg {
    margin-right: 2px;
    transition: ease 0.4s;
  }

  &:hover {
    border: 1px solid #ed484f;
    background: #ed484f;
    color: #fff;

    svg {
      fill: #fff;
    }
  }
`;

export const SellerFollowBtn = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  width: 99px;
  height: 28px;
  background: #ed484f;
  border-radius: 24px;
  font-family: Helvetica Neue, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  border: 1px solid transparent;
  line-height: 140%;
  color: #fff;
  transition: ease 0.4s;
  cursor: pointer;

  svg {
    margin-right: 4px;
    transition: ease 0.4s;
  }

  &:hover {
    background: transparent;
    color: #545454;
    border: 1px solid #c3c3c3;

    svg {
      path {
        fill: #c3c3c3;
      }
    }
  }
`;

export const SellerFollowersImg = styled.img`
  width: 28px;
  height: 28px;
  margin-left: -15px;
`;

export const SellerFollowersName = styled.span`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 12px;
  line-height: 132%;
  color: #000000;
  margin: 0 0 0 8px;
`;

export const SellerFollowersLink = styled.a`
  text-decoration: none;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 12px;
  line-height: 132%;
  color: #ed484f;
  margin: 0 0 0 4px;
`;
