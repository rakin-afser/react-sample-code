import styled, {css} from 'styled-components/macro';
import {Radio, Select} from 'antd';
import ButtonCustom from 'components/ButtonCustom';
import {mainFont, secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const Description = styled.div`
  background: #fff;
  height: ${({quickView}) => (quickView ? '100%' : 'auto')};
`;

export const DescriptionTop = styled.div`
  padding-right: ${({quickView}) => (quickView ? '10px' : '57px')};
`;

export const Wrap = styled.div`
  height: 100%;
  padding-bottom: 21px;
  max-width: ${({quickView}) => (quickView ? '374px' : '382px')};
  margin-left: ${({quickView}) => (quickView ? '10px' : 'auto')};
  margin-right: ${({quickView}) => (quickView ? 'auto' : '0')};
  ${({additionalStyles}) => additionalStyles || null};

  ${({quickView}) =>
    quickView &&
    `
    display: flex;
    flex-direction: column;
  `};
`;

export const Title = styled.span`
  position: relative;
  top: -4px;
  display: block;
  padding-bottom: 3px;
  padding-right: 15px;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 1.3;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const Rating = styled.div`
  position: relative;
  left: -5px;
  display: flex;
  align-items: center;

  & svg {
    margin-right: -1px;

    &:last-child {
      margin-right: 0;
    }
  }
`;

export const LikesNumber = styled.div`
  position: relative;
  top: -3px;
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 1;
  color: #7a7a7a;
  margin-left: 4px;
`;

export const Deliver = styled.div`
  margin-top: 31px;
  padding-left: 3px;
  display: flex;
  align-items: center;
  ${({isModal}) => (isModal ? 'none' : {borderTop: '1px solid #e4e4e4', paddingTop: '23px'})};
`;

export const DeliverDescription = styled.div`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #000000;
  margin-right: 3px;
`;

export const DeliverCity = styled.div`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 700;
  font-size: 12px;
  line-height: 132%;
  color: #4a90e2;
`;

export const VerticalDivider = styled.i`
  width: 1px;
  height: 16px;
  background: #e4e4e4;
  margin: 0 12px;
`;

export const DeliverCost = styled.div`
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 700;
  font-size: 12px;
  line-height: 132%;
  text-transform: uppercase;
  color: #000000;
  margin-right: 3px;
`;

export const DeliverDate = styled.div`
  font-family: SF Pro Display, sans-serif;
  font-weight: 700;
  font-size: 12px;
  line-height: 132%;
  color: #208c4e;
`;

export const Price = styled.div`
  padding-right: ${({quickView}) => (quickView ? '37px' : '0')};
  display: flex;
  margin-top: 22px;
  position: relative;
  align-items: flex-end;
`;

export const PriceDescription = styled.div`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 1;
  color: #a7a7a7;
  margin-right: 5px;
  ${({realPrice}) =>
    realPrice
      ? css`
          margin-right: 13px;
          font-weight: 600;
          font-size: 18px;
          line-height: 124%;
          letter-spacing: 0.019em;
          padding-top: 15px;
          color: #000000;
        `
      : null};
  ${({red}) =>
    red
      ? css`
          color: #ed494f;
        `
      : null} ${({oldPrice}) =>
    oldPrice
      ? css`
          text-decoration: line-through;
          position: absolute;
          top: 0;
          left: 0;
        `
      : null};

  ${({discount}) =>
    discount
      ? css`
          font-weight: 700;
          font-size: 14px;
          line-height: 140%;
          color: #ed494f;
        `
      : null};

  span {
    margin-right: 2px;
    font-family: ${mainFont};
    color: #8f8f8f;
    font-weight: 500;
    font-size: 14px;
  }
`;

export const Coins = styled.div`
  display: flex;
  align-items: center;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #398287;
  margin-top: 2px;
  margin-bottom: 15px;

  svg {
    margin-top: -1px;
    margin-left: 4px;
  }
`;

export const CoinsNumber = styled.div`
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #398287;
`;

export const HorizontalDivider = styled.i`
  display: block;
  width: 100%;
  height: 1px;
  background: #e4e4e4;
`;

export const Detailes = styled.div`
  margin-top: ${({quickView}) => (quickView ? '23px' : '11px')};
`;

export const DetailesTop = styled.div`
  padding-left: ${({quickView}) => (quickView ? '0' : '17px')};
`;

export const Feature = styled.div`
  padding-left: ${({quickView}) => (quickView ? '0' : '8px')};
  width: 100%;
  margin-bottom: 14px;
`;

export const FeatureName = styled.div`
  display: block;
  width: 58px;
  font-family: ${({dark}) => (dark ? secondaryFont : mainFont)};
  font-weight: 400;
  font-size: ${({dark}) => (dark ? '12px' : '14px')};
  line-height: 132%;
  color: ${({dark}) => (dark ? '#464646' : '#7A7A7A')};
  color: ${({red}) => (red ? '#ED484F' : '#7A7A7A')};
  margin: ${({brand}) => (brand ? '0' : '5px 12px 8px 0')};
`;

export const SizeGuideLink = styled.button`
  position: relative;
  right: -22px;
  top: -3px;
  display: block;
  width: 79px;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #4a90e2;
  background: transparent;
  border: none;
  margin-right: 0;
  padding: 0;
  cursor: pointer;
  text-decoration: none;
  outline: none;
  transition: ease 0.4s;

  &:hover {
    color: #000;
  }
`;

export const Text = styled.div`
  font-family: ${mainFont};
  font-weight: 400;
  font-size: 14px;
  line-height: 132%;
  color: #464646;
  margin: 0 0 0 36px;
`;

export const Count = styled.span`
  margin: 0 11px;
`;

export const BuyButton = styled.button`
  margin-bottom: ${({addToCart}) => (addToCart ? '16px' : '0')};
  margin-right: ${({addToCart}) => (addToCart ? '20px' : '0')};
  display: block;
  justify-content: center;
  align-items: center;
  width: ${({addToCart}) => (addToCart ? '100%' : '100%')};
  max-width: ${({quickView}) => (quickView ? '156' : 'none')};
  height: 36px;
  background: ${({addToCart}) => (addToCart ? 'transparent' : '#ed484f')};
  color: ${({addToCart}) => (addToCart ? '#ed484f' : '#ffffff')};
  border: ${({addToCart}) => (addToCart ? '1px solid #ed484f' : 'none')};
  outline: none;
  text-decoration: none;
  font-family: Helvetica Neue, sans-serif;
  font-weight: 700;
  cursor: ${({disabled}) => (disabled ? 'not-allowed' : 'pointer')};
  border-radius: 24px;
  transition: ease 0.3s;

  &:disabled {
    background-color: #ccc;
    border-color: #ccc;
    cursor: not-allowed;
    color: #fff;
  }

  ${({quickView}) =>
    quickView &&
    `
    background: transparent;
    color: #666666;
    border: 1px solid #666666;`}
  svg {
    margin-right: 6px;
  }

  div {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  &:hover:not([disabled]) {
    background-color: ${({addToCart}) => (addToCart ? 'transparent' : '#fff')};
    color: ${({addToCart}) => (addToCart ? '#000' : '#000')};
    border: ${({addToCart}) => (addToCart ? '1px solid #000' : '1px solid #000')};
    box-shadow: 0 0 3px rgba(000, 000, 000, 0.3);
  }

  &.BuyButton__addedToCart {
    background-color: transparent;
    color: #545454 !important;
    border: 1px solid #c3c3c3;
  }
`;

export const Buttons = styled.div`
  display: ${({quickView}) => (quickView ? 'flex' : 'block')};
  justify-content: ${({quickView}) => quickView && 'space-between'};
  padding: ${({quickView}) => (quickView ? '33px 0 0 0' : '8px 0 0 0')};
  position: relative;
  left: ${({quickView}) => (quickView ? '0' : '8px')};
  width: ${({quickView}) => (quickView ? '346px' : '308px')};
  margin: ${({quickView}) => (quickView ? 'auto 0 8px 0' : '0 0 27px;')};
`;

export const Return = styled.div`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-size: 12px;
  line-height: 132%;
  color: #ed484f;
  text-decoration: none;
  margin-top: 2px;
`;

export const Payment = styled.div`
  display: flex;
  align-items: center;

  & svg {
    margin-right: 20px;

    &:last-child {
      margin-right: 0;
    }
  }
`;

export const PaymentCash = styled.div`
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 500;
  font-size: 9px;
  line-height: 140%;
  color: #000000;
`;

export const Bottom = styled.div`
  justify-content: space-between;
  display: flex;

  svg {
    cursor: pointer;
  }

  ${({stylesForModal}) =>
    stylesForModal
      ? {margin: '14px 0 5px 25px'}
      : {
          margin: '18px 27px 0 25px',
          paddingBottom: '25px'
        }}
`;

export const Icon = styled.i`
  cursor: pointer;
  display: flex;
  align-items: center;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  font-style: normal;
  line-height: 132%;
  color: #464646;
  margin-right: 36px;

  & svg {
    margin-right: 8px;
  }
`;

export const StyledSelect = styled(Select)`
  &&& {
    & .ant-select-selection--single,
    .ant-select-selection--multiple {
      width: ${({quickView}) => (quickView ? '345px' : '308px;')};
      height: 40px;
    }

    & .ant-select-selection-selected-value {
    }

    & .ant-select-selection {
      border-radius: 2px;
    }

    & .ant-select-selection__rendered {
      height: 100%;
      line-height: 40px;
      margin: 0 16px;
    }

    & .ant-select-selection-selected-value {
      display: flex !important;
      align-items: center;
      font-family: Helvetica Neue, sans-serif;
      font-size: 14px;
      color: #545454;

      ${({color}) =>
        color
          ? css`
              &::before {
                content: '';
                display: block;
                width: 12px;
                height: 12px;
                border-radius: 50%;
                background: ${color};
                margin: 0 8px 0 0;
              }
            `
          : null}
    }

    & .ant-select-arrow {
      top: 21px;
      right: 19px;
      transition: ease 4s;
    }

    & .ant-select-open {
      .ant-select-arrow {
        transform: rotate(90deg);
      }
    }
  }
`;

export const Option = styled(Select.Option)`
  &&& {
    display: flex !important;
    align-items: center;
    font-family: 'Helvetica', sans-serif;
    font-weight: 400;
    font-size: 144px;
    line-height: 140%;
    color: ${({isDisabled}) => (isDisabled ? 'red' : 'red')};

    &::before {
      content: '';
      display: block;
      width: 12px;
      height: 12px;
      border-radius: 50%;
      background: #db95d1;
      margin: 0 8px 0 0;
    }
  }

  & .ant-select-arrow {
    top: 21px;
    right: 19px;
  }
`;

export const StyledRadioGroup = styled(Radio.Group)`
  &&& {
    display: grid;
    gap: 8px;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    max-width: 315px;
    justify-items: center;
  }
`;

export const StyledRadioButton = styled(Radio.Button)`
  &&& {
    opacity: ${({isDisabled}) => (isDisabled ? 0.2 : 1)};
    border-radius: 12px;
    width: 55px;
    height: 52px;
    border: 1px solid transparent !important;
    position: relative;
    box-shadow: none !important;
    outline: none !important;
    transition: opacity 0.1s ease-in, border-color 0.1s ease-in;

    &.ant-radio-button-wrapper-checked {
      border-color: #ccc !important;

      i {
        opacity: 1;
      }
    }

    &::before {
      display: none;
    }

    &:after {
      content: '';
      position: absolute;
      top: 0;
      right: 0;
      bottom: 0;
      left: 0;
      background-color: ${({color}) => color || 'transparent'};
      border: 1px solid #d8d8d8;
      border-radius: inherit;
      margin: 5px;
    }

    i {
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      opacity: 0;
      transition: opacity 0.1s ease-in;
      z-index: 1;
    }
  }
`;

export const ReturnTitle = styled.span`
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 400;
  font-size: 12px;
  line-height: 132%;
  color: #464646;
`;

// export const CloseCommentsContainer = styled.p`
//   margin-bottom: 0;
//   display: flex;
//   align-items: center;
//   justify-content: flex-end;
//   cursor: pointer;
// `;

export const FooterCol = styled.span`
  display: flex;
`;

export const BackBtn = styled.button`
  align-self: baseline;
  padding-left: 5px;
  padding-right: 10px;
  margin-bottom: 15px;
  border: 1px solid #e4e4e4;
  border-radius: 24px;
  font-weight: 500;
  font-size: 12px;
  color: #000;
  transition: ease 0.4s;

  svg {
    position: relative;
    top: -1px;
    transform: rotate(90deg) scale(0.88);

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    background-color: #000;
    color: #fff;

    svg {
      path {
        fill: #fff;
      }
    }
  }
`;

export const StyledCustomButton = styled(ButtonCustom)`
  height: 36px;
  margin-right: 26px;
  transition: ease 0.4s;

  &:hover:not([disabled]) {
    background-color: ${({loading}) => (loading ? 'transparent' : primaryColor)};
    color: #fff;
  }
`;

export const Error = styled.p`
  color: ${primaryColor};
`;
