import React, {useEffect, useState} from 'react';
import {useHistory} from 'react-router-dom';
import ClampLines from 'react-clamp-lines';
import PropTypes from 'prop-types';
import CommentsComponent from 'components/Comments';
import ItemInfo from 'components/ItemInfo';
import CoinsIcon from 'assets/ProductPage/Coins';
import AmericanExpress from 'assets/ProductPage/AmericanExpress';
import Mastercard from 'assets/ProductPage/Mastercard';
import Paypal from 'assets/ProductPage/Paypal';
import Visa from 'assets/ProductPage/Visa';
import {CSSTransition} from 'react-transition-group';
import Grid from 'components/Grid';
import StarRating from 'components/StarRating';
import SizeGuide from 'components/SizeGuide/SizeGuideApp';
import PostContent from 'components/PostContent/';
import AddedPopup from '../AddedPopup';
import {getCheckoutLink, getDiscountPercent, getVariationByAttrs, renderCoinsByVariation} from 'util/heplers';
import {useVariations} from 'hooks';
import Icon from 'components/Icon';

import {
  Buttons,
  BuyButton,
  Coins,
  CoinsNumber,
  StyledSelect,
  StyledRadioGroup,
  StyledRadioButton,
  Deliver,
  DeliverCity,
  DeliverCost,
  DeliverDate,
  DeliverDescription,
  Description,
  DescriptionTop,
  Detailes,
  DetailesTop,
  Feature,
  FeatureName,
  LikesNumber,
  Option,
  Payment,
  Price,
  PriceDescription,
  Rating,
  Return,
  Text,
  ReturnTitle,
  Title,
  SizeGuideLink,
  VerticalDivider,
  HorizontalDivider,
  Wrap,
  BackBtn,
  StyledCustomButton,
  Error
} from './styled';
import {ReactComponent as ArrowIcon} from './img/arrow.svg';
import {useQuery} from '@apollo/client';
import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import {useAddToCartMutation} from 'hooks/mutations';
import {CART_PRODUCTS} from 'queries';
import parse from 'html-react-parser';

const exampleData = {
  title: '2018 Floral Dresses Vestido De Festa Vestido De Festa',
  likes: '602',
  price: '499.99',
  oldPrice: '699',
  discount: '40%',
  midCoins: '19',
  brand: 'Zara',
  parameters: [
    {
      id: 1,
      title: 'Color',
      options: [
        {id: 1, value: 'Pink', additionalClass: '-pink'},
        {id: 2, value: 'Yellow', additionalClass: '-yellow'},
        {id: 3, value: 'Red', additionalClass: '-red'},
        {id: 4, value: 'Grey', additionalClass: '-grey'},
        {id: 5, value: 'Black', additionalClass: '-black'},
        {id: 6, value: 'Orange', additionalClass: '-orange'},
        {id: 7, value: 'Green', additionalClass: '-green'}
      ]
    },
    {
      id: 2,
      title: 'Size',
      options: [
        {id: 1, value: 'UK XS, 42', additionalClass: ''},
        {id: 2, value: 'UK S, 36', additionalClass: ''},
        {id: 3, value: 'UK M, 40', additionalClass: ''},
        {id: 4, value: 'UK L, 44', additionalClass: ''},
        {id: 5, value: 'UK XL, 48', additionalClass: ''},
        {id: 6, value: 'UK XXL, 52', additionalClass: ''},
        {id: 7, value: 'UK XXXL, 58', additionalClass: ''}
      ]
    }
  ],
  post: {
    title: "FILT'R INSTANT RETOUCH PALLETTE",
    description:
      'For Rihanna, creating the perfect soft matte base is the most important part of any look Trendy overview for last season, tenden collaboration, FW The magazine covers international, national and local fashion and beauty trends and news. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'
  }
};

const OrderInfo = ({
  returnsInfoDisplaying,
  paymentsInfoDisplaying,
  isModal,
  isMulipleModal,
  onlyFooter,
  postDiplaying,
  quickView,
  productData,
  setMainImage,
  author,
  setPostSelection,
  inCart,
  setInCart,
  setOrderInfoForMultiple,
  orderInfoForMultiple,
  multiple,
  displayPostInfo = true,
  postId,
  listId
}) => {
  const [user] = useUser();
  const [, globalActions] = useGlobal();
  const history = useHistory();
  const [isAddedToCart, setIsAddedToCart] = useState(false);
  const [isOpenGuide, setIsOpenGuide] = useState(false);
  const [commentsDisplaying, setCommentsDisplaying] = useState(false);

  const {slug, name = '', averageRating, availableShipping, stockQuantity} = productData?.product || {};

  const [variations, varState, setVarState] = useVariations(productData?.product);

  const {data: dataCart} = useQuery(CART_PRODUCTS);
  const productInCart = dataCart?.cart?.contents?.nodes?.some((content) => content?.product?.node?.slug === slug);

  const [addToCart, {loading: loadingAddToCart, error: errorAddToCart}] = useAddToCartMutation({
    onCompleted() {
      if (!quickView) {
        setIsAddedToCart(true);
      }
    }
  });

  const [buyNow, {loading: loadingBuyNow, error: errorBuyNow}] = useAddToCartMutation({
    onCompleted(data) {
      history.push(getCheckoutLink(data?.addToCart?.cart?.subcarts?.subcarts, user));
    }
  });

  let purchaseSource = postId ? {source_type: 'FEED_POST', id: +postId} : {};
  purchaseSource = listId ? {source_type: 'LIST', id: +listId} : purchaseSource;

  // const brand = productData?.product?.attributes?.nodes.find((el) => el.name === 'Brand')?.options[0];
  const brand = productData?.product?.productBrands?.nodes?.[0]?.name;
  const onAddToCart = () => {
    if (productInCart) {
      globalActions.setQuickView(null);
      history.push('/cart');
      return;
    }

    if (productData) {
      const {product} = productData;

      addToCart({
        variables: {
          input: {
            productId: product.databaseId,
            quantity: 1,
            variationId: variation?.databaseId,
            purchase_source: purchaseSource
          }
        }
      });
    }
  };

  const onBuyNow = () => {
    if (productInCart) {
      globalActions.setQuickView(null);
      history.push(getCheckoutLink(dataCart?.cart?.subcarts?.subcarts, user));
      return;
    }

    if (productData) {
      const {product} = productData;
      buyNow({
        variables: {
          input: {
            productId: product.databaseId,
            quantity: 1,
            variationId: variation?.databaseId,
            purchase_source: purchaseSource
          }
        }
      });
    }
    globalActions.setQuickView(null);
  };

  const onGoToProduct = () => {
    globalActions.setQuickView(null);
    const params = postId ? `?postId=${postId}` : '';
    history.push(`/product/${slug}${params}`);
  };

  const switchToPost = () => {
    if (!orderInfoForMultiple) {
      setPostSelection(true);
    } else {
      setOrderInfoForMultiple(false);
    }
  };

  function onAdd() {
    if (productInCart) {
      globalActions.setQuickView(null);
      history.push('/cart');
      return;
    }

    if (productData) {
      const {product} = productData;

      addToCart({
        variables: {
          input: {
            productId: product.databaseId,
            quantity: 1,
            variationId: variation?.databaseId,
            purchase_source: purchaseSource
          }
        }
      });
    }
  }

  const variation = getVariationByAttrs(productData?.product, varState);
  const {price, salePrice, regularPrice, currencySymbol: curSymb} = variation || productData?.product || {};
  const sale = getDiscountPercent(regularPrice, price);

  useEffect(() => {
    if (variation && setMainImage) {
      setMainImage(quickView ? variation?.image : variation?.image?.sourceUrl);
    }
  }, [variation, quickView, setMainImage]);

  function renderVariations() {
    const onChange = (value, label) => {
      setVarState({...varState, [label]: value});
    };

    return Object.keys(variations || {})?.map((varName) => {
      const {placeholder, options} = variations[varName] || {};

      switch (placeholder) {
        case 'Color':
          return (
            <Feature key={placeholder} quickView={quickView}>
              <FeatureName>{placeholder}:</FeatureName>
              <StyledRadioGroup onChange={(e) => onChange(e.target.value, varName)} value={varState[varName]}>
                {options?.map((option) => (
                  <StyledRadioButton
                    isDisabled={option.disabled ? 1 : 0}
                    color={option?.label}
                    key={option?.value}
                    value={option?.value}
                  >
                    <Icon type="checkbox" color="#000" />
                  </StyledRadioButton>
                ))}
              </StyledRadioGroup>
            </Feature>
          );

        default:
          return (
            <Feature key={placeholder} quickView={quickView}>
              <FeatureName>{placeholder}:</FeatureName>
              <StyledSelect
                onChange={(v) => onChange(v, varName)}
                quickView={quickView}
                suffixIcon={<ArrowIcon />}
                placeholder={`Select ${placeholder}`}
                value={varState[varName]}
              >
                {options?.map((option) => (
                  <Option key={option.value} value={option.value} isDisabled={option.disabled}>
                    <span style={{opacity: option.disabled ? 0.2 : 1}}>{option.label}</span>
                  </Option>
                ))}
              </StyledSelect>
            </Feature>
          );
      }
    });
  }

  function renderDelivery() {
    const {city, available} = availableShipping || {};

    if (!available) return null;

    return (
      <Deliver isModal={isModal}>
        <DeliverDescription>Deliver to</DeliverDescription>
        <DeliverCity>{city}</DeliverCity>
        {/* <VerticalDivider />
        <DeliverCost>Free</DeliverCost>
        <DeliverDescription>delivery by</DeliverDescription>
        <DeliverDate>Sat, Nov 23</DeliverDate> */}
      </Deliver>
    );
  }

  const renderAddToCard = () => {
    if (loadingAddToCart) {
      return <Icon type="loader" fill="#ed484f" />;
    }

    return productInCart ? 'Go to Cart' : 'Add to Cart';
  };

  const renderBuyNow = () => {
    if (loadingBuyNow) {
      return <Icon type="loader" fill="#ed484f" />;
    }

    return 'Buy now';
  };

  return (
    <>
      <Description quickView={quickView}>
        {postDiplaying ? (
          <PostContent
            data={exampleData.post}
            withComments={commentsDisplaying}
            changeCommentsDsiplaying={setCommentsDisplaying}
          />
        ) : (
          <Wrap quickView={quickView} additionalStyles={isModal && isMulipleModal ? 'padding-bottom: 110px' : null}>
            {!postDiplaying && multiple && (
              <BackBtn onClick={switchToPost}>
                <ArrowIcon /> Back
              </BackBtn>
            )}
            <DescriptionTop quickView={quickView}>
              {name ? (
                <Title>
                  <ClampLines buttons={false} text={name} lines={3} />
                </Title>
              ) : null}
              <Rating>
                <StarRating stars={averageRating} />
                {averageRating ? <LikesNumber>({averageRating})</LikesNumber> : null}
              </Rating>
              <Price quickView={quickView}>
                {salePrice ? <PriceDescription oldPrice>{regularPrice}</PriceDescription> : null}
                {price || regularPrice ? (
                  <PriceDescription red={salePrice ? 1 : 0} realPrice>
                    {salePrice ? price : regularPrice}
                  </PriceDescription>
                ) : null}
                {salePrice && sale ? <PriceDescription discount>-{sale}%</PriceDescription> : null}
              </Price>
              <Coins>
                Mid Coins:
                <CoinsNumber>+{renderCoinsByVariation(productData?.product?.midCoins, variation)}</CoinsNumber>
                <CoinsIcon />
              </Coins>
            </DescriptionTop>

            {commentsDisplaying ? (
              <CommentsComponent data={productData} quickView={quickView} hideComments={setCommentsDisplaying} />
            ) : (
              <>
                <HorizontalDivider />
                <Detailes quickView={quickView}>
                  {brand && (
                    <Grid aic margin="0 0 20px 0">
                      <FeatureName brand>Brand:</FeatureName>
                      <Text>{brand}</Text>
                    </Grid>
                  )}
                  {errorAddToCart ? <Error>{parse(errorAddToCart?.message || '')}</Error> : null}
                  {errorBuyNow ? <Error>{parse(errorBuyNow?.message || '')}</Error> : null}
                  <DetailesTop quickView={quickView}>
                    {renderVariations()}
                    <Buttons quickView={quickView}>
                      {!quickView && (
                        <BuyButton disabled={variations && !variation} addToCart onClick={onAdd}>
                          {renderAddToCard()}
                        </BuyButton>
                      )}
                      {quickView && (
                        <StyledCustomButton
                          onClick={onAddToCart}
                          disabled={variations && !variation}
                          isLoading={loadingAddToCart}
                          title={productInCart ? 'Go to Cart' : 'Add to Cart'}
                          variant="outlined"
                          color="primaryColor"
                          loading={loadingAddToCart ? 1 : 0}
                        />
                      )}
                      {quickView ? (
                        <BuyButton onClick={onGoToProduct} quickView>
                          Go To Product Page
                        </BuyButton>
                      ) : (
                        <BuyButton onClick={onBuyNow} disabled={variations && !variation}>
                          {renderBuyNow()}
                        </BuyButton>
                      )}
                    </Buttons>
                  </DetailesTop>
                  {renderDelivery()}
                  {returnsInfoDisplaying && (
                    <Grid margin="10px 0 13px 0">
                      <FeatureName dark>Returns</FeatureName>
                      <div>
                        <ReturnTitle>Returns and Exchanges accepted</ReturnTitle>
                        <Return>See return policy</Return>
                      </div>
                    </Grid>
                  )}
                  {paymentsInfoDisplaying && (
                    <Grid margin="0 0 18px 0">
                      <FeatureName dark>Payments</FeatureName>
                      <Payment>
                        <Mastercard />
                        <Visa />
                        <Paypal />
                        <AmericanExpress />
                      </Payment>
                    </Grid>
                  )}
                </Detailes>
              </>
            )}
            {!commentsDisplaying && displayPostInfo && (
              <ItemInfo data={productData} quickView={quickView} displayComments={setCommentsDisplaying} />
            )}
          </Wrap>
        )}

        <CSSTransition in={isAddedToCart} timeout={300} unmountOnExit>
          <AddedPopup variation={variation} productData={productData?.product} close={setIsAddedToCart} />
        </CSSTransition>
        <CSSTransition in={isOpenGuide} timeout={300} unmountOnExit>
          <SizeGuide close={isOpenGuide} setClose={setIsOpenGuide} />
        </CSSTransition>
      </Description>
    </>
  );
};

OrderInfo.defaultProps = {
  returnsInfoDisplaying: true,
  paymentsInfoDisplaying: true,
  isModal: false,
  isMulipleModal: false,
  onlyFooter: false,
  postDiplaying: false,
  quickView: false,
  productData: exampleData,
  postData: exampleData.post,
  setPostSelection: () => {}
};

OrderInfo.propTypes = {
  returnsInfoDisplaying: PropTypes.bool,
  paymentsInfoDisplaying: PropTypes.bool,
  isModal: PropTypes.bool,
  isMulipleModal: PropTypes.bool,
  onlyFooter: PropTypes.bool,
  postDiplaying: PropTypes.bool,
  quickView: PropTypes.bool,
  productData: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.any), PropTypes.shape()]),
  postData: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.any), PropTypes.shape()]),
  setPostSelection: PropTypes.func
};

export default OrderInfo;
