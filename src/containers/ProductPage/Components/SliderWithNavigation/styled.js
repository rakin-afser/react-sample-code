import styled from 'styled-components/macro';
import zoomIn from 'containers/ProductPage/img/zoom-in.png';

export const Slider = styled.div`
  display: flex;
  justify-content: ${({fullScreen}) => (fullScreen ? 'center' : 'flex-start')};
  height: ${({fullScreen}) => (fullScreen ? '100%' : 'auto')};
  padding-top: 5px;
  margin: 0 24px 0 0;
`;

export const SliderTabList = styled.div`
  position: relative;
  left: ${({fullScreen}) => (fullScreen ? '-20px' : '0')};
`;

export const SliderTab = styled.div`
  margin: 0 0 4px 0;
  border: ${({active}) => (active ? '2px solid #ED494F' : '0px solid transparent')};
  opacity: ${({active}) => (active ? '1' : '0.7')};
  background-color: #c4c4c4;
  box-sizing: border-box;
  width: 60px;
  height: 60px;
  cursor: pointer;
  transition: opacity ease 0.4s;

  &:hover {
    opacity: 1;
  }
`;

export const SliderPreview = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  object-position: center;
`;

export const SliderSlide = styled.div`
  display: flex;
  position: relative;
  left: ${({fullScreen}) => (fullScreen ? '-20px' : '0')};
  width: 100%;
  max-width: ${({fullScreen}) => (fullScreen ? '897px' : '635px')};
  height: ${({fullScreen}) => (fullScreen ? '100%' : '592px')};
  margin: 0 0 0 8px;
  overflow: ${({fullScreen}) => (fullScreen ? 'visible' : 'hidden')};

  & .slick-track {
    align-items: flex-start;
  }

  & .slick-slider {
    width: 100%;
    height: 100%;

    img {
      object-fit: contain;
      height: ${({fullScreen}) => (fullScreen ? 'calc(100vh - 70px)' : '592px')};
    }

    & .video-js {
      width: ${({fullScreen}) => (fullScreen ? '897px' : '635px')};
      height: ${({fullScreen}) => (fullScreen ? 'calc(100vh - 70px)' : '592px')};

      .vjs-big-play-button {
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
    }
  }

  img {
    object-fit: contain;
  }

  & .slick-arrow {
    z-index: 10;
    top: ${({fullScreen}) => (fullScreen ? 'calc(50% - 4px)' : 'calc(50% - 6px)')};
    background: #ffffff;
    opacity: 0.3;
    border-radius: 3px;
    width: 63px;
    height: 63px;
    transition: ease 0.4s;

    &::before {
      width: 43px !important;
    }

    &.slick-next {
      right: ${({fullScreen}) => (fullScreen ? '-10.4vw' : '26px')};
    }

    &.slick-prev {
      left: ${({fullScreen}) => (fullScreen ? '-10.4vw' : '24px')};

      &::before {
        position: relative;
        left: 24px;
      }
    }

    &:hover {
      background: #ffffff;
      opacity: 0.5;
    }
  }

  svg {
    position: absolute;
    right: 45px;
    bottom: 30px;
    z-index: 20;
    cursor: pointer;

    path {
      transition: ease 0.4s;
    }

    &:hover {
      path {
        fill: #b8bebc;
      }
    }
  }
`;
export const SliderPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
    // cursor: url(${zoomIn}), auto;
  margin: auto;
  //&:hover {
  //  max-width: none;
  //  max-height: none;
  //  min-width: 200%;
  //}
`;
