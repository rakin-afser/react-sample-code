import React, {useState, useRef} from 'react';
import SlickSlider from 'react-slick';
import PropTypes from 'prop-types';
import FullScreenModal from 'containers/ProductPage/Components/FullScreenModal';
import {Slider, SliderPic, SliderPreview, SliderSlide, SliderTab, SliderTabList} from './styled';
import VideoPlayer from 'components/VideoPlayer';
import {ReactComponent as ZoomInIcon} from './img/zoomIn.svg';

const SliderWithNavigation = ({
  slides,
  productVideo,
  slidesPreview,
  zoomIn,
  fullScreen,
  currentSlide,
  fade,
  ...props
}) => {
  const [productSlideActive, setProductSlideActive] = useState(currentSlide);
  const [modal, showModal] = useState(false);
  const productSliderRef = useRef(null);

  function onSlideChange(e) {
    const nextSlide = e.target.getAttribute('data-slide');
    productSliderRef.current.slickGoTo(nextSlide);
  }

  function getTabs() {
    return slidesPreview.map((item, i) => (
      <SliderTab key={i} active={i === productSlideActive} onClick={onSlideChange}>
        <SliderPreview src={item} data-slide={i} />
      </SliderTab>
    ));
  }

  const mouseMoveHandler = (ev) => {
    const t = ev.target;
    if (ev.type === 'mouseout') {
      t.style.top = ``;
      t.style.left = ``;
      t.style.position = ``;
      return;
    }
    const slide = t.parentNode;
    const coors = slide.getBoundingClientRect();
    const mouseCoors = {top: ev.clientY, left: ev.clientX};
    t.style.position = `absolute`;
    t.style.top = `${coors.top - mouseCoors.top}px`;
    t.style.left = `${coors.left - mouseCoors.left}px`;
  };

  return (
    <>
      <Slider fullScreen={fullScreen} {...props}>
        <SliderTabList fullScreen={fullScreen}>{getTabs()}</SliderTabList>
        <SliderSlide fullScreen={fullScreen}>
          {slides.length > 1 ? (
            <SlickSlider
              initialSlide={currentSlide}
              fade={fade}
              ref={productSliderRef}
              arrows
              lazyLoad
              afterChange={(index) => {
                setProductSlideActive(index);
              }}
            >
              {productVideo && (
                <VideoPlayer onStop={productSlideActive !== currentSlide} loop={false} src={productVideo} />
              )}
              {slides?.map((el, i) => {
                return (
                  <SliderPic
                    src={el}
                    key={i}
                    alt="product img"
                    // onMouseMove={(ev) => mouseMoveHandler(ev)}
                    // onMouseOut={(ev) => mouseMoveHandler(ev)}
                  />
                );
              })}
            </SlickSlider>
          ) : (
            <img src={slides[0]} alt="gallery slide" />
          )}

          {zoomIn && <ZoomInIcon onClick={() => showModal(true)} />}
        </SliderSlide>
        {/*<ZoomBox>*/}
        {/*  */}
        {/*</ZoomBox>*/}
      </Slider>
      <FullScreenModal
        productVideo={productVideo}
        slides={slides}
        displaying={modal}
        currentSlide={productSlideActive}
        slidesPreview={slidesPreview}
        closeModal={() => showModal(false)}
      />
    </>
  );
};

SliderWithNavigation.defaultProps = {
  zoomIn: true,
  fullScreen: false,
  currentSlide: 0,
  fade: false,
  productVideo: null
};

SliderWithNavigation.propTypes = {
  slides: PropTypes.arrayOf(PropTypes.any).isRequired,
  slidesPreview: PropTypes.arrayOf(PropTypes.any).isRequired,
  zoomIn: PropTypes.bool,
  fullScreen: PropTypes.bool,
  currentSlide: PropTypes.number,
  fade: PropTypes.bool,
  productVideo: PropTypes.string
};

export default SliderWithNavigation;
