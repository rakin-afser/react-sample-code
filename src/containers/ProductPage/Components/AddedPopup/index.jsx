import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router';
import {useTranslation} from 'react-i18next';

import {setRating} from 'components/helpers';
import imgProduct from './img/img-product.jpg';
import {ReactComponent as IconClose} from './img/iconClose.svg';
import {ReactComponent as IconCheck} from './img/iconCheck.svg';
import pic from './img/item.png';
import product1 from 'images/products/product9.jpg';
import product2 from 'images/products/product20.jpg';

import {
  Overlay,
  Header,
  Added,
  Item,
  Product,
  ProductItem,
  Wrap,
  Title,
  Price,
  TocartBtn,
  ContinueBtn,
  Grey,
  Heading,
  Card,
  ProductName,
  Likes,
  Rating,
  CloseBtn
} from './styled';
import FadeOnMount from 'components/Transitions';

const exampleData = {
  img: imgProduct,
  title: 'Black suit for men',
  price: '1499,99',
  productName: 'Black suit for men',
  likes: '25',
  similarProducts: [
    {
      image: pic,
      id: 1,
      title: 'Black suit for men...',
      price: '124.99',
      likes: 3
    },
    {
      image: product1,
      id: 2,
      title: 'Men Cotton Short...',
      price: '58.00',
      likes: 1
    },
    {
      image: product2,
      id: 3,
      title: 'Three Sixty Six Golf...',
      price: '459.00',
      likes: 10
    }
  ]
};

const AddedPopup = ({close, productData, variation}) => {
  const {t} = useTranslation();
  const history = useHistory();

  const onClose = () => {
    close(false);
  };

  const onGotoCart = () => {
    close(false);
    history.push('/cart');
  };

  function onContinue() {
    close(false);
  }

  const {name} = productData || {};
  const {image, price} = variation || productData || {};

  return (
    <Overlay>
      <Added>
        <Header>
          <span>1 {t('orderInfo.itemAddedToCart')}</span>
          <CloseBtn>
            <IconClose onClick={onClose} />
          </CloseBtn>
        </Header>
        <Product>
          <Item>
            <ProductItem right src={image?.sourceUrl} alt="" />
            <Wrap>
              <Title>{name}</Title>
              <Rating>{setRating(5)}</Rating>
              <Price>{price}</Price>
            </Wrap>
          </Item>
          <Item>
            <TocartBtn onClick={onGotoCart}>{t('orderInfo.GoToCart')}</TocartBtn>
            <ContinueBtn onClick={onContinue}>{t('orderInfo.ContinueShopping')}</ContinueBtn>
          </Item>
        </Product>
        <Grey />
        <Product>
          <Heading>{t('orderInfo.productHeading')}</Heading>
          <Item space>
            {exampleData.similarProducts.map((el) => (
              <Card key={el.id}>
                <ProductItem src={el.image} />
                <ProductName>{el.title}</ProductName>
                <Wrap flex>
                  <Price small>
                    {/* <span>BD</span> */}
                    {el.price}
                  </Price>
                  <Likes>{el.likes}</Likes>
                </Wrap>
              </Card>
            ))}
          </Item>
        </Product>
      </Added>
    </Overlay>
  );
};

AddedPopup.propTypes = {
  close: PropTypes.func.isRequired,
  setInCart: PropTypes.func.isRequired
};

export default AddedPopup;
