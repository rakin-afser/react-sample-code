import styled from 'styled-components/macro';
import {mainFont, secondaryFont} from 'constants/fonts';
import {midCoinsColor, primaryColor} from 'constants/colors';

export const Overlay = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1111;
  background: rgba(0, 0, 0, 0.5);

  &.enter {
    opacity: 0;
  }

  &.enter-active {
    opacity: 1;
    transition: opacity 800ms, transform 800ms;
  }

  &.enter-done {
    opacity: 1;
    transition: opacity 600ms, transform 600ms;
  }

  &.exit {
    opacity: 1;
  }

  &.exit-active {
    opacity: 0;
    transition: opacity 600ms, transform 600ms;
  }

  &.exit-done {
    opacity: 0;
    transition: opacity 600ms, transform 600ms;
  }
`;

export const Added = styled.div`
  padding: 25px 0;
  width: 441px;
  height: 530px;
  background: #fafafa;
  position: relative;
  top: 8px;
  border-radius: 4px;
`;

export const Header = styled.div`
  padding: 0 17px 0 23px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 600;
  font-size: 20px;
  line-height: 124%;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const Product = styled.div`
  padding: 24px 27px;
`;

export const Item = styled.div`
  display: flex;
  justify-content: ${({space}) => (space ? 'space-between' : 'flex-start')};
`;

export const ProductItem = styled.img`
  width: ${({right}) => (right ? '100px' : '100%')};
  height: ${({right}) => (right ? '100px' : '112px')};
  background: grey;
  object-fit: cover;
  margin-right: ${({right}) => (right ? '20px' : '0px')};
  margin-bottom: ${({right}) => (right ? '24px' : '8px')};
`;

export const Wrap = styled.div`
  display: ${({flex}) => (flex ? 'flex' : 'block')};
  justify-content: space-between;
`;

export const Price = styled.span`
  display: block;
  font-family: ${secondaryFont};
  color: #000000;
  padding-top: ${({small}) => (small ? '0px' : '5px')};
  font-size: ${({small}) => (small ? '12px' : '14px')};
  line-height: ${({small}) => (small ? '14px' : '17px')};

  span {
    margin-right: 5px;
    font-size: 12px;
    color: #999999;
  }
`;

export const Title = styled.span`
  font-weight: 700;
  font-size: 14px;
  line-height: 29px;
  color: #000000;
  max-width: 260px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  display: block;
`;

export const TocartBtn = styled.button`
  min-width: 184px;
  height: 40px;
  outline: none;
  background: #dc5555;
  border-radius: 20px;
  border: none;
  cursor: pointer;
  margin-right: 22px;
  font-family: ${mainFont};
  font-weight: bold;
  font-size: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  color: #ffffff;
  transition: ease 0.4s;

  &:hover {
    color: #fff;
    background-color: ${midCoinsColor};
    box-shadow: 0 0 4px rgba(000, 000, 000, 0.4);
  }
`;

export const ContinueBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  min-width: 184px;
  height: 40px;
  font-family: ${mainFont};
  font-family: ${mainFont};
  font-weight: 500;
  font-size: 16px;
  color: #ed494f;
  outline: none;
  background: transparent;
  border: 1px solid #dc5555;
  border-radius: 20px;
  cursor: pointer;
  transition: ease 0.4s;

  &:hover {
    color: ${midCoinsColor};
    border-color: ${midCoinsColor};
    box-shadow: 0 0 4px rgba(000, 000, 000, 0.4);
  }
`;

export const Grey = styled.div`
  height: 8px;
  background: #e4e4e4;
`;

export const Heading = styled.span`
  display: block;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  padding-bottom: 24px;
`;

export const Card = styled.div`
  flex-basis: 121px;
  background: #ffffff;
  padding: 3px 6px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.13);
`;

export const ProductName = styled.span`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-size: 12px;
  line-height: 14px;
  color: #000000;
  padding-bottom: 3px;
`;

export const Likes = styled.span`
  font-family: 'SF Pro Display', sans-serif;
  font-size: 12px;
  line-height: 14px;
  color: #313131;
`;

export const Rating = styled.div``;

export const CloseBtn = styled.button`
  cursor: pointer;

  svg {
    transform: rotate(0deg);
    transition: ease 0.3s;

    path {
      transition: ease 0.3s;
    }
  }

  &:hover {
    svg {
      transform: rotate(180deg);

      path {
        fill: ${primaryColor};
      }
    }
  }
`;
