import styled from 'styled-components/macro';

export const Container = styled.section`
  width: 100%;
  margin: 0;
`;

export const SellerProfile = styled.div``;

export const MoreSameSellerTitle = styled.span`
  display: block;
  font-family: Helvetica Neue, sans-serif;
  font-weight: 700;
  font-size: 16px;
  line-height: 20px;
  color: #000;
  margin: 0;
`;

export const MoreSameSeller = styled.div`
  margin: 26px 0 0 0;
`;

export const SliderWrapper = styled.div``;

export const CustomerFeedbacks = styled.div`
  padding: 24px 32px;
  margin: 24px 0 0 0;
  background: #fff;
`;

export const CustomerFeedbacksHeading = styled.div`
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  padding-left: 60px;
  ${({number}) =>
    number
      ? `
		display: inline;
		font-size: 16px;
  	line-height: 140%;
		font-weight: 400;
		color: #7A7A7A;
		padding-left: 17px;
		letter-spacing: -0.016em;
	`
      : null};
  ${({photos}) =>
    photos
      ? `
		display: block;
		margin: 41px 0 26px 0;
	`
      : null};
`;

export const FeedbackList = styled.div`
  padding: 0 42px;

  & div.slick-track {
    padding-bottom: 12px;
  }

  .slick-slide {
    margin: 0 10px;
  }
`;

export const FeedbackCard = styled.div`
  width: 222px;
  height: 282px;
  border: 1px solid #eeeeee;
  border-radius: 3px;
  overflow: hidden;
  display: flex !important;
  flex-direction: column;
`;

export const FeedbackInfo = styled.div`
  display: flex;
  align-items: center;
  padding: 6px 11px 7px;
`;

export const FeedbackAvatar = styled.div`
  margin-right: 7px;
  width: 46px;
  height: 46px;
  flex-shrink: 0;
  border-radius: 50%;
  overflow: hidden;
`;

export const FeedbackCity = styled.span`
  display: block;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const FeedbackDate = styled.span`
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
`;

export const Img = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const FeedbackImage = styled.div`
  flex-grow: 1;
  overflow: hidden;
`;
