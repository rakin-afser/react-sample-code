import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import {useTranslation} from 'react-i18next';
import {useQuery} from '@apollo/client';
import _ from 'lodash';
import {Link} from 'react-router-dom';
import moment from 'moment';

import {
  Container,
  SellerProfile,
  MoreSameSeller,
  MoreSameSellerTitle,
  CustomerFeedbacks,
  CustomerFeedbacksHeading,
  FeedbackList,
  FeedbackAvatar,
  FeedbackCard,
  FeedbackCity,
  FeedbackDate,
  FeedbackInfo,
  SliderWrapper,
  Img,
  FeedbackImage
} from './styled';
import Statistics from '../Statisctics';
import Feedback from '../Feedback';
import {GET_SELLER_PRODUCTS} from 'queries';
import ProductCard from 'components/Cards/Product';
import WithSlider from 'components/WithSlider';
import userPlaceholder from 'images/placeholders/user.jpg';

const SellerInfo = ({setRating, productData}) => {
  const {t} = useTranslation();
  const {data} = useQuery(GET_SELLER_PRODUCTS, {variables: {id: +productData?.seller?.id}});
  const reviews = productData?.reviews?.nodes || [];

  const postsWithPhotos =
    reviews.filter(
      (item) => !_.isEmpty(item?.galleryImages?.nodes?.filter((i) => i?.mimeType?.split('/')?.[0] === 'image'))
    ) || [];

  const getFeedbackStatistic = () => {
    const feedbackStatistic = [0, 0, 0, 0, 0];
    reviews.forEach((item) => {
      feedbackStatistic[item.rating - 1]++;
    });
    return feedbackStatistic
      .map((item) => {
        const percent = ((item * 100) / productData?.reviewCount).toFixed();
        return isNaN(percent) ? 0 : percent;
      })
      .reverse();
  };

  return (
    <Container>
      <MoreSameSellerTitle>More from this Seller </MoreSameSellerTitle>
      <SellerProfile>
        <MoreSameSeller>
          <SliderWrapper>
            <WithSlider withHeader={false} slidesToScroll={4} infinite={false} withSeeMore slidesToShow={4}>
              {data?.products?.nodes?.map((item, index) => (
                <ProductCard
                  hideLens
                  hideStoreBlock
                  key={index}
                  index={index}
                  margin="0 10px"
                  maxWidth="250px"
                  content={item}
                />
              ))}
              {/* <Button margin="0 0 0 -55px" type="seeMore" props={{withText: true}} /> */}
            </WithSlider>
          </SliderWrapper>
        </MoreSameSeller>
      </SellerProfile>
      {postsWithPhotos.length ? (
        <CustomerFeedbacks>
          <CustomerFeedbacksHeading>
            Customer feedbacks
            {productData?.reviewCount ? (
              <CustomerFeedbacksHeading number>({productData?.reviewCount})</CustomerFeedbacksHeading>
            ) : null}
          </CustomerFeedbacksHeading>
          <Statistics
            statisticsData={getFeedbackStatistic()}
            totalReviews={productData?.reviewCount}
            setRating={setRating}
            totalRating={productData?.averageRating}
          />

          <CustomerFeedbacksHeading photos>{t('productPage.PhotosFromFeedback')}</CustomerFeedbacksHeading>
          <FeedbackList>
            <Slider slidesToShow={4} arrows slidesToScroll={4} infinite={false}>
              {postsWithPhotos.map((item, key) => {
                const images = item?.galleryImages?.nodes.filter((i) => i?.mimeType?.split('/')?.[0] === 'image');
                const gallery = postsWithPhotos?.length > 3 ? [images?.[0]] : images;
                return gallery?.map((image) => (
                  <FeedbackCard key={key}>
                    <FeedbackInfo>
                      <FeedbackAvatar>
                        <Link to={`/profile/${item?.author?.node?.databaseId}/posts`}>
                          <Img src={item?.author?.node?.profile_picture || userPlaceholder} />
                        </Link>
                      </FeedbackAvatar>
                      <div>
                        <FeedbackCity>{item?.author?.node?.name}</FeedbackCity>
                        <FeedbackDate>{moment(item?.date).format('MMM Do')}</FeedbackDate>
                      </div>
                    </FeedbackInfo>
                    <FeedbackImage>
                      <Img src={image?.mediaItemUrl} />
                    </FeedbackImage>
                  </FeedbackCard>
                ));
              })}
            </Slider>
          </FeedbackList>
          <Feedback data={reviews} />
        </CustomerFeedbacks>
      ) : null}
    </Container>
  );
};

SellerInfo.propTypes = {
  setRating: PropTypes.func.isRequired,
  productData: PropTypes.shape().isRequired
};

export default SellerInfo;
