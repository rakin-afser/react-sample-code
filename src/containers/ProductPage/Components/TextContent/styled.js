import styled from 'styled-components';
import {blueColor, primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  margin-right: auto;
  max-width: 665px;
  flex: 1;
`;

export const Txt = styled.div`
  margin-bottom: 43px;

  font-size: 16px;

  h3 {
    font-size: inherit;
    font-weight: 700;
    color: #000;
  }

  p {
    font-size: inherit;
    color: #545454;
    letter-spacing: -0.3px;
    line-height: 175%;
  }

  ul {
    padding-top: 15px;
    padding-bottom: 32px;
    font-size: inherit;
  }
`;

export const ShowMore = styled.button`
  padding: 0;
  color: ${primaryColor};
  font-size: 14px;
  transition: ease 0.4s;

  svg {
    margin-left: 12px;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    color: ${blueColor};

    svg {
      path {
        fill: ${blueColor};
      }
    }
  }
`;
