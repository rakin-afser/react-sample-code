import React from 'react';
import PropTypes from 'prop-types';
import parse from 'html-react-parser';
import Expanded from 'components/Expanded';
import {Wrapper, Txt, ShowMore} from './styled';
import {ReactComponent as IconArrow} from './img/arrowIcon.svg';
import {ReactComponent as IconArrowUp} from './img/arrowIconUp.svg';

const TextContent = ({data}) => {
  const {description, seller} = data?.product || {};
  const {storeReturnPolicy, storeShippingPolicy} = seller || {};

  const ShowBtn = () => {
    return (
      <ShowMore>
        Show more
        <IconArrow />
      </ShowMore>
    );
  };

  const HideBtn = () => {
    return (
      <ShowMore>
        Less
        <IconArrowUp />
      </ShowMore>
    );
  };

  return (
    <Wrapper>
      <Txt>
        <h3>Description</h3>
        {parse(description || '')}
        {/* <p> */}
        {/*   Women's Casual knitted sweater with twist effect. Back length on size S is 64cm and sleeve length is 70cm. */}
        {/*   This model features a balloon effect waist. Collar Type: Round */}
        {/* </p> */}
        {/* <Expanded showBtn={<ShowBtn />} hideBtn={<HideBtn />}> */}
        {/*   <h3>Description</h3> */}
        {/*   <p> */}
        {/*     Women's Casual knitted sweater with twist effect. Back length on size S is 64cm and sleeve length is 70cm. */}
        {/*     This model features a balloon effect waist. Collar Type: Round */}
        {/*   </p> */}
        {/* </Expanded> */}
      </Txt>
      {storeReturnPolicy ? (
        <Txt>
          <h3>Returns & Exchanges</h3>
          {/* <p>I gladly accept returns, exchanges, and cancellations.</p> */}
          <Expanded showBtn={<ShowBtn />} hideBtn={<HideBtn />}>
            {parse(storeReturnPolicy || '')}
            {/* <ul>
            <li>
              <strong>Contact me within:</strong> 14 days of delivery
            </li>
            <li>
              <strong>Ship items back within:</strong> 21 days of delivery
            </li>
          </ul>
          <p>
            This pattern creates an oversized knitted cardigan, it is made to be loose fitting. It is also long, the
            model in photos is 5 foot 4 inches tall and it falls below the knees. You can make it shorter if you want by
            doing less rows than the pattern calls for, there is a brief note in the pattern on how to do this. If you
            are looking for a fitted cardigan this is NOT the pattern for you, but if you are looking for an oversized,
            comfy, blanket like cardigan then this IS for you.
          </p> */}
          </Expanded>
        </Txt>
      ) : null}
      {storeShippingPolicy ? (
        <Txt>
          <h3>Shipping</h3>
          {/* <p>I gladly accept returns, exchanges, and cancellations.</p> */}
          <Expanded showBtn={<ShowBtn />} hideBtn={<HideBtn />}>
            {parse(storeShippingPolicy || '')}
            {/* <ul>
            <li>
              <strong>Contact me within:</strong> 14 days of delivery
            </li>
            <li>
              <strong>Ship items back within:</strong> 21 days of delivery
            </li>
          </ul>
          <p>
            This pattern creates an oversized knitted cardigan, it is made to be loose fitting. It is also long, the
            model in photos is 5 foot 4 inches tall and it falls below the knees. You can make it shorter if you want by
            doing less rows than the pattern calls for, there is a brief note in the pattern on how to do this. If you
            are looking for a fitted cardigan this is NOT the pattern for you, but if you are looking for an oversized,
            comfy, blanket like cardigan then this IS for you.
          </p> */}
          </Expanded>
        </Txt>
      ) : null}
    </Wrapper>
  );
};

TextContent.defaultProps = {
  data: {}
};

TextContent.propTypes = {
  data: PropTypes.shape()
};

export default TextContent;
