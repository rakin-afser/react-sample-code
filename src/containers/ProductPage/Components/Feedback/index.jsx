import React from 'react';
import PropTypes from 'prop-types';
import parse from 'html-react-parser';
import moment from 'moment';
// import {InputMessage, UserPic} from 'components/Comments/styled';
// import send from 'components/Comments/img/send.svg';
// import myAvatar from 'components/Comments/img/avatar1.png';
import pic2 from 'containers/ProductPage/Components/Feedback/img/example.png';
import {Container, Text, Reply, Send, AddComment} from './styled';

import FeedbackItem from './FeedbackItem';

const exampleData = [
  {
    id: 1,
    name: 'raily_sempled',
    role: 'Influencer',
    rating: 5,
    feedback:
      'This product is amazing! I have finally found a moisturizer that actually works. I have very dry skin year\n' +
      'round and nothing until this product has given me enough hydration. I was having trouble with my skin\n' +
      'being so',
    products: [],
    date: 'Mar 2019',
    likes: 653
  },
  {
    id: 2,
    name: 'anna_smith',
    role: 'Influencer',
    rating: 4,
    feedback:
      'This product is amazing! I have finally found a moisturizer that actually works. I have very dry skin year\n' +
      'round and nothing until this product has given me enough hydration. I was having trouble with my skin\n' +
      'being so',
    products: [pic2, pic2, pic2, pic2, '', ''],
    date: 'Mar 2019',
    likes: 0
  }
];

const Feedback = ({data}) => {

  return (
    <Container>
      {/* {exampleData.map((el, key) => { */}
      {/*  const {name, role, rating, feedback, products, date, likes} = el; */}
      {/*  return ( */}
      {/*    <FeedbackItem */}
      {/*      name={name} */}
      {/*      role={role} */}
      {/*      rating={rating} */}
      {/*      feedback={feedback} */}
      {/*      products={products} */}
      {/*      key={key} */}
      {/*      date={date} */}
      {/*      likes={likes} */}
      {/*    /> */}
      {/*  ); */}
      {/* })} */}

      {data.map((el, key) => {
        const {author, content, rating = 0, galleryImages, date, totalComments, totalLikes} = el;
        return (
          <FeedbackItem
            avatar={author?.node?.profile_picture}
            authorId={author?.node?.databaseId}
            name={author?.node?.name}
            rating={rating}
            feedback={parse(content)}
            media={galleryImages?.nodes?.filter((i) => i?.mimeType?.split('/')?.[0] === 'image')}
            key={key}
            comments={totalComments}
            date={moment(parse(date)).fromNow()}
            likes={totalLikes}
          />
        );
      })}

      {/* <Reply> */}
      {/*  Reply to */}
      {/*  <Text left>@anna_smith</Text> */}
      {/* </Reply> */}
      {/* <AddComment> */}
      {/*  <UserPic src={myAvatar} /> */}
      {/*  <InputMessage placeholder="Add a comment" /> */}
      {/*  <Send> */}
      {/*    <img src={send} alt={send} /> */}
      {/*  </Send> */}
      {/* </AddComment> */}
    </Container>
  );
};

Feedback.propTypes = {
  data: PropTypes.shape().isRequired
};

export default Feedback;
