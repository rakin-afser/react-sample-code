import styled from 'styled-components/macro';

export const Container = styled.section`
  width: 1014px;
  border-top: 1px solid #e4e4e4;
  margin: 16px auto 0 auto;
`;

export const Text = styled.span`
  display: ${({block}) => (block ? 'block' : 'inline')};
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: ${({link}) => (link ? '#ED484F' : '#545454')};
  margin-left: ${({left}) => (left ? '8px' : '0px')};
  margin-top: ${({block}) => (block ? '17px' : '0px')};
`;

export const Reply = styled.div`
  border-bottom: 1px solid #e4e4e4;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
  padding: 12px 16px;
`;

export const Send = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
  background: #c3c3c3;
  border: none;
  box-sizing: border-box;
  border-radius: 50%;
  cursor: pointer;
`;

export const AddComment = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 8px 16px;
  border-bottom: 1px solid #e4e4e4;
  margin-bottom: 69px;
`;
