import styled, {css} from 'styled-components/macro';
import {mainFont} from 'constants/fonts';
import {blue00, blue70, primaryColor} from 'constants/colors';

export const UserAvatar = styled.img`
  position: relative;
  margin-right: 28px;
  width: 62px;
  height: 62px;
  overflow: hidden;
  border-radius: 50%;

  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }

  &::before {
    content: '';
    position: absolute;
    width: 14px;
    height: 14px;
    background: #c3c3c3;
    border: 2px solid #ffffff;
    border-radius: 50%;
    right: 0;
    bottom: 0;
  }
`;

export const Wrap = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${({bottom}) => (bottom ? '17px' : '0px')};
  justify-content: space-between;

  & svg {
    margin-right: 4px;
  }
`;

export const WrapSecond = styled.div``;

export const Text = styled.span`
  display: ${({block}) => (block ? 'block' : 'inline')};
  font-family: ${mainFont};
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: ${({link}) => (link ? '#ED484F' : '#545454')};
  margin-left: ${({left}) => (left ? '8px' : '0px')};
  margin-top: ${({block}) => (block ? '17px' : '0px')};
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 14px;
`;

export const Item = styled.div`
  display: ${({flex}) => (flex ? 'flex' : 'block')};
  align-items: ${({center}) => (center ? 'center' : 'flex-start')};
  justify-content: ${({spaceBetween}) => (spaceBetween ? 'space-between' : 'flex-start')};
  margin-left: ${({center}) => (center ? '292px' : '0')};
`;

export const Content = styled(Item)`
  margin-left: auto;
  max-width: 607px;
  width: 100%;
`;

export const UserName = styled.span`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 18px;
  line-height: 1;
  letter-spacing: 0.019em;
  color: #000000;
`;

export const FollowBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 99px;
  height: 28px;
  background: transparent;
  border: 1px solid #ed484f;
  border-radius: 24px;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #ed484f;
  cursor: pointer;
  margin-top: 11px;
  transition: ease 0.4s;

  svg {
    margin-right: 7px;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    background-color: ${primaryColor};
    color: #fff;

    svg {
      path {
        fill: #fff;
      }
    }
  }
`;

export const InfluenceBtn = styled.span`
  padding: 1px 8px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-family: ${mainFont};
  font-size: 12px;
  color: ${blue70};
  margin-top: 8px;
  background-color: ${blue00};
  border-radius: 24px;

  svg {
    margin-right: 3px;
    position: relative;
    top: -2px;
  }
`;

export const Date = styled.span`
  position: relative;
  top: 4px;
  font-family: ${mainFont};
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #999999;
`;

export const Settings = styled.button`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  width: 19px;
  height: 34px;
  margin-left: 12px;
  outline: none;
  background: transparent;
  border: none;
  text-decoration: none;
  cursor: pointer;
  margin-top: -8px;
`;

export const Dot = styled.i`
  position: relative;
  width: 4px;
  height: 4px;
  background: #7c7e82;
  border-radius: 50%;

  &::before {
    position: absolute;
    bottom: calc(100% + 3.5px);
    left: 0;
    content: '';
    width: 4px;
    height: 4px;
    background: #7c7e82;
    border-radius: 50%;
  }

  &::after {
    position: absolute;
    top: calc(100% + 3.5px);
    left: 0;
    content: '';
    width: 4px;
    height: 4px;
    background: #7c7e82;
    border-radius: 50%;
  }
`;

export const Example = styled.div`
  position: relative;
  margin: 12px 18px 21px 0;
  display: inline-block;
  width: 50px;
  height: 50px;

  ${({more}) =>
    more &&
    css`
      cursor: pointer;

      &::before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: #000000;
        opacity: 0.7;
      }
    `}
  ${({more}) =>
    more &&
    css` 
    &::after {
      content: '+${more}';
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      color: #fff;
      font-family: 'Helvetica Neue', sans-serif;
      font-weight: 500;
      font-size: 16px;
    }
  `}
  img {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
`;

export const Stats = styled.div`
  margin-left: auto;
  padding-top: 12px;
  display: flex;
`;

export const Wrapper = styled.div`
  padding-top: 42px;
  padding-bottom: 43px;
  border-bottom: 1px solid #e4e4e4;
`;

export const Comment = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;

  svg path {
    transition: ease 0.4s;
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;
