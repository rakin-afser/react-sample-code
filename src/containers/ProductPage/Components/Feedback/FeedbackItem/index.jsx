import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

import {setRating} from 'components/helpers';
import {ReactComponent as IconStar} from 'containers/ProductPage/Components/Feedback/img/iconStar.svg';
import {ReactComponent as IconPlus} from 'containers/ProductPage/Components/Feedback/img/plus.svg';
import Likes from 'components/Likes';
import useGlobal from 'store';
import Options from '../Options';
import {ReactComponent as CommentIcon} from '../img/IconComment.svg';
import {
  Wrapper,
  Date,
  Dot,
  Example,
  FollowBtn,
  Header,
  InfluenceBtn,
  Item,
  Content,
  Settings,
  Text,
  UserAvatar,
  UserName,
  Wrap,
  WrapSecond,
  Stats,
  Comment
} from './styled';
import userPlaceholder from 'images/placeholders/user.jpg';

const FeedbackItem = ({name, avatar, role, rating, feedback, authorId, date, comments, media = [], likes}) => {
  const [, setGlobalStore] = useGlobal();
  const [postEditing, setPostEditing] = useState(false);

  const onPostDelete = () => {
    setPostEditing(false);
    setGlobalStore.setActionAlert({data: {}, open: true});
  };

  return (
    <Wrapper>
      <Header>
        <Item flex>
          <Wrap>
            <Link to={`/profile/${authorId}/posts`}>
              <UserAvatar src={avatar || userPlaceholder} />
            </Link>
          </Wrap>
          <WrapSecond>
            <UserName>{name}</UserName>
            {role === 'influencer' && (
              <InfluenceBtn>
                <IconStar />
                {role}
              </InfluenceBtn>
            )}
            <FollowBtn>
              <IconPlus />
              Follow
            </FollowBtn>
          </WrapSecond>
        </Item>
        <Content>
          <Wrap>
            <div>{setRating(rating)}</div>
          </Wrap>
          <Text block>
            {feedback}
            {/* <Text link> More</Text> */}
          </Text>
          {media?.map((el, key) => {
            if (key < 3) {
              return (
                <Example last key={key}>
                  <img src={el?.mediaItemUrl} alt="img" />
                </Example>
              );
            }

            return null;
          })}
          {media?.[3] && (
            <Example more={media.length - 3}>
              <img src={media[3]?.mediaItemUrl} alt="img" />
            </Example>
          )}
        </Content>
        <Item flex>
          <Date>{date}</Date>
          <Settings onClick={() => setPostEditing(!postEditing)}>
            <Dot />
            {postEditing && <Options editAction={() => setPostEditing(false)} deleteAction={onPostDelete} />}
          </Settings>
        </Item>
      </Header>
      <Item flex center spaceBetween>
        <Stats>
          <Likes
            iconStyles={{marginRight: 5}}
            style={{marginRight: 30}}
            width={18}
            height={18}
            likesCount={likes}
            showCount
          />
          <Comment>
            <CommentIcon />
            <span style={{marginLeft: 5}}>{comments || 0}</span>
          </Comment>
        </Stats>
      </Item>
    </Wrapper>
  );
};

FeedbackItem.propTypes = {
  name: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  feedback: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  likes: PropTypes.number.isRequired
};

export default FeedbackItem;
