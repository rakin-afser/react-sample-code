import React from 'react';
import {OptionWrapper, OptionButton} from './styled';

const Options = ({editAction = (f) => f, deleteAction = (f) => f}) => (
  <OptionWrapper>
    <OptionButton onClick={editAction} top>
      Edit
    </OptionButton>
    <OptionButton onClick={deleteAction}>Delete</OptionButton>
  </OptionWrapper>
);

export default Options;
