import React from 'react';
import {Container, List, Item, Title, Rating, Percents, Total, TotalTitle, MaxResult, Stars} from './styled';

const colors = ['#208C4E', '#2ECC71', '#FFC131', '#FF7F0B', '#E4171F'];

const Statistics = ({statisticsData, setRating, totalRating}) => {
  function getItems() {
    return statisticsData.map((count, i) => (
      <Item key={count}>
        <Title>{5 - i} stars</Title>
        <Rating color={colors[i]} count={count} />
        <Percents>{count}%</Percents>
      </Item>
    ));
  }

  return (
    <Container>
      <List>{getItems()}</List>
      <Total>
        <TotalTitle>
          {totalRating}
          <MaxResult>/ 5</MaxResult>
        </TotalTitle>
        <Stars>{setRating(totalRating.toFixed())}</Stars>
      </Total>
    </Container>
  );
};

export default Statistics;
