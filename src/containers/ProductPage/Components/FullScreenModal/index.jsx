import React from 'react';
import {Modal} from 'antd';
import PropTypes from 'prop-types';
import {CloseContainer} from 'components/CreatePost/styled';
import Icon from 'components/Icon';
import SliderWithNavigation from 'containers/ProductPage/Components/SliderWithNavigation';

const FullScreenModal = ({slides, productVideo, displaying, closeModal, slidesPreview, currentSlide}) => {
  return (
    <Modal
      wrapClassName="productPage-fullscreen-modal"
      visible={displaying}
      onCancel={closeModal}
      width="100%"
      getContainer={false}
      footer={null}
      destroyOnClose
      bodyStyle={{
        padding: '35px 0 35px',
        height: '100%'
      }}
      closeIcon={
        <CloseContainer
          style={{
            position: 'relative',
            top: '43px',
            right: '3.8vw',
            padding: 22
          }}
        >
          <Icon width={50} height={50} color="#C3C3C3" type="close" />
        </CloseContainer>
      }
    >
      <SliderWithNavigation
        fade
        fullScreen
        zoomIn={false}
        productVideo={productVideo}
        currentSlide={currentSlide}
        slides={slides}
        slidesPreview={slidesPreview}
      />
    </Modal>
  );
};

FullScreenModal.defaultProps = {
  currentSlide: 0,
  productVideo: null
};

FullScreenModal.propTypes = {
  displaying: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired,
  slidesPreview: PropTypes.arrayOf(PropTypes.any).isRequired,
  currentSlide: PropTypes.number,
  slides: PropTypes.arrayOf(PropTypes.string).isRequired,
  productVideo: PropTypes.string
};

export default FullScreenModal;
