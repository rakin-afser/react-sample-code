import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';
import zoomIn from './img/zoom-in.png';
import {mainFont} from 'constants/fonts';

export const PageWrap = styled.div`
  background: #ffffff;
  padding: 34px 0 0 0;
`;

export const Container = styled.div`
  margin: 0 auto;
  max-width: 1200px;
`;

export const Row = styled.div`
  display: flex;
  align-items: flex-start;
`;

export const Column = styled.div``;

export const BackBtn = styled.button`
  position: relative;
  top: -18px;
  padding: 6px 11px 6px 6px;
  display: flex;
  align-items: center;
  font-family: 'Helvetica', sans-serif;
  font-size: 11px;
  border-radius: 20px;
  background-color: #efefef;
  color: #545454;
  transition: ease 0.4s;

  svg {
    margin-right: 8px;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    background-color: ${primaryColor};
    color: #fff;

    svg {
      path {
        stroke: #fff;
      }
    }
  }
`;

export const SliderControl = styled.i``;

export const Hashtags = styled.div`
  margin: 26px 0 0 93px;
`;

export const Tag = styled.a`
  font-family: Helvetica, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #ed494f;
  margin: 0 8px 0 0;
`;

export const Similar = styled.div`
  margin-top: 0;
  margin-bottom: 66px;
  padding-top: 24px;
  background: #ffffff;
`;

export const SliderContainer = styled.div``;

export const SimilarHeading = styled.span`
  h3 {
    font-family: Helvetica, sans-serif;
    font-weight: 700;
    font-size: 18px;
    line-height: 22px;
    color: #000000;
    background: #fff;
    margin: 0 0 0 90px;
  }

  .CardNewArrival__card {
    box-shadow: 0 2px 17px rgb(0 0 0 / 10%);
  }
`;

export const Recently = styled.div`
  background: #fff;
  padding: 0;
  margin: 2px 0 37px 0;

  .CardNewArrival__card {
    box-shadow: 0 2px 17px rgb(0 0 0 / 10%);
  }
`;

export const Line = styled.hr`
  margin: 0 16px;
  border: 0;
  border-top: 1px solid #efefef;
`;

export const MobileWrapper = styled.div`
  overflow-x: hidden;
  padding-bottom: 72px;
`;

export const Heading = styled.h2`
  margin-bottom: 21px;
  font-family: ${mainFont};
  font-weight: bold;
  font-size: 24px;
  letter-spacing: 0.013em;
  color: #343434;
`;

export const Stripe = styled.div`
  width: 100%;
  height: 2px;
  background-color: ${primaryColor};
`;

export const StickyButtons = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 16px 16px 16px;
  background-color: #fff;
  transform: ${({showSticky}) => (showSticky ? 'translateY(0)' : 'translateY(100%)')};
  transition: transform 0.3s ease;
  box-shadow: inset 0px 1px 6px rgba(90, 90, 90, 0.13);
`;
