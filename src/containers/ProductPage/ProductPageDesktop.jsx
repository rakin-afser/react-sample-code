import React, {useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useHistory} from 'react-router-dom';
import PropTypes from 'prop-types';
import SliderWithNavigation from 'containers/ProductPage/Components/SliderWithNavigation';
import TextContent from 'containers/ProductPage/Components/TextContent';
import SellerCard from 'containers/ProductPage/Components/SellerCard';
import PopularTags from 'components/PopularTags';
import {
  PageWrap,
  Container,
  Row,
  SliderContainer,
  Column,
  BackBtn,
  Hashtags,
  Tag,
  Similar,
  SimilarHeading,
  Recently,
  Heading,
  Stripe
} from './styled';
import OrderInfo from './Components/OrderInfo';
import SellerInfo from './Components/SellerInfo';

import Star from '../../assets/ProductPage/Star';
import WithSlider from '../../components/WithSlider';
import {ReactComponent as BackIcon} from './img/backArrow.svg';
import {useQuery} from '@apollo/client';
import {PRODUCTS_BY_CATEGORY, RECENTLY_VIEWED} from 'queries';
import ProductCard from 'components/Cards/Product';
import productPlaceholder from 'images/placeholders/product.jpg';
import qs from 'qs';

const ProductPageDesktop = ({data}) => {
  const history = useHistory();
  const {location} = history;
  const {postId, listId} = qs.parse(location?.search?.replace('?', ''));
  const [mainImage, setMainImage] = useState(data?.product?.image?.sourceUrl);

  const galleryImagesSourceUrls = data?.product?.galleryImages?.nodes?.map((el) => el.sourceUrl) || [];

  const productSlides = [mainImage, ...galleryImagesSourceUrls];

  const galleryImagesPreviewSourceUrls = data?.product?.galleryImagesPreview?.nodes
    ? data?.product?.galleryImagesPreview?.nodes?.map((el) => el.sourceUrl)
    : [];
  const productVideoPreview = data?.product?.product_video_url ? [productPlaceholder] : [];
  const sliderPreviews = [
    ...productVideoPreview,
    data?.product?.imageThumbnail?.sourceUrl,
    ...galleryImagesPreviewSourceUrls
  ];

  const databaseId = data?.product?.databaseId;
  const categoryIn = data?.product?.productCategories?.nodes?.map((cat) => cat?.slug);
  const {data: dataSimilar} = useQuery(PRODUCTS_BY_CATEGORY, {
    variables: {categoryIn, exclude: databaseId}
  });

  const {data: dataViewed} = useQuery(RECENTLY_VIEWED);

  function getSimilarProducts() {
    return dataSimilar?.products?.edges?.map((edge, index) => (
      <ProductCard key={edge?.node?.id} hideLens hideStoreBlock index={index} maxWidth="250px" content={edge?.node} />
    ));
  }

  function showViewed() {
    return dataViewed?.products?.nodes?.map((prod, index) => (
      <ProductCard key={prod?.id} hideLens hideStoreBlock index={index} maxWidth="250px" content={prod} />
    ));
  }

  const {t} = useTranslation();

  function setRating(value) {
    const Rating = [];
    for (let i = 0; i < value; i += 1) {
      Rating.push(<Star key={i} />);
    }
    if (value < 5) {
      for (let i = 0; i < 5 - value; i += 1) {
        Rating.push(<Star key={i} color="#efefef" />);
      }
    }
    return Rating;
  }

  return (
    <>
      <PageWrap>
        <Container>
          <BackBtn onClick={() => history.goBack()}>
            <BackIcon />
            <span>{t('productPage.backBtn')}</span>
          </BackBtn>
          <Row>
            <Column>
              <SliderWithNavigation
                productVideo={data?.product?.product_video_url}
                slides={productSlides}
                slidesPreview={sliderPreviews}
              />
              <Hashtags>
                {data?.product?.productTags?.nodes?.map((tag, key) => (
                  <Tag key={key}>#{tag.name}</Tag>
                ))}
              </Hashtags>
            </Column>

            <Column style={{flex: 1}}>
              <OrderInfo listId={listId} postId={postId} productData={data} setMainImage={setMainImage} />
            </Column>
          </Row>
          <Row style={{paddingTop: 75, paddingBottom: 55}}>
            <TextContent data={data} />
            <SellerCard sellerData={data?.product?.seller} setRating={setRating} />
          </Row>
          <Row>
            <SellerInfo productData={data?.product} setRating={setRating} />
          </Row>
          <Row />
          <Similar>
            <SimilarHeading>
              <h3>{t('productPage.SimilarItems')}</h3>
              <SliderContainer>
                <WithSlider
                  marginTop={0}
                  slidesToScroll={4}
                  infinite={false}
                  withSeeMore
                  slidesToShow={4}
                  withHeader={false}
                >
                  {getSimilarProducts()}
                </WithSlider>
              </SliderContainer>
            </SimilarHeading>
          </Similar>
        </Container>
        <Container>
          <Heading>{t('productPage.RecentlyViewed')}</Heading>
          <Stripe />
        </Container>
        <Recently>
          <WithSlider
            marginTop={0}
            title="Recently Viewed"
            slidesToScroll={4}
            infinite={false}
            withSeeMore
            slidesToShow={4}
            dots
            withHeader={false}
          >
            {showViewed()}
          </WithSlider>
        </Recently>
        <Container>
          <Heading>{t('productPage.PopularTags')}</Heading>
          <Stripe />
          <PopularTags />
        </Container>
      </PageWrap>
    </>
  );
};

ProductPageDesktop.propTypes = {
  data: PropTypes.shape().isRequired
};

export default ProductPageDesktop;
