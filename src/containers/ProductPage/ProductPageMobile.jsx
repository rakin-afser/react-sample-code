import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {product} from 'constants/staticData';

import BackLink from 'components/ProductPage/mobile/BackLink';
import MainSlider from 'components/ProductPage/mobile/MainSlider';
import Info from 'components/ProductPage/mobile/Info';
import Counters from 'components/ProductPage/mobile/Сounters';
import Description from 'components/ProductPage/mobile/Description';
import Divider from 'components/ProductPage/mobile/Divider';
import ReturnsPayments from 'components/ProductPage/mobile/ReturnsPayments';
import AboutSeller from 'components/ProductPage/mobile/AboutSeller';
import ProductSlider from 'components/ProductPage/mobile/ProductSlider';
import Feedbacks from 'components/ProductPage/mobile/Feedbacks';
import PopularHashtags from 'components/ProductPage/mobile/PopularHashtags';
import Buttons from 'components/ProductPage/mobile/Buttons';
import ProductAdded from 'components/ProductPage/mobile/ProductAdded';
import SizeGuide from 'components/ProductPage/mobile/SizeGuide';

import ActionPopup from 'components/Actions/popup';
import productPlaceholder from 'images/placeholders/product.jpg';
import {Line, MobileWrapper, StickyButtons} from './styled';
import {useQuery} from '@apollo/client';
import {GET_SELLER_PRODUCTS, PRODUCTS_BY_CATEGORY, RECENTLY_VIEWED} from 'queries';
import {useUser} from 'hooks/reactiveVars';
import {useVariations} from 'hooks';
import {getVariationByAttrs, renderCoinsByVariation} from 'util/heplers';
import QuickView from 'components/QuickView';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';
import Feedback from 'components/Modals/mobile/Feedback';
import qs from 'qs';

const ProductPageMobile = ({data}) => {
  const history = useHistory();
  const {location} = history;
  const {postId, listId} = qs.parse(location?.search?.replace('?', ''));
  const [showCommentsPopup, setShowCommentsPopup] = useState(false);
  const [showVariationsPopup, setShowVariationsPopup] = useState(false);
  const [showMessage, setShowMessage] = useState(false);
  const [showSizeGuide, setShowSizeGuide] = useState(false);
  const [showSticky, setShowSticky] = useState(false);

  const {
    databaseId,
    name,
    averageRating,
    reviewCount,
    midCoins,
    variations,
    productCategories,
    totalLikes,
    isLiked,
    seller,
    availableShipping
  } = data?.product || {};

  const [vars, varState, setVarState] = useVariations(data?.product);
  const variation = getVariationByAttrs(data?.product, varState);

  const {price, regularPrice, salePrice} = variation || data?.product || {};

  const [triggerLike] = useAddUpdateLikedProductMutation({
    variables: {input: {id: databaseId}},
    product: data?.product
  });
  const mainImage = data?.product?.image?.sourceUrl
    ? [{mimeType: 'image', mediaItemUrl: data?.product?.image?.sourceUrl}]
    : [];
  const productVideo = data?.product?.product_video_url
    ? [{mimeType: 'video', mediaItemUrl: data?.product?.product_video_url}]
    : [];
  const galleryImages = data?.product?.galleryImagesMedium?.nodes || [];
  const getSlides = [...productVideo, ...mainImage, ...galleryImages];

  const categoryIn = data?.product?.productCategories?.nodes?.map((cat) => cat?.slug);

  const {data: dataSimilar} = useQuery(PRODUCTS_BY_CATEGORY, {
    variables: {
      categoryIn,
      exclude: databaseId
    }
  });
  const {data: dataSellerProducts} = useQuery(GET_SELLER_PRODUCTS, {
    variables: {id: parseInt(data?.product?.seller?.id)}
  });
  const {data: dataViewed} = useQuery(RECENTLY_VIEWED);

  const goBack = () => {
    history.goBack();
  };

  return (
    <>
      <MobileWrapper>
        <BackLink onClick={goBack} />
        <MainSlider slides={getSlides} autoPlay skipViewPort />
        <Info
          listId={listId}
          postId={postId}
          databaseId={databaseId}
          name={name}
          rating={averageRating}
          reviews={reviewCount}
          price={price}
          regularPrice={regularPrice}
          salePrice={salePrice}
          coins={renderCoinsByVariation(midCoins, variation)}
          showVariationsPopup={showVariationsPopup}
          setShowVariationsPopup={setShowVariationsPopup}
          showMessage={showMessage}
          setShowMessage={setShowMessage}
          setShowSizeGuide={setShowSizeGuide}
          variations={variations}
          variation={variation}
          productCategories={productCategories}
          vars={vars}
          varState={varState}
          setVarState={setVarState}
          setShowSticky={setShowSticky}
          availableShipping={availableShipping}
        />
        <Counters
          triggerLike={triggerLike}
          isLiked={isLiked}
          likes={totalLikes}
          comments={reviewCount}
          share={product.share}
          item={data?.product}
          bookmark={product.bookmark}
          showCommentsPopup={showCommentsPopup}
          setShowCommentsPopup={setShowCommentsPopup}
        />
        <Description text={data?.product?.description} attributes={product.attributes} />
        <Divider />
        <ReturnsPayments seller={seller} />
        <Divider />
        <AboutSeller seller={seller} />
        <Line />
        <ProductSlider products={dataSellerProducts?.products?.nodes} />
        <Divider />

        <Feedbacks data={data?.product} />

        <Divider />
        <ProductSlider title="Similar Items" seeMoreText="View All" products={dataSimilar?.products?.edges} />

        <ProductSlider title="Recently Viewed" seeMoreText="View All" products={dataViewed?.products?.nodes} />

        <PopularHashtags tags={['fashion', 'trends', 'shoes']} />

        <StickyButtons showSticky={showSticky ? 1 : 0}>
          <Buttons
            setShowVariationsPopup={setShowVariationsPopup}
            variation={variation}
            disabled={variations && !variation}
            style={{
              marginTop: 0
            }}
            id={data?.product?.databaseId}
            showMessage={showMessage}
            setShowMessage={setShowMessage}
          />
        </StickyButtons>

        <ProductAdded showMessage={showMessage} setShowMessage={setShowMessage} />

        <ActionPopup />

        <SizeGuide showSizeGuide={showSizeGuide} setShowSizeGuide={setShowSizeGuide} />
      </MobileWrapper>
      <Feedback />
      <QuickView />
    </>
  );
};

ProductPageMobile.defaultProps = {};

ProductPageMobile.propTypes = {};

export default ProductPageMobile;
