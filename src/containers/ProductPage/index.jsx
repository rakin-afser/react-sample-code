import React, {useEffect} from 'react';
import {} from './styled';
import Layout from 'containers/Layout';
import {useWindowSize} from '@reach/window-size';
import {useQuery, gql, useMutation} from '@apollo/client';
import {useParams, useLocation} from 'react-router-dom';
import qs from 'qs';

import Loader from 'components/Loader';
import Error from 'components/Error';
import ProductPageDesktop from './ProductPageDesktop';
import ProductPageMobile from './ProductPageMobile';
import {useUser} from 'hooks/reactiveVars';
import {ADD_RECENTLY_VIEWED} from 'mutations';
import {ReviewsFragment} from 'fragments';
import SeoMeta from 'components/SeoMeta';
import productPlaceholder from 'images/placeholders/product.jpg';

const GET_PRODUCT = gql`
  ${ReviewsFragment}
  query Product($id: ID!) {
    product(id: $id, idType: SLUG) {
      id
      slug
      productCategories {
        nodes {
          id
          slug
        }
      }
      currencySymbol
      product_video_url
      type
      reviewCount
      isLiked
      productBrands {
        nodes {
          name
        }
      }
      availableShipping {
        city
        available
      }
      totalLikes
      seller {
        id
        name
        storeUrl
        followed
        rating
        gravatar
        icon
        location
        totalFollowers
        reviews {
          id
          rating
          image
          date
        }
        storeReturnPolicy
        storeShippingPolicy
      }
      ... on SimpleProduct {
        product_video_url
        currencySymbol
        databaseId
        date
        description
        name
        price
        regularPrice
        salePrice
        stockQuantity
        midCoins {
          id
          qty
        }
        averageRating
        description
        commentCount
        attributes {
          nodes {
            name
            options
            label
            id
          }
        }
        image {
          id
          sourceUrl(size: LARGE)
        }
        imageThumbnail: image {
          id
          sourceUrl(size: THUMBNAIL)
        }
        galleryImages {
          nodes {
            id
            sourceUrl(size: LARGE)
          }
        }
        galleryImagesMedium: galleryImages {
          nodes {
            id
            sourceUrl(size: MEDIUM_LARGE)
            mimeType
          }
        }
        galleryImagesPreview: galleryImages {
          nodes {
            id
            sourceUrl(size: THUMBNAIL)
          }
        }
        productTags {
          nodes {
            name
          }
        }
        upsell {
          nodes {
            name
            image {
              id
              altText
              sourceUrl
            }
            ... on SimpleProduct {
              salePrice
              regularPrice
              name
              isLiked
            }
          }
        }
        comments {
          nodes {
            date
            content
            author {
              node {
                ... on User {
                  id
                  avatar {
                    url
                  }
                  name
                  nickname
                  slug
                }
              }
            }
          }
        }
        reviews {
          nodes {
            ...ReviewsFragment
          }
        }
      }
      ... on VariableProduct {
        product_video_url
        stockQuantity
        currencySymbol
        databaseId
        date
        description
        name
        price
        regularPrice
        salePrice
        midCoins {
          id
          qty
        }
        averageRating
        description
        commentCount
        variations {
          nodes {
            id
            databaseId
            price
            salePrice
            regularPrice
            thumbnail: image {
              id
              sourceUrl(size: THUMBNAIL)
              altText
            }
            image {
              id
              sourceUrl(size: LARGE)
              altText
            }
            attributes {
              nodes {
                label
                name
                value
                attributeId
                id
              }
            }
          }
        }
        attributes {
          nodes {
            name
            options
            label
            id
          }
        }
        image {
          id
          sourceUrl(size: LARGE)
        }
        imageThumbnail: image {
          id
          sourceUrl(size: THUMBNAIL)
        }
        galleryImages {
          nodes {
            id
            sourceUrl(size: LARGE)
          }
        }
        galleryImagesMedium: galleryImages {
          nodes {
            id
            sourceUrl(size: MEDIUM_LARGE)
            mimeType
          }
        }
        galleryImagesPreview: galleryImages {
          nodes {
            id
            sourceUrl(size: THUMBNAIL)
          }
        }
        productTags {
          nodes {
            name
          }
        }
        upsell {
          nodes {
            name
            image {
              id
              altText
              sourceUrl
            }
            ... on VariableProduct {
              salePrice
              regularPrice
              name
              isLiked
            }
          }
        }
        comments {
          nodes {
            date
            content
            author {
              node {
                ... on User {
                  id
                  avatar {
                    url
                  }
                  name
                  nickname
                  slug
                }
              }
            }
          }
        }
        reviews {
          nodes {
            ...ReviewsFragment
          }
        }
      }
    }
  }
`;

const ProductPage = () => {
  const {productSlug} = useParams();
  const {loading, error, data} = useQuery(GET_PRODUCT, {variables: {id: productSlug}});
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [user] = useUser();
  const {search} = useLocation();
  const {listRef} = qs.parse(search, {ignoreQueryPrefix: true}) || {};

  const [addToViewed] = useMutation(ADD_RECENTLY_VIEWED);

  useEffect(() => {
    if (data?.product?.databaseId && user?.databaseId) {
      addToViewed({
        variables: {input: {product_id: data?.product?.databaseId, user_id: user?.databaseId}}
      });
    }
    // Collect recently viewed prodcts for guests
    // if (!user && data?.product?.databaseId) {
    //   const viewed = localStorage.getItem('viewed') ? JSON.parse(localStorage.getItem('viewed')) : [];
    //   if (!viewed?.includes(data?.product?.databaseId)) {
    //     localStorage.setItem('viewed', JSON.stringify([...viewed, data?.product?.databaseId]));
    //   }
    // }
  }, [data?.product?.databaseId, user?.databaseId]);

  console.log('user navigated to product page from list:', listRef); // if user navigated to product page from any list (referral)

  if (error) return <Error />;
  if (loading) return <Loader />;

  return (
    <Layout hideHeader={isMobile}>
      <SeoMeta title={data.product?.name} image={data.product?.image?.sourceUrl || productPlaceholder} />
      {isMobile ? <ProductPageMobile data={data} /> : <ProductPageDesktop data={data} />}
    </Layout>
  );
};

export default ProductPage;
