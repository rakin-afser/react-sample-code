import React, {useEffect} from 'react';
import {USER} from 'queries';
import {useUser} from 'hooks/reactiveVars';
import {userId} from 'util/heplers';
import {useLazyQuery} from '@apollo/client';

const AuthProvider = ({children}) => {
  const [user, setUser] = useUser();
  const {databaseId, email} = user || {};
  const [getUser] = useLazyQuery(USER, {
    variables: {id: userId || databaseId},
    onCompleted(data) {
      setUser(data?.user);
    }
  });

  useEffect(() => {
    if (!email && (userId || databaseId)) {
      getUser();
    }
  }, [getUser, email, databaseId]);

  return children;
};

export default AuthProvider;
