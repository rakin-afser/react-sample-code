import React from 'react';
import {useParams} from 'react-router-dom';

import Layout from '../Layout';
import {Wrapper, Content} from './styled';
import TabsBar from './components/TabsBar';
import AboutUs from 'containers/AboutPage/screens/AboutUs';
import HowToBuy from 'containers/AboutPage/screens/HowToBuy';
import Payments from 'containers/AboutPage/screens/Payments';
import Returns from 'containers/AboutPage/screens/Returns';
import RateEarn from 'containers/AboutPage/screens/RateEarn';
import SaveToLists from 'containers/AboutPage/screens/SaveToLists';
import InfluencerRewards from 'containers/AboutPage/screens/InfluencerRewards';
import ReferFriend from 'containers/AboutPage/screens/ReferFriend';
import ContactUs from 'containers/AboutPage/screens/ContactUs';
import FAQs from 'containers/AboutPage/screens/FAQs';

const AboutPage = () => {
  const {page} = useParams();

  const getPage = () => {
    switch (page) {
      case 'how-to-buy':
        return <HowToBuy />;
      case 'payments':
        return <Payments />;
      case 'returns':
        return <Returns />;
      case 'rate-&-earn':
        return <RateEarn />;
      case 'save-to-lists':
        return <SaveToLists />;
      case 'influencer-rewards':
        return <InfluencerRewards />;
      case 'refer-friend':
        return <ReferFriend />;
      case 'contact-us':
        return <ContactUs />;
      case 'faqs':
        return <FAQs />;
      case 'about-us':
      default:
        return <AboutUs />;
    }
  };

  return (
    <Layout>
      <Wrapper>
        <TabsBar />
        <Content>{getPage()}</Content>
      </Wrapper>
    </Layout>
  );
};

export default AboutPage;
