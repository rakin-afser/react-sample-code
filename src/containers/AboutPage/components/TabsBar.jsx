import React, {useState, useEffect, useRef} from 'react';
import {useLocation, useHistory} from 'react-router-dom';

import {TabsWrapper, Title, Tabs, StickyBar} from '../styled';

const TabsBar = () => {
  const barRef = useRef();
  const [stickyBar, setStickyBar] = useState(false);
  const [activeTab, setActiveTab] = useState(0);
  const {pathname} = useLocation();
  const {push} = useHistory();

  useEffect(() => {
    if (pathname) {
      const path = pathname.split('/').reverse();
      setActiveTab(path[0]);
    }
  }, [pathname]);

  const handleChange = (path) => {
    push(`/about/${path}`);
  };

  const scrollCallBack = () => {
    const sticky = barRef.current.offsetTop;
    if (window.pageYOffset > sticky - 20) {
      setStickyBar(true);
    }
    if (window.pageYOffset < 160) {
      setStickyBar(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', scrollCallBack);
    return () => {
      window.removeEventListener('scroll', scrollCallBack);
    };
  }, []);

  return (
    <StickyBar ref={barRef}>
      <TabsWrapper stickyBar={stickyBar}>
        <Tabs active={activeTab === 'about-us'} onClick={() => handleChange('about-us')} customStyles="margin: 0;">
          About Us
        </Tabs>
        <Title>How to Buy</Title>
        <Tabs active={activeTab === 'how-to-buy'} onClick={() => handleChange('how-to-buy')} thin>
          How to Buy
        </Tabs>
        <Tabs active={activeTab === 'payments'} onClick={() => handleChange('payments')} thin>
          Payments
        </Tabs>
        <Tabs active={activeTab === 'returns'} onClick={() => handleChange('returns')} thin>
          Returns
        </Tabs>
        <Title>How to Sell</Title>
        <Tabs active={activeTab === 'start-shop'} thin disabled>
          Start a Shop
        </Tabs>
        <Tabs active={activeTab === 'how-to-sell'} thin disabled>
          How to Sell
        </Tabs>
        <Tabs active={activeTab === 'pricing-plans'} thin disabled>
          Pricing Plans
        </Tabs>
        <Title>Share & Earn</Title>
        <Tabs active={activeTab === 'rate-&-earn'} onClick={() => handleChange('rate-&-earn')} thin>
          Rate & Earn
        </Tabs>
        <Tabs active={activeTab === 'save-to-lists'} onClick={() => handleChange('save-to-lists')} thin>
          Save to Lists
        </Tabs>
        <Tabs active={activeTab === 'influencer-rewards'} onClick={() => handleChange('influencer-rewards')} thin>
          Influencer Rewards
        </Tabs>
        <Tabs active={activeTab === 'refer-friend'} onClick={() => handleChange('refer-friend')} thin>
          Refer a Friend
        </Tabs>
        <Tabs active={activeTab === 'contact-us'} onClick={() => handleChange('contact-us')}>
          Contact Us
        </Tabs>
        <Tabs active={activeTab === 'faqs'} onClick={() => handleChange('faqs')}>
          FAQs
        </Tabs>
      </TabsWrapper>
    </StickyBar>
  );
};

export default TabsBar;
