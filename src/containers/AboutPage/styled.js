import styled from 'styled-components';
import {Select, Button, Input} from 'antd';
import media from 'constants/media';

export const Wrapper = styled.div`
  overflow: hidden;
  position: relative;
  display: grid;
  grid-template-columns: 148px minmax(auto, 756px);
  grid-gap: 0 48px;
  justify-content: center;
  margin: 88px 0 0;

  @media (max-width: ${media.mobileMax}) {
    display: block;
  }
`;

export const Content = styled.div`
  padding-right: 20px;

  @media (max-width: ${media.mobileMax}) {
    padding-left: 20px;
  }
`;

export const TabsWrapper = styled.div`
  padding-left: 20px;
  padding-right: 20px;
  display: flex;
  flex-direction: column;
  border-right: 1px solid #efefef;
  height: 543px;
  width: 148px;
  ${({stickyBar}) =>
    stickyBar
      ? `position: fixed;
    margin: 0;
    top: 20px;`
      : `top: 0;
      margin: 57px 0 0;
      position: relative`};

  @media (max-width: ${media.mobileMax}) {
    position: absolute;
    left: 0;
    top: 0;
    transform: translateX(-100%);
    background-color: #fff;
    z-index: 10;
  }
`;

export const StickyBar = styled.div`
  position: relative;
`;

export const Title = styled.h3`
  && {
    ${({customStyles}) => customStyles || null}
  }

  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  ${({big}) =>
    big
      ? `font-size: 24px;
      line-height: 29px;
      margin: 0 0 23px;`
      : `font-size: 16px;
      line-height: 20px;
      margin: 26px 0 0px;`}
`;

export const Text = styled.p`
  && {
    ${({customStyles}) => customStyles || null}
  }

  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 170%;
  letter-spacing: 0.5px;
  text-indent: 1px;
  color: #545454;
  margin: 0 0 35px;
`;

export const Tabs = styled.a`
  &&& {
    ${({disabled}) =>
      disabled
        ? `cursor: default;
    opacity: 0.7;`
        : null}
  }

  && {
    ${({customStyles}) => customStyles || null}
  }

  &:hover {
    opacity: 0.9;
    color: #${({thin, active}) => (thin && !active ? '999' : '000')};
    text-decoration-line: ${({active}) => (active ? 'underline' : 'none')};
  }

  font-family: Helvetica Neue;
  font-style: normal;
  text-decoration-line: ${({active}) => (active ? 'underline' : 'none')};

  ${({thin, active}) =>
    thin
      ? `font-weight: ${active ? 500 : 'normal'};
    font-size: 14px;
    line-height: 17px;
    color: #999;
    margin-top: 12px;`
      : `font-weight: bold;
      font-size: 16px;
      line-height: 20px;
    color: #000;
    margin-top: 24px;`} ${({active}) =>
    active
      ? `
      color: #000;
      text-decoration-line: underline;
  `
      : null}
`;

export const FormWrapper = styled.div`
  max-width: 500px;
  width: 100%;
`;

export const StyledSelect = styled(Select)`
  &&& {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;

    .ant-select-arrow {
      top: 22px;
      right: 15px;
    }

    .ant-select-selection {
      height: 40px;
      display: flex;
      align-items: center;
      background: #ffffff;
      border: 1px solid #a7a7a7;
      box-sizing: border-box;
      border-radius: 2px;
    }
  }
`;

export const InputStyled = styled(Input)`
  &&& {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    border-color: #a7a7a7;

    .ant-input {
      background: #ffffff;
      border: 1px solid #a7a7a7;
      box-sizing: border-box;
      border-radius: 2px;
    }
  }
`;

export const TextAreaStyled = styled(Input.TextArea)`
  &&& {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    border-color: #a7a7a7;

    .ant-input {
      background: #ffffff;
      border: 1px solid #a7a7a7;
      box-sizing: border-box;
      border-radius: 2px;
    }
  }
`;

export const ButtonStyled = styled(Button)`
  &&& {
    margin: 56px 0 0 auto;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 17px;
    text-align: center;
    color: #ffffff;
    width: 160px;
    height: 36px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
