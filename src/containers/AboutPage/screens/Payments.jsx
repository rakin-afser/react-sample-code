import React from 'react';
import { Link } from 'react-router-dom';

import {Title, Text} from '../styled';

const Payments = () => {
  return (
    <>
      <Title big>Payments</Title>

      <Text>We accept the following payment methods;</Text>

      <Text customStyles="margin: 20px 0 0;">Visa</Text>
      <Text customStyles="margin: 0;">Mastercard</Text>
      <Text customStyles="margin: 0;">Maestro</Text>
      <Text customStyles="margin: 0;">American Express</Text>
      <Text customStyles="margin: 0;">Cash on delivery - Currently being offered in Bahrain only.</Text>
      <Text customStyles="margin: 0;">Paypal - Coming Soon</Text>
      <Text customStyles="margin: 0;">Apple Pay - Coming Soon</Text>

      <Title customStyles="margin: 20px 0 32px;">Do we accept local currency?</Title>

      <Text customStyles="margin: 0;">We are currently offering BHD, AED, SAR, KWD, OMR and we are working on enabling more local currencies as we grow and expand</Text>

      <Text>If you want to shop in your local currency, then please and select your chosen country from the dropdown in the home page header.</Text>

      <Title customStyles="margin: 0 0 32px;">Are you having problems making a payment on testSample?</Title>

      <Text customStyles="margin: 0;">We understand that sometimes there are issues with your payment and we want to help. If you’re having trouble paying for an order, please try the following steps first:</Text>

      <Text>Check that your card details are correct. Make sure all of the information is filled out correctly in your profile and that it matches what’s on file with your bank or credit card company. Double-check to make sure no spaces were left in between numbers when typing in the number itself. Also, double-check to make sure any letters typed into the number fields match exactly what they should be (e.g., O instead of 0). </Text>
      <Text customStyles="margin: 0;">Try entering only the last four digits of your card number if you have a Visa or MasterCard; this can help determine whether there's an issue with filling out other parts of the form properly as well. </Text>
      <Text>You may also want to try entering just one set of numbers at a time (e.g., 123456789) rather than using all four sets at once (1234 5678 9012 3456). This makes it easier for our system to process each digit individually and helps avoid errors due to incorrect spacing or formatting within any given field entry, such as accidentally leaving out spaces between sets of numbers or incorrectly typing them in reverse order by mistake (e.g., 9012 345 6789 0123). </Text>
      <Text customStyles="margin: 0;">Try clearing cookies from your browser before</Text>

      <Text customStyles="margin: 0;">If none of these suggestions work for you, please contact our customer service team so they can help sort out any other issues or concerns that may be preventing your payment from going through successfully. Our support team is available 12/7 via email please <Link to="/about/contact-us">“Contact Us”</Link>.</Text>
      <Text customStyles="margin: 0;">We look forward to hearing from you soon!</Text>
    </>
  );
};

export default Payments;
