import React from 'react';

import {Title, Text} from '../styled';

const FAQs = () => {
  return (
    <>
      <Title big>Frequently Asking Questions</Title>
      <Title customStyles="margin: 28px 0 12px;">1. What does it cost?</Title>
      <Text>
        Your privacy is important to us at testSample. (“testSample,” “we,” “us,” or “our”), so we’ve created this privacy
        policy (this “Privacy Policy”) to explain testSample’s information collection and use practices in connection with
        the Service. You’re bound to the Privacy Policy, too: it’s part of the Agreement, and by using the Service you
        agree that you have read it, that you understand it, and that you will comply with it.
      </Text>{' '}
      <Title customStyles="margin: 28px 0 12px;">2. How do I get paid?</Title>
      <Text>
        Your privacy is important to us at testSample. (“testSample,” “we,” “us,” or “our”), so we’ve created this privacy
        policy (this “Privacy Policy”) to explain testSample’s information collection and use practices in connection with
        the Service. You’re bound to the Privacy Policy, too: it’s part of the Agreement, and by using the Service you
        agree that you have read it, that you understand it, and that you will comply with it.
      </Text>{' '}
      <Title customStyles="margin: 28px 0 12px;">3. How can I start the shop?</Title>
      <Text>
        Only those employees at testSample who need access to the Personal Data to perform their work tasks are processing
        the Personal Data. All those employees have entered into confidentiality agreements.
      </Text>{' '}
      <Title customStyles="margin: 28px 0 12px;">4. How does testSample protects his sellers?</Title>
      <Text>
        Your privacy is important to us at testSample. (“testSample,” “we,” “us,” or “our”), so we’ve created this privacy
        policy (this “Privacy Policy”) to explain testSample’s information collection and use practices in connection with
        the Service. You’re bound to the Privacy Policy, too: it’s part of the Agreement, and by using the Service you
        agree that you have read it, that you understand it, and that you will comply with it.
      </Text>{' '}
      <Title customStyles="margin: 28px 0 12px;">5. Delivery support</Title>
      <Text>
        Your privacy is important to us at testSample. (“testSample,” “we,” “us,” or “our”), so we’ve created this privacy
        policy (this “Privacy Policy”) to explain testSample’s information collection and use practices in connection with
        the Service. You’re bound to the Privacy Policy, too: it’s part of the Agreement, and by using the Service you
        agree that you have read it, that you understand it, and that you will comply with it.
      </Text>{' '}
    </>
  );
};

export default FAQs;
