import React from 'react';

import {Title, Text} from '../styled';

const HowToBuy = () => {
  return (
    <>
      <Title big>How to Buy</Title>
      <Text>
        Finding those perfect pieces for you, your home and your Instagram can be tough. All too often they’re sitting
        in a characterful, little store on a quiet street in a place you don't even know yet.
      </Text>{' '}
      <Text>
        That’s where testSample comes in. We’re here to make sure you don’t miss out on discovering beautiful objects for
        your home. Whether it’s a circular wooden wall shelf perfect for your books (and your #shelfie), a handmade
        brass pepper mill to help you dine in style, or a mustard velvet table lamp made for your bedside cabinet. By
        seeking out the best independent boutiques from stylish neighbourhoods across the world we make it easy for you
        to uncover just the right things for your home, wherever they may be hidden.
      </Text>{' '}
      <Text>
        We do this by carefully selecting boutiques run by people who love homewares as much as we do. There’s The
        Restoration with their retro-inspired collection housed in a 1920’s ballroom in London, Bows & Arrows a Parisian
        concept store who stock a selection of contemporary Japanese brands, and Sukha in Amsterdam who promote
        conscious consumption with their range of sustainable household goods, to name just a few. From minimalism to
        maximalism and everything in between, there's something for every taste and style.
      </Text>{' '}
      <Text>
        So, if you can’t get to the boutique of your dreams, testSample presents you with products from hundreds of the most
        stylish bricks-and-mortar boutiques all in one place. We want to help you find more beautiful things for your
        home, not just more things.
      </Text>
    </>
  );
};

export default HowToBuy;
