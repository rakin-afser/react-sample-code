import React from 'react';

import {Title, Text} from '../styled';

const AboutUs = () => {
  return (
    <>
      <Title big>About Us</Title>
      <Text>
        testSample is an innovative social e-commerce marketplace platform that captures the three dimensions of e-commerce
        which are transactional, relational, and social. It is the first social e-commerce marketplace platform in the
        middle east. On testSample, users can discover and search unique products, find curated content, follow stores,
        connect with other users and influencers.
      </Text>{' '}
      <Text>
        On testSample, users can share their stories, follow favorite shops, see what's trending and can shop and share
        feedback with the community and get rewarded for every purchase.
      </Text>{' '}
      <Text>
        Our social platform allows anyone to promote any product on videos or posts and get paid commissions by other
        users who buy the product after watching your video clip! The best part about it is that you don't have to be an
        expert at creating videos in order to get started with testSample!
      </Text>{' '}
      <Text>
        Our web app lets you add products to videos from your recent purchases, so you can sell directly from the video.
        This makes it easy for users to buy what they see in the videos. You earn a commission when a purchase is made
        from your post with products. It's very exciting!
      </Text>
      <Text>
        We created testSample as an alternative for content creators who want to earn real money from their videos and
        social media posts by selling products directly from their channels. We offer the highest commission rates in
        the industry. So if you are looking for an online business that can help you become financially free or just
        supplement your income then join us! Start making money today! https://testSample.com
      </Text>
      <Text>
        We have the perfect referral program for you! When someone signs up through your unique referral link, they'll
        get 5% off their first purchase on our site and you will receive a 2% cashback on your bank/PayPal account
        (Terms & Conditions Apply). And when you refer a seller to sign up for a subscription plan with us, you'll earn
        20% of their annual subscription fee as commission every time they renew their subscription. It's that easy!
      </Text>
      <Text>
        testSample is a community of buyers and sellers. Currently, no easy way exists for stores or brands to reach out to
        their customers locally in an efficient, measurable, and economic way. With testSample, sellers can add products,
        upload videos, and images, create posts, insert products, engage with users and accept payments directly on
        testSample... all from one easy-to-use platform. It's that easy!
      </Text>

      <Text>
        testSample is a community of buyers and sellers. Currently, no easy way exists for stores or brands to reach out to their customers locally in an efficient, measurable, and economic way. With testSample, sellers can add products, upload videos, and images, create posts, insert products, engage with users and accept payments directly on testSample... all from one easy-to-use platform. It's that easy!
      </Text>{' '}
    </>
  );
};

export default AboutUs;
