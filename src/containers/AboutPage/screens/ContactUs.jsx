import React from 'react';
import {Select, Input, Form} from 'antd';

import {Title, FormWrapper, StyledSelect, ButtonStyled, InputStyled, TextAreaStyled} from '../styled';
import {ReactComponent as DownIcon} from 'images/svg/Down.svg';

const {Option} = Select;
const {TextArea} = Input;

const ContactUsForm = ({form}) => {
  const {getFieldDecorator, validateFields} = form;

  const onSubmitHandler = (e) => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  return (
    <>
      <Title big customStyles="margin: 0 0 28px;">
        Contact Us
      </Title>
      <FormWrapper>
        <Form onSubmit={onSubmitHandler}>
          <Form.Item label="Please Select the Following">
            {getFieldDecorator('following', {
              rules: []
            })(
              <StyledSelect suffixIcon={<DownIcon />} defaultValue="1">
                <Option value="1">I am a testSample Byer</Option>
              </StyledSelect>
            )}
          </Form.Item>
          <Form.Item label="Your Email Address">
            {getFieldDecorator('email', {
              rules: []
            })(<InputStyled />)}
          </Form.Item>
          <Form.Item label="Topic">
            {getFieldDecorator('topic', {
              rules: []
            })(
              <StyledSelect suffixIcon={<DownIcon />} defaultValue="1">
                <Option value="1">Products</Option>
              </StyledSelect>
            )}
          </Form.Item>
          <Form.Item
            label="Messange"
            extra="Please explain your question in as much detail as possible. To help us better understand the issue, please attach revalant screenshot"
          >
            {getFieldDecorator('message', {
              rules: []
            })(<TextAreaStyled autoSize={{minRows: 4, maxRows: 4}} />)}
          </Form.Item>
          <ButtonStyled type="danger" shape="round" htmlType="submit">
            Submit Request
          </ButtonStyled>
        </Form>
      </FormWrapper>
    </>
  );
};

const ContactUs = Form.create({
  name: 'form-template'
})(ContactUsForm);

export default ContactUs;
