import React from 'react';

import {Title, Text} from '../styled';

const Returns = () => {
  return (
    <>
      <Title big>Returns</Title>

      <Text customStyles="margin: 0;">If the buyer does end up opening a case, this Help article explains what you need to do next.</Text>

      <Text customStyles="margin: 0;">testSample is a marketplace of independent sellers who offer their own unique products. Each Brand, Shop, or Retailer has an exclusive set of policies regarding returns, exchanges, and refunds which may differ from shop to shop.</Text>

      <Text customStyles="margin: 20px 0 0;">As a seller on testSample, they are expected to clearly state their policies regarding returns and refunds in your shop policies. This includes:</Text>

      <Text customStyles="margin: 20px 0 0;">- Whether or not they accept returns</Text>
      <Text customStyles="margin: 0;">- The period during  which they may accept a return</Text>
      <Text customStyles="margin: 0;">- Who will bear the cost of return shipping for any items that are sent back to the seller</Text>

      <Text customStyles="margin: 20px 0 0;">Non- Returnable Products</Text>

      <Text customStyles="margin: 20px 0 0;">The below are non-cancellable and non-refundable:</Text>

      <Text customStyles="margin: 20px 0 0;">Perishable products such as food, plants, perfumes, and toiletries</Text>
      <Text customStyles="margin: 0;">Personal products sold with a hygiene seal where that seal has been broken e.g. earrings, underwear, and swimwear</Text>
      <Text customStyles="margin: 0;">Personalized products and products made to order</Text>
      <Text customStyles="margin: 0;">Any product that has been used, returned without original packaging or tags, or is returned in an unsaleable condition. Faulty items must be returned in their original packaging.</Text>
      <Text customStyles="margin: 0;">Please do NOT return any COVID-related items such as masks to boutiques as this poses a public health risk.</Text>

    </>
  );
};

export default Returns;
