import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 50px auto;
  text-align: justify;
  max-width: 1200px;
  padding-left: 20px;
  padding-right: 20px;
`;

export default Wrapper;
