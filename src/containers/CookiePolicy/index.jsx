import React from 'react';
import {useQuery} from '@apollo/client';
import {Helmet} from 'react-helmet';

import {COOKIE_POLICY_CONTENT} from '../../queries';

import Loader from '../../components/Loader';
import Wrapper from './styled';
import Layout from '../Layout';

const CookiePolicy = () => {
  const {data, loading} = useQuery(COOKIE_POLICY_CONTENT);

  if (loading !== true) {
    const {
      page: {content, title}
    } = data;

    return (
      <Layout>
        <Wrapper>
          <Helmet>
            <title>{title || 'Cookie Policy'}</title>
            <meta name="description" content="testSample Cookie Policy" />
          </Helmet>
          <div dangerouslySetInnerHTML={{__html: content}} />
        </Wrapper>
      </Layout>
    );
  }

  return <Loader />;
};

export default CookiePolicy;
