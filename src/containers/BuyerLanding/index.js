import React from 'react';
import Layout from 'containers/Layout';
import CallToAction from './components/CallToAction';
import Navigation from './components/Navigation';
import About from './components/About';
import Explore from './components/Explore';
import SocialFeed from './components/SocialFeed';
import OfferToJoin from './components/OfferToJoin';
import Advantages from './components/Advantages';
import SubscribeForm from './components/SubscribeForm';
import OfferToOpenShop from './components/OfferToOpenShop';
import {Wrapper} from './styled';

const BuyerLanding = () => (
  <Layout isFooterShort>
    <Wrapper>
      <CallToAction />
      <Navigation />
      <About />
      <Explore />
      <SocialFeed />
      <OfferToJoin />
      <OfferToOpenShop />
      <Advantages />
      <SubscribeForm />
    </Wrapper>
  </Layout>
);

export default BuyerLanding;
