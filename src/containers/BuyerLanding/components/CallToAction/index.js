import React from 'react';
import {useWindowSize} from '@reach/window-size';
import {Section, Wrapper, Left, Right, Title, Text, Button, ButtonText, ButtonIcon, Image} from './styled';
import ArrowIcon from 'assets/Arrow';
import {mainWhiteColor as white} from 'constants/colors';
import ctaDesktopBg from 'assets/buyerLandingImages/ctaDesktopBg.png';
import ctaMobBg from 'assets/buyerLandingImages/ctaMobBg.png';

const CallToAction = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Section>
      <Wrapper>
        <Left>
          <Title>A social marketplace</Title>
          <Text>
            Explore &#38; Find Interesting Stores, Products &#38; Lists,
            {windowSize >= 1440 && <br />} Become an Influencer &#38; Earn !! Its exciting!
          </Text>
          <Button>
            <ButtonText>Get Started Now</ButtonText>
            <ButtonIcon>
              <ArrowIcon width={7} height={13.33} color={white} />
            </ButtonIcon>
          </Button>
        </Left>
        <Right>
          <Image src={windowSize > 375 ? ctaDesktopBg : ctaMobBg} />
        </Right>
      </Wrapper>
    </Section>
  );
};

export default CallToAction;
