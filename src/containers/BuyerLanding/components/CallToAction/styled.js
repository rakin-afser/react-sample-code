import styled from 'styled-components';
import {
  mainBlackColor as black,
  disabledLinkColor as gray600,
  primaryColor as primary,
  mainWhiteColor as white
} from 'constants/colors';

export const Section = styled.section`
  padding-bottom: 52px;

  @media (max-width: 1120px) {
    padding: 0 16px 380px 16px;
  }

  @media (max-width: 375px) {
    padding-bottom: 295px;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 1040px;
  margin: 0 auto;

  @media (max-width: 1120px) {
    flex-direction: column;
    width: 100%;
  }
`;

export const Left = styled.div`
  flex: 1;
`;

export const Title = styled.h1`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 40px;
  line-height: 140%;
  color: ${black};
  margin-top: 107px;
  margin-bottom: 37.7px;

  @media (max-width: 1120px) {
    text-align: center;
  }

  @media (max-width: 768px) {
    margin-top: 50px;
  }

  @media (max-width: 425px) {
    margin-top: 32px;
    margin-bottom: 24px;
    font-size: 24px;
    line-height: 29px;
  }

  @media (max-width: 375px) {
    margin-bottom: 21.7px;
  }
`;

export const Text = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 400;
  font-size: 18px;
  line-height: 180%;
  letter-spacing: -0.024em;
  margin-bottom: 43px;
  color: ${gray600};
  max-width: 474px;

  @media (max-width: 1120px) {
    max-width: initial;
    text-align: center;
  }

  @media (max-width: 425px) {
    font-family: 'Helvetica Neue';
    font-size: 16px;
    line-height: 19px;
  }

  @media (max-width: 375px) {
    width: 95%;
    margin: 0 auto 32px auto;
  }
`;

export const Button = styled.button`
  height: 40px;
  display: flex;
  align-items: center;
  background: ${primary};
  margin-bottom: 32px;
  border-radius: 24px;
  padding: 6px 24px 7px 39px;

  @media (max-width: 1120px) {
    margin: 0 auto;
  }
`;

export const ButtonText = styled.span`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: normal;
  color: ${white};
`;

export const ButtonIcon = styled.div`
  margin-left: 23px;
  padding-top: 7px;
`;

export const Right = styled.div`
  flex: 1;
  position: relative;
`;

export const Image = styled.img`
  position: absolute;
  top: -73px;
  left: 8px;

  @media (max-width: 1120px) {
    left: 50%;
    transform: translateX(-50%);
  }

  @media (max-width: 375px) {
    top: -168px;
  }
`;
