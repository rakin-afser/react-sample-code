import styled from 'styled-components';
import {
  mainWhiteColor as white,
  mainBlackColor as black,
  disabledLinkColor as gray600,
  primaryColor as primary
} from 'constants/colors';

export const Section = styled.section`
  height: 326px;

  @media (max-width: 1024px) {
    height: auto;
  }
`;

export const Wrapper = styled.div`
  max-width: 1440px;
  height: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: row;

  @media (max-width: 1024px) {
    flex-direction: column-reverse;
  }
`;

export const Left = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: flex-end;
  width: 604px;
  position: relative;
  height: 326px;

  @media (max-width: 1024px) {
    margin: 0 auto;
  }

  @media (max-width: 425px) {
    width: 100%;
    height: auto;
  }
`;

export const Overlay = styled.div`
  background: rgba(122, 122, 122, 0.4);
  width: 273px;
  height: 100%;
  text-align: center;

  @media (max-width: 425px) {
    background: rgba(102, 102, 102, 0.4);
    width: 100%;
    height: 233.5px;
    padding: 46px 0 0 129px;
  }

  @media (max-width: 375px) {
    background: rgba(102, 102, 102, 0.4);
    width: 100%;
    height: 206px;
  }
`;

export const OverlayTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 41.1381px;
  line-height: 140%;
  color: ${white};
  padding-top: 76px;

  @media (max-width: 425px) {
    font-size: 24px;
    line-height: 29px;
    padding-top: 0;
    max-width: 128px;
    margin: 0 auto;
  }
`;

export const Button = styled.button`
  background: ${white};
  border-radius: 20.7656px;
  padding: 9px 13px 7px 20px;

  @media (max-width: 425px) {
    margin-top: 25px;
  }
`;

export const ButtonText = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: ${primary};
`;

export const ButtonIcon = styled.span`
  margin-left: 20.5px;
`;

export const BgImage = styled.img`
  position: absolute;
  top: -6px;
  left: 0;
  z-index: -1;

  @media (max-width: 425px) {
    width: 102%;
    height: auto;
    top: -5px;
    left: -3px;
  }
`;

export const Right = styled.div`
  flex: 1;
  padding: 82px 0 0 140px;

  @media (max-width: 1024px) {
    padding: 0;
    padding-bottom: 50px;
    text-align: center;
  }
`;

export const Title = styled.h2`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 32px;
  line-height: 140%;
  color: ${black};
  margin-bottom: 6px;

  @media (max-width: 425px) {
    font-size: 24px;
    margin-bottom: 27px;
  }
`;

export const SubTitle = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 140%;
  color: ${black};
  margin-bottom: 16px;

  @media (max-width: 425px) {
    font-size: 16px;
    max-width: 295px;
    margin: 0 auto 15px auto;
  }
`;

export const Text = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 160%;
  color: ${gray600};
  max-width: 436px;

  @media (max-width: 1024px) {
    width: 100%;
    margin: 0 auto;
  }

  @media (max-width: 425px) {
    font-size: 14px;
    max-width: 330px;
    margin-bottom: -21px;
  }
`;
