import React from 'react';
import ArrowIcon from 'assets/Arrow';
import {
  Section,
  Wrapper,
  Left,
  Overlay,
  OverlayTitle,
  Button,
  ButtonText,
  ButtonIcon,
  BgImage,
  Right,
  Title,
  SubTitle,
  Text
} from './styled';
import offerToOpenShopImg from 'assets/buyerLandingImages/offerToOpenShopImg.png';

const OfferToOpenShop = () => (
  <Section>
    <Wrapper>
      <Left>
        <Overlay>
          <OverlayTitle>Open your Shop Now</OverlayTitle>
          <Button>
            <ButtonText>Start a Shop</ButtonText>
            <ButtonIcon>
              <ArrowIcon height={12} width={7} />
            </ButtonIcon>
          </Button>
        </Overlay>
        <BgImage src={offerToOpenShopImg} />
      </Left>
      <Right>
        <Title>Sell on testSample</Title>
        <SubTitle>The next generation Marketplace for your brand</SubTitle>
        <Text>
          A perfect platform for individual enterpreneurs or local brands to discover, gain and grow your audience.{' '}
        </Text>
      </Right>
    </Wrapper>
  </Section>
);

export default OfferToOpenShop;
