import React from 'react';
import {Section, Wrapper, Title, InputButtonWrapper, FormItem, Button, Container} from './styled';
import {Form, Input} from 'antd';
import {useAddSubscriberMutation} from 'hooks/mutations';
import {CSSTransition} from 'react-transition-group';

const SubscribeForm = (props) => {
  const {form} = props || {};
  const {getFieldDecorator, getFieldError} = form;
  const [subscribe, {data, loading}] = useAddSubscriberMutation();

  function onSubmit(e) {
    e.preventDefault();

    form.validateFields((err, values) => {
      if (!err) {
        subscribe({variables: {input: {email: values?.email}}});
      }
    });
  }

  function onExiting(node) {
    node.style.setProperty('margin-bottom', `-${node.clientHeight}px`);
  }

  function renderContent() {
    return (
      <CSSTransition onExiting={onExiting} in={!data?.addSubscriber?.message} timeout={300} unmountOnExit>
        <Container>
          <Form onSubmit={onSubmit}>
            <InputButtonWrapper>
              <FormItem>
                {getFieldDecorator('email', {
                  rules: [
                    {
                      type: 'email',
                      message: 'The input is not valid E-mail!'
                    },
                    {
                      required: true,
                      message: 'Please input your E-mail!'
                    }
                  ]
                })(<Input placeholder="Your e-mail" />)}
              </FormItem>
              <Button kind="subscribe" type="submit" loading={loading} disabled={getFieldError('email')} />
            </InputButtonWrapper>
          </Form>
        </Container>
      </CSSTransition>
    );
  }

  return (
    <Section className="subscribe">
      <Wrapper>
        <Title>{data?.addSubscriber?.message ? 'Thank you for subscribing!' : 'Subscribe to our newsletter'}</Title>
        {renderContent()}
      </Wrapper>
    </Section>
  );
};

const WrappedSubscribeForm = Form.create({name: 'subscribe'})(SubscribeForm);

export default WrappedSubscribeForm;
