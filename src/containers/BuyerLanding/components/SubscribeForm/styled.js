import styled from 'styled-components/macro';
import {Form} from 'antd';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  headerShadowColor as gray400,
  grayBackroundColor as gray200,
  primaryColor as primary
} from 'constants/colors';
import media from 'constants/media';
import Btn from 'components/Btn';

export const Section = styled.section`
  padding: 85px 16px 0px 16px;
  position: relative;

  &::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 1px;
    background: ${gray400};
    top: 0;
    right: 0;
  }

  @media (max-width: 425px) {
    padding-top: 24px;
    padding-top: 31px;

    &::after {
      width: 100%;
      top: 0;
      left: 0;
    }
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;
`;

export const Title = styled.h3`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 22px;
  line-height: 132%;
  text-align: center;
  letter-spacing: 0.016em;
  color: ${black};
  margin-bottom: 0;

  @media (max-width: 425px) {
    font-size: 16px;
    line-height: 20px;
  }
`;

export const InputButtonWrapper = styled.div`
  max-width: 446px;
  position: relative;
  margin: 0 auto;

  @media (max-width: 767px) {
    display: flex;
    flex-direction: column;
    align-items: center;
  }
`;

export const Input = styled.input`
  background: ${white};
  border: 1px solid ${gray400};
  box-sizing: border-box;
  border-radius: 44px;
  width: 446px;
  height: 46px;
  padding: 14px 0 11px 16px;

  &::placeholder {
    font-family: 'Helvetica Neue';
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 140%;
    color: ${gray200};
  }

  @media (max-width: 500px) {
    width: 100%;
  }
`;

export const FormItem = styled(Form.Item)`
  &&& {
    margin-bottom: 0;
  }

  .ant-input {
    font-size: 16px;
    height: 46px;
    border-radius: 46px;
    border-color: rgb(228, 228, 228);
    padding-left: 16px;
    padding-right: 165px;
  }
`;

export const Button = styled(Btn)`
  @media (min-width: 768px) {
    position: absolute;
    top: 5px;
    right: 5px;
  }

  @media (max-width: 767px) {
    margin-top: 30px;
  }
`;

export const Container = styled.div`
  padding-top: 30px;
  @media (max-width: 767px) {
    padding-top: 24px;
  }

  &.exit {
    opacity: 1;
  }

  &.exit-active {
    opacity: 0;
    transition: opacity 300ms ease, margin-bottom 300ms ease;
  }
`;
