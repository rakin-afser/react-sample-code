import styled from 'styled-components';
import {mainBlackColor as black, disabledLinkColor as gray600, primaryColor as primary} from 'constants/colors';

export const Section = styled.section`
  padding: 215px 30px 171px 30px;

  @media (max-width: 1024px) {
    padding: 115px 30px 80px 30px;
  }

  @media (max-width: 425px) {
    padding: 0 30px 32px 30px;
    margin-top: -15px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;
`;

export const AdvantagesCards = styled.div`
  display: flex;
  flex-direction: row;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const AdvantagesCard = styled.div`
  flex: 1;
  text-align: center;

  & + & {
    margin-left: 60px;
  }

  @media (max-width: 768px) {
    margin-top: 30px;

    & + & {
      margin-left: 0;
    }
  }
`;

export const ImageWrapper = styled.div`
  width: 92px;
  height: 92px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
`;

export const Image = styled.img``;

export const Title = styled.h3`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 140%;
  color: ${black};
  margin: 20px 0 16px;

  @media (max-width: 425px) {
    font-size: 18px;
    margin: 2px 0 8px;
  }
`;

export const Text = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 140%;
  text-align: center;
  color: ${gray600};

  @media (max-width: 425px) {
    font-size: 14px;
  }
`;

export const Mark = styled.span`
  color: ${primary};
`;
