import React from 'react';
import {Section, Wrapper, AdvantagesCards, AdvantagesCard, ImageWrapper, Image, Title, Text, Mark} from './styled';
import securePaymentsIcon from 'assets/buyerLandingImages/securePaymentsIcon.svg';
import homeDeliveryIcon from 'assets/buyerLandingImages/homeDeliveryIcon.svg';
import midcoinsIcon from 'assets/buyerLandingImages/midcoinsIcon.svg';

const Advantages = () => (
  <Section>
    <Wrapper>
      <AdvantagesCards>
        <AdvantagesCard>
          <ImageWrapper>
            <Image src={securePaymentsIcon} />
          </ImageWrapper>
          <Title>Secure Payments</Title>
          <Text>Our SSL encryption keep your payment details safe regardless of your payment method choise</Text>
        </AdvantagesCard>
        <AdvantagesCard>
          <ImageWrapper>
            <Image src={homeDeliveryIcon} />
          </ImageWrapper>
          <Title>Home Delivery</Title>
          <Text>Delivery currently available in Bahrain. K.S.A UAE, Kuwait, Qatar will be available soon</Text>
        </AdvantagesCard>
        <AdvantagesCard>
          <ImageWrapper>
            <Image src={midcoinsIcon} />
          </ImageWrapper>
          <Title>Midcoins</Title>
          <Text>
            Earn Midcoins on every purchase. T&#38;C Apply<Mark>*</Mark>
          </Text>
        </AdvantagesCard>
      </AdvantagesCards>
    </Wrapper>
  </Section>
);

export default Advantages;
