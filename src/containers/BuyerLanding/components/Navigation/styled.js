import styled from 'styled-components';
import {mainBlackColor as black} from 'constants/colors';

export const Section = styled.section`
  padding: 40px 0;
`;

export const NavMenu = styled.nav`
  padding-top: 1px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: center;
`;

export const NavItem = styled.a`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: ${black};

  & + & {
    margin-left: 48px;
  }

  @media (max-width: 425px) {
    font-size: 12px;
    line-height: 15px;
    color: #666666;

    & + & {
      margin-left: 26px;
    }
  }
`;
