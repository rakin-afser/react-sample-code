import React from 'react';
import {Section, NavMenu, NavItem} from './styled';

const navLinks = [
  {id: 1, href: '#about', title: 'About'},
  {id: 2, href: '#explore', title: 'Explore'},
  {id: 3, href: '#social', title: 'Social'},
  {id: 4, href: '#rewards', title: 'Rewards'},
  {id: 5, href: '#sellOntestSample', title: 'Sell on testSample'}
];

const Navigation = () => (
  <Section>
    <NavMenu>
      {navLinks.map(({id, href, title}) => (
        <NavItem href={href} key={id}>
          {title}
        </NavItem>
      ))}
    </NavMenu>
  </Section>
);

export default Navigation;
