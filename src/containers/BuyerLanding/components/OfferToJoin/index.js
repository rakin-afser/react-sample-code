import React from 'react';
import {useWindowSize} from '@reach/window-size';
import ArrowIcon from 'assets/Arrow';
import {
  Section,
  Wrapper,
  Flex,
  OfferCards,
  StarsImage,
  Main,
  Title,
  Text,
  Mark,
  Button,
  ButtonText,
  ButtonIcon,
  OfferCard,
  OfferTitle,
  OfferText,
  TextStar
} from './styled';
import starsIcon from 'assets/buyerLandingImages/starsIcon.svg';
import starsIconMob from 'assets/buyerLandingImages/starsIconMob.svg';
import {mainWhiteColor as white, mainBlackColor as black, secondaryTextColor as gray800} from 'constants/colors';

const OfferToJoin = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Section>
      <Wrapper>
        <Flex>
          <StarsImage src={windowSize > 425 ? starsIcon : starsIconMob} />
          <Main>
            <Title>
              Join now &#38; get <Mark color="#398287">10% as MidCoins</Mark> off your first Purchase
            </Title>
            <Text>
              See{' '}
              <Mark color={windowSize > 425 ? black : gray800} isUnderlined={windowSize <= 425}>
                Terms &#38; Conditions
              </Mark>
            </Text>
            <Button>
              <ButtonText>Join Now</ButtonText>
              <ButtonIcon>
                <ArrowIcon width={7} height={12} color={white} />
              </ButtonIcon>
            </Button>
          </Main>
        </Flex>
        <OfferCards>
          <OfferCard>
            <OfferTitle>Refer &#38; Earn</OfferTitle>
            <OfferText>Invite you friends, when they join, you and your friends both earn midcoins</OfferText>
          </OfferCard>
          <OfferCard>
            <OfferTitle>Share your Style</OfferTitle>
            <OfferText>
              Find &#38; Save products you love, create your style list &#38; start earning Midcoins when someone makes
              a purchase through your Lists. <b>T&#38;C Apply</b>
              <TextStar>*</TextStar>
            </OfferText>
          </OfferCard>
          <OfferCard>
            <OfferTitle>Purchase &#38; Leave a Review</OfferTitle>
            <OfferText>
              Every time you leave a review for your purchase you help others make a better decision. Get rewarded when
              they make a purchase.
            </OfferText>
          </OfferCard>
        </OfferCards>
      </Wrapper>
    </Section>
  );
};

export default OfferToJoin;
