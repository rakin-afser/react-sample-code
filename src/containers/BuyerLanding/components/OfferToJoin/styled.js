import styled from 'styled-components';
import {mainBlackColor as black, mainWhiteColor as white, disabledLinkColor as gray600} from 'constants/colors';

export const Section = styled.section`
  padding: 127.96px 16px 112px 16px;

  @media (max-width: 1024px) {
    padding: 89.96px 16px 99px 16px;
  }

  @media (max-width: 425px) {
    padding-top: 28px;
    padding-bottom: 26px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;
`;

export const Flex = styled.div`
  display: flex;

  @media (max-width: 1024px) {
    justify-content: center;
  }
`;

export const StarsImage = styled.img`
  margin-top: -4px;

  @media (max-width: 425px) {
    margin: -17px 0 0 13px;
  }
`;

export const Main = styled.div`
  padding: 35px 0px 0px 41px;

  @media (max-width: 425px) {
    padding-top: 0;
  }
`;

export const Title = styled.h3`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 140%;
  color: ${black};
  margin-bottom: 16px;

  @media (max-width: 425px) {
    font-size: 16px;
    line-height: 140%;
  }
`;

export const Text = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 140%;
  color: ${gray600};
  margin-bottom: 14px;

  @media (max-width: 425px) {
    font-size: 14px;
    line-height: 140%;
  }
`;

export const Mark = styled.span`
  color: ${({color}) => color};
  text-decoration: ${({isUnderlined}) => (isUnderlined ? 'underline' : 'none')};
`;

export const Button = styled.button`
  background: ${black};
  border-radius: 24px;
  display: flex;
  flex-direction: row;
  padding: 10px 22.5px 5px 37px;

  @media (max-width: 425px) {
    display: none;
  }
`;

export const ButtonText = styled.span`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 17px;
  color: ${white};
  margin-right: 21.5px;
`;

export const ButtonIcon = styled.div``;

export const OfferCards = styled(Flex)`
  margin-top: 78px;

  @media (max-width: 425px) {
    flex-direction: column;
    margin-top: 14px;
  }
`;

export const OfferCard = styled.div`
  flex: 1;

  & + & {
    margin-left: 90px;
  }

  @media (max-width: 425px) {
    & + & {
      margin-left: 0;
      margin-top: 4px;
    }
  }
`;

export const OfferTitle = styled.h4`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 140%;
  color: ${black};

  @media (max-width: 425px) {
    font-size: 18px;
    line-height: 22px;
    margin-bottom: 8px;
  }
`;

export const OfferText = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  color: ${gray600};

  @media (max-width: 425px) {
    font-size: 14px;
    line-height: 140%;
  }
`;

export const TextStar = styled.span`
  color: #cd151c;
`;
