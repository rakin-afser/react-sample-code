import React from 'react';
import {useWindowSize} from '@reach/window-size';
import {
  Section,
  Wrapper,
  Title,
  Text,
  Cards,
  Card,
  CardTitle,
  CardImage,
  CardDescriptionTitle,
  CardDescription,
  MCards,
  MCard,
  MCardTitle,
  MCardFlex,
  MCardLeft,
  MCardImage,
  MCardRight,
  MCardDescriptionTitle,
  MCardDescription
} from './styled';
import {aboutData} from './aboutData';

const About = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Section>
      <Wrapper>
        <Title>Creating a Community</Title>
        {windowSize > 425 ? (
          <Text>
            A brand new social e-commerce marketplace that will change the you <b>Shop &#38; Share.</b>
            {windowSize >= 768 && <br />}
            Find <b>Whats In Store</b>, See whats <b>Trending, Follow &#38; Chat</b> with Stores. Enjoy a{' '}
            <b>brand new experience</b>!
          </Text>
        ) : (
          <>
            <Text>
              A brand new social e-commerce marketplace that will change the you <b>Shop &#38; Share.</b>
            </Text>
            <Text>
              Find <b>Whats In Store</b>, See whats <b>Trending, Follow &#38; Chat</b> with Stores.{' '}
            </Text>
            <Text>
              Enjoy a <b>brand new experience</b>!
            </Text>
          </>
        )}
        {windowSize > 425 ? (
          <Cards>
            {aboutData.map(({id, title, img, descriptionTitle, description}) => (
              <Card key={id}>
                <CardTitle>{title}</CardTitle>
                <CardImage src={img} />
                <CardDescriptionTitle>{descriptionTitle}</CardDescriptionTitle>
                <CardDescription>{description}</CardDescription>
              </Card>
            ))}
          </Cards>
        ) : (
          <MCards>
            {aboutData.map(({id, title, img, descriptionTitle, description}) => (
              <MCard key={id}>
                <MCardTitle>{title}</MCardTitle>
                <MCardFlex>
                  {id % 2 === 0 ? (
                    <>
                      <MCardLeft>
                        <MCardImage src={img} />
                      </MCardLeft>
                      <MCardRight hasDescription>
                        <MCardDescriptionTitle>{descriptionTitle}</MCardDescriptionTitle>
                        <MCardDescription>{description}</MCardDescription>
                      </MCardRight>
                    </>
                  ) : (
                    <>
                      <MCardLeft hasDescription>
                        <MCardDescriptionTitle>{descriptionTitle}</MCardDescriptionTitle>
                        <MCardDescription>{description}</MCardDescription>
                      </MCardLeft>
                      <MCardRight>
                        <MCardImage src={img} />
                      </MCardRight>
                    </>
                  )}
                </MCardFlex>
              </MCard>
            ))}
          </MCards>
        )}
      </Wrapper>
    </Section>
  );
};

export default About;
