import aboutImg1 from 'assets/buyerLandingImages/aboutImg1.png';
import aboutImg2 from 'assets/buyerLandingImages/aboutImg2.png';
import aboutImg3 from 'assets/buyerLandingImages/aboutImg3.png';

export const aboutData = [
  {
    id: 1,
    title: 'Discover & Buy',
    img: aboutImg1,
    descriptionTitle: 'Unique Stores & Products',
    description: 'Find Stores, Products, Posts & Curated Lists'
  },
  {
    id: 2,
    title: 'Express Yourself',
    img: aboutImg2,
    descriptionTitle: 'Likes and Comments',
    description: 'Like, Share, Comment and Save your loved items  to Lists'
  },
  {
    id: 3,
    title: 'Earn Rewards',
    img: aboutImg3,
    descriptionTitle: 'Leave a Review & Earn',
    description: 'Share your reviews, help others decide and get rewarded'
  }
];
