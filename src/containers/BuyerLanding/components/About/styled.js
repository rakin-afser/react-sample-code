import styled from 'styled-components';
import {mainBlackColor as black, disabledLinkColor as gray600} from 'constants/colors';

export const Section = styled.section`
  background: #f5f9ff;
  padding: 42px 20px;
  text-align: center;

  @media (max-width: 425px) {
    text-align: left;
    padding: 10px 17px 42px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;
`;

export const Title = styled.h2`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  color: ${black};
  margin: 1px 8px 32px 0;

  @media (max-width: 425px) {
    font-size: 24px;
    line-height: 29px;
    margin-bottom: 23px;
  }
`;

export const Text = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: ${gray600};
  margin-bottom: 29px;

  @media (max-width: 425px) {
    font-size: 16px;
    line-height: 19px;
    margin-bottom: 28px;
  }
`;

export const Cards = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;

  @media (max-width: 768px) {
    flex-direction: column;
    align-items: center;
  }
`;

export const Card = styled.div`
  width: 330px;
  margin-top: 29px;
  text-align: center;

  & + & {
    margin-left: 25px;
  }

  @media (max-width: 768px) {
    & + & {
      margin-left: 0;
    }
  }
`;

export const CardTitle = styled.h4`
  margin-bottom: 16px;
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 140%;
  color: ${black};
`;

export const CardImage = styled.img`
  max-width: 330px;
  max-height: 200px;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const CardDescriptionTitle = styled.h6`
  margin: 24px 0 4px 0;
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  color: ${black};
`;

export const CardDescription = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 140%;
  letter-spacing: -0.016em;
  color: ${gray600};
  padding-top: 8px;
  max-width: 255px;
  width: 100%;
  margin: 0 auto;
`;

export const MCards = styled.div`
  display: flex;
  flex-direction: column;
`;

export const MCard = styled.div`
  & + & {
    margin-top: 30px;
  }
`;

export const MCardTitle = styled.h4`
  margin-bottom: 16px;
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: ${black};
  text-align: center;

  @media (max-width: 425px) {
    margin-bottom: 26px;
  }
`;

export const MCardFlex = styled.div`
  display: flex;
  flex-direction: row;
`;

export const MCardLeft = styled.div`
  flex: 1;
  padding-right: ${({hasDescription}) => (hasDescription ? '9px' : '')};
`;

export const MCardRight = styled.div`
  flex: 1;
  padding-left: ${({hasDescription}) => (hasDescription ? '8px' : '')};
  text-align: ${({hasDescription}) => (hasDescription ? 'right' : '')};
  display: ${({hasDescription}) => (hasDescription ? 'flex' : '')};
  flex-direction: ${({hasDescription}) => (hasDescription ? 'column' : '')};
  justify-content: ${({hasDescription}) => (hasDescription ? 'flex-end' : '')};
`;

export const MCardImage = styled.img`
  width: 168px;
  height: 106px;
  object-fit: cover;
  border-radius: 8px;
`;

export const MCardDescriptionTitle = styled.h6`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  color: ${black};
  margin-bottom: 8px;
`;

export const MCardDescription = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: ${gray600};
`;
