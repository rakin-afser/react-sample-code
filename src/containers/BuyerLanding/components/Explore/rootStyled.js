import styled from 'styled-components';
import {mainBlackColor as black, secondaryTextColor as gray800, grayTextColor as gray700} from 'constants/colors';

export const Section = styled.section`
  padding: 60px 16px 296px 16px;

  @media (max-width: 425px) {
    padding-top: 11px;
    padding-bottom: 35px;
  }
`;

export const Wrapper = styled.div`
  max-width: 1040px;
  margin: 0 auto;
`;

export const Title = styled.h2`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  text-align: center;
  color: ${black};
  margin-bottom: 20px;

  @media (max-width: 425px) {
    font-size: 24px;
    line-height: 29px;
    margin-bottom: 23px;
  }
`;

export const Text = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: ${gray700};
  text-align: center;
  margin-bottom: 30px;

  @media (max-width: 425px) {
    font-size: 16px;
    line-height: 132%;
    margin-bottom: -3px;
  }
`;

export const Info = styled.div`
  padding: ${({padding}) => (padding ? padding : 0)};
  margin: ${({margin}) => (margin ? margin : 0)};
  width: ${({width}) => (width ? width : '')};

  @media (max-width: 1024px) {
    width: 100%;
    max-width: 550px;
    margin: 0 auto;
    text-align: center;
    padding-left: 0;
    padding-right: 0;
  }
`;

export const SubTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 32px;
  line-height: 140%;
  color: ${black};

  @media (max-width: 425px) {
    font-size: 18px;
    line-height: 22px;
    margin-bottom: 16px;
  }
`;

export const List = styled.ul`
  padding: ${({padding}) => (padding ? padding : 0)};
  margin: ${({margin}) => (margin ? margin : 0)};
`;

export const ListItem = styled.li`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 160%;
  color: ${gray800};
  position: relative;
  padding-left: 10px;

  &:before {
    content: '';
    width: 4px;
    height: 4px;
    background: ${gray800};
    border-radius: 50%;
    position: absolute;
    top: 12px;
    left: 0;
  }

  @media (max-width: 1024px) {
    &:before {
      content: '';
      display: none;
    }
  }

  @media (max-width: 425px) {
    font-size: 14px;
    line-height: 160%;
  }
`;
