import styled from 'styled-components';
import {
  mainWhiteColor as white,
  mainBlackColor as black,
  primaryColor as primary,
  disabledLinkColor as gray500,
  headerShadowColor as gray400
} from 'constants/colors';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  position: relative;
  padding-top: 175px;

  @media (max-width: 1024px) {
    flex-direction: column;
    padding-top: 50px;
  }

  @media (max-width: 425px) {
    padding-top: 0;
  }
`;

export const Cards = styled.div`
  display: flex;
  flex-direction: row;
  height: 231px;
  margin-top: 9px;

  @media (max-width: 1024px) {
    justify-content: center;
    margin-top: 30px;
  }
`;

export const Card = styled.div`
  width: 250.27px;
  position: relative;
  align-self: ${({index}) => (index % 2 === 0 ? 'flex-start' : 'flex-end')};
  background: ${white};
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 3.375px;

  & + & {
    margin-left: 23.36px;
  }
`;

export const CardImage = styled.img`
  width: 250.27px;
  height: 84.37px;
  object-fit: cover;
`;

export const CardBody = styled.div`
  position: relative;
  padding: 46.31px 0 7.18px 14.67px;
`;

export const StoreLogo = styled.img`
  width: 66.03px;
  height: 66.03px;
  border-radius: 50%;
  object-fit: cover;
  position: absolute;
  top: 50px;
  left: 15px;
`;

export const StoreName = styled.h4`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 12.839px;
  line-height: 140%;
  color: #3a3a3a;
`;

export const Users = styled.div`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 11.0049px;
  line-height: 132%;
  color: #656565;
  display: flex;
  flex-direction: row;
  align-items: flex-end;
  margin-top: -3px;
`;

export const Followers = styled.div`
  margin-right: 11.15px;
`;

export const Comments = styled.div`
  margin-left: 3.95px;
`;

export const Hashtags = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 5px;
`;

export const Hashtag = styled.p`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 9.17073px;
  line-height: 140%;
  color: ${gray500};

  & + & {
    margin-left: 5px;
  }
`;

export const Button = styled.button`
  position: absolute;
  top: 15px;
  right: 16px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  background: ${white};
  border: 0.917073px solid ${({isFollowed}) => (isFollowed ? gray500 : primary)};
  border-radius: 22.0098px;
  align-items: baseline;
  width: 90px;
  padding: 2px 0;
`;

export const ButtonIcon = styled.div`
  margin-right: 3px;
`;

export const ButtonText = styled.div`
  color: ${({isFollowed}) => (isFollowed ? gray500 : primary)};
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 12.839px;
  line-height: 140%;
`;

export const BgImage = styled.img`
  position: absolute;
  top: 51px;
  right: -13px;
  z-index: -1;

  @media (max-width: 1024px) {
    width: 400px;
    top: 215px;
    left: 282px;
  }

  @media (max-width: 768px) {
    top: 214px;
    left: 163px;
  }
`;

export const MCards = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: flex-start;
  position: relative;
  height: 280px;
  margin-top: 29px;
`;

export const MCard = styled.div`
  align-self: ${({id}) => (id === 2 ? 'flex-end' : 'flex-start')};
  background: ${white};
  border: 1px solid ${gray400};
  box-sizing: border-box;
  border-radius: 4px;
`;

export const MCardImage = styled.img`
  width: 160px;
  height: 100px;
  object-fit: cover;
`;

export const MCardBody = styled.div`
  padding: 14px 10px 12px 10px;
`;

export const MStoreName = styled.h4`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: ${black};
  margin-bottom: 1px;
  text-align: center;
`;

export const MStoreInfo = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  margin-bottom: 6px;
`;

export const MFollowers = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #8f8f8f;
  margin-right: 11px;
  margin-bottom: 0;
  padding-top: 5px;
`;

export const MButton = styled.button`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  justify-content: center;
  border: ${({isFollowed}) => (isFollowed ? '1px solid #8F8F8F' : `1px solid ${primary}`)};
  border-radius: 24px;
  margin: 0 auto;
  padding: 3px 0;
  min-width: 100px;
`;

export const MButtonIcon = styled.div`
  margin-right: 8px;
`;

export const MButtonText = styled.div`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  color: ${({isFollowed}) => (isFollowed ? '#8F8F8F' : primary)};
`;

export const MBgImage = styled.img`
  position: absolute;
  top: -55px;
  left: -14px;
  z-index: -1;
`;
