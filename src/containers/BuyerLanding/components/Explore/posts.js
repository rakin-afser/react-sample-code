import React from 'react';
import {useWindowSize} from '@reach/window-size';
import HeartIcon from 'assets/Heart';
import {Info, SubTitle, List, ListItem} from './rootStyled';
import {
  Wrapper,
  Cards,
  Card,
  CardHeader,
  Flex,
  AccountAvatar,
  AccountName,
  LastUpdate,
  PostImage,
  CardBody,
  PostTitle,
  UsersAvatars,
  UserAvatar,
  CommentsMore,
  Views,
  Likes,
  BgImage,
  MCard,
  MCardHeader,
  MCardHeaderRight,
  MAccountAvatar,
  MAccountName,
  MLastUpdate,
  MCardImage,
  MCardFooter,
  MLikes,
  MLikesValue,
  MViews,
  MBgImage
} from './postsStyled';
import {postsData, mobPostData, postsInfoList as infoList} from './staticData';
import desktopBg from 'assets/buyerLandingImages/postsDesktopBg.svg';
import mobBg from 'assets/buyerLandingImages/postsMobBg.svg';

const Posts = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Wrapper>
      {windowSize > 425 ? (
        <Cards>
          {postsData.map(
            ({
              id,
              accountAvatar,
              accountName,
              lastUpdate,
              postImage,
              postTitle,
              usersAvatars,
              commentsMore,
              views,
              likes
            }) => (
              <Card key={id} id={id}>
                <CardHeader>
                  <Flex>
                    <AccountAvatar src={accountAvatar} />
                    <AccountName>{accountName}</AccountName>
                  </Flex>
                  <LastUpdate>{lastUpdate}</LastUpdate>
                </CardHeader>
                <PostImage src={postImage} />
                <CardBody>
                  <PostTitle>{postTitle}</PostTitle>
                  <Flex>
                    <UsersAvatars>
                      {usersAvatars.map(({id, avatar}) => (
                        <UserAvatar key={id} src={avatar} />
                      ))}
                      <CommentsMore>+{commentsMore}</CommentsMore>
                    </UsersAvatars>
                    <Views>
                      <b>{views}</b> Views
                    </Views>
                    <Likes>
                      <b>{likes}</b> Likes
                    </Likes>
                  </Flex>
                </CardBody>
              </Card>
            )
          )}
          <BgImage src={desktopBg} />
        </Cards>
      ) : (
        <MCard>
          <MCardHeader>
            <MAccountAvatar src={mobPostData.accountAvatar} />
            <MCardHeaderRight>
              <MAccountName>{mobPostData.accountName}</MAccountName>
              <MLastUpdate>{mobPostData.lastUpdate}</MLastUpdate>
            </MCardHeaderRight>
          </MCardHeader>
          <MCardImage src={mobPostData.postImage} />
          <MCardFooter>
            <MLikes>
              <HeartIcon isLiked={false} />
              <MLikesValue>{mobPostData.likes}</MLikesValue>
            </MLikes>
            <MViews>{mobPostData.views} Views</MViews>
          </MCardFooter>
          <MBgImage src={mobBg} />
        </MCard>
      )}
      <Info padding={windowSize > 425 ? '42px 0 0 27px' : '35px 0 0 27px'}>
        <SubTitle>Posts</SubTitle>
        <List>
          {infoList.map(({id, text}) => (
            <ListItem key={id}>{text}</ListItem>
          ))}
        </List>
      </Info>
    </Wrapper>
  );
};

export default Posts;
