import styled from 'styled-components';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  primaryColor as primary,
  menuTitleColor as gray900,
  textGray as gray400,
  transparentTextColor as gray300
} from 'constants/colors';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 102px;
  position: relative;

  @media (max-width: 1024px) {
    padding-top: 0;
    flex-direction: column-reverse;
    align-items: center;
  }

  @media (max-width: 425px) {
    flex-direction: column;
    align-items: center;
  }
`;

export const Cards = styled.div`
  width: 50%;
  height: 292.5px;
  position: relative;

  @media (max-width: 1024px) {
    margin: 25px 0 0 9%;
  }

  @media (max-width: 768px) {
    margin: 25px 0 0 -3%;
  }

  @media (max-width: 425px) {
    margin: 0 auto;
  }
`;

export const Card = styled.div`
  width: ${({theme: {isSmall}}) => (isSmall ? '176.61px' : '193px')};
  height: ${({theme: {isSmall}}) => (isSmall ? '267.43px' : '292.5px')};
  top: ${({theme: {isSmall}}) => (isSmall ? '' : '0')};
  left: ${({theme: {isSmall}}) => (isSmall ? '233px' : '0')};
  bottom: ${({theme: {isSmall}}) => (isSmall ? '0' : '')};
  background: ${white};
  position: absolute;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 3.38596px;
`;

export const CardImage = styled.img`
  width: ${({theme: {isSmall}}) => (isSmall ? '176.61px' : '193px')};
  height: ${({theme: {isSmall}}) => (isSmall ? '176.74px' : '193.14px')};
  object-fit: cover;
`;

export const CardBody = styled.div`
  padding: ${({theme: {isSmall}}) => (isSmall ? '6px 0px 18px 13px' : '6px 0 24.32px 13.54px')};
  position: relative;
`;

export const Price = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  margin-bottom: 5px;
`;

export const PriceBD = styled.span`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 11.8509px;
  line-height: 140%;
  display: flex;
  align-items: center;
`;

export const PriceValue = styled.span`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 13.5439px;
  line-height: 140%;
  color: ${({isDiscount}) => (isDiscount ? primary : black)};
  display: flex;
  align-items: center;
  margin-top: -1px;
`;

export const PriceWithoutDiscount = styled.span`
  margin-left: 10.41px;
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 10.1579px;
  line-height: 132%;
  text-decoration-line: line-through;
  text-transform: uppercase;
  color: ${gray900};
  display: flex;
  align-items: center;
`;

export const Name = styled.h4`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 11.8509px;
  line-height: 140%;
  color: ${black};
`;

export const Description = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 10.1579px;
  line-height: 132%;
  color: ${gray300};
`;

export const FavouriteItem = styled.div`
  width: ${({width}) => width + 'px'};
  height: ${({height}) => height + 'px'};
  position: absolute;
  left: ${({theme: {isSmall}}) => (isSmall ? '15px' : '15px')};
  bottom: ${({theme: {isSmall}}) => (isSmall ? '13px' : '12px')};
`;

export const WishedItem = styled.div`
  width: ${({width}) => width + 'px'};
  height: ${({height}) => height + 'px'};
  position: absolute;
  right: ${({theme: {isSmall}}) => (isSmall ? '13px' : '17px')};
  bottom: ${({theme: {isSmall}}) => (isSmall ? '12px' : '11px')};
`;

export const Discount = styled.div`
  background: ${primary};
  border-radius: 3.38596px;
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-size: 10.1579px;
  line-height: 12px;
  padding: 2px 2px 1px 4px;
  color: ${white};
  position: absolute;
  right: 4px;
  top: 4px;
`;

export const DesktopBgImage = styled.img`
  position: absolute;
  top: -130px;
  left: -102px;
  z-index: -1;
`;

export const MCards = styled.div`
  margin-top: 68px;
  height: 345px;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: flex-start;
  width: 100%;
  position: relative;
`;

export const MCard = styled.div`
  width: 168px;
  position: relative;
  background: ${white};
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  align-self: ${({id}) => (id === 2 ? 'flex-end' : 'flex-start')};

  & + & {
    margin-left: 8px;
  }
`;

export const MCardImage = styled.img`
  width: 168px;
  height: 168px;
  object-fit: cover;
`;

export const MCardBody = styled.div`
  position: relative;
  padding: 8px 0 2px 8px;
`;

export const MPrice = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  margin-bottom: -3px;
  padding-left: 3px;
`;

export const MPriceBD = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 15px;
  color: ${({isDiscount}) => (isDiscount ? gray400 : black)};
`;

export const MPriceValue = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: ${({isDiscount}) => (isDiscount ? primary : black)};
`;

export const MPriceWithoutDiscount = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  text-decoration-line: line-through;
  color: ${gray400};
  margin-left: 10px;
`;

export const MName = styled.h5`
  font-family: 'SF Pro Display';
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  line-height: 12px;
  display: flex;
  align-items: center;
  color: ${black};
  margin-bottom: 8px;
  padding-left: 3px;
`;

export const MDescription = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  line-height: 140%;
  color: ${gray400};
  padding-left: 2px;
`;

export const MIconMore = styled.div`
  position: absolute;
  transform: rotate(90deg);
  right: 4px;
  bottom: 2px;
`;

export const MIconFavourite = styled.div`
  position: absolute;
  bottom: 83px;
  right: 5px;
  background: ${white};
  width: 24px;
  height: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  background: rgba(255, 255, 255, 0.76);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
`;

export const MDiscount = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  line-height: 12px;
  position: absolute;
  top: 10px;
  right: 6px;
  background-color: ${primary};
  color: ${white};
  border-radius: 2px;
  padding: 2.5px 1px 2.5px 1px;
`;

export const MBgImage = styled.img`
  position: absolute;
  top: -140px;
  left: -20px;
  z-index: -1;
`;
