import React from 'react';
import {useWindowSize} from '@reach/window-size';
import {ThemeProvider} from 'styled-components';
import BookmarkIcon from 'assets/Bookmark';
import HeartIcon from 'assets/Heart';
import {ReactComponent as FilledHeartIcon} from 'assets/buyerLandingImages/filledHeart.svg';
import More from 'assets/More';
import {Info, SubTitle, List, ListItem} from './rootStyled';
import {
  Wrapper,
  Cards,
  Card,
  CardImage,
  CardBody,
  Price,
  PriceBD,
  PriceValue,
  PriceWithoutDiscount,
  Name,
  Description,
  FavouriteItem,
  WishedItem,
  Discount,
  DesktopBgImage,
  MCards,
  MCard,
  MCardImage,
  MCardBody,
  MPrice,
  MPriceBD,
  MPriceValue,
  MPriceWithoutDiscount,
  MName,
  MDescription,
  MIconMore,
  MIconFavourite,
  MDiscount,
  MBgImage
} from './itemsStyled';
import {itemsData, mobItemsData, itemsInfoList as infoList} from './staticData';
import desktopBg from 'assets/buyerLandingImages/itemsDesktopBg.svg';
import mobBg from 'assets/buyerLandingImages/itemsMobBg.svg';

const Items = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Wrapper>
      {windowSize > 425 && (
        <Cards>
          {itemsData.map(
            ({
              id,
              name,
              description,
              img,
              price,
              isDiscount,
              discountPercent,
              priceWithoutDiscount,
              isFavourite,
              isWished,
              isSmall
            }) => (
              <ThemeProvider theme={{isSmall}} key={id}>
                <Card>
                  <CardImage src={img} isSmall={isSmall} />
                  <CardBody>
                    <Price>
                      <PriceBD>BD</PriceBD>
                      <PriceValue isDiscount={isDiscount}>{price.toFixed(3)}</PriceValue>
                      {isDiscount && <PriceWithoutDiscount>BD{priceWithoutDiscount}</PriceWithoutDiscount>}
                    </Price>
                    <Name>{name}</Name>
                    <Description>{description}</Description>
                    <FavouriteItem width={isSmall ? 12.91 : 15} height={isSmall ? 11.62 : 13.51}>
                      {isFavourite ? (
                        <FilledHeartIcon width={isSmall ? 12.91 : 15} height={isSmall ? 11.62 : 13.51} />
                      ) : (
                        <HeartIcon isLiked={false} width={isSmall ? 12.91 : 15} height={isSmall ? 11.62 : 13.51} />
                      )}
                    </FavouriteItem>
                    <WishedItem width={isSmall ? 10.33 : 11.29} height={isSmall ? 12.91 : 14.11}>
                      <BookmarkIcon
                        isWished={isWished}
                        width={isSmall ? 10.33 : 11.29}
                        height={isSmall ? 12.91 : 14.11}
                      />
                    </WishedItem>
                  </CardBody>
                  {isDiscount && (
                    <Discount>
                      -<b>{discountPercent}</b>
                      &#37;
                    </Discount>
                  )}
                </Card>
              </ThemeProvider>
            )
          )}
          <DesktopBgImage src={desktopBg} />
        </Cards>
      )}
      <Info padding="33px 0px 0 200px">
        <SubTitle>Items</SubTitle>
        <List margin={windowSize > 425 ? '-11px 0 0 7' : '0'}>
          {infoList.map(({id, text}) => (
            <ListItem key={id}>{text}</ListItem>
          ))}
        </List>
      </Info>
      {windowSize <= 425 && (
        <MCards>
          {mobItemsData.map(
            ({id, name, description, img, price, isDiscount, discountPercent, priceWithoutDiscount}) => (
              <MCard key={id} id={id}>
                <MCardImage src={img} />
                <MCardBody>
                  <MPrice>
                    <MPriceBD isDiscount={isDiscount}>BD</MPriceBD>
                    <MPriceValue isDiscount={isDiscount}>{price}</MPriceValue>
                    {isDiscount && <MPriceWithoutDiscount>BD{priceWithoutDiscount}</MPriceWithoutDiscount>}
                  </MPrice>
                  <MName>{name}</MName>
                  <MDescription>{description}</MDescription>
                  <MIconMore>
                    <More />
                  </MIconMore>
                </MCardBody>
                {isDiscount && (
                  <MDiscount>
                    -<b>{discountPercent}</b>&#37;
                  </MDiscount>
                )}
                <MIconFavourite>
                  <HeartIcon width={11.43} height={11.43} isLiked={false} />
                </MIconFavourite>
              </MCard>
            )
          )}
          <MBgImage src={mobBg} />
        </MCards>
      )}
    </Wrapper>
  );
};

export default Items;
