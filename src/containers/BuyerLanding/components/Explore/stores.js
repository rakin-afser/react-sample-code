import React from 'react';
import {useWindowSize} from '@reach/window-size';
import StarRatings from 'react-star-ratings';
import Plus from 'assets/Plus';
import Checkbox from 'assets/Checkbox';
import {Info, SubTitle, List, ListItem} from './rootStyled';
import {
  Wrapper,
  Cards,
  Card,
  CardImage,
  CardBody,
  StoreLogo,
  StoreName,
  Users,
  Followers,
  Comments,
  Hashtags,
  Hashtag,
  Button,
  ButtonIcon,
  ButtonText,
  BgImage,
  MCards,
  MCard,
  MCardImage,
  MCardBody,
  MStoreName,
  MStoreInfo,
  MFollowers,
  MButton,
  MButtonIcon,
  MButtonText,
  MBgImage
} from './storesStyled';
import {primaryColor as primary, disabledLinkColor as gray500} from 'constants/colors';
import {storesData, mobStoresData, storesInfoList as infoList} from './staticData';
import desktopBg from 'assets/buyerLandingImages/storesDesktopBg.svg';
import mobBg from 'assets/buyerLandingImages/storesMobBg.svg';

const Stores = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Wrapper>
      <Info padding={windowSize > 425 ? '35px 112px 0px 7px' : '31px 112px 0px 7px'} margin="-2px 0px 0px -4px">
        <SubTitle>Stores</SubTitle>
        <List padding={windowSize <= 425 ? '4px 0 0 0' : '0'}>
          {infoList.map(({id, text}) => (
            <ListItem key={id} dangerouslySetInnerHTML={{__html: text}} />
          ))}
        </List>
      </Info>
      {windowSize > 425 ? (
        <Cards>
          {storesData.map(
            ({id, storeName, followers, rating, comments, hashtags, cardImage, logoImage, isFollowed}) => (
              <Card key={id} index={id}>
                <CardImage src={cardImage} />
                <StoreLogo src={logoImage} />
                <CardBody>
                  <StoreName>{storeName}</StoreName>
                  <Users>
                    <Followers>{followers}k followers</Followers>
                    <StarRatings
                      rating={rating}
                      starDimension="7.56px"
                      starSpacing="0.53px"
                      starRatedColor="#FFC131"
                      starEmptyColor="#CCCCCC"
                    />
                    {comments ? <Comments>({comments})</Comments> : null}
                  </Users>
                  <Hashtags>
                    {hashtags.map((hashtag, index) => (
                      <Hashtag key={index}>&#35;{hashtag}</Hashtag>
                    ))}
                  </Hashtags>
                  <Button isFollowed={isFollowed}>
                    <ButtonIcon>
                      {isFollowed ? (
                        <Checkbox width={8.56} height={8.56} color={gray500} />
                      ) : (
                        <Plus width={11.36} height={8.22} color={primary} />
                      )}
                    </ButtonIcon>
                    <ButtonText isFollowed={isFollowed}>{isFollowed ? 'Following' : 'Follow'}</ButtonText>
                  </Button>
                </CardBody>
              </Card>
            )
          )}
          <BgImage src={desktopBg} />
        </Cards>
      ) : (
        <MCards>
          {mobStoresData.map(({id, storeName, followers, rating, img, isFollowed}) => (
            <MCard key={id} id={id}>
              <MCardImage src={img} />
              <MCardBody>
                <MStoreName>{storeName}</MStoreName>
                <MStoreInfo>
                  <MFollowers>{followers} followers</MFollowers>
                  <StarRatings
                    rating={rating}
                    starDimension="7.56px"
                    starSpacing="0.53px"
                    starRatedColor="#FFC131"
                    starEmptyColor="#CCCCCC"
                  />
                </MStoreInfo>
                <MButton isFollowed={isFollowed}>
                  <MButtonIcon>
                    {isFollowed ? (
                      <Checkbox width={12} height={8.56} color={gray500} />
                    ) : (
                      <Plus width={9.33} height={9.33} color={primary} />
                    )}
                  </MButtonIcon>
                  <MButtonText isFollowed={isFollowed}>{isFollowed ? 'Following' : 'Follow'}</MButtonText>
                </MButton>
              </MCardBody>
            </MCard>
          ))}
          <MBgImage src={mobBg} />
        </MCards>
      )}
    </Wrapper>
  );
};

export default Stores;
