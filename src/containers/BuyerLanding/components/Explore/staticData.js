import itemImg1 from 'assets/buyerLandingImages/itemImg1.png';
import itemImg2 from 'assets/buyerLandingImages/itemImg2.png';
import itemMobImg1 from 'assets/buyerLandingImages/itemMobImg1.png';
import itemMobImg2 from 'assets/buyerLandingImages/itemMobImg2.png';

import storeImg1 from 'assets/buyerLandingImages/storeImg1.png';
import storeImg2 from 'assets/buyerLandingImages/storeImg2.png';
import storeMobImg1 from 'assets/buyerLandingImages/storeMobImg1.png';
import storeMobImg2 from 'assets/buyerLandingImages/storeMobImg2.png';
import adidasLogo from 'assets/buyerLandingImages/adidasLogo.svg';
import zaraLogo from 'assets/buyerLandingImages/zaraLogo.svg';

import zaraAvatar from 'assets/buyerLandingImages/zaraAvatar.png';
import chanelAvatar from 'assets/buyerLandingImages/chanelAvatar.png';
import postImg1 from 'assets/buyerLandingImages/postImg1.png';
import postImg2 from 'assets/buyerLandingImages/postImg2.png';
import postUserAvatar1 from 'assets/buyerLandingImages/postUserAvatar1.png';
import postUserAvatar2 from 'assets/buyerLandingImages/postUserAvatar2.png';
import postMobImg from 'assets/buyerLandingImages/postMobImg.png';

import listImg1 from 'assets/buyerLandingImages/listImg1.png';
import listImg2 from 'assets/buyerLandingImages/listImg2.png';
import listImg3 from 'assets/buyerLandingImages/listImg3.png';
import listImg4 from 'assets/buyerLandingImages/listImg4.png';
import listImg5 from 'assets/buyerLandingImages/listImg5.png';
import listImg6 from 'assets/buyerLandingImages/listImg6.png';
import listImg7 from 'assets/buyerLandingImages/listImg7.png';
import listImg8 from 'assets/buyerLandingImages/listImg8.png';
import listImg9 from 'assets/buyerLandingImages/listImg9.png';
import listMobImg1 from 'assets/buyerLandingImages/listMobImg1.png';
import listMobImg2 from 'assets/buyerLandingImages/listMobImg2.png';
import listMobImg3 from 'assets/buyerLandingImages/listMobImg3.png';
import listMobImg4 from 'assets/buyerLandingImages/listMobImg4.png';

export const itemsData = [
  {
    id: 1,
    name: 'Turtle Neck Black',
    description: 'Free Shipping',
    img: itemImg1,
    price: 120,
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243,
    isFavourite: true,
    isWished: false,
    isSmall: false
  },
  {
    id: 2,
    name: 'Fendi Zucca T-shirt',
    description: 'Free Shipping',
    img: itemImg2,
    price: 120,
    isDiscount: false,
    discountPercent: null,
    priceWithoutDiscount: null,
    isFavourite: false,
    isWished: false,
    isSmall: true
  }
];

export const mobItemsData = [
  {
    id: 1,
    name: 'Vitamin C Serum',
    description: 'Free Shipping',
    img: itemMobImg1,
    price: 132.98,
    isDiscount: true,
    discountPercent: 40,
    priceWithoutDiscount: 243.99
  },
  {
    id: 2,
    name: 'Camel Overcoat',
    description: 'Free Shipping',
    img: itemMobImg2,
    price: 132.98,
    isDiscount: false,
    discountPercent: null,
    priceWithoutDiscount: null
  }
];

export const itemsInfoList = [
  {id: 1, text: 'Explore our marketplace for things you need, unique items from both local and global brands.'},
  {id: 2, text: 'We added Likes & Comments to E-commerce.'},
  {id: 3, text: 'Experience the change.'}
];

export const storesData = [
  {
    id: 1,
    storeName: 'Adidas Official',
    followers: 905,
    rating: 4,
    comments: 335,
    hashtags: ['shirt', 'shoes', 'sweater'],
    cardImage: storeImg1,
    logoImage: adidasLogo,
    isFollowed: false
  },
  {
    id: 2,
    storeName: 'Zara Official',
    followers: 905,
    rating: 4,
    comments: 335,
    hashtags: ['shirt', 'shoes', 'sweater'],
    cardImage: storeImg2,
    logoImage: zaraLogo,
    isFollowed: true
  }
];

export const mobStoresData = [
  {
    id: 1,
    storeName: 'Cathalina Wear',
    followers: '129k',
    rating: 4,
    img: storeMobImg1,
    isFollowed: false
  },
  {
    id: 2,
    storeName: 'Handzo Kodzima',
    followers: '93k',
    rating: 4,
    img: storeMobImg2,
    isFollowed: true
  }
];

export const storesInfoList = [
  {id: 1, text: 'Now, discover both Local &#38; Global brands'},
  {id: 2, text: 'See <b>&#34;Whats In Store&#34;</b>'},
  {id: 3, text: 'Follow Stores'},
  {id: 4, text: 'Receive Store updates'}
];

export const postsData = [
  {
    id: 1,
    accountAvatar: zaraAvatar,
    accountName: 'Zara',
    lastUpdate: '12 minutes ago',
    postImage: postImg1,
    postTitle: 'Queen Sweatshirt Funny Lou...',
    usersAvatars: [
      {id: 1, avatar: postUserAvatar1},
      {id: 2, avatar: postUserAvatar2}
    ],
    commentsMore: 7,
    views: 890,
    likes: 701
  },
  {
    id: 2,
    accountAvatar: zaraAvatar,
    accountName: 'Zara',
    lastUpdate: '12 minutes ago',
    postImage: postImg2,
    postTitle: 'Queen Sweatshirt Funny Lou...',
    usersAvatars: [
      {id: 1, avatar: postUserAvatar1},
      {id: 2, avatar: postUserAvatar2}
    ],
    commentsMore: 7,
    views: 890,
    likes: 701
  }
];

export const mobPostData = {
  accountAvatar: chanelAvatar,
  accountName: 'Chanel Beauty',
  lastUpdate: '2d ago',
  postImage: postMobImg,
  views: 300,
  likes: 232
};

export const postsInfoList = [
  {id: 1, text: 'Share your look or your purchase'},
  {id: 2, text: 'Shop the Look from our curated posts'},
  {id: 3, text: 'Follow Stores or Influencers'},
  {id: 4, text: 'Share with your frirends or family. Its interesting! Try it out  NOW!'}
];

export const listsData = [
  {
    id: 1,
    images: [
      {id: 1, image: listImg1},
      {id: 2, image: listImg2},
      {id: 3, image: listImg3}
    ],
    title: 'Fashion',
    followers: '701',
    views: '890'
  },
  {
    id: 2,
    images: [
      {id: 1, image: listImg4},
      {id: 2, image: listImg5},
      {id: 3, image: listImg6}
    ],
    title: 'Electronics',
    followers: '12k',
    views: '74k'
  },
  {
    id: 3,
    images: [
      {id: 1, image: listImg7},
      {id: 2, image: listImg8},
      {id: 3, image: listImg9}
    ],
    title: 'Beauty',
    followers: '284',
    views: '3k'
  }
];

export const mobListData = {
  title: 'beauty products',
  followers: '3k',
  views: '340',
  images: [
    {id: 1, src: listMobImg1},
    {id: 2, src: listMobImg2},
    {id: 3, src: listMobImg3},
    {id: 4, src: listMobImg4}
  ]
};

export const listsInfoList = [
  {id: 1, text: 'Save items to lists'},
  {id: 2, text: 'Let your freinds follow your lists.'},
  {id: 3, text: 'Get rewarded when someone makes purchases from your lists. Its rewarding'}
];
