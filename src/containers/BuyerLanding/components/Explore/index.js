import React from 'react';
import Items from './items';
import Stores from './stores';
import Posts from './posts';
import Lists from './lists';
import {Section, Wrapper, Title, Text} from './rootStyled';

const Explore = () => (
  <Section>
    <Wrapper>
      <Title>Explore</Title>
      <Text>Products, Unique Stores, Curated Lists &#38; Posts which you will love. </Text>
      <Items />
      <Stores />
      <Posts />
      <Lists />
    </Wrapper>
  </Section>
);

export default Explore;
