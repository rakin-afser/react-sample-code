import React from 'react';
import MoreIcon from 'assets/More';
import {useWindowSize} from '@reach/window-size';
import More from 'assets/More';
import {Info, SubTitle, List, ListItem} from './rootStyled';
import {
  Wrapper,
  Cards,
  Card,
  CardImages,
  Image,
  CardBody,
  Title,
  Flex,
  Followers,
  Views,
  IconMore,
  BgImage,
  MCard,
  MCardImages,
  MCardImage,
  MCardBody,
  MTitle,
  MInfo,
  MFollowers,
  MViews,
  MLockIcon,
  MMoreIcon,
  MBgImage
} from './listsStyled';
import {listsData, mobListData, listsInfoList as infoList} from './staticData';
import lockIcon from 'assets/buyerLandingImages/lockIcon.svg';
import desktopBg from 'assets/buyerLandingImages/listsDesktopBg.svg';
import mobBg from 'assets/buyerLandingImages/listsMobBg.svg';

const Lists = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Wrapper>
      <Info
        padding={windowSize <= 425 ? '25px 89px 0 10px' : '17px 89px 0 10px'}
        margin="-2px 0px 0px -8px"
        width="50%"
      >
        <SubTitle>Lists</SubTitle>
        <List padding={windowSize <= 425 ? '2px 0 0 0' : '0'}>
          {infoList.map(({id, text}) => (
            <ListItem key={id} dangerouslySetInnerHTML={{__html: text}} />
          ))}
        </List>
      </Info>
      {windowSize > 425 ? (
        <Cards>
          {listsData.map(({id, images, title, followers, views}) => (
            <Card key={id} id={id}>
              <CardImages>
                {images.map(({id, image}) => (
                  <Image key={id} src={image} />
                ))}
              </CardImages>
              <CardBody>
                <Title>{title}</Title>
                <Flex>
                  <Followers>
                    <b>{followers}</b> Followers
                  </Followers>
                  <Views>
                    <b>{views}</b> Views
                  </Views>
                </Flex>
                <IconMore>
                  <More width={17.4} height={17.4} />
                </IconMore>
              </CardBody>
            </Card>
          ))}
          <BgImage src={desktopBg} />
        </Cards>
      ) : (
        <MCard>
          <MCardImages>
            {mobListData.images.map(({id, src}) => (
              <MCardImage key={id} src={src} id={id} />
            ))}
          </MCardImages>
          <MCardBody>
            <MTitle>{mobListData.title}</MTitle>
            <MInfo>
              <MFollowers>{mobListData.followers} Followers</MFollowers>
              <MViews>{mobListData.views} Views</MViews>
            </MInfo>
            <MLockIcon src={lockIcon} />
            <MMoreIcon>
              <MoreIcon />
            </MMoreIcon>
          </MCardBody>
          <MBgImage src={mobBg} />
        </MCard>
      )}
    </Wrapper>
  );
};

export default Lists;
