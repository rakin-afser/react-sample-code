import styled from 'styled-components';
import {
  mainWhiteColor as white,
  mainBlackColor as black,
  secondaryTextColor as gray800,
  disabledLinkColor as gray600,
  transparentTextColor as gray300,
  blueColor as blue
} from 'constants/colors';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 166px;

  @media (max-width: 1024px) {
    padding-top: 45px;
    flex-direction: column-reverse;
  }

  @media (max-width: 425px) {
    padding-top: 0;
  }
`;

export const Cards = styled.div`
  width: 565px;
  margin-top: -13px;
  position: relative;

  @media (max-width: 1024px) {
    margin: 35px auto 0 auto;
    width: auto;
  }
`;

export const Card = styled.div`
  display: inline-block;
  width: 168.75px;
  background: ${white};
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 3.375px;
  margin: ${({id}) => (id === 2 ? '27px 0 0 53.25px' : 0)};
  vertical-align: top;
`;

export const CardHeader = styled.div`
  padding: 5.91px 0 4.22px 6.75px;
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const AccountAvatar = styled.img`
  width: 18.49px;
  height: 18.49px;
  object-fit: cover;
`;

export const AccountName = styled.h5`
  margin-bottom: 0;
  margin-left: 6.75px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 10.125px;
  line-height: 12px;
  color: ${black};
  border-radius: 2.53125px;
`;

export const LastUpdate = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 8.4375px;
  line-height: 10px;
  color: ${gray300};
  margin-top: 3.38px;
  margin-bottom: 0;
`;

export const PostImage = styled.img`
  width: 168.75px;
  height: 168.75px;
  object-fit: cover;
`;

export const CardBody = styled.div`
  padding: 10.12px 4.2px 15.19px 6.75px;
`;

export const PostTitle = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10.125px;
  line-height: 12px;
  color: ${black};
  margin-bottom: 7px;
`;

export const UsersAvatars = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const UserAvatar = styled.img`
  width: 16px;
  height: 16px;
  object-fit: cover;

  & + & {
    margin-left: 0.67px;
  }
`;

export const CommentsMore = styled.p`
  margin: 1px 0 0 2px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10.125px;
  line-height: 12px;
  color: ${blue};
`;

export const Views = styled.p`
  margin-bottom: 0;
  margin-left: 22.43px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 8.4375px;
  line-height: 10px;
  color: ${gray800};
`;

export const Likes = styled.p`
  margin-bottom: 0;
  margin-left: 6.75px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 8.4375px;
  line-height: 10px;
  color: ${gray800};
`;

export const BgImage = styled.img`
  position: absolute;
  top: -93px;
  left: -55px;
  z-index: -1;

  @media (max-width: 1024px) {
    height: 460px;
    top: -65px;
  }
`;

export const MCard = styled.div`
  position: relative;
  background: ${white};
  border: 1px solid #eeeeee;
  box-sizing: border-box;
  border-radius: 3px;
  max-width: 225px;
  margin: 65px auto 0 auto;
  padding: 14px 23px 0 23px;
`;

export const MCardHeader = styled.div`
  display: flex;
  flex-direction: row;
`;

export const MCardHeaderRight = styled.div`
  padding-left: 7px;
  padding-top: 9px;
  margin-bottom: 6px;
`;

export const MAccountAvatar = styled.img`
  width: 50px;
  height: 50px;
  object-fit: cover;
  border-radius: 50%;
`;

export const MAccountName = styled.h5`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: ${black};
  margin-bottom: 2px;
`;

export const MLastUpdate = styled.p`
  font-family: 'SF Pro Display';
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: ${gray600};
`;

export const MCardImage = styled.img`
  width: 178px;
  height: 178px;
  object-fit: cover;
`;

export const MCardFooter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
  margin-top: 13px;
`;

export const MLikes = styled.p`
  display: flex;
  flex-direction: row;
`;

export const MLikesValue = styled.span`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 120%;
  display: flex;
  align-items: center;
  letter-spacing: 0.06em;
  color: #666666;
  margin-left: 6px;
`;

export const MViews = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 120%;
  letter-spacing: 0.06em;
  color: ${black};
`;

export const MBgImage = styled.img`
  position: absolute;

  @media (max-width: 425px) {
    top: -104px;
    left: -105px;
    z-index: -1;
    width: 429px;
  }

  @media (max-width: 375px) {
    width: auto;
    top: -90px;
    left: -76px;
  }
`;
