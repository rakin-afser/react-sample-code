import styled from 'styled-components';
import {
  mainWhiteColor as white,
  mainBlackColor as black,
  grayTextColor as gray700,
  headerShadowColor as gray400
} from 'constants/colors';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  padding-top: 180px;

  @media (max-width: 1024px) {
    padding-top: 51px;
    flex-direction: column;
  }
`;

export const Cards = styled.div`
  position: relative;
  margin: 1px 0px 0 16px;

  @media (max-width: 1024px) {
    margin: 60px 0 82px 24%;
  }

  @media (max-width: 768px) {
    margin: 60px 0 82px 14%;
  }

  @media (max-width: 425px) {
    margin: 1px 0px 0 16px;
  }
`;

export const Card = styled.div`
  display: inline-block;
  position: absolute;
  padding: 11.6px;
  background: ${white};
  mix-blend-mode: normal;
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 3.38596px;
  top: ${({id}) => {
    if (id === 1) return '-15px';
    else if (id === 2) return '79px';
    else if (id === 3) return '-13px';
  }};
  left: ${({id}) => {
    if (id === 1) return '-15px';
    else if (id === 2) return '140px';
    else if (id === 3) return '290px';
  }};
`;

export const CardImages = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  position: relative;

  &:after {
    position: absolute;
    bottom: 0;
    left: 0;
    content: '';
    width: 100%;
    height: 0.73px;
    background: ${gray400};
  }
`;

export const Image = styled.img`
  border-radius: 5.8px;

  & + & {
    margin-left: 2.17px;
  }
`;

export const CardBody = styled.div`
  padding: 5.08px 0;
  position: relative;
`;

export const Title = styled.h4`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-size: 12px;
  line-height: 140%;
  color: ${black};
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
`;

export const Followers = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 8.7px;
  line-height: 120%;
  color: ${gray700};
  margin-bottom: 0;
`;

export const Views = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 8.7px;
  line-height: 120%;
  letter-spacing: 0.06em;
  color: ${gray700};
  margin-bottom: 0;
  margin-left: 10px;
`;

export const IconMore = styled.div`
  width: 17.4px;
  height: 17.4px;
  transform: rotate(90deg);
  position: absolute;
  top: 0;
  right: 0;
`;

export const BgImage = styled.img`
  position: absolute;
  top: -161px;
  left: 0;
  z-index: -1;
`;

export const MCard = styled.div`
  position: relative;
  background: ${white};
  border: 1px solid #eeeeee;
  border-radius: 3px;
  padding-top: 16px;
  padding-left: 16px;
  margin-top: 63px;
`;

export const MCardImages = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: nowrap;
  overflow-x: hidden;
`;

export const MCardImage = styled.img`
  width: ${({id}) => (id < 4 ? '92px' : '26px')};
  height: 135px;
  object-fit: cover;
`;

export const MCardBody = styled.div`
  position: relative;
  padding-top: 12px;
`;

export const MTitle = styled.h4`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: ${black};
`;

export const MInfo = styled.div`
  display: flex;
  flex-direction: row;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  line-height: 120%;
  letter-spacing: 0.06em;
  color: #333333;
  padding-top: 2px;
  margin-bottom: 6px;
`;

export const MFollowers = styled.p``;

export const MViews = styled.p`
  margin-left: 11px;
`;

export const MLockIcon = styled.img`
  position: absolute;
  top: 14px;
  right: 51px;
`;

export const MMoreIcon = styled.div`
  position: absolute;
  top: 7px;
  right: 19px;
  transform: rotate(90deg);
`;

export const MBgImage = styled.img`
  position: absolute;
  top: -120px;
  left: -18px;
  z-index: -1;
`;
