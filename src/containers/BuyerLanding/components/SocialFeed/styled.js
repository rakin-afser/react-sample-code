import styled from 'styled-components';
import checkIcon from 'assets/buyerLandingImages/checkIcon.svg';

export const Section = styled.section`
  background: #fef9fa;
  max-height: 466px;
  position: relative;

  @media (max-width: 768px) {
    max-height: initial;
    padding: 55px 35px;
  }

  @media (max-width: 425px) {
    padding: 22px 15px;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 1086px;
  margin: 0 auto;

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const Image = styled.img`
  margin-top: -50px;

  @media (max-width: 768px) {
    margin-top: 0;
    width: 300px;
    height: auto;
  }

  @media (max-width: 425px) {
    margin-top: 0;
    width: 200px;
    height: auto;
  }
`;

export const Info = styled.div`
  width: 470px;
  margin-left: 113px;
  padding-top: 52px;
  z-index: 1;

  @media (max-width: 768px) {
    margin-left: 100px;
  }

  @media (max-width: 768px) {
    margin: 0;
    padding: 0;
    width: auto;
    text-align: center;
  }
`;

export const Title = styled.h3`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: bold;
  font-size: 36px;
  line-height: 140%;
  color: #1e2e3a;
  margin-bottom: 21.96px;

  @media (max-width: 425px) {
    font-size: 24px;
    line-height: 29px;
  }
`;

export const Text = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 140%;
  color: #656565;
  margin-bottom: 50px;

  @media (max-width: 768px) {
    max-width: 540px;
    margin: 0 auto;
  }

  @media (max-width: 425px) {
    margin-bottom: 8px;
    margin-left: 0;
    font-size: 16px;
    line-height: 140%;
  }
`;

export const List = styled.ul`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 140%;
  color: #656565;

  @media (max-width: 425px) {
    font-size: 14px;
    line-height: 140%;
    text-align: left;
    padding: 2px 0 0 24px;
  }
`;

export const ListItem = styled.li`
  padding-left: 35px;
  position: relative;

  & + & {
    margin-top: 36.3px;
  }

  &:before {
    content: '';
    width: 18px;
    height: 12.32px;
    background: url(${checkIcon});
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
  }

  @media (max-width: 768px) {
    padding-left: 0;

    &:before {
      display: none;
    }

    & + & {
      margin-top: 30px;
    }
  }

  @media (max-width: 425px) {
    padding-left: 35px;

    &:before {
      display: block;
    }
  }
`;

export const BgImage = styled.img`
  position: absolute;
  right: 0;
  top: 10px;

  @media (max-width: 768px) {
    top: 145px;
  }

  @media (max-width: 425px) {
    z-index: 0;
    top: 65px;
  }
`;
