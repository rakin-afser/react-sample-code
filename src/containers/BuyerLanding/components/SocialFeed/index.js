import React from 'react';
import {useWindowSize} from '@reach/window-size';
import {Section, Wrapper, Image, Info, Title, Text, List, ListItem, BgImage} from './styled';
import socialFeedImg from 'assets/buyerLandingImages/socialFeedImg.png';
import socialFeedBg from 'assets/buyerLandingImages/socialFeedBg.svg';

const SocialFeed = () => {
  const {width: windowSize} = useWindowSize();

  return (
    <Section>
      <Wrapper>
        {windowSize > 768 && <Image src={socialFeedImg} />}
        <Info>
          <Title>Social Feed</Title>
          <Text>A unique space where you can discover and follow trending Posts, Products &#38; Stores</Text>
          {windowSize <= 768 && <Image src={socialFeedImg} />}
          <List>
            <ListItem>Explore trending posts and prodcuts</ListItem>
            <ListItem>Follow brands, stores or your loved Influencers!</ListItem>
            <ListItem>Share your review, help your friends make a decision &#38; get rewarded</ListItem>
          </List>
        </Info>
      </Wrapper>
      <BgImage src={socialFeedBg} />
    </Section>
  );
};

export default SocialFeed;
