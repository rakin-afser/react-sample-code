import React, {cloneElement} from 'react';
import {Transition} from 'react-transition-group';

const TransitionOpacity = ({active, children}) => {
  const duration = 300;
  const transitionStyles = {
    entering: {
      opacity: 1,
      transition: `opacity ${duration}ms ease-in-out`
    },
    exiting: {
      opacity: 0,
      transition: `opacity ${duration}ms ease-in-out`
    }
  };

  function onEnter(node) {
    node.style.setProperty('opacity', '0');
    return node.offsetHeight;
  }
  return (
    <Transition in={active} timeout={duration} onEnter={onEnter} unmountOnExit>
      {(state) => cloneElement(children, {style: transitionStyles?.[state]})}
    </Transition>
  );
};

export default TransitionOpacity;
