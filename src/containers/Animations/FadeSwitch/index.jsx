import React, {useRef} from 'react';
import {CSSTransition, SwitchTransition} from 'react-transition-group';
import {Container} from './styled';

const FadeSwitch = ({mode = 'out-in', children, state}) => {
  const ref = useRef(null);
  return (
    <Container>
      <SwitchTransition mode={mode}>
        <CSSTransition
          nodeRef={ref}
          key={state}
          addEndListener={(done) => {
            ref.current.addEventListener('transitionend', done, false);
          }}
          classNames="fade"
        >
          <div ref={ref}>{children}</div>
        </CSSTransition>
      </SwitchTransition>
    </Container>
  );
};

export default FadeSwitch;
