import React from 'react';
import {useWindowSize} from '@reach/window-size';
import {useLocation, useParams} from 'react-router-dom';
import qs from 'qs';
import {useQuery} from '@apollo/client';

import DealsDesktop from './desktop';
import DealsMobile from './mobile';
import {CATEGORIES} from 'queries';
import {BANNER} from 'containers/DealsPage/api/queries';
import Loader from 'components/Loader';

const getPageTitle = (page) => {
  switch (page) {
    case 'featured':
      return 'Featured';
    case 'deals':
    default:
      return 'Deals';
  }
};

const DealsPage = () => {
  const {page} = useParams();
  const windowSize = useWindowSize();
  const location = useLocation();
  const selectedCategory = qs.parse(location.search, {ignoreQueryPrefix: true}).category;
  const {data: categoriesData, error: categoriesError, loading: categoriesLoading} = useQuery(CATEGORIES);
  const {data: bannerData, error: bannerError, loading: bannerLoading} = useQuery(BANNER, {
    variables: {slug: 'deals-main-banner-1'}
  });

  const categories = categoriesData?.productCategories?.nodes?.length
    ? [{id: 0, name: 'All', slug: 'all'}, ...categoriesData?.productCategories?.nodes]
    : [];
  // const pageTitle = bannerData?.banner?.title;
  const mainBanner = bannerData?.banner?.featuredImage?.node?.sourceUrl;

  if (categoriesLoading || bannerLoading) return <Loader />;

  return windowSize.width > 768 ? (
    <DealsDesktop
      categories={categories}
      selectedCategory={selectedCategory}
      banners={[mainBanner, mainBanner, mainBanner]}
      title={getPageTitle(page)}
    />
  ) : (
    <DealsMobile selectedCategory={selectedCategory} categories={categories} title={getPageTitle(page)} />
  );
};

export default DealsPage;
