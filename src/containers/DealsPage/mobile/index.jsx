import React, {useState} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import Helmet from 'react-helmet';
import {useQuery} from '@apollo/client';

import Layout from 'containers/Layout';
import IconMore from 'assets/More';
import IconArrowBack from 'assets/ArrowBack';
import {ReactComponent as IconArrowDown} from 'images/icons/arrowDown.svg';
import {mainBlackColor as black} from 'constants/colors';
import Categories from './components/Categories';
import ProductItem from './components/ProductItem';
import DropdownMenu from './components/DropdownMenu';
import {
  Header,
  ButtonBack,
  Title,
  ButtonMore,
  Tools,
  ItemsCount,
  ButtonSort,
  ButtonSortText,
  ItemsGrid,
  Left,
  Right
} from './styled';
import {PRODUCTS} from 'queries';
import Skeleton from 'containers/SearchResult/components/Skeleton';
import NothingFound from 'components/NothingFound';

const getVariables = (page, categorySlug) => {
  switch (page) {
    case 'featured':
      return {
        first: 12,
        featured: true,
        categorySlug: categorySlug === 'all' ? null : categorySlug
      };
    case 'deals':
    default:
      return {
        first: 12,
        isDealProduct: true,
        categorySlug: categorySlug === 'all' ? null : categorySlug
      };
  }
};

const getProductsColumns = (data) => {
  const itemsLeftCol = [];
  const itemsRightCol = [];

  data.forEach((item, index) => {
    const currentNumber = index + 1;

    if (currentNumber % 2 === 0) {
      itemsRightCol.push(item);
    } else {
      itemsLeftCol.push(item);
    }
  });

  return [itemsLeftCol, itemsRightCol];
};

const DealsMobile = ({categories, selectedCategory, title}) => {
  const {page} = useParams();
  const [showDropdownMenu, setShowDropdownMenu] = useState(false);
  const history = useHistory();
  const {data: productsData, loading: productsLoading, error: productsError} = useQuery(PRODUCTS, {
    variables: getVariables(page, selectedCategory),
    fetchPolicy: 'cache-and-network'
  });
  const data = productsData?.products?.nodes || [];

  const goBack = () => history.push('/');

  let headerTitle = title;

  if (selectedCategory) {
    headerTitle = categories?.find((item) => item.slug === selectedCategory)?.name;
  }

  const [itemsLeftCol, itemsRightCol] = getProductsColumns(data);

  return (
    <Layout hideHeader hideFooter>
      {showDropdownMenu && <DropdownMenu closeMenu={() => setShowDropdownMenu(false)} isOpen={showDropdownMenu} />}
      <Helmet>
        <meta charSet="UTF-8" />
      </Helmet>
      <Header>
        <ButtonBack onClick={goBack}>
          <IconArrowBack height={26} stroke={black} />
        </ButtonBack>
        <Title>{headerTitle}</Title>
        <ButtonMore onClick={() => setShowDropdownMenu(true)}>
          <IconMore />
        </ButtonMore>
      </Header>
      <Categories page={page} categories={categories} selectedCategory={selectedCategory} />
      <Tools>
        <ItemsCount>{data?.length} Items</ItemsCount>
        <ButtonSort>
          <ButtonSortText>Sort</ButtonSortText>
          <IconArrowDown />
        </ButtonSort>
      </Tools>
      <ItemsGrid>
        {productsLoading ? (
          <Skeleton />
        ) : (
          <>
            <Left>
              {itemsLeftCol.map((item, index) => (
                <ProductItem key={item?.id} item={item} isBigImage={(index + 1) % 2 === 0} />
              ))}
            </Left>
            <Right>
              {itemsRightCol.map((item, index) => (
                <ProductItem key={item?.id} item={item} isBigImage={(index + 1) % 2 !== 0} />
              ))}
            </Right>
          </>
        )}
        {!data?.length && !productsLoading && <NothingFound hideAdditionalText hideHelpText />}
      </ItemsGrid>
    </Layout>
  );
};

export default DealsMobile;
