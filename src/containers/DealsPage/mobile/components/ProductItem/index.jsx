import React from 'react';
import {ThemeProvider} from 'styled-components';
import Icon from 'components/Icon';
import {truncateText} from 'util/heplers';
import Action from '../../../../../components/Actions';
import {
  Item,
  Image,
  Body,
  Price,
  CurrentPrice,
  CurrentPriceValue,
  PriceWithoutDiscount,
  Name,
  Label,
  DiscountLabel,
  ButtonMore,
  Likes,
  ButtonLike,
  LikesNumber
} from './styled';
import {useUser} from 'hooks/reactiveVars';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';

const ProductItem = (props) => {
  const {item, isBigImage} = props || {};
  const {
    name,
    databaseId,
    image: {sourceUrl},
    price,
    isDiscount,
    discountPercent,
    priceWithoutDiscount,
    isLiked,
    type,
    totalLikes,
    shippingType
  } = item || {};
  const [user] = useUser();
  const formatedPrice = type === 'SIMPLE' ? price : price?.split('-')[0];

  const [triggerLike] = useAddUpdateLikedProductMutation({
    variables: {input: {id: databaseId}},
    product: item
  });

  const onUserLike = () => {
    if (user?.databaseId) {
      triggerLike();
    }
  };

  return (
    <Item>
      <Image src={sourceUrl} $isBigImage={isBigImage} />
      <Body>
        <ThemeProvider theme={{isDiscount}}>
          <Price>
            <CurrentPrice>
              <CurrentPriceValue>{formatedPrice}</CurrentPriceValue>
            </CurrentPrice>
            {isDiscount && <PriceWithoutDiscount>{priceWithoutDiscount?.toFixed(2)}</PriceWithoutDiscount>}
          </Price>
        </ThemeProvider>
        <Name>{truncateText(name, 20)}</Name>
        <Label>{shippingType}&nbsp;</Label>
        <Likes>
          <ButtonLike onClick={onUserLike}>
            <Icon width={12} height={11} type={isLiked ? 'liked' : 'like'} color="#8F8F8F" />
          </ButtonLike>
          <LikesNumber>{totalLikes}</LikesNumber>
        </Likes>
      </Body>
      {discountPercent && <DiscountLabel>-{discountPercent}%</DiscountLabel>}
      <ButtonMore>
        <Action />
      </ButtonMore>
    </Item>
  );
};

export default ProductItem;
