import React from 'react';
import qs from 'qs';
import {Wrapper, Categories, Category} from './styled';

export default ({categories, selectedCategory, page}) => (
  <Wrapper>
    <Categories>
      {categories?.map((category) => {
        let to = `/${page}`;

        if (category.name) {
          to += qs.stringify({category: category.slug}, {addQueryPrefix: true});
        }

        return (
          <Category key={category.id} to={to} $isActive={category.slug === selectedCategory}>
            {category.name}
          </Category>
        );
      })}
    </Categories>
  </Wrapper>
);
