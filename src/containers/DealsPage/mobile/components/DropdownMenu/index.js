import React from 'react';
import {useHistory} from 'react-router-dom';
import {primaryColor as primary} from 'constants/colors';
import IconCart from 'assets/Cart';
import {Backdrop, Menu, NavLinks, StyledNavLink, Buttons, Button, IconWrapper, Languages, Language} from './styled';
import {useUser} from 'hooks/reactiveVars';

const DropdownMenu = ({closeMenu}) => {
  const [user] = useUser();
  const databaseId = user?.databaseId;
  const navLinks = [];
  const buttons = [];

  const mode = () => {
    if (databaseId) return 'registeredUser';
    return 'notRegisteredUser';
  };
  const history = useHistory();

  switch (mode()) {
    // case 'visitor':
    //   navLinks.push(
    //     {id: 1, to: '/', title: 'Home Page'},
    //     {id: 2, to: '/feed', title: 'Feed'},
    //     {id: 3, to: '/categories', title: 'Categories'},
    //     {id: 4, to: '/profile/lists/wishlist', title: 'My Wishlist'},
    //     {id: 5, to: '/account-settings', title: 'My Account'}
    //   );
    //   break;
    case 'registeredUser':
      navLinks.push(
        {id: 1, to: '/account-settings', title: 'My Account'},
        {id: 2, to: '/', title: 'Shop Page'},
        {id: 3, to: '/feed', title: 'Feed'},
        {id: 4, to: '/categories', title: 'Categories'},
        {id: 5, to: '/profile/lists/wishlist', title: 'My Wishlist'},
        {id: 5, to: '/cart', title: 'Go to Cart', icon: <IconCart />, color: primary}
      );
      break;
    case 'notRegisteredUser':
      navLinks.push(
        {id: 1, to: '/', title: 'Marketplace'},
        {id: 2, to: '/feed', title: 'Feed'},
        {id: 3, to: '/categories', title: 'Categories'}
      );
      buttons.push(
        {id: 1, title: 'Sign In', onClick: () => history.push('/auth/sign-in'), variant: 'primary'},
        {id: 2, title: 'Create Account', onClick: () => history.push('/auth/sign-up'), variant: 'dark'}
      );
      break;
  }

  return (
    <Backdrop onClick={closeMenu}>
      <Menu>
        {navLinks.length !== 0 && (
          <NavLinks>
            {navLinks.map(({id, to, title, icon, color}) => (
              <StyledNavLink key={id} to={to} $color={color}>
                {icon && <IconWrapper>{icon}</IconWrapper>}
                {title}
              </StyledNavLink>
            ))}
          </NavLinks>
        )}
        {buttons.length !== 0 && (
          <Buttons>
            {buttons.map(({id, title, onClick, variant}) => (
              <Button key={id} onClick={onClick} $variant={variant}>
                {title}
              </Button>
            ))}
          </Buttons>
        )}
        {/* <Languages>
          <Language $isSelected>English</Language>
          <Language>لغة</Language>
        </Languages> */}
      </Menu>
    </Backdrop>
  );
};

export default DropdownMenu;
