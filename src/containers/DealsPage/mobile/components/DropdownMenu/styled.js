import styled from 'styled-components';
import {NavLink} from 'react-router-dom';
import {
  mainWhiteColor as white,
  mainBlackColor as black,
  headerShadowColor as gray400,
  primaryColor as primary
} from 'constants/colors';
import arrowRight from 'images/icons/arrowRight.svg';

export const Backdrop = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 1;
  background: rgba(0, 0, 0, 0.55);
`;

export const Menu = styled.div`
  width: 210px;
  background-color: ${white};
  border-radius: 12px;
  position: absolute;
  top: 50px;
  right: 20px;
`;

export const NavLinks = styled.nav`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding-top: 8px;
`;

export const StyledNavLink = styled(NavLink)`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: ${({$color}) => $color || black};
  padding: 10px 15px;
  border-bottom: 1px solid #efefef;
  position: relative;
  display: flex;

  &:after {
    content: '';
    background: url(${arrowRight});
    position: absolute;
    width: 8px;
    height: 13px;
    top: 50%;
    right: 15px;
    transform: translateY(-50%);
  }
`;

export const IconWrapper = styled.div`
  height: 1px;
  margin-top: 1px;
  margin-right: 7px;
`;

export const Buttons = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 45px 15px;
`;

export const Button = styled.button`
  width: 135px;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  padding: 3px;
  background-color: ${white};
  border-radius: 24px;

  ${({$variant}) => {
    switch ($variant) {
      case 'primary':
        return `
          color: ${primary};
          border: 1px solid ${primary};

          &:hover,
          &:focus {
            color: ${white};
            background-color: ${primary};
          }
        `;
      case 'dark':
        return `
          color: #333333;
          border: 1px solid #333333;

          &:hover,
          &:focus {
            color: ${white};
            background-color: #333333;
          }
        `;
    }
  }}

  & + & {
    margin-top: 10px;
  }
`;

export const Languages = styled.div`
  display: flex;
  flex-direction: row;
  padding: 15px;
`;

export const Language = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  color: #5c5b5b;
  font-weight: ${({$isSelected}) => ($isSelected ? 'bold' : '300')};
  position: relative;

  & + & {
    margin-left: 8px;
    padding-left: 10px;

    &:before {
      content: '';
      width: 2px;
      height: 28px;
      position: absolute;
      top: -5px;
      left: 0;
      background: ${gray400};
    }
  }
`;

export const Wrapper = styled.div`
  width: 100%;
  opacity: ${({opacity}) => (opacity ? 0 : 1)};
  transition: all ease 0.6s;
`;
