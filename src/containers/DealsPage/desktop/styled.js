import styled from 'styled-components';
import {HashLink} from 'react-router-hash-link';
import {mainBlackColor as black, primaryColor as primary, menuTitleColor as gray900} from 'constants/colors';

export const Wrapper = styled.div`
  max-width: 1200px;
  margin: 0 auto;
  padding: 36px 0;
`;

export const Title = styled.h2`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 29px;
  color: ${black};
`;

export const SliderWrapper = styled.div`
  position: relative;
  padding: 24px 0;

  &:before {
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    width: 100%;
    height: 2px;
    background-color: ${primary};
  }

  &:after {
    position: absolute;
    content: '';
    bottom: 0;
    left: 0;
    width: 100%;
    height: 2px;
    background-color: ${primary};
  }

  .slick-slider .slick-prev {
    left: 10px;
    z-index: 999;
  }

  .slick-slider .slick-next {
    right: 27px;
  }

  .slick-slider .slick-prev:before,
  .slick-slider .slick-next:before {
    background-position: center;
    background-color: rgba(255, 255, 255, 0.3);
    width: 37px !important;
    height: 37px !important;
    background-size: 10.79px 18.5px;
    mix-blend-mode: lighten;
    background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTIiIGhlaWdodD0iMTkiIHZpZXdCb3g9IjAgMCAxMiAxOSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xMC43Mjk5IDE4LjM2M0MxMS4yNTY3IDE3Ljg0NjkgMTEuMjU2NyAxNy4wMTAyIDEwLjcyOTkgMTYuNDk0MkwzLjU5MDAxIDkuNUwxMC43Mjk5IDIuNTA1ODJDMTEuMjU2NyAxLjk4OTc3IDExLjI1NjcgMS4xNTMwOSAxMC43Mjk5IDAuNjM3MDM3QzEwLjIwMzEgMC4xMjA5ODkgOS4zNDg5OSAwLjEyMDk4OSA4LjgyMjE5IDAuNjM3MDM3TDAuNzI4NDMzIDguNTY1NjFDMC4yMDE2MzIgOS4wODE2NiAwLjIwMTYzMiA5LjkxODM0IDAuNzI4NDMzIDEwLjQzNDRMOC44MjIxOCAxOC4zNjNDOS4zNDg5OCAxOC44NzkgMTAuMjAzMSAxOC44NzkgMTAuNzI5OSAxOC4zNjNaIiBmaWxsPSIjRkFGQUZBIi8+Cjwvc3ZnPgo=);
    background-repeat: no-repeat;
    border-radius: 3px;
  }

  .slick-slider .slick-dots {
    bottom: -24px !important;

    & .slick-active button:before {
      color: #ea2c34;
    }

    & button:before {
      font-size: 10px;
      color: #e4e4e4;
      opacity: 1 !important;
    }
  }

  .slick-dots li {
    margin: 0 -2px;
  }
`;

export const SliderImage = styled.img``;

export const Categories = styled.nav`
  padding: 18px 0 32px;
`;

export const Category = styled(HashLink)`
  font-family: Helvetica Neue;
  font-style: normal;
  font-size: 16px;
  line-height: 140%;
  color: ${black};

  & + & {
    margin-left: 32px;
  }
`;

export const SubTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 18px;
  line-height: 22px;
  color: ${black};
  position: relative;
  padding-bottom: 16px;
  margin-bottom: 24px;

  &:after {
    position: absolute;
    content: '';
    bottom: 0;
    left: 0;
    width: 100%;
    height: 2px;
    background-color: ${gray900};
  }
`;

export const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-gap: 40px;
`;

export const BlockWrapper = styled.section`
  padding-bottom: 32px;
`;
