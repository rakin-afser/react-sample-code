import React from 'react';
import BookmarkIcon from 'assets/Bookmark';
import HeartIcon from 'assets/Heart';
import EnlargeIcon from 'assets/Enlarge';
import {
  Card,
  CardImage,
  Image,
  CardImageShadow,
  EnlargeIconWrapper,
  CardBody,
  Flex,
  Price,
  PriceBD,
  PriceValue,
  PriceWithoutDiscount,
  Name,
  Description,
  Actions,
  FavouriteItem,
  WishedItem,
  Discount
} from './styled';

const ProductCard = ({
  name,
  description,
  img,
  price,
  isDiscount,
  discountPercent,
  priceWithoutDiscount,
  isLiked,
  isWished
}) => (
  <Card>
    <CardImage>
      <Image src={img} />
      <CardImageShadow>
        <EnlargeIconWrapper>
          <EnlargeIcon />
        </EnlargeIconWrapper>
      </CardImageShadow>
    </CardImage>
    <CardBody>
      <Flex>
        <Price>
          <PriceBD>BDT&nbsp;</PriceBD>
          <PriceValue isDiscount={isDiscount}>{price.toFixed(3)}</PriceValue>
          {isDiscount && <PriceWithoutDiscount>BD{priceWithoutDiscount}</PriceWithoutDiscount>}
        </Price>
        <Description>{description}</Description>
      </Flex>
      <Name>{name}</Name>
      <Actions>
        <FavouriteItem>
          <HeartIcon isLiked={isLiked} />
        </FavouriteItem>
        <WishedItem>
          <BookmarkIcon isWished={isWished} width={20} height={20} />
        </WishedItem>
      </Actions>
    </CardBody>
    {isDiscount && (
      <Discount>
        -<b>{discountPercent}</b>
        &#37;
      </Discount>
    )}
  </Card>
);

export default ProductCard;
