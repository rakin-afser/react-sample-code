import styled from 'styled-components';
import {
  mainBlackColor as black,
  mainWhiteColor as white,
  primaryColor as primary,
  menuTitleColor as gray900,
  transparentTextColor as gray300
} from 'constants/colors';

export const CardImage = styled.div`
  width: 262px;
  height: 266px;
  object-fit: cover;
  position: relative;
`;

export const Image = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const CardImageShadow = styled.div`
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.6);
  display: none;
`;

export const EnlargeIconWrapper = styled.div`
  width: 39px;
  height: 39px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background: ${white};
  border-radius: 50%;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const Card = styled.div`
  width: 100%;
  max-width: 262px;
  background: ${white};
  box-shadow: 0px 2px 25px rgba(0, 0, 0, 0.1);
  border-radius: 3.38596px;
  position: relative;

  &:hover {
    cursor: pointer;

    ${CardImageShadow} {
      display: block;
    }
  }
`;

export const CardBody = styled.div`
  position: relative;
  padding: 8px 10px;
`;

export const Flex = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Price = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
  margin-bottom: 5px;
`;

export const PriceBD = styled.span`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
`;

export const PriceValue = styled.span`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  color: ${({isDiscount}) => (isDiscount ? primary : black)};
  display: flex;
  align-items: center;
  margin-top: -1px;
`;

export const PriceWithoutDiscount = styled.span`
  margin-left: 8.1px;
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 13px;
  line-height: 132%;
  text-decoration-line: line-through;
  text-transform: uppercase;
  color: ${gray900};
  display: flex;
  align-items: center;
`;

export const Name = styled.h4`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: #656565;
`;

export const Description = styled.p`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  color: ${gray300};
`;

export const Actions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding-top: 15px;
`;

export const FavouriteItem = styled.div``;

export const WishedItem = styled.div``;

export const Discount = styled.div`
  background: ${primary};
  border-radius: 3.38596px;
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-size: 10.1579px;
  line-height: 12px;
  padding: 2px 2px 1px 4px;
  color: ${white};
  position: absolute;
  right: 4px;
  top: 4px;
`;
