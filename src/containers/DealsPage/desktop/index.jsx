import React from 'react';
import Slider from 'react-slick';
import _ from 'lodash';
import {useQuery} from '@apollo/client';
import {useParams} from 'react-router-dom';

import Layout from 'containers/Layout';
import {Wrapper, Title, SliderWrapper, SliderImage, Categories, Category, SubTitle, Grid, BlockWrapper} from './styled';
import Product from 'components/Cards/Product';
import {PRODUCTS} from 'queries';
import Skeleton from 'containers/SearchResult/components/Skeleton';

const getVariables = (page, categorySlug) => {
  switch (page) {
    case 'featured':
      return {featured: true, first: 8, categorySlug: categorySlug === 'all' ? null : categorySlug};
    case 'deals':
    default:
      return {isDealProduct: true, first: 8, categorySlug: categorySlug === 'all' ? null : categorySlug};
  }
};

const ProductsBlock = ({category}) => {
  const {page} = useParams();
  const {data, loading, error} = useQuery(PRODUCTS, {
    variables: getVariables(page, category?.slug)
  });

  if (loading) return <Skeleton />;
  if (_.isEmpty(data?.products?.nodes) || error) return null;
  return (
    <BlockWrapper id={category?.slug}>
      <SubTitle>{category?.name}</SubTitle>
      <Grid>
        {data?.products?.nodes?.map((item, i) => (
          <Product key={i} index={i} content={item} margin="0 0 48px 0" />
        ))}
      </Grid>
    </BlockWrapper>
  );
};

const DealsPage = ({categories, selectedCategory, banners, title = ''}) => {
  return (
    <Layout>
      <Wrapper>
        <Title>{title}</Title>
        <SliderWrapper>
          <Slider>
            {banners.map((item, index) => (
              <SliderImage key={index} src={item} alt="" />
            ))}
          </Slider>
        </SliderWrapper>
        <Categories>
          {categories?.map(({id, name, slug}) => (
            <Category key={id} to={`/deals#${slug}`}>
              {name}
            </Category>
          ))}
        </Categories>
        {categories?.map((item) => (
          <ProductsBlock category={item} scrollToThisBlock={selectedCategory === item?.slug} />
        ))}
      </Wrapper>
    </Layout>
  );
};

export default DealsPage;
