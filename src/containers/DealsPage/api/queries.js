import {gql} from '@apollo/client';

export const BANNER = gql`
  query Banner($slug: ID!) {
    banner(id: $slug, idType: SLUG) {
      id
      title
      content
      featuredImage {
        node {
          id
          sourceUrl
        }
      }
    }
  }
`;
