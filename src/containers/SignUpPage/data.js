export const signInData = {
  heading: 'Sign in',
  title: "Don't have an account yet? Create Account",
  submitTitle: 'Sign in',
  fields: [
    {
      label: 'Email or Username ',
      type: 'email',
      errorMessage: 'Please enter a valid username or email address',
      isRequired: true
    },
    {
      label: 'Password',
      type: 'password',
      errorMessage: 'Please enter a valid password',
      isRequired: true
    }
  ]
};

export const fakeBackendSignInErrors = {
  invalidCredentials: {
    type: 'error',
    title: 'Invalid Username or Password',
    text: 'Please try again or click Forgot Username or Forgot Password link below.'
  },
  temporarilyLocked: {
    type: 'error',
    title: '',
    text:
      'Your account is temporarily locked to prevent unauthorised access. Please try again later or contact admin@testSample.com'
  },
  locked: {
    type: 'error',
    title: '',
    text: 'Your account has been locked or suspended. Please contact admin@testSample.com'
  }
};
