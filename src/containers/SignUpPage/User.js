export class User {
  constructor(url, lastName, firstName, username, email) {
    this.avatar = {
      url: url
    };
    this.capabilities = [];
    this.company = '';
    this.country = '';
    this.databaseId = '';
    this.description = '';
    this.education = '';
    this.email = email;
    this.firstName = firstName;
    this.gender = '';
    this.id = '';
    this.job_title = '';
    this.lastName = lastName;
    this.name = firstName + ' ' + lastName;
    this.userId = '';
    this.username = username;
    this.url = url;
  }
}
