import React, {useState, useEffect} from 'react';
import {Auth} from 'aws-amplify';
import axios from 'axios';
import {useHistory} from 'react-router-dom';
import {Helmet} from "react-helmet";
import {Formik, useFormik} from 'formik';
import AuthHeading from '../../components/UserAuth/components/Heading';
import AuthTint from '../../components/UserAuth/components/Tint';
import {Wrapper, SignForm, Content, FullWidthFormField, DoubleFormFields, Agreements, BlueText} from './styled';
import ButtonCustom from '../../components/ButtonCustom';
import SeparatorLineAndText from '../../components/UserAuth/components/SeparatorLineAndText';
import FormMessage from '../../components/FormMessage';
import Input from '../../components/Input';
import {validateEmail, validatePassword} from '../../util/validators';
import Checkbox from '../../components/Checkbox';
import SocialButton from '../../components/UserAuth/components/SocialButton';
import LogoIcon from '../../assets/LogoIcon';
import {gql, useMutation, useApolloClient} from '@apollo/client';
import useGlobal from 'store';
import {Login, REGISTER_SOCIAL_CUSTOMER, UpdatedUser} from 'mutations';
import {GoogleLogin} from 'react-google-login';
import {useUser} from 'hooks/reactiveVars';
import {User} from './User';
import {FB_APP_ID, GOOGLE_APP_ID, localStorageKey} from 'constants/config';
import {GoogleApiProvider} from 'react-gapi';
import {getErrorByCode} from 'util/responseDecoding';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import {GET_INTERESTS} from 'queries';
import {getSelectedInterests} from 'util/heplers';
const ValidateEmail = gql`
  mutation ValidateEmail($input: ValidateEmailInput!) {
    validateEmail(input: $input) {
      message
      code
    }
  }
`;

const ValidateUsername = gql`
  mutation ValidateUsername($input: ValidateUsernameInput!) {
    validateUsername(input: $input) {
      message
      code
    }
  }
`;

const SendOTP = gql`
  mutation SendOTP($input: SendOTPInput!) {
    sendOTP(input: $input) {
      message
      code
    }
  }
`;

const SignUpComponent = ({isMobile, onClickClose}) => {
  const client = useApolloClient();
  const {push} = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const [locked, setLocked] = useState(false);
  const [formValid, setFormValid] = useState(false);
  const [prevUsername, setPrevUsername] = useState();
  const [prevEmail, setPrevEmail] = useState();
  const [usernameAvailability, setUsernameAvailability] = useState(null);
  const [emailAvailability, setEmailAvailability] = useState(false);
  const [, setUser] = useUser();
  const [validEmail] = useMutation(ValidateEmail);
  const [validUsername] = useMutation(ValidateUsername);
  const [sendOTP] = useMutation(SendOTP);
  const [signUpMutation] = useMutation(REGISTER_SOCIAL_CUSTOMER);
  const [updateUser] = useMutation(UpdatedUser);
  const [global, globalActions] = useGlobal();
  const [login] = useMutation(Login);
  const [localUser, setLocalUser] = useState(null);
  const [notificationMessage, setNotificationMessage] = useState();
  const [preSocialValues, setPreSocialValues] = useState(null);
  let userNameRef = null;
  useEffect(() => {
    if (userNameRef) {
      userNameRef.focus();
    }
  }, []);

  useEffect(() => {
    if (global?.signUpInfo) {
      setLocalUser(global.signUpInfo);
    }
  }, []);

  const checkNameAvailability = async (name) => {
    try {
      const {data} = await validUsername({
        variables: {
          input: {
            username: name
          }
        }
      });
      return data?.validateUsername?.code === 404;
    } catch (err) {
      console.log(err);
    }
  };
  const checkEmailAvailability = async (email) => {
    try {
      const {data} = await validEmail({
        variables: {
          input: {
            email
          }
        }
      });

      return data?.validateEmail?.code === 404;
    } catch (err) {
      console.log(err);
    }
  };

  const signUp = async (formData) => {
    setIsLoading(true);
    setErrorMessage(null);

    try {
      if (localUser != null && localUser.email) {
        setIsLoading(true);

        const {data, error, loading} = await signUpMutation({
          variables: {
            input: {
              email: localUser.email,
              firstName: formData.firstName,
              lastName: formData.lastName,
              password: '',
              username: formData.username
            }
          }
        });
        if (!error && !loading) {
          const {data: dataL, loading: loadingL, error: errorL} = await login({
            variables: {input: {username: localUser.email, password: '', social_login: true}}
          });
          if (!errorL && !loadingL) {
            setUser(Object.assign({}, dataL.login.user, {avatar: {url: localUser.avatar}}));
            localStorage.setItem('userId', new String(dataL.login.user.databaseId));

            localStorage.setItem('token', dataL.login.authToken);
            localStorage.setItem(localStorageKey.socialLogin, true);
            localStorage.setItem('avatar', localUser.avatar);
            setIsLoading(false);
            userInterestCheck().then();
          }
          const {data: dataP, error: errorP, loading: loadingP} = await updateUser({
            variables: {
              input: {
                id: dataL.login.user.databaseId,
                profile_picture: localUser.avatar
              }
            },
            update(cache, {data}) {
              cache.modify({
                fields: {
                  user() {
                    setUser(data.updateUser.user);
                    return data.updateUser.user;
                  }
                }
              });
            }
          });
        }
      } else {
        const {email} = formData;

        if (formValid) {
          await sendOTP({
            variables: {
              input: {
                email
              }
            }
          });
          setIsLoading(false);
          localStorage.setItem(localStorageKey.socialLogin, false);
          push('/auth/otp-verification', formData);
        }
      }
    } catch (error) {
      // setErrorMessage({
      //   type: 'error',
      //   title: '',
      //   text: error.message
      // });
      setIsLoading(false);
      console.log('error signing up:', error.message);
    }
  };
  const userInterestCheck = () => {
    return new Promise((resolve, reject) => {
      client
        .query({
          query: GET_INTERESTS
        })
        .then((res) => {
          setIsLoading(false);
          setErrorMessage(false);
          if (getSelectedInterests(res.data) < 3) {
            push('/user-onboarding');
          } else {
            push('/');
          }

          return resolve();
        });
    });
  };

  return (
    <Wrapper>
      <Helmet>
        <title>Create Account with testSample</title>
        <meta name="description" content="testSample makes it easy to create an account with just a few clicks"></meta>
      </Helmet>
      {isMobile && (
        <Content>
          <LogoIcon
            styles={{
              width: 119,
              height: 33,
              marginTop: 24
            }}
          />
        </Content>
      )}
      <AuthHeading
        title="Create account with testSample"
        onClickClose={onClickClose}
        onClickBack={() => push('/auth/sign-in')}
        isMobile={isMobile}
      />
      <Content>{errorMessage && <FormMessage styles={{marginBottom: 27}} message={errorMessage} />}</Content>
      <AuthTint isMobile={isMobile} isSingIn={false} />

      <Formik
        initialValues={{
          firstName: null,
          lastName: null,
          email: null,
          username: '', // global.signUpInfo && global.signUpInfo.email ? global.signUpInfo.email.split('@')[0] :
          password: '',
          agreements: false
        }}
        validate={async (values) => {
          const errors = {};

          const emailValid = validateEmail(localUser?.email || values.email);
          if (values.username && values.username !== prevUsername) {
            await checkNameAvailability(values.username).then((availability) =>
              setUsernameAvailability(!!availability)
            );
          }

          if (emailValid.isValid && values.email !== prevEmail) {
            await checkEmailAvailability(values.email).then((availability) => {
              setEmailAvailability(!!availability);
            });
          }

          setPrevUsername(values.username);
          setPrevEmail(values.email);

          if (!values.firstName) errors.firstName = 'Please enter your First Name';

          if (!values.lastName) errors.lastName = 'Please enter your Last Name';

          if (!values.agreements) errors.agreements = 'Error';
          if (localUser == null) {
            const passwordValid = validatePassword(values.password);
            if (!passwordValid.isValid) errors.password = passwordValid.message;
          }

          if (!values.username) {
            errors.username = 'Please select a Username';

            if (values.username && values.username.length) setUsernameAvailability(false);
          }

          if (!emailValid.isValid) {
            errors.email = emailValid.message;
            setEmailAvailability(false);
          }

          let hasError = false;
          for (let key in errors) {
            hasError = !!errors[key];
            if (hasError) break;
          }

          setFormValid(!hasError);
          return errors;
        }}
        onSubmit={(values) => {
          signUp(values).then();
        }}
      >
        {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur, setFieldTouched}) => {
          if (localUser != null) {
            if (values?.firstName == null || values?.firstName == 'null') {
              setFieldValue('firstName', localUser?.firstName);
            }
            if (values?.lastName == null || values?.lastName == 'null') {
              setFieldValue('lastName', localUser?.lastName);
            }
            if (values?.email == null || values?.email == 'null') {
              setFieldValue('email', localUser?.email);

              setFieldTouched('email', true);
            }
            if (preSocialValues != null && preSocialValues?.email != localUser?.email) {
              setPreSocialValues(localUser);
              setTimeout(() => {
                setFieldValue('email', localUser?.email);
                setFieldTouched('email', true);
              }, 100);
            }
          }

          return (
            <SignForm onSubmit={handleSubmit} autoComplete="nope" noValidate>
              <Content>
                <DoubleFormFields>
                  <Input
                    required
                    key="firstName"
                    type="text"
                    name="firstName"
                    id="firstName"
                    label="First Name"
                    placeholder="Enter your first name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.firstName && touched.firstName ? errors.firstName : ''}
                    value={values.firstName}
                    reset={values.firstName?.length > 0}
                    resetCallback={() => setFieldValue('firstName', '')}
                  />
                  <Input
                    required
                    key="lastName"
                    type="text"
                    name="lastName"
                    id="lastName"
                    label="Last Name"
                    placeholder="Enter your last name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={errors.lastName && touched.lastName ? errors.lastName : ''}
                    value={values.lastName}
                    reset={values.lastName?.length > 0}
                    resetCallback={() => setFieldValue('lastName', '')}
                  />
                </DoubleFormFields>

                <FullWidthFormField>
                  <Input
                    required
                    disabled={localUser && localUser.email ? true : false}
                    key="email"
                    type="email"
                    name="email"
                    id="email"
                    label="Email"
                    placeholder="Enter your email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    reset={localUser && localUser?.email ? false : true}
                    userNameAvailable={emailAvailability}
                    error={
                      errors.email && touched.email
                        ? errors.email
                        : !emailAvailability && touched.email
                        ? 'This email address already exists. Please enter another one.'
                        : ''
                    }
                    value={localUser?.email || values.email}
                    reset={values.email?.length > 0}
                    resetCallback={() => setFieldValue('email', '')}
                  />
                </FullWidthFormField>

                <FullWidthFormField>
                  <Input
                    required
                    key="username"
                    ref={(ref) => {
                      userNameRef = ref;
                    }}
                    type="text"
                    name="username"
                    id="username"
                    label="Select Username"
                    placeholder="Enter your username"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    autoComplete="nope"
                    userNameAvailable={usernameAvailability}
                    error={
                      usernameAvailability === false && touched.username === true
                        ? 'This Username is already taken. Please select another one.'
                        : errors.username && touched.username
                        ? errors.username
                        : ''
                    }
                    value={values.username}
                    reset={values.username.length > 0}
                    resetCallback={() => setFieldValue('username', '')}
                  />
                </FullWidthFormField>

                {localUser && localUser.email ? null : (
                  <FullWidthFormField>
                    <Input
                      required
                      passwordChars={true}
                      key="password"
                      type="password"
                      name="password"
                      id="password"
                      label="Password"
                      placeholder="Enter your password"
                      onChange={handleChange}
                      autoComplete="new-password"
                      onBlur={handleBlur}
                      error={errors.password && touched.password && values.password.length < 1 ? errors.password : ''}
                      value={values.password}
                      showPassword={true}
                    />
                  </FullWidthFormField>
                )}

                <Agreements>
                  <Checkbox name="agreements" id="agreements" onChange={handleChange} checked={values.agreements}>
                    <>
                      I agree to{' '}
                      <a target={'_blank'} href={'/terms-conditions'}>
                        Terms of Use
                      </a>
                      ,{' '}
                      <a target={'_blank'} href={'/privacy-policy'}>
                        Privacy Policy
                      </a>{' '}
                      and <BlueText>Cookies Policy</BlueText>.
                    </>
                  </Checkbox>
                </Agreements>
                <ButtonCustom
                  type="submit"
                  isLoading={isLoading}
                  title="Register"
                  variant="contained"
                  color="mainWhiteColor"
                  iconRight="chevronRight"
                  disabled={locked || !formValid || !emailAvailability || Object.keys(errors).length > 0}
                  styles={{margin: isMobile ? '19px auto 0 auto' : '28px auto 0 auto'}}
                />
              </Content>
              <SeparatorLineAndText />
              <Content>
                <GoogleLogin
                  clientId={GOOGLE_APP_ID}
                  render={(renderProps) => (
                    <SocialButton
                      setLocked={setLocked}
                      setIsLoading={setIsLoading}
                      login={login}
                      socialName="google"
                      disabled={renderProps.disabled}
                      setLocalUser={(newValues) => {
                        if (localUser != null) {
                          setPreSocialValues(localUser);
                        }
                        setLocalUser(newValues);
                      }}
                      styles={{
                        marginBottom: 16
                      }}
                      onClick={renderProps.onClick}
                    />
                  )}
                  onSuccess={(response) => {
                    console.log(response.profileObj);
                    const {email, familyName, givenName, imageUrl} = response.profileObj;
                    if (email) {
                      const formatedResponse = {
                        email: email,
                        firstName: givenName,
                        lastName: familyName,
                        avatar: imageUrl
                      };
                      globalActions.setSignUpInfo(formatedResponse);
                      if (localUser != null) {
                        setPreSocialValues(localUser);
                      }
                      setLocalUser(formatedResponse);
                    } else {
                      // setNotificationMessage({
                      //   text: 'Cancelled',
                      //   type: 'error'
                      // });
                      // setErrorMessage(true);
                    }

                    // signIn(email, '', true, formatedResponse).then();
                  }}
                  onFailure={() => {
                    // setNotificationMessage({
                    //   text: 'Please try again',
                    //   type: 'error'
                    // });
                  }}
                  cookiePolicy={'single_host_origin'}
                />

                <FacebookLogin
                  appId={FB_APP_ID} //FB_APP_ID
                  autoLoad={false}
                  fields="name,email,picture"
                  callback={(response) => {
                    if (response && response.email) {
                      const name = response.name.split(' ');
                      const formattedResponse = {
                        email: response.email,
                        firstName: name[0],
                        lastName: name.length > 1 ? name[1] : ' ',
                        avatar: `https://graph.facebook.com/${response.id}/picture?width=160&height=160`
                      };
                      globalActions.setSignUpInfo(formattedResponse);
                      if (localUser != null) {
                        setPreSocialValues(localUser);
                      }
                      setLocalUser(formattedResponse);
                    } else {
                      // setNotificationMessage({
                      //   text: 'Cancelled',
                      //   type: 'error'
                      // });
                      // setErrorMessage(true);
                    }
                  }}
                  render={(renderProps) => (
                    <SocialButton onClick={renderProps.onClick} socialName="facebook" disabled={locked} />
                  )}
                />
                {!isMobile && (
                  <AuthTint
                    styles={{
                      marginTop: 28
                    }}
                    isMobile={isMobile}
                    isSingIn={false}
                  />
                )}
              </Content>
            </SignForm>
          );
        }}
      </Formik>
    </Wrapper>
  );
};

export default SignUpComponent;
