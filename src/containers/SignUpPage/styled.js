import styled from 'styled-components';
import {blueColor, secondaryTextColor} from '../../constants/colors';
import media from '../../constants/media';

export const Wrapper = styled.div`
  padding-bottom: 64px;
  width: 100%;
  
  // @media (min-width: ${media.mobileMax}){
  //   & .custom-form-field {
  //     margin-bottom: 28px;
  //   }
  // }
`;

export const Content = styled.div`
  &&& {
    padding: 0 24px;
    display: flex;
    align-items: center;
    flex-direction: column;

    @media (min-width: ${media.mobileMax}) {
      padding: 0 22px;
    }
  }
`;

export const SignForm = styled.form`
  margin-top: 31px;
  @media (min-width: ${media.mobileMax}) {
    margin-top: 18px;
  }
`;

export const FullWidthFormField = styled.div`
  width: 100%;
`;

export const DoubleFormFields = styled.div`
  width: 100%;
  display: flex;

  & > div {
    width: 100%;
  }

  & > div:first-child {
    padding-right: 4.5px;
  }

  & > div:last-child {
    padding-left: 4.5px;
  }
`;

export const Agreements = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: ${secondaryTextColor}
  margin-top: 6px;
  
  .ant-checkbox {
    margin-right: 8px !important;
  }
  
  .ant-checkbox-inner {
    width: 16px !important;
    height: 16px !important;
    border-radius: 2px !important;
  }
  
  label {
    display: flex;
  }
`;

export const BlueText = styled.span`
  color: ${blueColor};
`;
