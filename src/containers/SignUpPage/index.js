import React from 'react';
import {useWindowSize} from '@reach/window-size';
import SignUpComponent from './SignUpComponent';

const SignUpPage = ({onClickClose}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return <SignUpComponent isMobile={isMobile} onClickClose={onClickClose} />;
};

export default SignUpPage;
