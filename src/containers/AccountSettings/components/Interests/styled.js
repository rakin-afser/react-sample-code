import styled from 'styled-components';

export const Add = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  width: 100px;
  height: 36px;
  border: 1px solid #c3c3c3;
  border-radius: 24px;
  background: transparent;
  font-family: SF Pro Display, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
`;

export const Select = styled.button`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  text-align: center;
  background: transparent;
  border: 1px solid #8f8f8f;
  border-radius: 20px;
  padding: 6px 20px;
  white-space: nowrap;
  margin: 0 15px 15px 0;
  &:last-child {
    margin-right: 0;
  }
`;

export const Selected = styled(Select)`
  color: #ed484f;
  border: 1px solid #ed484f;
`;

export const InputField = styled.input`
  width: 100%;
  height: 100%;
  background: transparent;
  border: none;
  outline: none;
  padding: 0 0 0 16px;
  font-family: Helvetica Neue, sans-serif;
  font-size: 14px;
  line-height: 140%;
  color: #000;

  &::placeholder {
    color: #999;
  }
`;
