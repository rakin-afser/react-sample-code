import React from 'react';
import Grid from 'components/Grid';
import AccountContent from 'components/AccountContent';
import {useQuery, useMutation} from '@apollo/client';
import {Selected, Select} from './styled';
import {GET_INTERESTS} from 'queries';
import {FOLLOW_INTEREST} from 'mutations';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';
import Error from 'components/Error';
import {userId} from 'util/heplers';

function Interests() {
  const {data: interests, loading, error} = useQuery(GET_INTERESTS);

  const [followInterest] = useMutation(FOLLOW_INTEREST);

  const onFollowInterest = (interestId) => {
    followInterest({
      variables: {interestId, userId},
      update(cache, {data}) {
        cache.modify({
          fields: {
            interests() {
              return {data};
            }
          }
        });
      }
    });
  };

  const InterestsSkeleton = () => {
    return (
      <SkeletonTheme>
        <Skeleton width={800} height={20} />
        <Skeleton width={800} height={20} />
        <Skeleton width={800} height={20} />
      </SkeletonTheme>
    );
  };

  const Content = () => {
    return interests?.interests?.nodes?.map((item, key) => {
      if (item?.isFollowed) {
        return (
          <Selected onClick={() => onFollowInterest(item?.databaseId)} key={key}>
            {item?.title} X
          </Selected>
        );
      }
      return (
        <Select onClick={() => onFollowInterest(item?.databaseId)} key={key}>
          + {item.title}
        </Select>
      );
    });
  };

  const renderContent = () => {
    if (error) return <Error />;

    if (loading) return <InterestsSkeleton />;

    return <Content />;
  };

  return (
    <AccountContent heading="Interests">
      <Grid column padding="24px 58px 24px 44px">
        <Grid wrap>{renderContent()}</Grid>
      </Grid>
    </AccountContent>
  );
}

export default Interests;
