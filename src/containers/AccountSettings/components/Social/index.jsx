import React from 'react';
import {
	InputField,
	Label,
    ConnectSocial,
    SocialImage,
} from './styled';
import Grid from 'components/Grid';
import AccountContent from "components/AccountContent";

import facebook from './img/facebook.svg';
import instagram from './img/instagram.png';

function Social() {
	return (
		<AccountContent heading='Social Medias'>
          <Grid column padding='20px 58px 24px 44px'>
            <div style={{margin:'0 0 15px'}}>
              <label style={{margin:'0 0 5px', display: 'block'}}>
                <SocialImage src={facebook} alt="Facebook"/>
                <Label>Facebook</Label>
              </label>
              <InputField value='@thosesunnydays' />
              <ConnectSocial type="button">Connect</ConnectSocial>
            </div>
            <div style={{margin:'0 0 15px'}}>
              <label style={{margin:'0 0 5px', display: 'block'}}>
                <SocialImage src={instagram} alt="Instagram"/>
                <Label>Instagram</Label>
              </label>
              <InputField value='@thosesunnydays' />
              <ConnectSocial type="button">Connect</ConnectSocial>
            </div>
          </Grid>
		</AccountContent>
	);
}

export default Social;