import styled from 'styled-components';

export const Label = styled.label``;

export const InputField = styled.input`
  width: 500px;
  height: 40px;
  background: transparent;
  border: 1px solid #a7a7a7;
  border-radius: 2px;
  padding-left: 16px;
  outline: none;
`;

export const ConnectSocial = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  min-width: 110px;
  height: 36px;
  margin: 0 0 0 13px;
  text-align: center;
  font-family: Helvetica, sans-serif;
  font-size: 14px;
  background: #c3c3c3;
  line-height: 140%;
  color: #fff;
  border: 1px solid #c3c3c3;
  border-radius: 24px;
  cursor: pointer;
  outline: none;
`;

export const SocialImage = styled.img`
  fill: #bec0c3;
  margin: 0 3px;
`;
