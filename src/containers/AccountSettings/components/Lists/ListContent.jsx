import React, {useState} from 'react';
import {
  Table,
  Item,
  Create,
  Title,
  Pic,
  PicGroup,
  Group,
  Details,
  Icon,
  Settings,
  Dot,
  Bold,
  EditList,
  Function,
  AdditionalInfo
} from './styled';
import pic1 from './img/pic1.png';
import pic2 from './img/pic2.png';
import photo1 from './img/photo1.png';
import photo2 from './img/photo2.png';
import photo3 from './img/photo3.png';
import eye from './img/eye.svg';
import settings from './img/settings.svg';

const titles = ['Fancy Trends 2020', 'Woman', 'Make Up', 'Twin Peaks', 'Personal', 'Ariana Grande', 'Lipsticks - MAC'];

function ListsContent() {
  const [editItemIndex, setEditItemIndex] = useState(null);
  return (
    <Table>
      {titles.map((title, i) => (
        <Item key={i} onClick={() => null}>
          <Group styles={{marginBottom: '8px'}}>
            <Pic src={photo1} />
            <Pic src={photo2} />
            <Pic src={photo3} />
          </Group>
          <Group styles={{borderTop: '1px solid #E4E4E4', paddingTop: '7px'}}>
            <Title>{title}</Title>
            <div>
              <Icon src={eye} />
              <Settings>
                <Dot />
              </Settings>
            </div>
          </Group>
          <AdditionalInfo>
            <Details>
              <Bold>701</Bold> Followers
            </Details>
            <Details purchased>
              <Bold>890</Bold> Views
            </Details>
          </AdditionalInfo>
          {typeof editItemIndex === 'number' ? (
            <EditList>
              <Function>
                <Icon settings src={settings} />
                Edit List
              </Function>
              <Function>
                <Icon settings src={settings} />
                Reorder List Sequence
              </Function>
              <Function>
                <Icon settings src={settings} />
                Reorder Cover Images
              </Function>
              <Function>
                <Icon settings src={settings} />
                Share / Invite
              </Function>
              <Function>
                <Icon settings src={settings} />
                Delete List
              </Function>
            </EditList>
          ) : null}
        </Item>
      ))}
    </Table>
  );
}

export default ListsContent;
