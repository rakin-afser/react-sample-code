import React from 'react';
import {
  Content,
  Nav,
  Tab,
  Wrap,
} from './styled';

import ShopsToFollow from '../ShopsToFollow';

const tabs = ['Preferences', 'Notifications', 'Addresses', 'Payout Details'];

function Lists({activeTab = 1, selectTab = (f) => f}) {
  return (
    <Wrap>
      <Content>
        <Nav>
          {tabs.map((tab, i) => (
            <Tab active={activeTab === i} key={i} onClick={() => selectTab(i)}>
              {tab}
            </Tab>
          ))}
        </Nav>
      </Content>
      <ShopsToFollow />
    </Wrap>
  );
}

export default Lists;
