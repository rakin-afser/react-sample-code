import React, {useState}from 'react';
import FadeOnMount from 'components/Transitions';
import Grid from 'components/Grid';
import {useFormik} from 'formik'
import * as Yup from "yup";
import {Container} from './styled';
import LocalPreferences from '../LocalPreferences';
import Password from '../Password';
import Social from '../Social';
import Interests from '../Interests';
import {CancelBtn, SaveBtn} from '../Notifications/styled';
import {useMutation} from '@apollo/client'
import {UpdatedUser} from 'mutations'
import {useUser} from 'hooks/reactiveVars';

function Preferences() {

  const [updatePreferences, {loading: updateUserLoading, error: updateUserError} ] = useMutation(UpdatedUser)

  const [user] = useUser()
  const [country,setCountry] = useState(user?.country)
  const [currency,setCurrency] = useState(user?.currency);
  const [timezone,setTimezone] = useState(user?.timezone);
  const [language,setLanguage] = useState(user?.language);

  const onSubmit=(values)=>{
    if (user) {
      let input = {
          id:user?.userId ,
          country:country,
          currency:currency,
          language:language,
          timezone:timezone.toString(),
          password:values?.newPassword
        };
      
      updatePreferences({
        variables: {
          input
        },
        update(cache, {data}) {
          cache.modify({
            fields: {
              user(existing) {
                const userData = data?.updateUser?.user
                return {...existing, userData};
              }
            }
          });
        }
      });
    }
  }

  const formik = useFormik({
    initialValues: {
      currentPassword: "",
      newPassword: "",
      confirmPassword: ""
    },
    validationSchema: Yup.object().shape({
      currentPassword: Yup.string().required("Old password is required"),
      newPassword: Yup.string().required("New password is required"),
      confirmPassword: Yup.string()
        .oneOf([Yup.ref("newPassword")], "Passwords do not match")
        .required("Password is required")
    }),
    onSubmit: (values) => {
     onSubmit(values)
    }
  });
  return (
  <>
  <form onSubmit={formik.handleSubmit}>
    <FadeOnMount>
      <Grid column width="100%">
          <Container>
            <LocalPreferences
                currency={currency} 
                countryName={country} 
                timezone={timezone}
                language={language}
                setCountry={setCountry}
                setCurrency={setCurrency}
                setTimezone={setTimezone}
                setLanguage={setLanguage}
            />
          </Container>
          <Container>
              <Password formik={formik}/>
          </Container>
          <Container>
            <Social />
          </Container>
          <Container>
            <Interests />
          </Container>
          <Grid aic margin="0 0 0 auto">
              <CancelBtn type="reset">Cancel</CancelBtn>
              <SaveBtn type="submit">Save</SaveBtn>
          </Grid>
        </Grid>
      </FadeOnMount>
    </form>
    </>
  );
}

export default Preferences;
