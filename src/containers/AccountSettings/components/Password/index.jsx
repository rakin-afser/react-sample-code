import React from 'react';
import {
	FullWidthFormField
} from './styled';
import Input from 'components/Input';
import Grid from "components/Grid";
import AccountContent from "components/AccountContent";

function getFields({formik}) {
	return(
	<>
		 <FullWidthFormField>
			<Input 
				label="Enter Old Password" 
				id="currentPassword"
				type='password'
				name="currentPassword"
				onChange={formik.handleChange}
				onBlur={formik.handleBlur}
				value={formik.values.currentPassword}
				showPassword={true}  
            />
				{formik.touched.currentPassword && formik.errors.currentPassword ? (
				<span style={{color:"red"}}>{formik.errors.currentPassword}</span>
				) : null}
		</FullWidthFormField>

		<FullWidthFormField>
			<Input 
				label="Enter New Password" 
				showPassword={true} passwordChars 
				id="newPass"
				type='password'
				name="newPassword"
				onChange={formik.handleChange}
				onBlur={formik.handleBlur}
				value={formik.values.newPassword}
				showPassword={true}
			  />
				{formik.touched.newPassword && formik.errors.newPassword ? (
				<span style={{color:"red"}}>{formik.errors.newPassword}</span>
				) : null}
		</FullWidthFormField>

		<FullWidthFormField>
			<Input 
				label="Re-Enter New  Password"
				id="confirmPass"
				type='password'
				name="confirmPass"
				onChange={formik.handleChange}
				onBlur={formik.handleBlur}
				value={formik.values.confirmPassword}
				showPassword={true} 
            />
               {formik.touched.confirmPassword && formik.errors.confirmPassword ? (
				<span style={{color:"red"}}>{formik.errors.confirmPassword}</span>
				) : null}
		</FullWidthFormField>
	</>
	)
}

function Password({formik}) {
	return (
		<AccountContent heading='Change Password'>
			<Grid column padding='24px 58px 24px 44px'>
				{ getFields({formik}) }
			</Grid>
		</AccountContent>
	);
}

export default Password;