import styled from 'styled-components';
import Check from './check.png';
import {blue} from 'constants/colors';
import Checkbox from 'components/Checkbox';

export const NotificationsLabel = styled.label`
  display: flex;
  align-items: center;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  padding-bottom: 16px;

  &::before {
    content: '';
    display: flex;
    align-items: center;
    justify-content: center;
    width: 20px;
    height: 20px;
    background: ${({active}) => (active ? `url('${Check}') no-repeat center #000` : 'transparent')};
    border-color: ${({active}) => (active ? `#000` : '#464646')};
    border-width: 1.5px;
    border-style: solid;
    box-sizing: border-box;
    border-radius: 4px;
    margin-right: 14px;
    color: #ffffff;
  }
`;

export const Title = styled.span`
  display: block;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 16px;
  line-height: 140%;
  color: #000000;
  padding-bottom: 16px;
`;

export const CancelBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 99px;
  height: 32px;
  background: transparent;
  border-radius: 24px;
  border: none;
  outline: none;
  cursor: pointer;
  font-family: Helvetica, sans-serif;
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #333;
  margin: 0 24px 0 auto;
  transition: ease 0.4s;

  &:hover {
    color: ${blue};
  }
`;

export const SaveBtn = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 99px;
  height: 32px;
  background: #ed484f;
  border-radius: 24px;
  border: none;
  outline: none;
  cursor: pointer;
  font-family: Helvetica, sans-serif;
  font-weight: 700;
  font-size: 14px;
  line-height: 140%;
  color: #ffff;
  transition: ease 0.4s;

  &:hover {
    background-color: #000;
  }
`;

export const Container = styled.div`
  width: 100%;
  margin: 0 0 16px;
  border: 1px solid #e4e4e4;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.1);
  height: auto;
  padding: 29px 26px 26px 44px;
`;
export const StyledCheckbox = styled(Checkbox)`
  &&& {
    display: flex;
    align-items: center;
    font-family: Helvetica, sans-serif;
    font-weight: 400;
    font-size: 14px;
    line-height: 140%;
    color: #000000;
    padding-bottom: 16px;
    .ant-checkbox {
      + span {
        transition: color 0.3s ease-in-out;
      }

      .ant-checkbox-inner {
        border-color: currentColor;
      }
    }

    .ant-checkbox-checked + span {
      font-weight: 500;
      color: #000;
    }

    & + & {
      margin-left: 0;
      margin-top: 16px;
    }
  }
`;
