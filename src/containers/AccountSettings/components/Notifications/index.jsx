import React,{useState} from 'react';
import Grid from 'components/Grid';
import FadeOnMount from 'components/Transitions';
import { Title, CancelBtn, SaveBtn, Container,StyledCheckbox} from './styled';
import {useMutation} from '@apollo/client'
import {UpdatedUser} from 'mutations'
import {useUser} from 'hooks/reactiveVars';

function Notifications() {
  const [updateNotification, {loading: updateUserLoading, error: updateUserError} ] = useMutation(UpdatedUser)

  const [user] = useUser()
  const[notiPublishPost,setNotiPublishPost]= useState(user?.noti_publish_posts)
  const[notiPublishGet,setNotiPublishGet]= useState(user?.noti_post_gets_like)
  const[noti_post_gets_commentPostGet,setNoti_post_gets_commentPostGet]= useState(user?.noti_post_gets_comment)
  const[noti_someone_reacts_to_Comment,setNoti_someone_reacts_to_Comment]= useState(user?.noti_someone_reacts_to_comment)

  const[ noti_followedList,setNoti_followedList]= useState(user?.noti_list_followed)
  const[noti_list_gets_a_likeGets,setNoti_list_gets_a_likeGets]= useState(user?.noti_list_gets_a_like)

  const[noti_received_messageProfile,setNoti_received_messageProfile]= useState(user?.noti_received_message)
  const[noti_send_messageProfile,setNoti_send_messageProfile]= useState(user?.noti_send_message)
  const[noti_follows_meProfile,setNoti_follows_meProfile]= useState(user?.noti_follows_me)
  const[noti_reward_coins_changeProfile,setNoti_reward_coins_changeProfile]= useState(user?.noti_reward_coins_change)
  const[noti_reward_earnsProfile,setNoti_reward_earnsProfile]= useState(user?.noti_reward_earns)

  const[noti_joins_by_my_linkFriends,setNoti_joins_by_my_linkFriends]= useState(user?.noti_joins_by_my_link)
  const[noti_upcoming_birthdays_of_my_Friends,setNoti_upcoming_birthdays_of_my_Friends]= useState(user?.noti_upcoming_birthdays_of_my_friends)

const onSubmit=()=>{
  if (user) {
    let input = {
        id:user?.userId ,
        noti_publish_posts:Boolean(notiPublishGet),
        noti_post_gets_like:Boolean(notiPublishGet),
        noti_post_gets_comment:Boolean(noti_post_gets_commentPostGet),
        noti_someone_reacts_to_comment:Boolean(noti_someone_reacts_to_Comment),

        noti_list_followed:Boolean(noti_followedList),
        noti_list_gets_a_like:Boolean(noti_list_gets_a_likeGets),

        noti_received_message:Boolean(noti_received_messageProfile),
        noti_send_message:Boolean(noti_send_messageProfile),
        noti_reward_coins_change:Boolean(noti_reward_coins_changeProfile),
        noti_follows_me:Boolean(noti_follows_meProfile),
        noti_reward_earns:Boolean(noti_reward_earnsProfile),

        noti_joins_by_my_link:Boolean(noti_joins_by_my_linkFriends),
        noti_upcoming_birthdays_of_my_friends:Boolean(noti_upcoming_birthdays_of_my_Friends)
      };
    
      updateNotification({
      variables: {
        input
      },
      update(cache, {data}) {
        cache.modify({
          fields: {
            user(existing) {
              const userData = data?.updateUser?.user
              console.log("userData",userData)
              return {...existing, userData};
            }
          }
        });
      }
    });
  }
}
  return (
    <FadeOnMount>
      <Grid column width="100%">
        <Container>
            <Title>Posts</Title>
            <StyledCheckbox onChange={(e)=>setNotiPublishPost(e.target.checked)} defaultChecked={notiPublishPost} value={notiPublishPost} name="notiPublishPost">I publish a post</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNotiPublishGet(e.target.checked)} defaultChecked={notiPublishGet} value={notiPublishGet}   name="notiPublishGet"> My post gets a like</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNoti_post_gets_commentPostGet(e.target.checked)} defaultChecked={noti_post_gets_commentPostGet} value={noti_post_gets_commentPostGet} name="noti_post_gets_commentPostGet">My post gets a comment</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNoti_someone_reacts_to_Comment(e.target.checked)}  value={noti_someone_reacts_to_Comment} defaultChecked={noti_someone_reacts_to_Comment} name="noti_someone_reacts_to_Comment">Someone reacts to my comment</StyledCheckbox>
        </Container>
        <Container>
            <Title>Lists</Title>
            <StyledCheckbox onChange={(e)=>setNoti_followedList(e.target.checked)}  value={noti_followedList} defaultChecked={noti_followedList} name="noti_followedList">My list is followed</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNoti_list_gets_a_likeGets(e.target.checked)}  value={noti_list_gets_a_likeGets} defaultChecked={noti_list_gets_a_likeGets} name="noti_list_gets_a_likeGets">My list gets a like</StyledCheckbox>
            {/* <StyledCheckbox>My post gets a comment</StyledCheckbox>
            <StyledCheckbox>Someone reacts to my comment</StyledCheckbox> */}
        </Container>
        <Container>
            <Title>Profile</Title>
            <StyledCheckbox onChange={(e)=>setNoti_received_messageProfile(e.target.checked)}  value={noti_received_messageProfile} defaultChecked={noti_received_messageProfile} name="noti_received_messageProfile">Someone sends me a message</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNoti_send_messageProfile(e.target.checked)}  value={noti_send_messageProfile} defaultChecked={noti_send_messageProfile} name="noti_send_messageProfile">I send a message</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNoti_follows_meProfile(e.target.checked)}  value={noti_follows_meProfile} defaultChecked={noti_follows_meProfile} name="noti_follows_meProfile">Someone follows me</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNoti_reward_coins_changeProfile(e.target.checked)}  value={noti_reward_coins_changeProfile} defaultChecked={noti_reward_coins_changeProfile} name="noti_reward_coins_changeProfile">My reward coins change</StyledCheckbox>
            <StyledCheckbox onChange={(e)=>setNoti_reward_earnsProfile(e.target.checked)}  value={noti_reward_earnsProfile} defaultChecked={noti_reward_earnsProfile} name="noti_reward_earnsProfile">Reward coins earned</StyledCheckbox>
        </Container>
        <Container>
            <Title>Friends</Title>
            <StyledCheckbox onChange={(e)=>setNoti_joins_by_my_linkFriends(e.target.checked)}  value={noti_joins_by_my_linkFriends} defaultChecked={noti_joins_by_my_linkFriends} name="noti_joins_by_my_linkFriends">Someone joins testSample by my invite</StyledCheckbox>
            {/* <StyledCheckbox>My friend from connected social network joins testSample</StyledCheckbox> */}
            <StyledCheckbox onChange={(e)=>setNoti_upcoming_birthdays_of_my_Friends(e.target.checked)}  value={noti_upcoming_birthdays_of_my_Friends} defaultChecked={noti_upcoming_birthdays_of_my_Friends} name="noti_upcoming_birthdays_of_my_Friends">Upcoming birthdays of my friend</StyledCheckbox>
        </Container>
        <Grid aic margin="0 0 0 auto">
            <CancelBtn>Cancel</CancelBtn>
            <SaveBtn onClick={onSubmit}>Save</SaveBtn>
        </Grid>
      </Grid>
    </FadeOnMount>
  );
}

export default Notifications;
