import React from 'react';
import FadeOnMount from 'components/Transitions';
import Grid from 'components/Grid';
import {Container, AddressAddButton, AddressAdd, Title} from './styled';
import {CancelBtn, SaveBtn} from '../Notifications/styled';
import BankAccount from '../BankAccount';
import PayPal from '../PayPal';

function PayoutDetails() {
  return (
    <FadeOnMount>
      <Grid column width="100%">
        <Container
          style={{
            border: '1px solid #e4e4e4',
            boxShadow: '0 0 4px rgba(0, 0, 0, 0.1)',
            paddingBottom: '12px',
            marginBottom: 24
          }}
        >
          <BankAccount />
          <PayPal />
        </Container>
        <Grid aic margin="0 0 0 auto">
          <CancelBtn>Cancel</CancelBtn>
          <SaveBtn>Save</SaveBtn>
        </Grid>
      </Grid>
    </FadeOnMount>
  );
}

export default PayoutDetails;
