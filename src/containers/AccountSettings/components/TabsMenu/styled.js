import styled from 'styled-components';

export const TabList = styled.ul`
  padding-left: 40px;
  display: flex;
  box-shadow: rgba(0, 0, 0, 0.1) 0 0 5px;
  background: rgb(255, 255, 255);
  border-radius: 4px;
  width: 100%;
`;

export const TabListItem = styled.li`
  margin-right: 50px;
  cursor: pointer;
  font-size: 16px;
  text-align: center;
  line-height: 20px;
  color: rgb(122, 122, 122);
  box-sizing: border-box;
  outline: none;
  transition: ease 0.3s;
`;

export const TabListLink = styled.a`
  font-size: 16px;
  color: ${({active}) => (active ? '#000' : '#656565')};
  font-weight: ${({active}) => (active ? '400' : '400')};
  display: inline-block;
  padding: 15px 0;
  text-align: center;
  ${({active}) => (active ? 'box-shadow: inset 0 -2px 0 0 #ED484F;' : 'box-shadow: inset 0 -2px 0 0 transparent')};
  transition: ease 0.3s;

  &:hover {
    color: #000;
  }
`;
