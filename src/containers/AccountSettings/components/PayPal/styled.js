import styled from 'styled-components';
import {Select} from 'antd';
import {primaryColor} from 'constants/colors';

export const Field = styled(Select)`
  &&& {
    & .ant-select-selection--single {
      width: 372px !important;
      height: 40px;
    }

    & .ant-select-selection__rendered {
      line-height: 40px;
      font-family: Helvetica Neue, sans-serif;
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      letter-spacing: -0.016em;
      color: #000;
    }
  }
`;

export const FormGroup = styled.div`
  display: block;
  margin: 0 0 24px;
`;

export const LocalPreferencesArea = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const InputField = styled.input`
  width: 400px;
  height: 40px;
  background: transparent;
  border: 1px solid #a7a7a7;
  border-radius: 2px;
  padding-left: 16px;
  outline: none;
`;

export const ConnectSocial = styled.button`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  min-width: 110px;
  height: 36px;
  margin: 0 0 0 13px;
  text-align: center;
  font-family: Helvetica, sans-serif;
  font-size: 14px;
  background: #c3c3c3;
  line-height: 140%;
  color: #fff;
  border: 1px solid #c3c3c3;
  border-radius: 24px;
  cursor: pointer;
  outline: none;
  transition: ease 0.4s;

  &:hover {
    background-color: ${primaryColor};
    color: #fff;
  }
`;

export const Label = styled.label``;
