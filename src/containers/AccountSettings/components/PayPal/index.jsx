import React, {useState} from 'react';
import AccountContent from 'components/AccountContent';
import {Label, FormGroup, LocalPreferencesArea, ConnectSocial, InputField} from './styled';
import Grid from '../../../../components/Grid';

const local = [
  {
    label: 'Account Holder Name',
    value: 'Account Holder Name'
  },
  {
    label: 'Bank Name',
    value: 'Bank Name'
  },
  {
    label: 'Account Number',
    value: 'Account Number'
  },
  {
    label: 'SWIFT',
    value: 'SWIFT'
  },
  {
    label: 'IBAN',
    value: 'IBAN'
  }
];

function PayPal() {
  const {valid, setValid} = useState(false);

  function validateEmail(mail) {
    return /^[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~](\.?[-!#$%&'*+\/0-9=?A-Z^_a-z`{|}~])*@[a-zA-Z0-9](-*\.?[a-zA-Z0-9])*\.[a-zA-Z](-?[a-zA-Z0-9])+$/.test(
      mail
    );
  }

  const onChange = (e) => {
    if (validateEmail(e.target.value)) {
      setValid(false);
    } else {
      setValid(true);
    }
  };

  return (
    <AccountContent style={{border: 'none', borderTop: '1px solid #e4e4e4', boxShadow: 'none'}} heading="PayPal">
      <Grid column padding="24px 58px 24px 44px">
        <LocalPreferencesArea>
          <div style={{margin: '0 0 15px'}}>
            <label style={{margin: '0 0 5px', display: 'block'}}>
              <Label>Enter email address</Label>
            </label>
            <InputField onChange={onChange} />
            <ConnectSocial disabled={!valid} type="button">
              Add
            </ConnectSocial>
          </div>
        </LocalPreferencesArea>
      </Grid>
    </AccountContent>
  );
}

export default PayPal;
