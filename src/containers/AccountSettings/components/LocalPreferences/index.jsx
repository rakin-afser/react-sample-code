import React from 'react';
import {Select} from 'antd';

import AccountContent from 'components/AccountContent';
import {Field, Label, FormGroup, LocalPreferencesArea} from './styled';
import Grid from 'components/Grid';
import {country, currencies, languages, timeZones} from 'constants/staticData';
const {Option} = Select;

function getFields({
  countryName,
  timezone,
  currency,
  language,
  setCountry,
  setCurrency,
  setTimezone,
  setLanguage
}) 
 {
  return (
    <>
    <FormGroup>
      <Label>Country</Label>
      <div>
      <Field 
        defaultValue={countryName}
        key="countryName"
        id="countryName"
        name="countryName"   
        onChange={(value)=> setCountry(value)}   
        value={countryName}
        style={{width: 360}} 
       > 
        {Object.keys(country).map((key) => (
          <Option value={key} key={key}>
              {key}
          </Option>
      ))}
      
      </Field>
      </div>
      </FormGroup>
      <FormGroup>
      <Label>Timezone</Label>
      <div>
      <Field 
      defaultValue={timezone} 
        key="timezone"
        id="timezone"
        name="timezone"   
        onChange={(value)=> setTimezone(value)}   
        value={timezone}
        style={{width: 360}} 
       > 
       {timeZones.map((timezone) => (
        <Option value={timezone.value} key={timezone.value}>
          {timezone.text}
        </Option>
      ))}
      
      </Field>
     
      </div>
      </FormGroup>
      <FormGroup>
      <Label>Currency</Label>
      <div>
      <Field
       defaultValue={currency} 
        key="currency"
        id="currency"
        name="currency"   
        onChange={(value)=> setCurrency(value)}   
        value={currency}
        style={{width: 360}} 
       > 
      {currencies.map((currency) => (
        <Option value={currency.cc} key={currency.cc}>
          {currency.cc} ({currency.name})
        </Option>
      ))}
      
      </Field>
      </div>
      </FormGroup>
      <FormGroup>
      <Label>Language</Label>
      <div>
      <Field
      defaultValue={language} 
        key="language"
        id="language"
        name="language"   
        onChange={(value)=> setLanguage(value)}  
        value={language}
        style={{width: 360}} 
       > 
         {languages.map((language) => (
      <Option value={language.code} key={language.code}>
        {language.name}
      </Option>
       ))}
      </Field>
      </div>
      </FormGroup>
  </>
  )
}

function LocalPreferences({
  countryName,
  timezone,
  currency,
  language,
  setCountry,
  setCurrency,
  setTimezone,
  setLanguage
}) {
  return (
    <AccountContent heading="Local Preferences">
      <Grid column padding="24px 58px 24px 44px">
        <LocalPreferencesArea>{getFields({
                    countryName,
                    timezone,
                    currency,
                    language,
                    setCountry,
                    setCurrency,
                    setTimezone,
                    setLanguage
                   })}
   </LocalPreferencesArea>
      </Grid>
    </AccountContent>
  );
}

export default LocalPreferences;

