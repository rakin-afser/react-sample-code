import styled from 'styled-components/macro';
import {mainFont, secondaryFont} from 'constants/fonts';
import {blue, primaryColor} from 'constants/colors';

export const Addresses = styled.div`
  height: 832px;
  background: #ffffff;
  border: 1px solid #e4e4e4;
  box-sizing: border-box;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  margin-left: 32px;
`;

export const AddressesItem = styled.div`
  position: relative;
  width: 100%;
  background: #fff;
  border-top: 1px solid #e4e4e4;
  padding: 16px;
  padding-bottom: 0;
`;

export const Title = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: #000000;
`;

export const AddressTitle = styled.span`
  display: block;
  font-family: ${mainFont};
  font-weight: 500;
  font-size: 18px;
  color: #000;
  width: 150px;
  letter-spacing: -0.024em;
`;

export const AddressText = styled.span`
  display: block;
  font-family: ${mainFont};
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;
  color: #656565;
  width: 250px;
`;

export const AddressAdd = styled.span`
  font-family: ${mainFont};
  font-size: 12px;
  color: inherit;
  padding: 0 0 0 10px;
`;

export const AddressAddButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 32px;
  border: 1px solid #ed484f;
  border-radius: 24px;
  padding: 12px 20px;
  transition: ease 0.4s;
  cursor: pointer;
  color: #ed484f;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    color: #fff;
    background-color: ${primaryColor};

    svg {
      path {
        fill: #fff;
      }
    }
  }
`;

export const Primary = styled.span`
  display: block;
  font-family: ${secondaryFont};
  font-weight: 500;
  font-size: 14px;
  line-height: 120%;
  color: #b9b9b9;
  letter-spacing: 0.06em;
`;

export const Settings = styled.div`
  margin-top: auto;
  margin-left: auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 68px;
`;

export const RoundButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 28px;
  height: 28px;
  background-color: #e4e4e4;
  border-radius: 50%;
  transition: ease 0.4s;

  i {
    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    background-color: ${primaryColor};

    i {
      svg {
        path {
          fill: #fff;
        }
      }
    }
  }
`;

export const MakePrimaryBtn = styled.button`
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #000000;
  transition: ease 0.4s;

  &:hover {
    color: ${blue};
  }
`;

export const Right = styled.div`
  display: flex;
  flex-direction: column;
`;

export const PrimaryBtnWrapper = styled.div`
  display: flex;
  align-items: center;

  i {
    svg {
      margin-right: 5px;

      path {
        fill: #b9b9b9;
      }
    }
  }
`;
