import React, {useState} from 'react';
import Grid from 'components/Grid';
import {useQuery, useMutation, gql} from '@apollo/client';
import Icon from 'components/Icon';
import FadeOnMount from 'components/Transitions';
import AccountContent from 'components/AccountContent';
import ActionAlert from 'components/Modals/ActionAlert';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';
import {
  AddressAdd,
  AddressesItem,
  AddressAddButton,
  AddressText,
  Settings,
  Primary,
  Title,
  AddressTitle,
  RoundButton,
  MakePrimaryBtn,
  Right,
  PrimaryBtnWrapper
} from './styled';
import {ADDRESSES} from 'queries';
import {REMOVE_SHIPPING_ADDRESS} from 'mutations';
import {useUser} from 'hooks/reactiveVars';
import {ShippingAddressFragment} from 'fragments';
import AddNewAddress from 'components/AddNewAddress';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';

/** imported from inner component it should be moved to components directory */
import EditLocation from 'containers/GuestCheckoutPage/components/EditLocation';
import exampleData from 'containers/GuestCheckoutPage/assets/data/exampleData';

const SET_PRIMARY_ADDRESS = gql`
  mutation SetPrimaryAddress($id: ID, $clientMutationId: String) {
    setShippingChosenId(input: {id: $id, clientMutationId: $clientMutationId}) {
      clientMutationId
      shippingAddressesSuccess
      shippingChosenId
    }
  }
`;

const Addresses = () => {
  const {data, loading} = useQuery(ADDRESSES);
  const [removeAddress] = useMutation(REMOVE_SHIPPING_ADDRESS);
  const [setPrimaryAddress] = useMutation(SET_PRIMARY_ADDRESS);
  const [user] = useUser();
  const [editLocation, setEditLocation] = useState(null);
  const [addNewAddress, setAddNewAddress] = useState(false);
  const [confirmDelete, setConfirmDelete] = useState(false);
  const [addressToDeleteId, setAddressToDeleteId] = useState(null);
  const [selectedAddress, setSelectedAddress] = useState();
  const [location, setLocation] = useState({
    country: '',
    mobileNumber: '',
    district: '',
    street: '',
    addressType: '',
    houseBuildingNo: '',
    blockNo: '',
    roadNo: '',
    floorNo: '',
    aptNo: '',
    addressNickname: '',
    location: '',
    additionalDirections: ''
  });

  const {country} = exampleData;
  const shipping = data?.testSampleAddresses?.shipping;

  const shippingAddressChanged = (address) => {
    setSelectedAddress(address);
  };

  const onDeleteAddress = () => {
    if (user) {
      removeAddress({
        variables: {input: {addrId: addressToDeleteId}},
        update(cache, {data}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = data?.removeShippingAddress?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                return {...existing, shipping};
              }
            }
          });
        }
      });
    }
  };

  const onSetPrimaryAddress = (id) => {
    setPrimaryAddress({
      variables: {id, clientMutationId: id},
      update(cache, {data}) {
        cache.modify({
          fields: {
            testSampleAddresses() {
              return data;
            }
          }
        });
      }
    });
  };

  const onDeleteAddressModal = (id) => {
    setAddressToDeleteId(id);
    setConfirmDelete(true);
  };

  const onConfirmDelete = () => {
    onDeleteAddress();
    setConfirmDelete(false);
    setAddressToDeleteId(null);
  };

  const [selectedEditAddress] = data?.testSampleAddresses?.shipping.filter((addr) => addr.id === editLocation) || [];

  const renderContent = () => {
    if (loading) {
      return (
        <SkeletonTheme style={{marginTop: 30}}>
          <Skeleton style={{marginBottom: 30, marginLeft: 20}} width={830} height={100} />
          <Skeleton style={{marginBottom: 30, marginLeft: 20}} width={830} height={100} />
          <Skeleton style={{marginBottom: 30, marginLeft: 20}} width={830} height={100} />
        </SkeletonTheme>
      );
    }

    return shipping?.map((item, i) => (
      <Grid sb wrap padding="24px" key={i}>
        <AddressesItem>
          <Grid sb padding="10px 0 18px" customStyles={{alignItems: 'stretch'}}>
            <Grid>
              <Grid aic margin="0 25px 0 35px">
                <AddressTitle>{item?.type}</AddressTitle>
              </Grid>
              <AddressText main>
                {item?.firstName} {item?.lastName}
                <br />
                {item?.address1}, {item?.build} <br />
                {item?.road && `Road ${item?.road}`}, {item?.block && `Block ${item?.block}`} <br />
                {item?.district} <br />
                {item?.city} <br />
                {getCountryFromCode(item?.country)} <br />
                {country[getCountryFromCode(item?.country)].dialCode} {item?.phone} <br />
                floor: {item?.floor} <br />
                apt: {item?.apartment} <br />
              </AddressText>
            </Grid>
            <Right jcsb>
              <Grid aic>
                {item?.primary ? (
                  <PrimaryBtnWrapper>
                    <Icon type="check" />
                    <Primary>Primary</Primary>
                  </PrimaryBtnWrapper>
                ) : (
                  <MakePrimaryBtn onClick={() => onSetPrimaryAddress(item.id)}>Make as Primary</MakePrimaryBtn>
                )}
              </Grid>
              <Settings>
                <RoundButton
                  onClick={() => {
                    setEditLocation(item.id);
                  }}
                >
                  <Icon type="edit" />
                </RoundButton>
                <RoundButton onClick={() => onDeleteAddressModal(item.id)}>
                  <Icon type="trash" />
                </RoundButton>
              </Settings>
            </Right>
          </Grid>
        </AddressesItem>
      </Grid>
    ));
  };

  return (
    <>
      <FadeOnMount>
        <AccountContent>
          <Grid sb wrap padding="0 24px 0 24px">
            <div
              style={{
                marginTop: '-28px',
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '100%'
              }}
            >
              <Title>Shipping Addresses</Title>
              <AddressAddButton
                onClick={() => {
                  setAddNewAddress(true);
                }}
              >
                <Icon type="plus" width={12} height={12} />
                <AddressAdd>Add New Address</AddressAdd>
              </AddressAddButton>
            </div>
          </Grid>
          {renderContent()}
        </AccountContent>
      </FadeOnMount>
      <EditLocation
        shippingAddressChanged={shippingAddressChanged}
        selectedEditAddress={selectedEditAddress}
        visible={editLocation}
        close={() => setEditLocation(null)}
        location={location}
        setLocation={setLocation}
      />
      <AddNewAddress
        shippingAddressChanged={shippingAddressChanged}
        visible={addNewAddress}
        close={() => setAddNewAddress(false)}
        location={location}
        setLocation={setLocation}
      />
      <ActionAlert
        content="Are you sure you want to delete this address?"
        headerTitle="Delete address"
        okBtnName="Yes, delete it"
        onClose={() => setConfirmDelete(false)}
        open={confirmDelete}
        onSubmit={onConfirmDelete}
        cancelBtnName="No, I changed my mind"
      />
    </>
  );
};

export default Addresses;
