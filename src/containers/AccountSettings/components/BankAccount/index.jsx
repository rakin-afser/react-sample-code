import React from 'react';
import AccountContent from 'components/AccountContent';
import Icon from 'components/Icon';
import Grid from '../../../../components/Grid';
import {Label, FormGroup, LocalPreferencesArea, InputFeild, AddressAddButton, AddressAdd, Title} from './styled';

const local = [
  {
    label: 'Account Holder Name',
    value: 'Account Holder Name'
  },
  {
    label: 'Bank Name',
    value: 'Bank Name'
  },
  {
    label: 'Account Number',
    value: 'Account Number'
  },
  {
    label: 'SWIFT',
    value: 'SWIFT'
  },
  {
    label: 'IBAN',
    value: 'IBAN'
  }
];

function getFields() {
  return local.map((item, key) => (
    <FormGroup key={key}>
      <Label>{item.label}</Label>
      <div>
        <InputFeild type="text" placeholder={local.value} />
      </div>
    </FormGroup>
  ));
}

function BankAccount() {
  return (
    <AccountContent style={{boxShadow: 'none', border: 'none'}}>
      <div
        style={{
          marginTop: '-35px',
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
          padding: '0 31px'
        }}
      >
        <Title>Bank Account</Title>
        <AddressAddButton>
          <Icon type="plus" width={12} height={12} />
          <AddressAdd>Add a New Method</AddressAdd>
        </AddressAddButton>
      </div>
      <Grid column padding="24px 58px 24px 44px">
        <LocalPreferencesArea>{getFields()}</LocalPreferencesArea>
      </Grid>
    </AccountContent>
  );
}

export default BankAccount;
