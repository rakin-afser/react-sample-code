import styled from 'styled-components/macro';
import {Select, Input} from 'antd';
import {primaryColor} from 'constants/colors';
import {mainFont} from 'constants/fonts';

export const Field = styled(Select)`
  &&& {
    & .ant-select-selection--single {
      width: 372px !important;
      height: 40px;
    }

    & .ant-select-selection__rendered {
      line-height: 40px;
      font-family: Helvetica Neue, sans-serif;
      font-style: normal;
      font-weight: normal;
      font-size: 16px;
      letter-spacing: -0.016em;
      color: #000;
    }
  }
`;

export const FormGroup = styled.div`
  display: block;
  margin: 0 0 31px;
`;

export const LocalPreferencesArea = styled.div`
  width: 100%;
  max-width: 400px;
`;

export const InputFeild = styled(Input)`
  &&& {
    & .ant-input {
      background: #fff;
      border: 1px solid #a7a7a7;
      box-sizing: border-box;
      border-radius: 2px;
      width: 300px;
    }
  }
`;

export const Label = styled.label`
  color: #464646;
`;

export const AddressAddButton = styled.button`
  display: flex;
  align-items: center;
  justify-content: center;
  border: 1px solid #ed484f;
  border-radius: 24px;
  padding: 0 21px;
  height: 34px;
  transition: ease 0.4s;
  cursor: pointer;
  color: #ed484f;

  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    background-color: ${primaryColor};
    color: #fff;

    svg {
      path {
        fill: #fff;
      }
    }
  }
`;

export const Title = styled.h3`
  font-family: ${mainFont};
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 132%;
  letter-spacing: -0.024em;
  color: #000000;
`;

export const AddressAdd = styled.span`
  font-family: ${mainFont};
  font-size: 12px;
  color: inherit;
  padding: 0 0 0 10px;
`;
