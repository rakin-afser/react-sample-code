import {useWindowSize} from '@reach/window-size';
import Layout from 'containers/Layout';
import React from 'react';
import {useHistory, useParams} from 'react-router';
import MyProfileDesktop from './MyProfileDesktop';
import AccountSettingsMobile from './mobile';

const withTitle = {
  'account-settings': 'Account Settings',
  preferences: 'Preferences',
  notifications: 'Notifications',
  addresses: 'Addresses',
  payout: 'Payout Details',
  connected: 'Connected Accounts'
};
const withBack = ['account-settings', 'preferences', 'notifications', 'addresses', 'payout', 'connected'];

const AccountSettings = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const {location} = useHistory();
  const page = useParams()?.page || location?.pathname?.split('/')?.pop();

  return isMobile ? (
    <Layout
      title={withTitle[page]}
      showBasketButton={!withBack.includes(page)}
      showSearchButton={!withBack.includes(page)}
      showBurgerButton={false}
      showBackButton={withBack.includes(page)}
      hideFooter
    >
      <AccountSettingsMobile />
    </Layout>
  ) : (
    <MyProfileDesktop />
  );
};

export default AccountSettings;
