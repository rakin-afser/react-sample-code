import React, {useState} from 'react';
import Grid from 'components/Grid';
import FadeOnMount from 'components/Transitions';
import Notifications from './components/Notifications';
import {TabList, TabListItem, TabListLink} from './components/TabsMenu/styled';
import Addresses from './components/Addresses';
import Preferences from './components/Preferences';
import PayoutDetails from './components/PayoutDetails';

const profileMenuLinks = ['My Profile', 'My Orders', 'My Rewards', 'Messages', 'Notifications', 'Settings'];
const links = ['Preferences', 'Notifications', 'Addresses', 'Payout Details'];

function getLinks(activeLink, setActiveLink) {
  return links.map((item) => (
    <TabListItem key={item}>
      <TabListLink name={item} onClick={(ev) => changeContent(ev, setActiveLink)} active={item === activeLink}>
        {item}
      </TabListLink>
    </TabListItem>
  ));
}

function changeContent(ev, setActiveLink) {
  const activeLink = ev.target.getAttribute('name');
  setActiveLink(activeLink);
}

const MyProfileDesktop = () => {
  const [menuItemIndex, setMenuItemIndex] = useState(0);
  const [activeLink, setActiveLink] = useState('Preferences');

  return (
    <FadeOnMount style={{width: '100%'}}>
      <Grid width="100%">
        <Grid column padding="0 0 0 20px" width="100%">
          <TabList>{getLinks(activeLink, setActiveLink)}</TabList>
          {activeLink === 'Preferences' ? <Preferences /> : null}
          {activeLink === 'Notifications' ? <Notifications /> : null}
          {activeLink === 'Addresses' ? <Addresses /> : null}
          {activeLink === 'Payout Details' ? <PayoutDetails /> : null}
        </Grid>
      </Grid>
    </FadeOnMount>
  );
};

export default MyProfileDesktop;
