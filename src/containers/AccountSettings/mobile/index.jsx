import React from 'react';
import {useHistory, useParams} from 'react-router';
import Default from './containers/Default';
import Preferences from './containers/Preferences';
import Notifications from './containers/Notifications';
import Addresses from './containers/Addresses';
import Payout from './containers/Payout';
import Connected from './containers/Connected';

const AccountSettingsMobile = () => {
  const {location} = useHistory();
  const params = useParams();

  const page = params?.page || location?.pathname?.split('/')?.pop();

  function getMobilePage() {
    switch (page) {
      case 'preferences':
        return <Preferences />;
      case 'notifications':
        return <Notifications />;
      case 'addresses':
        return <Addresses />;
      case 'payout':
        return <Payout />;
      case 'connected':
        return <Connected />;
      case 'account-settings':
      default:
        return <Default />;
    }
  }

  return getMobilePage();
};

export default AccountSettingsMobile;
