import React, {useState} from 'react';
import {SocialItem, Connect, DisConnect, SocialTitle} from './styled';
import Icon from 'components/Icon';
import {SwitchTransition, CSSTransition} from 'react-transition-group';

const Social = (props) => {
  const {children, icon} = props;

  const [connected, setConnected] = useState(false);

  return (
    <SocialItem>
      <Icon width="20" height="20" type={icon} />
      <SocialTitle>{children}</SocialTitle>
      <SwitchTransition>
        <CSSTransition
          key={connected}
          addEndListener={(node, done) => {
            node.addEventListener('transitionend', done, false);
          }}
          classNames="switch-fade"
        >
          {connected ? (
            <DisConnect onClick={() => setConnected(false)}>Disconnect</DisConnect>
          ) : (
            <Connect onClick={() => setConnected(true)}>Connect</Connect>
          )}
        </CSSTransition>
      </SwitchTransition>
    </SocialItem>
  );
};

export default Social;
