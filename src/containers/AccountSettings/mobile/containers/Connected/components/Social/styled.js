import {Button} from 'antd';
import styled from 'styled-components';

export const SocialItem = styled.li`
  padding: 24px 16px;
  border-bottom: 1px solid #efefef;
  display: flex;
  align-items: center;

  i {
    margin-right: 17px;
  }
`;

export const SocialTitle = styled.h4`
  font-size: 18px;
  margin-bottom: 0;
  line-height: 1;
`;

export const StyledBtn = styled(Button)`
  &&& {
    height: 28px;
    padding: 0 18px;
    border-radius: 30px;
    margin-left: auto;
  }

  &.switch-fade-enter {
    opacity: 0;
  }
  &.switch-fade-enter-active {
    opacity: 1;
  }
  &.switch-fade-exit {
    opacity: 1;
  }
  &.switch-fade-exit-active {
    opacity: 0;
  }
  &.switch-fade-enter-active,
  &.switch-fade-exit-active {
    transition: opacity 200ms ease-in-out;
  }
`;

export const Connect = styled(StyledBtn)`
  &&& {
    color: #ed484f;
    border-color: #ed484f;
  }
`;

export const DisConnect = styled(StyledBtn)`
  &&& {
    color: #666;
    border-color: transparent;
    font-weight: 500;
  }
`;
