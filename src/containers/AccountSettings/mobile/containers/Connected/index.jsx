import {useWindowSize} from '@reach/window-size';
import SavePanel from 'components/SavePanel';
import {mobileHeaderHeight} from 'constants/mobileSizes';
import React, {useEffect} from 'react';
import {Container, Socials} from './styled';
import Social from './components/Social';
import useGlobal from 'store';

const Connected = () => {
  const {height} = useWindowSize();

  const [, globalActions] = useGlobal();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/account-settings');
  }, []);

  const socials = [
    {title: 'Instagram', icon: 'instagramGrad'},
    {title: 'Facebook', icon: 'facebook'}
  ];

  return (
    <Container style={{height: height - parseInt(mobileHeaderHeight)}}>
      <Socials>
        {socials.map((item) => (
          <Social icon={item.icon} key={item.title}>
            {item.title}
          </Social>
        ))}
      </Socials>
      <SavePanel mt="auto" />
    </Container>
  );
};

export default Connected;
