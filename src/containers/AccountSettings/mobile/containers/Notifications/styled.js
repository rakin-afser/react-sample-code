import styled from 'styled-components';
import Checkbox from 'components/Checkbox';

export const Title = styled.h4`
  font-size: 18px;
  font-weight: 500;
  margin-bottom: 18px;
`;

export const StyledCheckbox = styled(Checkbox)`
  &&& {
    color: inherit;
    align-items: flex-start;

    .ant-checkbox {
      + span {
        transition: color 0.3s ease-in-out;
      }

      .ant-checkbox-inner {
        border-color: currentColor;
      }
    }

    .ant-checkbox-checked + span {
      font-weight: 500;
      color: #000;
    }

    & + & {
      margin-left: 0;
      margin-top: 16px;
    }
  }
`;

export const Wrap = styled.div`
  padding-top: ${({pt}) => `${pt ? pt : 16}px`};
  padding-bottom: 18px;
  padding-left: 16px;
  padding-right: 16px;
`;

export const Hr = styled.hr`
  border: 4px solid #fafafa;
  margin: 0;
`;
