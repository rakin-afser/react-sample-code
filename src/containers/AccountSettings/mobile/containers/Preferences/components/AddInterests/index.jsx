import BackPage from 'components/Modals/mobile/BackPage';
import SavePanel from 'components/SavePanel';
import Scrollbars from 'react-scrollbars-custom';
import React, {useState} from 'react';
import {Wrap, List, TagsWrap} from './styled';
import RoundTags from 'components/RoundTags';
import Interest from './components/Interest';
import {useWindowSize} from '@reach/window-size';

const AddInterests = (props) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [quantity, setQuantity] = useState(0);

  return (
    <BackPage title="Add Interests" {...props}>
      <TagsWrap mx={-16}>
        <RoundTags
          onFollowInterest={props.onFollowInterest}
          setQuantity={setQuantity}
          tags={props.interests.filter((el) => el.isFollowed)}
        />
      </TagsWrap>
      <Scrollbars
        clientWidth={4}
        noDefaultStyles={false}
        noScroll={false}
        style={{height: isMobile ? 'calc(100vh - 192px)' : '470px', width: '100%'}}
        thumbYProps={{className: 'thumbY'}}
      >
        <List quantity={quantity}>
          {props.interests.map((item) => (
            <Interest onFollowInterest={props.onFollowInterest} {...item} key={item.id} />
          ))}
        </List>
      </Scrollbars>

      <Wrap mx={-16}>
        <SavePanel onSave={() => props.setActive(false)} onCancel={() => props.setActive(false)} />
      </Wrap>
    </BackPage>
  );
};

export default AddInterests;
