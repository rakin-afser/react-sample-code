import styled from 'styled-components/macro';
import media from 'constants/media';

export const Wrap = styled.div`
  margin: ${({my}) => (my ? my : 0)}px ${({mx}) => (mx ? mx : 0)}px;
`;

export const TagsWrap = styled.div`
  margin: 0 -16px;

  li {
    padding-top: 16px;
  }
`;

export const List = styled.ul`
  height: calc(100vh - 100px);
  padding-top: 12px;
  padding-bottom: 12px;
  overflow: hidden;
  transition: max-height 200ms ease-in-out;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fit, ${({width}) => width || '135px'});
  grid-gap: ${({gap}) => gap || '7px 48px'};
  align-self: center;
  justify-content: center;

  @media (max-width: ${media.mobileMax}) {
    padding: 0 8px 86px;
    grid-template-columns: repeat(auto-fit, 80px);
  }
`;

export const Footer = styled.div``;
