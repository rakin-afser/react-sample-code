import {v1} from 'uuid';
import img1 from './img/1.jpg';
import img2 from './img/2.jpg';
import img3 from './img/3.jpg';
import img4 from './img/4.jpg';

export const exampleData = [
  {
    id: v1(),
    image: img1,
    title: 'Fashion',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img2,
    title: 'Beauty',
    followers: '129k',
    selected: true
  },
  {
    id: v1(),
    image: img3,
    title: 'Lifestyle',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img4,
    title: 'Handmade',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img1,
    title: 'Cars',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img1,
    title: 'Cars',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img2,
    title: 'Modern art',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img3,
    title: 'Make Up',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img4,
    title: 'Pets',
    followers: '129k',
    selected: false
  },
  {
    id: v1(),
    image: img1,
    title: 'Video Games',
    followers: '129k',
    selected: false
  }
];
