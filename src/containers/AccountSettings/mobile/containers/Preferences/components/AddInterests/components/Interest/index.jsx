import React from 'react';

/** IMPORTED AND USED STYLES FROM OTHER COMPONENT */
import {
  CardWrap,
  CardFollowers,
  CardImage,
  CardImageWrapper,
  CardOverlay,
  CardTitle
} from 'containers/UserOnboardingPage/styled';
import Icons from 'components/Icon';

const Interest = ({databaseId, featuredImage, isFollowed, title, totalFollowers, onFollowInterest}) => {
  return (
    <CardWrap onClick={() => onFollowInterest(databaseId)} key={databaseId}>
      <CardImageWrapper>
        <CardImage src={featuredImage?.node?.sourceUrl} />
        <CardOverlay selected={isFollowed}>
          <Icons type="checkmark" color="#fff" height={32} width={32} />
        </CardOverlay>
      </CardImageWrapper>
      <CardTitle>{title}</CardTitle>
      <CardFollowers>{totalFollowers} followers</CardFollowers>
    </CardWrap>
  );
};

export default Interest;
