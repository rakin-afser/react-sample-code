import styled from 'styled-components';
import {Button} from 'antd';

export const BtnWrap = styled.div`
  margin-top: 16px;
`;

export const AddMore = styled(Button)`
  &&& {
    color: #ed484f;
    border-color: #ed484f;
    height: 28px;
    border-radius: 30px;
    padding: 0 13px;
    display: inline-flex;
    align-items: center;
    font-size: 12px;

    i {
      padding-right: 8px;
    }
  }
`;
