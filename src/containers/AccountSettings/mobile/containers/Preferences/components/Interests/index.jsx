import React from 'react';
import {Title, Wrap} from 'containers/AccountSettings/mobile/containers/Preferences/styled';
import {AddMore, BtnWrap} from 'containers/AccountSettings/mobile/containers/Preferences/components/Interests/styled';
import RoundTags from 'components/RoundTags';
import Icon from 'components/Icon';
import {useQuery, useMutation} from '@apollo/client';
import {GET_INTERESTS} from 'queries';
import {FOLLOW_INTEREST} from 'mutations';
import Error from 'components/Error';
import {userId} from 'util/heplers';

import AddInterests from 'containers/AccountSettings/mobile/containers/Preferences/components/AddInterests';
import Loader from 'components/Loader';

const Interests = ({activeInterestsModal, setActiveInterestsModal}) => {
  const {data: interests, loading, error} = useQuery(GET_INTERESTS);

  const [followInterest] = useMutation(FOLLOW_INTEREST);

  const onFollowInterest = (interestId) => {
    followInterest({
      variables: {interestId, userId},
      update(cache, {data}) {
        cache.modify({
          fields: {
            interests() {
              return {data};
            }
          }
        });
      }
    });
  };

  if (error) return <Error />;

  if (loading) return <Loader />;

  return (
    <>
      <Wrap pb={24}>
        <Title>Interests</Title>
        <RoundTags
          onFollowInterest={onFollowInterest}
          tags={interests?.interests?.nodes?.filter((el) => el.isFollowed)}
          wrap={1}
        />
        <BtnWrap>
          <AddMore onClick={() => setActiveInterestsModal(true)}>
            <Icon type="plus" />
            Add More
          </AddMore>
        </BtnWrap>
      </Wrap>
      <AddInterests
        interests={interests?.interests?.nodes}
        active={activeInterestsModal}
        setActive={setActiveInterestsModal}
        onFollowInterest={onFollowInterest}
      />
    </>
  );
};

export default Interests;
