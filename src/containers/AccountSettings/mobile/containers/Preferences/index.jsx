import React, {useEffect, useState} from 'react';
import {Formik} from 'formik';
import {Wrap, Title, Hr} from './styled';
import Input from 'components/Input';
import SelectField from 'components/SelectField';
import {country, currencies, languages, timeZones} from 'constants/staticData';
import {Select} from 'antd';
import SavePanel from 'components/SavePanel';
import useGlobal from 'store';
import {useMutation} from '@apollo/client';
import {UpdatedUser} from 'mutations';
import {useUser} from 'hooks/reactiveVars';
import {localStorageKey} from 'constants/config';
import Interests from 'containers/AccountSettings/mobile/containers/Preferences/components/Interests';

const {Option} = Select;

const Preferences = () => {
  const [updatePreferences, {loading: updateUserLoading, error: updateUserError}] = useMutation(UpdatedUser, {
    onCompleted() {
      setActiveInterestsModal(false);
    }
  });
  const [user] = useUser();
  const [activeInterestsModal, setActiveInterestsModal] = useState(false);
  const [, globalActions] = useGlobal();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/account-settings');
  }, []);

  const socialLogin =
    typeof localStorage.getItem(localStorageKey.socialLogin) == 'string'
      ? JSON.parse(localStorage.getItem(localStorageKey.socialLogin))
      : localStorage.getItem(localStorageKey.socialLogin);
  const onSubmit = (values) => {
    if (user) {
      let input;
      if (socialLogin == true) {
        input = {
          id: user?.userId,
          country: values?.country,
          currency: values?.currency,
          language: values?.language,
          timezone: values?.timezone.toString()
        };
      } else {
        input = {
          id: user?.userId,
          country: values?.country,
          currency: values?.currency,
          language: values?.language,
          timezone: values?.timezone.toString(),
          password: values?.newPassword
        };
      }

      updatePreferences({
        variables: {
          input
        },
        update(cache, {data}) {
          cache.modify({
            fields: {
              user(existing) {
                const userData = data?.updateUser?.user;
                return {...existing, userData};
              }
            }
          });
        }
      });
    }
  };

  return (
    <>
      <Formik
        enableReinitialize
        initialValues={{
          country: '',
          currency: '',
          language: '',
          timezone: '',
          currentPassword: '',
          newPassword: '',
          confirmPassword: ''
        }}
        validate={(values) => {
          const errors = {};

          if (socialLogin != true) {
            if (!values.currentPassword) {
              errors.currentPassword = 'Current password is required';
            }

            if (!values.newPassword) {
              errors.newPassword = 'New password is required';
            }
            if (values.newPassword !== values.confirmPassword) {
              errors.confirmPassword = 'Passwords do not match';
            }

            if (!values.confirmPassword) {
              errors.confirmPassword = 'Password is required';
            }
          }

          return errors;
        }}
        onSubmit={onSubmit}
      >
        {({values, errors, handleChange, handleBlur, handleSubmit, submitCount, setFieldValue}) => {
          return (
            <form>
              <Wrap>
                <Title>Local Preferences</Title>
                <SelectField
                  key="country"
                  name="country"
                  id="country"
                  label="Country"
                  value={values.country}
                  onChange={(value) => {
                    setFieldValue('country', value);
                  }}
                >
                  {Object.keys(country).map((key) => (
                    <Option value={key} key={key}>
                      {key}
                    </Option>
                  ))}
                </SelectField>

                <SelectField
                  label="Currency"
                  key="currency"
                  name="currency"
                  id="currency"
                  onChange={(value) => {
                    setFieldValue('currency', value);
                  }}
                  value={values.currency}
                >
                  {currencies.map((currency) => (
                    <Option value={currency.cc} key={currency.cc}>
                      {currency.cc} ({currency.name})
                    </Option>
                  ))}
                </SelectField>
                <SelectField
                  label="Timezone"
                  key="timezone"
                  name="timezone"
                  id="timezone"
                  onChange={(value) => {
                    setFieldValue('timezone', value);
                  }}
                  value={values.timezone}
                >
                  {timeZones.map((timezone) => (
                    <Option value={timezone.value} key={timezone.value}>
                      {timezone.text}
                    </Option>
                  ))}
                </SelectField>
                <SelectField
                  label="Language"
                  key="language"
                  name="language"
                  id="language"
                  onChange={(value) => {
                    setFieldValue('language', value);
                  }}
                  value={values.language}
                >
                  {languages.map((language) => (
                    <Option value={language.code} key={language.code}>
                      {language.name}
                    </Option>
                  ))}
                </SelectField>
              </Wrap>

              <Hr />
              {socialLogin != true ? (
                <Wrap>
                  <Title>Change Password</Title>
                  <Input
                    showPassword
                    type="password"
                    name="currentPassword"
                    label="Enter Old Password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={submitCount > 0 && errors.currentPassword}
                    value={values.currentPassword}
                    required
                  />
                  <Input
                    showPassword
                    passwordChars
                    name="newPassword"
                    type="password"
                    label="Enter New Password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={submitCount > 0 && errors.newPassword}
                    value={values.newPassword}
                    required
                  />
                  <Input
                    showPassword
                    type="password"
                    label="Re-Enter New Password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    name="confirmPassword"
                    error={submitCount > 0 && errors.confirmPassword}
                    value={values.confirmPassword}
                    reset={values.confirmPassword.length > 0}
                    required
                  />
                </Wrap>
              ) : null}
              <Hr />
              <Interests
                activeInterestsModal={activeInterestsModal}
                setActiveInterestsModal={setActiveInterestsModal}
              />

              <SavePanel
                onCancel={() => setActiveInterestsModal(false)}
                onSave={() => {
                  handleSubmit();
                }}
                px="0"
                mt="39px"
                saveTitle="Save"
                isLoading={updateUserLoading}
              />
            </form>
          );
        }}
      </Formik>
    </>
  );
};

export default Preferences;
