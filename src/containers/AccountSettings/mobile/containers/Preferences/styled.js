import styled from 'styled-components';

export const Title = styled.h3`
  font-size: 18px;
  margin-bottom: 16px;
`;

export const Wrap = styled.div`
  padding: 24px 16px ${({pb}) => (pb ? pb : 8)}px;
`;

export const Hr = styled.hr`
  border: 4px solid #fafafa;
  margin: 0;
`;
