import React, {useEffect} from 'react';
import {useParams} from 'react-router-dom';

import {Wrapper, PageLink} from './styled';
import Icon from 'components/Icon';
import useGlobal from 'store';

const links = [
  {
    name: 'Preferences',
    to: '/account-settings/preferences'
  },
  {
    name: 'Notifications',
    to: '/account-settings/notifications'
  },
  {
    name: 'Addresses',
    to: '/account-settings/addresses'
  },
  {
    name: 'Payout Details',
    to: '/account-settings/payout'
  },
  {
    name: 'Connected Accounts',
    to: '/account-settings/connected'
  }
];

const Default = () => {
  const [, globalActions] = useGlobal();
  const {userParam} = useParams();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink(`/profile/${userParam ? `${userParam}/` : ''}settings`);
  }, []);

  return (
    <Wrapper>
      {links.map((link) => (
        <PageLink key={link.name} to={link.to}>
          {link.name}
          <Icon type="chevronRight" fill="#999" />
        </PageLink>
      ))}
    </Wrapper>
  );
};

export default Default;
