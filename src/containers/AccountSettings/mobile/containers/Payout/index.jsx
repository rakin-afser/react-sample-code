import React, {useEffect} from 'react';
import {Container, Wrap, Title, FieldSet} from './styled';
import Button from 'components/Buttons';
import Input from 'components/Input';
import SavePanel from 'components/SavePanel';
import {useWindowSize} from '@reach/window-size';
import {mobileHeaderHeight} from 'constants/mobileSizes';
import useGlobal from 'store';

const Payout = () => {
  const {height} = useWindowSize();

  const [, globalActions] = useGlobal();

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/account-settings');
  }, []);

  return (
    <Container style={{minHeight: height - parseInt(mobileHeaderHeight)}}>
      <Wrap mt={24}>
        <Title>Your Payout Details</Title>
        <FieldSet>
          <Input label="Account Holder Name" />
          <Input label="Bank Name" />
          <Input label="Account Name" />
          <Input label="SWIFT" />
          <Input label="IBAN" />
        </FieldSet>
      </Wrap>
      <Wrap mb={24}>
        <Button type="addNew" title="Add a New Method" />
      </Wrap>
      <SavePanel mt="auto" saveTitle="Save Info" />
    </Container>
  );
};

export default Payout;
