import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Wrap = styled.div`
  padding-top: ${({mt}) => (mt ? mt : 0)}px;
  padding-bottom: ${({mb}) => (mb ? mb : 0)}px;
  padding-right: 16px;
  padding-left: 16px;
`;

export const FieldSet = styled.div`
  padding-bottom: 8px;

  label {
    color: #000;
  }
`;

export const Title = styled.h4`
  font-size: 18px;
  margin-bottom: 16px;
`;
