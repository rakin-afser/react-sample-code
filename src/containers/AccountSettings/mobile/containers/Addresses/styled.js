import styled from 'styled-components';
import Icon from 'components/Icon';

export const Container = styled.div`
  display: flex;
  flex-direction: column;

  .save-panel {
    margin-top: auto;
  }
`;

export const Wrap = styled.div`
  padding: 24px 16px;
`;

export const Title = styled.h4`
  font-size: 18px;
  margin-bottom: 24px;
`;

export const Dots = styled(Icon)`
  position: absolute;
  top: 16px;
  right: 0;
`;

export const Item = styled.div`
  padding-top: 16px;
  padding-bottom: 16px;
  border-bottom: 1px solid #efefef;
  position: relative;
`;

export const List = styled.div`
  margin-bottom: 16px;
`;

export const Heading = styled.div`
  display: flex;
  align-items: center;
  color: ${({color}) => color};

  i {
    min-width: 26px;
  }
`;

export const AddrTitle = styled.h5`
  font-weight: 500;
  font-size: 14px;
  color: ${({primary}) => (primary ? '#000' : '#666')};
  line-height: 17px;
  margin-bottom: 0;
`;

export const Address = styled.p`
  color: #999;
  margin-bottom: 0;
  max-width: 241px;
  padding-top: 10px;
  padding-left: 5px;
`;

export const HeadingWrap = styled.div`
  margin-left: 10px;
  display: flex;
  flex-direction: column;
`;

export const Primary = styled.span`
  color: #ed484f;
  letter-spacing: 3px;
  font-size: 12px;
  letter-spacing: 1;
`;
