import React, {useEffect, useState} from 'react';
import Button from 'components/Buttons';
import SavePanel from 'components/SavePanel';
import {Container, Wrap, Title, List, Item, Heading, AddrTitle, Address, HeadingWrap, Dots, Primary} from './styled';
import {useWindowSize} from '@reach/window-size';
import {mobileHeaderHeight} from 'constants/mobileSizes';
import Icon from 'components/Icon';
import useGlobal from 'store';
import AddNew from './components/AddNew';
import {useQuery,useMutation} from '@apollo/client';
import {REMOVE_SHIPPING_ADDRESS} from 'mutations';
import {ADDRESSES} from 'queries'
import {useUser} from 'hooks/reactiveVars';
import {isEmpty} from 'lodash';
import {ShippingAddressFragment} from 'fragments';
const Addresses = () => {
  const [, globalActions] = useGlobal();
  const {data, error, loading} = useQuery(ADDRESSES)
  const [removeAddress] = useMutation(REMOVE_SHIPPING_ADDRESS);
  const [user] = useUser();
  
  const selectedAddr = data && data?.testSampleAddresses && data?.testSampleAddresses?.shipping.length > 0 ? data?.testSampleAddresses?.shipping : []

  useEffect(() => {
    globalActions.setHeaderBackButtonLink('/account-settings');
  }, []);

  const {height} = useWindowSize();

  const [active, setActive] = useState(false);
  const [primary, setPrimary] = useState(0);

  const [editAddress, setEditAddress] = useState();
  const onDeleteAddress = (id) => {
    if (user) {
      removeAddress({
        variables: {input: {addrId: id}},
        update(cache, {data}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = data?.removeShippingAddress?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                return {...existing, shipping};
              }
            }
          });
        }
      });
    }
  };
  // const addresses = [
  //   {
  //     title: 'Home',
  //     address: '7480 Mockingbird Hill undefined Ironville, Oklahoma 41284 United States',
  //     primary: true,
  //     icon: <Icon type="house" color="currentColor" />
  //   },
  //   {
  //     title: 'Work',
  //     address: '7480 Mockingbird Hill undefined Ironville, Oklahoma 41284 United States',
  //     primary: false,
  //     icon: <Icon type="work" color="currentColor" />
  //   },
  //   {
  //     title: 'Residence',
  //     address: '7480 Mockingbird Hill undefined Ironville, Oklahoma 41284 United States',
  //     primary: false,
  //     icon: <Icon type="marker" width="21" height="26" color="currentColor" />
  //   }
  // ];
  const {id} = selectedAddr?.find(({selected}) => selected) || {};
  // console.log("............id",id)
  function renderAddresses() {
    return (
      <List>
        {selectedAddr?.map((address, i) => {
          const {id, firstName, build, address1, city, district, country, addressNickname,type} = address;
          // console.log("..........props",id)
          const buttons = [
            {
              title: 'Select As Primary',
              onClick: () => {
                globalActions.setOptionsPopup({active: false});
                setPrimary(i);
              }
            },
            {
              title: 'Edit Address',
              onClick: () =>{ globalActions.setOptionsPopup({active: false})
                                setEditAddress(address)
                                setActive(true)
                            }
              
            },
            {
              title: 'Delete Address',
              onClick: () => {globalActions.setOptionsPopup({active: false})
                              onDeleteAddress(id)
                              }
            }
          ];

          return (
            <Item key={id}>
              <Heading color={primary === i ? '#ED484F' : '#666'}>
                {type.toLowerCase() ==='office'? <Icon type="work" color="currentColor" /> : <Icon type="house" color="currentColor" />}
                <HeadingWrap>
                  <AddrTitle primary={primary === i ? 1 : 0}> {type}</AddrTitle>
                  {primary === i ? <Primary>Primary</Primary> : null}
                </HeadingWrap>
              </Heading>
              <Address>{build } {address1} {district}  {city} {country}</Address>
              <Dots
                onClick={() => globalActions.setOptionsPopup({active: true, buttons})}
                type="dots"
                width={24}
                height={24}
              />
            </Item>
          );
        })}
      </List>
    );
  }

  return (
    <Container style={{minHeight: height - parseInt(mobileHeaderHeight)}}>
      <Wrap>
        <Title>Shipping Addresses</Title>
        {renderAddresses()}
        <Button props={{onClick: () => setActive(true)}} type="addNew" title="Add New Address" />
      </Wrap>
      <AddNew {...{active, setActive}} addressData={editAddress} setEditAddress={setEditAddress}/>
    </Container>
  );
};

export default Addresses;
