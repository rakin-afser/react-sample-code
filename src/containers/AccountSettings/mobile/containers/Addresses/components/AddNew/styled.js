import {Button} from 'antd';
import styled from 'styled-components';

export const Title = styled.h4`
  font-size: 18px;
  line-height: 1.22;
  color: #000;
  margin-top: 24px;
  margin-bottom: 16px;
`;

export const FieldSet = styled.div`
  display: flex;

  > div {
    max-width: 33.333%;
  }

  > div + div {
    margin-left: 8px;
  }
`;
export const RowFields = styled.div`
  display: flex;
  margin: 0 -4px 16px;

  > * {
    width: 100%;
    margin: 0 4px;
  }
`;
