import BackPage from 'components/Modals/mobile/BackPage';
import React from 'react';
import {Formik} from 'formik';
import {Title, RowFields} from './styled';
import SelectField from 'components/SelectField';
import {Select} from 'antd';
import {country} from 'constants/staticData';
import SavePanel from 'components/SavePanel';
import {ADD_SHIPPING_ADDRESS, EDIT_SHIPPING_ADDRESS} from 'mutations';
import {useMutation} from '@apollo/client';
import Input from 'components/Input';
import cities from '../../../../../../GuestCheckoutPage/assets/data/cities';
import {ShippingAddressFragment} from 'fragments';
import {useUser} from 'hooks/reactiveVars';
import {getCountryFromCode} from 'util/heplers';
const {Option} = Select;

const AddNew = (props) => {
  const [addAddress] = useMutation(ADD_SHIPPING_ADDRESS, {
    onCompleted() {
      props.setActive(false);
    }
  });

  const [updateAddress] = useMutation(EDIT_SHIPPING_ADDRESS, {
    onCompleted() {
      props.setActive(false);
    }
  });

  const [user] = useUser();

  const onSubmitCreate = (values) => {
    const {
      country: countryName,
      phone,
      firstName,
      district,
      city,
      address1,
      // type,
      // build,
      block,
      road,
      // floor,
      apartment,
      addressNickname
    } = values;

    const address = {
      addrCountry: country?.[countryName]?.code,
      addrPhone: phone,
      addrFirstName: firstName,
      addrDistrict: district,
      addrCity: city,
      addrAddress1: address1,
      // addrType: type,
      // addrBuild: build,
      addrBlock: parseInt(block),
      addrRoad: parseInt(road),
      // addrFloor: parseInt(floor),
      addrApartment: parseInt(apartment),
      addrAddressNickname: addressNickname
    };

    addAddress({
      variables: {input: address},
      update(cache, {data}) {
        cache.modify({
          fields: {
            testSampleAddresses(existing) {
              const shipping = data?.addShippingAddress?.shippingAddresses?.map((addr) =>
                cache.writeFragment({
                  id: cache.identify(addr),
                  data: addr,
                  fragment: ShippingAddressFragment
                })
              );
              return {...existing, shipping};
            }
          }
        });
      }
    });
  };

  const onSubmitEdit = (values) => {
    if (user) {
      const input = {
        addrId: props.addressData?.id,
        addrAddress1: values?.address1,
        addrAddressNickname: values?.addressNickname,
        addrApartment: parseInt(values?.apartment),
        addrBlock: parseInt(values?.block),
        addrBuild: values?.build,
        addrCity: values?.city,
        addrCountry: country?.[values?.country]?.code,
        addrDistrict: values?.district,
        addrFirstName: values?.firstName,
        addrFloor: parseInt(values?.floor),
        addrPhone: values?.phone,
        addrRoad: parseInt(values?.road),
        addrType: values?.type
      };

      updateAddress({
        variables: {
          input
        },
        update(cache, {data}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = data?.addShippingAddress?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                return {...existing, shipping};
              }
            }
          });
        }
      });
    }
  };

  const onSubmit = (values) => {
    if (props.addressData) {
      onSubmitEdit(values);
    } else {
      onSubmitCreate(values);
    }
  };

  const initialValue = !props.addressData
    ? {
        country: '',
        phone: '',
        firstName: '',
        district: '',
        city: '',
        address1: '',
        // type: '',
        // build: '',
        block: '',
        road: '',
        // floor: '',
        apartment: '',
        addressNickname: ''
      }
    : {...props.addressData, country: getCountryFromCode(props.addressData?.country)};

  const buttonName = props.addressData ? 'Update Address' : 'Add Address';
  return (
    <BackPage title="Add New Address" {...props}>
      {props.addressData ? <Title> Edit Address </Title> : <Title> New Address Information</Title>}
      <Formik
        enableReinitialize
        initialValues={initialValue}
        validate={(values) => {
          const errors = {};

          if (!values.country) {
            errors.country = 'Please select a country';
          }

          if (!values.phone) {
            errors.phone = 'Please enter a valid mobile number';
          }

          if (!values.firstName) {
            errors.firstName = 'Please enter a valid full name';
          }

          if (!values.district) {
            errors.district = 'Please select a District / Area';
          }

          if (!values.city) {
            errors.district = 'Please enter a valid City';
          }

          if (!values.address1) {
            errors.address1 = 'Please enter a valid Street / Building Name';
          }

          // if (!values.type) {
          //   errors.type = 'Please choose Address Type';
          // }

          // if (!values.build) {
          //   errors.build = 'Please enter a valid build / Building No.';
          // }

          if (!values.block) {
            errors.block = 'Please enter a valid Block No.';
          }

          if (!values.road) {
            errors.road = 'Please enter a valid Road No.';
          }

          return errors;
        }}
        onSubmit={onSubmit}
      >
        {({values, errors, status, handleChange, handleSubmit, submitCount, setFieldValue}) => {
          return (
            <form>
              <SelectField
                key="country"
                name="country"
                id="country"
                label="Country"
                onChange={(value) => {
                  setFieldValue('country', value);
                }}
                value={values.country}
                error={submitCount > 0 && errors.country}
                required
              >
                {Object.keys(country).map((key) => (
                  <Option value={key} key={key}>
                    {key}
                  </Option>
                ))}
              </SelectField>

              <Input
                key="phone"
                type="tel"
                name="phone"
                id="phone"
                label="Mobile Number"
                onChange={handleChange}
                error={submitCount > 0 && errors.phone}
                value={values.phone}
                reset={values.phone.length > 0}
                disabled={!values.country}
                resetCallback={() => {
                  setFieldValue('phone', '');
                }}
                number={country[values.country] ? country[values.country].dialCode : '+'}
                required
              />

              <Input
                key="firstName"
                type="text"
                name="firstName"
                id="firstName"
                label="Full Name (First and Last name)"
                onChange={handleChange}
                error={submitCount > 0 && errors.firstName}
                value={values.firstName}
                reset={values.firstName.length > 0}
                resetCallback={() => {
                  setFieldValue('firstName', '');
                }}
                required
              />

              <SelectField
                key="district"
                name="district"
                id="district"
                label="District / Area"
                onChange={(value) => {
                  setFieldValue('district', value);
                }}
                value={values.district}
                error={submitCount > 0 && errors.district}
                disabled={!values.country}
                required
              >
                {cities[values.country] &&
                  cities[values.country].map((city, key) => (
                    <Option value={city} key={key}>
                      {city}
                    </Option>
                  ))}
              </SelectField>

              <Input
                key="city"
                type="text"
                name="city"
                id="city"
                label="City"
                onChange={handleChange}
                error={submitCount > 0 && errors.city}
                value={values.city}
                reset={values.city.length > 0}
                resetCallback={() => {
                  setFieldValue('city', '');
                }}
                required
              />

              <Input
                key="address1"
                type="text"
                name="address1"
                id="address1"
                label="Street / Building Name"
                onChange={handleChange}
                error={submitCount > 0 && errors.address1}
                value={values.address1}
                reset={values.address1.length > 0}
                resetCallback={() => {
                  setFieldValue('address1', '');
                }}
                required
              />

              {/* <RowFields>
                <SelectField
                  key="type"
                  name="type"
                  id="type"
                  label="Address Type"
                  onChange={(value) => {
                    setFieldValue('type', value);
                  }}
                  value={values.type}
                  error={submitCount > 0 && errors.type}
                  required
                >
                  <Option value="Apartment">Apartment</Option>
                  <Option value="Office">Office</Option>
                </SelectField>

                <Input
                  key="build"
                  type="text"
                  name="build"
                  id="build"
                  label="build / Building No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.build}
                  value={values.build}
                  reset={values.build.length > 0}
                  resetCallback={() => {
                    setFieldValue('build', '');
                  }}
                  required
                />
              </RowFields> */}

              <RowFields>
                <Input
                  key="block"
                  type="number"
                  name="block"
                  id="block"
                  label="Block No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.block}
                  value={values.block}
                  reset={values.block}
                  resetCallback={() => {
                    setFieldValue('block', '');
                  }}
                  required
                />

                <Input
                  key="road"
                  type="number"
                  name="road"
                  id="road"
                  label="Road No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.road}
                  value={values.road}
                  reset={values.road}
                  resetCallback={() => {
                    setFieldValue('road', '');
                  }}
                  required
                />
                {/*
                <Input
                  key="floor"
                  type="number"
                  name="floor"
                  id="floor"
                  label="Floor No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.floor}
                  value={values.floor}
                  reset={values.floor}
                  resetCallback={() => {
                    setFieldValue('floor', '');
                  }}
                /> */}

                <Input
                  key="apartment"
                  type="number"
                  name="apartment"
                  id="apartment"
                  label="Apt./House No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.apartment}
                  value={values.apartment}
                  reset={values.apartment}
                  resetCallback={() => {
                    setFieldValue('apartment', '');
                  }}
                />
              </RowFields>

              <Input
                key="addressNickname"
                type="text"
                name="addressNickname"
                id="addressNickname"
                label="Address Nickname"
                onChange={handleChange}
                error={submitCount > 0 && errors.addressNickname}
                value={values.addressNickname}
                reset={values.addressNickname.length > 0}
                resetCallback={() => {
                  setFieldValue('addressNickname', '');
                }}
              />
              <SavePanel
                onCancel={() => props.setActive(false)}
                onSave={() => {
                  handleSubmit();
                }}
                px={'0'}
                mt="39px"
                saveTitle={buttonName}
              />
            </form>
          );
        }}
      </Formik>
    </BackPage>
  );
};

export default AddNew;
