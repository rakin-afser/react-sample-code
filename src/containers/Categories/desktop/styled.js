import styled from 'styled-components/macro';
import {primaryColor} from 'constants/colors';

export const Sidebar = styled.div`
  max-width: 254px;
  margin: 7px 30px 0 0;

  @media (max-width: 1366px) {
    margin: 7px 30px 0 30px;
  }
`;

export const Content = styled.div`
  padding-top: 10px;
  max-width: 886px;
`;

export const Header = styled.div`
  margin-bottom: 38px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const SelectWrapper = styled.div`
  width: 240px;

  & .ant-select-selection {
    border: none;

    &:focus {
    }
  }

  svg {
    path {
      fill: #000;
    }
  }
`;

export const Tags = styled.div``;

export const Tag = styled.div`
  position: relative;
  margin-bottom: 19px;
  margin-right: 12px;
  padding: 6px 12px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  background: #e4e4e4;
  border-radius: 24px;
  cursor: pointer;
  z-index: 1;

  i {
    margin-left: 3px;

    svg {
      path {
        transition: ease 0.4s;
      }

      &:hover {
        path {
          fill: ${primaryColor};
        }
      }
    }
  }
`;

export const CardWrapper = styled.div`
  &:hover {
    .hover-container {
      display: none;
    }
  }
`;

export const PaginationWrapper = styled.div`
  text-align: center;
`;
