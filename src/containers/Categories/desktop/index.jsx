import React, {useState, useEffect} from 'react';
import {Row, Col} from 'antd';
import _ from 'lodash';
import {Query} from '@apollo/client/react/components';
import {useHistory} from 'react-router-dom';

import usePagination from 'util/usePagination';
import {PRODUCTS} from 'queries';
import Error from 'components/Error';
import Pagination from 'components/PagePagination';
import Product from 'components/Cards/Product';
import Grid from 'components/Grid';
import Categories from 'components/Categories';
import Select from 'components/SelectTrue';
import Icons from 'components/Icon';
import NothingFound from 'components/NothingFound';
import {Sidebar, Content, Header, SelectWrapper, Tags, Tag, CardWrapper, PaginationWrapper} from './styled';
import Skeleton from '../components/Skeleton';
import {orderByValues, categoriesPageSortOption} from 'constants/sorting';

const CategoriesDesktop = ({categories, currentCategorySlug, filters, ...props}) => {
  const [sidebarValues, setSidebarValues] = useState([]);
  const [sortBy, setSortBy] = useState(null);
  const {currentPage, currentCursor, goNextPage, goPrevPage, resetPagination} = usePagination();
  const variables = {
    categorySlug: currentCategorySlug,
    minPrice: Number(filters?.priceValue?.[0]),
    maxPrice: Number(filters?.priceValue?.[1]),
    first: 9 * currentPage,
    cursor: currentCursor,
    onSale: filters?.offers?.includes('sale'),
    orderBy: {value: orderByValues[sortBy]}
  };
  const history = useHistory();

  useEffect(() => {
    resetPagination();
  }, [currentCategorySlug]);

  const onSort = (value) => {
    setSortBy(value);
  };

  const onTagRemove = (e) => {
    const valueToRemove = e.currentTarget.parentNode.textContent;
    setSidebarValues(sidebarValues.filter((el) => el !== valueToRemove));
  };

  return (
    <Grid padding="28px 0 0 0" pageContainer>
      <Row gutter={16}>
        <Col span={6}>
          <Sidebar>
            <Categories
              more
              values={sidebarValues}
              select={setSidebarValues}
              data={categories}
              filters={filters}
              {...props}
            />
          </Sidebar>
        </Col>

        <Col span={18}>
          <Content>
            <Query query={PRODUCTS} variables={variables}>
              {({data, loading, error}) => {
                if (loading || !data) return <Skeleton />;

                if (error) return <Error>{error?.message}</Error>;

                const productsData = data?.products?.nodes || [];
                const pageInfo = data?.products?.pageInfo || {};

                return (
                  <>
                    <Header>
                      {pageInfo?.total} Products
                      <SelectWrapper>
                        <Select
                          onChange={onSort}
                          options={categoriesPageSortOption}
                          valueKey="value"
                          value={sortBy || 'Sort by'}
                          labelKey="name"
                        />
                      </SelectWrapper>
                    </Header>
                    <Tags>
                      {sidebarValues.map((el, idx) => (
                        <Tag className="test" key={idx}>
                          <span>{el}</span>
                          <Icons onClick={onTagRemove} type="crossThin" width={16} height={16} />
                        </Tag>
                      ))}
                    </Tags>
                    {loading || !data ? (
                      <Skeleton />
                    ) : (
                      <Row gutter={16}>
                        {_.isEmpty(productsData) && <NothingFound hideAdditionalText />}
                        {productsData.map((item, i) => (
                          <Col key={i} span={12} lg={8}>
                            <CardWrapper>
                              <Product
                                // onClick={() => history.push(`/product/${item.slug}`)}
                                index={i}
                                content={item}
                                margin="0 0 48px 0"
                              />
                            </CardWrapper>
                          </Col>
                        ))}
                      </Row>
                    )}
                    {!(_.isEmpty(productsData) && currentPage === 1) && (
                      <PaginationWrapper>
                        <Pagination
                          totalPages={Math.ceil(pageInfo?.total / 9)}
                          prevPage={() => goPrevPage(pageInfo)}
                          nextPage={() => goNextPage(pageInfo)}
                          currentPage={currentPage}
                          style={{display: 'inline-flex'}}
                        />
                      </PaginationWrapper>
                    )}
                  </>
                );
              }}
            </Query>
          </Content>
        </Col>
      </Row>
    </Grid>
  );
};

export default CategoriesDesktop;
