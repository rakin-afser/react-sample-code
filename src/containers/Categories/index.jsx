import React, {useEffect, useState} from 'react';
import {useWindowSize} from '@reach/window-size';
import {useQuery} from '@apollo/client';
import {useParams, useHistory} from 'react-router-dom';
import qs from 'qs';
import _ from 'lodash';
import {useLocation} from 'react-router';

import {CATEGORIES} from 'queries';
import Loader from 'components/Loader';
import Layout from 'containers/Layout';
import CategoriesMobile from 'containers/Categories/mobile';
import CategoryMobile from 'containers/Categories/mobile/components/Category';
import CategoriesDesktop from 'containers/Categories/desktop';
import Error from 'components/Error';
import Icon from 'components/Icon';
import {DotsWrapper} from 'containers/Categories/styled';
import FadeOnMount from 'components/Transitions';
import DropdownMenu from 'containers/DealsPage/mobile/components/DropdownMenu';

const Categories = () => {
  const {width} = useWindowSize();
  const history = useHistory();
  const isMobile = width <= 767;
  const {data, error, loading} = useQuery(CATEGORIES);
  const {categorySlug, subCategorySlug1, subCategorySlug2} = useParams();
  const {search, pathname} = useLocation();
  const {filters: filtersValue, viewAll} = qs.parse(search.replace('?', '')) || {};
  const [currentCategory, setCurrentCategory] = useState({});
  const [categoryHistory, setCategoryHistory] = useState([]);
  const [showDropdownMenu, setShowDropdownMenu] = useState(false);

  useEffect(() => {
    const currentHistory = getCategoriesHistory({
      slug: '',
      name: 'All Categories',
      children: data?.productCategories
    });
    setCurrentCategory([...currentHistory].pop() || {});
    setCategoryHistory(currentHistory || []);
  }, [data, categorySlug, subCategorySlug1, subCategorySlug2]);

  const categoryClick = (slug, level) => {
    switch (level) {
      case 2:
        history.push(slug ? `/categories/${categorySlug}/${slug}` : `/categories/${categorySlug}`);
        break;
      case 3:
      case 4:
        if (slug !== subCategorySlug2)
          history.push(
            slug
              ? `/categories/${categorySlug}/${subCategorySlug1}/${slug}`
              : `/categories/${categorySlug}/${subCategorySlug1}`
          );
        break;
      case 1:
      default:
        history.push(slug ? `/categories/${slug}` : `/categories`);
        break;
    }
  };

  const setFilters = (filtersObj) => {
    const queryString = qs.stringify({viewAll, filters: filtersObj});
    history.push(`${pathname}?${queryString}`);
  };

  const getCategoriesHistory = (data) => {
    let history = [data];
    let historyItem = data;
    const levels = [categorySlug, subCategorySlug1, subCategorySlug2];

    levels.forEach((item) => {
      historyItem = historyItem?.children?.nodes?.find((el) => el.slug === item);
      if (item && historyItem) {
        history.push(historyItem);
      }
    });

    return history;
  };

  if (loading) return <Loader />;

  if (error) return <Error>{error?.message}</Error>;

  const categoriesData = {
    slug: '',
    name: isMobile ? 'Categories' : 'All Categories',
    children: data?.productCategories
  };
  const prevCategory = categoryHistory?.[categoryHistory?.length - 2] || {};
  const showCategoryPage = subCategorySlug1 || viewAll;

  return (
    <Layout
      showBottomNav={isMobile && !showCategoryPage}
      isFooterShort={isMobile}
      hideHeaderLinks={isMobile}
      showBurgerButton={false}
      showBackButton={isMobile}
      showSearchButton={false}
      hideCopyright={isMobile}
      stickyHeader={isMobile}
      title={currentCategory?.name}
      showBasketButton={false}
      layOutStyles={{
        paddingTop: 0
      }}
      customRightIcon={
        <DotsWrapper onClick={() => setShowDropdownMenu(true)}>
          <Icon type="more" width={20} height={20} />
        </DotsWrapper>
      }
      hideFooter={isMobile}
      hideHeader={isMobile && !showCategoryPage}
    >
      {isMobile ? (
        <>
        {showDropdownMenu && <DropdownMenu closeMenu={() => setShowDropdownMenu(false)} isOpen={showDropdownMenu}/>}
        {showCategoryPage ? (
          <CategoryMobile
            category={currentCategory}
            categoryClick={categoryClick}
            currentLevel={categoryHistory?.length}
            filterItems={
              _.isEmpty(currentCategory?.children?.nodes)
                ? [{name: 'All', id: 'all'}]
                : [{name: 'All', id: 'all'}, ...currentCategory?.children?.nodes]
            }
          />
        ) : (
          <FadeOnMount>
            <CategoriesMobile
              prevCategoryName={prevCategory?.name}
              currentLevel={categoryHistory?.length}
              categoryClick={categoryClick}
              currentCategory={currentCategory}
            />
          </FadeOnMount>
        )}
        </>
      ) : (
        <CategoriesDesktop
          filters={filtersValue}
          setFilters={setFilters}
          currentCategorySlug={currentCategory?.slug || categorySlug}
          categories={categoriesData}
          categoryClick={categoryClick}
          categoryHistory={categoryHistory}
        />
      )}
    </Layout>
  );
};

export default Categories;
