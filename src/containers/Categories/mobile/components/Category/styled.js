import styled from 'styled-components/macro';
import {mobileHeaderHeight} from 'components/Header/constants';

export const Wrapper = styled.div`
  padding: ${mobileHeaderHeight} 0 30px;
`;

export const ContentContainer = styled.div`
  padding: 0 16px;
`;

export const SortContainer = styled.div`
  margin-top: 19px;
  padding: 0 16px 16px 16px;
  border-bottom: 1px solid #efefefef;
`;

export const ProductsWrapper = styled.div`
  margin: 16px 0;
`;
