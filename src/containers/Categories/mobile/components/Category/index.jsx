import React, {useEffect, useState} from 'react';
import {useLocation, useHistory} from 'react-router-dom';
import {useQuery} from '@apollo/client';
import qs from 'qs';
import _ from 'lodash';

import useGlobal from 'store';
import Loader from 'components/Loader';
import Error from 'components/Error';
import FilterBarMobile from 'components/FilterBarMobile';
import {getUrlWithoutLastParam} from 'util/heplers';
import {
  Wrapper,
  ContentContainer,
  ProductsWrapper,
  SortContainer
} from 'containers/Categories/mobile/components/Category/styled';
import FilterButtons from 'components/FilterButtons';
import {productFilters} from 'constants/filters';
import {PRODUCTS} from 'queries';
import ProductGridAsymmetrical from 'components/ProductGridAsymmetrical/mobile';
import Skeleton from 'containers/SearchResult/components/Skeleton';
import usePagination from 'util/usePagination';

const filterItemsDefault = productFilters.children?.find((item) => item.id === 'category')?.children || [];

const CategoryMobile = ({filterItems = filterItemsDefault, category}) => {
  const [globalState, setGlobalState] = useGlobal();
  const {pathname, search} = useLocation();
  const history = useHistory();
  const {filters: filtersValue, viewAll} = qs.parse(search.replace('?', '')) || {};
  const {currentPage, currentCursor, goNextPage, goPrevPage, resetPagination} = usePagination();
  const [savedData, setSavedData] = useState([]);
  const {data, loading, error} = useQuery(PRODUCTS, {
    variables: {
      categoryIn: filtersValue?.categories || category?.slug,
      first: 12 * currentPage,
      cursor: currentCursor
    },
    fetchPolicy: 'network-only'
  });
  const pageInfo = data?.products?.pageInfo || {};

  useEffect(() => {
    const urlWithoutLastParam = getUrlWithoutLastParam(pathname);
    setGlobalState.setHeaderBackButtonLink(viewAll ? pathname : urlWithoutLastParam);
  }, [pathname]);

  useEffect(() => {
    setSavedData([]);
    resetPagination();
  }, [search]);

  useEffect(() => {
    if (!_.isEmpty(data?.products?.nodes)) {
      setSavedData([...savedData, ...data?.products?.nodes]);
    }
  }, [data]);

  useEffect(() => {
    window.addEventListener('scroll', onScroll);
    return () => window.removeEventListener('scroll', onScroll);
  }, [loading]);

  const onScroll = () => {
    const scrollHeight = Math.max(
      document.body.scrollHeight,
      document.documentElement.scrollHeight,
      document.body.offsetHeight,
      document.documentElement.offsetHeight,
      document.body.clientHeight,
      document.documentElement.clientHeight
    );
    if (scrollHeight - 500 < window.pageYOffset + document.body.clientHeight) {
      goNextPage(pageInfo);
    }
  };

  const showFilterModal = () => {
    setGlobalState.setFilterModal(true, {type: 'products'});
  };

  const categoryFilterClick = (item) => {
    const queryString = qs.stringify({viewAll, filters: {categories: [item?.slug]}});
    history.push(`${pathname}?${queryString}`);
  };

  if (error)
    return (
      <Wrapper>
        <Error>{error?.message}</Error>
      </Wrapper>
    );

  const productsData = data?.products?.nodes || [];
  const currentCategoryFilter =
    filterItems.find((item) => item?.slug === filtersValue?.categories?.[0]) || filterItems[0];

  return (
    <Wrapper>
      <FilterButtons
        selectedId={filterItems.indexOf(currentCategoryFilter)}
        onClick={categoryFilterClick}
        data={filterItems}
        wrapPadding="24px 24px 8px 8px"
      />
      <SortContainer>
        <FilterBarMobile
          onClick={showFilterModal}
          results={pageInfo?.total}
          title="Sort & Filter"
          resultLabel="Items"
          redDot={true}
          iconName="filter"
        />
      </SortContainer>
      <ContentContainer>
        <ProductsWrapper>
          {loading && currentPage === 1 ? (
            <Skeleton />
          ) : (
            <ProductGridAsymmetrical onProductClick={(slug) => history.push(`/product/${slug}`)} data={savedData} />
          )}
        </ProductsWrapper>
      </ContentContainer>
    </Wrapper>
  );
};

export default CategoryMobile;
