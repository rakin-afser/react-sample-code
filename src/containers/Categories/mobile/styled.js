import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  max-width: 100vw;
  min-height: 100vh;
  overflow: hidden;
  padding: 32px 0 50px;
  display: flex;
  align-items: flex-start;
  justify-content: flex-start;
  flex-direction: column;

  & svg {
    max-width: 82px;
  }
`;

export const CategoriesTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #000000;
  padding: 0 16px 24px;
  width: 100%;
  display: flex;
  align-items: center;
  margin: -32px 0 0;
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: center;
  justify-content: center;
  margin-bottom: 50px;
  border-bottom: 1px solid #efefef;
`;

export const CategoriesWrapper = styled.div`
  width: 100%;
  max-width: 100%;
  padding: 0 28px;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(85px, 1fr));
  grid-gap: 19px 12px;
`;

export const CategoryImage = styled.div`
  height: 83px;
  width: 83px;
  border: 1px solid #d8d8d8;
  box-sizing: border-box;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 12px;
  flex-grow: 0;
  overflow: hidden;

  & svg {
    width: 34px;
    margin: 0 2px 5px 0;
  }

  & img {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
`;

export const Category = styled.div`
  max-width: 83px;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  flex-direction: column;
  justify-self: center;
  margin: auto;
`;

export const CategoryTitle = styled.div`
  width: 100%;
  font-family: Helvetica Neue;
  font-size: 12px;
  color: #000;
  margin: auto;
  line-height: 1;
  text-align: center;
`;

export const BackButton = styled.button`
  margin: 0;
  padding: 0;
  position: absolute;
  top: 9px;
  left: 15px;
`;

export const ProductsWrapper = styled.div`
  padding: 45px 16px 16px;
  width: 100%;
`;

export const ProductsTitle = styled(CategoriesTitle)`
  padding: 0 0 24px;
`;

export const ViewAllButton = styled.button`
  margin: -29px 0 20px 42px;
  border: 1px solid #cccccc;
  border-radius: 22px;
  font-style: normal;
  font-size: 12px;
  line-height: 14px;
  color: #666666;
  padding: 6px 12px;
`;
