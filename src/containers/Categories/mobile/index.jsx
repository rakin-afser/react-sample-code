import React, {useEffect} from 'react';
import {useHistory, useLocation} from 'react-router-dom';
import {useQuery} from '@apollo/client';
import _ from 'lodash';
import qs from 'qs';

import {
  Wrapper,
  CategoryTitle,
  CategoriesWrapper,
  CategoryImage,
  Category,
  CategoriesTitle,
  BackButton,
  ProductsWrapper,
  ProductsTitle,
  ViewAllButton
} from 'containers/Categories/mobile/styled';
import FadeOnMount from 'components/Transitions';
import Icon from 'components/Icon';
import {getUrlWithoutLastParam} from 'util/heplers';
import ProductGridAsymmetrical from 'components/ProductGridAsymmetrical/mobile';
import Loader from 'components/Loader';
import Error from 'components/Error';
import {PRODUCTS} from 'queries';
import productPlaceholder from 'images/placeholders/product.jpg';

import Bag from 'assets/Categories/Bag';
import Beauty from 'assets/Categories/Beauty';
import Clothing from 'assets/Categories/Clothing';
import Electronics from 'assets/Categories/Electronics';
import Fashion from 'assets/Categories/Fashion';
import Jewelry from 'assets/Categories/Jewelry';
import Shoes from 'assets/Categories/Shoes';

export const icons = [
  {slug: 'clothing', icon: Clothing},
  {slug: 'bags', icon: Bag},
  {slug: 'shoes', icon: Shoes},
  {slug: 'beauty', icon: Beauty},
  {slug: 'electronics', icon: Electronics},
  {slug: 'fashion', icon: Fashion},
  {slug: 'jewelry', icon: Jewelry}
];

const CategoriesMobile = ({
  currentCategory = {},
  currentLevel = null,
  categoryClick = () => {},
  prevCategoryName = ''
}) => {
  const {pathname} = useLocation();
  const history = useHistory();

  // const {data, loading, error} = useQuery(PRODUCTS, {
  //   variables: {
  //     isDealProduct: true,
  //     first: 10,
  //   },
  // });

  const backButtonClick = () => {
    const urlWithoutLastParam = getUrlWithoutLastParam(window.location.pathname);
    history.push(urlWithoutLastParam);
  };

  const viewAllClick = () => {
    const queryString = qs.stringify({viewAll: true});
    history.push(`${pathname}?${queryString}`);
  };

  // if (error) return <Error>{error?.message}</Error>;

  return (
    <Wrapper>
      {currentLevel > 1 && (
        <BackButton onClick={backButtonClick}>
          <Icon type="arrowBack" color="#000" />
        </BackButton>
      )}
      <CategoriesTitle>
        {currentCategory?.name}
      </CategoriesTitle>

      {currentCategory?.slug && <ViewAllButton onClick={viewAllClick}>View All</ViewAllButton>}

      <CategoriesWrapper>
        {currentCategory?.children?.nodes?.map((item) => (
          <FadeOnMount>
            <Category key={`category-${item?.slug}`} onClick={() => categoryClick(item.slug, currentLevel)}>
              <CategoryImage>
                <img src={item?.image?.sourceUrl || currentCategory?.image?.sourceUrl || productPlaceholder} />
              </CategoryImage>
              <CategoryTitle>{item.name}</CategoryTitle>
            </Category>
          </FadeOnMount>
        ))}
      </CategoriesWrapper>
      {/* <AdWrapper>advertisement space</AdWrapper> */}
      {/* {loading ? (
        <Loader wrapperWidth="100%" wrapperHeight="200px" />
      ) : (
        !_.isEmpty(data?.products?.nodes) && (
          <ProductsWrapper>
            <ProductsTitle>Deals</ProductsTitle>
            <ProductGridAsymmetrical onProductClick={(slug) => history.push(`/product/${slug}`)} data={data?.products?.nodes} />
          </ProductsWrapper>
        )
      )} */}
    </Wrapper>
  );
};

export default CategoriesMobile;
