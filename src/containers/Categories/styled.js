import styled from 'styled-components/macro';

export const DotsWrapper = styled.div`
  display: flex;
  z-index: 5;
`;
