import React from 'react';
import {Wrapper, Title, Cards, Card} from './styled';

const Skeleton = ({title, marginTop}) => (
  <Wrapper marginTop={marginTop}>
    <Title>{title}</Title>
    <Cards>
      <Card />
      <Card />
      <Card />
      <Card />
      <Card />
      <Card />
    </Cards>
  </Wrapper>
);

export default Skeleton;
