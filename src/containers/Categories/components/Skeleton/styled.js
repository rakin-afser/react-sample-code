import styled from 'styled-components';
import {mainBlackColor} from 'constants/colors';

export const Wrapper = styled.div`
  max-width: 1170px;
  margin: 0 auto;
`;

export const Title = styled.div`
  font-size: 32px;
  letter-spacing: 0.5px;
  color: ${mainBlackColor};
  font-weight: bold;
  position: relative;
  padding-bottom: 9px;
`;

export const Cards = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  padding: 32px 10px;
  gap: 30px 20px;
`;

export const Card = styled.div`
  width: 260px;
  height: 380px;
  background: #ececec;
`;
