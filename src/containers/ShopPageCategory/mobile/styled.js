import styled from 'styled-components';

export const PageWrap = styled.div`
  width: 100%;
`;

export const ContentContainer = styled.div`
  padding: 0 16px;
`;

export const SortContainer = styled.div`
  margin-top: 19px;
  padding: 0 16px 16px 16px;
  border-bottom: 1px solid #efefefef;
`;

export const CategoryContainer = styled.div`
  margin-top: 4px;
`;

export const SubCategoryContainer = styled.div`
  margin-top: 16px;
`;

export const Products = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 16px -3.5px 0;

  & > div {
    margin: 0 3.5px 16px;
    flex: 0 0 auto;
    width: calc(50% - 7px);
  }
`;
