import React, {useEffect, useState} from 'react';
import {useHistory, useParams} from 'react-router-dom';
import useGlobal from 'store';
import {useSwipeable} from 'react-swipeable';
import {CategoryContainer, ContentContainer, PageWrap, Products, SortContainer, SubCategoryContainer} from './styled';
import CategoriesMobile from '../../../components/ProductCategoriesMobile';
import {allCategoriesMobile, featuredProducts} from '../../../constants/staticData';
import SubCategoryMobile from '../../HomePage/components/SubCategoryMobile';
import CardNewArrival from '../../../components/CardNewArrival';
import FilterProductsPopupMobile from '../../../components/FilterProductsPopupMobile';
import FilterBarMobile from '../../../components/FilterBarMobile';
import {findRecursive, getUrlWithoutLastParam} from 'util/heplers';

const ShopPageCategoryMobile = () => {
  let {shopName, categoryName, subCategoryName, subCategoryName2} = useParams();
  const history = useHistory();
  const [globalState, globalActions] = useGlobal();
  const [categoryShow, setCategoryShow] = useState(true);
  const [categories, setCategories] = useState({
    levelOne: null,
    levelTwo: null,
    levelThree: null
  });

  const setBackButtonLink = () => {
    const urlWithoutLastParam = getUrlWithoutLastParam(window.location.pathname);

    if (globalState.headerBackButtonLink !== urlWithoutLastParam) {
      globalActions.setHeaderBackButtonLink(urlWithoutLastParam);
    }
  };

  const findAllCategories = () => {
    const propertyName = 'slug';
    const nestedFieldName = 'subcategories';

    const levelOne = findRecursive(allCategoriesMobile, categoryName, propertyName, nestedFieldName);
    const levelTwo = findRecursive(allCategoriesMobile, subCategoryName, propertyName, nestedFieldName);
    const levelThree = findRecursive(allCategoriesMobile, subCategoryName2, propertyName, nestedFieldName);

    return {
      levelOne,
      levelTwo,
      levelThree
    };
  };

  const handleTopCategoryClick = (category) => {
    const urlWithoutLastParam = getUrlWithoutLastParam(window.location.pathname);
    history.push(`${urlWithoutLastParam}/${category.slug}`);
  };

  const handleBottomCategoryClick = (category) => {
    const currentUrl = window.location.pathname;
    history.push(`${currentUrl}/${category.slug}`);
  };

  const goToNextCategory = () => {
    const currentCategoryIndex = allCategoriesMobile.findIndex((c) => c.slug === categoryName);
    if (currentCategoryIndex && allCategoriesMobile[currentCategoryIndex + 1]) {
      history.push(`/shop/${shopName}/categories/${allCategoriesMobile[currentCategoryIndex + 1].slug}`);
    }
  };

  const goToPreviousCategory = () => {
    const currentCategoryIndex = allCategoriesMobile.findIndex((c) => c.slug === categoryName);
    if (
      currentCategoryIndex &&
      allCategoriesMobile[currentCategoryIndex - 1] &&
      allCategoriesMobile[currentCategoryIndex - 1].slug !== 'all'
    ) {
      history.push(`/shop/${shopName}/categories/${allCategoriesMobile[currentCategoryIndex - 1].slug}`);
    }
  };

  const getTopCategories = () => {
    const {levelOne, levelTwo, levelThree} = categories;

    return levelThree
      ? levelTwo.subcategories
      : levelTwo
      ? levelOne.subcategories
      : levelOne
      ? [...allCategoriesMobile].splice(1)
      : [];
  };

  const getBottomCategories = () => {
    const {levelOne, levelTwo, levelThree} = categories;
    return levelThree ? [] : levelTwo ? levelTwo.subcategories : levelOne ? levelOne.subcategories : [];
  };

  const handlers = useSwipeable({
    onSwipedDown: (eventData) => {
      setCategoryShow(true);
    },
    onSwipedUp: (eventData) => {
      setCategoryShow(false);
    },
    onSwipedLeft: (eventData) => {
      goToNextCategory();
    },
    onSwipedRight: (eventData) => {
      goToPreviousCategory();
    },
    delta: 10,
    preventDefaultTouchmoveEvent: false,
    trackTouch: true,
    trackMouse: false,
    rotationAngle: 0
  });

  useEffect(() => {
    setBackButtonLink();
    const allCategories = findAllCategories();
    setCategories(allCategories);
  }, [categoryName, subCategoryName, subCategoryName2, globalState.headerBackButtonLink]);

  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, []);

  return (
    <PageWrap>
      <CategoryContainer>
        <CategoriesMobile
          active={
            categories.levelThree
              ? categories.levelThree.slug
              : categories.levelTwo
              ? categories.levelTwo.slug
              : categories.levelOne
              ? categories.levelOne.slug
              : ''
          }
          onClick={handleTopCategoryClick}
          categories={getTopCategories()}
        />
      </CategoryContainer>
      <SubCategoryContainer>
        <SubCategoryMobile categories={getBottomCategories()} onClick={handleBottomCategoryClick} />
      </SubCategoryContainer>
      <SortContainer>
        <FilterBarMobile results="1,247" title="Sort" resultLabel="Items" redDot={false} iconName="chevron" />
      </SortContainer>
      <ContentContainer>
        <Products {...handlers}>
          {featuredProducts.map((product, index) => (
            <CardNewArrival key={index} {...product} />
          ))}
        </Products>
      </ContentContainer>
      <FilterProductsPopupMobile onClick={() => {}} />
    </PageWrap>
  );
};

export default ShopPageCategoryMobile;
