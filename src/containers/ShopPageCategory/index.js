import React, {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {useWindowSize} from '@reach/window-size';

import useGlobal from 'store';
import Layout from 'containers/Layout';
import ShopPageCategoryMobile from './mobile';
import ShopNavigationMenuMobile from 'components/ShopNavigationMenuMobile';

const ShopPageCategory = ({storeData = {}, isOwner, filtersValue = {}}) => {
  const [, globalActions] = useGlobal();
  let {shopName} = useParams();
  const {width} = useWindowSize();

  const isMobile = width <= 767;

  useEffect(() => {
    globalActions.setHeaderBackButtonLink(`/shop/${shopName}/products`);
  }, [shopName]);

  return isMobile ? (
    <Layout
      showBottomNav={false}
      isFooterShort
      hideHeaderLinks={true}
      showBurgerButton={false}
      showBackButton={true}
      showSearchButton={false}
      hideCopyright={true}
      stickyHeader={false}
      title={storeData?.store?.name}
      layOutStyles={{
        paddingTop: 0
      }}
    >
      <ShopNavigationMenuMobile />
      <ShopPageCategoryMobile />
    </Layout>
  ) : (
    'DESKTOP'
  );
};

export default ShopPageCategory;
