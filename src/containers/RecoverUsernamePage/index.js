import React from 'react';
import {useWindowSize} from '@reach/window-size';
import RecoverUsernameComponent from './RecoverUsernameComponent';

const RecoverUsernamePage = ({onClickClose}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return <RecoverUsernameComponent isMobile={isMobile} onClickClose={onClickClose} />;
};

export default RecoverUsernamePage;
