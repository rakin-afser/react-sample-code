import React, {useState} from 'react';
import axios from 'axios';
import {useHistory, useLocation} from 'react-router-dom';
import {Formik} from 'formik';
import {Wrapper} from './styled';
import Navigation2 from '../GuestCheckoutPage/components/mobile/Navigation2';
import InfoText from '../../components/UserAuth/components/InfoText';
import Input from '../../components/Input';
import {validateEmail} from '../../util/validators';
import {Content, FullWidthFormField, ResetForm} from './styled';
import ButtonCustom from '../../components/ButtonCustom';
import FormMessage from '../../components/FormMessage';
import AuthHeading from '../../components/UserAuth/components/Heading';

const RecoverUsernameComponent = ({isMobile, onClickClose}) => {
  const {push} = useHistory();
  const location = useLocation();
  const [errorMessage, setErrorMessage] = useState();
  const [notificationMessage, setNotificationMessage] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [emailValid, setEmailValid] = useState(false);
  const [step, setStep] = useState(1);

  const recoverUsername = async (email) => {
    setIsLoading(true);
    try {
      const res = await axios.get(`${process.env.REACT_APP_testSample_USER_URI}/user/recovery?email=${email}`);
      setStep(2);
      setNotificationMessage({
        type: 'success',
        title: 'Success!',
        text: 'An email with your username has been sent to',
        boldText: email
      });
      setIsLoading(false);
    } catch (error) {
      console.log(error.response);
      if (error.response.data === 'User does not exist.') {
        setErrorMessage('The email address you have entered does not exist');
      } else {
        setErrorMessage(error.response.data);
      }

      setIsLoading(false);
      setNotificationMessage({
        type: 'error',
        title: 'Error!',
        text: 'An email with your username can not be sent. Please try again later.'
      });
    }
  };

  const onClickBack = () => {
    return step === 1
      ? push(location.state && location.state.backUrl ? location.state.backUrl : '/auth/sign-in')
      : step === 2
      ? setStep(1)
      : () => {};
  };

  return (
    <Wrapper>
      {isMobile ? (
        <Navigation2 onClickBack={onClickBack} title="Recover Username" onClickClose={onClickClose} />
      ) : (
        <AuthHeading onClickBack={onClickBack} title="Recover Username" onClickClose={onClickClose} />
      )}

      {step === 1 ? (
        <>
          <InfoText text="Please enter your registered email address or Username associated with this account." />
          <Formik
            initialValues={{
              email: ''
            }}
            validate={(values) => {
              const errors = {};
              const emailValid = validateEmail(values.email);

              if (!emailValid.isValid) {
                errors.email = emailValid.message;
              }

              setEmailValid(emailValid.isValid);
              return errors;
            }}
            onSubmit={(values) => {
              recoverUsername(values.email).then();
            }}
          >
            {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur}) => (
              <ResetForm onSubmit={handleSubmit} noValidate>
                <Content>
                  <FullWidthFormField>
                    <Input
                      required
                      key="email"
                      type="email"
                      name="email"
                      id="email"
                      label="Email"
                      placeholder="Enter your email"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={errorMessage ? errorMessage : errors.email && touched.email ? errors.email : ''}
                      value={values.email}
                      reset={values.email.length > 0}
                      resetCallback={() => setFieldValue('email', '')}
                    />
                  </FullWidthFormField>
                  <ButtonCustom
                    type="submit"
                    isLoading={isLoading}
                    title="Recover"
                    variant="contained"
                    color="mainWhiteColor"
                    disabled={!emailValid}
                    styles={{margin: '32px auto 0 auto'}}
                  />
                </Content>
              </ResetForm>
            )}
          </Formik>
        </>
      ) : step === 2 ? (
        <>
          <FormMessage message={notificationMessage} styles={{marginTop: 24}} />
          <ButtonCustom
            title="Return to Sign in"
            variant="outlined"
            color="primaryColor"
            styles={{margin: '48px auto 0 auto'}}
            onClick={() => push('sign-in')}
          />
        </>
      ) : null}
    </Wrapper>
  );
};

export default RecoverUsernameComponent;
