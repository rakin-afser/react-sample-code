import {gql} from '@apollo/client';

export const STORES_SEARCH = gql`
  query StoresSearch($first: Int, $after: String, $where: StoreSearchWhere) {
    stores(first: $first, after: $after, where: $where) {
      nodes {
        id
        name
        storeUrl
        rating
        gravatar
        followed
        totalFollowers
        banner
        dokanCategory
        categories {
          slug
          name
        }
      }
      pageInfo {
        total
        endCursor
        hasNextPage
      }
    }
  }
`;

export const GET_LISTS = gql`
  query GetLists($first: Int, $after: String, $where: ListSearchWhere) {
    lists(first: $first, after: $after, where: $where) {
      nodes {
        id
        user_id
        list_name
        list_description
        list_id
        is_private
        hashtags
        isFollowed
        totalFollowers
        addedProducts {
          nodes {
            name
            slug
            image {
              altText
              sourceUrl
            }
          }
        }
      }
      pageInfo {
        total
        endCursor
        hasNextPage
      }
    }
  }
`;

export const SEARCH = gql`
  query Search($search: String) {
    products(where: {search: $search}, first: 4) {
      nodes {
        id
        slug
        name
        shippingType
        totalLikes
        image {
          id
          altText
          sourceUrl
        }
      }
    }
    stores(first: 2, where: {search: $search}) {
      nodes {
        id
        storeUrl
        name
        followed
        totalFollowers
        gravatar
      }
    }
    users(where: {search: $search}, first: 2) {
      nodes {
        id
        databaseId
        slug
        username
        totalFollowers
        isFollowed
        avatar {
          url
        }
      }
    }
    lists(first: 2, where: {list_name: $search, is_private: false}) {
      nodes {
        id
        list_name
        list_id
        user_id
        totalFollowers
        isFollowed
      }
    }
    posts(where: {search: $search}, first: 2) {
      nodes {
        id
        databaseId
        slug
        title
        galleryImages(first: 1) {
          nodes {
            id
            sourceUrl(size: THUMBNAIL)
            poster {
              id
              sourceUrl(size: THUMBNAIL)
            }
          }
        }
      }
    }
  }
`;

export const GET_CATEGORIES_BY_TEXT = gql`
  query CategoriesByText($search: String) {
    productCategories(where: {descriptionLike: $search, orderby: COUNT}, first: 5) {
      nodes {
        id
        name
        slug
        products(where: {search: $search}, first: 5) {
          nodes {
            id
          }
        }
      }
    }
  }
`;

export const GET_CATEGORIES_BY_PARENT = gql`
  query GetCategoriesByParent($parentId: Int) {
    productCategories(where: {parent: $parentId, hideEmpty: true}, first: 100) {
      nodes {
        databaseId
        name
        count
        parentDatabaseId
      }
    }
  }
`;
