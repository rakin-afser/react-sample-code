import React, {useState, useEffect} from 'react';
import _ from 'lodash';

import {useLazyQuery} from '@apollo/client';

import {STORES_SEARCH} from 'containers/SearchResult/api/queries';
import usePaginationv2 from 'util/usePaginationv2';

const useStores = ({triggerClearSavedData, shouldUpdateSavedData = true, limit = 12, variables}) => {
  const [savedData, setSavedData] = useState([]);
  const {currentPage, goPrevPage, goNextPage, resetPagination} = usePaginationv2(limit);
  const totalResults = 40; // replace to normal total results when BE will be ready

  const [getStores, {data, loading, error}] = useLazyQuery(STORES_SEARCH, {
    variables: {first: limit, ...variables}
  });
  const storesData = data?.stores?.nodes || [];

  useEffect(() => {
    getStores();
  }, [currentPage]);

  useEffect(() => {
    if (currentPage !== 1) {
      setSavedData([]);
      resetPagination();
      getStores();
    }
  }, [triggerClearSavedData]);

  useEffect(() => {
    if (shouldUpdateSavedData && !loading) {
      if (currentPage === 1) {
        setSavedData([...storesData]);
      } else {
        setSavedData([...savedData, ...storesData]);
      }
    }
  }, [data, loading]);

  return {
    currentData: storesData,
    allData: savedData,
    loading,
    error,
    storesPagination: {
      totalResults,
      currentPage,
      totalPages: Math.ceil(totalResults / limit),
      prevPage: () => {
        if (!loading) {
          goPrevPage();
        }
      },
      nextPage: () => {
        if (!loading) {
          goNextPage(totalResults);
        }
      },
      resetPagination
    }
  };
};

export default useStores;
