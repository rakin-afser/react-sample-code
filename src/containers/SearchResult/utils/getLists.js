import React, {useState, useEffect} from 'react';
import _ from 'lodash';
import {useLazyQuery} from '@apollo/client';

import {GET_LISTS} from 'containers/SearchResult/api/queries';
import usePaginationv2 from 'util/usePaginationv2';

const useLists = ({triggerClearSavedData, shouldUpdateSavedData = true, limit = 12, variables = {}, fetchPolicy}) => {
  const [savedData, setSavedData] = useState([]);
  const {currentPage, goPrevPage, goNextPage, resetPagination} = usePaginationv2(limit);
  const [getLists, {data, loading, error}] = useLazyQuery(GET_LISTS, {
    variables: {limit, offset: currentPage - 1, ...variables},
    fetchPolicy
  });
  const totalResults = data?.lists?.[0]?.total;

  useEffect(() => {
    getLists();
  }, [currentPage]);

  useEffect(() => {
    if (currentPage !== 1) {
      setSavedData([]);
      resetPagination();
      getLists();
    }
  }, [triggerClearSavedData]);

  useEffect(() => {
    if (!_.isEmpty(data?.lists?.[0]?.nodes) && !loading && shouldUpdateSavedData) {
      if (currentPage === 1) {
        setSavedData([...data?.lists?.[0]?.nodes]);
      } else {
        setSavedData([...savedData, ...data?.lists?.[0]?.nodes]);
      }
    }
  }, [data, loading]);

  return {
    currentData: data?.lists?.[0]?.nodes || [],
    allData: savedData || [], // for infinite scroll
    loading,
    error,
    listsPagination: {
      totalResults,
      currentPage,
      totalPages: Math.ceil(totalResults / limit),
      prevPage: () => {
        if (!loading) {
          goPrevPage();
        }
      },
      nextPage: () => {
        if (!loading) {
          goNextPage(totalResults);
        }
      },
      resetPagination
    }
  };
};

export default useLists;
