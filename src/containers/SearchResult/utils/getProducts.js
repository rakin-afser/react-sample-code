import React, {useState, useEffect} from 'react';
import _ from 'lodash';

import {useLazyQuery} from '@apollo/client';
import {PRODUCTS} from 'queries';
import usePagination from 'util/usePagination';

const useProducts = ({triggerClearSavedData, shouldUpdateSavedData = true, limit = 12, variables, fetchPolicy}) => {
  const [savedData, setSavedData] = useState([]);
  const {currentPage, currentCursor, goPrevPage, goNextPage, resetPagination} = usePagination();

  const [getProducts, {data, loading, error}] = useLazyQuery(PRODUCTS, {
    variables: {first: limit * currentPage, cursor: currentCursor, ...variables},
    fetchPolicy
  });

  const productsData = data?.products?.nodes || [];
  const pageInfo = data?.products?.pageInfo || {};
  const totalResults = pageInfo?.total;

  useEffect(() => {
    getProducts();
  }, [currentPage]);

  useEffect(() => {
    if (currentPage !== 1) {
      setSavedData([]);
      resetPagination();
      getProducts();
    }
  }, [triggerClearSavedData]);

  useEffect(() => {
    if (!_.isEmpty(productsData) && shouldUpdateSavedData && !loading) {
      if (currentPage === 1) {
        setSavedData([...productsData]);
      } else {
        setSavedData([...savedData, ...productsData]);
      }
    }
  }, [data, loading]);

  return {
    currentData: productsData,
    allData: savedData,
    loading,
    error,
    productsPagination: {
      totalResults,
      currentPage,
      totalPages: Math.ceil(totalResults / limit),
      prevPage: () => {
        if (!loading) {
          goPrevPage(pageInfo);
        }
      },
      nextPage: () => {
        if (!loading) {
          goNextPage(pageInfo);
        }
      },
      resetPagination
    }
  };
};

export default useProducts;
