import React from 'react';
import qs from 'qs';
import _ from 'lodash';
import {useWindowSize} from '@reach/window-size';
import {useParams, useHistory, useLocation} from 'react-router-dom';

import Layout from 'containers/Layout';
import SearchResultMobile from 'containers/SearchResult/Mobile';
import SearchResultDesktop from './containers/SearchResultDesktop';
// import getProducts from 'containers/SearchResult/utils/getProducts';
// import {getTagsFromString, getTextFromString} from 'util/heplers';

const SearchResult = () => {
  const history = useHistory();
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const {page, param} = useParams();
  const {search, pathname} = useLocation();
  const {search: searchValue, filters: filtersValue} = qs.parse(search.replace('?', '')) || {};

  const tabChange = (pageName, searchString, filters) => {
    if (!pageName) {
      history.push('/search');
    } else {
      const queryString = qs.stringify({search: searchString, filters}); // don't save filters if user searches again
      history.push(`/search/${pageName}?${queryString}`);
    }
  };

  const setFilters = (filters) => {
    const queryString = qs.stringify({filters, search: searchValue}); // save search
    history.push(`${pathname}?${queryString}`);
  };

  return (
    <Layout hideFooter={isMobile} hideHeader={isMobile}>
      {isMobile ? (
        <SearchResultMobile
          tabChange={tabChange}
          search={searchValue}
          filters={filtersValue}
          page={page}
          onClose={() => history.push('/')}
        />
      ) : (
        <SearchResultDesktop
          tabChange={tabChange}
          setFilters={setFilters}
          filters={filtersValue}
          page={page}
          searchValue={searchValue}
        />
      )}
    </Layout>
  );
};

export default SearchResult;
