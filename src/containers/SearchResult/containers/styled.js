import styled from 'styled-components/macro';
import {Tabs} from 'antd';

const rowStyles = `
  max-width: 1230px;
  margin: 0 auto;
  padding: 0 15px;
`;

export const Row = styled.div`
  ${rowStyles}
`;

export const ProductContainer = styled(Row)`
  display: flex;
  justify-content: space-between;
  text-align: center;
`;

export const Title = styled.span`
  font-family: Helvetica Neue, sans-serif;
  font-style: normal;
  font-weight: normal;
  font-size: 14px !important;
  line-height: 140%;
  text-align: right;
  margin-right: 19px;
  color: #000000;
`;

export const TabList = styled.div`
  display: flex;
  padding-bottom: 10px;
`;

export const BreadcrumbsContainer = styled.div`
  margin: 30px 0 30px;
  background: #fff;
  display: flex;
  > div {
    line-height: 14%;
    display: flex;
    margin: 0;
  }
`;

export const GreyColoredDiv = styled.div`
  background: #fafafa;
  z-index: 2;
`;

export const SearchParam = styled.span`
  font-weight: normal;
  font-size: 24px;
  color: #000 !important;
`;

export const Categories = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  margin-top: 40px;
  ${({additionalStyles}) => additionalStyles || null}
`;

export const StyledTabs = styled(Tabs)`
  width: 100%;
  .ant-tabs-top-bar {
    margin-bottom: 19px;
  }
  .ant-tabs-ink-bar {
    background-color: #000;
  }
  .ant-tabs-nav .ant-tabs-tab:active,
  .ant-tabs-nav .ant-tabs-tab:hover,
  .ant-tabs-nav .ant-tabs-tab-active {
    color: #000;
  }
  .ant-tabs-nav-wrap {
    background: #fbfbfb;
    padding-top: 10px;
  }
  .ant-tabs-nav-scroll {
    ${rowStyles}
  }
  .ant-tabs-nav .ant-tabs-tab {
    padding: 10px 0;
    font-weight: bold;
  }
`;

export const OnDemandDelivery = styled.div`
  position: relative;
  align-self: flex-end;

  button {
    &.ant-switch-checked {
      background-color: #398287;
    }
  }
`;

export const PopularTagsTitle = styled.div`
  line-height: 120%;
  color: #343434;
  font-weight: bold;
  font-size: 22px;
  letter-spacing: 0.013em;
  padding-bottom: 12px;
  border-bottom: 2px solid #ed4850;
`;

export const Tags = styled.div`
  display: flex;
  margin-top: 16px;
  flex-wrap: wrap;
`;

export const HashTag = styled.button`
  outline: none;
  margin: 0 16px 16px 0;
  border: 1px solid #c3c3c3;
  box-sizing: border-box;
  border-radius: 4px;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  text-align: center;
  color: #545454;
  padding: 6px 20px;
  background: #fff;
  transition: ease 0.6s;

  &:hover {
    color: #000;
    border-color: #000;
  }
`;

export const StyledCategory = styled.div`
  text-align: left;
  font-size: 16px;
  line-height: 140%;
  letter-spacing: 0;
  color: #000000;
  margin: 13px 0;
`;

export const CategoryQuantity = styled.span`
  color: #656565;
`;

export const Sidebar = styled.div`
  max-width: 270px;
  float: left;
  margin: 7px 30px 0 0;
  h3 {
    font-size: 18px;
    text-align: left;
  }
`;

export const SearchedProducts = styled.div`
  float: right;
  flex-grow: 1;
`;

export const Grid = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(262px, 1fr));
  grid-gap: 20px;
  margin-top: 41px;
`;

export const FiltersWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  padding-bottom: 20px;
`;

export const FilterButton = styled.div`
  cursor: pointer;
  display: flex;
  align-items: baseline;
  background: #0c0c0c;
  border-radius: 24px;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  color: #ffffff;
  padding: 5px 12px;
  height: fit-content;

  & svg {
    height: 13px;
    margin-left: 10px;
  }
`;

export const ClearAll = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #ed494f;
  cursor: pointer;
  flex-grow: 1;
  padding: 5px 12px;
  text-align: right;
`;

export const FilterItem = styled.div`
  font-family: SF Pro Display;
  color: #464646;
  cursor: pointer;
  display: flex;
  align-items: baseline;
  background: #e4e4e4;
  border-radius: 24px;
  font-size: 14px;
  padding: 5px 27px 5px 12px;
  position: relative;
  margin: 20px 0 0 18px;
  display: flex;
  align-items: center;
  text-transform: capitalize;

  & svg {
    width: 18px;
    height: 18px;
    right: 7px;
    position: absolute;
    transition: transform 0.3s;
    &:hover {
      transform: scale(1.2);
    }
  }
`;

export const FiltersBlock = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-grow: 2;
  max-width: 80%;
  margin-top: -20px;
`;

export const Heading = styled.div`
  display: flex;
  & > div {
    padding: 0 16px;
    &:first-child {
      padding-left: 0;
      border-right: 1px solid #ccc;
    }
  }
`;

export const SelectBlock = styled.div`
  width: 100%;
  max-width: 180px;
  margin-right: 20px;
  font-weight: 500;
  align-self: flex-end;

  &:last-child {
    margin-right: 0;
  }

  &&& {
    .ant-select {
      color: #000;
      border-bottom: 1px solid #c3c3c3;

      &-selection {
        margin-right: 10px;
        border: none !important;
        box-shadow: none;
      }

      &-search {
        line-height: unset;
      }

      &-selection__clear {
        margin-right: 7px;
      }

      &-arrow {
        right: 0;
      }

      &-arrow-icon {
        color: #000;
      }
    }
  }
`;

export const TabFiltersBlock = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const FiltersTitle = styled.div`
  font-family: Helvetica, sans-serif;
  font-size: 15px;
  margin-top: 9px;
  flex-grow: 1;
  font-weight: bold;
  letter-spacing: 0.016em;
  color: #000;
`;
