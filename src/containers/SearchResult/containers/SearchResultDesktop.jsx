import React, {useState} from 'react';
import PropTypes from 'prop-types';

import ListHeading from 'components/ListHeading';
import {Row, TabList, GreyColoredDiv, SearchParam, StyledTabs, Heading, PopularTagsTitle} from './styled';
import ProductSearch from './ProductSearch';
import UserSearch from './UserSearch';
import StoresSearch from './StoresSearch';
import PostSearch from './PostSearch';
import ListSearch from './ListSearch';
import PopularTags from 'components/PopularTags';

const headingStyles = `
  padding: 11px 0;
  span {
    font-weight: normal ;
    font-size: 16px ;
    color: #656565;
  }
`;

const {TabPane} = StyledTabs;

const tabs = [
  {page: 'products', text: 'Products', component: ProductSearch},
  {page: 'users', text: 'Users', component: UserSearch},
  {page: 'stores', text: 'Stores', component: StoresSearch},
  {page: 'posts', text: 'Posts', component: PostSearch},
  {page: 'lists', text: 'Lists', component: ListSearch}
];

const SearchResultDesktop = ({
  page = 'products',
  tabChange = () => {},
  paginationData = {},
  searchValue = '',
  setFilters = () => {},
  filters = {},
  loading,
  error
}) => {
  return (
    <>
      <div>
        <GreyColoredDiv>
          <Row>
            <ListHeading
              heading={
                <Heading>
                  <div>
                    Search Results:
                    <SearchParam> {searchValue}</SearchParam>
                  </div>
                  <div>
                    Total Results:
                    <SearchParam> {paginationData?.totalResults}</SearchParam>
                  </div>
                </Heading>
              }
              customStyles={headingStyles}
            />
          </Row>
        </GreyColoredDiv>
        <TabList>
          <StyledTabs
            defaultActiveKey="products"
            activeKey={page}
            onChange={(pageName) => tabChange(pageName, searchValue)}
          >
            {tabs.map((item) => (
              <TabPane tab={item.text} key={item.page}>
                <item.component
                  paginationData={paginationData}
                  searchValue={searchValue}
                  setFilters={setFilters}
                  filters={filters}
                  loading={loading}
                  error={error}
                />
              </TabPane>
            ))}
          </StyledTabs>
        </TabList>
      </div>
      <Row>
        <PopularTagsTitle>Popular Tags</PopularTagsTitle>
        <PopularTags />
      </Row>
    </>
  );
};

SearchResultDesktop.propTypes = {
  page: PropTypes.string,
  search: PropTypes.string,
  tabChange: PropTypes.func,
  paginationData: PropTypes.shape({}),
  searchValue: PropTypes.string,
  setFilters: PropTypes.string,
  filters: PropTypes.shape({}),
  loading: PropTypes.bool,
  error: PropTypes.shape({})
};

SearchResultDesktop.defaultProps = {
  page: 'products',
  search: '',
  tabChange: () => {},
  paginationData: {},
  searchValue: '',
  setFilters: () => {},
  filters: {},
  loading: false,
  error: undefined
};

export default SearchResultDesktop;
