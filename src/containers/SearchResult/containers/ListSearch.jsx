import React, {useEffect} from 'react';

import Filters from 'components/Filters/StoresFilters';
import ListResult from 'components/ListResult';
import Skeleton from '../components/Skeleton';
import {Row, ProductContainer} from './styled';
import NothingFound from 'components/NothingFound';
import {getTagsFromString} from 'util/heplers';
import {useListsLazyQuery} from 'hooks/queries';
import {useOnScreen} from 'hooks';
import Loader from 'components/Loader';

const ListSearch = ({searchValue}) => {
  const [getLists, {data, loading, error, fetchMore, called}] = useListsLazyQuery({
    variables: {
      first: 12,
      where: {
        list_name: searchValue,
        hashtags: getTagsFromString(searchValue),
        is_private: false
      }
    },
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getLists();
    }
  }, [searchValue, getLists]);

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first: 12,
          where: {
            list_name: searchValue,
            hashtags: getTagsFromString(searchValue),
            is_private: false
          },
          after: endCursor
        }
      });
    }
  });

  const {hasNextPage, endCursor} = data?.lists?.pageInfo || {};
  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  function renderLists() {
    if (loading && !data?.lists?.nodes?.length) return <Skeleton isListSection />;

    if (!loading && !data?.lists?.nodes?.length && called) return <NothingFound />;

    return <ListResult error={error} data={data?.lists?.nodes} />;
  }

  return (
    <div>
      <Row>
        <Filters type="lists" title="Filter Lists" />
      </Row>
      <ProductContainer>{renderLists()}</ProductContainer>
      {data?.lists?.nodes?.length && loading ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
      {endRef}
    </div>
  );
};

export default ListSearch;
