import React, {useEffect} from 'react';

// import Filters from 'components/Filters/StoresFilters';
import Result from 'components/PostResult';
import Error from 'components/Error';
import {Row, SelectBlock, TabFiltersBlock, FiltersTitle} from './styled';
import Skeleton from '../components/Skeleton';
import HashtagsSelector from 'containers/SearchResult/components/HashtagsSelector';
import useFilters from 'util/useFilters';
import SelectTrue from 'components/SelectTrue';
import {orderByValues, postsSortByOptions} from 'constants/sorting';
import {useOnScreen} from 'hooks';
import {usePostsSearchLazyQuery} from 'hooks/queries';
import Loader from 'components/Loader';

const postType = [
  {value: 'common', name: 'Common Posts'},
  {value: 'feedback', name: 'Feedback Posts'}
];

const PostSearch = ({searchValue, setFilters, filters}) => {
  const {filters: currentFilters, changeFilters} = useFilters(filters);
  const [getPosts, {data, loading, error, fetchMore}] = usePostsSearchLazyQuery({
    variables: {
      first: 12,
      where: {
        search: searchValue,
        tagSlugAnd: filters?.hashtags?.[0],
        orderby: orderByValues[filters?.sortBy?.[0]] ? {value: orderByValues[filters?.sortBy?.[0]]} : undefined,
        isFeedbackPost: filters?.postType?.[0] ? filters?.postType?.[0] === 'feedback' : undefined
      }
    },
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getPosts();
    }
  }, [searchValue]);

  const {hasNextPage, endCursor} = data?.posts?.pageInfo || {};
  const ref = useOnScreen({
    doAction() {
      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.posts?.nodes || [];
          const newNodes = fetchMoreResult?.posts?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            posts: {
              ...fetchMoreResult?.posts,
              nodes
            }
          };
        },
        variables: {
          first: 12,
          where: {
            search: searchValue,
            tagSlugAnd: filters?.hashtags?.[0],
            orderby: orderByValues[filters?.sortBy?.[0]] ? {value: orderByValues[filters?.sortBy?.[0]]} : undefined,
            isFeedbackPost: filters?.postType?.[0] ? filters?.postType?.[0] === 'feedback' : undefined
          },
          after: endCursor
        }
      });
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  useEffect(() => {
    setFilters(currentFilters);
  }, [currentFilters]);

  if (error) return <Error />;

  return (
    <div>
      <Row>
        <TabFiltersBlock>
          <FiltersTitle>Filter Posts</FiltersTitle>
          {/* <SelectBlock>
            <SelectTrue
              onChange={onSort}
              options={storesSortByOptions}
              valueKey="value"
              value={filters?.category}
              placeholder="Category"
              labelKey="name"
            />
          </SelectBlock> */}

          <SelectBlock>
            <SelectTrue
              allowClear
              onChange={(value) => changeFilters({id: 'postType', unic: true}, value)}
              options={postType}
              valueKey="value"
              value={filters?.postType}
              placeholder="Post Type"
              labelKey="name"
            />
          </SelectBlock>
          <SelectBlock>
            <HashtagsSelector
              onChange={(value) => changeFilters({id: 'hashtags', unic: true}, value)}
              value={filters?.hashtags?.[0]}
            />
          </SelectBlock>
          <SelectBlock>
            <SelectTrue
              allowClear
              onChange={(value) => changeFilters({id: 'sortBy', unic: true}, value)}
              options={postsSortByOptions}
              valueKey="value"
              placeholder="Sort by"
              labelKey="name"
              value={filters?.sortBy}
            />
          </SelectBlock>
        </TabFiltersBlock>
        {/* <Filters type="posts" title="Filter Posts" /> */}
      </Row>
      <Row>
        {!data?.posts?.nodes?.length && loading ? <Skeleton isPostSection /> : <Result data={data?.posts?.nodes} />}
        {data?.posts?.nodes?.length && loading ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
        {endRef}
      </Row>
    </div>
  );
};

export default PostSearch;
