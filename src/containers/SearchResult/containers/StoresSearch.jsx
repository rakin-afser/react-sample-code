import React, {useEffect} from 'react';

import StoresResult from 'components/StoresResult';
import Error from 'components/Error';
import {Row, ProductContainer, SelectBlock, TabFiltersBlock, FiltersTitle} from './styled';
import SelectTrue from 'components/SelectTrue';
import Skeleton from '../components/Skeleton';
import NothingFound from 'components/NothingFound';
import {storesSortByOptions} from 'constants/sorting';
import useFilters from 'util/useFilters';
import HashtagsSelector from 'containers/SearchResult/components/HashtagsSelector';
import {useStoresSearchLazyQuery} from 'hooks/queries';
import {useOnScreen} from 'hooks';
import Loader from 'components/Loader';

const StoresSearch = ({searchValue, setFilters, filters}) => {
  const {changeFilters} = useFilters(filters);
  const [getStore, {data, loading, error, fetchMore, called}] = useStoresSearchLazyQuery({
    variables: {
      first: 12,
      where: {
        search: searchValue,
        store_tag: filters?.hashtags?.[0],
        sortBy: filters?.sortBy?.[0]
      }
    },
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getStore();
    }
  }, [searchValue]);

  const {stores} = data || {};
  const {hasNextPage, endCursor} = stores?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first: 12,
          where: {
            search: searchValue,
            store_tag: filters?.hashtags?.[0],
            sortBy: filters?.sortBy?.[0]
          },
          after: endCursor
        }
      });
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  // useEffect(() => {
  //   setFilters(currentFilters);
  // }, [currentFilters]);

  function renderStores() {
    if (loading && !stores?.nodes?.length) return <Skeleton isStoreSection />;
    if (error) return <Error />;
    if (!stores?.nodes?.length && called) return <NothingFound />;
    return <StoresResult data={stores?.nodes} />;
  }

  return (
    <div>
      <Row>
        <TabFiltersBlock>
          <FiltersTitle>Filter Stores</FiltersTitle>
          {/* <SelectBlock>
            <SelectTrue
              onChange={onSort}
              options={storesSortByOptions}
              valueKey="value"
              value={filters?.category}
              placeholder="Category"
              labelKey="name"
            />
          </SelectBlock> */}

          {/* <SelectBlock>
            <SelectTrue
              onChange={onSort}
              options={preferences}
              valueKey="value"
              value={filters?.otherPreferences}
              placeholder="Other preferences"
              labelKey="name"
            />
          </SelectBlock> */}
          <SelectBlock>
            <HashtagsSelector
              onChange={(value) => changeFilters({id: 'hashtags', unic: true}, value)}
              value={filters?.hashtags?.[0]}
            />
          </SelectBlock>
          <SelectBlock>
            <SelectTrue
              allowClear
              onChange={(value) => changeFilters({id: 'sortBy', unic: true}, value)}
              options={storesSortByOptions}
              valueKey="value"
              placeholder="Sort by"
              labelKey="name"
              value={filters?.sortBy}
            />
          </SelectBlock>
        </TabFiltersBlock>
      </Row>
      <ProductContainer>{renderStores()}</ProductContainer>
      {loading ? <Loader dotsSize="10px" wrapperHeight="auto" wrapperWidth="100%" /> : null}
      {endRef}
    </div>
  );
};

export default StoresSearch;
