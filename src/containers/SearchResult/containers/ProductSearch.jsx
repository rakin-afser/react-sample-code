import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import qs from 'qs';
import {useLocation, useHistory} from 'react-router-dom';

import useGlobal from 'store';
import Icon from 'components/Icon';
import Pagination from 'components/PagePagination';
import Filters from 'components/Filters/ProductFilter';
import Product from 'components/Cards/Product';
import NothingFound from 'components/NothingFound';
import Error from 'components/Error';

import {
  ProductContainer,
  StyledCategory,
  CategoryQuantity,
  SearchedProducts,
  Grid,
  FiltersWrapper,
  FilterButton,
  ClearAll,
  FilterItem,
  FiltersBlock
} from 'containers/SearchResult/containers/styled';
import getAttribute from 'components/Modals/mobile/Filters/utils/getAttribute';
import Skeleton from '../components/Skeleton';
import useFilters from 'util/useFilters';
import {useOnScreen} from 'hooks';
import Loader from 'components/Loader';
import {getAttributesFilter, getTagsFromString, getTextFromString} from 'util/heplers';
import {useProductsSearchLazyQuery} from 'hooks/queries';

const SelectedFilterItem = () => {
  const history = useHistory();
  const {pathname, search} = useLocation();
  const {search: searchValue, filters: filtersValue} = qs.parse(search.replace('?', '')) || {};
  const {clearFilterItem, reloadFilters} = useFilters(filtersValue);

  const {data} = getAttribute(filtersValue) || {};

  useEffect(() => {
    reloadFilters(filtersValue);
  }, [search]);

  const onClear = (filterId, itemId) => {
    const currentFilters = clearFilterItem(filterId, itemId);
    setTimeout(() => {
      const queryString = qs.stringify({filters: currentFilters, search: searchValue}); // save search
      history.push(`${pathname}?${queryString}`);
    });
  };

  return Object.keys(filtersValue || {})?.map((item) => {
    const currentAttribute = data.find((i) => i?.slug === item) || {};

    return currentAttribute?.options
      ?.filter((i) => filtersValue?.[item].includes(i.slug))
      ?.map((el, index) => (
        <FilterItem key={index}>
          {el?.name} <Icon onClick={() => onClear(item, el?.slug)} type="close" color="#464646" />
        </FilterItem>
      ));
  });
};

const ProductSearch = ({productsData = []}) => {
  const [, setGlobalState] = useGlobal();
  const history = useHistory();
  const {search, pathname} = useLocation();
  const {search: searchValue, filters: filtersValue} = qs.parse(search.replace('?', '')) || {};

  const where = {
    search: getTextFromString(searchValue),
    minPrice: Number(filtersValue?.priceValue?.[0]) || undefined,
    maxPrice: Number(filtersValue?.priceValue?.[1]) || undefined,
    tagIn: getTagsFromString(searchValue),
    categoryIn: _.isEmpty(filtersValue?.subCategory) ? filtersValue?.categories : filtersValue?.subCategory,
    brands: filtersValue?.brands,
    onSale: filtersValue?.offers?.includes('sale'),
    shippingType: filtersValue?.offers?.includes('free') ? 'free_shipping' : undefined,
    taxonomyFilter: getAttributesFilter(filtersValue || {}),
    shopcoins_available: filtersValue?.productsWithShopcoins?.includes('true') ? true : undefined
  };

  const [getProducts, {data, loading, error, fetchMore, called}] = useProductsSearchLazyQuery({
    variables: {first: 12, where: {search: searchValue, ...where}},
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getProducts();
    }
  }, [searchValue]);

  const {products} = data || {};
  const {hasNextPage, endCursor} = data?.products?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({variables: {first: 12, where: {search: searchValue, ...where}, after: endCursor}});
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  const switchRenderComponent = () => {
    if (loading && !products?.nodes?.length) return <Skeleton />;
    if (!products?.nodes?.length && called) return <NothingFound />;
    if (error) return <Error />;
    return (
      <Grid>
        {data?.products?.nodes?.map((item, i) => (
          <Product key={item?.id} index={i} content={item} margin="0 0 48px 0" />
        ))}
      </Grid>
    );
  };

  const filterClick = () => {
    setGlobalState.setFilterModal(true, {productsData});
  };

  const resetFilters = () => {
    const queryString = qs.stringify({search: searchValue});
    history.push(`${pathname}?${queryString}`);
  };

  return (
    <div>
      <ProductContainer>
        <SearchedProducts>
          <FiltersWrapper>
            <FilterButton onClick={filterClick}>
              Filters <Icon type="filter" color="#fff" />
            </FilterButton>
            <FiltersBlock>
              <SelectedFilterItem />
            </FiltersBlock>
            <ClearAll onClick={resetFilters}>Clear All</ClearAll>
          </FiltersWrapper>
          {switchRenderComponent()}
          {endRef}
          {loading ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
          {/* {!(_.isEmpty(productsData) && currentPage === 1) && (
            <Pagination
              totalPages={totalPages}
              prevPage={prevPage}
              nextPage={nextPage}
              currentPage={currentPage}
            />
          )} */}
        </SearchedProducts>
      </ProductContainer>
    </div>
  );
};

export default ProductSearch;
