import React from 'react';
import Btn from 'components/Btn';
import {useFollowUnfollowUserMutation} from 'hooks/mutations';

const FollowBtn = (props) => {
  const {databaseId, id, isFollowed} = props;
  const [followUnfollow] = useFollowUnfollowUserMutation({id, isFollowed});

  return (
    <Btn
      kind={isFollowed ? 'following' : 'follow'}
      ml="auto"
      onClick={() => followUnfollow({variables: {input: {id: databaseId}}})}
    />
  );
};

export default FollowBtn;
