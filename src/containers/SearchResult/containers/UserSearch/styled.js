import {Link} from 'react-router-dom';
import styled, {css} from 'styled-components';

export const Container = styled.div`
  max-width: 700px;
  margin: 0 auto;
  padding: 0 15px;
`;

export const User = styled.div`
  display: flex;
  align-items: center;

  & + & {
    margin-top: 16px;
  }
`;

export const UserPic = styled.img`
  width: 60px;
  height: 60px;
  flex-shrink: 0;
  border-radius: 50%;
  object-fit: cover;
`;

export const UserDesc = styled.div`
  margin-left: 20px;
`;

export const UserName = styled.span`
  display: block;
  font-size: 14px;
  line-height: 20px;
  color: #000000;
`;

export const UserText = styled.div`
  font-size: 12px;
  color: #7a7a7a;
`;
