import React, {useEffect} from 'react';
import qs from 'qs';
import {useLocation} from 'react-router';
import {useUsersSearchLazyQuery} from 'hooks/queries';
import {Container, User, UserPic, UserDesc, UserName, UserText} from './styled';
import {useOnScreen} from 'hooks';
import Error from 'components/Error';
import NothingFound from 'components/NothingFound';
import Loader from 'components/Loader';
import FollowBtn from './components/FollowBtn';
import userPlaceholder from 'images/placeholders/user.jpg';
import {Link} from 'react-router-dom';

const UserSearch = () => {
  const {search} = useLocation();
  const {search: searchValue} = qs.parse(search.replace('?', '')) || {};
  const [getUsers, {data, loading, error, fetchMore, called}] = useUsersSearchLazyQuery({
    variables: {first: 10, where: {search: searchValue}},
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getUsers();
    }
  }, [searchValue, getUsers]);

  const {users} = data || {};

  const {hasNextPage, endCursor} = data?.users?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({variables: {first: 10, where: {search: searchValue}, after: endCursor}});
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  function renderContent() {
    if (!users?.nodes?.length && loading) return <Loader wrapperWidth="100%" wrapperHeight="auto" />;
    if (!users?.nodes?.length && called) return <NothingFound />;
    if (error) return <Error />;
    return users?.nodes?.map((user) => (
      <User key={user?.id}>
        <Link to={`/profile/${user?.databaseId}/posts`}>
          <UserPic src={user?.avatar?.url || userPlaceholder} alt={user?.nickname} />
        </Link>
        <UserDesc>
          <Link to={`/profile/${user?.databaseId}/posts`}>
            <UserName>{user?.username}</UserName>
          </Link>
          <UserText>{user?.totalFollowers} followers</UserText>
        </UserDesc>
        <FollowBtn {...user} />
      </User>
    ));
  }

  return (
    <Container>
      {renderContent()}
      {endRef}
      {users?.nodes?.length && loading ? <Loader wrapperWidth="100%" wrapperHeight="auto" /> : null}
    </Container>
  );
};

export default UserSearch;
