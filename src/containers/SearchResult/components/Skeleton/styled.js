import styled from 'styled-components';
import media from 'constants/media';

export const Wrapper = styled.div`
  width: 100%;
  max-width: 1170px;
  margin: 0 auto;
`;

export const Cards = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  padding: 32px 10px;
  gap: 30px 20px;

  @media (max-width: ${media.mobileMax}) {
    flex-direction: row;
    padding: 0;
    gap: 10px;
  }
`;

export const Card = styled.div`
  width: ${({isStoreSection}) => {
    if (isStoreSection) return '270px';
    return '260px';
  }};

  height: ${({isStoreSection, isListSection}) => {
    if (isListSection) return '210px';
    if (isStoreSection) return '230px';
    return '380px';
  }};

  @media (max-width: ${media.mobileMax}) {
    width: 48%;
    flex-basis: 48%;
    height: auto;

    ${({isPostSection, isListSection}) => {
      if (isPostSection || isListSection) {
        return 'width: 100%; flex-basis: 100%';
      }

      return null;
    }}
`;
