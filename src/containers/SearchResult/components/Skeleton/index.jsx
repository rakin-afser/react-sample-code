import React from 'react';
import PropTypes from 'prop-types';
import ProductSkeleton from './components/ProductSkeleton';
import StoreSkeleton from './components/StoreSkeleton';
import PostsSkeleton from './components/PostSkeleton';
import ListsSkeleton from './components/ListsSkeleton';
import {Wrapper, Cards, Card} from './styled';

const Skeleton = ({isStoreSection, isPostSection, isListSection}) => {
  const renderSkeleton = (count) => {
    const renderByType = () => {
      if (isStoreSection) {
        return <StoreSkeleton />;
      }

      if (isPostSection) {
        return <PostsSkeleton />;
      }

      if (isListSection) {
        return <ListsSkeleton />;
      }

      return <ProductSkeleton />;
    };

    return (
      <>
        {Array.from(Array(count).keys()).map((elm, idx) => (
          <Card key={idx} isStoreSection={isStoreSection} isListSection={isListSection} isPostSection={isPostSection}>
            {renderByType()}
          </Card>
        ))}
      </>
    );
  };

  return (
    <Wrapper>
      <Cards>{renderSkeleton(12)}</Cards>
    </Wrapper>
  );
};

Skeleton.defaultProps = {
  isStoreSection: false,
  isPostSection: false,
  isListSection: false
};

Skeleton.propTypes = {
  isStoreSection: PropTypes.bool,
  isPostSection: PropTypes.bool,
  isListSection: PropTypes.bool
};

export default Skeleton;
