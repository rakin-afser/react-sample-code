import React from 'react';
import {useWindowSize} from '@reach/window-size';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';

const ListsSkeleton = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  if (isMobile) {
    return (
      <SkeletonTheme>
        <div style={{display: 'flex', alignItems: 'center'}}>
          <div style={{marginRight: '15px'}}>
            <Skeleton width={80} height={80} />
          </div>
          <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
            <Skeleton width={220} height={20} />
            <Skeleton width={220} height={20} />
            <Skeleton width={220} height={20} />
          </div>
        </div>
      </SkeletonTheme>
    );
  }

  return (
    <SkeletonTheme>
      <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <Skeleton circle width={40} height={40} />
        <Skeleton width={80} height={30} />
        <Skeleton width={80} height={30} />
      </div>
      <Skeleton height={30} />
      <div style={{display: 'flex', justifyContent: 'space-between'}}>
        <Skeleton height={80} width={80} />
        <Skeleton height={80} width={80} />
        <Skeleton height={80} width={80} />
      </div>
      <Skeleton height={30} />
    </SkeletonTheme>
  );
};

export default ListsSkeleton;
