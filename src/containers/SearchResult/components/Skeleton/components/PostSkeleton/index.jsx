import React from 'react';
import {useWindowSize} from '@reach/window-size';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';

const PostsSkeleton = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  if (isMobile) {
    return (
      <SkeletonTheme>
        <div style={{display: 'flex', alignItems: 'center'}}>
          <div style={{marginRight: 15}}>
            <Skeleton width={50} height={50} />
          </div>
          <div>
            <div>
              <Skeleton height={20} width={200} />
            </div>
            <div>
              <Skeleton height={20} width={200} />
            </div>
          </div>
          <div style={{marginLeft: 'auto'}}>
            <Skeleton width={20} height={20} />
          </div>
        </div>
      </SkeletonTheme>
    );
  }

  return (
    <SkeletonTheme>
      <div style={{display: 'flex', justifyContent: 'space-between'}}>
        <div>
          <Skeleton circle width={50} height={50} />
        </div>
        <div>
          <div>
            <Skeleton height={20} width={80} />
          </div>
          <div>
            <Skeleton height={20} width={80} />
          </div>
        </div>
        <div>
          <Skeleton height={20} width={80} />
        </div>
      </div>
      <Skeleton height={200} />
      <Skeleton height={30} />
    </SkeletonTheme>
  );
};

export default PostsSkeleton;
