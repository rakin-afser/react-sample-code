import React from 'react';
import {useWindowSize} from '@reach/window-size';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';

const StoreSkeleton = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  if (isMobile)
    return (
      <SkeletonTheme>
        <Skeleton height={100} />
        <Skeleton count={3} />
      </SkeletonTheme>
    );

  return (
    <SkeletonTheme>
      <Skeleton height={60} />
      <p style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
        <Skeleton circle width={50} height={50} />
        <Skeleton width={90} height={20} />
      </p>
      <p>
        <Skeleton count={2} />
      </p>
    </SkeletonTheme>
  );
};

export default StoreSkeleton;
