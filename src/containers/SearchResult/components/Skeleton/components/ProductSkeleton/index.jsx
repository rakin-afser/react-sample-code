import React from 'react';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';
import {useWindowSize} from '@reach/window-size';

const ProductSkeleton = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return (
    <SkeletonTheme>
      <div>
        <Skeleton height={isMobile ? 150 : 250} />
        <p>
          <Skeleton height={20} count={3} />
        </p>
      </div>
    </SkeletonTheme>
  );
};

export default ProductSkeleton;
