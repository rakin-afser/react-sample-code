import React, {useState} from 'react';
import {useQuery} from '@apollo/client';

import {GET_TAGS} from 'components/CreatePost/api/queries';
import SelectTrue from 'components/SelectTrue';

const HashtagsSelector = ({value, onChange}) => {
  const [search, setSearch] = useState('');
  const {data: tagsData, loading: tagsLoading, error: tagsError} = useQuery(GET_TAGS, {variables: {search}}); // todo: add pagination
  const tagsOptions = tagsData?.tags?.nodes?.map((item) => ({name: item?.name, value: item?.slug})) || [];

  const onSearch = (text) => {
    setSearch(text);
  };

  return (
    <SelectTrue
      mode="tags"
      allowClear
      onSearch={onSearch}
      onChange={onChange}
      options={tagsOptions}
      valueKey="value"
      placeholder="Hashtags"
      value={value}
      labelKey="name"
      showSearch
    />
  );
};

export default HashtagsSelector;
