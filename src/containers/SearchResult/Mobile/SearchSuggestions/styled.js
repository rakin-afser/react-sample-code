import {Link} from 'react-router-dom';
import styled, {css} from 'styled-components/macro';

export const SuggestionsWrapper = styled.div`
  background: #fff;
  padding: 64px 0 16px 0;
  transition: 0.5s;

  ${({hideHeader}) =>
    hideHeader &&
    `
    padding-top: 42px;
  `}

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    direction: rtl;
  `}
`;

export const RecentHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 16px;
`;

export const RecentTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  color: #000000;
`;

export const Action = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #ed484f;
`;

export const SearchItemWrapper = styled.div`
  margin-bottom: 9px;
`;

export const SuggestionsBlock = styled.div`
  margin: 20px 16px 0;
`;

export const BlockHeader = styled(RecentHeader)`
  padding: 0;
  border-bottom: 1px solid #c3c3c3;
  padding-bottom: 12px;

  ${({noBorder}) =>
    noBorder &&
    `
    border-bottom: none;
  `}
`;

export const BlockContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;

  ${({products}) =>
    products &&
    css`
      justify-content: space-between;
    `}
`;

export const TagButton = styled.button`
  background: #f2f2f2;
  border-radius: 22px;
  padding: 8px 21px;
  font-family: SF Pro Display;
  font-weight: 600;
  font-size: 12px;
  color: #000000;
  margin: 15px 10px 0 0;
  line-height: 1;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin: 15px 0 0 10px;
  `}
`;

export const ItemImage = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  object-fit: cover;
  flex-shrink: 0;
  margin-right: 16px;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin: 0 0 0 16px;
  `}
`;

export const ItemTitle = styled.span`
  font-size: 14px;
  color: #000000;
  white-space: nowrap;
  max-width: 100%;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  display: inline-block;
`;

export const ProductItem = styled.div`
  display: flex;
  align-items: center;
  width: calc(50% - 15px);
  min-width: 165px;
  margin: 15px 10px 0 0;

  &:nth-child(2n) {
    margin-right: 0;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin: 15px 0 0 10px;

    &:nth-child(2n) {
      margin-left: 0;
    }
  `}
`;

export const StoresItem = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  margin-top: 15px;
`;

export const ItemDescription = styled.div`
  flex-grow: 1;
  margin-right: 10px;
  max-width: 65%;

  ${({theme: {isArabic}}) =>
    isArabic &&
    `
    margin: 0 0 0 10px;
  `}

  ${({products}) =>
    products &&
    css`
      max-width: calc(100% - 52px);
    `}
`;

export const ItemText = styled.div`
  font-size: 12px;
  color: #999;
  line-height: 1;
  white-space: nowrap;
  max-width: 100%;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  display: inherit;
`;

export const ViewButton = styled(Link)`
  font-size: 14px;
  font-weight: 700;
  padding: 3px 11px 3px 11px;
  color: #545454;
  margin-left: auto;
`;

export const StyledLink = styled(Link)`
  display: inline-block;
  max-width: 100%;
`;

export const SearchItemEmpty = styled.div`
  font-size: 14px;
  color: #999;
  padding: 16px 16px 0;
`;
