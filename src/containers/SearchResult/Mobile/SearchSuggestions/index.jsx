import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {useTranslation} from 'react-i18next';
import _ from 'lodash';
import {useLazyQuery} from '@apollo/client';
import SearchItem from 'containers/SearchResult/Mobile/SearchItem';
import {
  RecentHeader,
  RecentTitle,
  Action,
  SuggestionsWrapper,
  SearchItemWrapper,
  SuggestionsBlock,
  BlockHeader,
  BlockContent,
  TagButton,
  ItemImage,
  ItemTitle,
  ProductItem,
  ItemDescription,
  StoresItem,
  ItemText,
  StyledLink,
  ViewButton,
  SearchItemEmpty
} from 'containers/SearchResult/Mobile/SearchSuggestions/styled';
import PopularTags from 'components/PopularTags';
import {SEARCH} from 'containers/SearchResult/api/queries';
import productPlaceholder from 'images/placeholders/product.jpg';
import userPlaceholder from 'images/placeholders/user.jpg';
import {useUser} from 'hooks/reactiveVars';
import Loader from 'components/Loader';
import {FollowBtnUser, FollowBtnStore, FollowBtnList} from './components/FollowButtons';

const Products = ({data}) => {
  const {t} = useTranslation();
  function renderProdShortInfo(p) {
    const {shippingType, totalLikes} = p || {};
    if (shippingType) return <ItemText product>{shippingType}</ItemText>;
    if (totalLikes) {
      return <ItemText product>{`${totalLikes} Like${totalLikes?.toString()?.endsWith('1') ? '' : 's'}`}</ItemText>;
    }
    return null;
  }

  if (_.isEmpty(data)) return null;

  return (
    <SuggestionsBlock>
      <BlockHeader>
        <RecentTitle>{t('general.products')}</RecentTitle>
        <Link to="/search/products/all">
          <Action>{t('general.viewAll')}</Action>
        </Link>
      </BlockHeader>
      <BlockContent products>
        {data?.map((item) => (
          <ProductItem key={item?.id}>
            <Link to={`/product/${item?.slug}`}>
              <ItemImage src={item?.image?.sourceUrl || productPlaceholder} />
            </Link>
            <ItemDescription products>
              <StyledLink to={`/product/${item?.slug}`}>
                <ItemTitle>{item?.name}</ItemTitle>
              </StyledLink>
              {renderProdShortInfo(item)}
            </ItemDescription>
          </ProductItem>
        ))}
      </BlockContent>
    </SuggestionsBlock>
  );
};

const Stores = ({data}) => {
  const {t} = useTranslation();

  if (_.isEmpty(data)) return null;

  return (
    <SuggestionsBlock>
      <BlockHeader>
        <RecentTitle>{t('general.stores')}</RecentTitle>
        <Link to="/search/stores/all">
          <Action>{t('general.viewAll')}</Action>
        </Link>
      </BlockHeader>
      <BlockContent products>
        {data?.map((item) => (
          <StoresItem key={item?.id}>
            <Link to={`/shop/${item?.storeUrl}/products`}>
              <ItemImage src={item?.gravatar || userPlaceholder} />
            </Link>
            <ItemDescription>
              <StyledLink to={`/shop/${item?.storeUrl}/products`}>
                <ItemTitle>{item.name}</ItemTitle>
              </StyledLink>
              <ItemText>{item.totalFollowers} followers</ItemText>
            </ItemDescription>
            <FollowBtnStore {...item} />
          </StoresItem>
        ))}
      </BlockContent>
    </SuggestionsBlock>
  );
};

const Users = ({data}) => {
  const {t} = useTranslation();

  if (_.isEmpty(data)) return null;

  return (
    <SuggestionsBlock>
      <BlockHeader>
        <RecentTitle>Users</RecentTitle>
        <Link to="/search/users/all">
          <Action>{t('general.viewAll')}</Action>
        </Link>
      </BlockHeader>
      <BlockContent products>
        {data?.map((item) => (
          <StoresItem key={item?.id}>
            <Link to={`/profile/${item?.databaseId}/posts`}>
              <ItemImage src={item?.avatar?.url || userPlaceholder} />
            </Link>
            <ItemDescription>
              <StyledLink to={`/profile/${item?.databaseId}/posts`}>
                <ItemTitle>{item.username}</ItemTitle>
              </StyledLink>
              <ItemText>
                {item.totalFollowers} followers {item?.isFollowed ? '| Following' : null}
              </ItemText>
            </ItemDescription>
            <FollowBtnUser {...item} />
          </StoresItem>
        ))}
      </BlockContent>
    </SuggestionsBlock>
  );
};

const Posts = ({data}) => {
  const {t} = useTranslation();

  if (_.isEmpty(data)) return null;

  return (
    <SuggestionsBlock>
      <BlockHeader>
        <RecentTitle>{t('myProfilePage.posts')}</RecentTitle>
        <Link to="/search/posts/all">
          <Action>{t('general.viewAll')}</Action>
        </Link>
      </BlockHeader>
      <BlockContent products>
        {data?.map((item) => (
          <StoresItem key={item?.id}>
            <Link to={`/post/${item?.databaseId}`}>
              <ItemImage
                src={
                  item?.galleryImages?.nodes?.[0]?.sourceUrl ||
                  item?.galleryImages?.nodes?.[0]?.poster?.sourceUrl ||
                  productPlaceholder
                }
                alt={item?.slug}
              />
            </Link>

            <ItemDescription>
              <StyledLink to={`/post/${item?.databaseId}`}>
                <ItemTitle>{item.title}</ItemTitle>
              </StyledLink>
            </ItemDescription>
            <ViewButton to={`/post/${item?.databaseId}`}>{t('general.view')}</ViewButton>
          </StoresItem>
        ))}
      </BlockContent>
    </SuggestionsBlock>
  );
};

const Lists = ({data}) => {
  const {t} = useTranslation();

  if (_.isEmpty(data)) return null;

  return (
    <SuggestionsBlock>
      <BlockHeader>
        <RecentTitle>{t('myProfilePage.lists')}</RecentTitle>
        <Link to="/search/lists/all">
          <Action onClick={() => {}}>{t('general.viewAll')}</Action>
        </Link>
      </BlockHeader>
      <BlockContent products>
        {data?.map((item) => (
          <StoresItem key={item?.id}>
            <Link to={`/profile/${item?.user_id}/lists/${item?.list_id}`}>
              <ItemImage src={productPlaceholder} alt={item?.list_name} />
            </Link>
            <ItemDescription>
              <StyledLink to={`/profile/${item?.user_id}/lists/${item?.list_id}`}>
                <ItemTitle>{item?.list_name}</ItemTitle>
              </StyledLink>
              <ItemText>{item?.totalFollowers} followers</ItemText>
            </ItemDescription>
            <FollowBtnList {...item} />
          </StoresItem>
        ))}
      </BlockContent>
    </SuggestionsBlock>
  );
};

const SearchSuggestions = ({data = [], search, onClear = () => {}, onClick = () => {}}) => {
  const [user] = useUser();
  const {t} = useTranslation();

  const [getSearchResults, {data: searchData, loading}] = useLazyQuery(SEARCH);

  useEffect(() => {
    if (search) {
      getSearchResults({variables: {search}});
    }
  }, [search, getSearchResults]);

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="215px" />;

  return (
    <SuggestionsWrapper hideHeader={!!search?.length}>
      {!search?.length && user?.email ? (
        <RecentHeader>
          <RecentTitle>{t('searchPage.recent')}</RecentTitle>
          <Action onClick={onClear}>{t('general.clearAll')}</Action>
        </RecentHeader>
      ) : null}
      {!_.isEmpty(data) && (
        <SearchItemWrapper>
          {data?.slice(-5)?.map((item, index) => (
            <SearchItem
              key={index}
              onClick={() =>
                onClick(item?.type, item?.text, false, {name: item?.categoryName, slug: item?.categorySlug})
              }
              title={item.text}
              category={item?.type === 'products' && item?.categoryName ? item?.categoryName : _.capitalize(item?.type)}
            />
          ))}
        </SearchItemWrapper>
      )}
      {_.isEmpty(data) && !search?.length && user?.email && (
        <SearchItemEmpty>You don't have any search history yet</SearchItemEmpty>
      )}
      {search?.length ? null : (
        <SuggestionsBlock>
          <BlockHeader noBorder>
            <RecentTitle>{t('searchPage.trendingNow')}</RecentTitle>
          </BlockHeader>
          <BlockContent>
            <PopularTags filled />
          </BlockContent>
        </SuggestionsBlock>
      )}
      {searchData?.products?.nodes?.length ? <Products data={searchData?.products?.nodes} /> : null}
      {searchData?.stores?.nodes?.length ? <Stores data={searchData?.stores?.nodes} /> : null}
      {searchData?.users?.nodes?.length ? <Users data={searchData?.users?.nodes} /> : null}
      {searchData?.posts?.nodes?.length ? <Posts data={searchData?.posts?.nodes} /> : null}
      {searchData?.lists?.nodes?.length ? <Lists data={searchData?.lists?.nodes} /> : null}
    </SuggestionsWrapper>
  );
};

SearchSuggestions.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  onClick: PropTypes.func,
  onClear: PropTypes.func
};

export default SearchSuggestions;
