import React, {useState, useEffect, useRef} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {useLocation, useParams} from 'react-router';
import {useLazyQuery} from '@apollo/client';

import useGlobal from 'store';
import SearchSuggestions from 'containers/SearchResult/Mobile/SearchSuggestions';
import SearchHeader from 'containers/SearchResult/Mobile/Header';
import {productFilters, storeFilters, postFilters, listFilters} from 'constants/filters';
import SearchResults from './SearchResults';
import {Wrapper, Tabs, TabItem} from './styled';
import useRecentSearches from 'util/useRecentSearches';
import {GET_CATEGORIES_BY_TEXT} from 'containers/SearchResult/api/queries';
import {useStateDebounce} from 'hooks';

const tabs = [
  {page: 'all', text: 'All'},
  {page: 'users', text: 'Users'},
  {page: 'products', text: 'Products'},
  {page: 'stores', text: 'Stores'},
  {page: 'posts', text: 'Posts'},
  {page: 'lists', text: 'Lists'}
];

const SearchPage = ({onClose, page, search = '', tabChange}) => {
  const {search: searchString} = useLocation();
  const {param} = useParams();
  const searchInputRef = useRef(null);
  const [globalState, setGlobalState] = useGlobal();
  const [shownTabs, setShownTabs] = useState(true);
  const {recentSearches, saveSearch, clearRecentSearches} = useRecentSearches();
  const [searchList, setSearchList] = useState([]);
  const [searchValue, searchValueDebounced, setSearchValue] = useStateDebounce(search);

  const [getCategoriesByText, {data: categoriesData, loading: categoriesLoading}] = useLazyQuery(
    GET_CATEGORIES_BY_TEXT
  );

  useEffect(() => {
    if (searchValueDebounced) {
      getCategoriesByText({variables: {search: searchValueDebounced}});
    }
  }, [searchValueDebounced, getCategoriesByText]);

  useEffect(() => {
    if (!searchValue && param !== 'all') return;
    if (categoriesLoading) {
      setSearchList(
        searchList?.map((item) => ({
          text: searchValue,
          type: 'products',
          categoryName: item?.name,
          categorySlug: item?.slug
        })) || []
      );
    } else {
      const filteredCategories = categoriesData?.productCategories?.nodes?.filter(
        (c) => c?.products?.nodes?.length > 0
      );
      setSearchList(
        filteredCategories?.map((item) => ({
          text: searchValue,
          type: 'products',
          categoryName: item?.name,
          categorySlug: item?.slug
        })) || []
      );
    }
  }, [categoriesData]);

  const showFilterModal = () => {
    let filtersData;
    switch (page) {
      case 'stores':
        filtersData = storeFilters;
        break;
      case 'posts':
        filtersData = postFilters;
        break;
      case 'lists':
        filtersData = listFilters;
        break;
      case 'products':
      default:
        filtersData = productFilters;
    }
    setGlobalState.setFilterModal(true, {data: filtersData, type: page});
  };

  const tabClick = (tab, s) => {
    tabChange(tab, s);
  };

  const onSearch = (pageName, searchString, filtersValue) => {
    tabChange(pageName || page || 'products', searchString, filtersValue);
  };

  const onSubmit = (searchType, searchString, doNotSave = false, categoryObj) => {
    if (!doNotSave) {
      saveSearch(searchString, searchType, categoryObj?.slug, categoryObj?.name);
    }
    setSearchValue(searchString);
    onSearch(searchType, searchString, {categories: [categoryObj?.slug]});
    searchInputRef.current.blur();
  };

  return (
    <Wrapper active>
      <SearchHeader
        searchInputRef={searchInputRef}
        onCancel={() => {
          onClose();
          setTimeout(() => {
            setSearchValue('');
          }, 200);
        }}
        onChange={(e) => {
          const {value} = e.target;
          setSearchValue(value);
        }}
        onReset={() => {
          setSearchValue('');
          onSearch();
        }}
        onSubmit={() => {
          onSubmit(page || 'products', searchValue, false, {
            name: searchList?.[0]?.categoryName,
            slug: searchList?.[0]?.categorySlug
          });
        }}
        searchValue={searchValue}
      />
      <div
        style={{
          paddingTop: 58,
          paddingBottom: 50
        }}
      >
        <Tabs active={shownTabs}>
          {tabs?.map((item) => (
            <TabItem
              key={`tab-${item.page}`}
              active={page === item.page}
              onClick={() => tabClick(item.page, searchValue)}
            >
              {item.text}
            </TabItem>
          ))}
        </Tabs>

        {page === 'all' ? (
          <SearchSuggestions
            data={searchValue?.length ? searchList : recentSearches}
            search={searchValueDebounced}
            onClick={onSubmit}
            onClear={clearRecentSearches}
          />
        ) : null}
        {page !== 'all' ? (
          <SearchResults
            searchValue={searchValueDebounced}
            onFilterClick={showFilterModal}
            page={page}
            recentSearches={recentSearches}
          />
        ) : null}
      </div>
    </Wrapper>
  );
};

SearchPage.propTypes = {
  onClose: PropTypes.func,
  page: PropTypes.string,
  search: PropTypes.string,
  tabChange: PropTypes.func,
  paginationData: PropTypes.shape({})
};

SearchPage.defaultProps = {
  onClose: () => {},
  page: 'all',
  search: '',
  tabChange: () => {},
  paginationData: {}
};

export default SearchPage;
