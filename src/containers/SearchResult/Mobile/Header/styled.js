import styled from 'styled-components/macro';

import {primaryColor} from 'constants/colors';

export const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 8.5px 16px;
  border-bottom: 1px solid #cccccc;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 120;
  background: #fff;
`;

export const SearchContainer = styled.form`
  position: relative;
  width: 100%;
  margin-right: 16px;
`;

export const SearchInput = styled.input`
  width: 100%;
  height: 40px;
  background: #efefef;
  border-radius: 4px;
  padding: 5px 44px 5px 52px;
  font-size: 18px;
  font-weight: 500;
  line-height: 21px;
  color: #000;
  border: 0;
  outline: none;
  box-shadow: none;
  caret-color: red;

  &:placeholder {
    font-weight: inherit;
    font-size: inherit;
    line-height: inherit;
    color: #666666;
  }
`;

export const SearchIcon = styled.span`
  position: absolute;
  left: 14px;
  top: 50%;
  transform: translate(0, -50%);
  max-width: 20px;
  max-height: 20px;
`;

export const SearchReset = styled.span`
  position: absolute;
  right: 16px;
  top: 50%;
  transform: translate(0, -50%);
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background: #cccccc;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`;

export const Cancel = styled.span`
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  margin-left: auto;
  cursor: pointer;
  color: ${primaryColor};
`;

export const DotsWrap = styled.div`
  display: flex;
  margin-left: 5px;
`;
