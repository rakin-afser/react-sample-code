import React, {useState, useEffect} from 'react';
import {useWindowSize} from '@reach/window-size';
import {useParams, useHistory, useLocation} from 'react-router-dom';

import Search from 'assets/Search';
import CloseIcon from 'assets/CloseIcon';
import Icon from 'components/Icon';

import {
  HeaderWrapper,
  SearchContainer,
  SearchInput,
  SearchIcon,
  SearchReset,
  Cancel,
  DotsWrap
} from 'containers/SearchResult/Mobile/Header/styled';
import DropdownMenu from 'containers/DealsPage/mobile/components/DropdownMenu';

const Header = ({searchValue, onCancel, onChange, onReset = () => {}, onSubmit = () => {}, searchInputRef}) => {
  const history = useHistory();
  const {page, param} = useParams();
  const {search} = useLocation();
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [firstRender, setFirstRender] = useState(true);
  const [showDropdownMenu, setShowDropdownMenu] = useState(false);

  useEffect(() => {
    searchInputRef.current.focus();
  }, []);

  return (
    <HeaderWrapper>
      <SearchContainer
        action="."
        onSubmit={(e) => {
          e.preventDefault();
          onSubmit();
        }}
      >
        <SearchIcon>
          <Search color="#999" />
        </SearchIcon>
        <SearchInput
          type="search"
          value={searchValue}
          onChange={onChange}
          onFocus={() => {
            if (firstRender) {
              setFirstRender(false);
            } else if (param === 'all') {
              history.push(`/search/${page}${search}`);
            }
          }}
          ref={searchInputRef}
        />
        {searchValue?.length > 0 ? (
          <SearchReset
            onClick={() => {
              onReset();
              searchInputRef.current.focus();
            }}
          >
            <CloseIcon width={12} height={12} color="#fff" />
          </SearchReset>
        ) : null}
      </SearchContainer>
      <Cancel onClick={onCancel}>Cancel</Cancel>
      {isMobile && (
        <>
          {showDropdownMenu && <DropdownMenu closeMenu={() => setShowDropdownMenu(false)} isOpen={showDropdownMenu} />}
          <DotsWrap onClick={() => setShowDropdownMenu(true)}>
            <Icon type="more" color="#000" width={20} height={20} />
          </DotsWrap>
        </>
      )}
    </HeaderWrapper>
  );
};

export default Header;
