import React, {useEffect, useMemo} from 'react';
import Error from 'components/Error';
import Loader from 'components/Loader';
import Skeleton from '../../components/Skeleton';
import NotFound from 'containers/SearchResult/Mobile/NotFound';
import ProductGridAsymmetrical from 'components/ProductGridAsymmetrical/mobile';
import FilterBarMobile from 'components/FilterBarMobile';
import {Row} from '../styled';
import {useOnScreen} from 'hooks';
import {useProductsSearchLazyQuery} from 'hooks/queries';
import {useLocation} from 'react-router';
import qs from 'qs';
import {getAttributesFilter, getTagsFromString} from 'util/heplers';
import _ from 'lodash';

const Products = ({searchValue, onFilterClick}) => {
  const {search} = useLocation();
  const {filters: filtersValue} = qs.parse(search.replace('?', '')) || {};
  const where = useMemo(() => {
    return {
      search: searchValue,
      minPrice: Number(filtersValue?.priceValue?.[0]) || undefined,
      maxPrice: Number(filtersValue?.priceValue?.[1]) || undefined,
      tagIn: getTagsFromString(searchValue),
      categoryIn: _.isEmpty(filtersValue?.subCategory) ? filtersValue?.categories : filtersValue?.subCategory,
      brands: filtersValue?.brands,
      onSale: filtersValue?.offers?.includes('sale'),
      shippingType: filtersValue?.offers?.includes('free') ? 'free_shipping' : undefined,
      taxonomyFilter: getAttributesFilter(filtersValue || {}),
      shopcoins_available: filtersValue?.productsWithShopcoins?.includes('true') ? true : undefined
    };
  }, [filtersValue, searchValue]);

  const [getProducts, {data, loading, error, fetchMore, called}] = useProductsSearchLazyQuery({
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getProducts({
        variables: {
          first: 12,
          where
        }
      });
    }
  }, [searchValue, getProducts, where]);

  const {products} = data || {};
  const {total, hasNextPage, endCursor} = products?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({variables: {first: 12, where, after: endCursor}});
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  function renderProducts() {
    if (!searchValue) return null;
    if (loading && !products?.nodes?.length) return <Skeleton />;
    if (error) return <Error />;
    if (!products?.nodes?.length && called) return <NotFound />;
    return <ProductGridAsymmetrical data={products?.nodes} />;
  }

  return (
    <>
      <Row>
        <FilterBarMobile results={total} onClick={onFilterClick} />
      </Row>
      {renderProducts()}
      {loading ? <Loader dotsSize="10px" wrapperHeight="auto" wrapperWidth="100%" /> : null}
      {endRef}
    </>
  );
};

export default Products;
