import React, {useEffect} from 'react';
import Loader from 'components/Loader';
import {useOnScreen} from 'hooks';
import {useUsersSearchLazyQuery} from 'hooks/queries';
import FilterBarMobile from 'components/FilterBarMobile';
import {Row} from '../../styled';
import NotFound from 'containers/SearchResult/Mobile/NotFound';
import Error from 'components/Error';
import {UsersList, User, Image, Description, Title, Text} from './styles';
import {useHistory} from 'react-router';

const Users = ({searchValue}) => {
  const {push} = useHistory();
  const [getUsers, {data, loading, error, fetchMore, called}] = useUsersSearchLazyQuery({
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getUsers({variables: {first: 12, where: {search: searchValue}}});
    }
  }, [searchValue, getUsers]);

  const {users} = data || {};
  const {total, hasNextPage, endCursor} = users?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({variables: {first: 12, where: {search: searchValue}, after: endCursor}});
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  if (!searchValue) return null;
  if (loading && !users?.nodes?.length) return <Loader dotsSize="10px" wrapperHeight="auto" wrapperWidth="100%" />;
  if (error) return <Error />;
  if (!users?.nodes?.length && called) return <NotFound />;

  return (
    <div>
      <Row>
        <FilterBarMobile results={total} />
      </Row>
      <UsersList>
        {users?.nodes?.map(({id, databaseId, avatar, username, totalFollowers}) => (
          <User key={id} onClick={() => push(`/profile/${databaseId}/posts`)}>
            <Image src={avatar?.url} alt={username} />
            <Description>
              <Title>{username}</Title>
              <Text>{totalFollowers} followers</Text>
            </Description>
          </User>
        ))}
      </UsersList>
      {loading ? <Loader dotsSize="10px" wrapperHeight="auto" wrapperWidth="100%" /> : null}
      {endRef}
    </div>
  );
};

export default Users;
