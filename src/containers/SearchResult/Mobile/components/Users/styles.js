import styled from 'styled-components';

export const UsersList = styled.div``;

export const User = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  margin-top: 15px;
`;

export const Image = styled.img`
  width: 36px;
  height: 36px;
  border-radius: 50%;
  object-fit: cover;
  flex-shrink: 0;
  margin-right: 16px;
`;

export const Description = styled.div`
  flex-grow: 1;
  margin-right: 10px;
`;

export const Title = styled.div`
  font-size: 14px;
  color: #000000;
  white-space: nowrap;
  max-width: 100%;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Text = styled.div`
  font-size: 12px;
  color: #999;
  line-height: 1;
  white-space: nowrap;
  max-width: 100%;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
`;
