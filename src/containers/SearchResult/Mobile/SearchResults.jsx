import React from 'react';
import PropTypes from 'prop-types';
import SearchStores from './SearchStores';
import SearchLists from './SearchLists';
import SearchPosts from './SearchPosts';
import {TabContent} from './styled';
import Products from './components/Products';
import Users from './components/Users';

const SearchResults = ({page, searchValue, onFilterClick = () => {}}) => {
  return (
    <>
      {page === 'users' && (
        <TabContent active={page === 'users'}>
          <Users searchValue={searchValue} />
        </TabContent>
      )}
      {page === 'products' && (
        <TabContent active={page === 'products'}>
          <Products searchValue={searchValue} onFilterClick={onFilterClick} />
        </TabContent>
      )}
      {page === 'stores' && (
        <TabContent active={page === 'stores'}>
          <SearchStores searchValue={searchValue} onFilterClick={onFilterClick} />
        </TabContent>
      )}
      {page === 'posts' && (
        <TabContent active={page === 'posts'}>
          <SearchPosts searchValue={searchValue} onFilterClick={onFilterClick} />
        </TabContent>
      )}
      {page === 'lists' && (
        <TabContent active={page === 'lists'}>
          <SearchLists searchValue={searchValue} onFilterClick={onFilterClick} />
        </TabContent>
      )}
    </>
  );
};

SearchResults.propTypes = {
  page: PropTypes.string,
  onFilterClick: PropTypes.func,
  searchValue: PropTypes.string
};

SearchResults.defaultProps = {
  onFilterClick: () => {},
  searchValue: ''
};

export default SearchResults;
