import styled from 'styled-components/macro';

export const SearchNotFoundContainer = styled.div`
  margin: auto;
  text-align: center;
  max-width: 80%;
`;

export const SearchNotFoundTitle = styled.p`
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  text-align: center;
  color: #000000;
  margin: 58px 0 7px;
`;

export const SearchNotFoundText = styled.p`
  margin: 0;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: #999999;
`;
