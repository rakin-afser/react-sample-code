import React from 'react';

import {
  SearchNotFoundContainer,
  SearchNotFoundTitle,
  SearchNotFoundText
} from 'containers/SearchResult/Mobile/NotFound/styled';
import SearchNotFound from 'assets/SearchNoFound';

const NotFound = () => {
  return (
    <SearchNotFoundContainer>
      <SearchNotFound />
      <SearchNotFoundTitle>No Results Found</SearchNotFoundTitle>
      <SearchNotFoundText>
        Oh Not! You did Nott search for anything. Start searching for anything and you’ll get results
      </SearchNotFoundText>
    </SearchNotFoundContainer>
  );
};

export default NotFound;
