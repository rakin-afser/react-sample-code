import React, {useEffect} from 'react';
import {useHistory} from 'react-router-dom';

import FilterBarMobile from 'components/FilterBarMobile';
import NotFound from 'containers/SearchResult/Mobile/NotFound';
import Error from 'components/Error';
import Loader from 'components/Loader';
import {getTagsFromString, getTextFromString} from 'util/heplers';
import {Row, List, ListThumb, ListThumbs, ListContent, ListTitle, ListCounters, ListDots, StoreFollow} from './styled';
import Plus from 'assets/Plus';
import Skeleton from 'containers/SearchResult/components/Skeleton';
import {useOnScreen} from 'hooks';
import {useListsLazyQuery} from 'hooks/queries';

const SearchLists = ({onFilterClick, searchValue}) => {
  const history = useHistory();

  const [getLists, {data, loading, error, fetchMore, called}] = useListsLazyQuery({
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getLists({
        variables: {
          first: 12,
          where: {
            list_name: `${getTextFromString(searchValue)}`,
            hashtags: getTagsFromString(searchValue),
            is_private: false
          }
        }
      });
    }
  }, [searchValue, getLists]);

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first: 12,
          where: {
            list_name: getTextFromString(searchValue),
            hashtags: getTagsFromString(searchValue),
            is_private: false
          },
          after: endCursor
        }
      });
    }
  });

  const {total, hasNextPage, endCursor} = data?.lists?.pageInfo || {};
  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  const onListClick = (list) => {
    history.push(`/profile/${list?.user_id}/lists/${list?.list_id}`);
  };

  function renderLists() {
    if (!searchValue) return null;
    if (error) return <Error />;
    if (!data?.lists?.nodes?.length && loading) return <Skeleton isListSection />;
    if (!data?.lists?.nodes?.length && called) return <NotFound />;

    return data?.lists?.nodes?.map((item) => {
      const productData = item?.addedProducts?.nodes;
      return (
        <List key={item?.id}>
          <ListThumb src={productData?.[0]?.image?.sourceUrl} onClick={() => onListClick(item)} />
          <ListThumbs onClick={() => onListClick(item)}>
            <img src={productData?.[1]?.image?.sourceUrl} alt="" />
            <img src={productData?.[2]?.image?.sourceUrl} alt="" />
          </ListThumbs>
          <ListContent>
            <ListDots>
              <span />
              <span />
              <span />
            </ListDots>
            <ListTitle onClick={() => onListClick(item)}>{item?.list_name}</ListTitle>
            <ListCounters>
              <span>{item?.totalFollowers} Followers</span>
              <span>{productData?.length} Products</span>
            </ListCounters>
            <StoreFollow
              isFollowing={item?.isFollowing}
              style={{
                marginLeft: 0,
                marginRight: 0,
                marginTop: 'auto'
              }}
            >
              {item?.isFollowing ? null : <Plus />}
              <span>{item?.isFollowing ? 'Following' : 'Follow'}</span>
            </StoreFollow>
          </ListContent>
        </List>
      );
    });
  }

  return (
    <>
      <Row>
        <FilterBarMobile results={total} onClick={onFilterClick} />
      </Row>
      {renderLists()}
      {data?.lists?.nodes?.length && loading ? (
        <Loader dotsSize="10px" wrapperHeight="100px" wrapperWidth="100%" />
      ) : null}
      {endRef}
    </>
  );
};

export default SearchLists;
