import React from 'react';

import {Loading, Button, Counter} from 'containers/SearchResult/Mobile/Atoms/styled';

const LoadingButton = ({text = 'Load More ...', count = 8, total = 12, onClick}) => (
  <Loading>
    <Button onClick={onClick}>{text}</Button>
    {count && total && (
      <Counter>
        {count} of {total} Items
      </Counter>
    )}
  </Loading>
);

export default LoadingButton;
