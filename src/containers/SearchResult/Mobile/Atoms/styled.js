import styled from 'styled-components/macro';

export const Loading = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  flex-direction: column;
  margin-top: 36px;
`;

export const Button = styled.span`
  outline: none;
  min-width: 126px;
  height: 30px;
  border: 1px solid #999999;
  border-radius: 4px;
  padding: 0 20px;
  font-weight: 500;
  font-size: 14px;
  display: flex;
  align-items: center;
  text-align: center;
  justify-content: center;
  color: #666666;
`;

export const Counter = styled.span`
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  color: #000000;
  margin-top: 16px;
`;
