import React, {useEffect} from 'react';
import {useLocation, useHistory} from 'react-router';
import qs from 'qs';

import FilterBarMobile from 'components/FilterBarMobile';
import NotFound from 'containers/SearchResult/Mobile/NotFound';
import Error from 'components/Error';
import Loader from 'components/Loader';
import {
  Row,
  SearchStoreContainer,
  Store,
  StoreThumb,
  StoreContent,
  StoreInside,
  StoreTitle,
  StoreRating,
  StoreFollow
} from './styled';
import Star from 'assets/Star';
import Plus from 'assets/Plus';
import productPlaceholder from 'images/placeholders/product.jpg';
import Skeleton from 'containers/SearchResult/components/Skeleton';
import {useStoresSearchLazyQuery} from 'hooks/queries';
import {useOnScreen} from 'hooks';

const SearchStores = ({onFilterClick, searchValue}) => {
  const history = useHistory();
  const {search: searchString} = useLocation();
  const {filters: filtersValue} = qs.parse(searchString.replace('?', '')) || {};
  const [getStores, {data, loading, error, fetchMore, called}] = useStoresSearchLazyQuery({
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getStores({
        variables: {
          first: 12,
          where: {
            search: searchValue,
            store_tag: filtersValue?.hashtags?.[0],
            sortBy: filtersValue?.sortBy?.[0]
          }
        }
      });
    }
  }, [searchValue, filtersValue, getStores]);

  const {stores} = data || {};
  const {total, hasNextPage, endCursor} = stores?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        variables: {
          first: 12,
          where: {
            search: searchValue,
            store_tag: filtersValue?.hashtags?.[0],
            sortBy: filtersValue?.sortBy?.[0]
          },
          after: endCursor
        }
      });
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  if (!searchValue) return null;
  if (loading && !stores?.nodes?.length) return <Skeleton isStoreSection />;
  if (error) return <Error />;
  if (!stores?.nodes?.length && called) return <NotFound />;

  return (
    <>
      <Row>
        <FilterBarMobile results={total} onClick={onFilterClick} />
      </Row>
      <SearchStoreContainer>
        {stores?.nodes?.map((item, index) => {
          return (
            <Store key={index} onClick={() => history.push(history.push(`/shop/${item?.storeUrl}/products`))}>
              <StoreThumb src={item?.banner || productPlaceholder} />
              <StoreContent>
                <StoreInside>
                  <StoreTitle dangerouslySetInnerHTML={{__html: item?.name}} />
                  <StoreRating>
                    <span>{item?.totalFollowers || 0} Followers</span>
                    <Star fill={item?.rating >= 1 ? '#FFC131' : '#CCC'} />
                    <Star fill={item?.rating >= 2 ? '#FFC131' : '#CCC'} />
                    <Star fill={item?.rating >= 3 ? '#FFC131' : '#CCC'} />
                    <Star fill={item?.rating >= 4 ? '#FFC131' : '#CCC'} />
                    <Star fill={item?.rating >= 5 ? '#FFC131' : '#CCC'} />
                  </StoreRating>
                </StoreInside>
                <StoreFollow isFollowing={item?.followed}>
                  {item?.followed ? null : <Plus />}
                  <span>{item?.followed ? 'Following' : 'Follow'}</span>
                </StoreFollow>
              </StoreContent>
            </Store>
          );
        })}
      </SearchStoreContainer>
      {loading ? <Loader dotsSize="10px" wrapperHeight="auto" wrapperWidth="100%" /> : null}
      {endRef}
    </>
  );
};

export default SearchStores;
