import React, {useEffect, useMemo} from 'react';
import {useLocation} from 'react-router-dom';
import qs from 'qs';

import FilterBarMobile from 'components/FilterBarMobile';
import NotFound from 'containers/SearchResult/Mobile/NotFound';
import Error from 'components/Error';
import Loader from 'components/Loader';
import {Row, CardsList, PostSkeleton, PostSkeletonHolder} from './styled';
import PostCardView from 'components/Cards/Post/mobile/CardView';
import {orderByValues} from 'constants/sorting';
import {usePostsSearchLazyQuery} from 'hooks/queries';
import {useOnScreen} from 'hooks';

const SearchPosts = ({onFilterClick, searchValue}) => {
  const {search} = useLocation();
  const {filters: filtersValue} = qs.parse(search.replace('?', '')) || {};
  const {sortBy, postType, hashtags} = filtersValue || {};

  const where = useMemo(() => {
    return {
      search: searchValue,
      tagSlugAnd: hashtags?.[0],
      orderby: orderByValues[sortBy?.[0]] ? {value: orderByValues[sortBy?.[0]]} : undefined,
      isFeedbackPost: postType?.[0] ? postType?.[0] === 'feedback' : undefined
    };
  }, [sortBy, postType, searchValue, hashtags]);

  const [getPosts, {data, loading, error, fetchMore, called}] = usePostsSearchLazyQuery({
    notifyOnNetworkStatusChange: true
  });

  useEffect(() => {
    if (searchValue) {
      getPosts({variables: {first: 12, where}});
    }
  }, [searchValue, where, getPosts]);

  const {hasNextPage, endCursor, total} = data?.posts?.pageInfo || {};

  const ref = useOnScreen({
    doAction() {
      fetchMore({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.posts?.nodes || [];
          const newNodes = fetchMoreResult?.posts?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            posts: {
              ...fetchMoreResult?.posts,
              nodes
            }
          };
        },
        variables: {
          first: 12,
          where,
          after: endCursor
        }
      });
    }
  });

  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  function renderPosts() {
    if (!searchValue) return null;
    if (error) return <Error />;

    if (!data?.posts?.nodes?.length && loading)
      return (
        <CardsList>
          {[...Array(12).keys()].map((_, i) => (
            <PostSkeletonHolder key={i}>
              <PostSkeleton height="100%" />
            </PostSkeletonHolder>
          ))}
        </CardsList>
      );

    if (!data?.posts?.nodes?.length && called) return <NotFound />;

    return (
      <CardsList>
        {data?.posts?.nodes?.map((post) => (
          <PostCardView key={post?.id} data={post} />
        ))}
      </CardsList>
    );
  }

  return (
    <>
      <Row>
        <FilterBarMobile results={total} onClick={onFilterClick} />
      </Row>
      {renderPosts()}
      {data?.posts?.nodes?.length && loading ? (
        <Loader dotsSize="10px" wrapperHeight="100px" wrapperWidth="100%" />
      ) : null}
      {endRef}
    </>
  );
};

export default SearchPosts;
