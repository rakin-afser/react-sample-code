import styled from 'styled-components/macro';

export const Item = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #000000;
  padding: 24px 16px 0;
  display: flex;
  align-items: center;
  text-transform: capitalize;

  & span {
    display: inline-block;
    color: #999;
  }

  & svg {
    transform: rotate(45deg);
    margin-left: auto;
    width: 19px;
    height: auto;
  }

  ${({theme: {isArabic}}) =>
    isArabic &&
    ` 
    & svg {
      transform: rotate(45deg);
      margin: 0 auto 0 0;
    }
  `}
`;
