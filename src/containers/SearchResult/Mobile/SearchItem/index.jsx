import React from 'react';
import PropTypes from 'prop-types';

import LinkIcon from 'assets/ArrowLeft';
import {Item} from 'containers/SearchResult/Mobile/SearchItem/styled';

const SearchItem = ({title = '', category = '', onClick = () => {}}) => {
  return (
    <Item onClick={onClick}>
      {title}&nbsp;<span>in {category}</span>
      <LinkIcon />
    </Item>
  );
};

SearchItem.propTypes = {
  title: PropTypes.string,
  category: PropTypes.string,
  onClick: PropTypes.func
};

export default SearchItem;
