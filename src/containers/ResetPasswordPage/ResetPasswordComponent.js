import React, {useState} from 'react';
import {Auth} from 'aws-amplify';
import {useHistory, useLocation} from 'react-router-dom';
import {Formik} from 'formik';
import {Wrapper} from './styled';
import Navigation2 from '../GuestCheckoutPage/components/mobile/Navigation2';
import InfoText from '../../components/UserAuth/components/InfoText';
import Input from '../../components/Input';
import {validateEmail} from '../../util/validators';
import {Content, FullWidthFormField, ResetForm} from './styled';
import ButtonCustom from '../../components/ButtonCustom';
import AuthHeading from '../../components/UserAuth/components/Heading';
import {gql, useMutation} from '@apollo/client';
import {SendOTP} from 'mutations';

const ResetPasswordComponent = ({isMobile, onClickClose}) => {
  const {push} = useHistory();
  const location = useLocation();
  const [errorMessage, setErrorMessage] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [emailValid, setEmailValid] = useState(false);

  const [sendOtp] = useMutation(SendOTP);

  const resetPassword = async (email) => {
    setErrorMessage(null);
    setIsLoading(true);

    try {
      await sendOtp({variables: {input: {email}}});
      setIsLoading(false);
      push('/auth/otp-verification', {
        username: email,
        email: email,
        title: 'Reset Password',
        backUrl: '/auth/reset-password',
        type: 'resetPassword'
      });
    } catch (error) {
      setIsLoading(false);
      if (error.code === 'UserNotFoundException') {
        setErrorMessage('The email address or username you have entered does not exist');
      } else {
        setErrorMessage(error.message);
      }
    }
  };

  return (
    <Wrapper>
      {isMobile ? (
        <Navigation2
          onClickBack={() => push(location.state && location.state.backUrl ? location.state.backUrl : '/auth/sign-in')}
          onClickClose={onClickClose}
          title="Reset Password"
        />
      ) : (
        <AuthHeading
          onClickBack={() => push(location.state && location.state.backUrl ? location.state.backUrl : '/auth/sign-in')}
          onClickClose={onClickClose}
          title="Reset Password"
        />
      )}

      <InfoText text="Please enter your registered email address or Username associated with this account." />
      <Formik
        initialValues={{
          email: ''
        }}
        validate={(values) => {
          const errors = {};
          const emailValid = validateEmail(values.email);

          if (!emailValid.isValid) {
            errors.email = emailValid.message;
          }

          setEmailValid(emailValid.isValid);
          return errors;
        }}
        onSubmit={(values) => {
          resetPassword(values.email).then();
        }}
      >
        {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur}) => (
          <ResetForm onSubmit={handleSubmit} noValidate>
            <Content>
              <FullWidthFormField>
                <Input
                  required
                  key="email"
                  type="email"
                  name="email"
                  id="email"
                  label="Email"
                  placeholder="Enter your email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  autoComplete="nope"
                  error={errorMessage ? errorMessage : errors.email && touched.email ? errors.email : ''}
                  value={values.email}
                  reset={values.email.length > 0}
                  resetCallback={() => setFieldValue('email', '')}
                />
              </FullWidthFormField>
              <ButtonCustom
                type="submit"
                isLoading={isLoading}
                title="Reset"
                variant="contained"
                color="mainWhiteColor"
                autoComplete="new-password"
                disabled={!emailValid}
                styles={{margin: '32px auto 0 auto'}}
              />
            </Content>
          </ResetForm>
        )}
      </Formik>
    </Wrapper>
  );
};

export default ResetPasswordComponent;
