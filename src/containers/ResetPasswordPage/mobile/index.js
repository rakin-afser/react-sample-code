import React, {useState} from 'react';
import {Auth} from 'aws-amplify';
import {useHistory} from 'react-router-dom';
import {Formik} from 'formik';
import {Wrapper} from './styled';
import Navigation2 from '../../GuestCheckoutPage/components/mobile/Navigation2';
import InfoText from '../../../components/UserAuth/components/InfoText';
import Input from '../../../components/Input';
import {validateEmail} from '../../../util/validators';
import {Content, FullWidthFormField, ResetForm} from './styled';
import ButtonCustom from '../../../components/ButtonCustom';

const ResetPasswordMobile = () => {
  const {push} = useHistory();
  const [errorMessage, setErrorMessage] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [emailValid, setEmailValid] = useState(false);

  const resetPassword = async (email) => {
    setErrorMessage(null);
    setIsLoading(true);

    try {
      await Auth.forgotPassword(email);
      setIsLoading(false);
      push('/auth/otp-verification', {
        username: email,
        email: email,
        title: 'Reset Password',
        backUrl: '/auth/reset-password',
        type: 'resetPassword'
      });
    } catch (error) {
      setIsLoading(false);
      if (error.code === 'UserNotFoundException') {
        setErrorMessage('The email address or username you have entered does not exist');
      } else {
        setErrorMessage(error.message);
      }
    }
  };

  return (
    <Wrapper>
      <Navigation2 onClickBack={() => push('/sign-in')} onClickClose={() => push('/')} title="Reset Password" />
      <InfoText text="Please enter your registered email address or Username associated with this account." />
      <Formik
        initialValues={{
          email: ''
        }}
        validate={(values) => {
          const errors = {};
          const emailValid = validateEmail(values.email);

          if (!emailValid.isValid) {
            errors.email = emailValid.message;
          }

          setEmailValid(emailValid.isValid);
          return errors;
        }}
        onSubmit={(values) => {
          resetPassword(values.email).then();
        }}
      >
        {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur}) => (
          <ResetForm onSubmit={handleSubmit}>
            <Content>
              <FullWidthFormField>
                <Input
                  required
                  key="email"
                  type="email"
                  name="email"
                  id="email"
                  label="Email"
                  placeholder="Enter your email"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={errorMessage ? errorMessage : errors.email && touched.email ? errors.email : ''}
                  value={values.email}
                  reset={values.email.length > 0}
                  resetCallback={() => setFieldValue('email', '')}
                />
              </FullWidthFormField>
              <ButtonCustom
                type="submit"
                isLoading={isLoading}
                title="Reset"
                variant="contained"
                color="mainWhiteColor"
                disabled={!emailValid}
                styles={{margin: '32px auto 0 auto'}}
              />
            </Content>
          </ResetForm>
        )}
      </Formik>
    </Wrapper>
  );
};

export default ResetPasswordMobile;
