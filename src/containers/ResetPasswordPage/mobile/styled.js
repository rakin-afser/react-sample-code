import styled from 'styled-components';

export const Wrapper = styled.div``;

export const ResetForm = styled.form`
  margin-top: 35px;
`;

export const Content = styled.div`
  &&& {
    padding: 0 24px;
    display: flex;
    align-items: center;
    flex-direction: column;
  }
`;

export const FullWidthFormField = styled.div`
  width: 100%;
`;
