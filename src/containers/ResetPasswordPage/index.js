import React from 'react';
import {useWindowSize} from '@reach/window-size';
import ResetPasswordComponent from './ResetPasswordComponent';

const ResetPasswordPage = ({onClickClose}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return <ResetPasswordComponent isMobile={isMobile} onClickClose={onClickClose} />;
};

export default ResetPasswordPage;
