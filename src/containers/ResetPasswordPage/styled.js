import styled from 'styled-components';

export const Wrapper = styled.div`
  padding-bottom: 64px;
  width: 100%;
`;

export const ResetForm = styled.form`
  margin-top: 22px;
`;

export const Content = styled.div`
  &&& {
    padding: 0 24px;
    display: flex;
    align-items: center;
    flex-direction: column;
  }
`;

export const FullWidthFormField = styled.div`
  width: 100%;
`;
