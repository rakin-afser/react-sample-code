import React, {useRef, useEffect, useCallback} from 'react';
import {Link} from 'react-router-dom';
import Wrapper from 'containers/Page404/styled';
import {useTranslation} from 'react-i18next';
import Layout from 'containers/Layout';

const Page404 = () => {
  const refGhostEye = useRef(null);
  const {t} = useTranslation();

  const eyesAnimation = useCallback((event) => {
    const pageX = document.body.clientWidth;
    const pageY = document.body.clientHeight;
    let mouseY = 0;
    let mouseX = 0;

    if (refGhostEye.current) {
      mouseY = event.pageY;
      const yAxis = ((pageY / 2 - mouseY) / pageY) * 300;

      mouseX = event.pageX / -pageX;
      const xAxis = -mouseX * 100 - 100;

      refGhostEye.current.style.transform = `translate(${xAxis}%,-${yAxis}%)`;
    }
  }, []);

  useEffect(() => {
    document.body.addEventListener('mousemove', (event) => eyesAnimation(event));

    return () => {
      document.body.removeEventListener('mousemove', eyesAnimation);
    };
  }, [eyesAnimation]);

  return (
    <Layout hideFooter>
      <Wrapper>
        <div className="box">
          <div className="box__ghost">
            <div className="symbol">&nbsp;</div>
            <div className="symbol">&nbsp;</div>
            <div className="symbol">&nbsp;</div>
            <div className="symbol">&nbsp;</div>
            <div className="symbol">&nbsp;</div>
            <div className="symbol">&nbsp;</div>

            <div className="box__ghost-container">
              <div ref={refGhostEye} className="box__ghost-eyes">
                <div className="box__eye-left">&nbsp;</div>
                <div className="box__eye-right">&nbsp;</div>
              </div>
              <div className="box__ghost-bottom">
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
                <div>&nbsp;</div>
              </div>
            </div>
            <div className="box__ghost-shadow">&nbsp;</div>
          </div>

          <div className="box__description">
            <div className="box__description-container">
              <h1 className="box__description-title">{t('page404.whoops')}</h1>
              <h2 className="box__description-text">{t('page404.message')}</h2>
            </div>

            <Link to="/" className="box__button">
              {t('page404.buttonTxt')}
            </Link>
          </div>
        </div>
      </Wrapper>
    </Layout>
  );
};

export default Page404;
