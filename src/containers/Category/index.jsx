import React from 'react';
import {useParams} from 'react-router-dom';
import {useWindowSize} from '@reach/window-size';
import {useTranslation} from 'react-i18next';

import Layout from 'containers/Layout';
import CategoryMobile from 'containers/Category/mobile';
import CategoryDesktop from 'containers/Category/desktop';
import {categories} from 'containers/Categories';
import Icon from 'components/Icon';
import {DotsWrapper} from 'containers/Category/styled';
import DropdownMenu from 'containers/DealsPage/mobile/components/DropdownMenu';

const Category = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const {categoryName} = useParams();
  const {t} = useTranslation();
  const categorySlug = categories.find((item) => item.id === categoryName)?.name;
  const categoryTitle = categorySlug ? t(categorySlug) : categoryName;
  const [showDropdownMenu, setShowDropdownMenu] = useState(false);

  return (
    <Layout
      showBottomNav={isMobile}
      isFooterShort
      hideHeaderLinks={isMobile}
      showBurgerButton={false}
      showBackButton={isMobile}
      showSearchButton={false}
      hideCopyright
      stickyHeader={isMobile}
      title={categoryTitle}
      showBasketButton={false}
      layOutStyles={{
        paddingTop: 0
      }}
      customRightIcon={
        <DotsWrapper onClick={() => setShowDropdownMenu(true)}>
          <Icon type="dots" width={24} height={24} />
        </DotsWrapper>
      }
    >
      {isMobile ? (
        <>
          {showDropdownMenu && <DropdownMenu closeMenu={() => setShowDropdownMenu(false)} isOpen={showDropdownMenu} />}
          <CategoryMobile categoryName={categoryName} categories={categories} />
        </>
      ) : (
        <CategoryDesktop categoryName={categoryName} />
      )}
    </Layout>
  );
};

export default Category;
