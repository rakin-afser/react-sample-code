import React from 'react';

const CategoryDesktop = ({categoryName}) => {
  return <div>{categoryName} Desktop Page</div>;
};

export default CategoryDesktop;
