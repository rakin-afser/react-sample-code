import React, {useEffect} from 'react';

import useGlobal from 'store';
import {featuredProducts} from 'constants/staticData';
import CardNewArrival from 'components/CardNewArrival';
import FilterBarMobile from 'components/FilterBarMobile';
import {getUrlWithoutLastParam} from 'util/heplers';
import {Wrapper, ContentContainer, Products, SortContainer} from 'containers/Category/mobile/styled';
import FilterButtons from 'components/FilterButtons';
import {productFilters} from 'constants/filters';

const filterItems = productFilters.children?.find((item) => item.id === 'category')?.children || [];

const CategoryMobile = ({categoryName, categories}) => {
  const [globalState, setGlobalState] = useGlobal();

  useEffect(() => {
    const urlWithoutLastParam = getUrlWithoutLastParam(window.location.pathname);

    if (globalState.headerBackButtonLink !== urlWithoutLastParam) {
      setGlobalState.setHeaderBackButtonLink(urlWithoutLastParam);
    }
  }, []);

  return (
    <Wrapper>
      <FilterButtons data={filterItems} wrapPadding="24px 24px 8px 8px" />
      <SortContainer>
        <FilterBarMobile results="12,346" title="Sort & Filter" resultLabel="Items" redDot={true} iconName="filter" />
      </SortContainer>
      <ContentContainer>
        <Products>
          {featuredProducts.map((product, index) => (
            <CardNewArrival key={index} {...product} />
          ))}
        </Products>
      </ContentContainer>
    </Wrapper>
  );
};

export default CategoryMobile;
