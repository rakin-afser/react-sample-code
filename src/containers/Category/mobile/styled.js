import styled from 'styled-components/macro';
import {headerHeight, mobileHeaderHeight} from 'components/Header/constants';

export const Wrapper = styled.div`
  padding: ${mobileHeaderHeight} 0 30px;
`;

export const ContentContainer = styled.div`
  padding: 0 16px;
`;

export const SortContainer = styled.div`
  margin-top: 19px;
  padding: 0 16px 16px 16px;
  border-bottom: 1px solid #efefefef;
`;

export const Products = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-around;
  margin: 16px -3.5px 0;

  & > div {
    margin: 0 3.5px 16px;
    flex: 0 0 auto;
    width: calc(50% - 7px);
  }
`;
