import React from 'react';
import {useWindowSize} from '@reach/window-size';
import PostsPageDesktop from './desktop/PostsPage';
import PostsPageMobile from './mobile/PostsPage';
import Layout from 'containers/Layout';

const Post = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return (
    <Layout title="Posts" showBackButton showSearchButton={false} showBasketButton={false}>
      {isMobile ? <PostsPageMobile /> : <PostsPageDesktop />}
    </Layout>
  );
};

export default Post;
