import React from 'react';
import useGlobal from 'store';
import IconMore from 'assets/More';
import {
  Item,
  Image,
  Body,
  Price,
  CurrentPrice,
  CurrentPriceValue,
  PriceWithoutDiscount,
  Name,
  Label,
  DiscountLabel,
  ButtonMore,
  Likes,
  ButtonLike,
  LikesNumber,
  PlayBtn,
  ImageWrapper
} from 'containers/ShopPage/mobile/ProductGridAsymmetrical/components/ProductCard/styled';
import productPlaceholder from 'images/placeholders/product.jpg';
import {useUser} from 'hooks/reactiveVars';
import Icon from 'components/Icon';
import {useAddUpdateLikedProductMutation} from 'hooks/mutations';

const ProductCard = ({item = {}, isBigImage, onClick, ...props}) => {
  const [, setGlobalState] = useGlobal();
  const [user] = useUser();
  const {
    databaseId,
    name,
    image,
    isLiked,
    shippingType,
    slug,
    totalLikes,
    seller,
    product_video_url: productVideoUrl,
    productVideoPoster,
    variations
  } = item;

  const {salePrice, regularPrice} = variations?.nodes?.[0] || item || {};

  const [triggerLike] = useAddUpdateLikedProductMutation({
    variables: {input: {id: databaseId}},
    product: item
  });

  let discountPercent = null;

  if (salePrice)
    discountPercent = (100 - (salePrice?.replace(/^\D+/g, '') / regularPrice?.replace(/^\D+/g, '')) * 100).toFixed(0);

  const onCardClick = () => {
    if (typeof onClick === 'function') {
      onClick(slug);
    } else {
      setGlobalState.setQuickView(item);
    }
  };

  const onUserLike = () => {
    if (user?.databaseId) {
      triggerLike();
    }
  };

  const onDotsClick = () => {
    if (user?.databaseId) {
      setGlobalState.setActionsPopup(true, {productId: databaseId, slug, seller, name});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  return (
    <Item>
      <ImageWrapper onClick={onCardClick}>
        <Image
          src={productVideoPoster || image?.sourceUrl || productPlaceholder}
          alt={image?.altText}
          $isBigImage={isBigImage}
          {...props}
        />
        {productVideoUrl ? (
          <PlayBtn>
            <Icon type="playRed" />
          </PlayBtn>
        ) : null}
      </ImageWrapper>
      <Body>
        <Name>{name}</Name>
        <Price>
          <CurrentPrice isDiscount={salePrice ? 1 : 0}>
            <CurrentPriceValue>{salePrice || regularPrice}</CurrentPriceValue>
          </CurrentPrice>
          {salePrice ? <PriceWithoutDiscount>{regularPrice}</PriceWithoutDiscount> : null}
        </Price>
        <Label>{shippingType}&nbsp;</Label>
        <Likes>
          <ButtonLike onClick={onUserLike}>
            <Icon width={12} height={11} type={isLiked ? 'liked' : 'like'} color="#8F8F8F" />
          </ButtonLike>
          {totalLikes ? <LikesNumber>{totalLikes}</LikesNumber> : null}
        </Likes>
      </Body>
      {discountPercent ? <DiscountLabel>-{discountPercent}%</DiscountLabel> : null}
      <ButtonMore onClick={onDotsClick}>
        <IconMore />
      </ButtonMore>
    </Item>
  );
};

export default ProductCard;
