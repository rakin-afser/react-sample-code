import React from 'react';

import ProductCard from 'containers/ShopPage/mobile/ProductGridAsymmetrical/components/ProductCard';
import {ItemsGrid, Left, Right} from './styled';

const ProductGridAsymmetrical = ({data = [], onProductClick}) => {
  const items = data;

  const itemsLeftCol = [];
  const itemsRightCol = [];

  items.forEach((item, index) => {
    const currentNumber = index + 1;

    if (currentNumber % 2 === 0) {
      itemsRightCol.push(item);
    } else {
      itemsLeftCol.push(item);
    }
  });

  return (
    <ItemsGrid>
      <Left>
        {itemsLeftCol.map((item, index) => (
          <ProductCard key={item.id} item={item} onClick={onProductClick} isBigImage={(index + 1) % 2 === 0} />
        ))}
      </Left>
      <Right>
        {itemsRightCol.map((item, index) => (
          <ProductCard key={item.id} item={item} onClick={onProductClick} isBigImage={(index + 1) % 2 !== 0} />
        ))}
      </Right>
    </ItemsGrid>
  );
};

export default ProductGridAsymmetrical;
