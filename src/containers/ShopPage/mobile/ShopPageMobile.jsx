import React, {useState, useEffect} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import {useQuery} from '@apollo/client';
// import {useSwipeable} from 'react-swipeable';

import useGlobal from 'store';
import {useOnScreen} from 'hooks';
import {useProductsQuery} from 'hooks/queries';

import {PageWrap, Products, ContentContainer, SearchContainer, FilterContainer, Tabs, CardsList, Title, FeturedTitle, FeturedShowMoreContainer, FeturedShowMore} from './styled';
import ShopInfo from 'components/ShopInfo';
// import {allCategoriesMobile} from 'constants/staticData';
import Divider from '../../../components/Divider';
// import Pagination from 'components/PagePagination';
// import CategoriesMobile from '../../../components/ProductCategoriesMobile';
// import CardNewArrival from '../../../components/CardNewArrival';
import SearchInputMobile from '../../../components/ShopPage/ShopSearchInput/SearchInputMobile';
import FilterBarMobile from './FilterBarMobile';
// import InstagramPostsMobile from '../../../components/InstagramPostsMobile';
import Description from 'components/ProductPage/mobile/Description';
import Feedbacks from 'components/ProductPage/mobile/Feedbacks';
import PostCardView from 'components/Cards/Post/mobile/CardView';
import FilterProductsPopupMobile from 'components/FilterProductsPopupMobile';
import ProductCard from 'containers/ShopPage/mobile/ProductGridAsymmetrical/components/ProductCard';
import Grid from 'components/Grid';
import Post from 'components/Post';
import FadeSwitch from 'containers/Animations/FadeSwitch';
import usePagination from 'util/usePagination';
// import instagram1 from '../desktop/Products/img/instagram1.png';
// import instagram2 from '../desktop/Products/img/instagram2.png';
// import instagram3 from '../desktop/Products/img/instagram3.png';
// import instagram4 from '../desktop/Products/img/instagram4.png';
import SocialMediaMobile from '../../../components/SocialMediaMobile';
import PaymentOptionsMobile from '../../../components/PaymentOptionsMobile';
import Layout from 'containers/Layout/index';
import Icon from 'components/Icon';
import ShopNavigationMenuMobile from 'components/ShopNavigationMenuMobile';
import {FEED_POSTS, SHOP_FEATURED_PRODUCTS} from 'queries';
import Error from 'components/Error';
import Loader from 'components/Loader';
import ProductGridAsymmetrical from './ProductGridAsymmetrical/mobile';
import NothingFound from 'components/NothingFound';
import TopMenu from 'containers/ShopPage/mobile/TopMenu';
import {orderByValues, shopPageSortOptions} from 'constants/sorting';

// const instagramPosts = [
//   {image: instagram1},
//   {image: instagram2},
//   {image: instagram3},
//   {image: instagram4},
//   {image: instagram1},
//   {image: instagram2},
//   {image: instagram3},
//   {image: instagram4},
//   {image: instagram1},
//   {image: instagram2},
//   {image: instagram3},
//   {image: instagram4}
// ];

const ShopPageMobile = ({storeData, isOwner}) => {
  const history = useHistory();
  const [, setGlobalState] = useGlobal();
  const [feedView, setFeedView] = useState(false);
  const [searchValue, setSearchValue] = useState('');
  const [activeTab, setActiveTab] = useState('products');
  const [sortBy, setSortBy] = useState(null);

  const [clickedPost, setClickedPost] = useState(null);
  const [visible, setVisible] = useState([]);
  const active = visible?.length === 0 ? null : Math.min(...visible);

  const productsPerPage = Number(storeData?.store?.storePpp) || 9;
  const {shopName, page, categoryName, subCategoryName, subCategoryName2} = useParams();

  const {data: dataPosts, loading: loadingPosts, fetchMore: fetchMorePosts} = useQuery(FEED_POSTS, {
    variables: {
      first: 9,
      where: {
        author: parseInt(storeData?.store?.id)
      }
    },
    notifyOnNetworkStatusChange: true
  });
  
  const {data: dataFeaturedProducts, loading: loadingFeaturedProducts, fetchMore: fetchFeaturedProducts} = useQuery(SHOP_FEATURED_PRODUCTS, {
    variables: {
      first: 2,
      where: {
        featured: true
      }
    },
    notifyOnNetworkStatusChange: true
  });

  const {posts} = dataPosts || {};
  const {currentCursor} = usePagination();
  const variables = {
    vendorId: +storeData?.store?.id,
    first: productsPerPage,
    cursor: currentCursor,
    search: searchValue,
    categorySlug: subCategoryName2 || subCategoryName || categoryName,
    orderBy: {value: orderByValues[sortBy]}
  };
  const {data, loading, error, fetchMore} = useProductsQuery({variables, notifyOnNetworkStatusChange: true});
  const {hasNextPage, endCursor} = data?.products?.pageInfo || {};
  const ref = useOnScreen({
    doAction() {
      fetchMore({variables: {...variables, cursor: endCursor}});
    }
  });
  const endRef = hasNextPage && !loading ? <div ref={ref} /> : null;

  const {endCursor: endCursorPosts, hasNextPage: hasNextPagePosts} = posts?.pageInfo || {};
  const {endCursor: endCursorFeaturedProducts, hasNextPage: hasNextFeaturedProducts} = dataFeaturedProducts?.products?.pageInfo || {};

  const loadFeaturedProductsMore = () => {
    if (hasNextFeaturedProducts) {
      // fetchFeaturedProducts({variables: {first: 9, cursor: endCursorFeaturedProducts, where: {featured: true}}});

      fetchFeaturedProducts({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.products?.nodes || [];
          const newNodes = fetchMoreResult?.products?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            products: {
              ...fetchMoreResult?.products,
              nodes
            }
          };
        },
        variables: {
          first: 9,
          where: {
            featured: true
          },
          after: endCursorFeaturedProducts
        }
      });
    }
  };

  const refPosts = useOnScreen({
    doAction() {
      fetchMorePosts({
        updateQuery(prev, {fetchMoreResult}) {
          if (!fetchMoreResult) return prev;
          const oldNodes = prev?.posts?.nodes || [];
          const newNodes = fetchMoreResult?.posts?.nodes || [];
          const nodes = [...oldNodes, ...newNodes];
          return {
            ...fetchMoreResult,
            posts: {
              ...fetchMoreResult?.posts,
              nodes
            }
          };
        },
        variables: {
          first: 9,
          where: {
            author: parseInt(storeData?.store?.id)
          },
          after: endCursorPosts
        }
      });
    }
  });

  const endRefPosts = hasNextPagePosts && !loadingPosts ? <div ref={refPosts} /> : null;

  const {vendorBiography, storeTermsofService, storeShippingPolicy, storeReturnPolicy, name} = storeData?.store || {};

  // const onCategoryClick = (category) => {
  //   if (category.slug !== 'all') {
  //     history.push(`/shop/${shopName}/categories/${category.slug}`);
  //   }
  // };

  const backButtonClick = () => {
    history.push('/');
  };

  // const goToNextCategory = () => {
  //   history.push(`/shop/${shopName}/categories/${allCategoriesMobile[1].slug}`);
  // };

  const navigateToFeedback = () => {
    history.push(`/shop/${shopName}/feedback`);
  };

  const handleChangeSearch = (ev) => {
    setSearchValue(ev.target.value);
  };

  const handleSort = (sortValue) => {
    setSortBy(sortValue);
  };

  const postCardClick = (databaseId) => {
    setClickedPost(databaseId);
    setFeedView(true);
  };

  // const handlers = useSwipeable({
  //   onSwipedDown: (eventData) => {
  //     setCategoryShow(false);
  //   },
  //   onSwipedUp: (eventData) => {
  //     setCategoryShow(true);
  //   },
  //   onSwipedLeft: (eventData) => {
  //     goToNextCategory();
  //   },
  //   delta: 10,
  //   preventDefaultTouchmoveEvent: false,
  //   trackTouch: true,
  //   trackMouse: false,
  //   rotationAngle: 0
  // });

  useEffect(() => {
    setGlobalState.setHeaderBackButtonLink('/');
  }, [setGlobalState, shopName]);

  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, []);

  const sortingClick = () => {
    setGlobalState.setProductFiltersPopup(true);
  };

  const getPageTitle = () => {
    switch (page) {
      case 'feedback':
        return 'Feedback';
      default:
        return '';
    }
  };

  if (error) return <Error />;

  return (
    <Layout
      isFooterShort
      hideHeaderLinks
      showBurgerButton={false}
      showBackButton={false}
      showBasketButton={false}
      showSearchButton={false}
      title={name}
      hideCopyright
      layOutStyles={{
        paddingBottom: 60,
        paddingTop: 46
      }}
    >
      <TopMenu isOwner={isOwner} title={name} backButtonClick={backButtonClick} />
      <ShopNavigationMenuMobile isOwner={isOwner} data={storeData} />
      <PageWrap>
        <ShopInfo isOwner={isOwner} isMobile data={storeData} />

        {['products', 'posts', 'categories'].includes(page) && (
          <>
            <Tabs>
              <button
                type="button"
                className={`item${activeTab === 'products' ? ' active' : ''}`}
                onClick={() => setActiveTab('products')}
              >
                <span className="title">
                  <span className="title--dinamic">Products</span>
                  <span className="title--static">Products</span>
                </span>
                {data?.products?.pageInfo?.total ? (
                  <span className="count">{data?.products?.pageInfo?.total}</span>
                ) : null}
              </button>
              <button
                type="button"
                className={`item${activeTab === 'posts' ? ' active' : ''}`}
                onClick={() => setActiveTab('posts')}
              >
                <span className="title">
                  <span className="title--dinamic">Posts</span>
                  <span className="title--static">Posts</span>
                </span>
                {dataPosts?.posts?.pageInfo?.total ? (
                  <span className="count">{dataPosts?.posts?.pageInfo?.total}</span>
                ) : null}
              </button>
            </Tabs>
            {/* {page !== 'posts' && (
              <CategoriesMobile active="all" onClick={onCategoryClick} categories={allCategoriesMobile} />
            )} */}
            <ContentContainer activeTab={activeTab}>
              {activeTab === 'products' ? (
                <div className="products-container">
                  {loadingFeaturedProducts && !dataFeaturedProducts && <Loader wrapperWidth="100%" wrapperHeight="200px" />}
                  {!!dataFeaturedProducts?.products?.pageInfo?.total && (
                    <>
                      <FeturedTitle>Featured Products</FeturedTitle>
                      <Products isFeatured>
                        {
                          dataFeaturedProducts?.products?.nodes.map((item) => (
                            <ProductCard key={item.id} item={item} isBigImage={false} />
                          ))
                        }
                        {!loadingFeaturedProducts && dataFeaturedProducts?.products?.pageInfo?.total > 2 ? (
                          <FeturedShowMoreContainer>
                            <FeturedShowMore onClick={loadFeaturedProductsMore}>
                              <span>Show More</span>
                              <Icon type="arrow" color="#333333" width={6} height={11}/>
                            </FeturedShowMore>
                          </FeturedShowMoreContainer>
                        ) : null}
                        {loadingFeaturedProducts && <div className="loader"><Loader wrapperWidth="100%" wrapperHeight="200px" /></div>}
                      </Products>
                    </>
                  )}
                  <SearchContainer>
                    <SearchInputMobile onChange={handleChangeSearch} isMobile value={searchValue} />
                  </SearchContainer>
                  <FilterContainer>
                    <FilterBarMobile results={data?.products?.pageInfo?.total} onClick={sortingClick} />
                  </FilterContainer>
                  {!data?.products?.pageInfo?.total && !loading && <NothingFound hideHelpText hideAdditionalText />}
                  {!!data?.products?.pageInfo?.total && (
                    <Products>
                      <ProductGridAsymmetrical data={data?.products?.nodes} />
                    </Products>
                  )}
                  {loading && <Loader wrapperWidth="100%" wrapperHeight="200px" />}
                  {endRef}
                </div>
              ) : null}

              {activeTab === 'posts' ? (
                <>
                  {!posts?.pageInfo?.total && !loading ? (
                    <NothingFound hideHelpText hideAdditionalText />
                  ) : (
                    <>
                      <Grid sb padding="0 0 10px 0" width="100%">
                        <Title>Recent</Title>
                        <FadeSwitch state={feedView}>
                          <Icon type={feedView ? 'feed' : 'cards'} onClick={() => setFeedView(!feedView)} />
                        </FadeSwitch>
                      </Grid>
                      <FadeSwitch state={feedView}>
                        {feedView ? (
                          posts?.nodes?.map((item, i) => (
                            <Post
                              scrollToThisPost={item?.databaseId === clickedPost}
                              active={i === active}
                              visible={visible}
                              setVisible={setVisible}
                              order={i}
                              key={item.id}
                              data={item}
                              isUserLogeIn
                            />
                          ))
                        ) : (
                          <CardsList>
                            {posts?.nodes?.map((item) => (
                              <PostCardView data={item} onClick={() => postCardClick(item?.databaseId)} />
                            ))}
                          </CardsList>
                        )}
                      </FadeSwitch>
                      {loadingPosts && <Loader wrapperWidth="100%" wrapperHeight="200px" />}
                      {endRefPosts}
                    </>
                  )}
                </>
              ) : null}
            </ContentContainer>
            {/* {!!data?.products?.pageInfo?.total && (
              <PaginationWrap>
                <Pagination
                  isMobile
                  totalPages={Math.ceil(data?.products?.pageInfo?.total / productsPerPage)}
                  prevPage={() => goPrevPage(data?.products?.pageInfo)}
                  nextPage={() => goNextPage(data?.products?.pageInfo)}
                  currentPage={currentPage}
                />
              </PaginationWrap>
            )} */}
            {/* <Divider style={{height: 8, margin: '21px 0'}} /> */}
            {/* <InstagramPostsMobile posts={instagramPosts} /> */}
          </>
        )}

        {page === 'about' && (
          <>
            <Description title="About" text={vendorBiography || 'No biography yet'} />
            <Divider style={{height: 8}} />
            <Feedbacks hideFooter={false} showSeeMoreBtn onClickSeeMore={navigateToFeedback} disablePopup />
            <Divider style={{height: 8}} />
            <SocialMediaMobile />
            <Divider style={{height: 8}} />
            <Description title="Shop Terms of Service" text={storeTermsofService} />
            <Divider style={{height: 8}} />
            <Description title="Shipping Policies" text={storeShippingPolicy} />
            <Divider style={{height: 8}} />
            <PaymentOptionsMobile />
            <Divider style={{height: 8}} />
            <Description title="Returns &amp; Exchanges" text={storeReturnPolicy} />
          </>
        )}
        <FilterProductsPopupMobile onClick={handleSort} sortOptions={shopPageSortOptions} />
      </PageWrap>
    </Layout>
  );
};

export default ShopPageMobile;
