import styled from 'styled-components';
import {bookmarkFillColor} from 'constants/colors';

export const PageWrap = styled.div`
  width: 100%;

  .products-container {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
  }
`;

export const ContentContainer = styled.div`
  padding: 18px 16px 0;

  ${({activeTab}) =>
    activeTab === 'posts'
      ? `
    > div {
      width: 100%;
    }
  `
      : ''}
`;

export const SearchContainer = styled.div`
  width: calc(100% - 43px);
  padding-right: 13px;
`;

export const FilterContainer = styled.div`
  width: 43px;
`;

export const Products = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  margin: 19px 0 0;

  ${({isFeatured}) =>
    isFeatured &&
    `
    padding-bottom: 24px;
    margin-top: 0;
    margin-bottom: 30px;
    border-bottom: 1px solid #E4E4E4;

    > div {
      width: calc(50% - 6.5px);

      &:nth-child(even) {
        margin-left: auto;
      }
    }

    .loader {
      width: 100% !important;
    }
  `}
`;

export const PaginationWrap = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 13px;
`;

export const Tabs = styled.div`
  display: flex;
  margin-top: 10px;

  .item {
    height: 40px;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 17px;
    text-align: center;
    color: #999999;
    transition: all 0.3s ease;
    box-shadow: 0 2px 0 0 #f9f7f7;
    appearance: none;
    -webkit-appearance: none;

    &.active {
      box-shadow: 0 2px 0 0 #ed484f;
      font-weight: 700;
      color: #000000;

      .title {
        &--dinamic {
          opacity: 0;
        }

        &--static {
          opacity: 1;
        }
      }
    }
  }

  .title {
    position: relative;

    &--dinamic {
      position: absolute;
      top: 2px;
      left: 0;
    }

    &--static {
      position: static;
      font-weight: 700 !important;
      opacity: 0;
    }
  }

  .count {
    font-style: normal;
    font-weight: 500;
    font-size: 10px;
    line-height: 12px;
    display: inline-flex;
    text-align: center;
    align-items: center;
    justify-content: center;
    color: #7c7e82;
    height: 14px;
    padding: 0 7px;
    min-width: 26px;
    margin-left: 8px;
    border: 0.5px solid #e8e1e1;
    border-radius: 22px;
  }
`;

export const CardsList = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  grid-auto-flow: row;
  grid-gap: 2px;
  padding: 9px;
  background-color: rgba(196, 196, 196, 0.13);
  border-radius: 8px;
  margin: 0;
  width: 100%;

  > {
    width: 100%;
  }
`;

export const Title = styled.h4`
  color: ${bookmarkFillColor};
  margin-bottom: 0;
`;

export const FeturedTitle = styled.h4`
  font-family: 'Helvetica Neue';
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 120%;
  color: #000000;
  margin-bottom: 18px;
`;

export const FeturedShowMoreContainer = styled.div`
  width: 100% !important;
  text-align: center;
`;

export const FeturedShowMore = styled.button`
  border: 0.5px solid #cccccc;
  border-radius: 22px;
  margin: 4px auto 0;
  width: 124px;
  height: 34px;

  i {
    transform: rotate(90deg);
    margin-left: 9px;
    top: 1px;
    position: relative;
  }
`;
