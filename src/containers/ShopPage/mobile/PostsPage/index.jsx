import React, {useEffect} from 'react';
import {useQuery} from '@apollo/client';
import {useParams, useHistory} from 'react-router-dom';

import useGlobal from 'store';
import {POSTS} from 'queries';
import Loader from 'components/Loader';
import Error from 'components/Error';
import Post from 'components/Post';
import {PageWrap} from 'containers/ShopPage/mobile/styled';
import Layout from 'containers/Layout';
import TopMenu from 'containers/ShopPage/mobile/TopMenu';
import ShopNavigationMenuMobile from 'components/ShopNavigationMenuMobile';

const PostsPage = () => {
  const history = useHistory();
  const {shopName} = useParams();
  const {data, loading, error} = useQuery(POSTS, {variables: {authorId: +shopName}});

  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, []);

  const backButtonClick = () => {
    history.push(`/shop/${shopName}/products`);
  };

  if (loading) return <Loader />;
  if (error) return <Error />;

  return (
    <Layout
      hideHeader
      isFooterShort
      hideHeaderLinks={true}
      showBurgerButton={false}
      showBackButton={true}
      showSearchButton={true}
      hideCopyright={true}
      layOutStyles={{
        paddingBottom: 60,
        paddingTop: 46
      }}
    >
      <TopMenu backButtonClick={backButtonClick} title={'Posts'} />
      <ShopNavigationMenuMobile />
      <PageWrap>
        {data?.posts?.nodes?.map((post) => (
          <Post key={post.databaseId} data={post} isUserLogeIn={true} />
        ))}
      </PageWrap>
    </Layout>
  );
};

export default PostsPage;
