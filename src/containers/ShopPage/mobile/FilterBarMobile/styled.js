import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;

  border: 0.5px solid #cccccc;
  border-radius: 22px;
  height: 26px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Results = styled.div`
  font-family: Helvetica Neue;
  font-size: 14px;
  line-height: 16px;
  color: #000;
`;

export const RightBlock = styled.div`
  display: flex;
  align-items: center;
`;

export const ResTitle = styled.div`
  position: relative;
  margin-right: 27px;
  font-family: Helvetica Neue;
  font-size: 14px;
  line-height: 17px;
  font-weight: 500;
  color: #000;
`;

export const Badge = styled.div`
  position: absolute;
  right: 8px;
  top: 3px;
  width: 7px;
  height: 7px;
  background-color: #ed484f;
  border-radius: 50%;
  border: 1px solid #fff;
  z-index: 1;
`;

export const Icon = styled.div`
  display: flex;
`;
