import React from 'react';
import PropTypes from 'prop-types';
import FilterIcon from 'assets/FilterSimply';
import Chevron from 'assets/Chevron';

import {Container, Results, ResTitle, RightBlock, Badge, Icon} from './styled';
import useGlobal from 'store';

const icons = {
  filter: <FilterIcon />,
  chevron: <Chevron />
};

const FilterBarMobile = ({redDot, iconName, onClick}) => {
  const [globalState, globalActions] = useGlobal();

  const handleClick = () => {
    globalActions.setProductFiltersPopup(true);
  };

  return (
    <Container onClick={onClick || handleClick}>
      <RightBlock>
        <Icon>
          {redDot && <Badge />}
          {icons[iconName]}
        </Icon>
      </RightBlock>
    </Container>
  );
};

FilterBarMobile.defaultProps = {
  results: 0,
  title: 'Sort & Filter',
  resultLabel: 'Results',
  redDot: true,
  iconName: 'filter',
  onClick: () => {}
};

FilterBarMobile.propTypes = {
  results: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  title: PropTypes.string,
  resultLabel: PropTypes.string,
  redDot: PropTypes.bool,
  iconName: PropTypes.string,
  onClick: PropTypes.func
};

export default FilterBarMobile;
