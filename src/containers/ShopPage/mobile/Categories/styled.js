import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  margin: 60px 0 60px 0;
`;

export const Title = styled.div`
  padding: 20px 16px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  color: #000;
`;
