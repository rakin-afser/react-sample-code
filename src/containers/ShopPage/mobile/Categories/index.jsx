import React, {useState, useEffect} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import {useQuery} from '@apollo/client';

import {Wrapper, Title} from 'containers/ShopPage/mobile/Categories/styled';
import TopMenu from 'containers/ShopPage/mobile/TopMenu';
import ShopNavigationMenuMobile from 'components/ShopNavigationMenuMobile';
import {FilterItem, FilterItemWrapper} from 'components/Modals/mobile/Filters/styled';
import Arrow from 'assets/Arrow';

import {Products, ContentContainer, PaginationWrap, FilterContainer} from 'containers/ShopPage/mobile/styled';
import Pagination from 'components/PagePagination';
import FilterBarMobile from 'components/FilterBarMobile';
import {PRODUCTS} from 'queries';
import Error from 'components/Error';
import Loader from 'components/Loader';
import ProductGridAsymmetrical from 'components/ProductGridAsymmetrical/mobile';
import NothingFound from 'components/NothingFound';
import usePagination from 'util/usePagination';

const Categories = ({title = '', storeData}) => {
  const {shopName, page, categoryName, subCategoryName, subCategoryName2} = useParams();
  const history = useHistory();
  const [viewAll, setViewAll] = useState(false);
  const productsPerPage = Number(storeData?.store?.storePpp) || 9;
  const {currentPage, currentCursor, goNextPage, goPrevPage, resetPagination} = usePagination();
  const {data, loading, error} = useQuery(PRODUCTS, {
    variables: {
      vendorId: +storeData?.store?.id,
      first: productsPerPage * currentPage,
      cursor: currentCursor,
      categorySlug: subCategoryName2 || subCategoryName || categoryName
    }
  });

  const categoryItemClick = (slug) => {
    resetPagination();
    history.push(`/shop/${shopName}/categories/${slug}`);
  };

  const backButtonClick = () => {
    resetPagination();
    if (viewAll) {
      setViewAll(false);
      return;
    }
    if (!categoryName) {
      history.push(`/shop/${shopName}`);
      return;
    }
    history.push(`/shop/${shopName}/categories`);
  };

  const getCategoryName = (slug) =>
    storeData?.store?.productCategories?.find((item) => item?.slug === slug)?.name?.replace('&amp;', '&');

  const totalProducts = () =>
    storeData?.store?.productCategories
      ?.filter((item) => item?.parent === '0')
      ?.reduce((count, item) => count + item?.productIds?.length, 0);

  if (error) return <Error />;

  return (
    <Wrapper>
      <TopMenu
        backButtonClick={backButtonClick}
        title={viewAll ? 'All' : getCategoryName(subCategoryName2 || subCategoryName || categoryName)}
      />
      {categoryName || viewAll ? (
        <>
          <ContentContainer>
            <FilterContainer>
              <FilterBarMobile results={data?.products?.pageInfo?.total} title="Sort" />
            </FilterContainer>
            {loading && <Loader wrapperWidth="100%" wrapperHeight="200px" />}
            {!data?.products?.pageInfo?.total && !loading && <NothingFound hideHelpText hideAdditionalText />}
            {!!data?.products?.pageInfo?.total && (
              <Products>
                <ProductGridAsymmetrical data={data?.products?.nodes} />
              </Products>
            )}
          </ContentContainer>
          {!!data?.products?.pageInfo?.total && (
            <PaginationWrap>
              <Pagination
                isMobile={true}
                totalPages={Math.ceil(data?.products?.pageInfo?.total / productsPerPage)}
                prevPage={() => goPrevPage(data?.products?.pageInfo)}
                nextPage={() => goNextPage(data?.products?.pageInfo)}
                currentPage={currentPage}
              />
            </PaginationWrap>
          )}
        </>
      ) : (
        <>
          <Title>Categories</Title>
          <FilterItemWrapper onClick={() => setViewAll(true)}>
            <FilterItem>
              View All&nbsp;<span>/ {totalProducts()}</span>
              <Arrow color="black" width={8} />
            </FilterItem>
          </FilterItemWrapper>
          {storeData?.store?.productCategories
            ?.filter((item) => item?.parent === '0')
            ?.map((item) => (
              <FilterItemWrapper key={item.slug} onClick={() => categoryItemClick(item.slug)}>
                <FilterItem>
                  {item?.name?.replace('&amp;', '&')}&nbsp;<span>/ {item?.productIds?.length}</span>
                  <Arrow color="black" width={8} />
                </FilterItem>
              </FilterItemWrapper>
            ))}
        </>
      )}

      <ShopNavigationMenuMobile />
    </Wrapper>
  );
};

export default Categories;
