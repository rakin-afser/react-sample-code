import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';
import {mainWhiteColor, headerShadowColor} from 'constants/colors';

export const HeaderWrapper = styled.div`
  top: 0;
  z-index: 1000;
  background: ${mainWhiteColor};
  box-shadow: inset 0px -1px 0px ${headerShadowColor};
  position: fixed;
  left: 0;
  right: 0;
`;

export const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  height: 47px;
  margin: 0 auto;
  padding: 0 16px;
  max-width: 1440px;
  background: ${mainWhiteColor};
  box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
  justify-content: space-between;
`;

export const LeftButton = styled.button`
  position: absolute;
  left: 14px;
  z-index: 100;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0;
  margin: 0 26px 0 0;
  background: none;
  border: none;
  cursor: pointer;
  outline: none;
`;

export const HeaderTitle = styled.div`
  font-family: 'Helvetica Neue', sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #000;
  position: absolute;
  left: 0;
  top: auto;
  width: 100%;
  text-align: center;
`;

export const RightWrapperMobile = styled.div`
  margin-left: auto;
  display: flex;
  align-items: center;
`;

export const OwnerMenu = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  display: flex;
  flex-wrap: wrap;
  z-index: 1000;
  opacity: 0;
  transition: all 0.3s ease;
  pointer-events: none;

  .bg {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: rgba(0, 0, 0, 0.6);
    z-index: 1001;
  }

  .inner {
    width: 100%;
    height: 100%;
    max-width: 83.2%;
    background: #fafafa;
    transition: all 0.3s ease;
    transform: translateX(-100%);
    position: relative;
    z-index: 1002;
    display: flex;
    flex-wrap: wrap;
    flex-direction: column;
    overflow-y: auto;
    overflow-x: hidden;
  }

  ${({show}) =>
    show &&
    `
    opacity: 1;
    pointer-events: all;

    .inner {
      transform: translateX(0);
    }
  `}
`;

export const LogoContainer = styled(Link)`
  padding-top: 6px;
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 82px;
  min-width: 82px;

  svg {
    max-width: 82px;
  }
`;

export const Divider = styled.div`
  flex-shrink: 0;
  width: 100%;
  height: ${({height}) => height || '10px'};
  background-color: ${({color}) => color || '#FAFAFA'};
`;

export const LogoutContainer = styled.div`
  background: #ffffff;
  padding: 0 16px;
  display: flex;
  align-items: center;
  height: 53px;
  min-height: 53px;
  border-top: 1px solid #efefef;
  border-bottom: 1px solid #efefef;
  margin-top: 22px;
  box-shadow: 0 0 0 50px #fafafa;
  margin-top: auto;
`;

export const LogoutButton = styled.button`
  background: #ffffff;
  border: 0;
  border-radius: 0;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  font-weight: 500;
  color: #8f8f8f;
  padding: 0;
  margin-left: 16px;
`;
