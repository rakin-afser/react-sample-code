import React, {useState} from 'react';
import { Link } from 'react-router-dom';

import {useApolloClient} from '@apollo/client';
import {useUser} from 'hooks/reactiveVars';

import DropdownMenu from 'containers/DealsPage/mobile/components/DropdownMenu';
import Icon from 'components/Icon';
import Footer from 'components/Header/components/Footer';
import UserBlock from 'components/Header/components/UserBlock';
import FadeOnMount from 'components/Transitions';
import ReferEarn from 'components/Header/components/ReferEarn';

import ArrowBack from 'assets/ArrowBack';
import LogoIcon from 'assets/LogoIcon';
import Notification from 'assets/Notification';

import {
  HeaderWrapper,
  HeaderContainer,
  LeftButton,
  HeaderTitle,
  RightWrapperMobile,
  LogoContainer,
  OwnerMenu,
  LogoutContainer,
  LogoutButton
} from 'containers/ShopPage/mobile/TopMenu/styled';
import {MenuItem, Arrow, DefaultLink} from 'components/Header/components/Menu/styled';

const TopMenu = ({title = '', backButtonClick = () => {}, isOwner = false}) => {
  const client = useApolloClient();
  const [user] = useUser();
  const [showDropdownMenu, setShowDropdownMenu] = useState(false);
  const [showOwnerMenu, setShowOwnerMenu] = useState(false);
  const [currentTab, setCurrentTab] = useState(null);

  const handleLogout = async (e) => {
    e.preventDefault();
    localStorage.removeItem('userId');
    localStorage.removeItem('token');
    localStorage.removeItem('woo-session');
    client.resetStore();
    window.location.href = '/';
  };

  const onClose = () => {
    setShowOwnerMenu(false);
    setCurrentTab(null);
  };

  if (isOwner) {
    return (
      <>
        <HeaderWrapper>
          <HeaderContainer>
            <LeftButton onClick={() => setShowOwnerMenu(true)}>
              <Icon type="burgerIcon" color="#000" isOpen={false} />
            </LeftButton>

            <LogoContainer
              to="/"
              onClick={onClose}
            >
              <LogoIcon />
            </LogoContainer>

            <RightWrapperMobile style={{ paddingTop: 5 }}>
              <Link to="/profile/notifications">
                <Notification color="#000" />
              </Link>
            </RightWrapperMobile>
          </HeaderContainer>
        </HeaderWrapper>
        <OwnerMenu show={showOwnerMenu}>
          <div className="bg" onClick={onClose} />
          <div className="inner">
            {currentTab === 'refer-earn' && (
              <FadeOnMount transition="all ease 0.3s" style={{flexGrow: '1'}}>
                <ReferEarn />
              </FadeOnMount>
            )}

            {!currentTab && (
              <>
                <UserBlock />

                <MenuItem color="#fff">
                  <Icon type="dashboard" width={24} height={24} />
                  <DefaultLink
                    href="https://seller.testSample.com/dashboard"
                    color="#000"
                    style={{fontWeight: 'normal', paddingLeft: 16}}
                  >
                    Dashboard
                  </DefaultLink>
                  <Arrow>
                    <Icon type="arrow" color="#000" height={14} width={12} style={{right: 18}}/>
                  </Arrow>
                </MenuItem>
                
                <MenuItem color="#fff" onClick={() => setCurrentTab('refer-earn')}>
                  <Icon type="coinsProfile" color="#999999" width={22} height={22} />
                  <DefaultLink
                    href="#"
                    color="#000"
                    onClick={(event) => {event.preventDefault()}}
                    style={{fontWeight: 'normal', paddingLeft: 16}}
                  >
                    Invite Friends
                  </DefaultLink>
                  <Arrow>
                    <Icon type="arrow" color="#000" height={14} width={12} style={{right: 18}}/>
                  </Arrow>
                </MenuItem>
                
                <MenuItem color="#fff">
                  <Icon type="gearThin" color="#999999" width={24} height={24}/>
                  <DefaultLink
                    href="https://seller.testSample.com/my-account"
                    color="#000"
                    style={{fontWeight: 'normal', paddingLeft: 16}}
                  >
                    Settings
                  </DefaultLink>
                  <Arrow>
                    <Icon type="arrow" color="#000" height={14} width={12} style={{right: 18}} />
                  </Arrow>
                </MenuItem>
              </>
            )}

            {user?.email ? (
              <LogoutContainer onClick={handleLogout}>
                <Icon type="logout" fill="#828282" />
                <LogoutButton>Log Out</LogoutButton>
              </LogoutContainer>
            ) : null}
            <Footer />
          </div>
        </OwnerMenu>
      </>
    );
  }

  return (
    <HeaderWrapper>
      {showDropdownMenu && <DropdownMenu closeMenu={() => setShowDropdownMenu(false)} />}
      <HeaderContainer>
        <LeftButton onClick={backButtonClick}>
          <ArrowBack stroke="#000" />
        </LeftButton>

        {!!title && <HeaderTitle>{title}</HeaderTitle>}

        <RightWrapperMobile>
          <Icon type="dotsVertical" onClick={() => setShowDropdownMenu(true)} isOpen={showDropdownMenu} />
        </RightWrapperMobile>
      </HeaderContainer>
    </HeaderWrapper>
  );
};

export default TopMenu;
