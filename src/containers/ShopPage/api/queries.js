import {gql} from '@apollo/client';

export const GET_SHOP_DATA = gql`
  query GetShopData($storeUrl: String!) {
    store(storeUrl: $storeUrl) {
      id
      name
      storeUrl
      banner
      followed
      gravatar
      rating
      totalFollowers
      totalFollowing
      vendorBiography
      storePpp
      storeTnc
      enableTnc
      storeTermsofService
      storeShippingPolicy
      storeReturnPolicy
      storeTags
      social {
        fb
        flickr
        gplus
        instagram
        linkedin
        pinterest
        twitter
        youtube
      }
      categories {
        name
        slug
      }
      address {
        city
        countryName
      }
      reviews {
        authorId
        authorNicename
        content
        date
        followed
        id
        image
        rating
        title
      }
      productCategories {
        name
        parent
        productIds
        slug
        id
      }
    }
  }
`;
