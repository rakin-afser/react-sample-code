import React, {Suspense, lazy, useEffect} from 'react';
import {useParams, useHistory} from 'react-router-dom';
import {useQuery} from '@apollo/client';
import {useWindowSize} from '@reach/window-size';

import Layout from 'containers/Layout';
import Loader from 'components/Loader';
import Error from 'components/Error';
import {GET_SHOP_DATA} from 'containers/ShopPage/api/queries';
import {useUser} from 'hooks/reactiveVars';
import SeoMeta from 'components/SeoMeta';
import storePlaceholderImg from 'images/placeholder.png';

const ShopPageFeedback = lazy(() => import('containers/ShopPageFeedback'));
const PostsPageDesktop = lazy(() => import('containers/ShopPage/desktop/PostsPage'));
const PostsPageMobile = lazy(() => import('containers/ShopPage/mobile/PostsPage'));
const ShopPageDesktop = lazy(() => import('containers/ShopPage/desktop/ShopPageDesktop'));
const ShopPageMobile = lazy(() => import('containers/ShopPage/mobile/ShopPageMobile'));
const ShopPageCategory = lazy(() => import('containers/ShopPage/mobile/Categories'));

const ShopPage = () => {
  const {shopUrl, page} = useParams();
  const {data, error, loading} = useQuery(GET_SHOP_DATA, {variables: {storeUrl: shopUrl}});
  const history = useHistory();
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [user] = useUser();
  const isOwner = data?.store?.id == user?.databaseId;

  useEffect(() => {
    if (!page) {
      history.push(`/shop/${shopUrl}/products`);
    }
  }, [page]);

  const tabChange = (slug) => {
    history.push(`/shop/${shopUrl}/${slug}`);
  };

  const getTitle = (props) => (
    <SeoMeta title={props?.store?.name} image={props?.store?.banner || storePlaceholderImg} />
  );

  const getPage = (props) => {
    const title = getTitle(props);

    switch (page) {
      case 'posts':
        return (
          <Layout title="Posts" showBackButton showSearchButton={false} showBasketButton={false}>
            {title}
            <PostsPageDesktop page={page} data={props} tabChange={tabChange} />
          </Layout>
        );
      case 'feedback':
        return (
          <Layout isFooterShort>
            {title}
            <ShopPageFeedback />
          </Layout>
        );
      case 'categories':
      case 'products':
      default:
        return (
          <Layout isFooterShort>
            {title}
            <ShopPageDesktop tabChange={tabChange} data={props} isOwner={isOwner} page={page} />
          </Layout>
        );
    }
  };

  const getMobilePage = (props) => {
    const title = getTitle(props);
    switch (page) {
      case 'posts':
        return (
          <>
            {title}
            <PostsPageMobile />
          </>
        );
      case 'categories':
        return (
          <>
            {title}
            <ShopPageCategory storeData={props} isOwner={isOwner} />
          </>
        );
      case 'feedback':
        return (
          <Layout isFooterShort>
            {title}
            <ShopPageFeedback />
          </Layout>
        );
      case 'products':
      default:
        return (
          <>
            {title}
            <ShopPageMobile storeData={props} isOwner={isOwner} />
          </>
        );
    }
  };

  if (error) return <Error />;
  if (loading) return <Loader />;

  return <Suspense fallback={<Loader />}>{isMobile ? getMobilePage(data) : getPage(data)}</Suspense>;
};

export default ShopPage;
