import React, {useState} from 'react';
import PropTypes from 'prop-types';
import qs from 'qs';
import {useHistory, useParams, useLocation} from 'react-router-dom';

import ShopInfo from 'components/ShopInfo';
import ProductCategoires from 'components/ProductCategoires';
import NavBar from 'components/ShopPage/NavBar';
import FadeOnMount from 'components/Transitions';
import InstagramWidget from 'components/InstagramWidget';
import {PageWrap, Container, Aside, Content} from './styled';
import Products from 'containers/ShopPage/desktop/Products';
import Search from 'containers/ShopPage/desktop/components/Search';
import usePagination from 'util/usePagination';

const tabs = [
  {slug: 'products', name: 'Products'},
  {slug: 'posts', name: 'Posts'}
];

const ShopPageDesktop = ({location, isOwner, data, tabChange}) => {
  const {shopName, page, categoryName, subCategoryName, subCategoryName2} = useParams();
  const {currentPage, currentCursor, goNextPage, goPrevPage, resetPagination} = usePagination();
  const [searchValue, setSearchValue] = useState('');
  const history = useHistory();
  const {search, pathname} = useLocation();
  const {filters: filtersValue} = qs.parse(search.replace('?', '')) || {};

  const goToCategory = (slug = '', level) => {
    let categoryPath;
    switch (level) {
      case 1:
        categoryPath = `/shop/${shopName}/categories/${slug}`;
        break;
      case 2:
        categoryPath = `/shop/${shopName}/categories/${categoryName}/${slug}`;
        break;
      case 3:
        categoryPath = `/shop/${shopName}/categories/${categoryName}/${subCategoryName}/${slug}`;
        break;
      default:
        categoryPath = `/shop/${shopName}/categories`;
    }
    resetPagination();
    history.push(categoryPath);
  };

  const setFilters = (filters) => {
    const queryString = qs.stringify({filters});
    resetPagination();
    history.push(`${pathname}?${queryString}`);
  };

  return (
    <FadeOnMount>
      <PageWrap>
        <Container>
          <Aside>
            <ShopInfo data={data} isOwner={isOwner} />
            <Search value={searchValue} onChange={(value) => setSearchValue(value)} />
            <ProductCategoires
              goToCategory={goToCategory}
              categories={data?.store?.productCategories}
              filtersValue={filtersValue}
              setFilters={setFilters}
              currentCategory={subCategoryName2 || subCategoryName || categoryName}
            />
            <InstagramWidget />
          </Aside>
          <Content>
            <NavBar tabs={tabs} currentTab={page === 'categories' ? 'products' : page} onChange={tabChange} />
            <Products
              searchValue={searchValue}
              storeData={data}
              location={location}
              filtersValue={filtersValue}
              pagination={{currentPage, currentCursor, goNextPage, goPrevPage, resetPagination}}
            />
          </Content>
        </Container>
      </PageWrap>
    </FadeOnMount>
  );
};

ShopPageDesktop.defaultProps = {
  location: undefined,
  isOwner: false
};

ShopPageDesktop.propTypes = {
  location: PropTypes.string,
  isOwner: PropTypes.bool,
  data: PropTypes.shape().isRequired
};

export default ShopPageDesktop;
