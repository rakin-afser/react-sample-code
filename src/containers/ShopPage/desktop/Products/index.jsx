import React, {useEffect, useRef, useState} from 'react';
import {useQuery} from '@apollo/client';
import {useHistory, useParams} from 'react-router-dom';

import Feedback from 'components/Feedback';
import Statistics from 'components/Statisctics';
import FeedbackCard from 'components/ShopPage/FeedbackCard';
import Star from 'assets/Star';
import Slider from 'react-slick';
import CardPopularPost from 'components/CardPopularPost';
import Visa from 'assets/ProductPage/Visa';
import Paypal from 'assets/ProductPage/Paypal';
import Mastercard from 'assets/ProductPage/Mastercard';
import AmericanExpress from 'assets/ProductPage/AmericanExpress';
import Pagination from 'components/PagePagination';
import ExpandedText from 'components/ExpandedText';
import {useLocation} from 'react-router';
import SortSelect from 'components/Select/SortSelect';
import About from 'components/About';
import chevron from './img/chevronDown.svg';
import chevronUp from './img/chevronUp.svg';
import ProductCard from 'components/Cards/ProductV2';
import {
  ProductWrap,
  InfoBar,
  SelectBlock,
  ProductsBlock,
  WideDivider,
  ReviewsTitle,
  FeedbackList,
  ShippingTitle,
  PaginationWrap,
  CustomerFeedbacksHeading,
  PopularPostContainer,
  StatisticsTitle,
  StatisticsCount,
  PopularPostTitle,
  InstagramPostsTitle,
  InstagramPosts,
  InstagramPost,
  Policies,
  PoliciesTitle,
  PoliciesSubTitle,
  PoliciesText,
  PoliciesSmallTitle,
  Payment,
  Cash,
  ShowMore,
  ChevronImg,
  SeeSellersPosts,
  StatisticsWrap
} from './styled.js';
import products from './exampleData/exampleData.js';
import feedbacks from './exampleData/exampleFeedbackData.js';
import popularPosts from './exampleData/examplePopularPosts.js';
import instagramPosts from './exampleData/exampleInstagramPosts.js';
import ReturnPolicies from './exampleData/ReturnPolicies.jsx';
import Error from 'components/Error';
import Loader from 'components/Loader';
import {PRODUCTS} from 'queries';
import NothingFound from 'components/NothingFound';
import Select from 'components/SelectTrue';
import {orderByValues, shopPageSortOptions} from 'constants/sorting';
import usePosts from 'hooks/usePosts';

const Products = ({storeData, searchValue, filtersValue = {}, pagination = {}}) => {
  const [sortBy, setSortBy] = useState(null);
  const history = useHistory();
  const productsPerPage = Number(storeData?.store?.storePpp) || 9;
  const {shopName, page, categoryName, subCategoryName, subCategoryName2} = useParams();
  const {currentPage, currentCursor, goNextPage, goPrevPage, resetPagination} = pagination;
  const {data, loading, error} = useQuery(PRODUCTS, {
    variables: {
      vendorId: +storeData?.store?.id,
      first: productsPerPage * currentPage,
      cursor: currentCursor,
      search: searchValue,
      categorySlug: subCategoryName2 || subCategoryName || categoryName,
      minPrice: Number(filtersValue?.price?.[0]),
      maxPrice: Number(filtersValue?.price?.[1]),
      orderBy: {value: orderByValues[sortBy]}
    }
  });
  const {currentData, loading: postsLoading, error: postsError} = usePosts({
    first: 9,
    variables: {
      authorId: +shopName,
      orderby: {value: {field: 'COMMENT_COUNT', order: 'DESC'}} // most popular
    }
  });
  const {storeTnc, enableTnc, reviews, storeTermsofService, storeShippingPolicy, storeReturnPolicy} = storeData?.store;

  const onSort = (value) => {
    setSortBy(value);
  };

  const setRating = (value) => {
    const Rating = [];
    for (let i = 0; i < value; i++) {
      Rating.push(<Star key={i} fill="#FFC131" height={13.33} width={13.46} />);
    }
    if (value < 5) {
      for (let i = 0; i < 5 - value; i++) {
        Rating.push(<Star key={i} fill="#efefef" height={13.33} width={13.46} />);
      }
    }
    return Rating;
  };

  const reviewsRef = useRef(null);

  const [selectedSortBy, setSelectedSortBy] = useState([]);
  const [isSeeMore, setIsSeeMore] = useState(false);
  const [productsData, setProductsData] = useState(products);

  const location = useLocation();

  const handleUserLike = (productId) => {
    setProductsData(
      productsData.map((productData) => {
        if (productId === productData.id) {
          productData.isLiked = !productData.isLiked;
        }

        return productData;
      })
    );
  };

  const getProducts = () => {
    return data?.products?.nodes?.map((item, index) => (
      <ProductCard
        slug={item?.slug}
        name={item?.name}
        shippingType={item?.shippingType}
        img={item?.image?.sourceUrl}
        regularPrice={item?.regularPrice}
        salePrice={item?.salePrice}
        databaseId={item?.databaseId}
        isDiscount={item?.regularPrice && item?.salePrice}
        isLiked={item?.isLiked}
        isWished={item?.isWished}
        key={index}
        item={item}
      />
    ));
  };

  const getFeedback = () => {
    return feedbacks.map((feedback, index) => {
      return <FeedbackCard feedback={feedback} key={index} />;
    });
  };

  const getPopularCards = () => {
    return currentData.map((post) => {
      return <CardPopularPost key={post.id} content={post} margin="0" />;
    });
  };

  const getInstagramPosts = () => {
    return instagramPosts.map((post, index) => {
      return <InstagramPost key={index} src={post.image} />;
    });
  };

  if (error) return <Error />;

  return (
    <ProductWrap>
      <InfoBar>
        <span>{data?.products?.pageInfo?.total} Products</span>
        <SelectBlock>
          <Select
            onChange={onSort}
            options={shopPageSortOptions}
            valueKey="value"
            value={sortBy || 'Sort by'}
            labelKey="name"
          />
        </SelectBlock>
      </InfoBar>
      <ProductsBlock loading={loading}>
        {loading ? <Loader wrapperWidth="100%" wrapperHeight="70vh" /> : getProducts()}
      </ProductsBlock>
      {!data?.products?.pageInfo?.total && !loading && <NothingFound hideHelpText hideAdditionalText />}
      {!!data?.products?.pageInfo?.total && Math.ceil(data?.products?.pageInfo?.total / productsPerPage) > 1 && (
        <PaginationWrap>
          <Pagination
            totalPages={Math.ceil(data?.products?.pageInfo?.total / productsPerPage)}
            prevPage={() => goPrevPage(data?.products?.pageInfo)}
            nextPage={() => goNextPage(data?.products?.pageInfo)}
            currentPage={currentPage}
          />
          <WideDivider />
        </PaginationWrap>
      )}
      {/* <InstagramPostsTitle>Instagram Posts</InstagramPostsTitle>
      <InstagramPosts>
        <Slider variableWidth={true} arrows={true} slidesToScroll={5} dots={true}>
          {getInstagramPosts()}
        </Slider>
      </InstagramPosts> */}
      {!postsLoading && (
        <>
          <WideDivider />
          <PopularPostTitle>Popular Posts</PopularPostTitle>
          <PopularPostContainer>
            <Slider slidesToShow={3} arrows slidesToScroll={4} dots={false}>
              {getPopularCards()}
            </Slider>
          </PopularPostContainer>
        </>
      )}

      {/* <StatisticsWrap>
        <StatisticsTitle>
          Customer Feedback <StatisticsCount>({reviews?.length || 0})</StatisticsCount>
        </StatisticsTitle>
        <Statistics setRating={setRating} />
      </StatisticsWrap> */}
      {!!reviews?.length && (
        <>
          <ReviewsTitle>All Reviews</ReviewsTitle>
          <Feedback setRating={setRating} data={reviews} isForShopPage />
          {/* <CustomerFeedbacksHeading photos>Photos from Reviews</CustomerFeedbacksHeading>
      <FeedbackList>
        <Slider slidesToShow={4} arrows={true} slidesToScroll={4} dots={true}>
          {getFeedback()}
        </Slider>
      </FeedbackList> */}
        </>
      )}

      <About data={storeData} />
      {!!storeTermsofService && (
        <Policies>
          <PoliciesTitle>Shop Terms of Service</PoliciesTitle>
          <PoliciesText dangerouslySetInnerHTML={{__html: storeTermsofService}} />
          <WideDivider />
        </Policies>
      )}
      {!!storeTnc && (
        <Policies>
          <PoliciesTitle>Shop T&C</PoliciesTitle>
          <PoliciesText dangerouslySetInnerHTML={{__html: storeTnc}} />
          <WideDivider />
        </Policies>
      )}
      <Policies>
        <PoliciesTitle>Policies</PoliciesTitle>
        {/* <PoliciesSubTitle>Shop Policies</PoliciesSubTitle>
        <PoliciesText>
          <ExpandedText text={shopPolicies} divider={151} />
        </PoliciesText>
        <WideDivider /> */}
        <PoliciesSubTitle>Shipping Policies</PoliciesSubTitle>
        <PoliciesText>
          {/* <ShippingTitle>Processing time</ShippingTitle>
          <span>The time I need to prepare an order for shipping varies. For details, see individual items.</span> */}
          <ExpandedText text={storeShippingPolicy} divider={151} />
        </PoliciesText>
        <WideDivider />
        <PoliciesSubTitle>Payments Options</PoliciesSubTitle>
        <PoliciesSmallTitle>Secure options</PoliciesSmallTitle>
        <PoliciesText>
          testSample keeps your payment information secure. testSample shops never receive your credit card information.
        </PoliciesText>
        <Payment>
          <Mastercard />
          <Visa />
          <Paypal />
          <AmericanExpress />
          <Cash>Cash</Cash>
        </Payment>
        <WideDivider />
        <PoliciesSubTitle>Returns & Exchanges</PoliciesSubTitle>
        {/* <PoliciesSmallTitle>I gladly accept returns and exchanges</PoliciesSmallTitle> */}
        <PoliciesText>
          <ExpandedText text={storeReturnPolicy} divider={151} />
        </PoliciesText>
        <WideDivider />
        <SeeSellersPosts>See Sellers Posts</SeeSellersPosts>
      </Policies>
    </ProductWrap>
  );
};

export default Products;
