import {mainFont} from 'constants/fonts';
import styled, {css} from 'styled-components/macro';

export const InfoBar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  > span {
    display: inline-block;
    padding-top: 10px;
    padding-left: 23px;
    font-size: 12px;
    color: #464646;
  }
`;

export const SearchBlock = styled.div`
  width: 65%;
  .ant-select {
    display: none;
  }
  .svtoe {
    max-width: 100%;
    margin: 0;
  }
  .svtoe .ant-input {
    max-width: 90%;
  }
`;

export const SelectBlock = styled.div`
  width: 100%;
  max-width: 180px;
  margin-left: auto;
  font-weight: 500;

  &&& {
    .ant-select {
      color: #000;

      &-selection {
        margin-right: 10px;
        border: none !important;
        box-shadow: none;
      }

      &-selection__placeholder {
        color: #000;
      }

      &-arrow {
        right: 0;
      }

      &-arrow-icon {
        color: #000;
      }
    }
  }
`;

export const ProductWrap = styled.div`
  display: block;
  width: 100%;
  max-width: 900px;
  padding-left: 32px;
  & .slick-dots li.slick-active button:before {
    font-size: 10px;
    opacity: 1;
  }
  & .slick-dots li button:before {
    font-size: 10px;
  }
  & .slick-dots li {
    margin: 0px;
  }
  & .slick-slider .slick-prev:before,
  .slick-slider .slick-next:before {
    opacity: 1;
    background-size: 14.58px 25px;
  }
`;

export const StatisticsTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: bold;
  font-size: 24px;
  line-height: 29px;
  display: flex;
  align-items: center;
  color: #000000;
  margin-bottom: 31px;
`;

export const StatisticsCount = styled.span`
  display: inline-block;
  margin-left: 17px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  display: flex;
  align-items: center;
  letter-spacing: -0.016em;
  color: #7a7a7a;
`;

export const ProductsBlock = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(262px, 1fr));
  grid-gap: 20px;
  justify-items: center;
  width: 100%;
  max-width: 868px;
  margin-top: 18px;
  > div {
    margin-bottom: 42px;
  }
  ${({loading}) =>
    loading &&
    css`
      display: flex;
    `}
`;

export const LoadButton = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;

export const Divider = styled.div`
  max-width: 500px;
  width: 100%;
  margin: 24px 0;
  height: 2px;
  background: #e4e4e4;
  border-radius: 2px;
`;

export const ProductCounter = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #545454;
  margin-bottom: 32px;
`;

export const WideDivider = styled.div`
  max-width: 100%;
  width: 100%;
  margin: 24px 0;
  height: 1px;
  background: #e4e4e4;
  border-radius: 2px;
`;

export const ReviewsTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin-top: 88px;
  margin-bottom: 0;
`;

export const CustomerFeedbacks = styled.div`
  padding: 24px 32px;
  margin: 24px 0 0 0;
  background: #fff;
`;

export const CustomerFeedbacksHeading = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin-bottom: 22px;
  ${({number}) =>
    number
      ? `
		display: inline;
		font-size: 16px;
  	line-height: 140%;
		font-weight: 400;
		color: #7A7A7A;
		padding-left: 17px;
		letter-spacing: -0.016em;
	`
      : null};
  ${({photos}) =>
    photos
      ? `
		display: block;
		margin: 80px 0 16px 0;
	`
      : null};
`;

export const FeedbackList = styled.div`
  padding: 0 25px;

  & div.slick-track {
    padding-bottom: 12px;
  }
  & .slick-slider .slick-dots {
    bottom: auto;
    top: -48px;
    text-align: right;
    right: -2px;
    left: auto;
    bottom: auto !important;
  }
  .slick-slider .slick-dots li.slick-active button:before {
    color: #ed484f;
  }
  & .slick-slider .slick-prev {
    left: -25px;
  }
  & .slick-slider .slick-next {
    right: -20px;
  }
  margin-bottom: 46px;
`;

export const PopularPostContainer = styled.div`
  width: 100%;
  max-width: 719px;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 65px;
  margin-top: 37px;
  .slick-slider .slick-dots {
    text-align: right;
    bottom: auto;
    top: -56px;
    right: 41px;
    left: auto;
    bottom: auto !important;
  }
  .slick-slider .slick-dots li.slick-active button:before {
    color: #ed484f;
  }
`;

export const PopularPostTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 22px;
  line-height: 27px;
  color: #000000;
  margin-bottom: 24px;
`;

export const InstagramPostsTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 22px;
  line-height: 27px;
  color: #000000;
  margin-bottom: 24px;
`;

export const InstagramPosts = styled.div`
  width: 770px;
  margin: 0 auto;
  .slick-slider .slick-dots {
    text-align: right;
    bottom: auto;
    top: -56px;
    right: -6px;
    left: auto;
    bottom: auto !important;
    width: auto;
  }
  .slick-slider .slick-dots li.slick-active button:before {
    color: #ed484f;
  }
  & .slick-slider .slick-next {
    right: -35px;
  }
  & .slick-slider .slick-prev {
    left: -47px;
  }
  margin-bottom: 56px;
`;

export const InstagramPost = styled.img`
  border-radius: 5px;
  width: 185px;
  padding-right: 4px;
`;

export const Policies = styled.div`
  padding: 70px 0 0 0;
  width: 100%;
`;

export const PoliciesTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 29px;
  color: #000000;
  margin-bottom: 24px;
  margin-top: 0;
`;

export const PoliciesSubTitle = styled.h4`
  font-family: Helvetica Neue;
  max-width: 600px;
  width: 100%;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 200%;
  letter-spacing: -0.024em;
  color: #000000;
  margin-bottom: 16px;
  margin-top: 0;
`;

export const PoliciesText = styled.div`
  max-width: 600px;
  width: 100%;
  line-height: 200%;
  color: #000000;
  > div {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    text-align: left;
    line-height: 200%;
    color: #545454;
    padding: 0;
  }
  & b {
    font-weight: 500;
    color: #000;
  }
  h5 {
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: 500;
    font-size: 14px;
    line-height: 200%;
    color: #000000;
  }
`;

export const RedText = styled.span`
  color: #ed484f;
`;

export const PoliciesSmallTitle = styled.h5`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const ShippingTitle = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
  margin-bottom: 0;
  & ~ div b {
    color: #000;
    font-weight: 500;
  }
`;

export const Payment = styled.div`
  display: flex;
  align-items: center;
  & svg {
    margin-right: 20px;
    &:last-child {
      margin-right: 0;
    }
  }
`;

export const Cash = styled.span`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  font-size: 9px;
  line-height: 140%;
  text-align: center;
  color: #000000;
`;

export const ShowMore = styled.button`
  display: flex;
  outline: none;
  border: none;
  background: #fff;
  align-items: center;
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: 500;
  padding-left: 0;
  font-size: 14px;
  line-height: 140%;
  color: #ea2c34;
`;

export const ChevronImg = styled.img`
  margin-left: 12px;
`;

export const SeeSellersPosts = styled.button`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  color: #ed484f;
  border: 1px solid #ed484f;
  box-sizing: border-box;
  border-radius: 24px;
  outline: none;
  cursor: pointer;
  margin: 32px auto 0;
  background: #fff;
  width: 186px;
  height: 36px;
`;

export const StatisticsWrap = styled.div`
  > div {
    padding-left: 0;
  }
`;

export const PaginationWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  > div {
    margin-top: 0;
  }
`;
