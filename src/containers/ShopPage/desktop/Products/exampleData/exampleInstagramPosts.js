import instagram1 from '../img/instagram1.png';
import instagram2 from '../img/instagram2.png';
import instagram3 from '../img/instagram3.png';
import instagram4 from '../img/instagram4.png';

export default [
  {image: instagram1},
  {image: instagram2},
  {image: instagram3},
  {image: instagram4},
  {image: instagram1},
  {image: instagram2},
  {image: instagram3},
  {image: instagram4},
  {image: instagram1},
  {image: instagram2},
  {image: instagram3},
  {image: instagram4}
];

export const instaPosts = [
  {
    image: 'src/containers/ShopPage/Products/img/instagram1.png'
  },
  {
    image: 'src/containers/ShopPage/Products/img/instagram2.png'
  },
  {
    image: 'src/containers/ShopPage/Products/img/instagram3.png'
  }
];
