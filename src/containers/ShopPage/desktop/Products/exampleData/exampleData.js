import product1 from '../img/Img.png';
import product2 from '../img/Img-1.png';
import product3 from '../img/Img-2.png';
import product4 from '../img/Img-3.png';
import product5 from '../img/Img-4.png';
import product6 from '../img/Img-5.png';
import product7 from '../img/Img-6.png';
import product8 from '../img/Img-7.png';
import product9 from '../img/Img-8.png';
import product10 from '../img/Img-9.png';
import product11 from '../img/Img-10.png';
import product12 from '../img/Img-11.png';

export default [
  {
    id: 1,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 132,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product1,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 2,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product2,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 3,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: null,
    countOfViews: 783,
    imgSrc: product3,
    sale: null,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 4,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product4,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 5,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 132,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product5,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 6,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product6,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 7,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: null,
    countOfViews: 783,
    imgSrc: product7,
    sale: null,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 8,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product8,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 9,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 132,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product9,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 10,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product10,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 11,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: null,
    countOfViews: 783,
    imgSrc: product11,
    sale: null,
    likesCount: 142,
    isWished: false,
    isLiked: false
  },
  {
    id: 12,
    title: 'Quieen Sweetshirt Funny Lou',
    newPrice: 120,
    oldPrice: 243,
    countOfViews: 783,
    imgSrc: product12,
    sale: -40,
    likesCount: 142,
    isWished: false,
    isLiked: false
  }
];
