import React from 'react';

const ReturnPolicies = () => {
  return (
    <div>
      <p>Ship items back within: <b>14 days of delivery</b></p>
      <h5>I don't accept cancellations</h5>
      <p>But please contact me if you have any problems with your order</p>
      <h5>The following items can't be returned or exchanged</h5>
      <p>Because of the nature of these items, unless they arrive damaged or defective, I can't accept returns for:</p>
      <ul style={{
        listStyleType: 'none'
      }}>
        <li>- Custom or personalized orders</li>
        <li>- Perishable products (like food or flowers)</li>
        <li>- Digital downloads </li>
        <li>- Intimate items (for health/hygiene reasons)</li>
        <li>- Items on sale</li>
      </ul>
      <h5>Conditions & Terms</h5>
      <p>Buyers are responsible for return shipping costs. If the item is not returned in its original condition, the buyer is responsible for any loss in value.</p>
    </div>
  )
}

export default ReturnPolicies;
