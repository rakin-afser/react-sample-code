import storLogo from '../img/avatar.png';
import posterImage1 from '../img/poster1.png';
import posterImage2 from '../img/poster2.png';
import posterImage3 from '../img/poster3.png';
import vieweravatar1 from '../img/viewer1.png';
import vieweravatar2 from '../img/viewer2.png';
import vieweravatar3 from '../img/viewer3.png';

export default [
  {
    id: 101,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage1,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 102,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage2,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 103,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage3,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 104,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage1,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 105,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage2,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 106,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage3,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 107,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage2,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 108,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage1,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  },
  {
    id: 109,
    time: '12 minutes ago',
    logo: storLogo,
    name: 'Zara',
    poster: posterImage3,
    text: 'Queen Sweatshirt Funny Lou',
    views: 890,
    likes: 701,
    viewed: {
      moreViews: 7,
      viewer1: vieweravatar1,
      viewer2: vieweravatar2,
      viewer3: vieweravatar3
    }
  }
];
