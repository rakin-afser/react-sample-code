import React, {useState, Fragment} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import Slider from 'react-slick';
import {useQuery} from '@apollo/client';

import Post from 'components/Post';
import FeaturedProductCard from 'components/Cards/FeaturedProductCard';
import popularProductData from 'components/Cards/PopularProductCard/exampleData.js';
import PopularProductCard from 'components/Cards/PopularProductCard';
import EmptyFeed from 'components/EmptyFeed';
import EmptyProduct from 'components/EmptyProduct';
import {
  Wrap,
  PostColumn,
  PopularProductsColumn,
  PopularProductsContainer,
  Title,
  FeaturedProductsWrap,
  TitleSmall,
  FeaturedContainer,
  Links,
  Link,
  DotDivider
  // FeaturedProductsLoading
} from './styled.js';
import Loader from 'components/Loader/index.jsx';

const PostsContent = ({forFeed, data = [], withPopularProducts, showReportPopup, loading, endRefEl}) => {
  const [visible, setVisible] = useState([]);
  const active = visible?.length === 0 ? null : Math.min(...visible);

  const {data: productsData, loading: productsLoading} = {};
  //  useQuery(PRODUCTS_FEED_FEATURED, {
  //   variables: {
  //     where: {featured: true},
  //     first: 9
  //   }
  // });

  // const getFeaturedProductsWrapper = () => {
  // if (productsLoading)
  //   return (
  //     <FeaturedProductsLoading>
  //       {productLoadingList.map((id) => (
  //         <EmptyProduct key={id} />
  //       ))}
  //     </FeaturedProductsLoading>
  //   );
  //   if (productsLoading) {
  //     return null;
  //   }

  //   return (
  //     <FeaturedProductsWrap order={4}>
  //       <NavLink
  //         exact
  //         to={{
  //           pathname: '/shop',
  //           hash: '#selectProduct',
  //           state: {
  //             label: 'Featured Products'
  //           }
  //         }}
  //       >
  //         <TitleSmall>Featured Products</TitleSmall>
  //       </NavLink>
  //       <FeaturedContainer>
  //         <Slider slidesToShow={2} arrows slidesToScroll={2} dots>
  //           {productsData?.products?.nodes?.map((product, i) => (
  //             <FeaturedProductCard key={i} content={product} margin="0 4px" />
  //           ))}
  //         </Slider>
  //       </FeaturedContainer>
  //     </FeaturedProductsWrap>
  //   );
  // };

  const getPosts = (posts) => {
    // order every third post with +1 value.
    // Will be able to insert data from FeaturedProducts;
    let counter = 0;

    return (
      <>
        {posts?.map((post, i) => {
          counter += 1;

          return (
            <Fragment key={post?.id}>
              {!loading && posts.length - 2 === i && endRefEl}
              {/* {counter === 3 && getFeaturedProductsWrapper()} */}
              <Post
                active={i === active}
                order={i}
                visible={visible}
                setVisible={setVisible}
                forFeed
                showReportPopup={showReportPopup}
                data={post}
              />
            </Fragment>
          );
        })}
        {loading && <Loader wrapperWidth="100%" wrapperHeight="100%" order={counter} />}
      </>
    );
  };

  const getPopularProducts = (products) => {
    return products.map((product) => <PopularProductCard key={product.id} product={product} />);
  };

  return (
    <Wrap forFeed={forFeed} additionalStyles={withPopularProducts ? {marginLeft: '32px'} : null}>
      <PostColumn>{getPosts(data)}</PostColumn>
      {withPopularProducts && (
        <PopularProductsColumn>
          <PopularProductsContainer>
            <Title>Popular Products</Title>
            {getPopularProducts(popularProductData)}
          </PopularProductsContainer>
          <Links>
            <Link>About</Link>
            <DotDivider>·</DotDivider>
            <Link>Sell on testSample</Link>
            <DotDivider>·</DotDivider>
            <Link>Contact</Link>
            <DotDivider>·</DotDivider>
            <Link to="/privacy-policy">Privacy Policy</Link>
            <DotDivider>·</DotDivider>
            <Link>Terms of Service</Link>
          </Links>
        </PopularProductsColumn>
      )}
    </Wrap>
  );
};

PostsContent.defaultProps = {
  data: [],
  withPopularProducts: true,
  showReportPopup: () => {},
  loading: false
};

PostsContent.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({})),
  withPopularProducts: PropTypes.bool,
  showReportPopup: PropTypes.func,
  loading: PropTypes.bool
};

export default PostsContent;
