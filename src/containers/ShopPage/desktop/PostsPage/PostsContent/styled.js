import styled, {keyframes} from 'styled-components/macro';

export const Wrap = styled.div`
  max-width: 885px;
  display: flex;
  justify-content: space-between;
  ${({additionalStyles}) => (additionalStyles ? {...additionalStyles} : null)}

  ${({forFeed}) =>
    forFeed &&
    `
    margin-top: 20px;
  `};
`;

export const PostColumn = styled.div`
  max-width: 576px;
  width: 100%;
  background: #fff;
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const PopularProductsColumn = styled.div`
  max-width: 292px;
  width: 100%;
  margin-left: 16px;
`;

export const PopularProductsContainer = styled.div`
  width: 100%;
  background: #ffffff;
  border-radius: 4px;
  border: 1px solid #e4e4e4;
  padding: 16px 15.7534% 22px;
`;

export const Title = styled.h2`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  display: flex;
  align-items: center;
  color: #000000;
  margin-top: 24px;
`;

export const FeaturedProductsWrap = styled.div`
  order: ${({order}) => (order ? order : null)};
  background: #ffffff;
  border: 1px solid #e4e4e4;
  box-sizing: border-box;
  border-radius: 4px;
  margin-bottom: 24px;
  width: 100%;
  padding: 18px 20px 28px 20px;
`;

export const FeaturedProductsWrapMobile = styled.div`
  order: ${({order}) => (order ? order : null)};
  background: #ffffff;
  border: 1px solid #e4e4e4;
  box-sizing: border-box;
  width: 100%;
`;

export const TitleSmall = styled.div`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 16px;
  line-height: 140%;
  color: #131313;
  margin-bottom: 24px;
  margin-left: 8px;
`;

export const FeaturedContainer = styled.div`
  width: 100%;
  & .slick-slider .slick-dots {
    bottom: -21px !important;
    & li {
      margin: 0 -2px;
    }
  }
  & .slick-slider .slick-prev {
    left: -21px;
  }
  & .slick-slider .slick-next {
    right: -12px;
  }
  .slick-slider .slick-prev:before,
  .slick-slider .slick-next:before {
    background-size: 9px 15px;
    background-position: center;
  }
`;

export const Divider = styled.div`
  background: #e4e4e4;
  max-width: 416px;
  width: 100%;
  height: 2px;
  display: block;
  margin: 17px auto 10px;
`;

export const ShowAll = styled.button`
  font-family: Helvetica;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  display: flex;
  align-items: center;
  text-align: center;
  width: 150px;
  margin: 0 auto;
  color: #ed494f;
  outline: none;
  border: none;
  background: #fff;
`;

export const SubTitle = styled.p`
  margin-bottom: 0;
  margin-top: 3px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 132%;
  color: #a7a7a7;
`;

export const Links = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: baseline;
  margin-top: 16px;
`;

export const Link = styled.a`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 28px;
  color: #666666;
  text-decoration: none;
  transition: 0.2s ease;
  &:hover {
    text-decoration: none;
    color: #4a90e2;
  }
`;

export const DotDivider = styled.div`
  display: block;
  margin: 0 4px;
  color: black;
  font-weight: 700;
`;

export const FeaturedProductsLoading = styled.div`
  display: flex;
`;
