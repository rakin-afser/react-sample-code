import React from 'react';
import {PageWrap, Container, Aside, Content} from './styled';
import FeaturedProduct from 'components/ShopInfo';
import ProductCategoires from 'components/ProductCategoires';
import hazelGloryImage from './img/hazel_glory.svg';
import PostsContent from './PostsContent';
import NavBar from 'components/ShopPage/NavBar';
import InstagramPhotos from 'components/InstagramPhotos';

const PostsPageDesktop = ({isOwner}) => {
  return (
    <PageWrap>
      <Container>
        <Aside>
          <FeaturedProduct isOwner={isOwner} />
          <InstagramPhotos />
        </Aside>
        <Content>
          <NavBar />
          <PostsContent />
        </Content>
      </Container>
    </PageWrap>
  );
};

export default PostsPageDesktop;
