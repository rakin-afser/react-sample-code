import styled from 'styled-components';

export const PageWrap = styled.div`
  width: 100%;
  @media (max-width: 1400px) {
    padding: 0 50px 0;
  }
`;

export const Container = styled.div`
  padding: 34px 0 0 0;
  max-width: 1200px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Aside = styled.div`
  display: flex;
  flex-direction: column;
  width: 25%;
  @media (max-width: 960px) {
    width: 32%;
  }
`;

export const Content = styled.div`
  width: 75%;
  max-width: none;
  @media (max-width: 960px) {
    width: 68%;
  }
`;

export const Head = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: center;
  margin-left: 15px;
`;

export const HeadImage = styled.img``;

export const HeadTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  text-align: center;
  color: #000000;
  margin-top: 16px;
`;

export const VerticalDivider = styled.div`
  width: 1px;
  height: 40px;
  background: #e4e4e4;
`;
