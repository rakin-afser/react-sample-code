import React from 'react';
import {useParams} from 'react-router-dom';

import FadeOnMount from 'components/Transitions';
import {PageWrap, Container, Aside, Content} from './styled';
import ShopInfo from 'components/ShopInfo';
import PostsContent from './PostsContent';
import NavBar from 'components/ShopPage/NavBar';
import InstagramPhotos from 'components/InstagramPhotos';
import usePosts from 'hooks/usePosts';
import useInfiniteScroll from 'util/useInfiniteScroll';

const tabs = [
  {slug: 'products', name: 'Products'},
  {slug: 'posts', name: 'Posts'}
];

const PostsPageDesktop = ({isOwner, data, tabChange}) => {
  const {shopUrl, page} = useParams();
  const {currentData, allData, loading, error, postsPagination} = usePosts({
    first: 9,
    variables: {authorName: shopUrl}
  });
  const {nextPage, currentPage} = postsPagination || {};
  useInfiniteScroll({onLoad: nextPage, offset: 1200});

  return (
    <FadeOnMount>
      <PageWrap>
        <Container>
          <Aside>
            <ShopInfo data={data} isOwner={isOwner} />
            <InstagramPhotos />
          </Aside>
          <Content className="some-class-name-custom">
            <NavBar tabs={tabs} currentTab={page} onChange={tabChange} />
            <PostsContent data={allData} />
          </Content>
        </Container>
      </PageWrap>
    </FadeOnMount>
  );
};

export default PostsPageDesktop;
