import {Button} from 'antd';
import styled from 'styled-components';

export const Wrap = styled.div`
  margin-top: 26px;
  position: relative;
  padding-left: 49px;
  background-color: #efefef;
  overflow: hidden;
  border-radius: 4px;
`;

export const Input = styled.input`
  border: none;
  outline: none;
  background-color: transparent;
  width: 100%;
  padding: 5px 10px 5px 0;
  height: 40px;
  caret-color: #ed484f;
  color: #000;

  &::placeholder {
    transform: translateX(0);
    transition: transform 0.2s ease;
  }

  &:focus::placeholder {
    transform: translateX(-100%);
  }
`;

export const SearchBtn = styled(Button)`
  &&& {
    background-color: transparent;
    border: none;
    min-width: 24px;
    height: 24px;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    color: #999;

    position: absolute;
    left: 0;
    top: 50%;
    transform: translateY(-50%);

    &:hover {
      color: #ed484f;
    }
  }
`;

export const Clear = styled(Button)`
  &&& {
    position: absolute;
    right: 10px;
    top: 50%;
    transform: translateY(-50%);
    background-color: #cccccc;
    border: none;
    padding: 0;
    min-width: 16px;
    height: 16px;
    display: inline-flex;
    justify-content: center;
    align-items: center;

    &:hover {
      background-color: #ed484f;
    }
  }
`;
