import React, {useState} from 'react';
import SearchIcon from 'assets/SearchIcon';
import {Wrap, Input, SearchBtn, Clear} from './styled';
import Cross from 'assets/Cross';

const Search = ({onChange = () => {}}) => {
  const [value, setValue] = useState('');

  return (
    <Wrap>
      <SearchBtn>
        <SearchIcon fill="currentColor" />
      </SearchBtn>
      <Input
        placeholder="Search Store"
        type="search"
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onBlur={() => onChange(value)}
      />
      {value && (
        <Clear
          onClick={() => {
            setValue('');
            onChange('');
          }}
        >
          <Cross />
        </Clear>
      )}
    </Wrap>
  );
};

export default Search;
