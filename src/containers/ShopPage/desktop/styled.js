import styled from 'styled-components';

export const PageWrap = styled.div`
  width: 100%;
  @media (max-width: 1400px) {
    padding: 0 50px 0;
  }
`;

export const Container = styled.div`
  padding: 34px 0 0 0;
  max-width: 1200px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const Aside = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 300px;
  width: 25%;
  min-width: 240px;
  @media (max-width: 960px) {
    width: 32%;
  }
`;

export const Content = styled.div`
  width: 75%;
  @media (max-width: 960px) {
    width: 68%;
  }
`;

export const Head = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
  align-items: center;
  margin-left: 15px;
`;

export const HeadImage = styled.img``;

export const HeadTitle = styled.h3`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  text-align: center;
  color: #000000;
  margin-top: 16px;
`;

export const NavBar = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 21px rgba(0, 0, 0, 0.06);
  border-radius: 4px;
  max-width: 885px;
  width: 100%;
  margin-left: 15px;
  display: flex;
  justify-content: space-around;
  height: 40px;
  align-items: center;
  margin-bottom: 30px;
  & a {
    flex: 1;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 140%;
    text-align: center;
    color: #7a7a7a;
  }
`;

export const Hover = styled.span`
  &:hover {
    color: '#000',
    borderBottom: '2px solid #000000',
    height: "100%",
    verticalAllign: 'middle',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
`;

export const VerticalDivider = styled.div`
  width: 1px;
  height: 40px;
  background: #e4e4e4;
`;
