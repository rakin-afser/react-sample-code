import {gql} from '@apollo/client';

export const shippingAddressFragment = gql`
  fragment ShippingAddressFragment on ShippingAddress {
    addDirections
    address1
    addressNickname
    apartment
    block
    city
    country
    district
    firstName
    floor
    id
    lastName
    map
    phone
    postCode
    road
    searchRoad
    type
    build
    selected
    primary
  }
`;
