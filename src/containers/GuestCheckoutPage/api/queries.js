import {gql} from '@apollo/client';
import {shippingAddressFragment} from './fragments';

export const GET_SHIPPING_ADDRESSES = gql`
  ${shippingAddressFragment}
  query {
    testSampleCheckout(action: get) {
      shippingAddresses {
        ...ShippingAddressFragment
      }
    }
  }
`;

export const GET_ORDER = gql`
  query($id: ID) {
    order(id: $id, idType: DATABASE_ID) {
      orderNumber
      needsPayment
      needsProcessing
      needsShippingAddress
      paymentMethod
      paymentMethodTitle
      shippingTotal
      status
      subtotal
      transactionId
      id
      date
      total
      shipping {
        address1
        address2
        city
        company
        country
        email
        firstName
        lastName
        phone
        postcode
        state
      }
      shippingAddressMapUrl
    }
  }
`;
