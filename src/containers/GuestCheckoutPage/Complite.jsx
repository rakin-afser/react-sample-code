import React, { useState } from 'react';
import {Link, useHistory} from 'react-router-dom';

import {
  Wrapper, CommpliteWrapper
} from './styled';
import ShareBlock from './components/ShareBlock';
import AddPurchases from './components/AddPurchases';
import Icon from 'components/Icon';

const order = 780342846;
const track = {
  name: 'TVL561TRS',
  link: '/'
};

const Complite = () => {
  const [purchases, setPurchases] = useState([]);
  const [loading, setLoading] = useState(false);
  const [share, setShare] = useState(false);
  const {push} = useHistory();

  const addPurchases = (index) => {
    setLoading(true);

    setTimeout(() => {
      if (purchases.includes(index)) {
        const newArray = purchases;
        const key = purchases.findIndex(item => item === index);

        newArray.splice(key, 1);

        setPurchases(newArray);
        setLoading(false);
      } else {
        setPurchases([...purchases, index]);
        setLoading(false);
      }
    }, 0);
  };

  return (
    <Wrapper>
      {
        !share
          ?
            <CommpliteWrapper>
              <h2 className="title">Thank you for your order #{order} </h2>
              <span className="message">
                Please register to track your order #<Link to={track.link}>{track.name}</Link>
              </span>

              {loading ? null : null} {/* TODO: SET LOADING  */}
              <div className="share-purchase">
                <div className="share-purchase__header">
                  Share your purchase & Earn Midcoins
                </div>

                <div className="share-purchase__content">
                  <AddPurchases
                    purchases={purchases}
                    addPurchases={addPurchases}
                  />
                </div>

                <div className="share-purchase__footer">
                  <button type="button" className="cancel" onClick={() => push('/')}>Cancel</button>
                  <button
                    type="button"
                    className="next"
                    disabled={purchases.length < 1}
                    onClick={() => setShare(true)}
                  >
                    Next
                    <Icon type="arrow" color="#fff" width={6} height={9}/>
                  </button>
                </div>
              </div>
            </CommpliteWrapper>
          : null
      }

      {
        share
          ?
          <CommpliteWrapper>
            <h2 className="title">Thank you for your order #{order} </h2>
            <span className="message">
              Please register to track your order #<Link to={track.link}>{track.name}</Link>
            </span>
            <ShareBlock setShare={setShare}/>
          </CommpliteWrapper>
          : null
      }
    </Wrapper>
  );
};

export default Complite;
