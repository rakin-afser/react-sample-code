import React from 'react';

import {Wrapper} from './styled';
import SignInForms from './components/SignInForms';

const SignIn = (props) => {
  return (
    <Wrapper>
      <SignInForms {...props} />
    </Wrapper>
  );
};

export default SignIn;
