import React, {useState} from 'react';
import {useQuery, useMutation} from '@apollo/client';

import {Wrapper, Title, CheckoutForm} from './styled';
import Location from './components/Location';
import EditLocation from './components/EditLocation';
import OrderReview from 'containers/GuestCheckoutPage/components/OrderReview';
import ShippingAddress from './components/ShippingAddress';
import Payment from './components/Payment';
import CartSummary from './components/CartSummary';
import {CART, ADDRESSES} from 'queries';
import {CALCULATE_SUBCARTS} from 'mutations';
import {ShippingAddressFragment} from 'fragments';
import {useHistory} from 'react-router';

const Checkout = ({step = '', setStep = () => {}, email}) => {
  const {push} = useHistory();
  const [selectedAddress, setSelectedAddress] = useState();
  const {data, error, loading} = useQuery(ADDRESSES, {
    onCompleted(dataAddress) {
      const selectedAddr =
        dataAddress?.testSampleAddresses?.shipping?.find((addr) => addr?.selected) ||
        JSON.parse(localStorage.getItem('gAddress'));
      setSelectedAddress(selectedAddr);

      const {id, address1, block, build, city, country, district, firstName, phone, road, type} = selectedAddr || {};

      calculate({
        variables: {
          input: {
            addrId: id,
            addrAddress1: address1,
            addrBlock: block,
            addrBuild: build,
            addrCity: city,
            addrCountry: country,
            addrDistrict: district,
            addrFirstName: firstName,
            addrPhone: phone,
            addrRoad: road,
            addrType: type
          }
        },
        optimisticResponse: {
          calculateSubcarts: {
            __typename: 'CalculateSubcartsPayload',
            shippingAddresses: dataAddress?.testSampleAddresses?.shipping?.map((addr) =>
              addr?.id === id ? {...addr, selected: true} : {...addr, selected: false}
            ),
            cart: cartData?.cart
          }
        },
        update(cache, {data: dataCalculate}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = dataCalculate?.calculateSubcarts?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                return {...existing, shipping};
              },
              cart(existing) {
                return {
                  ...existing,
                  total: dataCalculate?.calculateSubcarts?.cart?.total,
                  subcarts: dataCalculate?.calculateSubcarts?.cart?.subcarts
                };
              }
            }
          });
        }
      });
    }
  });
  const {data: cartData} = useQuery(CART, {
    // ToDO: Add redirect if checkout is empty
    // onCompleted(dataCart) {
    //   const {contents} = dataCart?.cart || {};
    //   if (!contents?.nodes?.length) {
    //     push('/');
    //   }
    // }
  });
  const [calculate, {loading: loadingCalculate}] = useMutation(CALCULATE_SUBCARTS);

  const [editLocation, setEditLocation] = useState(null);
  const [cart, setCart] = useState({
    items: {},
    shipping: {},
    currency: '',
    shippingCharges: false,
    error: false,
    discount: {
      coupon: 0,
      influencer: {}
    },
    paymentMethod: null
  });
  const [influencer, setInfluencer] = useState({use: {}, code: {}});
  const [location, setLocation] = useState({
    country: '',
    mobileNumber: '',
    district: '',
    street: '',
    addressType: '',
    houseBuildingNo: '',
    blockNo: '',
    roadNo: '',
    floorNo: '',
    aptNo: '',
    addressNickname: '',
    location: '',
    additionalDirections: ''
  });

  const checkShipping = () => {
    if (cart.shipping.filter((el) => el === null).length > 0) {
      setCart((prev) => ({...prev, error: true}));
    }
    for (const key in cart.shipping) {
      if (cart.shipping[key] !== 0 && !cart.shipping[key]) {
        return !cart.shipping[key];
      }
    }
    return false;
  };

  const shippingAddressChanged = (address) => {
    setSelectedAddress(address);
    const {id, address1, block, build, city, country, district, firstName, phone, road, type} = address || {};
    calculate({
      variables: {
        input: {
          addrId: id,
          addrAddress1: address1,
          addrBlock: block,
          addrBuild: build,
          addrCity: city,
          addrCountry: country,
          addrDistrict: district,
          addrFirstName: firstName,
          addrPhone: phone,
          addrRoad: road,
          addrType: type
        }
      },
      optimisticResponse: {
        calculateSubcarts: {
          __typename: 'CalculateSubcartsPayload',
          shippingAddresses: data?.testSampleAddresses?.shipping?.map((addr) =>
            addr?.id === id ? {...addr, selected: true} : {...addr, selected: false}
          ),
          cart: cartData?.cart
        }
      },
      update(cache, {data: dataCalculate}) {
        cache.modify({
          fields: {
            testSampleAddresses(existing) {
              const shipping = dataCalculate?.calculateSubcarts?.shippingAddresses?.map((addr) =>
                cache.writeFragment({
                  id: cache.identify(addr),
                  data: addr,
                  fragment: ShippingAddressFragment
                })
              );
              return {...existing, shipping};
            },
            cart(existing) {
              return {
                ...existing,
                total: dataCalculate?.calculateSubcarts?.cart?.total,
                subcarts: dataCalculate?.calculateSubcarts?.cart?.subcarts
              };
            }
          }
        });
      }
    });
  };

  const renderSecureCheckout = () => {
    switch (step) {
      case 'details':
        return (
          <ShippingAddress
            setStep={setStep}
            setEditLocation={setEditLocation}
            data={data?.testSampleAddresses?.shipping}
            selectedAddress={selectedAddress}
            setSelectedAddress={setSelectedAddress}
            onChange={shippingAddressChanged}
            cartData={cartData}
            loadingCalculate={loading || loadingCalculate}
            shippingAddressChanged={shippingAddressChanged}
          />
        );

      case 'address':
        return (
          <Location
            setStep={setStep}
            location={location}
            setLocation={setLocation}
            savedData={selectedAddress}
            shippingAddressChanged={shippingAddressChanged}
          />
        );

      case 'review':
        return (
          <OrderReview
            setStep={setStep}
            cart={cart}
            influencer={influencer}
            setInfluencer={setInfluencer}
            cartData={cartData}
            selectedAddress={selectedAddress}
          />
        );

      case 'payment':
        return (
          <Payment
            cartData={cartData?.cart}
            setStep={setStep}
            setCart={setCart}
            cart={cart}
            selectedAddress={selectedAddress}
            email={email}
          />
        );
      default:
        return null;
    }
  };

  const [selectedEditAddress] = data?.testSampleAddresses?.shipping.filter((addr) => addr.id === editLocation) || [];

  return (
    <Wrapper>
      <Title>Secure Checkout</Title>
      <CheckoutForm>
        <div className="grid">
          {renderSecureCheckout()}
          <CartSummary
            step={step}
            cart={cart}
            cartData={cartData?.cart}
            setCart={setCart}
            checkShipping={checkShipping}
          />
        </div>
      </CheckoutForm>
      <EditLocation
        shippingAddressChanged={shippingAddressChanged}
        selectedEditAddress={selectedEditAddress}
        visible={editLocation}
        close={() => setEditLocation(null)}
        location={location}
        setLocation={setLocation}
      />
    </Wrapper>
  );
};

export default Checkout;
