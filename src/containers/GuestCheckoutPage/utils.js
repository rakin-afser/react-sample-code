import exampleData from 'containers/GuestCheckoutPage/assets/data/exampleData';

const {country} = exampleData;

export const getCountryFromCode = (code) => Object.values(country)?.find((item) => item.code === code)?.name;

export const getCircleStatus = (circleNumber, orderStatus) => {
  switch (orderStatus) {
    case 'CANCELLED':
      return null;
    case 'COMPLETED':
      return 'completed';
    case 'DriverAssigned':
      return circleNumber === 3 ? 'inProgressBig' : 'completed';
    case 'FAILED':
      return null;
    case 'ON_HOLD':
      return null;
    case 'OrderReturned':
      return null;
    case 'OutforDelivery':
      return circleNumber === 3 ? 'inProgressBig' : 'completed';
    case 'PENDING':
      return circleNumber === 3 ? null : 'inProgress';
    case 'PROCESSING':
      return circleNumber === 3 ? null : 'inProgress';
    case 'REFUNDED':
      return null;
    case 'ReadytoShip':
      return circleNumber === 3 ? null : 'completed';
    default:
      return null;
  }
};

export const getLineStatus = (lineNumber, orderStatus) => {
  switch (orderStatus) {
    case 'CANCELLED':
      return null;
    case 'COMPLETED':
      return 'completed';
    case 'DriverAssigned':
      return 'completed';
    case 'FAILED':
      return null;
    case 'ON_HOLD':
      return null;
    case 'OrderReturned':
      return null;
    case 'OutforDelivery':
      return 'completed';
    case 'PENDING':
      return lineNumber === 1 ? 'halfCompleted' : null;
    case 'PROCESSING':
      return lineNumber === 1 ? 'completed' : null;
    case 'REFUNDED':
      return null;
    case 'ReadytoShip':
      return lineNumber === 1 ? 'completed' : 'halfCompleted';
    default:
      return null;
  }
};

export const isStatusInProgress = (textNumber, orderStatus) => {
  switch (orderStatus) {
    case 'CANCELLED':
      return null;
    case 'COMPLETED':
      return null;
    case 'DriverAssigned':
      return textNumber === 3 ? 'inProgress' : null;
    case 'FAILED':
      return null;
    case 'ON_HOLD':
      return null;
    case 'OrderReturned':
      return null;
    case 'OutforDelivery':
      return textNumber === 3 ? 'inProgress' : null;
    case 'PENDING':
      return textNumber === 2 ? 'inProgress' : null;
    case 'PROCESSING':
      return textNumber === 2 ? 'inProgress' : null;
    case 'REFUNDED':
      return null;
    case 'ReadytoShip':
      return textNumber === 2 ? 'inProgress' : null;
    default:
      return null;
  }
};

export const isOrderDelivered = (orders) => {
  return !orders?.nodes?.find((item) => item?.status !== 'COMPLETED');
};
