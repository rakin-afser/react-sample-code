import React, {useState, useEffect} from 'react';
import {useWindowSize} from '@reach/window-size';
import {useParams, useHistory} from 'react-router-dom';

import Layout from '../Layout';
import SignIn from './SignIn';
import SecureCheckout from './SecureCheckout';
import HeaderCheckout from '../../components/Header/HeaderCheckout';
import VerifyEmail from './components/VerifyEmail';
import PaymentHandler from 'components/PaymentHandler';
import Success from 'containers/GuestCheckoutPage/components/Success';
// import CheckoutMobile from './CheckoutMobile';
import CheckoutMobileNew from './CheckoutMobileNew';
import {useUser} from 'hooks/reactiveVars';
import {gEmail} from 'util/heplers';
import Failed from './components/Failed';

const Checkout = () => {
  const {step} = useParams();
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const history = useHistory();
  const [email, setEmail] = useState(gEmail);
  const [orderNumber, setOrderNumber] = useState('');
  const [user] = useUser();

  useEffect(() => {
    // if (!email && !user && step !== 'start') {
    //   setStep('start');
    // }
    if (user && ['verify', 'start'].includes(step)) {
      setStep('details');
    }
  }, [user]);

  const setStep = (step, state) => {
    history.push(`/checkout/${step}`, state);
  };

  function renderCheckout() {
    switch (step) {
      case 'start':
        return <SignIn setEmail={setEmail} setStep={setStep} />;
      case 'verify':
        return <VerifyEmail email={email} setEmail={setEmail} setStep={setStep} />;
      case 'details':
      case 'address':
      case 'review':
      case 'payment':
        return (
          <SecureCheckout
            email={email}
            setStep={setStep}
            step={step}
            orderCreated={(orderNumber) => setOrderNumber(orderNumber)}
          />
        );
      case 'payment-handler':
        return <PaymentHandler />;
      case 'failed':
        return <Failed />;
      case 'success':
        return <Success orderNumber={orderNumber} email={email} />;
      default:
        return null;
    }
  }

  return (
    <Layout hideHeader={step !== 'success' || isMobile} hideFooter>
      {isMobile ? (
        <CheckoutMobileNew />
      ) : (
        <>
          {step !== 'success' && <HeaderCheckout />}
          {renderCheckout()}
        </>
      )}
    </Layout>
  );
};

export default Checkout;
