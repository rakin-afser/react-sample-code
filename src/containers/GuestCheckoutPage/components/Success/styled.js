import styled from 'styled-components/macro';
import {mainBlackColor} from 'constants/colors';
import {secondaryFont} from 'constants/fonts';
import {Wrapper as WrapperAtom} from 'containers/GuestCheckoutPage/styled';

export const Wrapper = styled(WrapperAtom)`
  min-height: calc(100vh - 60px);
`;

export const Title = styled.div`
  font-size: 26px;
  line-height: 1.2;
  font-weight: 700;
  margin-bottom: 24px;
  color: #000;
  font-family: ${secondaryFont};
`;

export const SubTitle = styled.div`
  font-size: 28px;
  padding-bottom: 17px;
  border-bottom: 2px solid #ed484f;
  margin-bottom: 18px;
  font-weight: 700;
  font-family: ${secondaryFont};
  color: ${mainBlackColor};
`;

export const FormWrapper = styled.div`
  width: 100%;
`;

export const SuccessWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding: 70px 16px;
`;

export const DescriptionWrapper = styled.div`
  font-size: 16px;
`;

export const InfoWrapper = styled.div`
  font-size: 16px;
  margin: 20px;
  width: 100%;
  border: 2px solid #999;
  border-radius: 5px;
  padding: 16px;
`;

export const InfoText = styled.div`
  color: #999;
  & span {
    color: #555;
  }
`;

export const DetailsBlock = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  padding: 11px 32px 18px;
  margin-top: 44px;
  width: 600px;
`;

export const DetailsTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  color: #000000;
`;

export const DetailsText = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 12px;
  color: #8f8f8f;
  margin: 16px 26px 0 0;

  & span {
    font-weight: normal;
    font-size: 14px;
    color: #000000;
    line-height: 140%;
    margin-left: 12px;
  }
`;

export const InfoBlock = styled.div`
  /* max-width: 538px; */
  display: flex;
  flex-wrap: wrap;
`;

export const GuestTrackingMessage = styled.p`
  font-size: 16px;
  text-align: center;
  margin: 20px 0;
  font-weight: 500;
`;
