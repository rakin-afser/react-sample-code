import React, {useEffect} from 'react';
import {useApolloClient, useLazyQuery} from '@apollo/client';
import qs from 'qs';
import moment from 'moment';
import {useLocation} from 'react-router';

import {
  Wrapper,
  SuccessWrapper,
  Title,
  SubTitle,
  FormWrapper,
  DetailsBlock,
  DetailsTitle,
  DetailsText,
  InfoBlock,
  GuestTrackingMessage
} from './styled';
import Loader from 'components/Loader';
import Error from 'components/Error';
import {GET_TAP_ORDER, testSample_COD_PAYMENT} from 'queries';
import OrderSignUpForm from 'components/OrderSignUpForm';
import {useUser} from 'hooks/reactiveVars';
import TrackingStatus from 'containers/GuestCheckoutPage/components/TrackingStatus';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import EarnedCoins from 'containers/GuestCheckoutPage/components/EarnedCoins';
import {usetestSampleCheckoutdotcomPaymentLazyQuery} from 'hooks/queries';

const Success = ({email}) => {
  const {search} = useLocation();
  const [user] = useUser();
  const {tap_id: tapId, ckoId, cod} = qs.parse(search.replace('?', '')) || {};
  const {cache} = useApolloClient();

  const [
    getCheckoutOrder,
    {data: dataCk, loading: loadingCk, error: errorCk}
  ] = usetestSampleCheckoutdotcomPaymentLazyQuery({
    variables: {ckoId},
    onCompleted(data) {
      const {status} = data?.testSampleCheckoutDotComPayment || {};
      if (status !== 'DECLINED') {
        cache.modify({
          fields: {
            cart() {
              return {};
            }
          }
        });
      }
    }
  });

  const [getTapOrder, {data: dataTap, loading: loadingTap, error: errorTap}] = useLazyQuery(GET_TAP_ORDER, {
    variables: {tapId},
    onCompleted(data) {
      const {status} = data?.testSampleTapPayment || {};
      if (status !== 'DECLINED') {
        cache.modify({
          fields: {
            cart() {
              return {};
            }
          }
        });
      }
    }
  });

  const [getCodOrder, {data: dataCod, loading: loadingCod, error: errorCod}] = useLazyQuery(testSample_COD_PAYMENT, {
    variables: {orderId: cod}
  });

  useEffect(() => {
    if (ckoId) {
      getCheckoutOrder();
    }
  }, [ckoId, getCheckoutOrder]);

  useEffect(() => {
    if (tapId) {
      getTapOrder();
    }
  }, [tapId, getTapOrder]);

  useEffect(() => {
    if (cod) {
      getCodOrder();
    }
  }, [cod, getCodOrder]);

  const {message} = errorTap || errorCod || errorCk || {};
  const {orderId, date, midcoinsAmount, shopcoinsAmount, amount, currency, error: orderError, status, type, orders} =
    dataTap?.testSampleTapPayment || dataCod?.testSampleCodPayment || dataCk?.testSampleCheckoutDotComPayment || {};
  const shipping = orders?.nodes?.[0]?.shipping;

  const isDeclined = status === 'DECLINED';

  if (message) return <Error>{message}</Error>;
  if (loadingTap || loadingCod || loadingCk) return <Loader />;

  return (
    <Wrapper>
      <SuccessWrapper>
        <Title>Thank you for your order #{orderId}</Title>
        <EarnedCoins midcoinsAmount={midcoinsAmount} shopcoinsAmount={shopcoinsAmount} />
        <DetailsBlock>
          <DetailsTitle>Order Tracking</DetailsTitle>
          {user?.email ? (
            <TrackingStatus orders={orders} />
          ) : (
            <GuestTrackingMessage>
              Please create an account and sign in or check the status of your order via email.
            </GuestTrackingMessage>
          )}
        </DetailsBlock>
        <DetailsBlock>
          <DetailsTitle>Invoice Details</DetailsTitle>
          <InfoBlock>
            <DetailsText>
              Order ID<span>{orderId}</span>
            </DetailsText>
            <DetailsText>
              Order Date<span>{moment(date).format('DD MMMM, YYYY')}</span>
            </DetailsText>
            {isDeclined ? (
              <DetailsText>
                Status
                <span>Cancelled</span>
              </DetailsText>
            ) : (
              <DetailsText>
                Amount Paid
                <span>{amount}</span>
              </DetailsText>
            )}

            <DetailsText>
              Shipping Address
              <span>
                {[
                  shipping?.postCode,
                  shipping?.build,
                  shipping?.address1,
                  shipping?.city,
                  shipping?.district,
                  getCountryFromCode(shipping?.country)
                ]
                  .filter((item) => !!item)
                  .join(', ')}
              </span>
            </DetailsText>
          </InfoBlock>
        </DetailsBlock>
      </SuccessWrapper>

      {user?.email ? null : (
        <FormWrapper>
          <SubTitle>Create account</SubTitle>
          <OrderSignUpForm email={email} />
        </FormWrapper>
      )}
    </Wrapper>
  );
};

export default Success;
