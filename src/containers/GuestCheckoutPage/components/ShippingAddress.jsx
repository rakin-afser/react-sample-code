import React, {useEffect} from 'react';
import {func} from 'prop-types';
import {gql, useApolloClient, useMutation} from '@apollo/client';
import {isEmpty} from 'lodash';

import Icon from 'components/Icon';
import {ShippingAddressWrapper, WarningMessage} from '../styled';
import {REMOVE_SHIPPING_ADDRESS} from 'mutations';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import {useHistory} from 'react-router';
import {useUser} from 'hooks/reactiveVars';
import {testSampleSubcartsFragment, ShippingAddressFragment} from 'fragments';
import {isDeliveryRegion} from 'util/heplers';
import Btn from 'components/Btn';

const ShippingAddress = ({
  setEditLocation,
  setStep = () => {},
  data,
  onChange = () => {},
  selectedAddress,
  setSelectedAddress,
  cartData,
  loadingCalculate,
  shippingAddressChanged = () => {}
}) => {
  const {push} = useHistory();
  const [removeAddress] = useMutation(REMOVE_SHIPPING_ADDRESS);
  const [user] = useUser();
  const {email} = user || {};

  useEffect(() => {
    // Launch shipping calculation
    //  automatically if user is not logged in

    if (!email && !isEmpty(selectedAddress)) {
      onChange(selectedAddress);
    }
  }, [email, selectedAddress]);

  const onDeleteAddress = (id) => {
    if (user) {
      removeAddress({
        variables: {input: {addrId: id}},
        update(cache, {data}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = data?.removeShippingAddress?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                if (data?.removeShippingAddress?.shippingAddresses?.length === 1) {
                  const [address] = data?.removeShippingAddress?.shippingAddresses || [];
                  shippingAddressChanged(address);
                }
                return {...existing, shipping};
              }
            }
          });
        }
      });
    }

    if (!user) {
      localStorage.removeItem('gAddress');
      setSelectedAddress(null);
    }
  };

  const renderAddress = (addresses) => {
    if (!user && !isEmpty(selectedAddress)) {
      const {firstName, build, address1, city, district, country} = selectedAddress || {};
      return (
        <div className="shipping__address">
          <div className="shipping__address-info">
            <span className="shipping__address-name">{firstName}</span>
            <span className="shipping__address-line">
              {[build, address1, city, district, getCountryFromCode(country)].filter((item) => !!item).join(', ')}
            </span>
            {!isEmpty(cartData) && !loadingCalculate && !isDeliveryRegion(cartData?.cart?.subcarts?.subcarts) ? (
              <WarningMessage>Sorry, delivery to this region is not possible yet</WarningMessage>
            ) : null}
          </div>
          <div className="shipping__address-tools">
            <span className="shipping__address-edit" onClick={() => setEditLocation('1')}>
              <Icon type="edit" />
            </span>

            <span className="shipping__address-delete" onClick={() => onDeleteAddress()}>
              <Icon type="close" color="#545454" />
            </span>
          </div>
        </div>
      );
    }

    return addresses?.map((item) => {
      return (
        <div key={item.id} className="shipping__address">
          <label htmlFor={`address_${item.id}`} className="shipping__address-checkbox">
            <input
              type="radio"
              id={`address_${item.id}`}
              name="address"
              onChange={() => onChange(item)}
              checked={item?.selected}
            />

            <span className="shipping__address-icon" />

            <div className="shipping__address-info">
              <span className="shipping__address-name">
                {item.firstName} {item.lastName}
              </span>
              <span className="shipping__address-line">
                {[item.postCode, item.build, item.address1, item.city, item.district, getCountryFromCode(item.country)]
                  .filter((item) => !!item)
                  .join(', ')}
              </span>
              {!isEmpty(cartData) &&
              !loadingCalculate &&
              item?.selected &&
              !isDeliveryRegion(cartData?.cart?.subcarts?.subcarts) ? (
                <WarningMessage>Sorry, delivery to this region is not possible yet</WarningMessage>
              ) : null}
            </div>

            <div className="shipping__address-type">{item?.addressNickname}</div>
          </label>

          <div className="shipping__address-tools">
            <span className="shipping__address-edit" onClick={() => setEditLocation(item.id)}>
              <Icon type="edit" />
            </span>

            <span className="shipping__address-delete" onClick={() => onDeleteAddress(item.id)}>
              <Icon type="close" color="#545454" />
            </span>
          </div>
        </div>
      );
    });
  };

  const disabled =
    loadingCalculate ||
    !isDeliveryRegion(cartData?.cart?.subcarts?.subcarts) ||
    !selectedAddress ||
    (!data?.length && email);

  return (
    <ShippingAddressWrapper>
      <div className="shipping">
        <div className="shipping__content">
          <h3>1. Shipping Address</h3>

          <div className="shipping__addresses">{renderAddress(data)}</div>

          <div className="shipping__footer">
            <span className="shipping__footer-add" onClick={() => push('/checkout/address')}>
              <Icon type="plus" />
              Add Shipping Address
            </span>
            <Btn
              style={{marginLeft: 'auto'}}
              kind="continue"
              loading={loadingCalculate}
              disabled={disabled}
              onClick={() => setStep('review')}
            />

            {/* <button
              type="button"
              className={`shipping__footer-save${disabled ? ' disabled' : ''}`}
              disabled={disabled}
              onClick={() => setStep('review')}
            >
              Save & Continue
              <Icon type="arrow" color={selectedAddress ? '#000' : '#888'} />
            </button> */}
          </div>
        </div>

        <div className="shipping__content inactive">
          <h3>2. Review Order</h3>
        </div>

        <div className="shipping__content inactive">
          <h3>3. Payment</h3>
        </div>
      </div>

      {/* {renderPopup()} */}
    </ShippingAddressWrapper>
  );
};

ShippingAddress.propTypes = {
  setStep: func.isRequired
};

export default ShippingAddress;
