import styled from 'styled-components';
import {Modal} from 'antd';
import {primaryColor} from 'constants/colors';

export const StyledModal = styled(Modal)`
  & .ant-modal-content {
    border-radius: 8px;
  }
`;

export const Wrapper = styled.div`
  background-color: #fff;
`;

export const Title = styled.h3`
  margin-bottom: 32px;
  font-size: 24px;
  line-height: 29px;
  display: flex;
  align-items: center;
  color: #000000;
`;

export const Text = styled.div`
  p {
    margin-bottom: 21px;
    font-size: 14px;
    line-height: 150%;
    color: #000;

    &:last-child {
      margin-bottom: 33px;
    }
  }
`;

export const Images = styled.div`
  display: flex;
  align-items: center;

  img {
    margin-right: 10px;
    width: 64px;
    height: auto;
    object-fit: contain;
  }
`;

export const CloseContainer = styled.div`
  svg {
    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;
