import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icon';
import img1 from './images/img1.png';
import img2 from './images/img2.png';
import img3 from './images/img3.png';
import img4 from './images/img4.png';
import img5 from './images/img5.png';
import {StyledModal, Wrapper, Title, Text, Images, CloseContainer} from './styled';

const CardSecurityInformation = ({isOpen, closeModal}) => {
  return (
    <StyledModal
      visible={isOpen}
      onCancel={closeModal}
      style={{top: 200, transition: 'ease .4s'}}
      width={630}
      footer={null}
      destroyOnClose
      bodyStyle={{
        padding: '39px 35px 61px 40px',
        minHeight: '124px'
      }}
      closeIcon={
        <CloseContainer
          style={{
            top: '15px',
            position: 'relative',
            right: '4px'
          }}
        >
          <Icon color="#000" type="close" width={33} height={33} />
        </CloseContainer>
      }
    >
      <Wrapper>
        <Title>Card security information</Title>
        <Text>
          <p>testSample does not store your credit card information.</p>

          <p>
            Any payment data that you enter is securely passed on to a payment processor via secure socket layer (SSL)
            that encrypts all sensitive card information.
          </p>

          <p>
            The payment processor then tokenizes your card information (substituting your sensitive information with a
            non-sensitive equivalent).
          </p>

          <p>
            The payment processor manages and stores your card information in a very secure environment with a very high
            security standard and are PCI DSS compliant.
          </p>
        </Text>
        <Images>
          <img src={img1} alt="Thawte" />
          <img src={img2} alt="Master Card" />
          <img src={img3} alt="Visa" />
          <img src={img4} alt="Safe key" />
          <img src={img5} alt="PCI DSS" />
        </Images>
      </Wrapper>
    </StyledModal>
  );
};

CardSecurityInformation.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired
};

export default CardSecurityInformation;
