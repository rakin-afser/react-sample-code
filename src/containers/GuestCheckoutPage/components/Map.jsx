import React, {useRef} from 'react';
import {GoogleMap, LoadScript, Marker, StandaloneSearchBox} from '@react-google-maps/api';

const libraries = ['places'];

const Map = (props) => {
  const {center, position, setPosition, setCenter} = props || {};
  const ref = useRef();
  function onPlacesChanged() {
    const {location} = ref?.current?.getPlaces()?.[0]?.geometry || {};
    const {address_components: addressComponents} = ref?.current?.getPlaces() || {};

    setCenter({lat: location?.lat(), lng: location?.lng()});
    setPosition({lat: location?.lat(), lng: location?.lng()});
  }

  return (
    <LoadScript language="en" libraries={libraries} googleMapsApiKey="AIzaSyCHdkvgxa2gSIsZxu8Rjnulh_iiZh2QC5s">
      <StandaloneSearchBox
        onLoad={(r) => {
          ref.current = r;
        }}
        onPlacesChanged={onPlacesChanged}
      >
        <input type="text" placeholder="Enter your address" />
      </StandaloneSearchBox>
      <GoogleMap
        onClick={(e) => setPosition({lat: e.latLng.lat(), lng: e.latLng.lng()})}
        zoom={10}
        center={center}
        mapContainerStyle={{
          height: 220
        }}
        options={{
          fullscreenControl: false,
          zoomControl: false,
          rotateControl: false,
          streetViewControl: false,
          mapTypeControl: false
        }}
      >
        {position ? <Marker onClick={() => setPosition(null)} position={position} /> : null}
      </GoogleMap>
    </LoadScript>
  );
};

export default Map;
