import React, {useState} from 'react';
import {Input} from 'antd';
import PropTypes from 'prop-types';

import FormTamplate from './FormTemplate';
import Checkbox from './Checkbox';
import {ReactComponent as DoubleCheckIcon} from '../assets/icon/doubleCheck.svg';
import {ReactComponent as LoadingDotsIcon} from '../assets/icon/loadingDots.svg';
import {toNumber} from 'util/heplers';

const discountValue = 10;

const Influencer = ({influencer, setInfluencer, idx, price, setInlucode, setInfDiscount}) => {
  const [influValue, setInfluValue] = useState('');
  const [checkedCode, setCheckedCode] = useState('');

  const addInfluencerCode = (values) => {
    const index = Object.keys(values)[0];
    setCheckedCode('load');
    setTimeout(() => {
      setCheckedCode('checked');
      setInfluencer((prev) => ({
        ...prev,
        code: {...prev.code, [idx]: {value: values[index], discount: discountValue}}
      }));
      const newDiscount = (toNumber(price) * (discountValue / 100)).toFixed(3);
      setInfDiscount(newDiscount);
      setInlucode(true);
    }, 1000);
  };

  const checkInfluencer = ({target: {checked}}) => {
    setInfluencer((prev) => ({...prev, use: {...prev.use, [idx]: checked}}));
  };

  const influencerInputIcons = () => {
    if (checkedCode === 'load') {
      return <LoadingDotsIcon className="suffix-icon load-dots" />;
    }
    if (checkedCode === 'checked') {
      return <DoubleCheckIcon className="suffix-icon" />;
    }
    return '';
  };

  return (
    <div className="influencer-form">
      <Checkbox disabled onChange={checkInfluencer} defaultChecked={influencer.use[idx]}>
        Use an Influencer Code
      </Checkbox>
      {influencer.use[idx] && (
        <>
          <FormTamplate
            fields={[
              {
                name: idx,
                renderField: (
                  <Input
                    placeholder="Influencer Code"
                    allowClear={!checkedCode}
                    onChange={({target: {value}}) => setInfluValue(value)}
                    suffix={influencerInputIcons()}
                    disabled={checkedCode === 'checked'}
                  />
                ),
                rules: [{required: true, message: 'Enter a valid Code'}]
              }
            ]}
            callback={addInfluencerCode}
            submitBtn={checkedCode !== 'checked' && influValue && influValue.length > 0 ? 'Apply' : ''}
            btnProps={{
              ghost: true,
              styles: 'width: 72px;height:24px;font-size: 12px;line-height: 140%;'
            }}
          />
          {influencer.code[idx] && <span className="discount-message">Discount -{influencer.code[idx].discount}%</span>}
        </>
      )}
    </div>
  );
};

// Influencer.propTypes = {
//   idx: PropTypes.string.isRequired,
//   influencer: PropTypes.objectOf(PropTypes.any).isRequired,
//   setInfluencer: PropTypes.func.isRequired,
//   setCart: PropTypes.func.isRequired,
//   price: PropTypes.string.isRequired,
//   setInlucode: PropTypes.func.isRequired
// };

export default Influencer;
