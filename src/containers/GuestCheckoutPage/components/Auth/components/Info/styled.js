import styled from 'styled-components/macro';
import {secondaryTextColor} from '../../../../../../constants/colors';
import media from '../../../../../../constants/media';

export const Text = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: ${secondaryTextColor};
  margin-top: 15px;
  margin-bottom: 0;
  padding-right: 70px;
  line-height: 19.5px;

  @media (min-width: ${media.mobileMax}) {
    margin-top: 22px;
    margin-bottom: -14px;
    color: #000;
  }
`;
