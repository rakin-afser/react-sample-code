import React from 'react';
import {Text} from './styled';

const Info = ({text}) => {
  return <Text>{text}</Text>;
};

export default Info;
