import styled from 'styled-components/macro';

export const Text = styled.h3`
  font-style: normal;
  font-weight: 500;
  font-size: 22px;
  line-height: 27px;
  color: #000;
  text-align: center;
  margin: 0;
  margin-top: 35px;
  margin-bottom: 23px;
`;
