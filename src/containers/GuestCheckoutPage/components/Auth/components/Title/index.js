import React from 'react';
import {Text} from './styled';

const Title = ({title}) => {
  return <Text>{title}</Text>;
};

export default Title;
