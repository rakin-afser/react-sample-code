import React from 'react';
import {Text} from './styled';

const SubTitle = ({title}) => {
  return <Text>{title}</Text>;
};

export default SubTitle;
