import styled from 'styled-components/macro';
import media from '../../../../../../constants/media';

export const Text = styled.h4`
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #000;
  text-align: left;
  margin: 0;
  width: 100%;
  margin-top: 22px;

  @media (min-width: ${media.mobileMax}) {
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: 600;
    font-size: 20px;
    line-height: 124%;
    margin-top: 7px;
  }
`;
