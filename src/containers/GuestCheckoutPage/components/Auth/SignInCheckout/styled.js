import styled from 'styled-components/macro';
import media from '../../../../../constants/media';
import {Tabs} from 'antd';
const {TabPane} = Tabs;

export const TabsNav = styled(Tabs)`
  &&& .ant-tabs-nav-scroll {
    // width: 100%;
  }

  &&& .ant-tabs-nav {
    // display: flex;
    // justify-content: space-between;
  }

  &&& .ant-tabs-nav-wrap {
    display: flex;
    justify-content: center;
  }

  &&& .ant-tabs-tab {
    font-size: 14px;
    padding: 9px 42px 9px 32px;
    margin: 0;

    @media (min-width: ${media.mobileMax}) {
      font-size: 16px;
      line-height: 140%;
      padding: 10px 20px;
    }
  }

  &&& .ant-tabs-tab-active {
    color: #000;
  }

  &&& .ant-tabs-tab:hover {
    color: #000;
  }

  &&& .ant-tabs-ink-bar {
    background-color: #f53d3d;
  }
`;

export const TabItem = styled(TabPane)`
  padding: 0 24px;
`;

export const Overlay = styled.div`
  @media (min-width: ${media.mobileMax}) {
    height: 100vh;
    background-color: #f5f5f5;
    display: flex;
    justify-content: center;
  }
`;

export const Wrapper = styled.div`
  padding-bottom: 64px;
  width: 100%;

  @media (min-width: ${media.mobileMax}) {
    margin-top: 28px;
    padding-top: 15px;
    padding-bottom: 26px;
    max-width: 375px;
    height: max-content;
    background: #fff;
    border-radius: 8px;
    box-shadow: 0px 2px 20px rgb(0 0 0 / 7%);
  }
`;

export const Content = styled.div`
  &&& {
    display: flex;
    align-items: center;
    flex-direction: column;
    @media (min-width: ${media.mobileMax}) {
      padding: 0 30px;
    }
  }
`;

export const SignForm = styled.form.attrs(() => ({noValidate: true}))`
  margin-top: 33px;

  @media (min-width: ${media.mobileMax}) {
    margin-top: 21px;
  }
`;

export const FullWidthFormField = styled.div`
  width: 100%;
`;
