import React, {useState} from 'react';
import {useWindowSize} from '@reach/window-size';
import {useHistory} from 'react-router-dom';
import {Auth} from 'aws-amplify';
import {Formik} from 'formik';
import {SignForm, Content, Wrapper, Overlay, FullWidthFormField, TabsNav, TabItem} from './styled';
import ForgotBox from '../../../../../components/UserAuth/components/ForgotBox';
import ButtonCustom from '../../../../../components/ButtonCustom';
import SeparatorLineAndText from '../../../../../components/UserAuth/components/SeparatorLineAndText';
import FormMessage from '../../../../../components/FormMessage';
import {getErrorByCode} from '../../../../../util/responseDecoding';
import SocialButton from '../../../../../components/UserAuth/components/SocialButton';
import useGlobal from 'store';
import Input from '../../../../../components/Input';
import Title from '../components/Title';
import SubTitle from '../components/SubTitle';
import Header from '../../mobile/Header';
import Info from '../components/Info';
import {validateEmail} from '../../../../../util/validators';

const SignInCheckout = ({setStep}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [, globalActions] = useGlobal();
  const {push} = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [notificationMessage, setNotificationMessage] = useState();
  const [notificationMessageGuest, setNotificationMessageGuest] = useState();
  const [locked, setLocked] = useState(false);
  const [formValid, setFormValid] = useState(false);
  const [guestFormValid, setGuestFormValid] = useState(false);

  const handleSocialLoginClick = () => {
    const checkout = {
      socialLogin: true
    };
    localStorage.setItem('checkout', JSON.stringify(checkout));
  };

  const signInAsGuest = async (email) => {
    setIsLoading(true);
    try {
      setIsLoading(false);
      setError(false);
      // const user = await Auth.signIn(username, password); API TO SIGN AS GUEST
      // setStep
    } catch (err) {
      setNotificationMessageGuest(getErrorByCode(error.code));
      setIsLoading(false);
      console.log('error signing in', error);
    }
  };

  const signIn = async (username, password) => {
    setLocked(false);
    setIsLoading(true);
    try {
      const user = await Auth.signIn(username, password);
      globalActions.setUser(user);
      setIsLoading(false);
      setError(false);
      setStep(isMobile ? 'shippingAddress' : 'checkout');
    } catch (error) {
      setNotificationMessage(getErrorByCode(error.code));
      setIsLoading(false);
      console.log('error signing in', error);
    }
  };

  const onClickClose = () => {};

  return (
    <Overlay>
      <Wrapper>
        {isMobile && (
          <>
            <Header />
            <Title title="Checkout" />
          </>
        )}
        <TabsNav defaultActiveKey="1" width={width}>
          <TabItem tab="Existing Customers" key="1">
            <Content>
              {notificationMessage && <FormMessage styles={{marginBottom: 27}} message={notificationMessage} />}
              <SubTitle title={isMobile ? 'Sign in to testSample' : 'Sign In'} />
            </Content>
            <Formik
              initialValues={{
                EmailorUsername: '',
                Password: ''
              }}
              validate={(values) => {
                const errors = {};
                if (!values.Password) errors.Password = 'Please enter a valid password';
                if (!values.EmailorUsername) errors.EmailorUsername = 'Please enter a valid username or email address';
                setFormValid(!(!values.Password || !values.EmailorUsername));
                return errors;
              }}
              onSubmit={(values) => {
                signIn(values.EmailorUsername, values.Password).then();
              }}
            >
              {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur}) => (
                <SignForm onSubmit={handleSubmit} noValidate>
                  <Content>
                    <FullWidthFormField>
                      <Input
                        required
                        key="EmailorUsername"
                        type="text"
                        name="EmailorUsername"
                        id="email"
                        label="Email or Username"
                        placeholder="Enter your email or username"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={errors.EmailorUsername && touched.EmailorUsername ? errors.EmailorUsername : ''}
                        value={values.EmailorUsername}
                        reset={values.EmailorUsername.length > 0}
                        resetCallback={() => setFieldValue('EmailorUsername', '')}
                      />
                    </FullWidthFormField>

                    <FullWidthFormField>
                      <Input
                        required
                        key="Password"
                        type="password"
                        name="Password"
                        id="password"
                        label="Password"
                        placeholder="Enter your password"
                        onChange={handleChange}
                        autoComplete="on"
                        onBlur={handleBlur}
                        error={errors.Password && touched.Password && values.Password.length < 1 ? errors.Password : ''}
                        value={values.Password}
                        showPassword={true}
                      />
                    </FullWidthFormField>
                    <ForgotBox backUrl="/checkout/start" />
                    <ButtonCustom
                      type="submit"
                      isLoading={isLoading}
                      title="Sign in"
                      variant="contained"
                      color="mainWhiteColor"
                      iconRight="chevronRight"
                      disabled={!formValid}
                      styles={{margin: isMobile ? '25px auto 0 auto' : '28px auto 0 auto'}}
                    />
                  </Content>
                </SignForm>
              )}
            </Formik>
          </TabItem>
          <TabItem tab="Guest Checkout" key="2">
            <Content>
              {notificationMessageGuest && (
                <FormMessage styles={{marginBottom: 27}} message={notificationMessageGuest} />
              )}
              <SubTitle title="Guest Checkout" />
              <Info text="Please proceed to checkout now and create account later" />
            </Content>
            <Formik
              initialValues={{
                Email: ''
              }}
              validate={(values) => {
                const errors = {};
                const emailValid = validateEmail(values.Email);
                if (!emailValid.isValid) errors.Email = 'Please enter a valid email address';
                setGuestFormValid(emailValid.isValid);
                return errors;
              }}
              onSubmit={(values) => {
                signInAsGuest(values.Email).then();
              }}
            >
              {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur}) => (
                <SignForm onSubmit={handleSubmit} noValidate>
                  <Content>
                    <FullWidthFormField>
                      <Input
                        required
                        key="Email"
                        type="email"
                        name="Email"
                        id="Email"
                        label="Email"
                        placeholder="Enter your email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        error={errors.Email && touched.Email ? errors.Email : ''}
                        value={values.Email}
                        reset={values.Email.length > 0}
                        resetCallback={() => setFieldValue('Email', '')}
                      />
                    </FullWidthFormField>
                    <ButtonCustom
                      type="submit"
                      isLoading={isLoading}
                      title="Continue as Guest"
                      variant="contained"
                      color="mainWhiteColor"
                      iconRight="chevronRight"
                      disabled={!guestFormValid}
                      styles={{
                        margin: isMobile ? '7px auto 0 auto' : '28px auto 0 auto',
                        width: 220,
                        maxWidth: 220,
                        paddingRight: 23
                      }}
                      iconStyles={{
                        marginLeft: 20
                      }}
                    />
                  </Content>
                </SignForm>
              )}
            </Formik>
          </TabItem>
        </TabsNav>
        <SeparatorLineAndText />
        <Content>
          <SocialButton
            socialName="google"
            disabled={locked}
            styles={{
              marginBottom: 16
            }}
            onClick={handleSocialLoginClick}
          />
          <SocialButton socialName="facebook" disabled={locked} onClick={handleSocialLoginClick} />
        </Content>
      </Wrapper>
    </Overlay>
  );
};

export default SignInCheckout;
