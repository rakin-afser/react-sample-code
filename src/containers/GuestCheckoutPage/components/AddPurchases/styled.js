import styled from 'styled-components';

export const SearchBlock = styled.div`
  /* margin: 16px 0 19px;
  padding-top: 18px; */
`;

export const TextBlock = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: #464646;
  margin-bottom: 16px;
  h3 {
    font-weight: 500;
    margin-bottom: 0;
    font-size: 14px;
  }
  span {
    font-weight: normal;
    font-size: 12px;
    line-height: 140%;
  }
`;

export const SearchInputContainer = styled.div`
  position: relative;
  margin: 16px 0;
  input {
    background: #ffffff;
    border: 1px solid #c8c8c8;
    border-radius: 4px;
    width: 100%;
    height: 30px;
    padding: 4px 35px 6px 16px;
    outline: none;
    color: #000;
  }
  img {
    position: absolute;
    top: 50%;
    right: 10px;
    transform: translateY(-50%);
  }
`;

export const ListBlock = styled.div`
  max-height: 332px;
  overflow-y: auto;
  text-align: center;
  width: calc(100% + 4px);
  padding: 16px 0 0;
  margin: 0 -4px 16px;
  /* -ms-overflow-style: none;
  overflow: -moz-scrollbars-none;
  &::-webkit-scrollbar {
    display: none;
  } */
`;

export const ListItem = styled.div`
  position: relative;
  display: inline-block;
  background: #ffffff;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.13);
  border-radius: 2px;
  padding: 8px;
  margin: 0 4px 16px 4px;
  width: calc(33.3333% - 8px);
  height: 148px;
  cursor: pointer;
  p {
    margin: 0;
    text-align: left;
  }
  &:nth-child(3n + 3) {
    margin-right: 0;
  }
  img {
    width: 80px;
    height: 80px;
    margin: 0 auto 8px;
    border-radius: 4px;
  }
  h3 {
    text-align: left;
    color: #000000;
    margin-bottom: 5px;
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 140%;
  }
`;

export const CurrentPrice = styled.span`
  font-weight: 500;
  font-size: 10px;
  line-height: 120%;
  letter-spacing: 0.06em;
  color: #999999;
  margin-right: 2px;
`;

export const CurrentPriceRed = styled.span`
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #ed484f;
  margin-right: 6px;
`;

export const OldPrice = styled.span`
  font-weight: normal;
  font-size: 8px;
  line-height: 10px;
  color: #a7a7a7;
  margin-right: 2px;
`;

export const OldPriceLineThrough = styled.span`
  font-weight: normal;
  font-size: 8px;
  line-height: 10px;
  text-decoration-line: line-through;
  color: #a7a7a7;
`;

export const SelectedCover = styled.div`
  position: relative;
  width: 80px;
  height: 80px;
  margin: 0 auto 8px;
  border-radius: 4px;
`;

export const BlackCover = styled.div`
  ${({isSelected}) =>
    isSelected
      ? {
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          background: 'linear-gradient(0deg, rgba(0, 0, 0, 0.44), rgba(0, 0, 0, 0.44)), url(fenty-sub-3.jpg)',
          borderRadius: '4px',
          i: {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
          }
        }
      : {display: 'none'}}
`;
