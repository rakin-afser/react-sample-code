import React from 'react';
import { object, func } from 'prop-types';

import Icon from 'components/Icon';

import loupe from './images/loupe.png';
import purchaseImg from './images/purchaseImg.png';

import {
  SearchBlock,
  TextBlock,
  ListBlock,
  SearchInputContainer,
  ListItem,
  CurrentPrice,
  CurrentPriceRed,
  OldPrice,
  OldPriceLineThrough,
  SelectedCover,
  BlackCover
} from './styled';

const exampleData = [
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  },
  {
    img: purchaseImg,
    title: '2018 Floral Dress...',
    newProce: '198.000',
    oldPrice: '84'
  }
];


const AddPurchases = ({ purchases, addPurchases }) => {
  return (
    <div>
      <SearchBlock>
        <ListBlock>
          {exampleData.map((el, i) => (
            <ListItem key={i} onClick={() => addPurchases(i)}>
              <SelectedCover>
                <BlackCover isSelected={purchases.indexOf(i) >= 0}>
                  <Icon type="checkbox" color="#fff" />
                </BlackCover>
                <img src={el.img} alt="purchase" />
              </SelectedCover>
              <h3>{el.title}</h3>
              <p>
                <CurrentPrice>BD</CurrentPrice>
                <CurrentPriceRed>198.000</CurrentPriceRed>
                <OldPrice>BD</OldPrice>
                <OldPriceLineThrough>84</OldPriceLineThrough>
              </p>
            </ListItem>
          ))}
        </ListBlock>
      </SearchBlock>
    </div>
  )
};

AddPurchases.propTypes = {
  purchases: object.isRequired,
  addPurchases: func.isRequired
};

export default AddPurchases;
