import React from 'react';
import {arrayOf, any, string, objectOf, func, object, bool} from 'prop-types';
import {Form, Input} from 'antd';

import Icon from 'components/Icon';

import Button from './Button';

const FormTemplate = ({fields, submitBtn, form, callback, btnProps, loading}) => {
  const {getFieldDecorator, validateFields} = form;

  const onSubmitHandler = (e) => {
    e.preventDefault();
    validateFields((err, values) => {
      callback(values, err);
      if (!err) {
        console.log('Received values of form: ', values);
      }
    });
  };

  return (
    <Form onSubmit={onSubmitHandler}>
      {fields.map(({label, rules, name, type, renderField}, key) => (
        <Form.Item label={label || ''} key={key}>
          {getFieldDecorator(name, {
            valuePropName: type || 'value',
            rules: rules || []
          })(renderField || <Input />)}
        </Form.Item>
      ))}
      {submitBtn && (
        <Button htmlType="submit" {...btnProps} loading={loading}>
          {loading ? <Icon className="loading" type="loader"/> : submitBtn}
        </Button>
      )}
    </Form>
  );
};

FormTemplate.defaultProps = {
  submitBtn: '',
  callback: () => {},
  btnProps: {},
  loading: false
};

FormTemplate.propTypes = {
  fields: arrayOf(any).isRequired,
  submitBtn: string,
  form: objectOf(any).isRequired,
  callback: func,
  btnProps: objectOf(any),
  loading: bool
};

const WrappedFormTemplate = Form.create({
  name: 'form-template',
  onFieldsChange({callbackAllFields}, changedFields, allFields) {
    if (callbackAllFields) {
      callbackAllFields(allFields);
    }
  }
})(FormTemplate);

export default WrappedFormTemplate;
