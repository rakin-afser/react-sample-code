import styled from 'styled-components';
import {Select} from 'antd';

const Wrapper = styled(Select)`
  .ant-select-search input {
    padding: 10px 0;
  }
  .ant-select-arrow {
    top: 13px;
    right: 5px;
  }
`;

export default Wrapper;
