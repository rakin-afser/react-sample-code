import React, {forwardRef} from 'react';
import PropTypes from 'prop-types';

import Wrapper from './styled';
import {ReactComponent as DownIcon} from './down.svg';

const Select = forwardRef(({children, suffixIcon, ...props}, ref) => {
  const selectIcon = suffixIcon || <DownIcon />;

  return (
    <Wrapper ref={ref} {...props} suffixIcon={selectIcon}>
      {children}
    </Wrapper>
  );
});

Select.defaultProps = {
  suffixIcon: null,
  children: []
};

Select.propTypes = {
  suffixIcon: PropTypes.element,
  children: PropTypes.arrayOf(PropTypes.any)
};

export default Select;
