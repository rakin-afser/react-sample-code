import styled from 'styled-components';
import {Button} from 'antd';

const Wrapper = styled(Button)`
  &&&& {
    ${({styles}) => styles || null};
  }
  &&& {
    ${({ghost}) =>
      !ghost
        ? `
    background: #ED484F;
    color: #ffffff;`
        : `color: #ED484F;`};
    border-radius: 24px;

    padding: 0 20px;

    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: 500;
    line-height: 132%;
    letter-spacing: -0.024em;

    display: flex;
    align-items: center;
    justify-content: center;

    svg {
      position: absolute;
      right: 16px;
      fill: ${({fill}) => fill || '#ED484F'};

      &.loading {
        position: relative;
        right: 0;
        top: 4px;
      }
    }
  }
`;

export default Wrapper;
