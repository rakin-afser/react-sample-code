import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './styled';
import {ReactComponent as RightIcon} from './right.svg';

const Button = ({children, rightIcon, fill, ...props}) => {
  return (
    <Wrapper {...props} type="danger" fill={fill}>
      {children}
      {rightIcon && <RightIcon />}
    </Wrapper>
  );
};

Button.defaultProps = {
  rightIcon: false
};

Button.propTypes = {
  children: PropTypes.string.isRequired,
  rightIcon: PropTypes.bool
};

export default Button;
