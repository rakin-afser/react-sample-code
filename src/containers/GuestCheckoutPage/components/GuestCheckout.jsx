import React, {useState} from 'react';
import {func} from 'prop-types';
import {Input,message} from 'antd';
import {SendOTP} from 'mutations';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import FormTemplate from './FormTemplate';
import {SignForm, BtnList, BtnItem, Or} from '../styled';
import {useMutation} from '@apollo/client';
import { useUser } from 'hooks/reactiveVars';
import { Login, VALIDATE_EMAIL } from 'mutations';
import { FB_APP_ID, GOOGLE_APP_ID, localStorageKey } from 'constants/config';
import useGlobal from 'store';
import Btn from 'components/Btn';

const fields = [
  {
    label: 'Email',
    name: 'mail',
    type: 'email',
    renderField: <Input allowClear />,
    rules: [
      {required: true, message: 'Please enter a valid username or email address'},
      {type: 'email', message: 'Please enter a valid username or email address'}
    ]
  }
];

const GuestCheckout = ({setStep, setEmail}) => {
  const [sendOTP] = useMutation(SendOTP);
  const [loading, setLoading] = useState(false);
  const [,globalActions] = useGlobal();
  const [validateEmailMutation] = useMutation(VALIDATE_EMAIL);
  const [login] = useMutation(Login, {
    onCompleted(data) {
      console.log(data.login.user.databaseId,'data.login.user.databaseId')
      localStorage.setItem('userId', data.login.user.databaseId);
      localStorage.setItem('token', data.login.authToken);
      setUser(data?.login?.user);
      setStep('details');
      setLoading(false);
    }, 
    onError() {
      setLoading(false);
    }
  });
  const [, setUser] = useUser();

  const signIn = async (values, err) => {
    if (!err) {
      setLoading(true);

      try {
        const {data} = await sendOTP({variables: {input: {email: values?.mail}}});
        setEmail(values?.mail);

        if (data?.sendOTP) {
          setStep('verify', {email: values?.mail});
        }
        setLoading(false);
      } catch (error) {
        console.log(`error`, error);
        setLoading(false);
      }
    }
  };

  const signInWithSocialGuest = async (username, password, social_login, fb) => {
    try {
      validateEmail(username).then(async (exist) => {
        if (exist) {
          const {data, loading, error} = await login({
            variables: {input: {username, password: '', social_login: true}}
          });
          if (!error && !loading) {
            setUser(fb ? Object.assign({}, data.login.user, {avatar: {url: fb.avatar}}) : data.login.user);
            localStorage.setItem('avatar', fb.avatar);
            localStorage.setItem('userId', data.login.user.databaseId);
            localStorage.setItem('token', data.login.authToken);
            localStorage.setItem(localStorageKey.socialLogin, false);
            setStep('details');
          }
        } else {
          if(username){
            setEmail(username);
            localStorage.setItem('gEmail',username);
            setStep('details');
          }
        }
      });

    } catch (error) {
      console.log(error)
    }
  };
  

  const validateEmail = (email) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await validateEmailMutation({
          variables: {
            input: {
              email: email
            }
          }
        });
        const {
          data: {
            validateEmail: {code}
          }
        } = response;
        if (code == 200) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (e) {
        reject(e);
      }
    });
  };

  return (
    <SignForm style={{marginLeft: 64}}>
      <h2>Guest Checkout</h2>
      <h3>Please proceed to checkout now and create accout later</h3>
      <FormTemplate fields={fields} submitBtn="Continue as Guest" callback={signIn} loading={loading}/>
      <Or mr />
      <BtnList mr>
        <GoogleLogin
          clientId={GOOGLE_APP_ID}
          key="guestGoogle"
          uxMode='popup'
          render={(renderProps) => (
            <BtnItem  onClick={renderProps.onClick} id="googleGuest">
              <Btn kind="googleAuth" />
            </BtnItem>
          )}
          onSuccess={(response) => {

                  const {email, familyName, givenName, imageUrl} = response.profileObj;
                  const formatedResponse = {
                    email: email,
                    firstName: givenName,
                    lastName: familyName,
                    avatar: imageUrl
                  };
                  signInWithSocialGuest(email, '', true, formatedResponse).then();
            
          }}
          onFailure={(err) => {
          console.log(err)
          }}
          cookiePolicy={'single_host_origin'}
          
          />
        <FacebookLogin
                appId={FB_APP_ID}
                autoLoad={false}
                key="guestFb"
                fields="name,email,picture"
                callback={(response) => {
                  if (response && response.email) {
                    const name = response.name.split(' ');
                    const formattedResponse = {
                      email: response.email,
                      firstName: name[0],
                      lastName: name.length > 1 ? name[1] : ' ',
                      avatar: `https://graph.facebook.com/${response.id}/picture?width=160&height=160`
                    };
                    signInWithSocialGuest(response.email, '', true, formattedResponse).then();
                  } else {
                    // setNotificationMessage({
                    //   text: 'Cancelled',
                    //   type: 'error'
                    // });
                    // setError(true);
                  }
                }}
                render={(renderProps) => (
                  <BtnItem id="facebookGuest" onClick={renderProps.onClick}>
                    <Btn kind="facebookAuth" />
                  </BtnItem>
                )}
                />
        
      </BtnList>
    </SignForm>
  );
};

GuestCheckout.propTypes = {
  setStep: func.isRequired
};

export default GuestCheckout;
