import React, {Fragment} from 'react';
import {Divider} from 'antd';
import Icon from 'components/Icon';

import Influencer from 'containers/GuestCheckoutPage/components/Influencer';
import {AttributeColor, StyledSelect, StyledOption} from './styled';
import {useMutation} from '@apollo/client';
import {UpdateItemQuantities} from 'mutations';

const Product = ({influencer, setInfluencer, idx, _idx, currency, setCart, setInlucode, setInfDiscount, product}) => {
  const {name, quantity, variation, key} = product;
  const {image, salePrice, regularPrice} = variation?.node || product || {};
  const {sourceUrl, altText} = image || {};
  const {attributes} = variation || {};

  const [updateQuantity] = useMutation(UpdateItemQuantities);

  function onQuantity(value) {
    updateQuantity({
      variables: {input: {items: [{key, quantity: value}]}},
      update(cache, {data}) {
        cache.modify({
          fields: {
            cart: () => data?.updateItemQuantities?.cart
          }
        });
      }
    });
  }

  return (
    <div key={idx}>
      <div className="item">
        <img src={sourceUrl} alt={altText} />
        <div className="descrition">
          <h3>{name}</h3>
          <div className="characteristic">
            {attributes?.map(({id, value, label}) => (
              <Fragment key={id}>
                <span>
                  <span>{label}:</span> {label === 'Color' ? <AttributeColor color={value} /> : <span>{value}</span>}
                </span>
                <Divider type="vertical" />
              </Fragment>
            ))}
            <span>
              <span>Quantity:</span>
              <StyledSelect
                onChange={onQuantity}
                value={quantity}
                suffixIcon={<Icon color="#8F8F8F" type="chevronDown" />}
              >
                {[1, 2, 3, 4, 5].map((v) => (
                  <StyledOption key={v} value={v}>
                    {v}
                  </StyledOption>
                ))}
              </StyledSelect>
            </span>
          </div>
        </div>
        <div className="price">
          <small>{currency}</small>
          <strong>{salePrice || regularPrice}</strong>
          {salePrice ? (
            <small className="old-price">
              {currency} <span>{regularPrice}</span>
            </small>
          ) : null}
        </div>
      </div>
      <Influencer
        setInfDiscount={setInfDiscount}
        influencer={influencer}
        setInfluencer={setInfluencer}
        idx={`${idx}-${_idx}`}
        setCart={setCart}
        price={regularPrice}
        setInlucode={setInlucode}
      />
    </div>
  );
};

// Product.propTypes = {
//   influencer: PropTypes.objectOf(PropTypes.any).isRequired,
//   setInfluencer: PropTypes.func.isRequired,
//   setCart: PropTypes.func.isRequired,
//   product: PropTypes.objectOf(PropTypes.any).isRequired,
//   idx: PropTypes.number.isRequired,
//   _idx: PropTypes.number.isRequired,
//   currency: PropTypes.string.isRequired,
//   setTotalPrice: PropTypes.func.isRequired,
//   setInlucode: PropTypes.func.isRequired
// };

export default Product;
