import {Select} from 'antd';
import styled from 'styled-components';

export const AttributeColor = styled.span`
  background-color: ${({color}) => (color ? color : 'transparent')};
  width: 35px;
  height: 24px;
  border-radius: 8px;
  border: 1px solid #e4e4e4;
  display: inline-block;
`;

export const StyledSelect = styled(Select)`
  &&& {
    vertical-align: middle;
    .ant-select-selection {
      box-shadow: none;
      border: none;
      height: 16px;
      width: 60px;
      &__rendered {
        height: 100%;
      }
    }

    .ant-select-arrow {
      margin-top: -3px;
    }
  }
`;

export const StyledOption = styled(Select.Option)``;
