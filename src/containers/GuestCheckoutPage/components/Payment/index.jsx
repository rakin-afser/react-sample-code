import React, {Fragment, useState} from 'react';
import {Button} from 'antd';
import {useMutation, useQuery} from '@apollo/client';
import {GoSellElements} from '@tap-payments/gosell';
import CardSecurityInformation from 'containers/GuestCheckoutPage/components/CardSecurityInformation';
import {
  PaymentWrapper,
  CheckboxWrapper,
  RewardsBlockColumn,
  PaymentBlockWrapper,
  PaymentSubTitle,
  CardWrapper,
  SecurityBtn
} from './styled';
import Checkbox from '../Checkbox';
import GoSell from 'containers/GuestCheckoutPage/components/GoSell/GoSell';
import {testSample_ORDER_CREATE} from 'mutations';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import PaymentMethods from 'containers/GuestCheckoutPage/components/PaymentMethods';
// import {ReactComponent as EditIcon} from '../../assets/icon/edit.svg';
// import {ReactComponent as DoubleCheckIcon} from '../../assets/icon/doubleCheck.svg';
import {useUser} from 'hooks/reactiveVars';
import Icon from 'components/Icon';
import Error from 'components/Error';
import {useHistory} from 'react-router';
import Loader from 'components/Loader';
import Midcoins from './components/rewards/Midcoins';
import Coupons from './components/rewards/Coupons';
import {testSample_CHECKOUT} from 'queries';
import {usetestSampleGetSavedCardTokenMutation} from 'hooks/mutations';
import CheckoutFrames from 'components/CheckoutFrames/desktop';
import {Frames} from 'frames-react';
import Btn from 'components/Btn';

const Payment = ({setStep, cartData, selectedAddress, email}) => {
  const [user] = useUser();
  const [paymentLoading, setPaymentLoading] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState('cred');
  const [saveCard, setSaveCard] = useState(false);
  const [securityInfo, setSecurityInfo] = useState(false);
  const [err, setErr] = useState([]);
  const {push} = useHistory();
  const {data: dataPayments, loading: loadingPayments} = useQuery(testSample_CHECKOUT);
  const {checkoutPublicKey, checkoutEnable, tapPublicKey, tapEnable} = dataPayments?.testSampleCheckout || {};

  const initialInput = {
    email: user ? user?.email : email,
    addrAddress1: selectedAddress?.address1,
    addrBuild: selectedAddress?.build,
    addrBlock: selectedAddress?.block,
    addrCity: selectedAddress?.city,
    addrCountry: selectedAddress?.country,
    addrDistrict: selectedAddress?.district,
    addrFirstName: selectedAddress?.firstName,
    addrPhone: selectedAddress?.phone,
    addrRoad: selectedAddress?.road,
    addrApartment: selectedAddress?.apartment,
    addrType: selectedAddress?.type,
    tapRedirectUrl: `${window.location.origin}/checkout/payment-handler`,
    checkoutFailedUrl: `${window.location.origin}/checkout/failed`,
    checkoutRedirectUrl: `${window.location.origin}/checkout/payment-handler`
  };

  const [createOrder, {error}] = useMutation(testSample_ORDER_CREATE, {
    onCompleted(data) {
      setErr([]);
      setPaymentLoading(false);
      const {transactionUrl, orderCreateId, errors} = data?.testSampleOrderCreate || {};
      if (transactionUrl) {
        window.location.replace(transactionUrl);
        return null;
      }
      if (!transactionUrl) {
        console.log(`data`, data);
      }
      if (orderCreateId && !errors?.length) {
        push(`/checkout/success?cod=${orderCreateId}`);
        return null;
      }
      if (errors?.length) {
        setErr(errors);
      }
      return null;
    },
    onError() {
      setPaymentLoading(false);
    }
  });

  const [getCardTokenTap] = usetestSampleGetSavedCardTokenMutation({
    onCompleted(dataCardToken) {
      const {token} = dataCardToken?.testSampleGetSavedCardToken || {};
      const input = {
        ...initialInput,
        paymentMethod: 'tap',
        paymentToken: token
      };
      createOrder({variables: {input}});
    }
  });

  const [getCardTokenCheckout] = usetestSampleGetSavedCardTokenMutation({
    onCompleted(dataCardToken) {
      const {token} = dataCardToken?.testSampleGetSavedCardToken || {};
      const input = {
        ...initialInput,
        paymentMethod: 'checkoutdotcom',
        paymentToken: token
      };
      createOrder({variables: {input}});
    }
  });

  const confirm = (token, method) => {
    if (token) {
      const input = {
        ...initialInput,
        paymentMethod: method,
        paymentToken: token,
        paymentSaveCart: saveCard,
        tapRedirectUrl: method === 'tap' ? `${window.location.origin}/checkout/payment-handler` : undefined,
        checkoutFailedUrl: method === 'checkoutdotcom' ? `${window.location.origin}/checkout/failed` : undefined,
        checkoutRedirectUrl:
          method === 'checkoutdotcom' ? `${window.location.origin}/checkout/payment-handler` : undefined
      };
      createOrder({
        variables: {
          input
        }
      });
    }
    if (!token) {
      setPaymentLoading(false);
    }
  };

  function renderCheckoutCard() {
    const onSubmitCreditCheckout = () => {
      setPaymentLoading(true);
      Frames.submitCard();
    };

    if (loadingPayments) return <Loader key="checkout" wrapperWidth="100%" wrapperHeight="100%" />;
    if (checkoutEnable)
      return (
        <Fragment key="checkout">
          <CheckoutFrames publicKey={checkoutPublicKey} callback={(token) => confirm(token, 'checkoutdotcom')} />
          {['cred'].includes(paymentMethod) ? (
            <>
              <CheckboxWrapper>
                <Checkbox onChange={() => setSaveCard(!saveCard)} checked={saveCard}>
                  Save your card details for a faster and secure checkout
                </Checkbox>
                <p>CVV/CVC number will not be saved</p>
              </CheckboxWrapper>
              <SecurityBtn onClick={() => setSecurityInfo(true)}>
                Read Card Security Information
                <Icon type="circleInfo" width={20} height={20} />
              </SecurityBtn>
            </>
          ) : null}
          {error ? <Error customText="Sorry, your payment didn't pass. Please try again later or contact us." /> : null}
          {err?.length
            ? err?.map((e) => (
                <Error key={e?.code} title={e?.code ? `Code: ${e.code}` : undefined} customText={e?.description} />
              ))
            : null}
          <Btn
            kind="primary-big"
            maxw="270px"
            mt="25px"
            mb="35px"
            disabled={paymentLoading}
            loading={paymentLoading}
            onClick={onSubmitCreditCheckout}
          >
            Confirm &amp; Pay
          </Btn>
        </Fragment>
      );
    return null;
  }

  function renderTapCard() {
    const onSubmitCreditTap = () => {
      setPaymentLoading(true);
      GoSellElements.submit();
    };

    if (loadingPayments) return <Loader key="tap" wrapperWidth="100%" wrapperHeight="100%" />;
    if (tapEnable)
      return (
        <Fragment key="tap">
          <GoSell publicKey={tapPublicKey} callback={(token) => confirm(token, 'tap')} />
          {['cred'].includes(paymentMethod) ? (
            <>
              <CheckboxWrapper>
                <Checkbox onChange={() => setSaveCard(!saveCard)} checked={saveCard}>
                  Save your card details for a faster and secure checkout
                </Checkbox>
                <p>CVV/CVC number will not be saved</p>
              </CheckboxWrapper>
              <SecurityBtn onClick={() => setSecurityInfo(true)}>
                Read Card Security Information
                <Icon type="circleInfo" width={20} height={20} />
              </SecurityBtn>
            </>
          ) : null}
          {error ? <Error customText="Sorry, your payment didn't pass. Please try again later or contact us." /> : null}
          {err?.length
            ? err?.map((e) => (
                <Error key={e?.code} title={e?.code ? `Code: ${e.code}` : undefined} customText={e?.description} />
              ))
            : null}
          <Btn
            kind="primary-big"
            maxw="270px"
            mt="25px"
            mb="35px"
            disabled={paymentLoading}
            loading={paymentLoading}
            onClick={onSubmitCreditTap}
          >
            Confirm &amp; Pay
          </Btn>
        </Fragment>
      );
    return null;
  }

  function renderDebitButtons() {
    const buttons = [];
    const onSubmitDebit = (method) => {
      setPaymentLoading(true);
      const input = {
        ...initialInput,
        paymentMethod: method,
        paymentToken: 'src_bh.benefit'
      };

      createOrder({variables: {input}});
    };

    // if (tapEnable ) {
    buttons.push(
      <Btn
        key="tap"
        kind="primary-big"
        maxw="270px"
        mt="25px"
        mb="35px"
        disabled={paymentLoading}
        loading={paymentLoading}
        onClick={() => onSubmitDebit('tap')}
      >
        Confirm &amp; Pay
      </Btn>
    );
    // }

    // if (checkoutEnable ) {
    //   buttons.push(
    //     <Btn
    //       key="tap"
    //       kind="primary-big"
    //       maxw="270px"
    //       mt="25px"
    //       mb="35px"
    //       disabled={paymentLoading}
    //       loading={paymentLoading}
    //       onClick={() => onSubmitDebit('checkoutdotcom')}
    //     >
    //       Confirm &amp; Pay
    //     </Btn>
    //   );
    // }
    return buttons;
  }

  function onSubmitCod() {
    setPaymentLoading(true);
    const input = {
      ...initialInput,
      tapRedirectUrl: undefined,
      checkoutFailedUrl: undefined,
      checkoutRedirectUrl: undefined,
      paymentMethod: 'cod'
    };

    createOrder({
      variables: {input},
      update(cache) {
        cache.modify({
          fields: {
            cart: () => {}
          }
        });
      }
    });
  }

  function renderSavedCardsButtons() {
    const buttons = [];
    if (tapEnable) {
      const onSubmitCardTokenTap = () => {
        setPaymentLoading(true);
        getCardTokenTap({variables: {input: {card_id: paymentMethod}}});
      };
      buttons.push(
        <Btn
          key="tap"
          kind="primary-big"
          maxw="270px"
          mt="25px"
          mb="35px"
          disabled={paymentLoading}
          loading={paymentLoading}
          onClick={onSubmitCardTokenTap}
        >
          Confirm &amp; Pay
        </Btn>
      );
    }

    if (checkoutEnable) {
      const onSubmitCardTokenCheckout = () => {
        setPaymentLoading(true);
        getCardTokenCheckout({variables: {input: {card_id: paymentMethod}}});
      };

      buttons.push(
        <Btn
          key="checkout"
          kind="primary-big"
          maxw="270px"
          mt="25px"
          mb="35px"
          disabled={paymentLoading}
          loading={paymentLoading}
          onClick={onSubmitCardTokenCheckout}
        >
          Confirm &amp; Pay
        </Btn>
      );
    }
    return buttons;
  }

  function renderCardWrapper() {
    if (paymentMethod === 'cred') return [renderCheckoutCard(), renderTapCard()];
    if (paymentMethod === 'deb') return renderDebitButtons();

    if (paymentMethod === 'cod')
      return (
        <Btn
          kind="primary-big"
          maxw="270px"
          mt="25px"
          mb="35px"
          disabled={paymentLoading}
          loading={paymentLoading}
          onClick={onSubmitCod}
        >
          Confirm &amp; Pay
        </Btn>
      );

    return renderSavedCardsButtons();
  }

  return (
    <PaymentWrapper>
      <div className="order-wrapper">
        <div className="other-tab">
          <h3 className="title">1. Shipping Address</h3>
          <span>
            {[
              selectedAddress?.postCode,
              selectedAddress?.build,
              selectedAddress?.address1,
              selectedAddress?.city,
              selectedAddress?.district,
              getCountryFromCode(selectedAddress?.country)
            ]
              .filter((item) => !!item)
              .join(', ')}
            {!!selectedAddress?.addressNickname && ' | '}
            {selectedAddress?.addressNickname}
          </span>
          <Button type="link" shape="circle" className="edit-icon" onClick={() => setStep('details')}>
            <Icon type="edit" width={18} height={18} fill="#545454" />
          </Button>
        </div>
        <div className="other-tab">
          <h3 className="title">2. Review Order</h3>
          <span className="description">Total to Pay {cartData?.total}</span>
          <Button type="link" shape="circle" className="edit-icon" onClick={() => setStep('review')}>
            <Icon type="edit" width={18} height={18} fill="#545454" />
          </Button>
        </div>
        <PaymentBlockWrapper>
          <div className="border-payment payment">
            <h3 className="payment-title">3. Payment</h3>
            <Midcoins />
            <Coupons />
          </div>
          {/* <Divider /> */}
          <RewardsBlockColumn>
            <PaymentSubTitle>Pay with</PaymentSubTitle>
            <PaymentMethods paymentMethod={paymentMethod} setPaymentMethod={(value) => setPaymentMethod(value)} />
          </RewardsBlockColumn>
          <CardWrapper>{renderCardWrapper()}</CardWrapper>
        </PaymentBlockWrapper>
      </div>
      <CardSecurityInformation isOpen={securityInfo} closeModal={() => setSecurityInfo(false)} />
    </PaymentWrapper>
  );
};

export default Payment;
