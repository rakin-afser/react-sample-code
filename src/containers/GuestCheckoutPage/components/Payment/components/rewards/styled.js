import {mainFont, secondaryFont} from 'constants/fonts';
import styled from 'styled-components';
import Icon from 'components/Icon';

export const CheckboxWrapper = styled.div`
  padding: 14px 0;

  > p {
    margin-bottom: 0;
    padding-left: 30px;
    font-size: 12px;
  }
`;

export const RewardsAdditionalTitle = styled.div`
  font-size: 14px;
  color: #545454;
  margin-top: 8px;
  flex-grow: 1;
`;

export const RewardsBlock = styled.div`
  padding: 16px 0 0 0;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;

  & .ant-form-item {
    width: 200px;
    margin: 0 12px 0 0;
  }

  & .ant-form-item-label {
  }

  & button {
    margin: 0 0 7px 12px;
  }

  & h3 {
    font-size: 16px !important;
  }
`;

export const AppliedCoins = styled.div`
  font-family: ${mainFont};
  color: #545454;
  font-size: 14px;
  font-weight: 700;
`;

export const CoinTitle = styled.span`
  color: #398287;
  margin-right: 5px;
`;

export const Cancel = styled(Icon)`
  vertical-align: bottom;
  cursor: pointer;
`;

export const List = styled.div`
  margin-top: 10px;
`;

export const Coupon = styled.span`
  margin-right: 5px;
`;
