import React, {useState} from 'react';
import {Form, Input, Checkbox} from 'antd';
import Btn from 'components/Btn';
import {CheckboxWrapper, RewardsAdditionalTitle, RewardsBlock, Cancel, Coupon, List} from './styled';
import Icon from 'components/Icon';
import {useApplyCouponMutation, useRemoveCouponsMutation} from 'hooks/mutations';
import {useAppliedCouponsQuery} from 'hooks/queries';
import {primaryColor} from 'constants/colors';

const Coupons = () => {
  const [coupon, setCoupon] = useState('');
  const [visible, setVisible] = useState(false);
  const [apply, {loading}] = useApplyCouponMutation({
    variables: {input: {code: coupon}},
    onCompleted() {
      setCoupon('');
    }
  });
  const [remove, {loading: loadingRemove}] = useRemoveCouponsMutation();
  const {data: dataApplied} = useAppliedCouponsQuery({fetchPolicy: 'cache-only'});
  const {appliedCoupons} = dataApplied?.cart || {};

  return (
    <CheckboxWrapper>
      <Checkbox onChange={() => setVisible(!visible)} checked={visible}>
        Use a Coupon
        {/* <RewardsAdditionalTitle>Coupon gives you 10% discount</RewardsAdditionalTitle> */}
      </Checkbox>
      {appliedCoupons?.length ? (
        <List>
          {appliedCoupons?.map((c) => (
            <div key={c.code}>
              <Coupon>{c.code}</Coupon>
              {loadingRemove ? (
                <Icon type="loader" fill={primaryColor} width={30} height={6} />
              ) : (
                <Cancel
                  type="cancel"
                  onClick={() => remove({variables: {input: {codes: [c.code]}}})}
                  color="#000"
                  width={15}
                  height={15}
                />
              )}
            </div>
          ))}
        </List>
      ) : null}

      {visible ? (
        <RewardsBlock>
          <Form.Item label="Enter Coupon Code" className="coupen-field">
            <Input onChange={(e) => setCoupon(e.target.value)} value={coupon} />
          </Form.Item>
          <Btn onClick={() => apply()} loading={loading} disabled={!coupon} kind="pr-bare-sm">
            Apply
          </Btn>
        </RewardsBlock>
      ) : null}
    </CheckboxWrapper>
  );
};

export default Coupons;
