import React, {useEffect, useState} from 'react';
import {Form, Input, Checkbox} from 'antd';
import {useUser} from 'hooks/reactiveVars';
import {userId} from 'util/heplers';
import Btn from 'components/Btn';
import {AppliedCoins, CheckboxWrapper, CoinTitle, RewardsAdditionalTitle, RewardsBlock, Cancel} from './styled';
import {useGetCoinsLazyQuery, useAppliedMidcoinsQuery} from 'hooks/queries';
import {useApplyMidcoinsDiscountMutation, useRemoveMidcoinsDiscountMutation} from 'hooks/mutations';
import Icon from 'components/Icon';
import {primaryColor} from 'constants/colors';

const Midcoins = () => {
  const [qty, setQty] = useState();
  const [visible, setVisible] = useState(false);
  const [user] = useUser();
  const [get, {data}] = useGetCoinsLazyQuery();
  const {databaseId} = user || {};
  const {currency, coinsConversionRate, availableMidcoins} = data?.user || {};
  const coinsConvertedValue = qty ? (qty * coinsConversionRate)?.toFixed(2) : 0;
  const {data: dataApplied} = useAppliedMidcoinsQuery({fetchPolicy: 'cache-only'});
  const {appliedMidCoins} = dataApplied?.cart || {};
  const [apply, {loading: loadingApply}] = useApplyMidcoinsDiscountMutation({
    qty,
    variables: {input: {qty: parseFloat(qty || 0)}},
    onCompleted() {
      setQty(null);
    }
  });
  const [remove, {loading: loadingRemove}] = useRemoveMidcoinsDiscountMutation({qty: appliedMidCoins?.qty});

  useEffect(() => {
    if (databaseId || userId) {
      get({variables: {id: databaseId || userId}});
    }
  }, [databaseId, get]);

  const onChangeMidcoins = ({target: {value}}) => {
    if (/^([0-9]+|\d+)$/i.test(value) || /\./i.test(value) || !value) {
      const currentValue = availableMidcoins > value ? value : availableMidcoins;
      setQty(currentValue);
    }
  };

  return (
    <CheckboxWrapper>
      <Checkbox onChange={() => setVisible(!visible)} checked={visible}>
        Use Rewards Points
        <RewardsAdditionalTitle>
          Total available <CoinTitle>MidCoins = {availableMidcoins || 0}</CoinTitle>
        </RewardsAdditionalTitle>
      </Checkbox>
      {appliedMidCoins ? (
        <AppliedCoins>
          Applied <CoinTitle>MidCoins = {appliedMidCoins?.qty}</CoinTitle>
          {loadingRemove ? (
            <Icon type="loader" fill={primaryColor} width={30} height={6} />
          ) : (
            <Cancel type="cancel" onClick={() => remove()} color="#000" width={15} height={15} />
          )}
        </AppliedCoins>
      ) : null}
      {visible ? (
        <RewardsBlock>
          <Form.Item label="Redeem">
            <Input placeholder="0" onChange={onChangeMidcoins} value={qty} />
          </Form.Item>
          <Form.Item label={`Value in “${currency}”`}>
            <Input readOnly value={`= ${currency} ${coinsConvertedValue}`} />
          </Form.Item>
          <Btn onClick={() => apply()} kind="pr-bare-sm" disabled={!qty} loading={loadingApply}>
            Apply
          </Btn>
        </RewardsBlock>
      ) : null}
    </CheckboxWrapper>
  );
};

export default Midcoins;
