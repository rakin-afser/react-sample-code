import styled from 'styled-components';

export const PaymentWrapper = styled.div`
  width: 70%;
  .order-wrapper {
    margin-right: 40px;

    .other-tab {
      margin: 0 0 24px;
      border: 1px solid #e4e4e4;
      box-sizing: border-box;
      border-radius: 3px;
      display: flex;
      align-items: center;
      padding: 11px 24px;
      font-family: Helvetica;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 132%;
      letter-spacing: -0.024em;
      color: #545454;

      & .title {
        margin-bottom: 0;
        margin-right: 10px;
        font-size: 18px;
        color: #000000;
      }

      .description {
        font-family: SF Pro Display;
        font-size: 14px;
        line-height: 140%;
        color: #545454;
      }

      .edit-icon {
        margin-left: auto;
      }
    }

    .ant-checkbox-wrapper {
      font-family: Helvetica Neue;
      font-weight: bold;
      color: #000000;

      .ant-checkbox-checked .ant-checkbox-inner {
        background-color: #000;
      }

      .ant-checkbox {
        margin-right: 4px;

        .ant-checkbox-inner {
          border-color: #000;
          border-radius: 4px;
          width: 20px;
          height: 20px;
        }

        .ant-checkbox-inner::after {
          left: 30%;
        }
      }

      .ant-checkbox:after {
        border: none;
      }
    }
  }

  .payment-title {
    font-weight: 600;
    font-size: 20px;
    line-height: 124%;
    color: inherit;
    padding: 0;
    margin-bottom: 24px;
  }

  .border-payment-method {
    padding-bottom: 24px !important;
  }

  .mid-coins {
    padding-bottom: 32px;
    margin-bottom: 32px;
    border-bottom: 1px solid #e4e4e4;

    .ant-form-item {
      margin-bottom: 0;
    }

    .coupen-form {
      padding: 16px 0 0 33px;

      .ant-form-item label {
        font-weight: normal;
        font-size: 11px;
        line-height: 140%;
        color: #000000;
      }

      .ant-col {
        max-width: 135px;
        margin-right: 24px;
      }

      .ant-input {
        color: #000;
      }
    }

    .submit-coupon {
      margin-left: 0 !important;
      height: 28px !important;
      width: 72px !important;
      border-color: #000 !important;
      font-weight: 500;
      font-size: 12px;
      line-height: 140%;
      color: #000000 !important;
    }
  }

  .coupon {
    padding-bottom: 32px;
    margin-bottom: -16px;
    border-bottom: 1px solid #e4e4e4;

    .ant-form-item label {
      font-weight: 500;
      font-size: 12px;
      line-height: 140%;
      color: #000000;
    }

    .ant-form-item-label > label::after {
      display: none;
    }

    .submit-coupon {
      margin-top: 42px !important;
      margin-left: 24px !important;
      height: 28px !important;
      width: 72px !important;
      border-color: #000 !important;
      font-weight: 500;
      font-size: 12px;
      line-height: 140%;
      color: #000000 !important;
    }

    .discount-check {
      margin-top: 44px !important;
    }

    .ant-row,
    .ant-col,
    input {
      max-width: 297px;
    }
  }

  .payment {
    box-sizing: border-box;
    border-radius: 3px;
    font-family: SF Pro Display;
    font-style: normal;
    font-weight: normal;
    line-height: 124%;
    letter-spacing: 0.019em;
    color: #000000;
    border-bottom: 0;

    .cupon-checkbox {
      & > span {
        margin-left: 123px;
        color: #545454;
      }
    }

    .coupen-form {
      display: flex;
      align-items: center;

      .coupen-field {
        margin-top: 26px;
        font-family: Helvetica Neue;
        font-style: normal;
        font-weight: bold;
        font-size: 14px;
        line-height: 140%;
        color: #000000;
        width: 406px;
        margin-left: 33px;
        margin-bottom: 0;

        input {
          width: 406px;
          font-family: SF Pro Display;
          color: #000000;
        }
      }

      .submit-coupon {
        margin-top: 20px;
        border: 1px solid #ed484f;
        box-sizing: border-box;
        width: 96px;
        height: 36px;
        color: #ed484f;
        margin-left: 24px;
      }

      .discount-check {
        font-size: 12px;
        color: #ed494f;
        margin-top: 25px;
        margin-left: 25px;
      }
    }
  }

  .payment-method {
    border-top: none;

    & > div {
      display: grid;
      grid-template-columns: 406px;

      .ant-radio-group {
        display: grid !important;

        .ant-radio-wrapper {
          display: flex;
          align-items: center;
          box-sizing: border-box;
          border-radius: 4px;
          margin: 0px 0 16px;
          font-family: Helvetica Neue;
          line-height: 140%;
          color: #000000;

          & > span:last-child {
            width: 100%;
          }
        }

        .method {
          display: flex;
          align-items: center;
          justify-content: space-between;
        }
      }

      .description {
        font-family: Helvetica Neue;
        font-style: normal;
        font-weight: normal;
        font-size: 12px;
        line-height: 132%;
        color: #7a7a7a;
        margin-bottom: 24px;
      }
    }

    .confirm-pay {
      font-family: Helvetica Neue;
      font-size: 18px;
      letter-spacing: -0.024em;
      color: #ffffff;
      width: 268px;
      height: 46px;
      transition: all 0.3s ease;

      &:disabled {
        background: #f9c0c3;

        &:hover {
          color: #fff;
        }
      }
    }
  }
`;

export const CheckboxWrapper = styled.div`
  padding: 14px 0;

  > p {
    margin-bottom: 0;
    padding-left: 30px;
    font-size: 12px;
  }
`;

export const RewardsBlock = styled.div`
  padding: 16px 0 0 0;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;

  & .ant-form-item {
    width: 200px;
    margin: 0 12px 0 0;
  }

  & .ant-form-item-label {
  }

  & button {
    border-radius: 24px;
    border: 1px solid #ed484f;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: 500;
    font-size: 12px;
    color: #ed484f;
    padding: 5px 19px;
    margin: 0 0 7px 12px;
  }

  & h3 {
    font-size: 16px !important;
  }
`;

export const RewardsBlockColumn = styled(RewardsBlock)`
  flex-direction: column;
  align-items: flex-start;
  padding: 16px 30px 0;
`;

export const RewardsAdditionalTitle = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #545454;
  margin-top: 8px;
  flex-grow: 1;

  i {
    position: relative;
    left: 3px;
    top: 1px;
  }
`;

export const PaymentBlockWrapper = styled.div`
  border: 1px solid #e4e4e4;
  border-radius: 3px;
  padding: 24px;
`;

export const PaymentSubTitle = styled.div`
  font-weight: 600;
  font-size: 16px;
  color: #000;
  padding: 0;
  margin-bottom: 24px;
`;

export const CardWrapper = styled.div`
  margin-top: 16px;
  margin-left: 30px;
  max-width: 569px;
`;

export const SecurityBtn = styled.button`
  display: flex;
  align-items: center;
  font-weight: 500;
  font-size: 14px;
  color: #ed494f;
  transition: ease 0.4s;
  cursor: pointer;

  i {
    margin-left: 10px;

    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:hover {
    color: #000;

    i {
      svg {
        path {
          stroke: #000;
        }
      }
    }
  }
`;
