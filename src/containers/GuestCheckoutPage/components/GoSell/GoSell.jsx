import React from 'react';
import {GoSellElements} from '@tap-payments/gosell';
import {Wrapper, Message} from './styled';

const GoSell = ({callback = () => {}, publicKey}) => {
  return (
    <Wrapper>
      <GoSellElements
        gateway={{
          publicKey,
          language: 'en',
          supportedCurrencies: 'all',
          supportedPaymentMethods: 'all',
          notifications: 'msg',
          saveCardOption: true,
          customerCards: true,
          callback: (res) => callback(res?.id),
          labels: {
            cardNumber: 'Card Number',
            expirationDate: 'MM/YY',
            cvv: 'CVV',
            cardHolder: 'Name on Card',
            actionButton: 'Pay'
          },
          style: {
            base: {
              color: '#000',
              lineHeight: '18px',
              fontFamily: `"SF Pro Display", sans-serif`,
              fontSmoothing: 'antialiased',
              fontSize: '14px',
              '::placeholder': {
                color: '#8f8f8f',
                fontSize: '14px'
              }
            },
            invalid: {
              color: 'red',
              iconColor: '#fa755a'
            }
          }
        }}
      />
      <Message id="msg" />
    </Wrapper>
  );
};

export default GoSell;
