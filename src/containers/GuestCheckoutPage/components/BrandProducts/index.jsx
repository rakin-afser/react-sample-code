import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Select, Rate} from 'antd';

import Product from 'containers/GuestCheckoutPage/components/Product';
import {ErrorDelivery} from 'containers/GuestCheckoutPage/styled';
import userPlaceholder from 'images/placeholders/user.jpg';

const {Option} = Select;

const BrandProducts = ({influencer, setInfluencer, idx, cart, subCartIndex, subCartData = {}, setMethod, cartData}) => {
  const [influCode, setInlucode] = useState(false);
  const [infDiscount, setInfDiscount] = useState(0);

  const {items, store, totals, shipping: shippingMethods} = subCartData || {};
  const onSetMethod = (setShippingMetodId, setShippingMetodSubcart) => {
    setMethod({
      variables: {
        input: {
          setShippingMetodId,
          setShippingMetodSubcart
        }
      },
      optimisticResponse: {
        setSubcartShippingMethod: {
          __typename: 'SetSubcartShippingMethodPayload',
          cart: cartData?.cart,
          calculateSuccess: true
        }
      },
      update(cache, {data}) {
        cache.modify({
          fields: {
            cart(existing) {
              return {
                ...existing,
                subcarts: {
                  ...data?.setSubcartShippingMethod?.cart?.subcarts,
                  subcarts: data?.setSubcartShippingMethod?.cart?.subcarts?.subcarts?.map((s, i) => ({
                    ...s,
                    shipping: {
                      ...s?.shipping,
                      chosenMethodId: i === setShippingMetodSubcart ? setShippingMetodId : s?.shipping?.chosenMethodId
                    }
                  }))
                },
                total: data?.setSubcartShippingMethod?.cart?.total
              };
            }
          }
        });
      }
    });
  };
  return (
    <div className="product">
      <div className="product-title">
        <img src={store?.gravatar || userPlaceholder} alt={store?.name} />
        <div>
          <h3>{store?.name}</h3>
          <div className="info">
            <span className="rating">{store?.rating}</span> <Rate disabled defaultValue={store?.rating} />{' '}
            {store?.totalFollowers ? <span className="followers">({store?.totalFollowers})</span> : null}
          </div>
        </div>
      </div>
      <div className="list-item">
        {items?.map((item, index) => {
          const {variationId, key} = item || {};
          return (
            <Product
              key={key}
              influencer={influencer}
              setInfluencer={setInfluencer}
              _idx={index}
              idx={idx}
              product={item}
              setInfDiscount={setInfDiscount}
              setInlucode={setInlucode}
            />
          );
        })}
        <div className="shipping">
          <div className="shipping-title">
            <h3>
              Select Delivery Option <span>*</span>
            </h3>
          </div>
        </div>
        <div className="shipping-options">
          {shippingMethods?.availableMethods?.map((item) => (
            <label key={item?.id} className="shipping-options__field">
              <input
                type="radio"
                name={`method_${store?.name}`}
                id={`method_${store?.name}_${item?.id}`}
                checked={shippingMethods?.chosenMethodId === item?.id}
                onChange={() => onSetMethod(item?.id, subCartIndex)}
              />
              <span className="icon" />
              <span className="text">
                {item?.label}
                {'  '}
                {!item?.cost && <strong style={{color: '#26A95E'}}>(Free Shipping)</strong>}
              </span>
              <span className="price">{item?.cost}</span>
            </label>
          ))}
        </div>

        {cart.error && cart.shipping && cart.shipping.length && cart.shipping[idx] === null ? (
          <ErrorDelivery>Please select delivery option to continue</ErrorDelivery>
        ) : null}

        <div className="total">
          <div className="items">
            Items: <span className="price">{totals?.itemsTotal}</span>
          </div>
          <div className="items">
            Shipping Charges: <span className="price">{totals?.shipping}</span>
          </div>
          {influCode ? (
            <div className="items">
              Influencer Discount:{' '}
              <span className="price" style={{color: '#ED484F'}}>
                {infDiscount}
              </span>
            </div>
          ) : null}
          {/* <div className="coins">
            Shop*Coins:{' '}
            <span className="price">
              <small>{currency}</small>
              {shopCoins}
            </span>
          </div> */}
          <div className="total-price">
            {store?.name} Total: <span className="price">{totals?.total}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

// BrandProducts.propTypes = {
//   setCart: PropTypes.func.isRequired,
//   cart: PropTypes.objectOf(PropTypes.any).isRequired,
//   influencer: PropTypes.objectOf(PropTypes.any).isRequired,
//   setInfluencer: PropTypes.func.isRequired,
//   idx: PropTypes.number.isRequired,
//   shopCoins: PropTypes.string.isRequired,
//   brand: PropTypes.objectOf(PropTypes.any).isRequired
// };

export default BrandProducts;
