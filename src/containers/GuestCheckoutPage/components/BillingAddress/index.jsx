import React, {useState} from 'react';
import {Form, Input, Select} from 'antd';

import {Wrapper, Row, Text} from 'containers/GuestCheckoutPage/components/BillingAddress/styled';
import exampleData from 'containers/GuestCheckoutPage/assets/data/exampleData';
import StyledSelect from 'containers/GuestCheckoutPage/components/Select';

const {country} = exampleData;
const {Option} = Select;

const BillingAddress = ({form, onChange, formValues, initialValues = {}}) => {
  const {getFieldDecorator, validateFields} = form;

  const onSubmit = (e) => {
    e.preventDefault();
    validateFields((err, values) => {
      if (!err) {
        console.log(values);
      }
    });
  };

  const onChangeForm = (fieldName, value) => {
    onChange({...formValues, [fieldName]: value});
  };

  return (
    <Wrapper>
      <Form onSubmit={onSubmit}>

        <Text>Enter your billing address</Text>

        <Row>
          <Form.Item label="Address Line 1">
            {getFieldDecorator('billingAddress1', {
              initialValue: initialValues?.billingAddress1
            })(<Input placeholder="" onChange={(e) => onChangeForm('billingAddress1', e.target.value)} />)}
          </Form.Item>

          <Form.Item label="Address Line 2">
            {getFieldDecorator('billingAddress2', {
              initialValue: initialValues?.billingAddress2
            })(<Input placeholder="" onChange={(e) => onChangeForm('billingAddress2', e.target.value)} />)}
          </Form.Item>
        </Row>

        <Row>
          <Form.Item label="City">
            {getFieldDecorator('billingCity', {
              rules: [{required: true, message: 'Enter a valid City'}],
              initialValue: initialValues?.billingCity
            })(<Input placeholder="" onChange={(e) => onChangeForm('billingCity', e.target.value)} />)}
          </Form.Item>

          <Form.Item label="Country">
            {getFieldDecorator('billingCountry', {
              rules: [{required: true, message: 'Enter a valid country'}],
              initialValue: initialValues?.billingCountry
            })(
              <StyledSelect
                placeholder="Select Your Country"
                showSearch
                allowClear
                onChange={(value) => onChangeForm('billingCountry', country[value]?.code)}
                filterOption={(input, {props: {value}}) =>
                  country[value]?.name.toLowerCase().startsWith(input.toLowerCase())
                }
              >
                {Object.keys(country).map((key) => (
                  <Option value={key} key={key}>
                    {key}
                  </Option>
                ))}
              </StyledSelect>
            )}
          </Form.Item>

          <Form.Item label="Zip Code">
            {getFieldDecorator('billingPostCode', {
              initialValue: initialValues?.billingPostCode
            })(<Input placeholder="" onChange={(e) => onChangeForm('billingPostCode', e.target.value)} />)}
          </Form.Item>
        </Row>

      </Form>
    </Wrapper>
  );
};

const BillingAddressForm = Form.create({
  name: 'billingAddress',
  onFieldsChange(props, changedFields) {
    const {name, value} = changedFields[Object.keys(changedFields)[0]];
  }
})(BillingAddress);

export default BillingAddressForm;
