import styled from 'styled-components/macro';
import {Form, Input, Radio, Button} from 'antd';

export const Wrapper = styled.div`
  width: 100%;

  & .ant-form {
    margin-bottom: 16px;
  }

  & .ant-form-item label {
    line-height: normal;
  }
`;

export const CheckoutInput = styled.input`
  width: ${({width}) => width || '250px'};
`;

export const Text = styled.div`
  color: #000;
  margin: 0 0 10px 0;
`;

export const Row = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 14px;

  & .ant-form-item {
    flex-grow: 1;
  }
`;
