import React from 'react';
import {func, string, node, objectOf, any} from 'prop-types';
import {Button} from 'antd';

import {ReviewOrderWrapper} from '../styled';
import {ReactComponent as EditIcon} from '../assets/icon/edit.svg';
import exampleData from '../assets/data/exampleData';
import StyledButton from './Button';
import BrandProducts from './BrandProducts';

const {chosenProduct} = exampleData;

const ReviewOrder = ({
  setStep,
  setEditLocation,
  setCart,
  shopCoins,
  shippingAdress,
  checkShipping,
  influencer,
  setInfluencer,
  cart
}) => {
  const toPayment = () => {
    const error = checkShipping();
    setCart((prev) => ({...prev, error}));
    if (!error) {
      setStep('payment');
    }
  };

  const checkValidShipping = () => {
    return !cart.shipping.length || !!cart.shipping.filter((el) => el === false || el === null).length;
  };

  return (
    <ReviewOrderWrapper>
      <div className="order-wrapper">
        <div className="other-tab">
          <h3 className="title">1. Shipping Address</h3>
          {shippingAdress}
          <Button type="link" shape="circle" className="edit-icon">
            <EditIcon onClick={() => setEditLocation(true)} />
          </Button>
        </div>
        <div className="orders">
          <div className="order">
            <h2 className="order-title">2. Review Order</h2>
            <div className="products-list">
              {chosenProduct.map((brand, key) => {
                return (
                  <BrandProducts
                    brand={brand}
                    idx={key}
                    key={key}
                    shopCoins={shopCoins}
                    influencer={influencer}
                    setInfluencer={setInfluencer}
                    setCart={setCart}
                    cart={cart}
                  />
                );
              })}
            </div>
          </div>
          <StyledButton
            ghost
            className="payment-button"
            rightIcon
            onClick={toPayment}
            disabled={checkValidShipping()}
            styles="padding-left: 0px;color: #000 !important;border-color: #000 !important;"
            fill="#000"
          >
            Proceed to Payment
          </StyledButton>
        </div>
        <div className="other-tab">
          <h3 className="title">3. Payment</h3>
        </div>
      </div>
    </ReviewOrderWrapper>
  );
};

ReviewOrder.propTypes = {
  setStep: func.isRequired,
  setEditLocation: func.isRequired,
  setCart: func.isRequired,
  shopCoins: string.isRequired,
  shippingAdress: node.isRequired,
  checkShipping: func.isRequired,
  influencer: objectOf(any).isRequired,
  setInfluencer: func.isRequired,
  cart: objectOf(any).isRequired
};

export default ReviewOrder;
