import React from 'react';
import {objectOf, any, func} from 'prop-types';
import {Form, Input} from 'antd';

import {Submit} from '../styled';

const CreateAccountForm = ({form, setCreateAccount}) => {
  const {getFieldDecorator} = form;

  const onSubmitHandler = (e) => {
    e.preventDefault();
    form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        setCreateAccount('newAccount');
      }
    });
  };

  return (
    <Form onSubmit={onSubmitHandler}>
      <div className="name-field">
        <Form.Item label="First Name">
          {getFieldDecorator('firstName', {
            rules: [
              {
                required: true,
                message: 'First name is required'
              }
            ]
          })(<Input placeholder="" />)}
        </Form.Item>
        <Form.Item label="Last Name">
          {getFieldDecorator('lastName', {
            rules: [
              {
                required: true,
                message: 'Last name is required'
              }
            ]
          })(<Input placeholder="" />)}
        </Form.Item>
      </div>
      <Form.Item label="Select Username">
        {getFieldDecorator('username', {
          rules: []
        })(<Input placeholder="Enter a Username" />)}
      </Form.Item>
      <Form.Item label="Set Password">
        {getFieldDecorator('setPassword', {
          rules: []
        })(<Input placeholder="Create a Password" />)}
      </Form.Item>
      <Submit type="danger" htmlType="submit" ghost>
        Create Account
      </Submit>
    </Form>
  );
};

CreateAccountForm.propTypes = {
  form: objectOf(any).isRequired,
  setCreateAccount: func.isRequired
};

const WrappedCreateAccountForm = Form.create({
  name: 'create-account-guest'
})(CreateAccountForm);

export default WrappedCreateAccountForm;
