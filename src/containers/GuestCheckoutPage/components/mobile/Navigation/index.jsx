import React from 'react';
import { func, string } from 'prop-types';
import { useHistory } from 'react-router-dom';

import Divider from 'components/Divider';
import Icon from 'components/Icon';

import {
  Wrapper,
  Container
} from './styled';

const Navigation = ({ step, setStep, title }) => {
  const history = useHistory();

  const onSwitchStep = () => {
    switch(step) {
      case 'selectLocation': return 'shippingAddress'
      default: return 'authorization';
    }
  };

  return <Wrapper>
    <Container>
      {step !== 'thankYou' ? <Icon type="arrowBack" onClick={() => setStep(onSwitchStep())}/> : null}
      {title}
      {step === 'thankYou' ? <Icon type="close" className="close" color="#000" onClick={() => history.push('/')}/> : null}
    </Container>
    {
      step === 'selectLocation'
        ? ''
        :
        <Divider
          style={{
            height: 8,
            background: '#FAFAFA'
          }}
        />
    }
  </Wrapper>
};

Navigation.defaultProps = {
  title: 'Secure Checkout',
  step: 'authorization'
};

Navigation.propTypes = {
  step: string.isRequired,
  setStep: func.isRequired,
  title: string
};

export default Navigation;