import React from 'react';
import {func, string} from 'prop-types';
import Icon from 'components/Icon';

import {Container, IconBox} from './styled';

const Navigation2 = ({onClickBack, onClickClose, title}) => (
  <Container>
    <IconBox styles={{
      position: 'relative',
      left: -6
    }}>
      {onClickBack && (
        <Icon
          type="back"
          color="#000"
          onClick={onClickBack}
        />
      )}
    </IconBox>
    <span>{title}</span>
    <IconBox styles={{
      position: 'relative',
      right: -9,
      top: 2
    }}>
      {onClickClose && (
        <Icon
          type="close"
          color="#000"
          height={24}
          width={24}
          onClick={onClickClose}
        />
      )}
    </IconBox>
  </Container>
);

Navigation2.defaultProps = {
  title: '',
  onClickBack: null,
  onClickClose: null
};

Navigation2.propTypes = {
  title: string,
  onClickBack: func,
  onClickClose: func
};

export default Navigation2;
