import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 16px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #000000;
  min-height: 72px;
  border-bottom: 1px solid #efefef;
  cursor: pointer;
`;

export const IconBox = styled.span.attrs(({styles}) => ({style: {...styles}}))`
  height: 29px;
  width: 29px;
`
