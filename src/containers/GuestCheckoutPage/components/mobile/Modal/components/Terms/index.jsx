import React, {useState} from 'react';
import {Wrapper, AgreementWrap} from 'containers/GuestCheckoutPage/components/mobile/Modal/components/Terms/styled';
import FadeOnMount from 'components/Transitions';
import Scrollbars from 'react-scrollbars-custom';
import Checkbox from 'containers/GuestCheckoutPage/components/Checkbox';

const Terms = () => {
  const [agree, setAgree] = useState(false);

  const Agreement = () => {
    return (
      <AgreementWrap>
        <Checkbox
          onChange={() => {
            setAgree((prev) => !prev);
          }}
          checked={agree}
        />
        I agree to <span>&nbsp;&nbsp;Terms and Conditions</span>
      </AgreementWrap>
    );
  };

  return (
    <FadeOnMount>
      <Scrollbars
        clientWidth={4}
        noDefaultStyles={false}
        noScroll={false}
        style={{height: '560px', width: '100%'}}
        thumbYProps={{className: 'thumbY'}}
      >
        <Wrapper>
          <p>
            Please read the following terms and conditions carefully as it sets out the terms of a legally binding
            agreement between you (the reader) and Business Standard Private Limited.
          </p>
          <p>
            <strong>1) Introduction</strong>
            <br /> This following sets out the terms and conditions on which you may use the content on
            business-standard.com website, business-standard.com's mobile browser site, Business Standard instore
            Applications and other digital publishing services (www.smartinvestor.in, www.bshindi.com and
            www.bsmotoring,com) owned by Business Standard Private Limited, all the services herein will be referred to
            as Business Standard Content Services.
          </p>
          <p>
            <strong>2) Registration Access and Use</strong>
            <br /> We welcome users to register on our digital platforms. We offer the below mentioned registration
            services which may be subject to change in the future. All changes will be appended in the terms and
            conditions page and communicated to existing users by email.
          </p>
          <p>
            Registration services are offered for individual subscribers only. If multiple individuals propose to access
            the same account or for corporate accounts kindly contact or write in to us. Subscription rates will vary
            for multiple same time access.
          </p>
          <p>
            The nature and volume of Business Standard content services you consume on the digital platforms will vary
            according to the type of registration you choose, on the geography you reside or the offer you subscribe to.
          </p>
          <p>
            a) Free Registration
            <br />
            <br /> b) Premium Registration
            <br />
            <br /> c) Special Offers
            <br />
            <br /> d) Combo registrations with partners{' '}
          </p>
          <p>
            The details of the services and access offered for each account have been listed on
            www.business-standard.com/subscription-cart/product
          </p>
          <p>
            We may in exceptional circumstances cease to provide subscription services. We will give you at least 7 days
            notice of this, if possible. If we do so, then we will have no further obligation to you.
          </p>
          <Agreement />
        </Wrapper>
      </Scrollbars>
    </FadeOnMount>
  );
};

export default Terms;
