import styled from 'styled-components';
import {blue} from 'constants/colors';

export const Wrapper = styled.div`
  padding-bottom: 81px;
  color: #000;

  p {
    margin-bottom: 20px;
    font-size: 14px;
    line-height: 145%;
    color: #000;
  }
`;

export const AgreementWrap = styled.div`
  padding: 0 16px;
  position: fixed;
  width: calc(100% - 54px);
  height: 51px;
  display: flex;
  align-items: center;
  bottom: 30px;
  left: 27px;
  z-index: 2;
  background: #efefef;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  box-shadow: 0 6px 10px rgba(0, 0, 0, 0.15);
  border-radius: 15px;
  font-size: 16px;
  line-height: 140%;
  color: #000000;

  span {
    color: ${blue};
  }
`;
