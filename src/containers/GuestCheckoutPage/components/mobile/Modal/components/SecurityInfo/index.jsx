import React from 'react';
import {
  Images,
  Text,
  Wrapper
} from 'containers/GuestCheckoutPage/components/mobile/Modal/components/SecurityInfo/styled';
import img1 from 'containers/GuestCheckoutPage/components/CardSecurityInformation/images/img1.png';
import img2 from 'containers/GuestCheckoutPage/components/CardSecurityInformation/images/img2.png';
import img3 from 'containers/GuestCheckoutPage/components/CardSecurityInformation/images/img3.png';
import img4 from 'containers/GuestCheckoutPage/components/CardSecurityInformation/images/img4.png';
import img5 from 'containers/GuestCheckoutPage/components/CardSecurityInformation/images/img5.png';
import FadeOnMount from 'components/Transitions';

const SecurityInfo = () => {
  return (
    <FadeOnMount>
      <Wrapper>
        <Text>
          <p>testSample does not store your credit card information.</p>

          <p>
            Any payment data that you enter is securely passed on to a payment processor via secure socket layer (SSL)
            that encrypts all sensitive card information.
          </p>

          <p>
            The payment processor then tokenizes your card information (substituting your sensitive information with a
            non-sensitive equivalent).
          </p>

          <p>
            The payment processor manages and stores your card information in a very secure environment with a very high
            security standard and are PCI DSS compliant.
          </p>
        </Text>
        <Images>
          <img src={img1} alt="Thawte" />
          <img src={img2} alt="Master Card" />
          <img src={img3} alt="Visa" />
          <img src={img4} alt="Safe key" />
          <img src={img5} alt="PCI DSS" />
        </Images>
      </Wrapper>
    </FadeOnMount>
  );
};

export default SecurityInfo;
