import styled from 'styled-components';

export const Wrapper = styled.div``;

export const Text = styled.div`
  p {
    margin-bottom: 17px;
    font-size: 14px;
    line-height: 150%;
    color: #000;

    &:last-child {
      margin-bottom: 33px;
    }
  }
`;

export const Images = styled.div`
  padding-top: 18px;
  margin-left: -8px;
  display: flex;
  align-items: center;

  img {
    margin-right: 4px;
    width: 64px;
    height: auto;
    object-fit: contain;
  }
`;
