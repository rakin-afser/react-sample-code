import styled from 'styled-components';
import {blue} from 'constants/colors';

export const SecurityBtn = styled.button`
  margin-bottom: 24px;
  padding: 0;
  display: flex;
  align-items: center;
  font-weight: 500;
  font-size: 14px;
  color: #ed494f;
  transition: ease 0.4s;
  cursor: pointer;

  i {
    margin-left: 10px;

    svg {
      path {
        transition: ease 0.4s;
      }
    }
  }

  &:active {
    color: #000;

    i {
      svg {
        path {
          stroke: #000;
        }
      }
    }
  }
`;

export const Agreement = styled.p`
  padding-top: 14px;
  border-top: 1px solid #cccccc;
  font-size: 14px;
  line-height: 140%;
  color: #000000;
`;

export const AgreementBtn = styled.button`
  padding: 0;
  color: #ed494f;
  transition: ease 0.4s;

  &:active {
    color: ${blue};
  }
`;

export const CheckboxWrapper = styled.div`
  margin-top: 20px;
  padding: 8px 0 9px;

  & .ant-checkbox-wrapper {
    font-size: 14px;
    line-height: 140%;
    color: #000000;
  }

  p {
    margin-top: 7px;
    margin-left: 31px;
    font-size: 12px;
    line-height: 140%;
    color: #666666;
  }
`;
