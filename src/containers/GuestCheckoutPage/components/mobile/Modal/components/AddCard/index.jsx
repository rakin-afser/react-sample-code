import React, {useState} from 'react';
import PropTypes from 'prop-types';
import GoSell from 'containers/GuestCheckoutPage/components/GoSell/GoSell';
import {SecurityBtn, Agreement, AgreementBtn, CheckboxWrapper} from './styled';
import FadeOnMount from 'components/Transitions';
import {OrderItem, StepFooter} from 'containers/GuestCheckoutPage/styled';
import Checkbox from 'containers/GuestCheckoutPage/components/Checkbox';
import {GoSellElements} from '@tap-payments/gosell';
import {useMutation, useQuery} from '@apollo/client';
import {testSample_ORDER_CREATE} from 'mutations';
import {useUser} from 'hooks/reactiveVars';
import Error from 'components/Error';
import Icon from 'components/Icon';
import {useHistory} from 'react-router';
import Btn from 'components/Btn';
import {CART} from 'queries';
import CheckoutFrames from 'components/CheckoutFrames/mobile';
import {Frames} from 'frames-react';
import {usetestSampleCheckoutQuery} from 'hooks/queries';

const AddCard = ({showSecurityInfo, showTerms, selectedAddress, totals, email}) => {
  const [user] = useUser();
  const [err, setErr] = useState([]);
  const {total} = totals || {};
  const [saveCard, setSaveCard] = useState(false);
  const [paymentLoading, setPaymentLoading] = useState(false);
  const {push} = useHistory();

  const {data: dataCart} = useQuery(CART, {fetchPolicy: 'cache-only'});
  const {totalMidCoins, totalShopCoins} = dataCart?.cart || {};

  const {data: dataCheckout} = usetestSampleCheckoutQuery({fetchPolicy: 'cache-only'});
  const {tapPublicKey, tapEnable, checkoutPublicKey, checkoutEnable} = dataCheckout?.testSampleCheckout || {};

  const [createOrder, {error}] = useMutation(testSample_ORDER_CREATE, {
    onCompleted(data) {
      setErr([]);
      setPaymentLoading(false);
      const {transactionUrl, orderCreateId, errors} = data?.testSampleOrderCreate || {};
      if (transactionUrl) {
        window.location.replace(transactionUrl);
        return null;
      }
      if (!transactionUrl) {
        console.log(`data`, data);
      }
      if (orderCreateId && !errors?.length) {
        push(`/checkout/success?cod=${orderCreateId}`);
        return null;
      }
      if (errors?.length) {
        setErr(errors);
      }
      return null;
    },
    onError() {
      setPaymentLoading(false);
    }
  });

  const confirm = (token, method) => {
    setPaymentLoading(true);
    const input = {
      paymentMethod: method,
      paymentToken: token,
      paymentSaveCart: saveCard,
      email: user ? user?.email : email,
      addrAddress1: selectedAddress?.address1,
      addrBuild: selectedAddress?.build,
      addrBlock: selectedAddress?.block,
      addrCity: selectedAddress?.city,
      addrCountry: selectedAddress?.country,
      addrDistrict: selectedAddress?.district,
      addrFirstName: selectedAddress?.firstName,
      addrPhone: selectedAddress?.phone,
      addrRoad: selectedAddress?.road,
      addrApartment: selectedAddress?.apartment,
      addrType: selectedAddress?.type,
      tapRedirectUrl: method === 'tap' ? `${window.location.origin}/checkout/payment-handler` : undefined,
      checkoutFailedUrl: method === 'checkoutdotcom' ? `${window.location.origin}/checkout/failed` : undefined,
      checkoutRedirectUrl:
        method === 'checkoutdotcom' ? `${window.location.origin}/checkout/payment-handler` : undefined
    };

    if (token) {
      createOrder({variables: {input}});
    }
    if (!token) {
      setPaymentLoading(false);
    }
  };

  function renderCardField() {
    const fields = [];
    if (checkoutPublicKey && checkoutEnable) {
      fields.push(
        <CheckoutFrames publicKey={checkoutPublicKey} callback={(response) => confirm(response, 'checkoutdotcom')} />
      );
    }
    if (tapPublicKey && tapEnable) {
      fields.push(<GoSell publicKey={tapPublicKey} callback={(response) => confirm(response, 'tap')} />);
    }
    return fields;
  }

  function renderButtons() {
    const buttons = [];
    if (tapEnable) {
      const onSubmitCreditTap = () => {
        setPaymentLoading(true);
        GoSellElements.submit();
      };
      buttons.push(
        <Btn kind="primary-bold" loading={paymentLoading} onClick={onSubmitCreditTap} full>
          Confirm &amp; Pay
        </Btn>
      );
    }

    if (checkoutEnable) {
      const onSubmitCreditCheckout = () => {
        if (Frames.isCardValid()) {
          setPaymentLoading(true);
          Frames.submitCard();
        }
      };

      buttons.push(
        <Btn kind="primary-bold" loading={paymentLoading} onClick={onSubmitCreditCheckout} full>
          Confirm &amp; Pay
        </Btn>
      );
    }

    return buttons;
  }

  return (
    <FadeOnMount>
      {renderCardField()}
      <CheckboxWrapper>
        <Checkbox onChange={() => setSaveCard(!saveCard)} checked={saveCard}>
          Save your card details for a faster and secure checkout
        </Checkbox>
        <p>CVV/CVC number will not be saved</p>
      </CheckboxWrapper>

      <SecurityBtn onClick={showSecurityInfo}>
        Read Card Security Information
        <Icon type="circleInfo" width={20} height={20} />
      </SecurityBtn>

      <OrderItem total>
        <span>Total To Pay</span>
        <span>
          {/* <small>BD</small> */}
          <strong> {total}</strong>
        </span>
      </OrderItem>

      <OrderItem coins>
        <span>MidCoins </span>
        <span>
          <span>{totalMidCoins}</span>
          <Icon type="coins" />
        </span>
      </OrderItem>

      <OrderItem coins>
        <span>Shop Coins</span>
        <span>
          <span>{totalShopCoins}</span>
          <Icon type="coins" />
        </span>
      </OrderItem>

      <Agreement>
        By placing this order you agree to the Credit Card payment{' '}
        <AgreementBtn onClick={showTerms}>terms and conditions</AgreementBtn>.
      </Agreement>

      {error ? <Error customText="Sorry, your payment didn't pass. Please try again later or contact us." /> : null}
      {err?.length
        ? err?.map((e) => (
            <Error key={e?.code} title={e?.code ? `Code: ${e.code}` : undefined} customText={e?.description} />
          ))
        : null}
      <StepFooter>{renderButtons()}</StepFooter>
    </FadeOnMount>
  );
};

AddCard.propTypes = {
  showSecurityInfo: PropTypes.func.isRequired,
  showTerms: PropTypes.func.isRequired
};

export default AddCard;
