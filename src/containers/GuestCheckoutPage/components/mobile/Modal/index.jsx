import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {CSSTransition} from 'react-transition-group';
import Icons from 'components/Icon';
import AddCard from 'containers/GuestCheckoutPage/components/mobile/Modal/components/AddCard';
import SecurityInfo from 'containers/GuestCheckoutPage/components/mobile/Modal/components/SecurityInfo';
import Terms from 'containers/GuestCheckoutPage/components/mobile/Modal/components/Terms';
import {Wrapper, Inner, Header, Back, Title, Close, Overlay, Container} from './styled';

const Modal = ({publicKey, selectedAddress, onClose, onShow, email, totals}) => {
  const [screen, setScreen] = useState('addCard');

  const activeScreen = (scr) => {
    switch (scr) {
      case 'addCard':
        return (
          <AddCard
            publicKey={publicKey}
            email={email}
            totals={totals}
            selectedAddress={selectedAddress}
            showSecurityInfo={() => setScreen('securityInfo')}
            showTerms={() => {
              setScreen('terms');
            }}
          />
        );
      case 'securityInfo':
        return <SecurityInfo />;
      case 'terms':
        return <Terms />;
      default:
        return (
          <AddCard
            showSecurityInfo={() => setScreen('securityInfo')}
            showTerms={() => {
              setScreen('terms');
            }}
          />
        );
    }
  };

  const onBackBtn = () => {
    if (screen === 'addCard') {
      onClose();
    }

    setScreen('addCard');
  };

  const renderTitle = (scr) => {
    switch (scr) {
      case 'addCard':
        return 'Enter New Card';
      case 'securityInfo':
        return 'Card security information';
      case 'terms':
        return 'Terms and Conditions';
      default:
        return 'Enter New Card';
    }
  };

  return (
    <CSSTransition in={onShow} timeout={300} unmountOnExit>
      <Container>
        <Overlay active={onShow ? 1 : 0} onClick={onClose} />
        <Wrapper>
          <Inner terms={screen === 'terms'}>
            <Header>
              <Back onClick={onBackBtn}>
                <Icons type="arrowBack" color="#000" />
              </Back>
              <Title>{renderTitle(screen)}</Title>
              <Close terms={screen === 'terms'} onClick={onClose}>
                <Icons type="close" color="#000" width={30} height={30} />
              </Close>
            </Header>

            {activeScreen(screen)}
          </Inner>
        </Wrapper>
      </Container>
    </CSSTransition>
  );
};

Modal.propTypes = {
  onClose: PropTypes.func.isRequired,
  onShow: PropTypes.bool.isRequired
};

export default Modal;
