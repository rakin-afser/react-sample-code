import styled from 'styled-components/macro';

export const Inner = styled.div`
  padding: 23px 20px;
  background: #ffffff;
  border-radius: 34px;
  height: 653px;
  width: 100%;
  transition: transform ease 0.3s;
  overflow: auto;

  ${({terms}) =>
    terms &&
    `
       padding: 23px 9px 23px 20px;
    `}
`;

export const Overlay = styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 0;
  opacity: ${({active}) => (active ? 1 : 0)};
  transition: opacity ease 0.3s;
`;

export const Container = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  z-index: 10;

  &.enter {
    ${Inner} {
      transform: translateY(100%);
    }
  }

  &.enter-active {
    ${Inner} {
      transform: translateY(0);
    }
  }

  &.exit {
    ${Inner} {
      transform: translateY(0);
    }
  }

  &.exit-active {
    ${Inner} {
      transform: translateY(100%);
    }
  }
`;

export const Wrapper = styled.div`
  overflow: auto;
  max-height: 100vh;
  position: absolute;
  left: 10px;
  right: 10px;
  bottom: 0;
  display: flex;
  flex-direction: column;
  z-index: 1;
  padding: 10px 0;
`;

export const Header = styled.div`
  margin-bottom: 22px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Title = styled.div`
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  text-align: center;
  color: #000;
`;

export const Close = styled.button`
  position: relative;
  right: ${({terms}) => (terms ? '-2px' : '-13px')};
  top: 2px;
`;

export const Back = styled.button`
  position: relative;
  left: -13px;
  top: 3px;
`;
