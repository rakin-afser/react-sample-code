import React, {Fragment, useState, useRef} from 'react';
import {string, number} from 'prop-types';

import Input from 'components/Input';
import Button from 'components/Button';

import {
  Wrapper,
  ProductThumb,
  ProductInfo,
  ProductPrice,
  NewPrice,
  OldPrice,
  Sale,
  ProductTitle,
  ProductAttributes,
  ProductDivider,
  ProductCounters,
  Counter,
  CounterField,
  CounterButton,
  AttrLabel,
  AttrValue,
  ProductCoupon,
  ProductCouponField,
  ProductCouponAction,
  AttrColor
} from './styled';
import {getDiscountPercent, renderCoinsByVariation} from 'util/heplers';
import {useMutation} from '@apollo/client';
import {UpdateItemQuantities} from 'mutations';

const Product = (props) => {
  const {name, quantity, midCoins, shopCoins, variation, keyCart, variationId} = props;
  const {image, salePrice, regularPrice} = variation?.node || props || {};
  const {sourceUrl, altText} = image || {};
  const {attributes} = variation || {};
  const variationToPass = {
    databaseId: variationId
  };

  const [updateQuantity] = useMutation(UpdateItemQuantities);

  const el = useRef(null);

  const [coupon, setCoupon] = useState({
    value: '',
    loading: false,
    apply: false
  });

  function onQuantity(value) {
    updateQuantity({
      variables: {input: {items: [{key: keyCart, quantity: value}]}},
      update(cache, {data}) {
        cache.modify({
          fields: {
            cart: () => data?.updateItemQuantities?.cart
          }
        });
      }
    });
  }

  return (
    <>
      <Wrapper ref={el}>
        <ProductThumb src={sourceUrl} alt={altText} />
        <ProductInfo>
          <ProductPrice>
            <NewPrice>
              {/* <small>BD</small> */}
              {salePrice || regularPrice}
            </NewPrice>
            {salePrice ? (
              <OldPrice>
                {/* <small>BD</small> */}
                <span>{regularPrice}</span>
              </OldPrice>
            ) : null}
            {salePrice ? <Sale>-{getDiscountPercent(regularPrice, salePrice)}%</Sale> : null}
          </ProductPrice>

          <ProductTitle>{name}</ProductTitle>

          <ProductAttributes>
            {attributes?.map(({id, value, label}) => (
              <Fragment key={id}>
                <AttrLabel>{label}</AttrLabel>
                {label === 'Color' ? <AttrColor color={value} /> : <AttrValue>{value}</AttrValue>}
                <ProductDivider />
              </Fragment>
            ))}
            <Counter>
              <CounterButton onClick={() => onQuantity(quantity - 1)}>–</CounterButton>
              <CounterField value={quantity} type="tel" />
              <CounterButton onClick={() => onQuantity(quantity + 1)}>+</CounterButton>
            </Counter>
          </ProductAttributes>
        </ProductInfo>

        <ProductCounters>
          Mid Coins: <span>+{midCoins || 0}</span>
          Shop Coins: <span>+{shopCoins || 0}</span>
        </ProductCounters>

        <ProductCoupon>
          <ProductCouponField>
            <Input
              value={coupon.value}
              disabled
              placeholder="Please enter Influencer Code"
              onChange={(e) => {
                setCoupon({...coupon, value: e.target.value});
              }}
              loading={coupon.loading}
              userNameAvailable={coupon.apply}
            />
          </ProductCouponField>
          <ProductCouponAction>
            {coupon.apply ? (
              <span>Discount -10%</span>
            ) : (
              <Button
                disabled
                onClick={() => {
                  setCoupon({...coupon, loading: true});

                  setTimeout(() => {
                    setCoupon({...coupon, loading: false, apply: true});
                  }, 400);
                }}
                disabled={!(coupon.value && coupon.value.length)}
              >
                Apply
              </Button>
            )}
          </ProductCouponAction>
        </ProductCoupon>
      </Wrapper>
    </>
  );
};

export default Product;
