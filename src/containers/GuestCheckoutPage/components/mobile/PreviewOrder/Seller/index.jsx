import React, {useState} from 'react';

import productSrc1 from 'images/products/card1.jpg';
import productSrc2 from 'images/products/card2.jpg';

import Icon from 'components/Icon';
import Divider from 'components/Divider';
import Product from '../Product';

import {Wrapper, ProductWrapper, SellerContainer, SelectDeliveryOption, OrderWrapper} from './styled';

import {OrderItem} from '../../../../styled';
import {getCountryFromCode} from 'util/heplers';

const Seller = (props) => {
  const {store, items, shipping: shipMethods, totals, setMethod, subCartIndex, cartData} = props;
  const {id, name, gravatar, address} = store;
  const {itemsTotal, shipping, shopCoins, total} = totals;
  const [coupon, setCoupon] = useState({
    value: '',
    loading: false,
    apply: false
  });

  const onSetMethod = (setShippingMetodId, setShippingMetodSubcart) => {
    setMethod({
      variables: {
        input: {
          setShippingMetodId,
          setShippingMetodSubcart
        }
      },
      optimisticResponse: {
        setSubcartShippingMethod: {
          __typename: 'SetSubcartShippingMethodPayload',
          cart: cartData?.cart,
          calculateSuccess: true
        }
      },
      update(cache, {data}) {
        cache.modify({
          fields: {
            cart(existing) {
              return {
                ...existing,
                subcarts: {
                  ...data?.setSubcartShippingMethod?.cart?.subcarts,
                  subcarts: data?.setSubcartShippingMethod?.cart?.subcarts?.subcarts?.map((s, i) => ({
                    ...s,
                    shipping: {
                      ...s?.shipping,
                      chosenMethodId: i === setShippingMetodSubcart ? setShippingMetodId : s?.shipping?.chosenMethodId
                    }
                  }))
                },
                total: data?.setSubcartShippingMethod?.cart?.total
              };
            }
          }
        });
      }
    });
  };

  return (
    <Wrapper>
      <SellerContainer>
        <img src={gravatar} alt={name} />
        <span>
          <strong>{name},</strong> {getCountryFromCode(address?.country)}
        </span>
      </SellerContainer>

      <ProductWrapper>
        {items?.map((prod) => (
          <Product {...prod} keyCart={prod?.key} />
        ))}
      </ProductWrapper>

      <SelectDeliveryOption>
        <span className="title">
          Select Delivery Option <span>*</span>
        </span>
        <div className="radio-wrapper">
          {shipMethods.availableMethods.map((method) => (
            <label className="radio">
              <input
                name={`shipping_${id}_${method.id}`}
                type="radio"
                checked={shipMethods?.chosenMethodId === method?.id}
                onChange={() => onSetMethod(method?.id, subCartIndex)}
              />
              <span className="icon"></span>
              <span className="text">
                <span>
                  {!method.cost ? <strong className="green">Free</strong> : null} {method?.label}
                </span>
                <span className="right">
                  <strong>{method.cost}</strong>
                </span>
              </span>
            </label>
          ))}
        </div>
      </SelectDeliveryOption>

      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />

      <OrderWrapper>
        <OrderItem>
          <span>Items Total</span>
          <span>
            {/* <small>BD</small> */}
            <strong> {itemsTotal}</strong>
          </span>
        </OrderItem>

        <OrderItem>
          <span>Estimated Shipping</span>
          <span>
            {/* <small>BD</small> */}
            <strong> {shipping}</strong>
          </span>
        </OrderItem>

        {/* <OrderItem>
          <span>Influencer Discount</span>
          <span className="red">
            <small>-BD</small>
            <strong> 36.000</strong>
          </span>
        </OrderItem> */}

        <OrderItem total>
          <span>{name} Total</span>
          <span>
            {/* <small>BD</small> */}
            <strong> {total}</strong>
          </span>
        </OrderItem>
      </OrderWrapper>
    </Wrapper>
  );
};

Seller.defaultProps = {
  seller: {},
  currency: 'bd',
  idx: 1
};

Seller.propTypes = {};

export default Seller;
