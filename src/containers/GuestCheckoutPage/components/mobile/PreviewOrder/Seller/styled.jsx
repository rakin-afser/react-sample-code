import styled from 'styled-components';

export const Wrapper = styled.div`
  &:not(:first-child) {
    border-top: 8px solid rgb(250, 250, 250);
  }
`;

export const PreviewOrderWrapper = styled.div`
  margin-top: -40px;
`;

export const ProductWrapper = styled.div`
  padding: 0 16px;

  .shipping {
    background: #f7f7f7;
    box-shadow: 0px 5px 11px rgba(0, 0, 0, 0.08);
    padding: 6px 23px;
    display: flex;
    align-items: center;
    justify-content: space-between;

    .ant-select {
      margin-right: auto;
      position: relative;
      top: 2px;
    }

    .ant-select-selection__placeholder {
      text-align: right;
    }

    .ant-form-item {
      margin: 0;
    }
    .shipping-title {
      h3 {
        margin: 0;
        font-weight: bold;
        font-size: 14px;
        color: #000000;
      }
      span {
        color: #ed484f;
      }
    }
    .ant-select-selection {
      border: none;
      background: inherit;
      min-width: 300px;
      box-shadow: none;
      .ant-select-selection-selected-value {
        color: #000;
      }
    }
  }
  .shipping .price {
    margin-left: auto;
  }
`;

export const SellerContainer = styled.div`
  padding: 12px 0;
  margin: 0 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 300;
  font-size: 12px;
  line-height: 15px;
  color: #000000;
  margin-bottom: 12px;
  border-bottom: 1px solid #e4e4e4;

  strong {
    font-weight: bold;
    font-size: 16px;
    line-height: 20px;
  }

  img {
    width: 34px;
    height: 34px;
    border-radius: 50%;
    margin-right: 9px;
  }
`;

export const OrderWrapper = styled.div`
  padding: 16px 16px 0;
`;

export const SelectDeliveryOption = styled.div`
  display: flex;
  flex-wrap: wrap;

  .title {
    display: flex;
    align-items: center;
    width: 100%;
    padding: 10px 32px;
    min-height: 40px;
    background: #fafafa;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 1.4;
    color: #000000;

    span {
      margin-left: 4px;
      color: red;
    }
  }

  .radio-wrapper {
    width: 100%;
    padding: 8px 0;
  }

  .radio {
    display: flex;
    align-items: center;
    position: relative;
    width: 100%;
    padding: 8px 32px;

    input {
      display: none;

      &:checked ~ {
        .icon {
          border-color: #4190f7;

          &:before {
            opacity: 1;
          }
        }
      }
    }

    .icon {
      width: 20px;
      height: 20px;
      border-radius: 50%;
      border: 1.5px solid #d9d9d9;
      margin-right: 8px;
      transition: all 0.3s ease;
      position: relative;

      &:before {
        content: '';
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        transition: all 0.3s ease;
        width: 10px;
        height: 10px;
        background: #4190f7;
        border-radius: 50%;
        opacity: 0;
        pointer-events: none;
      }
    }

    .text {
      width: calc(100% - 28px);
      display: flex;
      align-items: center;
      color: #000;

      .green {
        color: #26a95e;
      }
    }

    .right {
      margin-left: auto;
      color: #000;
    }
  }
`;
