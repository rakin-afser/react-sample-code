import React from 'react';
import {string, func} from 'prop-types';

import Icon from 'components/Icon';
import Divider from 'components/Divider';

import Seller from './Seller';

import {Wrapper, PreviewOrderWrapper, Continue} from './styled';

import {Step, StepHeader} from '../../../styled';
import {useMutation} from '@apollo/client';
import {SET_SUBCART_SHIPPING_METHOD} from 'mutations';
import {formatCartsData} from 'util/heplers';

const PreviewOrder = ({setStep, cartData}) => {
  const [setMethod, {loading: loadingMethod}] = useMutation(SET_SUBCART_SHIPPING_METHOD);

  const cartsData = formatCartsData(cartData);

  function renderSellers() {
    return cartsData?.map((seller, i) => (
      <Seller subCartIndex={i} key={seller?.store?.id} {...seller} setMethod={setMethod} cartData={cartData} />
    ));
  }

  return (
    <Wrapper>
      <Step mini>
        <StepHeader mini onClick={() => setStep('details')}>
          1. Shipping Address
          <Icon className="pencil" type="pencil" color="#000" />
        </StepHeader>
      </Step>

      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />

      <Step>
        <StepHeader opened>
          2. Preview Order
          <Icon type="arrow" className="arrow" color="#000" />
        </StepHeader>
      </Step>

      <PreviewOrderWrapper>{renderSellers()}</PreviewOrderWrapper>

      <Continue kind="continue-big" full loading={loadingMethod} onClick={() => setStep('payment')}>
        Proceed to Payment
      </Continue>
      {/* <Button
        type="submit"
        styleType="blackBorder"
        disabled={loadingMethod}
        onClick={() => setStep('payment')}
        style={{
          margin: '24px auto 0',
          minWidth: 295
        }}
      >
        <span>Proceed to Payment</span>
        <Icon
          type="arrow"
          color="#000"
          width={7}
          height={12}
          style={{
            margin: '1px 0 0 10px',
            position: 'relative',
            right: -20
          }}
        />
      </Button> */}

      <Divider
        style={{
          height: 24,
          background: '#fff'
        }}
      />

      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />

      <Step mini>
        <StepHeader mini disabled>
          3. Payment
          <Icon type="arrow" color="#CCCCCC" />
        </StepHeader>
      </Step>
    </Wrapper>
  );
};

PreviewOrder.defaultProps = {};

PreviewOrder.propTypes = {
  step: string.isRequired,
  setStep: func.isRequired
};

export default PreviewOrder;
