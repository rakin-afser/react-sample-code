import Btn from 'components/Btn';
import styled from 'styled-components';

export const Wrapper = styled.div``;

export const PreviewOrderWrapper = styled.div`
  margin-top: -40px;
`;

export const ProductWrapper = styled.div`
  padding: 0 16px;

  .shipping {
    background: #f7f7f7;
    box-shadow: 0px 5px 11px rgba(0, 0, 0, 0.08);
    padding: 6px 23px;
    display: flex;
    align-items: center;
    justify-content: space-between;

    .ant-select {
      margin-right: auto;
      position: relative;
      top: 2px;
    }

    .ant-select-selection__placeholder {
      text-align: right;
    }

    .ant-form-item {
      margin: 0;
    }
    .shipping-title {
      h3 {
        margin: 0;
        font-weight: bold;
        font-size: 14px;
        color: #000000;
      }
      span {
        color: #ed484f;
      }
    }
    .ant-select-selection {
      border: none;
      background: inherit;
      min-width: 300px;
      box-shadow: none;
      .ant-select-selection-selected-value {
        color: #000;
      }
    }
  }
  .shipping .price {
    margin-left: auto;
  }
`;

export const Seller = styled.div`
  padding: 12px 0;
  margin: 0 16px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 300;
  font-size: 12px;
  line-height: 15px;
  color: #000000;
  margin-bottom: 12px;
  border-bottom: 1px solid #e4e4e4;

  strong {
    font-weight: bold;
    font-size: 16px;
    line-height: 20px;
  }

  img {
    width: 34px;
    height: 34px;
    border-radius: 50%;
    margin-right: 9px;
  }
`;

export const Continue = styled(Btn)`
  margin: 24px auto 0;
  display: flex;
`;
