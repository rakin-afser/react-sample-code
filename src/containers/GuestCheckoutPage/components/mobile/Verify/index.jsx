import React, {useState} from 'react';

import Icon from 'components/Icon';
import OTP from 'components/OTP';
import FormMessage from 'components/FormMessage';

import {Wrapper, Title, SubTitle, PleaseEnterOTP, ResendOTP, ResendOTPWrapper} from './styled';
import {useLocation} from 'react-router';
import {useMutation} from '@apollo/client';
import {SendOTP, ValidateOTP} from 'mutations';
import Btn from 'components/Btn';

const Verify = ({setStep, setEmail}) => {
  const [code, setCode] = useState();
  const [invalidCode, setInvalidCode] = useState(null);
  const [message, setMessage] = useState(null);
  const {state} = useLocation();

  const [validateOtp, {loading}] = useMutation(ValidateOTP, {
    onCompleted(data) {
      if (data?.validateOTP?.code === '200') {
        localStorage.setItem('gEmail', state?.email);
        setStep('details');
        setEmail(state?.email);
      } else {
        setCode('');
        setInvalidCode('Invalid Code');
      }
    }
  });
  const [sendOTP] = useMutation(SendOTP, {
    onCompleted() {
      setMessage({
        type: 'success',
        text: 'A new OTP code has been sent to your email.'
      });
    }
  });

  const onSubmit = () => {
    validateOtp({
      variables: {
        input: {
          otp: parseInt(code),
          email: state?.email
        }
      }
    });
  };

  const resendOTP = () => {
    sendOTP({variables: {input: {email: state?.email}}});
  };

  return (
    <Wrapper>
      <Title style={{marginBottom: 12}}>
        Verify Email
        <Icon type="close" onClick={() => setStep('start')} color="#000" />
      </Title>

      <FormMessage message={message} />

      <SubTitle>
        We have sent an email OTP code to
        <br />
        <strong>{state?.email}</strong>
      </SubTitle>

      <PleaseEnterOTP>Please Enter OTP</PleaseEnterOTP>

      <OTP value={code} onChange={setCode} error={invalidCode} />
      <Btn min={168} kind="primary-bold" onClick={onSubmit} disabled={code?.length !== 6} loading={loading}>
        Confirm
      </Btn>

      <ResendOTPWrapper>
        <ResendOTP onClick={resendOTP}>Resend OTP</ResendOTP>
      </ResendOTPWrapper>
    </Wrapper>
  );
};

export default Verify;
