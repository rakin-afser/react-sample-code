import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 35px 16px;
  text-align: center;
`;

export const Title = styled.h3`
  font-weight: 500;
  font-size: 22px;
  line-height: 27px;
  text-align: center;
  color: #000000;
  margin: 0 0 24px;
  position: relative;

  i {
    position: absolute;
    top: 50%;
    right: -6px;
    transform: translate(0, -50%);
  }
`;

export const SubTitle = styled.p`
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  text-align: center;
  margin: 0 0 40px;
`;

export const PleaseEnterOTP = styled.p`
  font-weight: 500;
  font-size: 18px;
  line-height: 22px;
  color: #464646;
  text-align: center;
  margin: 0 0 40px;
`;

export const ResendOTPWrapper = styled.div`
  text-align: center;
`;

export const ResendOTP = styled.span`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  margin: 32px auto 0;
  color: #4a90e2;
  display: inline-block;
  cursor: pointer;
`;
