import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: center;
  padding: 16px 16px 50px;
`;

export const FailMessage = styled.p`
  background: #f15960;
  border-radius: 4px;
  padding: 12px 35px;
  color: #fff;
  max-width: 600px;
  margin: 31px auto 63px;
`;

export const InfoMessage = styled.p`
  font-family: ${secondaryFont};
  text-align: center;
  max-width: 323px;
  color: #000000;
  margin: 0 auto 32px;
`;
