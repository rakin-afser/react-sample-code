import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Wrapper = styled.div`
  background: #FFFFFF;
  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.1);
  display: flex;
  align-items: center;
  justify-content: center;
  height: 46px;
`;

export const CustomLink = styled(Link)`
  display: inline-flex;
  width: 82px;
  height: 22px;
  
  svg {
    width: 100%;
    height: 100%;
    max-width: 82px;
    max-height: 22px;
  }
`;