import React from 'react';

import Logo from 'assets/LogoIcon'

import {
  Wrapper,
  CustomLink
} from './styled';

const Header = ( ) => {
  return <Wrapper>
    <CustomLink to="/">
      <Logo/>
    </CustomLink>
  </Wrapper>
};

export default Header;