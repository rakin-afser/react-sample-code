import React, {useState} from 'react';
import {string, func} from 'prop-types';

import Icon from 'components/Icon';
import Input from 'components/Input';
import Button from 'components/Button';
import Map from '../../Map';

import {
  Wrapper,
  Step,
  MapContainer,
  MapSearch,
  AddressContainer,
  AddressLabel,
  AddressValue,
  AddressControls
} from './styled';

const SelectLocation = ({step, setStep, setShowLocation}) => {
  const [center, setCenter] = useState({lat: 26.0667, lng: 50.5577});
  const [position, setPosition] = useState({lat: 26.0667, lng: 50.5577});

  return (
    <Wrapper>
      <Step>
        <MapContainer>
          <Map center={center} position={position} setPosition={setPosition} setCenter={setCenter} />
          {/* <MapSearch>
            <Icon type="search" color="#7a7a7a" className="icon" />
            <Input
              key="searchAddress"
              type="text"
              name="searchAddress"
              id="searchAddress"
              label=""
              placeholder="Enter your address"
            />
          </MapSearch> */}
        </MapContainer>

        <AddressContainer>
          <AddressLabel>
            <Icon type="marker" color="#666666" className="icon" width={16} height={20} />
            <span>Your address on Map</span>
          </AddressLabel>
          <AddressValue>Block 321, Rd. 318, Al Qudaibiya, Manama, Bahrain</AddressValue>

          <AddressControls>
            <Button className="back" onClick={() => setShowLocation(false)}>
              Back
            </Button>
            <Button className="next" onClick={() => setShowLocation(false)}>
              <span>Next</span>
              <Icon type="arrow" color="#000" className="icon" width={7} height={12} />
            </Button>
          </AddressControls>
        </AddressContainer>
      </Step>
    </Wrapper>
  );
};

SelectLocation.defaultProps = {};

SelectLocation.propTypes = {
  step: string.isRequired,
  setStep: func.isRequired
};

export default SelectLocation;
