import styled from 'styled-components';

export const Wrapper = styled.div``;

export const Step = styled.div`
  padding: 0 0 24px;

  ${({mini}) =>
    mini &&
    `
    padding: 0 0;
  `}
`;

export const MapContainer = styled.div`
  position: relative;
`;

export const MapSearch = styled.div`
  height: auto !important;
  display: flex;
  padding: 24px 16px;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;

  > *:not(.icon) {
    width: 100%;
    margin: 0;
    position: relative;
  }

  .icon {
    position: absolute;
    top: 50%;
    left: 33px;
    z-index: 10;
    transform: translate(0, -50%);
  }

  input {
    width: 100%;
    padding-left: 49px;
    border: 1px solid #999999;
  }
`;

export const AddressContainer = styled.div`
  padding: 24px 16px 0;
`;

export const AddressLabel = styled.label`
  display: flex;
  align-items: center;

  span {
    padding-left: 8px;
    font-size: 14px;
    line-height: 1.4;
    color: #666666;
  }
`;

export const AddressValue = styled.label`
  display: block;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 20px;
  color: #000000;
  margin: 8px 0 24px;
  padding-left: 12px;
  padding-right: 60px;
`;

export const AddressControls = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  .back {
    background: transparent;
    border: 1px solid transparent;
    font-weight: 500;
    font-size: 14px;
    line-height: 140%;
    color: #666666;
    width: calc(100% - 3.5px);
  }

  .next {
    width: calc(100% - 3.5px);
    background: transparent;
    border: 1px solid #000000;
    border-radius: 24px;

    font-weight: 500;
    font-size: 16px;
    line-height: 140%;
    display: flex;
    align-items: center;
    text-align: center;
    color: #000000;
    position: relative;
    padding-right: 10%;

    .icon {
      position: absolute;
      right: 15%;
      top: calc(50% + 1px);
      transform: translate(0, -50%);
    }
  }
`;
