import styled from 'styled-components/macro';

export const Wrapper = styled.div``;

export const CouponContainer = styled.div`
  padding: 0 16px 24px;
  margin-top: -24px;

  input {
    width: 100%;
    height: 24px;
    min-height: 24px;
    border: 0;
    border-bottom: 1px solid #c3c3c3;
    padding: 4px 0;
    font-size: 12px;
    color: #000000;
  }

  .checkbox {
    margin-bottom: 0;

    .text {
      padding-left: 12px !important;
      line-height: 1.3;
    }

    .check {
      position: relative;
      top: -2px;
    }
  }
`;

export const PaymentContainer = styled.div`
  padding: 16px;

  h3 {
    margin-top: 7px;
    margin-bottom: 15px;
    font-weight: bold;
    font-size: 16px;
    color: #000000;
  }

  .info {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 17px;
    color: #999999;
    margin: 0;
  }

  & .ant-radio-group {
    display: flex;
    flex-direction: column;
  }

  & .ant-radio-wrapper {
    display: flex;
    align-items: center;
    margin: 8px 0;
    color: #000;

    span + span {
      width: 100%;
    }
  }

  & .ant-radio {
    position: relative;
    top: -2px;
  }

  & .ant-radio-inner {
    border-color: #000 !important;
    box-shadow: none !important;

    &::after {
      background-color: #000;
    }
  }
`;
