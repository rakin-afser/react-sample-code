import React from 'react';
import {Radio} from 'antd';
import {useQuery} from '@apollo/client';
import {testSample_GET_SAVED_CARDS} from 'queries';
import {images1, images2} from '../../imagesData';
import {useUser} from 'hooks/reactiveVars';
import {RadioInner, ImagesWrap, Img, Txt, Exp} from './styled';

const PaymentMethods = ({paymentMethod, setPaymentMethod, setModal}) => {
  const [user] = useUser();
  const {data: dataCards} = useQuery(testSample_GET_SAVED_CARDS, {
    variables: {id: user?.databaseId},
    onCompleted(data) {
      if (data?.user?.savedCards?.length) {
        setPaymentMethod(data?.user?.savedCards[0]?.id);
      }
    }
  });

  const renderCards = () => {
    if (dataCards?.user?.savedCards) {
      return dataCards?.user?.savedCards?.map((card) => {
        let brand = null;

        const expMonth = () => {
          if (card.exp_month) {
            if (card.exp_month < 10) {
              return `0${card.exp_month}`;
            }

            return card.exp_month;
          }

          return '--';
        };

        switch (card.brand) {
          case 'MASTERCARD':
            brand = images1[0].url;
            break;
          case 'VISA':
            brand = images1[1].url;
            break;
          case 'AMERICANEXPRESS':
            brand = images1[2].url;
            break;
          default:
            break;
        }

        return (
          <Radio key={card.id} id={`payment${card.id}`} value={card.id}>
            <RadioInner>
              {brand ? (
                <ImagesWrap style={{marginRight: 7}}>
                  <Img>
                    <img src={brand} alt="payment logo" />
                  </Img>
                </ImagesWrap>
              ) : null}
              <Txt>xxxx-{card.last_four}</Txt>
              <Exp>
                Exp {expMonth()}/{card.exp_year ? card.exp_year : '--'}
              </Exp>
            </RadioInner>
          </Radio>
        );
      });
    }

    return null;
  };

  function renderTapCheckboxes() {
    return (
      <>
        {renderCards()}
        <Radio id="paymentCreditCard" value="cred">
          <RadioInner>
            <Txt>+ Add New Credit Card</Txt>
            <ImagesWrap>
              {images1.map((el) => (
                <Img key={el.id}>
                  <img src={el.url} alt="payment logo" />
                </Img>
              ))}
            </ImagesWrap>
          </RadioInner>
        </Radio>

        <Radio id="paymentDebitCard" value="deb">
          <RadioInner>
            <Txt>Debit Card</Txt>
            <ImagesWrap>
              {images2.map((el) => (
                <Img key={el.id}>
                  <img src={el.url} alt="payment logo" />
                </Img>
              ))}
            </ImagesWrap>
          </RadioInner>
        </Radio>
      </>
    );
  }

  function onChange(e) {
    const {value} = e.target;
    setPaymentMethod(value);
    if (value === 'cred') setModal(true);
  }

  return (
    <Radio.Group onChange={onChange} value={paymentMethod}>
      {renderTapCheckboxes()}
      <Radio id="paymentCash" value="cod">
        <RadioInner>
          <Txt>Cash on delivery</Txt>
        </RadioInner>
      </Radio>
    </Radio.Group>
  );
};

export default PaymentMethods;
