import styled from 'styled-components';

export const RadioInner = styled.span`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Txt = styled.span`
  margin-right: ${({second}) => (second ? '24px' : '20px')};
`;

export const ImagesWrap = styled.span`
  width: auto !important;
  display: flex;
  align-items: center;
`;

export const Img = styled.div`
  margin-right: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 17px;
  border: 1px solid #bdbdbd;
  border-radius: 2px;
  overflow: hidden;

  &:last-child {
    margin-right: 0;
  }

  img {
    width: 94%;
    height: 94%;
    object-fit: contain;
  }
`;

export const Exp = styled.div`
  margin-left: auto;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #666666;
  min-width: 82px;
`;
