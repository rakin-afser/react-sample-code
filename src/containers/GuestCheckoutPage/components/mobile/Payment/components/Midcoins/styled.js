import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';
import Icon from 'components/Icon';

export const MidcoinsCheckWrapper = styled.div`
  margin-top: 18px;
`;

export const RewardsAdditionalTitle = styled.div`
  font-family: SF Pro Display;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  color: #545454;
  margin-top: 8px;
  flex-grow: 1;

  i {
    position: relative;
    left: 3px;
    top: 1px;
  }
`;

export const RewardsBlock = styled.div`
  padding: 16px 0 0 0;
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;

  & .ant-form-item {
    width: 200px;
    margin: 0 12px 0 0;
  }

  & .ant-form-item-label {
  }

  button {
    align-self: flex-start;
    margin-left: auto;
  }

  & h3 {
    font-size: 16px !important;
  }
`;

export const AppliedCoins = styled.div`
  color: #545454;
  line-height: 1;
  font-family: ${secondaryFont};

  i {
    vertical-align: middle;
  }
`;

export const CoinTitle = styled.span`
  color: #398287;
  margin-right: 5px;
`;

export const Cancel = styled(Icon)`
  cursor: pointer;
`;
