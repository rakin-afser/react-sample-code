import React, {useEffect, useState} from 'react';
import Icon from 'components/Icon';
import Input from 'components/Input';
import {MidcoinsCheckWrapper, RewardsAdditionalTitle, RewardsBlock, AppliedCoins, CoinTitle, Cancel} from './styled';
import Btn from 'components/Btn';
import {useUser} from 'hooks/reactiveVars';
import {useAppliedMidcoinsQuery, useGetCoinsLazyQuery} from 'hooks/queries';
import {useApplyMidcoinsDiscountMutation, useRemoveMidcoinsDiscountMutation} from 'hooks/mutations';
import {userId} from 'util/heplers';
import {primaryColor} from 'constants/colors';

const Midcoins = () => {
  const [qty, setQty] = useState();
  const [visible, setVisible] = useState(false);
  const [user] = useUser();
  const {data: dataApplied} = useAppliedMidcoinsQuery({fetchPolicy: 'cache-only'});
  const {appliedMidCoins} = dataApplied?.cart || {};
  const [get, {data}] = useGetCoinsLazyQuery();
  const {databaseId} = user || {};
  const {currency, coinsConversionRate, availableMidcoins} = data?.user || {};
  const coinsConvertedValue = qty ? (qty * coinsConversionRate)?.toFixed(2) : 0;
  const [apply, {loading}] = useApplyMidcoinsDiscountMutation({
    qty,
    variables: {input: {qty: parseFloat(qty || 0)}},
    onCompleted() {
      setQty('');
    }
  });
  const [remove, {loading: loadingRemove}] = useRemoveMidcoinsDiscountMutation({qty: appliedMidCoins?.qty});

  useEffect(() => {
    if (databaseId || userId) {
      get({variables: {id: databaseId || userId}});
    }
  }, [databaseId, get]);

  const onChangeMidcoins = ({target: {value}}) => {
    if (/^([0-9]+|\d+)$/i.test(value) || /\./i.test(value) || !value) {
      const currentValue = availableMidcoins > value ? value : availableMidcoins;
      setQty(currentValue);
    }
  };

  return (
    <>
      <MidcoinsCheckWrapper>
        <Input id="useMidCoins" type="checkbox" onChange={() => setVisible(!visible)} checked={visible}>
          Use Mid-Coins
        </Input>
      </MidcoinsCheckWrapper>

      <RewardsAdditionalTitle>
        Total available <span style={{color: '#398287'}}>MidCoins = {availableMidcoins || 0}</span>
        <Icon type="coins" color="#398287" />
      </RewardsAdditionalTitle>
      {appliedMidCoins ? (
        <AppliedCoins>
          Applied{' '}
          <CoinTitle>
            MidCoins = {appliedMidCoins?.qty} <Icon type="coins" color="#398287" />
          </CoinTitle>
          {loadingRemove ? (
            <Icon type="loader" fill={primaryColor} width={30} height={6} />
          ) : (
            <Cancel type="cancel" onClick={() => remove()} color="#000" width={15} height={15} />
          )}
        </AppliedCoins>
      ) : null}

      {visible ? (
        <RewardsBlock>
          <div className="field" style={{marginRight: 16}}>
            <Input value={qty} placeholder="0" onChange={onChangeMidcoins} />
          </div>
          <div className="field">
            <Input readOnly value={`= ${currency || ''} ${coinsConvertedValue}`} />
          </div>
          <Btn kind="bare-sm" disabled={!qty} onClick={() => apply()} loading={loading}>
            Apply
          </Btn>
        </RewardsBlock>
      ) : null}
    </>
  );
};

export default Midcoins;
