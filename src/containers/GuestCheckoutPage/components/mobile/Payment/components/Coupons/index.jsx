import React, {useState} from 'react';
import Input from 'components/Input';
import {useApplyCouponMutation, useRemoveCouponsMutation} from 'hooks/mutations';
import Btn from 'components/Btn';
import {Wrapper, List, Item, Coupon, Cancel} from './styled';
import {useAppliedCouponsQuery} from 'hooks/queries';
import Icon from 'components/Icon';
import {primaryColor} from 'constants/colors';

const Coupons = () => {
  const [coupon, setCoupon] = useState('');
  const [visible, setVisible] = useState(false);
  const [apply, {loading}] = useApplyCouponMutation({
    variables: {input: {code: coupon}},
    onCompleted() {
      setCoupon('');
    }
  });

  const [remove, {loading: loadingRemove}] = useRemoveCouponsMutation();
  const {data: dataApplied} = useAppliedCouponsQuery({fetchPolicy: 'cache-only'});
  const {appliedCoupons} = dataApplied?.cart || {};

  return (
    <Wrapper>
      <Input id="checkUseCoupon" type="checkbox" onChange={() => setVisible(!visible)} checked={visible}>
        Use Coupon
      </Input>
      {appliedCoupons?.length ? (
        <List>
          {appliedCoupons?.map((c) => (
            <Item key={c.code}>
              <Coupon>{c.code}</Coupon>
              {loadingRemove ? (
                <Icon type="loader" fill={primaryColor} width={30} height={6} />
              ) : (
                <Cancel
                  type="cancel"
                  onClick={() => remove({variables: {input: {codes: [c.code]}}})}
                  color="#000"
                  width={15}
                  height={15}
                />
              )}
            </Item>
          ))}
        </List>
      ) : null}

      {visible ? (
        <div className="coupon">
          <div className="field">
            <Input value={coupon} placeholder="Please enter Coupon Code" onChange={(e) => setCoupon(e.target.value)} />
          </div>
          <div className="action">
            <Btn kind="bare-sm" onClick={() => apply()} loading={loading} disabled={!coupon}>
              Apply
            </Btn>
          </div>
        </div>
      ) : null}
    </Wrapper>
  );
};

export default Coupons;
