import styled from 'styled-components';
import Icon from 'components/Icon';

export const Wrapper = styled.div`
  .coupon {
    width: 100%;
    display: flex;
    align-items: center;
    padding-top: 20px;
    min-height: 38px;
  }

  .field {
    position: relative;
    width: 65%;

    > * {
      margin-bottom: 0;
    }

    .icon--loader {
      top: 2px !important;
      right: 1px !important;
    }

    .icon--double-check {
      top: 7px;
      right: 3px;
    }
  }

  .action {
    position: relative;
    width: 40%;
    display: flex;
    align-items: center;
    justify-content: flex-end;
  }
`;

export const Cancel = styled(Icon)`
  vertical-align: bottom;
  cursor: pointer;
`;

export const List = styled.div`
  margin-top: 10px;
`;

export const Item = styled.div`
  line-height: 1;
`;

export const Coupon = styled.span`
  margin-right: 5px;
`;
