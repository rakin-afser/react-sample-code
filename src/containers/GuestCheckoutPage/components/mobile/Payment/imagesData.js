import Img1 from './images/img1.svg';
import Img2 from './images/img2.svg';
import Img3 from './images/img3.png';
import Img4 from './images/img4.png';
import Img5 from './images/img5.png';
import Img6 from './images/img6.png';
import Img7 from './images/img7.png';

export const images1 = [
  {id: 1, url: Img1},
  {id: 2, url: Img2},
  {id: 3, url: Img3}
];

export const images2 = [
  {id: 1, url: Img4},
  {id: 2, url: Img5},
  {id: 3, url: Img6},
  {id: 4, url: Img7}
];
