import React, {useState} from 'react';
import {string, func} from 'prop-types';
import Icon from 'components/Icon';
import Divider from 'components/Divider';
import {Wrapper, CouponContainer, PaymentContainer} from './styled';
import Modal from '../Modal';
import {Step, StepHeader} from '../../../styled';
import Coupons from './components/Coupons';
import Midcoins from './components/Midcoins';
import PaymentMethods from './components/PaymentMethods';

const Payment = ({paymentMethod, setPaymentMethod, selectedAddress, step, setStep, totals, email}) => {
  const [modal, setModal] = useState(false);

  return (
    <Wrapper>
      <Step mini>
        <StepHeader mini onClick={() => setStep('details')}>
          1. Shipping Address
          <Icon className="pencil" type="pencil" color={step === 'shippingAddress' ? '#CCCCCC' : '#000'} />
        </StepHeader>
      </Step>
      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />
      <Step mini>
        <StepHeader mini onClick={() => setStep('review')}>
          2. Preview Order
          <Icon type="arrow" className="arrow" color="#000" />
        </StepHeader>
      </Step>
      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />
      <Step>
        <StepHeader opened>
          3. Payment
          <Icon type="arrow" className="arrow" color="#000" />
        </StepHeader>
      </Step>
      <CouponContainer>
        <Coupons />
        <Midcoins />
      </CouponContainer>
      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />

      <PaymentContainer>
        <h3>Pay with</h3>

        <PaymentMethods paymentMethod={paymentMethod} setPaymentMethod={setPaymentMethod} setModal={setModal} />
        {/* <FadeOnMount>
          <p className="info">
            You’ll be redirected to a secure HTTP(s) page where you’ll be able to complete the Card payment for your
            order.
          </p>
        </FadeOnMount> */}

        <Modal
          email={email}
          totals={totals}
          selectedAddress={selectedAddress}
          onClose={() => setModal(false)}
          onShow={modal}
        />
      </PaymentContainer>
    </Wrapper>
  );
};

Payment.defaultProps = {};

Payment.propTypes = {
  step: string.isRequired,
  setStep: func.isRequired
};

export default Payment;
