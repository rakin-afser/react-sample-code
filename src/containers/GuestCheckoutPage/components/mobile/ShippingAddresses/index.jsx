import React, {useEffect, useState} from 'react';
import Icon from 'components/Icon';
import Divider from 'components/Divider';
import {
  Wrapper,
  IconsContainer,
  RadioWrapper,
  StyledRadio,
  StyledRadioGroup,
  AddrNickName,
  FullName,
  AddNew,
  GuestAddress,
  WarningMessage,
  Continue
} from './styled';
import {Step, StepHeader, StepContent} from '../../../styled';
import {getCountryFromCode, isDeliveryRegion} from 'util/heplers';
import {useMutation} from '@apollo/client';
import {REMOVE_SHIPPING_ADDRESS} from 'mutations';
import {ShippingAddressFragment} from 'fragments';
import {useUser} from 'hooks/reactiveVars';
import {isEmpty} from 'lodash';
import EditAddress from './components/EditAddress';

const ShippingAddresses = ({
  step,
  setStep,
  addresses,
  shippingAddressChanged,
  cartData,
  loadingCalculate,
  setSelectedAddress,
  selectedAddress
}) => {
  const [removeAddress] = useMutation(REMOVE_SHIPPING_ADDRESS);
  const [user] = useUser();
  const {email} = user || {};
  const [editAddress, setEditAddress] = useState(false);

  useEffect(() => {
    // Launch shipping calculation
    //  automatically if user is not logged in

    if (!email && !isEmpty(selectedAddress)) {
      shippingAddressChanged(selectedAddress);
    }
  }, [email, selectedAddress]);

  const onChangeRadio = (e) => {
    const id = e?.target?.value;
    const selectedAddr = addresses?.find((addr) => addr?.id === id);
    shippingAddressChanged(selectedAddr);
  };

  const onDeleteAddress = (id) => {
    if (user) {
      removeAddress({
        variables: {input: {addrId: id}},
        update(cache, {data}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = data?.removeShippingAddress?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                if (data?.removeShippingAddress?.shippingAddresses?.length === 1) {
                  const [address] = data?.removeShippingAddress?.shippingAddresses || [];
                  shippingAddressChanged(address);
                }
                return {...existing, shipping};
              }
            }
          });
        }
      });
    }

    if (!user) {
      localStorage.removeItem('gAddress');
      setSelectedAddress(null);
    }
  };

  function renderAddresses() {
    if (!user && !isEmpty(selectedAddress)) {
      const {id, addressNickname, firstName, build, address1, city, district, country} = selectedAddress || {};
      return (
        <GuestAddress>
          <IconsContainer>
            <Icon type="pencil" color="#545454" onClick={() => setEditAddress(selectedAddress)} />
            <Icon onClick={() => onDeleteAddress(id)} type="crossThin" color="#545454" />
          </IconsContainer>
          <div>
            <AddrNickName>{addressNickname}</AddrNickName>
            <FullName>{firstName}</FullName>
            {[build, address1, city, district, getCountryFromCode(country)]?.filter((item) => item)?.join(', ')}
          </div>
          {!isEmpty(cartData) && !loadingCalculate && !isDeliveryRegion(cartData?.cart?.subcarts?.subcarts) ? (
            <WarningMessage>Sorry, delivery to this region is not possible yet</WarningMessage>
          ) : null}
        </GuestAddress>
      );
    }

    const {id} = addresses?.find(({selected}) => selected) || {};
    return (
      <StyledRadioGroup value={id} onChange={onChangeRadio} name="address">
        {addresses?.map((props) => {
          const {id, firstName, build, address1, city, district, country, addressNickname} = props;
          return (
            <RadioWrapper key={id}>
              <IconsContainer>
                <Icon type="pencil" color="#545454" onClick={() => setEditAddress(props)} />
                <Icon onClick={() => onDeleteAddress(id)} type="crossThin" color="#545454" />
              </IconsContainer>
              <StyledRadio value={id}>
                <AddrNickName>{addressNickname}</AddrNickName>
                <FullName>{firstName}</FullName>
                {[build, address1, city, district, getCountryFromCode(country)]?.filter((item) => item)?.join(', ')}
              </StyledRadio>
              {!isEmpty(cartData) &&
              !loadingCalculate &&
              props?.selected &&
              !isDeliveryRegion(cartData?.cart?.subcarts?.subcarts) ? (
                <WarningMessage>Sorry, delivery to this region is not possible yet</WarningMessage>
              ) : null}
            </RadioWrapper>
          );
        })}
      </StyledRadioGroup>
    );
  }

  const disabled =
    loadingCalculate ||
    !isDeliveryRegion(cartData?.cart?.subcarts?.subcarts) ||
    !selectedAddress ||
    (!addresses?.length && email);

  return (
    <>
      <Wrapper>
        <Step>
          <StepHeader>
            1. Shipping Address
            <Icon
              type="pencil"
              className="pencil"
              color="#000"
              onClick={step !== 'details' ? () => setStep('details') : null}
            />
          </StepHeader>
          <StepContent>
            {renderAddresses()}
            <AddNew onClick={() => setStep('address')}>
              <Icon type="plus" />
              Add New Address
            </AddNew>
            <Continue
              kind="continue-big"
              loading={loadingCalculate}
              disabled={disabled}
              full
              onClick={() => setStep('review')}
            />
          </StepContent>
        </Step>

        <Divider
          style={{
            height: 8,
            background: '#FAFAFA'
          }}
        />

        <Step mini>
          <StepHeader mini disabled>
            2. Preview Order
            <Icon type="arrow" color={step === 'shippingAddress' ? '#CCCCCC' : '#000'} />
          </StepHeader>
        </Step>

        <Divider
          style={{
            height: 8,
            background: '#FAFAFA'
          }}
        />

        <Step mini>
          <StepHeader mini disabled>
            3. Payment
            <Icon type="arrow" color={step === 'shippingAddress' ? '#CCCCCC' : '#000'} />
          </StepHeader>
        </Step>
      </Wrapper>
      <EditAddress
        shippingAddressChanged={shippingAddressChanged}
        editAddress={editAddress}
        setEditAddress={setEditAddress}
      />
    </>
  );
};

export default ShippingAddresses;
