import styled from 'styled-components';
import {Button, Radio} from 'antd';
import Btn from 'components/Btn';

export const Wrapper = styled.div``;

export const RadioWrapper = styled.div`
  position: relative;
  z-index: 0;

  & + & {
    border-top: 1px solid #e4e4e4;
    margin-top: 8px;
    padding-top: 8px;
  }
`;

export const IconsContainer = styled.span`
  position: absolute;
  top: 8px;
  right: 0;
  width: 52px;
  height: 20px;
  padding: 3px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  z-index: 1;
`;

export const StyledRadioGroup = styled(Radio.Group)`
  &&& {
    width: 100%;
    margin-bottom: 16px;
  }
`;

export const StyledRadio = styled(Radio)`
  &&& {
    display: flex;
    align-items: center;
    width: 100%;

    .ant-radio-inner {
      width: 20px;
      height: 20px;

      &::after {
        top: 4px;
        left: 4px;
        width: 10px;
        height: 10px;
      }
    }
  }
`;

export const AddrNickName = styled.p`
  font-weight: 500;
  line-height: 1;
  margin-bottom: 4px;
`;

export const FullName = styled.p`
  line-height: 1;
  margin-bottom: 0;
`;

export const AddNew = styled(Button)`
  &&& {
    width: 100%;
    border: 1px solid #efefef;
    background: #ffffff;
    height: 48px;
    color: #000;
    font-weight: 500;

    i {
      margin-right: 21px;
      line-height: 1;
    }
  }
`;

export const GuestAddress = styled.div`
  position: relative;
  margin-bottom: 20px;
`;

export const WarningMessage = styled.div`
  color: #ed484f;
  margin-left: 30px;
`;

export const Continue = styled(Btn)`
  margin: 24px auto 0;
  display: flex;
`;
