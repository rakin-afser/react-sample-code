import styled from 'styled-components';

export const RowFields = styled.div`
  display: flex;
  margin: 0 -4px 16px;

  > * {
    width: 100%;
    margin: 0 4px;
  }
`;

export const Form = styled.form`
  padding: 20px 0;
`;
