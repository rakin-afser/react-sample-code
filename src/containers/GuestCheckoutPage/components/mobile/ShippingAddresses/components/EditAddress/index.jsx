import React from 'react';
import {Formik} from 'formik';
import {Select} from 'antd';
import {Form, RowFields} from './styled';

import Icon from 'components/Icon';
import Input from 'components/Input';
import SelectField from 'components/SelectField';
import Button from 'components/Button';
import BackPage from 'components/Modals/mobile/BackPage';
import exampleData from 'containers/GuestCheckoutPage/assets/data/exampleData';
import cities from 'containers/GuestCheckoutPage/assets/data/cities';
import {isEmpty} from 'lodash';
import {getCountryFromCode} from 'util/heplers';
import {EDIT_SHIPPING_ADDRESS} from 'mutations';
import {ShippingAddressFragment} from 'fragments';
import {useMutation} from '@apollo/client';
import {useUser} from 'hooks/reactiveVars';

const {Option} = Select;
const {country} = exampleData;

const EditAddress = ({editAddress, setEditAddress, shippingAddressChanged}) => {
  const [updateAddress] = useMutation(EDIT_SHIPPING_ADDRESS, {
    onCompleted() {
      setEditAddress(false);
    }
  });
  const [user] = useUser();

  function onSubmit(values, {resetForm}) {
    const address = {
      ...values,
      country: country?.[values?.country]?.code,
      // floor: parseInt(values?.floor),
      apartment: parseInt(values?.apartment),
      block: parseInt(values?.block),
      road: parseInt(values?.road)
    };

    if (!user) {
      shippingAddressChanged(address);
      localStorage.setItem('gAddress', JSON.stringify(address));
      setEditAddress(false);
    }

    if (user) {
      const input = {
        addrId: editAddress?.id,
        addrAddDirections: values?.addDirections,
        addrAddress1: values?.address1,
        addrAddressNickname: values?.addressNickname,
        addrApartment: parseInt(values?.apartment),
        addrBlock: parseInt(values?.block),
        // addrBuild: values?.build,
        addrCity: values?.city,
        addrCountry: country?.[values?.country]?.code,
        addrDistrict: values?.district,
        addrFirstName: values?.firstName,
        // addrFloor: parseInt(values?.floor),
        addrMap: values?.map,
        addrPhone: values?.phone,
        addrRoad: parseInt(values?.road),
        addrSearchRoad: values?.searchRoad
        // addrType: values?.type
      };

      updateAddress({
        variables: {
          input
        },
        update(cache, {data}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                if (
                  data?.editShippingAddress?.shippingAddresses?.find((addr) => addr?.id === editAddress?.id)?.selected
                ) {
                  shippingAddressChanged({
                    ...values,
                    country: country?.[values?.country]?.code,
                    floor: parseInt(values?.floor),
                    apartment: parseInt(values?.apartment),
                    block: parseInt(values?.block),
                    road: parseInt(values?.road)
                  });
                }
                const shipping = data?.addShippingAddress?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                return {...existing, shipping};
              }
            }
          });
        }
      });
    }

    resetForm({
      country: '',
      phone: '',
      firstName: '',
      district: '',
      city: '',
      address1: '',
      // type: '',
      // build: '',
      block: '',
      road: '',
      // floor: '',
      apartment: '',
      addressNickname: ''
    });
  }

  return (
    <BackPage active={!isEmpty(editAddress)} setActive={setEditAddress} title="Edit Address">
      <Formik
        enableReinitialize
        initialValues={
          isEmpty(editAddress)
            ? {
                country: '',
                phone: '',
                firstName: '',
                district: '',
                city: '',
                address1: '',
                type: '',
                // build: '',
                // block: '',
                road: '',
                // floor: '',
                apartment: '',
                addressNickname: ''
              }
            : {...editAddress, country: getCountryFromCode(editAddress.country)}
        }
        validate={(values) => {
          const errors = {};

          if (!values.country) {
            errors.country = 'Please select a country';
          }

          if (!values.phone) {
            errors.phone = 'Please enter a valid mobile number';
          }

          if (!values.firstName) {
            errors.firstName = 'Please enter a valid full name';
          }

          if (!values.district) {
            errors.district = 'Please select a District / Area';
          }

          if (!values.city) {
            errors.city = 'Please enter a valid City';
          }

          if (!values.address1) {
            errors.address1 = 'Please enter a valid Street / Building Name';
          }

          // if (!values.type) {
          //   errors.type = 'Please choose Address Type';
          // }

          // if (!values.build) {
          //   errors.build = 'Please enter a valid build / Building No.';
          // }

          if (!values.block) {
            errors.block = 'Please enter a valid Block No.';
          }

          if (!values.road) {
            errors.road = 'Please enter a valid Road No.';
          }

          return errors;
        }}
        onSubmit={onSubmit}
      >
        {({values, errors, status, handleChange, handleSubmit, submitCount, setFieldValue}) => {
          return (
            <Form onSubmit={handleSubmit}>
              <SelectField
                key="country"
                name="country"
                id="country"
                label="Country"
                onChange={(value) => {
                  setFieldValue('country', value);
                }}
                value={values.country}
                error={submitCount > 0 && errors.country}
                required
              >
                {Object.keys(country).map((key) => (
                  <Option value={key} key={key}>
                    {key}
                  </Option>
                ))}
              </SelectField>

              <Input
                key="phone"
                type="tel"
                name="phone"
                id="phone"
                label="Mobile Number"
                onChange={handleChange}
                error={submitCount > 0 && errors.phone}
                value={values.phone}
                reset={values.phone.length > 0}
                disabled={!values.country}
                resetCallback={() => {
                  setFieldValue('phone', '');
                }}
                number={country[values.country] ? country[values.country].dialCode : '+'}
                required
              />

              <Input
                key="firstName"
                type="text"
                name="firstName"
                id="firstName"
                label="Full Name (First and Last name)"
                onChange={handleChange}
                error={submitCount > 0 && errors.firstName}
                value={values.firstName}
                reset={values.firstName.length > 0}
                resetCallback={() => {
                  setFieldValue('firstName', '');
                }}
                required
              />

              <SelectField
                key="district"
                name="district"
                id="district"
                label="District / Area"
                onChange={(value) => {
                  setFieldValue('district', value);
                }}
                value={values.district}
                error={submitCount > 0 && errors.district}
                disabled={!values.country}
                required
              >
                {cities[values.country] &&
                  cities[values.country].map((city, key) => (
                    <Option value={city} key={key}>
                      {city}
                    </Option>
                  ))}
              </SelectField>

              <Input
                key="city"
                type="text"
                name="city"
                id="city"
                label="City"
                onChange={handleChange}
                error={submitCount > 0 && errors.city}
                value={values.city}
                reset={values.city.length > 0}
                resetCallback={() => {
                  setFieldValue('city', '');
                }}
                required
              />

              <Input
                key="address1"
                type="text"
                name="address1"
                id="address1"
                label="Street / Building Name"
                onChange={handleChange}
                error={submitCount > 0 && errors.address1}
                value={values.address1}
                reset={values.address1.length > 0}
                resetCallback={() => {
                  setFieldValue('address1', '');
                }}
                required
              />

              {/* <RowFields>
                <SelectField
                  key="type"
                  name="type"
                  id="type"
                  label="Address Type"
                  onChange={(value) => {
                    setFieldValue('type', value);
                  }}
                  value={values.type}
                  error={submitCount > 0 && errors.type}
                  required
                >
                  <Option value="Apartment">Apartment</Option>
                  <Option value="Office">Office</Option>
                </SelectField>

                <Input
                  key="build"
                  type="text"
                  name="build"
                  id="build"
                  label="build / Building No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.build}
                  value={values.build}
                  reset={values.build.length > 0}
                  resetCallback={() => {
                    setFieldValue('build', '');
                  }}
                  required
                />
              </RowFields> */}

              <RowFields>
                <Input
                  key="block"
                  type="number"
                  name="block"
                  id="block"
                  label="Block No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.block}
                  value={values.block}
                  reset={values.block}
                  resetCallback={() => {
                    setFieldValue('block', '');
                  }}
                  required
                />

                <Input
                  key="road"
                  type="number"
                  name="road"
                  id="road"
                  label="Road No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.road}
                  value={values.road}
                  reset={values.road}
                  resetCallback={() => {
                    setFieldValue('road', '');
                  }}
                  required
                />

                {/* <Input
                  key="floor"
                  type="number"
                  name="floor"
                  id="floor"
                  label="Floor No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.floor}
                  value={values.floor}
                  reset={values.floor}
                  resetCallback={() => {
                    setFieldValue('floor', '');
                  }}
                /> */}

                <Input
                  key="apartment"
                  type="number"
                  name="apartment"
                  id="apartment"
                  label="Apt./House No."
                  onChange={handleChange}
                  error={submitCount > 0 && errors.apartment}
                  value={values.apartment}
                  reset={values.apartment}
                  resetCallback={() => {
                    setFieldValue('apartment', '');
                  }}
                />
              </RowFields>
              <Input
                key="addressNickname"
                type="text"
                name="addressNickname"
                id="addressNickname"
                label="Address Nickname"
                onChange={handleChange}
                error={submitCount > 0 && errors.addressNickname}
                value={values.addressNickname}
                reset={values.addressNickname.length > 0}
                resetCallback={() => {
                  setFieldValue('addressNickname', '');
                }}
              />

              <Button
                type="submit"
                styleType="blackBorder"
                style={{
                  margin: '24px auto 0',
                  minWidth: 295
                }}
              >
                <span>Save</span>
                <Icon
                  type="arrow"
                  color="#000"
                  width={7}
                  height={12}
                  style={{
                    margin: '1px 0 0 10px',
                    position: 'relative',
                    right: -20
                  }}
                />
              </Button>
            </Form>
          );
        }}
      </Formik>
    </BackPage>
  );
};

export default EditAddress;
