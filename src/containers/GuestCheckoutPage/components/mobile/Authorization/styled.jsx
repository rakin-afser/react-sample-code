import styled, {css} from 'styled-components';

export const Wrapper = styled.div`
  padding: 35px 16px;
`;

export const Title = styled.h3`
  font-weight: 500;
  font-size: 22px;
  line-height: 27px;
  text-align: center;
  color: #000000;
  margin: 0 0 24px;
  position: relative;

  i {
    position: absolute;
    top: 50%;
    left: -6px;
    transform: translate(0, -50%);

    svg {
      stroke: #000;
    }
  }
`;

export const SubTitle = styled.p`
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #464646;
  text-align: center;
  margin: 0 0 24px;
`;

export const ForgotLinks = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 24px;
`;

export const ForgotLink = styled.span`
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #4a90e2;
  cursor: pointer;
`;

export const BtnList = styled.ul`
  margin-top: 9px;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`;

export const BtnItem = styled.button`
  text-align: center;

  & + & {
    margin-top: 15px;
  }
`;

export const Or = styled.p`
  position: relative;
  color: #464646;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 0;

  &::before {
    content: 'Or';
    padding: 5px 10px;
    background-color: #fff;
    position: relative;
    z-index: 2;
  }

  &::after {
    content: '';
    border-bottom: 1px solid #e4e4e4;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto 0;
    height: 0;
    z-index: 1;
  }
`;
