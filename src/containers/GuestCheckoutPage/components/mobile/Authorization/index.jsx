import React, {useState} from 'react';
import {Formik} from 'formik';
import {func} from 'prop-types';
import {useHistory} from 'react-router-dom';
import Input from 'components/Input';
import Button from 'components/Button';
import Icon from 'components/Icon';
import Divider from 'components/Divider';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import {message} from 'antd';
import {Wrapper, Title, ForgotLinks, ForgotLink, SubTitle, BtnList, BtnItem, Or} from './styled';
import {Login, SendOTP, VALIDATE_EMAIL} from 'mutations';
import {useUser} from 'hooks/reactiveVars';
import {useMutation} from '@apollo/client';
import Btn from 'components/Btn';
import { FB_APP_ID, GOOGLE_APP_ID, localStorageKey } from 'constants/config';
import useGlobal from 'store';

const Authorization = ({setStep,setEmail}) => {
  const {push} = useHistory();
  const [, setUser] = useUser();
  const [,globalActions] = useGlobal();
  const [loader, setLoader] = useState(false);
  const [validateEmailMutation] = useMutation(VALIDATE_EMAIL);
  const [login, {loading: loadingLogin}] = useMutation(Login, {
    onCompleted(data) {
      localStorage.setItem('userId', data.login.user.databaseId);
      localStorage.setItem('token', data.login.authToken);
      setUser(data?.login?.user);
      setStep('details');
    }
  });

  const [sendOTP] = useMutation(SendOTP);

  const signInWithSocial = async (username, password, social_login, fb) => {
 
    
    try {
      validateEmail(username).then(async (exist) => {
        if (exist) {
          const {data, loading, error} = await login({
            variables: {input: {username, password: '', social_login: true}}
          });
          if (!error && !loading) {
            setUser(fb ? Object.assign({}, data.login.user, {avatar: {url: fb.avatar}}) : data.login.user);
            localStorage.setItem('avatar', fb.avatar);
            localStorage.setItem('userId', data.login.user.databaseId);
            localStorage.setItem('token', data.login.authToken);
            localStorage.setItem(localStorageKey.socialLogin, false);
            setStep('details');
            
 
          }
        } else {
          // alert("Account not exist")
          // show message account not valid
        }
      });

      // push('/');
    } catch (error) {
      // setNotificationMessage(getErrorByCode(error.code));
      // setIsLoading(false);
    }
  };



  const signInWithSocialByGuest = async (email, password, social_login, fb) => {

    try {
      // console.log(email,".....email")
      if(email){
        setEmail(email);
        localStorage.setItem('gEmail',email);
        setStep('details');
      }else{
        message.error("Email not found")
      }
     
    } catch (error) {
      console.log(error)
    }
  };
  

  const validateEmail = (email) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await validateEmailMutation({
          variables: {
            input: {
              email: email
            }
          }
        });
        const {
          data: {
            validateEmail: {code}
          }
        } = response;
        if (code == 200) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (e) {
        reject(e);
      }
    });
  };
  return (
    <Wrapper>
      <Title>
        <Icon type="arrowBack" onClick={() => push('/cart')} />
        Please Sign in
      </Title>

      <Formik
        initialValues={{username: '', password: ''}}
        validate={(values) => {
          const errors = {};

          if (!values.password) {
            errors.password = 'Please enter a valid password';
          }

          if (!values.username) {
            errors.username = 'Please enter a valid username or email address';
          }

          return errors;
        }}
        onSubmit={(values) => {
          login({
            variables: {input: {...values}}
          });
        }}
      >
        {({values, errors, handleChange, handleSubmit, setFieldValue, submitCount, isSubmitting}) => (
          <form onSubmit={handleSubmit}>
            <Input
              key="username"
              type="text"
              name="username"
              id="username"
              label="Email or Username"
              onChange={handleChange}
              error={submitCount > 0 && errors.username}
              value={values.username}
              reset={values.username.length > 0}
              resetCallback={() => {
                setFieldValue('username', '');
              }}
              required
            />

            <Input
              key="password"
              type="password"
              name="password"
              id="password"
              label="Password"
              onChange={handleChange}
              error={submitCount > 0 && errors.password}
              value={values.password}
              showPassword
              required
            />

            <ForgotLinks>
              <ForgotLink>Forgot Username?</ForgotLink>
              <ForgotLink style={{marginLeft: 'auto'}}>Forgot Password?</ForgotLink>
            </ForgotLinks>

            <Button
              type="submit"
              disabled={loadingLogin}
              style={{
                margin: '0 auto',
                minWidth: 168
              }}
            >
              {
                isSubmitting
                ? <Icon type="loader" svgStyle={{width: 30, height: 6, fill: '#fff'}}/>
                : <>
                  <span>Sign in</span>
                  <Icon
                    type="arrow"
                    color="#fff"
                    width={7}
                    height={12}
                    style={{
                      margin: '1px 0 0 10px',
                      position: 'relative',
                      right: -20
                    }}
                  />
                </>
              }
            </Button>
          </form>
        )}
      </Formik>
      <Or />
      <BtnList>
      <GoogleLogin
          clientId={GOOGLE_APP_ID}
          render={(renderProps) => (
            <BtnItem disabled={renderProps.disabled} onClick={renderProps.onClick}>
              <Btn  style={{width: 246}} kind="googleAuth" />
            </BtnItem>
          )}
          onSuccess={(response) => {

                  const {email, familyName, givenName, imageUrl} = response.profileObj;
                  const formatedResponse = {
                    email: email,
                    firstName: givenName,
                    lastName: familyName,
                    avatar: imageUrl
                  };
                  globalActions.setSignUpInfo(formatedResponse);
                  signInWithSocial(email, '', true, formatedResponse).then();
            
          }}
          onFailure={() => {
           
          }}
          cookiePolicy={'single_host_origin'}
          
          />
    
        <FacebookLogin
                appId={FB_APP_ID}
                autoLoad={false}
                fields="name,email,picture"
                callback={(response) => {
                  if (response && response.email) {
                    const name = response.name.split(' ');
                    const formattedResponse = {
                      email: response.email,
                      firstName: name[0],
                      lastName: name.length > 1 ? name[1] : ' ',
                      avatar: `https://graph.facebook.com/${response.id}/picture?width=160&height=160`
                    };
                    globalActions.setSignUpInfo(formattedResponse);
                    signInWithSocial(response.email, '', true, formattedResponse).then();
                  } else {
                  }
                }}
                render={(renderProps) => (
                  <BtnItem onClick={renderProps.onClick}>
                    <Btn kind="facebookAuth" />
                  </BtnItem>
                )}
                />
      </BtnList>

      <Divider
        style={{
          height: 8,
          margin: '24px -24px',
          background: '#EFEFEF'
        }}
      />

      <Title style={{marginBottom: 12}}>Guest Checkout</Title>
      <SubTitle>
        Please proceed to checkout now and <br />
        create account later
      </SubTitle>

      <Formik
        initialValues={{email: ''}}
        validate={(values) => {
          const errors = {};

          if (!values.email) {
            errors.email = 'Please enter a valid username or email address';
          }

          return errors;
        }}
        onSubmit={async (values) => {
          try {
            const {data} = await sendOTP({variables: {input: {email: values?.email}}});
            if (data?.sendOTP) {
              setStep('verify', {email: values?.email});
            }
          } catch (error) {
            console.log(`error`, error);
          }
        }}
      >
        {({values, errors, handleChange, handleSubmit, isSubmitting, setFieldValue, submitCount}) => (
          <form onSubmit={handleSubmit}>
            <Input
              key="email"
              type="email"
              name="email"
              id="email"
              label="Email or Username"
              onChange={handleChange}
              error={submitCount > 0 && errors.email}
              value={values.email}
              reset={values.email.length > 0}
              resetCallback={() => {
                setFieldValue('email', '');
              }}
              required
            />
            <Button
              type="submit"
              styleType="blackBorder"
              disabled={isSubmitting}
              style={{
                margin: '24px auto 0',
                minWidth: 220
              }}
            >
              {
                isSubmitting
                ? <Icon type="loader" svgStyle={{width: 30, height: 6, fill: '#ED484F'}}/>
                : <>
                  <span>Continue as Guest</span>
                  <Icon
                    type="arrow"
                    color="#000"
                    width={7}
                    height={12}
                    style={{
                      margin: '1px 0 0 10px',
                      position: 'relative',
                      right: -20
                    }}
                  /> 
                </>
              }
            </Button>
          </form>
        )}
      </Formik>

      <Or />
      <BtnList>
      <GoogleLogin
          clientId={GOOGLE_APP_ID}
          render={(renderProps) => (
            <BtnItem disabled={renderProps.disabled} onClick={renderProps.onClick}>
              <Btn kind="googleAuth" />
            </BtnItem>
          )}
          onSuccess={(response) => {

                  const {email, familyName, givenName, imageUrl} = response.profileObj;
                  const formatedResponse = {
                    email: email,
                    firstName: givenName,
                    lastName: familyName,
                    avatar: imageUrl
                  };
                  console.log(email,"email")
                  // globalActions.setSignUpInfo(formatedResponse);
                  signInWithSocialByGuest(email, '', true, formatedResponse).then();
            
          }}
          onFailure={() => {
          
          }}
          cookiePolicy={'single_host_origin'}
          
          />
    
    <FacebookLogin
                appId={FB_APP_ID}
                autoLoad={false}
                fields="name,email,picture"
                callback={(response) => {
                  if (response && response.email) {
                    const name = response.name.split(' ');
                    const formattedResponse = {
                      email: response.email,
                      firstName: name[0],
                      lastName: name.length > 1 ? name[1] : ' ',
                      avatar: `https://graph.facebook.com/${response.id}/picture?width=160&height=160`
                    };
                    // console.log(response.email,"email")
                    // globalActions.setSignUpInfo(formattedResponse);
                    signInWithSocialByGuest(response.email, '', true, formattedResponse).then();
                  } else {
                    // setNotificationMessage({
                    //   text: 'Cancelled',
                    //   type: 'error'
                    // });
                    // setError(true);
                  }
                }}
                render={(renderProps) => (
                  <BtnItem onClick={renderProps.onClick}>
                    <Btn kind="facebookAuth" />
                  </BtnItem>
                )}
                />
      </BtnList>
    </Wrapper>
  );
};

Authorization.propTypes = {
  setStep: func.isRequired,
  setData: func.isRequired
};

export default Authorization;
