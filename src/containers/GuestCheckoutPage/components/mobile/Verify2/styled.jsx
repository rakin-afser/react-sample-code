import styled from 'styled-components';
import {secondaryTextColor} from '../../../../../constants/colors';

export const Wrapper = styled.div`

`;

export const Title = styled.h3`
  font-weight: 500;
  font-size: 22px;
  line-height: 27px;
  text-align: center;
  color: #000000;
  margin: 0 0 24px;
  position: relative;
  
  i {
    position: absolute;
    top: 50%;
    right: -6px;
    transform: translate(0, -50%);
  }
`;

export const SubTitle = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  text-align: center;
  padding: 0px 30px;
  color: ${secondaryTextColor}
  margin-top: 24px;
  margin-bottom: 37px;
  line-height: 17px;
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const RedirectInfo = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 140%;
  color: #656565;
  padding: 0 35px;
  text-align: center;
`

export const PleaseEnterOTP = styled.p`
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: ${secondaryTextColor}
  text-align: center;
  margin: 0 0 31px;
`;

export const EnterNewPassword = styled.div`
  font-size: 16px;
  color: ${secondaryTextColor}
  text-align: center;
  padding: 0px 30px;
  margin-top: 48px;
  letter-spacing: -0.2px;
`;

export const ResendOTP = styled.span`
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  margin: 32px auto 0;
  color: #4A90E2;
  display: inline-block;
  cursor: pointer;
`;

export const ResetForm = styled.form`
  margin-top: 35px;
`;

export const FullWidthFormField = styled.div`
  width: 100%;
`;
