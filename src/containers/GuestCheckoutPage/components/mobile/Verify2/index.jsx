import React, {useState, useEffect} from 'react';
import {Formik} from 'formik';
import {bool, object, string} from 'prop-types';
import useGlobal from 'store';
import {useHistory} from 'react-router-dom';
import OTP from 'components/OTP';
import FormMessage from 'components/FormMessage';
import {Wrapper, SubTitle, Container, PleaseEnterOTP, RedirectInfo, EnterNewPassword} from './styled';
import Navigation2 from '../Navigation2';
import ButtonCustom from '../../../../../components/ButtonCustom';
import {validatePassword} from '../../../../../util/validators';
import {Content, FullWidthFormField, ResetForm} from '../../../../ResetPasswordPage/mobile/styled';
import Input from '../../../../../components/Input';
import AuthHeading from '../../../../../components/UserAuth/components/Heading';

const Verify2 = ({
  title,
  otpTitle,
  email,
  info,
  onClickBack,
  onSubmit,
  isSubmitting,
  isResending,
  errorMessage,
  isSuccess,
  onResend,
  type,
  isMobile,
  optError,
  validateOTP,
  setOptError
}) => {
  const [globalState, globalActions] = useGlobal();
  const {push} = useHistory();
  const [code, setCode] = useState();
  const [password, setPassword] = useState('');
  const [passwordValid, setPasswordValid] = useState(true);

  const handleSubmit = () => {
    onSubmit(code, email, password);
  };

  const onClickClose = () => {
    globalActions.setAuthPopup(false);
  };

  return (
    <Wrapper>
      {isMobile ? (
        <Navigation2 onClickBack={onClickBack} onClickClose={onClickClose} title={title} />
      ) : (
        <AuthHeading onClickBack={onClickBack} onClickClose={onClickClose} title={title} />
      )}

      <FormMessage message={errorMessage} styles={{marginTop: 24, marginBottom: 28}} />
      {isSuccess ? (
        <>
          <RedirectInfo>If you do nothing , the system will return to sign in 10 seconds</RedirectInfo>
          <ButtonCustom
            isLoading={isSubmitting}
            title="Return to Sign In"
            variant="outlined"
            color="primaryColor"
            styles={{margin: '44px auto 0 auto'}}
            onClick={() => push('sign-in')}
          />
        </>
      ) : (
        <>
          <SubTitle>
            {type === 'resetPassword' ? 'We just sent you an email with OTP code to' : info}
            <strong> {email}</strong>
          </SubTitle>
          <PleaseEnterOTP>{otpTitle}</PleaseEnterOTP>
          <OTP
            value={code}
            onChange={setCode}
            error={optError}
            styles={{
              maxWidth: 276,
              margin: '0 auto'
            }}
          />
          {type === 'resetPassword' && (
            <>
              <EnterNewPassword>Please enter your new password</EnterNewPassword>
              <Formik
                initialValues={{
                  password: '',
                  repeatPassword: ''
                }}
                validate={(values) => {
                  const errors = {};
                  const passwordValid = validatePassword(values.password);
                  setPassword(values.password);

                  if (!passwordValid.isValid) {
                    errors.password = passwordValid.message;
                  }

                  if (values.repeatPassword !== values.password) {
                    errors.repeatPassword = 'Passwords do not match. Please re-enter';
                  }
                  setPasswordValid(passwordValid.isValid && !errors.repeatPassword);
                  return errors;
                }}
              >
                {({values, errors, handleChange, touched, handleBlur}) => (
                  <ResetForm>
                    <Content>
                      <FullWidthFormField>
                        <Input
                          required
                          passwordChars={true}
                          key="password"
                          type="password"
                          name="password"
                          id="password"
                          label="Enter New Password"
                          placeholder="Enter your password"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          error={
                            errors.password && touched.password && values.password.length < 1 ? errors.password : ''
                          }
                          value={values.password}
                          showPassword={true}
                        />
                      </FullWidthFormField>
                      <FullWidthFormField>
                        <Input
                          required
                          key="repeatPassword"
                          type="password"
                          name="repeatPassword"
                          id="repeatPassword"
                          label="Re-Enter New Password"
                          placeholder="Enter your password"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          error={errors.repeatPassword && touched.repeatPassword ? errors.repeatPassword : ''}
                          value={values.repeatPassword}
                          showPassword={true}
                        />
                      </FullWidthFormField>
                    </Content>
                  </ResetForm>
                )}
              </Formik>
            </>
          )}
          <Container>
            <ButtonCustom
              isLoading={isSubmitting}
              title="Confirm"
              variant="contained"
              color="mainWhiteColor"
              styles={{marginTop: type === 'resetPassword' ? 32 : 64}}
              onClick={handleSubmit}
              disabled={code?.length !== 6}
            />
            <ButtonCustom
              isLoading={isResending}
              title="Resend OTP"
              color="blueColor"
              animationColor="primaryColor"
              styles={{marginTop: 22}}
              onClick={onResend}
            />
          </Container>
        </>
      )}
    </Wrapper>
  );
};

Verify2.defaultProps = {
  data: {},
  otpTitle: 'Please Enter OTP',
  email: null,
  info: 'We just sent an email with OTP code to',
  title: 'Verify OTP',
  type: '',
  optError: ''
};

Verify2.propTypes = {
  data: object,
  otpTitle: string,
  email: string,
  info: string,
  title: string,
  type: string,
  isMobile: bool,
  optError: string
};

export default Verify2;
