import styled from 'styled-components';

export const Wrapper = styled.div`

`;

export const RowFields = styled.div`
  display: flex;
  margin: 0 -4px 16px;
  
  > * {
    width: 100%;
    margin: 0 4px;
  }
`;

export const SelectLocation = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 24px 0;
  font-weight: 500;
  font-size: 14px;
  line-height: 17px;
  color: #ED484F;
  cursor: pointer;
  
  i {
    margin-right: 8px;
  }
`;