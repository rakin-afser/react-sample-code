import React, {useEffect, useState} from 'react';
import {Formik} from 'formik';
import {string, func} from 'prop-types';
import {Select} from 'antd';

import Icon from 'components/Icon';
import Input from 'components/Input';
import SelectField from 'components/SelectField';
import Button from 'components/Button';
import Divider from 'components/Divider';

import exampleData from '../../../assets/data/exampleData';
import cities from '../../../assets/data/cities';

import {Wrapper, RowFields, SelectLocation} from './styled';

import {Step, StepHeader, StepContent, StepContentTitle} from '../../../styled';
import SelectLocationMap from '../SelectLocation';
import {useMutation} from '@apollo/client';
import {ADD_SHIPPING_ADDRESS} from 'mutations';
import {ShippingAddressFragment} from 'fragments';
import {useUser} from 'hooks/reactiveVars';

const {Option} = Select;
const {country} = exampleData;

const ShippingAddress = ({step, setStep, showLocation, setShowLocation, shippingAddressChanged}) => {
  const [user] = useUser();
  const [addAddress] = useMutation(ADD_SHIPPING_ADDRESS, {
    onCompleted() {
      setStep('details');
    }
  });

  if (showLocation) {
    return <SelectLocationMap setShowLocation={setShowLocation} />;
  }

  const onSubmit = (values) => {
    const {
      country: countryName,
      phone,
      firstName,
      district,
      city,
      address1,
      // type,
      // build,
      block,
      road,
      // floor,
      apartment,
      addressNickname
    } = values;

    const address = {
      addrCountry: country?.[countryName]?.code,
      addrPhone: phone,
      addrFirstName: firstName,
      addrDistrict: district,
      addrCity: city,
      addrAddress1: address1,
      // addrType: type,
      // addrBuild: build,
      addrBlock: parseInt(block),
      addrRoad: parseInt(road),
      // addrFloor: parseInt(floor),
      addrApartment: parseInt(apartment),
      addrAddressNickname: addressNickname
    };

    const addressCalc = {
      country: country?.[countryName]?.code,
      phone,
      firstName,
      district,
      city,
      address1,
      block: parseInt(block),
      road: parseInt(road),
      apartment: parseInt(apartment),
      addressNickname
    };

    if (user) {
      addAddress({
        variables: {input: address},
        update(cache, {data}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = data?.addShippingAddress?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                if (shipping?.length === 1) {
                  shippingAddressChanged(addressCalc);
                }
                return {...existing, shipping};
              }
            }
          });
        }
      });
    }

    if (!user) {
      shippingAddressChanged({...values, country: country?.[countryName]?.code});
      localStorage.setItem('gAddress', JSON.stringify({...values, country: country?.[countryName]?.code}));
      setStep('details');
    }
  };

  return (
    <Wrapper>
      <Step>
        <StepHeader>
          1. Shipping Address
          <Icon
            type="pencil"
            className="pencil"
            color="#000"
            onClick={step !== 'details' ? () => setStep('details') : null}
          />
        </StepHeader>

        <StepContent>
          <StepContentTitle>Add New Address</StepContentTitle>

          <Formik
            initialValues={{
              country: '',
              phone: '',
              firstName: '',
              district: '',
              city: '',
              address1: '',
              type: '',
              build: '',
              block: '',
              road: '',
              floor: '',
              apartment: '',
              addressNickname: ''
            }}
            validate={(values) => {
              const errors = {};

              if (!values.country) {
                errors.country = 'Please select a country';
              }

              if (!values.phone) {
                errors.phone = 'Please enter a valid mobile number';
              }

              if (!values.firstName) {
                errors.firstName = 'Please enter a valid full name';
              }

              if (!values.district) {
                errors.district = 'Please select a District / Area';
              }

              if (!values.city) {
                errors.city = 'Please enter a valid City';
              }

              if (!values.address1) {
                errors.address1 = 'Please enter a valid Street / Building Name';
              }

              // if (!values.type) {
              //   errors.type = 'Please choose Address Type';
              // }

              // if (!values.build) {
              //   errors.build = 'Please enter a valid build / Building No.';
              // }

              if (!values.block) {
                errors.block = 'Please enter a valid Block No.';
              }

              if (!values.road) {
                errors.road = 'Please enter a valid Road No.';
              }

              return errors;
            }}
            onSubmit={onSubmit}
          >
            {({values, errors, status, handleChange, handleSubmit, submitCount, setFieldValue}) => {
              return (
                <form onSubmit={handleSubmit}>
                  <SelectField
                    key="country"
                    name="country"
                    id="country"
                    label="Country"
                    onChange={(value) => {
                      setFieldValue('country', value);
                    }}
                    value={values.country}
                    error={submitCount > 0 && errors.country}
                    required
                  >
                    {Object.keys(country).map((key) => (
                      <Option value={key} key={key}>
                        {key}
                      </Option>
                    ))}
                  </SelectField>

                  <Input
                    key="phone"
                    type="tel"
                    name="phone"
                    id="phone"
                    label="Mobile Number"
                    onChange={handleChange}
                    error={submitCount > 0 && errors.phone}
                    value={values.phone}
                    reset={values.phone.length > 0}
                    disabled={!values.country}
                    resetCallback={() => {
                      setFieldValue('phone', '');
                    }}
                    number={country[values.country] ? country[values.country].dialCode : '+'}
                    required
                  />

                  <Input
                    key="firstName"
                    type="text"
                    name="firstName"
                    id="firstName"
                    label="Full Name (First and Last name)"
                    onChange={handleChange}
                    error={submitCount > 0 && errors.firstName}
                    value={values.firstName}
                    reset={values.firstName.length > 0}
                    resetCallback={() => {
                      setFieldValue('firstName', '');
                    }}
                    required
                  />

                  <SelectField
                    key="district"
                    name="district"
                    id="district"
                    label="District / Area"
                    onChange={(value) => {
                      setFieldValue('district', value);
                    }}
                    value={values.district}
                    error={submitCount > 0 && errors.district}
                    disabled={!values.country}
                    required
                  >
                    {cities[values.country] &&
                      cities[values.country].map((city, key) => (
                        <Option value={city} key={key}>
                          {city}
                        </Option>
                      ))}
                  </SelectField>

                  <Input
                    key="city"
                    type="text"
                    name="city"
                    id="city"
                    label="City"
                    onChange={handleChange}
                    error={submitCount > 0 && errors.city}
                    value={values.city}
                    reset={values.city.length > 0}
                    resetCallback={() => {
                      setFieldValue('city', '');
                    }}
                    required
                  />

                  <Input
                    key="address1"
                    type="text"
                    name="address1"
                    id="address1"
                    label="Street / Building Name"
                    onChange={handleChange}
                    error={submitCount > 0 && errors.address1}
                    value={values.address1}
                    reset={values.address1.length > 0}
                    resetCallback={() => {
                      setFieldValue('address1', '');
                    }}
                    required
                  />

                  {/* <RowFields>
                    <SelectField
                      key="type"
                      name="type"
                      id="type"
                      label="Address Type"
                      onChange={(value) => {
                        setFieldValue('type', value);
                      }}
                      value={values.type}
                      error={submitCount > 0 && errors.type}
                      required
                    >
                      <Option value="Apartment">Apartment</Option>
                      <Option value="Office">Office</Option>
                    </SelectField>

                    <Input
                      key="build"
                      type="text"
                      name="build"
                      id="build"
                      label="build / Building No."
                      onChange={handleChange}
                      error={submitCount > 0 && errors.build}
                      value={values.build}
                      reset={values.build.length > 0}
                      resetCallback={() => {
                        setFieldValue('build', '');
                      }}
                      required
                    />
                  </RowFields> */}

                  <RowFields>
                    <Input
                      key="block"
                      type="number"
                      name="block"
                      id="block"
                      label="Block No."
                      onChange={handleChange}
                      error={submitCount > 0 && errors.block}
                      value={values.block}
                      reset={values.block}
                      resetCallback={() => {
                        setFieldValue('block', '');
                      }}
                      required
                    />

                    <Input
                      key="road"
                      type="number"
                      name="road"
                      id="road"
                      label="Road No."
                      onChange={handleChange}
                      error={submitCount > 0 && errors.road}
                      value={values.road}
                      reset={values.road}
                      resetCallback={() => {
                        setFieldValue('road', '');
                      }}
                      required
                    />

                    <Input
                      key="apartment"
                      type="number"
                      name="apartment"
                      id="apartment"
                      label="Apt./House No."
                      onChange={handleChange}
                      error={submitCount > 0 && errors.apartment}
                      value={values.apartment}
                      reset={values.apartment}
                      resetCallback={() => {
                        setFieldValue('apartment', '');
                      }}
                    />
                  </RowFields>

                  <Input
                    key="addressNickname"
                    type="text"
                    name="addressNickname"
                    id="addressNickname"
                    label="Address Nickname"
                    onChange={handleChange}
                    error={submitCount > 0 && errors.addressNickname}
                    value={values.addressNickname}
                    reset={values.addressNickname.length > 0}
                    resetCallback={() => {
                      setFieldValue('addressNickname', '');
                    }}
                  />

                  <SelectLocation onClick={() => setShowLocation(true)}>
                    <Icon type="marker" color="#ED484F" />
                    <span>Select Location</span>
                  </SelectLocation>

                  <Button
                    type="submit"
                    styleType="blackBorder"
                    style={{
                      margin: '24px auto 0',
                      minWidth: 295
                    }}
                  >
                    <span>Save & Continue</span>
                    <Icon
                      type="arrow"
                      color="#000"
                      width={7}
                      height={12}
                      style={{
                        margin: '1px 0 0 10px',
                        position: 'relative',
                        right: -20
                      }}
                    />
                  </Button>
                </form>
              );
            }}
          </Formik>
        </StepContent>
      </Step>

      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />

      <Step mini>
        <StepHeader mini disabled>
          2. Preview Order
          <Icon type="arrow" color={step === 'shippingAddress' ? '#CCCCCC' : '#000'} />
        </StepHeader>
      </Step>

      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />

      <Step mini>
        <StepHeader mini disabled>
          3. Payment
          <Icon type="arrow" color={step === 'shippingAddress' ? '#CCCCCC' : '#000'} />
        </StepHeader>
      </Step>
    </Wrapper>
  );
};

ShippingAddress.defaultProps = {};

ShippingAddress.propTypes = {
  step: string.isRequired,
  setStep: func.isRequired
};

export default ShippingAddress;
