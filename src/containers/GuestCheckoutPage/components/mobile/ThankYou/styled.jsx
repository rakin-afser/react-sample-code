import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';

export const Wrapper = styled.div`
  .header {
    padding: 32px 15px 40px;
    display: flex;
    flex-direction: column;
    align-items: center;
    border-bottom: none;
  }

  .title {
    font-style: normal;
    font-weight: 600;
    font-size: 26px;
    line-height: 31px;
    text-align: center;
    color: #000000;
    display: block;
  }

  .order-id {
    font-weight: 600;
    color: #000000;
    font-size: 20px;
    margin-bottom: 26px;
  }

  .label {
    font-style: normal;
    font-weight: normal;
    font-size: 14px;
    line-height: 132%;
    text-align: center;
    letter-spacing: -0.024em;
    color: #656565;
    display: block;
    margin-bottom: 8px;
  }

  .code {
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    line-height: 132%;
    text-align: center;
    letter-spacing: -0.024em;
    text-decoration-line: underline;
    display: block;
    color: #287bdd;
  }

  .content {
    padding: 16px 16px 50px;
  }

  .form {
    text-align: left;

    .title {
      font-style: normal;
      font-weight: 500;
      font-size: 18px;
      line-height: 22px;
      display: block;
      margin-bottom: 24px;
      color: #000000;
      text-align: left;
    }
  }
`;

export const DetailsBlock = styled.div`
  background: #ffffff;
  box-shadow: 0px 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  padding: 11px 32px 18px;
  margin-top: 30px;
  margin-bottom: 30px;
  width: 100%;
`;

export const DetailsTitle = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  color: #000000;
`;

export const GuestTrackingMessage = styled.p`
  font-size: 16px;
  text-align: center;
  margin: 20px 0;
  font-weight: 500;
`;
