import React, {useEffect} from 'react';
import {useLocation} from 'react-router';
import qs from 'qs';
import {useApolloClient, useLazyQuery} from '@apollo/client';
import moment from 'moment';

import Divider from 'components/Divider';
import {GET_TAP_ORDER, testSample_COD_PAYMENT} from 'queries';
import {Wrapper, DetailsBlock, DetailsTitle, GuestTrackingMessage} from './styled';
import {DetailsText, InfoBlock} from 'containers/GuestCheckoutPage/components/Success/styled';
import Loader from 'components/Loader';
import {useUser} from 'hooks/reactiveVars';
import Error from 'components/Error';
import OrderSignUpForm from 'components/OrderSignUpForm';
import EarnedCoins from 'containers/GuestCheckoutPage/components/EarnedCoins';
import TrackingStatus from 'containers/GuestCheckoutPage/components/mobile/TrackingStatus';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import {usetestSampleCheckoutdotcomPaymentLazyQuery} from 'hooks/queries';

const ThankYou = ({email}) => {
  const {search} = useLocation();
  const {tap_id: tapId, ckoId, cod} = qs.parse(search.replace('?', '')) || {};
  const [user] = useUser();
  const {cache} = useApolloClient();

  const [
    getCheckoutOrder,
    {data: dataCk, loading: loadingCk, error: errorCk}
  ] = usetestSampleCheckoutdotcomPaymentLazyQuery({
    variables: {ckoId},
    onCompleted(data) {
      const {status} = data?.testSampleCheckoutDotComPayment || {};
      if (status !== 'DECLINED') {
        cache.modify({
          fields: {
            cart() {
              return {};
            }
          }
        });
      }
    }
  });

  const [getTapOrder, {data: dataTap, loading: loadingTap, error: errorTap}] = useLazyQuery(GET_TAP_ORDER, {
    variables: {tapId},
    onCompleted(data) {
      const {status} = data?.testSampleTapPayment || {};
      if (status !== 'DECLINED') {
        cache.modify({
          fields: {
            cart() {
              return {};
            }
          }
        });
      }
    }
  });

  const [getCodOrder, {data: dataCod, loading: loadingCod, error: errorCod}] = useLazyQuery(testSample_COD_PAYMENT, {
    variables: {orderId: cod}
  });

  useEffect(() => {
    if (ckoId) {
      getCheckoutOrder();
    }
  }, [ckoId, getCheckoutOrder]);

  useEffect(() => {
    if (tapId) {
      getTapOrder();
    }
  }, [tapId, getTapOrder]);

  useEffect(() => {
    if (cod) {
      getCodOrder();
    }
  }, [cod, getCodOrder]);

  const {orderId, amount, date, midcoinsAmount, shopcoinsAmount, orders, status} =
    dataTap?.testSampleTapPayment || dataCod?.testSampleCodPayment || dataCk?.testSampleCheckoutDotComPayment || {};
  const {message} = errorTap || errorCod || errorCk || {};
  const shipping = orders?.nodes?.[0]?.shipping;

  if (message) return <Error>{message}</Error>;

  if (loadingTap || loadingCod || loadingCk) return <Loader />;

  return (
    <Wrapper>
      <div className="header">
        <span className="title">Thank you for your order</span>
        <span className="order-id">#{orderId}</span>
        <EarnedCoins midcoinsAmount={midcoinsAmount} shopcoinsAmount={shopcoinsAmount} />
      </div>
      <Divider
        style={{
          height: 8,
          background: '#FAFAFA'
        }}
      />

      <div className="content">
        <DetailsBlock>
          <DetailsTitle>Order Tracking</DetailsTitle>
          {user?.email ? (
            <TrackingStatus orders={orders} />
          ) : (
            <GuestTrackingMessage>
              Please create an account and sign in or check the status of your order via email.
            </GuestTrackingMessage>
          )}
        </DetailsBlock>
        <DetailsBlock>
          <DetailsTitle>Invoice Details</DetailsTitle>
          <InfoBlock>
            <DetailsText>
              Order ID<span>{orderId}</span>
            </DetailsText>
            <DetailsText>
              Order Date<span>{moment(date).format('DD MMMM, YYYY')}</span>
            </DetailsText>

            <DetailsText>
              Amount Paid
              <span>{amount}</span>
            </DetailsText>

            <DetailsText>
              Shipping Address
              <span>
                {[
                  shipping?.postCode,
                  shipping?.build,
                  shipping?.address1,
                  shipping?.city,
                  shipping?.district,
                  getCountryFromCode(shipping?.country)
                ]
                  .filter((item) => !!item)
                  .join(', ')}
              </span>
            </DetailsText>
          </InfoBlock>
        </DetailsBlock>
        {user?.email ? null : (
          <div className="form">
            <span className="title">Create Account & Unlock Coins</span>
            <OrderSignUpForm email={email} />
          </div>
        )}
      </div>
    </Wrapper>
  );
};

export default ThankYou;
