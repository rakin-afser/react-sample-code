import styled, {css} from 'styled-components/macro';

const circleSize = 20;

export const Wrapper = styled.div`
  padding: 38px 20px 0 0;
  display: flex;
  justify-content: flex-start;
`;

export const Block = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 450px;
`;

export const Circle = styled.div`
  width: ${circleSize}px;
  height: ${circleSize}px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  box-sizing: border-box;
  border-radius: 50%;
  border: 1px solid #a7a7a7;
  color: #a7a7a7;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  z-index: 5;
  cursor: pointer;
  transition: all 0.4s;
  position: relative;

  ${({status}) =>
    status === 'completed' &&
    css`
      background: #67dd99;
      border: 1px solid #67dd99;
      color: #fff;
    `}

  ${({status}) =>
    status === 'inProgress' &&
    css`
      background: #fff;
      color: #000000;
      border: 1px solid #000000;
    `}

  ${({status}) =>
    status === 'inProgressBig' &&
    css`
      background: #fff;
      color: #000000;
      border: 1px solid #000000;
      width: 30px;
      height: 30px;
      top: -5px;
    `}
`;

export const Line = styled.div`
  width: 100%;
  height: 2px;
  background: #f3f3f3;
  position: absolute;
  top: ${circleSize / 2}px;
  z-index: 1;
  left: 0;
  margin-left: ${circleSize / 2}px;

  ${({status}) =>
    status === 'completed' &&
    css`
      background: #67dd99;
    `}

  ${({status}) =>
    status === 'halfCompleted' &&
    css`
      &::before {
        content: '';
        width: 40%;
        height: 100%;
        background: #67dd99;
        position: absolute;
      }
    `}
`;

export const Table = styled.table`
  width: 100%;
`;

export const Row = styled.tr`
  height: 40px;
`;

export const Cell = styled.td`
  position: relative;
  padding-top: 0;
  vertical-align: top;
  padding: ${({padding}) => padding};
  z-index: 5;
  width: 28%;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 10px;
  color: #666666;
`;

export const Text = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 10px;
  line-height: 140%;
  color: #a7a7a7;

  ${({status}) =>
    status === 'inProgress' &&
    css`
      color: #000000;
    `}
`;

export const DeliveryLine = styled.div`
  width: 100%;
  height: 3px;
  background: #f3f3f3;
  position: relative;
  top: ${circleSize / 2 - 1}px;
  z-index: 1;
  margin-left: 33px;
  position: absolute;
  left: 0;

  &::before {
    content: '';
    width: 20%;
    height: 100%;
    background: #ffc131;
    position: absolute;
  }

  & span {
    top: 14px;
    white-space: nowrap;
    right: 0;
    position: absolute;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 140%;
    color: #8f8f8f;
    text-align: right;
    & b {
      color: #464646;
    }
  }
`;
