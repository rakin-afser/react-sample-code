import React from 'react';

import {Wrapper, Block, Row, Cell, Table, Text, Circle, Line, DeliveryLine} from './styled';
import Icon from 'components/Icon';
import {getCircleStatus, isStatusInProgress, isOrderDelivered, getLineStatus} from 'containers/GuestCheckoutPage/utils';
import OrderDelivered from 'containers/GuestCheckoutPage/components/OrderDelivered';

const TrackingStatus = ({orders}) => {
  if (isOrderDelivered(orders)) return <OrderDelivered />;

  return (
    <Wrapper>
      <Block>
        <Table>
          <tbody>
            {orders?.nodes?.map((item) => {
              const {orderNumber, status} = item;
              return (
                <>
                  <Cell style={{whiteSpace: 'nowrap'}} padding="0 0 12px 0">
                    Order ID {orderNumber}
                  </Cell>
                  <Cell> </Cell>
                  <Cell> </Cell>
                  <Row>
                    <Cell>
                      <Circle status="completed">
                        <Icon type="checkmark" color="#fff" />
                      </Circle>
                      <Line status={getLineStatus(1, status)} />
                    </Cell>
                    <Cell>
                      <Circle status={getCircleStatus(2, status)}>
                        {getCircleStatus(2, status) === 'completed' ? <Icon type="checkmark" color="#fff" /> : '2'}
                      </Circle>
                      <Line status={getLineStatus(2, status)} />
                    </Cell>
                    <Cell>
                      <Circle status={getCircleStatus(3, status)}>
                        {getCircleStatus(3, status) === 'completed' ? <Icon type="checkmark" color="#fff" /> : '3'}
                      </Circle>
                      {status === 'OutforDelivery' && (
                        <DeliveryLine>
                          <span>
                            Estimated time <b>1.5 Hours</b>
                          </span>
                        </DeliveryLine>
                      )}
                    </Cell>
                  </Row>
                  <Row>
                    <Cell>
                      <Text status={isStatusInProgress(1, status)}>Order Placed</Text>
                    </Cell>
                    <Cell>
                      <Text status={isStatusInProgress(2, status)}>Processing</Text>
                    </Cell>
                    <Cell>
                      <Text style={{whiteSpace: 'nowrap'}} status={isStatusInProgress(3, status)}>
                        Out for delivery
                      </Text>
                    </Cell>
                  </Row>
                </>
              );
            })}
          </tbody>
        </Table>
      </Block>
    </Wrapper>
  );
};

export default TrackingStatus;
