import styled from 'styled-components';

export const Wrapper = styled.div``;

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;
  font-style: normal;
  font-weight: 500;
  font-size: 20px;
  line-height: 24px;
  text-align: center;
  color: #000000;
  min-height: 73px;

  i {
    position: absolute;
    top: 50%;
    transform: translate(0, -50%);
    left: 14px;

    svg {
      stroke: #000;
    }

    &.close {
      left: initial;
      right: 14px;

      svg {
        stroke: none;
      }
    }
  }
`;
