import React, {useState} from 'react';
import Icon from 'components/Icon';
import Divider from 'components/Divider';
import {Wrapper, Container} from './styled';
import {ShippingAddress, ShippingAddresses, Navigation, SelectLocation, PreviewOrder, Payment, ThankYou} from '..';
import {Step, StepHeader, StepFooter, OrderItem} from '../../../styled';
import {useHistory} from 'react-router';
import {useMutation, useQuery} from '@apollo/client';
import {CART, ADDRESSES} from 'queries';
import {CALCULATE_SUBCARTS, testSample_ORDER_CREATE} from 'mutations';
import {MethodsFragment, ShippingAddressFragment, StoreFragment} from 'fragments';
import {useUser} from 'hooks/reactiveVars';
import Btn from 'components/Btn';
import PaymentHandler from 'components/PaymentHandler';
import Failed from '../Failed';
import Error from 'components/Error';
import {usetestSampleGetSavedCardTokenMutation} from 'hooks/mutations';
import {usetestSampleCheckoutQuery} from 'hooks/queries';

const SecureCheckout = ({step, setStep, email}) => {
  const {push} = useHistory();
  const [options, setOptions] = useState({showOrderSummary: true});
  const [selectedAddress, setSelectedAddress] = useState();
  const [showLocation, setShowLocation] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState('');
  const [err, setErr] = useState([]);
  const [user] = useUser();

  const initialInput = {
    email: user ? user?.email : email,
    addrAddress1: selectedAddress?.address1,
    addrBuild: selectedAddress?.build,
    addrBlock: selectedAddress?.block,
    addrCity: selectedAddress?.city,
    addrCountry: selectedAddress?.country,
    addrDistrict: selectedAddress?.district,
    addrFirstName: selectedAddress?.firstName,
    addrPhone: selectedAddress?.phone,
    addrRoad: selectedAddress?.road,
    addrApartment: selectedAddress?.apartment,
    addrType: selectedAddress?.type,
    tapRedirectUrl: `${window.location.origin}/checkout/payment-handler`,
    checkoutFailedUrl: `${window.location.origin}/checkout/failed`,
    checkoutRedirectUrl: `${window.location.origin}/checkout/payment-handler`
  };

  const {data: dataCheckout} = usetestSampleCheckoutQuery();
  const {tapPublicKey, tapEnable, checkoutPublicKey, checkoutEnable} = dataCheckout?.testSampleCheckout || {};

  const [createOrder, {error: errorCreateOrder, loading: loadingCreateOrder}] = useMutation(testSample_ORDER_CREATE, {
    onCompleted(data) {
      setErr([]);
      const {transactionUrl, orderCreateId, errors} = data?.testSampleOrderCreate || {};
      if (transactionUrl) {
        window.location.replace(transactionUrl);
        return null;
      }
      if (!transactionUrl) {
        console.log(`data`, data);
      }
      if (orderCreateId && !errors?.length) {
        push(`/checkout/success?cod=${orderCreateId}`);
        return null;
      }
      if (errors?.length) {
        setErr(errors);
      }
      return null;
    }
  });

  const [getCardTokenTap, {loading: loadingCardTap}] = usetestSampleGetSavedCardTokenMutation({
    onCompleted(dataCardToken) {
      const {token} = dataCardToken?.testSampleGetSavedCardToken || {};
      const input = {
        ...initialInput,
        paymentMethod: 'tap',
        paymentToken: token
      };
      createOrder({variables: {input}});
    }
  });

  const [getCardTokenCheckout, {loading: loadingCardCheckout}] = usetestSampleGetSavedCardTokenMutation({
    onCompleted(dataCardToken) {
      const {token} = dataCardToken?.testSampleGetSavedCardToken || {};
      const input = {
        ...initialInput,
        paymentMethod: 'checkoutdotcom',
        paymentToken: token
      };
      createOrder({variables: {input}});
    }
  });

  const {data: cartData} = useQuery(CART, {
    // ToDO: Add redirect if checkout is empty
    // onCompleted(dataCart) {
    //   const {contents} = dataCart?.cart || {};
    //   if (!contents?.nodes?.length) {
    //     push('/');
    //   }
    // }
  });
  const {data, loading} = useQuery(ADDRESSES, {
    onCompleted(dataAddress) {
      const selectedAddr =
        dataAddress?.testSampleAddresses?.shipping?.find((addr) => addr?.selected) ||
        JSON.parse(localStorage.getItem('gAddress'));
      setSelectedAddress(selectedAddr);

      const {id, address1, block, build, city, country, district, firstName, phone, road, type} = selectedAddr || {};

      if (!selectedAddr) {
        return;
      }

      calculate({
        variables: {
          input: {
            addrId: id,
            addrAddress1: address1,
            addrBlock: block,
            addrBuild: build,
            addrCity: city,
            addrCountry: country,
            addrDistrict: district,
            addrFirstName: firstName,
            addrPhone: phone,
            addrRoad: road,
            addrType: type
          }
        },
        optimisticResponse: {
          calculateSubcarts: {
            __typename: 'CalculateSubcartsPayload',
            shippingAddresses: dataAddress?.testSampleAddresses?.shipping?.map((addr) =>
              addr?.id === id ? {...addr, selected: true} : {...addr, selected: false}
            ),
            cart: cartData?.cart
          }
        },
        update(cache, {data: dataCalculate}) {
          cache.modify({
            fields: {
              testSampleAddresses(existing) {
                const shipping = dataCalculate?.calculateSubcarts?.shippingAddresses?.map((addr) =>
                  cache.writeFragment({
                    id: cache.identify(addr),
                    data: addr,
                    fragment: ShippingAddressFragment
                  })
                );
                return {...existing, shipping};
              },
              cart(existing) {
                return {
                  ...existing,
                  total: dataCalculate?.calculateSubcarts?.cart?.total,
                  subcarts: dataCalculate?.calculateSubcarts?.cart?.subcarts
                };
              }
            }
          });
        }
      });
    }
  });
  const [calculate, {loading: loadingCalculate}] = useMutation(CALCULATE_SUBCARTS);

  const {totalMidCoins, totalShopCoins} = cartData?.cart || {};
  const {itemsTotal, shipping} = cartData?.cart?.subcarts?.totals || {};
  const {appliedShopCoins, appliedMidCoins, appliedCoupons, total} = cartData?.cart || {};

  const shippingAddressChanged = (address) => {
    setSelectedAddress(address);
    const {id, address1, block, build, city, country, district, firstName, phone, road, type} = address || {};
    calculate({
      variables: {
        input: {
          addrId: id,
          addrAddress1: address1,
          addrBlock: block,
          addrBuild: build,
          addrCity: city,
          addrCountry: country,
          addrDistrict: district,
          addrFirstName: firstName,
          addrPhone: phone,
          addrRoad: road,
          addrType: type
        }
      },
      optimisticResponse: {
        calculateSubcarts: {
          __typename: 'CalculateSubcartsPayload',
          shippingAddresses: data?.testSampleAddresses?.shipping?.map((addr) =>
            addr?.id === id ? {...addr, selected: true} : {...addr, selected: false}
          ),
          cart: cartData?.cart
        }
      },
      update(cache, {data: dataCalculate}) {
        cache.modify({
          fields: {
            testSampleAddresses(existing) {
              const shipping = dataCalculate?.calculateSubcarts?.shippingAddresses?.map((addr) =>
                cache.writeFragment({
                  id: cache.identify(addr),
                  data: addr,
                  fragment: ShippingAddressFragment
                })
              );
              return {...existing, shipping};
            },
            cart(existing) {
              return {
                ...existing,
                subcarts: {
                  ...dataCalculate?.calculateSubcarts?.cart?.subcarts,
                  subcarts: dataCalculate?.calculateSubcarts?.cart?.subcarts?.subcarts?.map((s) => {
                    return {
                      ...s,
                      shipping: {
                        ...s?.shipping,
                        availableMethods: s?.shipping?.availableMethods?.map((m) =>
                          cache.writeFragment({
                            id: cache.identify(m),
                            data: m,
                            fragment: MethodsFragment
                          })
                        )
                      },
                      store: cache.writeFragment({
                        id: cache.identify(s?.store),
                        data: s?.store,
                        fragment: StoreFragment
                      })
                    };
                  })
                },
                total: dataCalculate?.calculateSubcarts?.cart?.total
              };
            }
          }
        });
      }
    });
  };

  function renderSecureCheckout() {
    switch (step) {
      case 'details':
        return (
          <ShippingAddresses
            shippingAddressChanged={shippingAddressChanged}
            step={step}
            setStep={setStep}
            addresses={data?.testSampleAddresses?.shipping}
            cartData={cartData}
            loadingCalculate={loading || loadingCalculate}
            setSelectedAddress={setSelectedAddress}
            selectedAddress={selectedAddress}
          />
        );
      case 'address':
        return (
          <ShippingAddress
            setStep={setStep}
            showLocation={showLocation}
            setShowLocation={setShowLocation}
            shippingAddressChanged={shippingAddressChanged}
          />
        );
      case 'review':
        return <PreviewOrder setStep={setStep} cartData={cartData} />;
      case 'payment':
        return (
          <Payment
            paymentMethod={paymentMethod}
            setPaymentMethod={setPaymentMethod}
            selectedAddress={selectedAddress}
            setStep={setStep}
            email={email}
            totals={cartData?.cart?.subcarts?.totals}
          />
        );
      case 'payment-handler':
        return <PaymentHandler />;
      case 'failed':
        return <Failed />;
      case 'success':
        return <ThankYou email={email} />;
      default:
        return null;
    }
  }
  const back = {
    details: '/cart',
    address: showLocation ? () => setShowLocation(false) : '/checkout/details',
    review: '/checkout/details',
    payment: '/checkout/review',
    success: '/checkout/payment'
  };

  const onClick = showLocation ? back[step] : () => push(back[step]);

  function renderHeader() {
    if (['payment-handler'].includes(step)) return null;

    return (
      <>
        <Container>
          {!['success', 'failed'].includes(step) ? <Icon type="arrowBack" onClick={onClick} /> : null}
          Secure Checkout
          {['success', 'failed'].includes(step) ? (
            <Icon type="close" className="close" color="#000" onClick={() => push('/')} />
          ) : null}
        </Container>
        {step === 'selectLocation' ? (
          ''
        ) : (
          <Divider
            style={{
              height: 8,
              background: '#FAFAFA'
            }}
          />
        )}
      </>
    );
  }

  function renderDebitButtons() {
    const buttons = [];
    const onSubmitDebit = (method) => {
      const input = {
        ...initialInput,
        paymentMethod: method,
        paymentToken: 'src_bh.benefit'
      };
      createOrder({variables: {input}});
    };
    // if (tapEnable ) {
    buttons.push(
      <Btn key="tap" kind="primary-bold" loading={loadingCreateOrder} full onClick={() => onSubmitDebit('tap')}>
        Confirm & Pay
      </Btn>
    );
    // }

    // if (checkoutEnable ) {
    //   buttons.push(
    //     <Btn key="checkout" kind="primary-bold" loading={loadingCreateOrder} full onClick={() => onSubmitDebit('checkoutdotcom')}>
    //       Confirm & Pay
    //     </Btn>
    //   );
    // }

    return buttons;
  }

  function renderSavedCardsButtons() {
    const buttons = [];
    if (tapEnable) {
      const onSubmitCardTokenTap = () => {
        getCardTokenTap({variables: {input: {card_id: paymentMethod}}});
      };
      buttons.push(
        <Btn
          key="tap"
          kind="primary-bold"
          full
          loading={loadingCreateOrder || loadingCardTap || loadingCardCheckout}
          onClick={onSubmitCardTokenTap}
        >
          Confirm &amp; Pay
        </Btn>
      );
    }

    if (checkoutEnable) {
      const onSubmitCardTokenCheckout = () => {
        getCardTokenCheckout({variables: {input: {card_id: paymentMethod}}});
      };

      buttons.push(
        <Btn
          key="checkout"
          kind="primary-bold"
          full
          loading={loadingCreateOrder || loadingCardTap || loadingCardCheckout}
          onClick={onSubmitCardTokenCheckout}
        >
          Confirm &amp; Pay
        </Btn>
      );
    }
    return buttons;
  }

  function onSubmitCod() {
    const input = {
      ...initialInput,
      tapRedirectUrl: undefined,
      checkoutFailedUrl: undefined,
      checkoutRedirectUrl: undefined,
      paymentMethod: 'cod'
    };

    createOrder({
      variables: {input},
      update(cache) {
        cache.modify({
          fields: {
            cart: () => {}
          }
        });
      }
    });
  }

  function renderButtons() {
    switch (paymentMethod) {
      case '':
      case 'cred':
        return (
          <Btn kind="primary-bold" full disabled>
            Confirm & Pay
          </Btn>
        );

      case 'deb':
        return renderDebitButtons();

      case 'cod':
        return (
          <Btn
            kind="primary-bold"
            full
            disabled={loadingCreateOrder}
            loading={loadingCreateOrder}
            onClick={onSubmitCod}
          >
            Confirm & Pay
          </Btn>
        );

      default:
        return renderSavedCardsButtons();
    }
  }

  return (
    <Wrapper>
      {renderHeader()}
      {renderSecureCheckout()}
      {showLocation || ['success', 'payment-handler', 'failed'].includes(step) ? null : (
        <>
          <Divider
            style={{
              height: 8,
              background: '#FAFAFA'
            }}
          />
          <Step mini>
            <StepHeader
              opened={options.showOrderSummary}
              mini
              onClick={() => setOptions({...options, showOrderSummary: !options.showOrderSummary})}
            >
              Order Summary
              <Icon type="arrow" color="#000" />
            </StepHeader>

            {options.showOrderSummary && (
              <>
                <OrderItem>
                  <span>Items Total</span>
                  <span>
                    {/* <small>BD</small> */}
                    <strong> {itemsTotal}</strong>
                  </span>
                </OrderItem>

                <OrderItem>
                  <span>Estimated Shipping</span>
                  <span>
                    {/* <small>BD</small> */}
                    <strong> {shipping}</strong>
                  </span>
                </OrderItem>

                {cartData?.cart?.subcarts?.totals?.totalTaxes?.map(({id, label, amount}) => (
                  <OrderItem key={id}>
                    <span>{label}</span>
                    <span>
                      {/* <small>BD</small> */}
                      <strong> {amount}</strong>
                    </span>
                  </OrderItem>
                ))}
                {/* {appliedShopCoins, appliedMidCoins, appliedCoupons} */}
                {appliedCoupons?.length
                  ? appliedCoupons?.map((c) => (
                      <OrderItem key={c?.code}>
                        <span>Coupons</span>
                        <span>
                          <strong> -{c?.discountAmount}</strong>
                        </span>
                      </OrderItem>
                    ))
                  : null}
                {appliedMidCoins ? (
                  <OrderItem>
                    <span>MidCoins</span>
                    <span>
                      <strong> -{appliedMidCoins?.discountAmount}</strong>
                    </span>
                  </OrderItem>
                ) : null}
                {appliedShopCoins ? (
                  <OrderItem>
                    <span>ShopCoins</span>
                    <span>
                      <strong> -{appliedShopCoins?.discountAmount}</strong>
                    </span>
                  </OrderItem>
                ) : null}

                <OrderItem total>
                  <span>Total To Pay</span>
                  <span>
                    {/* <small>BD</small> */}
                    <strong> {total}</strong>
                  </span>
                </OrderItem>

                {totalMidCoins ? (
                  <OrderItem coins>
                    <span>MidCoins </span>
                    <span>
                      <span>+{totalMidCoins || 0}</span>
                      <Icon type="coins" />
                    </span>
                  </OrderItem>
                ) : null}

                {totalShopCoins ? (
                  <OrderItem coins>
                    <span>Shop Coins</span>
                    <span>
                      <span>+{totalShopCoins || 0}</span>
                      <Icon type="coins" />
                    </span>
                  </OrderItem>
                ) : null}
              </>
            )}

            {errorCreateOrder ? (
              <Error customText="Sorry, your payment didn't pass. Please try again later or contact us." />
            ) : null}
            {err?.length
              ? err?.map((e) => (
                  <Error key={e?.code} title={e?.code ? `Code: ${e.code}` : undefined} customText={e?.description} />
                ))
              : null}

            <StepFooter>{renderButtons()}</StepFooter>
          </Step>
        </>
      )}
    </Wrapper>
  );
};

export default SecureCheckout;
