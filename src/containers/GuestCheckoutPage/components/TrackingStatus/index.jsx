import React from 'react';

import {Wrapper, Block, Row, Cell, Table, Circle, Line, DeliveryLine} from './styled';
import Icon from 'components/Icon';
import {getCircleStatus, getLineStatus, isOrderDelivered} from 'containers/GuestCheckoutPage/utils';
import OrderDelivered from 'containers/GuestCheckoutPage/components/OrderDelivered';

const TrackingStatus = ({orders}) => {
  if (isOrderDelivered(orders)) return <OrderDelivered />;

  return (
    <Wrapper>
      <Block>
        <Table>
          <tbody>
            {orders?.nodes?.map((item) => {
              const {orderNumber, status} = item;
              return (
                <Row>
                  <Cell>Order ID {orderNumber}</Cell>
                  <Cell>
                    <Circle status="completed">
                      <Icon type="checkmark" color="#fff" />
                    </Circle>
                    <Line status={getLineStatus(1, status)} />
                  </Cell>
                  <Cell>
                    <Circle status={getCircleStatus(2, status)}>
                      {getCircleStatus(2, status) === 'completed' ? <Icon type="checkmark" color="#fff" /> : '2'}
                    </Circle>
                    <Line status={getLineStatus(2, status)} />
                  </Cell>
                  <Cell>
                    <Circle status={getCircleStatus(3, status)}>
                      {getCircleStatus(3, status) === 'completed' ? <Icon type="checkmark" color="#fff" /> : '3'}
                    </Circle>
                    {status === 'OutforDelivery' && (
                      <DeliveryLine>
                        <span>
                          Estimated time <b>1.5 Hours</b>
                        </span>
                      </DeliveryLine>
                    )}
                  </Cell>
                </Row>
              );
            })}

            <Row>
              <Cell> </Cell>
              <Cell>Order Placed</Cell>
              <Cell>Processing</Cell>
              <Cell>Out for delivery</Cell>
            </Row>
          </tbody>
        </Table>
      </Block>
    </Wrapper>
  );
};

export default TrackingStatus;
