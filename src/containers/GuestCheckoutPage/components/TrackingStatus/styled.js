import styled, {css} from 'styled-components/macro';

export const Wrapper = styled.div`
  padding: 38px 110px 0 0;
  display: flex;
  justify-content: center;
`;

export const Block = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 450px;
`;

export const Circle = styled.div`
  width: 25px;
  height: 25px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  box-sizing: border-box;
  border-radius: 50%;
  border: 1px solid #a7a7a7;
  color: #a7a7a7;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  z-index: 5;
  margin: auto;
  cursor: pointer;
  transition: all 0.4s;
  position: relative;

  ${({status}) =>
    status === 'completed' &&
    css`
      background: #67dd99;
      border: 1px solid #67dd99;
      color: #fff;
    `}

  ${({status}) =>
    status === 'inProgress' &&
    css`
      background: #fff;
      color: #000000;
      border: 1px solid #000000;
    `}

  ${({status}) =>
    status === 'inProgressBig' &&
    css`
      background: #fff;
      color: #000000;
      border: 1px solid #000000;
      width: 38px;
      height: 38px;
      top: -7.5px;
    `}

  &:hover {
  }
`;

export const Line = styled.div`
  width: 100%;
  height: 2px;
  background: #f3f3f3;
  position: absolute;
  top: 12.5px;
  z-index: 1;
  left: 50%;
  margin-left: 12.5px;

  ${({status}) =>
    status === 'completed' &&
    css`
      background: #67dd99;
    `}

  ${({status}) =>
    status === 'halfCompleted' &&
    css`
      &::before {
        content: '';
        width: 40%;
        height: 100%;
        background: #67dd99;
        position: absolute;
      }
    `}
`;

export const Table = styled.table`
  width: 100%;
`;

export const Row = styled.tr`
  height: 50px;

  &:last-child {
    height: auto;
    td {
      padding-top: 15px;
      text-align: center;
    }
  }
`;

export const Cell = styled.td`
  position: relative;
  padding-top: 0;
  vertical-align: top;
  padding: ${({padding}) => padding};
  z-index: 5;

  &:first-child {
    padding-top: 6px;
  }
`;

export const DeliveryLine = styled.div`
  width: 160px;
  height: 3px;
  background: #f3f3f3;
  position: relative;
  top: 11.5px;
  z-index: 1;
  margin-left: 22px;
  position: absolute;
  left: 50%;

  &::before {
    content: '';
    width: 20%;
    height: 100%;
    background: #ffc131;
    position: absolute;
  }

  & span {
    top: 8px;
    position: absolute;
    font-family: Helvetica Neue;
    font-style: normal;
    font-weight: normal;
    font-size: 12px;
    line-height: 140%;
    color: #8f8f8f;
    & b {
      color: #464646;
    }
  }
`;
