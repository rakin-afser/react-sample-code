import styled from 'styled-components/macro';

export const CoinsBlock = styled.div`
  background: #f9f9f9;
  border-radius: 8px;
  padding: 17px 20px 16px 25px;
  display: flex;
  align-items: center;
`;

export const Coins = styled.div`
  margin-right: 21px;
  display: flex;
  flex-direction: column;

  & i {
    &:nth-child(1) {
      margin: 0 9px 3px auto;
    }
    &:nth-child(2) {
      margin: 0 auto 4px 0;
    }
    &:nth-child(3) {
      margin-left: 10px;
    }
  }
`;

export const CoinsTitle = styled.div`
  font-family: Helvetica Neue;
  font-weight: 500;
  font-size: 18px;
  color: #000000;
  line-height: 22px;
  margin-bottom: 8px;
`;

export const CoinsSubTitle = styled.div`
  font-family: SF Pro Display;
  font-weight: 600;
  font-size: 14px;
  color: #398287;
  line-height: 132%;

  & span {
    margin-right: 10px;
  }
`;
