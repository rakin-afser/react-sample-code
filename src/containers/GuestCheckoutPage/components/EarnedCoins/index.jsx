import React from 'react';

import {Coins, CoinsBlock, CoinsTitle, CoinsSubTitle} from './styled';
import Icon from 'components/Icon';

const EarnedCoins = ({midcoinsAmount, shopcoinsAmount}) => {
  return (
    <CoinsBlock>
      <Coins>
        <Icon type="coin" width={12} height={12} />
        <Icon type="coin" width={17} height={17} />
        <Icon type="coin" />
      </Coins>
      <div>
        <CoinsTitle>You’ve earned</CoinsTitle>
        <CoinsSubTitle>
          <span>Midcoins +{midcoinsAmount || 0}</span>
          <span>Shopcoins +{shopcoinsAmount || 0}</span>
        </CoinsSubTitle>
      </div>
    </CoinsBlock>
  );
};

export default EarnedCoins;
