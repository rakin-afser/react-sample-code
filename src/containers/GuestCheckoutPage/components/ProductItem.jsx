import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import {Divider} from 'antd';

import Influencer from './Influencer';

const Product = ({
  influencer,
  setInfluencer,
  idx,
  _idx,
  currency,
  product: {name, image, size, color, quantity, price, oldPrice},
  setCart,
  setTotalPrice,
  setInlucode
}) => {
  useEffect(() => {
    setCart((prev) => ({...prev, items: {...prev.items, [`${idx}-${_idx}`]: price}}));
    setTotalPrice((prev) => prev + Number(price));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div key={idx}>
      <div className="item">
        <img src={image} alt="product" />
        <div className="descrition">
          <h3>{name}</h3>
          <div className="characteristic">
            <span>
              Size: <span>{size}</span>
            </span>
            <Divider type="vertical" />
            <span>
              Color: <span>{color}</span>
            </span>
            <Divider type="vertical" />
            <span>
              Quantity: <span>{quantity}</span>
            </span>
          </div>
        </div>
        <div className="price">
          <small>{currency}</small>
          <strong>{price}</strong>
          {oldPrice && (
            <small className="old-price">
              {currency} <span>{oldPrice}</span>
            </small>
          )}
        </div>
      </div>
      <Influencer
        influencer={influencer}
        setInfluencer={setInfluencer}
        idx={`${idx}-${_idx}`}
        setCart={setCart}
        price={price}
        setInlucode={setInlucode}
      />
    </div>
  );
};

Product.propTypes = {
  influencer: PropTypes.objectOf(PropTypes.any).isRequired,
  setInfluencer: PropTypes.func.isRequired,
  setCart: PropTypes.func.isRequired,
  product: PropTypes.objectOf(PropTypes.any).isRequired,
  idx: PropTypes.number.isRequired,
  _idx: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired,
  setTotalPrice: PropTypes.func.isRequired,
  setInlucode: PropTypes.func.isRequired
};

export default Product;
