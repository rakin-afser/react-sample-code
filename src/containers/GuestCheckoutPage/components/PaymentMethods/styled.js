import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  & .ant-radio {
    + * {
      width: 100%;
    }
  }

  & .ant-radio-group {
    display: flex;
    flex-direction: column;
  }

  & .ant-radio-wrapper {
    display: flex;
    align-items: center;
    margin: 8px 0;
    color: #000;
  }

  & .ant-radio-inner {
    border-color: #000 !important;
    box-shadow: none !important;

    &::after {
      background-color: #000;
    }
  }
`;

export const RadioInner = styled.span`
  display: flex;
  align-items: center;
`;

export const Txt = styled.span`
  margin-right: ${({second}) => (second ? '24px' : '20px')};
`;

export const ImagesWrap = styled.span`
  display: flex;
  align-items: center;
`;

export const Img = styled.div`
  margin-right: 5px;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 17px;
  border: 1px solid #bdbdbd;
  border-radius: 2px;
  overflow: hidden;

  img {
    width: 94%;
    height: 94%;
    object-fit: contain;
  }
`;

export const Exp = styled.div`
  margin-left: auto;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #666666;
  min-width: 86px;
`;
