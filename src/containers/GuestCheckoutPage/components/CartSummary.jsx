import React, {useEffect, useState, useRef} from 'react';
import {shape, func, string} from 'prop-types';

import {CartSummery} from '../styled';
import Icon from 'components/Icon';

const CartSummary = ({step, cart, cartData, setCart, checkShipping}) => {
  const [discount, setDiscount] = useState({
    itemsNum: 0,
    items: 0,
    shipping: 0,
    pay: 0,
    coupons: 0,
    influencer: 0
  });
  const [stickyCart, setStickyCart] = useState(false);
  const {totalMidCoins, totalShopCoins} = cartData || {};
  const {itemsTotal, totalTaxes, shipping} = cartData?.subcarts?.totals || {};
  const {appliedShopCoins, appliedMidCoins, appliedCoupons, total} = cartData || {};
  const hasRedenptions = appliedShopCoins || appliedMidCoins || appliedCoupons;

  const cartRef = useRef();

  useEffect(() => {
    const items = Object.keys(cart.items).reduce((acc, key) => acc + Number(cart.items[key]), 0);
    const shippingAmount = Object.keys(cart.shipping).reduce((acc, key) => acc + Number(cart.shipping[key]), 0);
    const influencer = Object.keys(cart.discount.influencer).reduce(
      (acc, key) => acc + Number(cart.discount.influencer[key]),
      0
    );
    const coupons = cart.discount.coupon ? (items + shippingAmount) * (cart.discount.coupon / 100) : 0;
    const pay = items + shippingAmount - influencer - coupons;
    const midCoinsAmount = Math.round(pay * 0.01);
    const shopCoinsAmount = Math.round(pay * 0.01);

    const newTotal = {
      itemsNum: Object.keys(cart.items).length,
      items,
      shipping: shippingAmount,
      pay,
      coupons,
      influencer,
      midCoins: midCoinsAmount,
      shopCoins: shopCoinsAmount
    };
    setDiscount(newTotal);
  }, [cart]);

  const scrollCallBack = () => {
    const sticky = cartRef.current.offsetTop;
    if (window.pageYOffset > sticky - 20) {
      setStickyCart(true);
    }
    if (window.pageYOffset < 160) {
      setStickyCart(false);
    }
  };

  useEffect(() => {
    window.addEventListener('scroll', scrollCallBack);
    return () => {
      window.removeEventListener('scroll', scrollCallBack);
    };
  }, []);

  const confirm = () => {
    const error = checkShipping();
    setCart((prev) => ({...prev, error}));
    if (!error) {
      console.log('CONFIRM & PAY');
    }
  };

  const checkNextStep = () => {
    if (step !== 'payment') return true;

    return step === 'payment' && cart.paymentMethod === null;
  };

  const totalQuantity = cartData?.contents?.nodes
    ?.map((el) => el.quantity)
    .reduce((acc, quantity) => acc + quantity, 0);

  return (
    <CartSummery ref={cartRef} sticky={stickyCart}>
      <div className="wrapper">
        <h3 className="title">Cart Summary</h3>
        <div className="row">
          <span>Items Total ({totalQuantity})</span>
          <span>{itemsTotal}</span>
        </div>
        <div className="row">
          <span>Shipping Charges</span>
          <span>{shipping}</span>
        </div>
        {totalTaxes?.map(({id, label, amount}) => (
          <div key={id} className="row">
            <span>{label}</span>
            <span>{amount}</span>
          </div>
        ))}
        {hasRedenptions ? (
          <div className="row redemptions">
            <span>Redemptions:</span>
          </div>
        ) : null}
        {discount.influencer ? (
          <div className="row redemptions-discount">
            <span>Influencer Codes</span>
            <span className="coupons">
              <small>- {cart.currency}</small>
              {`${discount.influencer}`}
            </span>
          </div>
        ) : null}

        {appliedCoupons?.length
          ? appliedCoupons?.map((c) => (
              <div key={c?.code} className="row redemptions-discount">
                <span>Coupons</span>
                <span className="coupons">-{c?.discountAmount}</span>
              </div>
            ))
          : null}

        {appliedMidCoins ? (
          <div className="row redemptions-discount">
            <span>MidCoins</span>
            <span className="coupons">-{appliedMidCoins?.discountAmount}</span>
          </div>
        ) : null}

        {appliedShopCoins ? (
          <div className="row redemptions-discount">
            <span>ShopCoins</span>
            <span className="coupons">-{appliedShopCoins?.discountAmount}</span>
          </div>
        ) : null}

        <div className="row total-pay">
          <h3>Total To Pay</h3>
          <span>{total}</span>
        </div>
        {totalMidCoins ? (
          <div className="row coins">
            <h3>Mid Coins earned:</h3>
            <span>
              {`+${totalMidCoins}`} <Icon type="coins" />
            </span>
          </div>
        ) : null}
        {totalShopCoins ? (
          <div className="row coins">
            <h3>Shop Coins earned:</h3>
            <span>
              {`+${totalShopCoins}`} <Icon type="coins" />
            </span>
          </div>
        ) : null}

        {/* <div className="message">
          You <span>SAVE</span> <strong>40%</strong> on this order
        </div> */}
        {/* {cart.error ? (
          <div className="error-message">
            <CancelIcon /> <span>Please select delivery option</span>
          </div>
        ) : null} */}
      </div>
    </CartSummery>
  );
};

CartSummary.propTypes = {
  step: string.isRequired,
  cart: shape({}),
  setCart: func.isRequired,
  checkShipping: func.isRequired
};

export default CartSummary;
