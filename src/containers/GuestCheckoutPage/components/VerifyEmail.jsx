import React, {useState, useRef} from 'react';
import {Link, useLocation} from 'react-router-dom';
import {InputNumber} from 'antd';
import {func} from 'prop-types';

// import FormTemplate from './FormTemplate';
import {VerifyEmailWrapper, VerifyEmailForm, VerifyEmailTitle, Resend} from '../styled';
import {ReactComponent as BackArrow} from '../assets/icon/backArrow.svg';
import {ReactComponent as LoadingDots} from '../assets/icon/loadingDots.svg';
import {useMutation} from '@apollo/client';
import {SendOTP, ValidateOTP} from 'mutations';
import OTP from 'components/OTP';
import Btn from 'components/Btn';

const VerifyEmail = ({setStep, setEmail = () => {}}) => {
  const [invalidCode, setInvalidCode] = useState(false);
  const [succes, setSucces] = useState(false);
  const [error, setError] = useState(false);
  const [code, setCode] = useState();
  const [disabledResent, setDisabledResent] = useState(false);
  const [validateOtp, {loading}] = useMutation(ValidateOTP, {
    onCompleted(data) {
      if (data?.validateOTP?.code === '200') {
        setEmail(state?.email);
        localStorage.setItem('gEmail', state?.email);
        setStep('details');
        setError(false);
        setSucces(true);
        setInvalidCode(false);
      } else {
        setError(true);
        setInvalidCode(true);
        setSucces(false);
      }
    },
    onError(err) {
      console.log(`err`, err);
    }
  });
  const [sendOTP] = useMutation(SendOTP, {
    onCompleted() {
      setDisabledResent(false);
    },
    onError(err) {
      setDisabledResent(false);
      console.log('err', err);
    }
  });
  const {state} = useLocation();

  const handleSubmit = () => {
    validateOtp({
      variables: {
        input: {
          otp: parseInt(code),
          email: state?.email
        }
      }
    });
  };

  const resendOTP = () => {
    setDisabledResent(true);
    sendOTP({variables: {input: {email: state.email}}});
  };

  return (
    <VerifyEmailWrapper>
      <BackArrow className="back-arrow" onClick={() => setStep('start')} />
      <VerifyEmailTitle>
        <h2>Verify Email</h2>
        {!succes && (
          <h3>
            A new OTP code has been sent to your email at <strong>{state?.email}</strong>.
          </h3>
        )}
        <p>Please Enter OTP</p>
      </VerifyEmailTitle>
      <VerifyEmailForm error={error}>
        {/* <FormTemplate fields={fields} submitBtn="Verify" callback={handleSubmit} /> */}
        <OTP value={code} onChange={setCode} />
        <Btn onClick={handleSubmit} kind="primary-small" loading={loading} disabled={code?.length !== 6}>
          Confirm
        </Btn>
        {invalidCode && <span className="error-message">Invalid Code</span>}
      </VerifyEmailForm>
      <div className="link">
        <Resend onClick={resendOTP}>{disabledResent ? <LoadingDots /> : 'Resend OTP'}</Resend>
      </div>
    </VerifyEmailWrapper>
  );
};

VerifyEmail.propTypes = {
  setStep: func.isRequired
};

export default VerifyEmail;
