import Btn from 'components/Btn';
import styled from 'styled-components/macro';

export const ReviewOrderWrapper = styled.div`
  width: 70%;

  .green {
    color: green;
  }
  .price {
    white-space: nowrap;
    font-style: normal;
    font-size: 14px;
    line-height: 140%;
    color: inherit;
    & > small {
      font-size: 10px;
      line-height: 140%;
      text-transform: uppercase;
      margin-right: 2px;
    }
  }
  .order-wrapper {
    min-width: 680px;
    margin-right: 40px;
    .other-tab {
      margin: 0 0 24px;
      border: 1px solid #e4e4e4;
      box-sizing: border-box;
      border-radius: 3px;
      display: flex;
      align-items: center;
      padding: 11px 24px;
      font-family: Helvetica;
      font-style: normal;
      font-weight: normal;
      font-size: 14px;
      line-height: 132%;
      letter-spacing: -0.024em;
      color: #545454;
      & .title {
        margin-bottom: 0;
        margin-right: 10px;
        font-size: 18px;
        color: #000000;
        flex-shrink: 0;
      }
      .description {
        font-family: SF Pro Display;
        font-size: 14px;
        line-height: 140%;
        color: #545454;
        position: relative;
        top: 2px;
      }
      .edit-icon {
        margin-left: auto;
      }
    }
    .orders {
      border: 1px solid #e4e4e4;
      box-sizing: border-box;
      margin-top: 24px;
      margin-bottom: 24px;
      }
    }
    .order {
      & .order-title {
        font-size: 18px;
        margin: 0;
        box-sizing: border-box;
        padding: 12px 24px;
        border-bottom: 1px solid #e4e4e4;
      }
      .products-list {
        & .product-title {
          display: flex;
          margin: 16px 24px;
          font-family: Helvetica Neue;
          font-style: normal;
          font-weight: bold;
          font-size: 16px;
          line-height: 20px;
          color: #000000;
          img {
            width: 60px;
            height: 60px;
            display: block;
            border: 1px solid #e4e4e4;
            border-radius: 50%;
            object-fit: cover;
            margin-right: 12px;
          }
          h3 {
            font-weight: bold;
            font-size: 16px;
            color: #000000;
          }
          .info {
            display: flex;
            align-items: center;
            .rating {
              font-family: SF Pro Display;
              font-size: 12px;
              color: #7a7a7a;
            }
            ul {
              font-size: 12px;
              margin: 0 9px 0 7px;
              li {
                margin-right: 4px;
              }
            }
            .followers {
              font-family: SF Pro Display;
              font-weight: normal;
              font-size: 12px;
              color: #7a7a7a;
            }
          }
        }
        .list-item {
          .item {
            border-top: 1px solid #e4e4e4;
            display: flex;
            margin: 0 24px 0;
            padding: 16px 0;
            & > img {
              height: 96px;
              width: 96px;
              object-fit: contain;
            }
            & > .descrition {
              min-width: 0;
              flex-grow: 1;
              display: flex;
              flex-direction: column;
              margin-left: 10px;
              font-family: Helvetica;
              font-style: normal;
              font-weight: normal;
              line-height: 140%;
              color: #000000;
              & > h3 {
                font-size: 16px;
                margin: 5px 0 10px;
                overflow: hidden;
                text-overflow: ellipsis;
                white-space: nowrap;
              }
              .characteristic {
                flex-grow: 1;
                align-items: center;
                display: flex;
                font-family: Helvetica Neue;
                span span {
                  vertical-align: middle;
                  &:last-child {
                    color: #a7a7a7;
                  }
                }
                .ant-divider {
                  margin: 0 12px;
                }
              }
            }
            .price {
              font-family: Helvetica Neue;
              font-style: normal;
              font-size: 16px;
              line-height: 140%;
              letter-spacing: -0.016em;
              color: #000000;
              small {
                font-size: 10px;
                color: #8f8f8f;
              }
              strong {
                font-weight: normal;
                font-size: 16px;
                margin: 0 4px 0 2px;
              }
              small:last-child span {
                text-decoration-line: line-through;
                span {
                  font-size: 12px;
                }
              }
            }
          }
          .influencer-form {
            display: flex;
            padding-left: 24px;
            padding-bottom: 13px;
            min-height: 40px;
            align-items: center;

            .ant-checkbox-wrapper {
            }
            form {
              display: flex;

              button {
                border-color: #000;
                color: #000;
              }

              .ant-form-item-control {
                max-height: 40px;
                line-height: inherit;

                &:before,
                &:after {
                  display: none;
                }
              }

              .ant-form-item-control-wrapper {
              }
              .ant-form-item {
                width: 160px;
                height: 23px;
                color: #999999;
                display: flex;
                align-items: center;
                margin-bottom: 0;
                margin-right: 24px;
                margin-left: 17px;
                .ant-input[disabled] {
                  background-color: inherit;
                }
                .ant-form-explain {
                  font-size: 10px;
                  line-height: 12px;
                  color: #ee121d;
                  min-height: 10px;
                  height: 0;
                  position: absolute;
                  top: calc(100% + 3px);
                }
                input {
                  width: 160px;
                  height: 21px;
                  padding: 0 30px 2px 0;
                  border-top: none;
                  border-left: none;
                  border-right: none;
                  box-shadow: none;
                  border-radius: 0;
                  border-top: 1px solid transparent;
                  border-bottom: 1px solid #c3c3c3;
                }
                .suffix-icon {
                  width: 16px;
                }
                .load-dots {
                  margin: 5px 0;
                  height: 15px;
                }
              }
            }
            .discount-message {
              font-family: SF Pro Display;
              font-size: 12px;
              line-height: 132%;
              color: #ed494f;
              position: relative;
              bottom: -5px;
            }
          }
          .shipping {
            background: #f7f7f7;
            box-shadow: 0px 5px 11px rgba(0, 0, 0, 0.08);
            padding: 9.5px 23px;
            display: flex;
            align-items: center;
            justify-content: space-between;

            .ant-select {
              margin-right: auto;
              position: relative;
              top: 2px;
            }

            .ant-select-selection__placeholder {
              text-align: right;
            }

            .ant-form-item {
              margin: 0;
            }
            .shipping-title {
              h3 {
                margin: 0;
                font-weight: bold;
                font-size: 14px;
                color: #000000;
              }
              span {
                color: #ed484f;
              }
            }
            .ant-select-selection {
              border: none;
              background: inherit;
              min-width: 300px;
              box-shadow: none;
              .ant-select-selection-selected-value {
                color: #000;
              }
            }
          }
          .shipping .price {
            margin-left: auto;
          }
          .total {
            display: flex;
            flex-direction: column;
            align-items: flex-end;
            padding: 16px 24px;
            border-bottom: 1px solid #e4e4e4;
            font-family: Helvetica Neue;
            font-style: normal;
            font-size: 14px;
            line-height: 140%;
            color: #000000;
            & > div {
              margin: 6px 0;
            }
            & > .coins {
              color: #398287;
            }
            & > .total-price {
              font-weight: bold;
            }
          }
        }
      }
    }
  }
  .payment-button {
    color: #ed494f !important;
    width: 194px;
    height: 32px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin: 24px 24px 24px auto;
  }

  .shipping-options {
    padding: 24px 0 24px 71px;
    margin: 0 24px;
    border-bottom: 1px solid #e4e4e4;

    &__field {
      display: flex;
      align-items: center;

      &:not(:last-child) {
        margin-bottom: 16px;
      }

      input {
        display: none;

        &:checked ~ {
          .icon {
            border: 1.5px solid #4190f7;

            &:before {
              opacity: 1;
            }
          }
        }
      }

      .icon {
        width: 20px;
        height: 20px;
        border: 1.5px solid #d9d9d9;
        border-radius: 50%;
        margin-right: 15px;
        position: relative;
        display: flex;
        align-items: center;
        justify-content: center;
        transition: all 0.3s ease;

        &:before {
          content: '';
          width: 10px;
          height: 10px;
          background: #4190f7;
          border-radius: 50%;
          transition: all 0.3s ease;
          opacity: 0;
        }
      }

      .text {
        font-size: 14px;
        line-height: 140%;
        color: #000000;
      }

      .price {
        margin-left: 35px;
        font-weight: normal;
        font-size: 14px;
        line-height: 1;
        margin-top: 2px;
        color: #000000;
        display: inline-flex;
        align-items: center;

        small {
          font-weight: normal;
          font-size: 10px;
          line-height: 1;
          text-align: right;
          color: #000000;
        }
      }
    }
  }
`;

export const Continue = styled(Btn)`
  margin: 24px 24px 24px auto;
  display: flex;
`;
