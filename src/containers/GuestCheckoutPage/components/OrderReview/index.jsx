import React, {useRef, useEffect} from 'react';
import {func, string, node, objectOf, any} from 'prop-types';
import {Button} from 'antd';

import {ReviewOrderWrapper, Continue} from './styled';
import {ReactComponent as EditIcon} from 'containers/GuestCheckoutPage/assets/icon/edit.svg';
import BrandProducts from 'containers/GuestCheckoutPage/components/BrandProducts';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import {useMutation} from '@apollo/client';
import {SET_SUBCART_SHIPPING_METHOD} from 'mutations';
import {formatCartsData} from 'util/heplers';

const OrderReview = ({setStep, influencer, setInfluencer, cart, cartData = {}, selectedAddress = {}}) => {
  const [setMethod, {loading: loadingMethod}] = useMutation(SET_SUBCART_SHIPPING_METHOD);

  const toPayment = () => {
    setStep('payment');
  };

  const cartsData = formatCartsData(cartData);

  return (
    <ReviewOrderWrapper>
      <div className="order-wrapper">
        <div className="other-tab">
          <h3 className="title">1. Shipping Address</h3>
          <span>
            {[
              selectedAddress?.postCode,
              selectedAddress?.build,
              selectedAddress?.address1,
              selectedAddress?.city,
              selectedAddress?.district,
              getCountryFromCode(selectedAddress?.country)
            ]
              .filter((item) => !!item)
              .join(', ')}
            {!!selectedAddress?.addressNickname && ' | '}
            {selectedAddress?.addressNickname}
          </span>
          <Button type="link" shape="circle" className="edit-icon">
            <EditIcon onClick={() => setStep('details')} />
          </Button>
        </div>
        <div className="orders">
          <div className="order">
            <h2 className="order-title">2. Review Order</h2>
            <div className="products-list">
              {cartsData?.map((item, index) => (
                <BrandProducts
                  cartData={cartData}
                  key={item?.store?.id}
                  idx={item?.store?.id}
                  influencer={influencer}
                  setInfluencer={setInfluencer}
                  cart={cart}
                  subCartData={item}
                  subCartIndex={index}
                  setMethod={setMethod}
                />
              ))}
            </div>
          </div>

          <Continue kind="continue" loading={loadingMethod} onClick={toPayment}>
            Proceed to Payment
          </Continue>
        </div>
        <div className="other-tab">
          <h3 className="title">3. Payment</h3>
        </div>
      </div>
    </ReviewOrderWrapper>
  );
};

// OrderReview.propTypes = {
//   setStep: func.isRequired,
//   setEditLocation: func.isRequired,
//   setCart: func.isRequired,
//   shopCoins: string.isRequired,
//   shippingAdress: node.isRequired,
//   checkShipping: func.isRequired,
//   influencer: objectOf(any).isRequired,
//   setInfluencer: func.isRequired,
//   cart: objectOf(any).isRequired
// };

export default OrderReview;
