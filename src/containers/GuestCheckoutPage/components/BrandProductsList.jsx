import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Select, Rate} from 'antd';

import ShippingSelect from './Select';
import Product from './Product';

import {ErrorDelivery} from '../styled';

const {Option} = Select;

const BrandProducts = ({
  setCart,
  influencer,
  setInfluencer,
  idx,
  shopCoins,
  cart,
  brand: {brand, rating, followers, currency, products}
}) => {
  const [influCode, setInlucode] = useState('');
  const [totalPrice, setTotalPrice] = useState(0);
  const [shipping, setShipping] = useState(null);

  const changeShipping = (index, price) => {
    setShipping(price);
    setCart((prev) => {
      const i = {...prev};
      i.shipping[index] = price;
      i.shippingCharges = true;
      i.error = false;
      return i;
    });
  };

  const defaultValue = cart.shipping[idx] !== 0 && cart.shipping[idx] ? cart.shipping[idx] : null;
  const coins = shopCoins;

  return (
    <div className="product">
      <div className="product-title">
        <h3>{brand}</h3>
        <div className="info">
          <span className="rating">{rating}</span> <Rate disabled defaultValue={rating} />{' '}
          <span className="followers">({followers}K)</span>
        </div>
      </div>
      <div className="list-item">
        {products.map((product, index) => {
          return (
            <Product
              key={index}
              influencer={influencer}
              setInfluencer={setInfluencer}
              setCart={setCart}
              _idx={index}
              idx={idx}
              product={product}
              setTotalPrice={setTotalPrice}
              currency={currency}
              setInlucode={setInlucode}
            />
          );
        })}
        <div className="shipping">
          <div className="shipping-title">
            <h3>
              Select Delivery Option <span>*</span>
            </h3>
          </div>

          {/* <ShippingSelect placeholder="Please Select" onChange={(value) => changeShipping(idx, value)}>
            <Option value={0}>
              <div className="shipping-option">
                <span style={{color: 'green'}}>Free</span> Shipping (10-20 days)
              </div>
            </Option>
            <Option value={10}>
              <div className="shipping-option">
                Express Shipping ( 8-10 days){' '}
                <span className="price">
                  <small>{currency}</small>
                  {(10).toFixed(3)}
                </span>
              </div>
            </Option>
            <Option value={15}>
              <div className="shipping-option">
                On Demand Delivery - 6 hours{' '}
                <span className="price">
                  <small>{currency}</small>
                  {(15).toFixed(3)}
                </span>
              </div>
            </Option>
            <Option value={25}>
              <div className="shipping-option">
                Seller Shipping (3-5 days){' '}
                <span className="price">
                  <small>{currency}</small>
                  {(25).toFixed(3)}
                </span>
              </div>
            </Option>
          </ShippingSelect> */}
        </div>

        <div className="shipping-options">
          <label htmlFor={`shipping-options-default_${brand}`} className="shipping-options__field">
            <input
              type="radio"
              id={`shipping-options-default_${brand}`}
              defaultChecked
              name={`shipping-option_${brand}`}
              onChange={() => {
                changeShipping(idx, 0);
              }}
            />
            <span className="icon" />
            <span className="text">
              <strong style={{color: '#26A95E'}}>Free</strong> Shipping, (10-20 days) Default
            </span>
            <span className="price">
              <small>BD</small>0.000
            </span>
          </label>

          <label htmlFor={`shipping-options-express_${brand}`} className="shipping-options__field">
            <input
              type="radio"
              id={`shipping-options-express_${brand}`}
              name={`shipping-option_${brand}`}
              onChange={() => {
                changeShipping(idx, 10);
              }}
            />
            <span className="icon" />
            <span className="text">Express Shipping, (8-10 days)</span>
            <span className="price">
              <small>BD</small>10.000
            </span>
          </label>

          <label htmlFor={`shipping-options-demand_${brand}`} className="shipping-options__field">
            <input
              type="radio"
              id={`shipping-options-demand_${brand}`}
              name={`shipping-option_${brand}`}
              onChange={() => {
                changeShipping(idx, 15);
              }}
            />
            <span className="icon" />
            <span className="text">On Demand Delivery, (0-6 hours)</span>
            <span className="price">
              <small>BD</small>15.000
            </span>
          </label>
        </div>

        {cart.error && cart.shipping && cart.shipping.length && cart.shipping[idx] === null ? (
          <ErrorDelivery>Please select delivery option to continue</ErrorDelivery>
        ) : null}

        <div className="total">
          <div className="items">
            Items:{' '}
            <span className="price">
              <small>{currency}</small>
              {`${totalPrice}`}
            </span>
          </div>
          <div className="items">
            {shipping > 0 ? 'Shipping Charges' : 'Estimated Shipping'}:{' '}
            <span className="price">
              <small>{currency}</small>
              {`${cart.shipping[idx] || 0}`}
            </span>
          </div>
          {influCode ? (
            <div className="items">
              Influencer Discount:{' '}
              <span className="price" style={{color: '#ED484F'}}>
                <small>{currency}</small>
                25.000
              </span>
            </div>
          ) : null}
          {/* <div className="coins">
            Shop*Coins:{' '}
            <span className="price">
              <small>{currency}</small>
              {shopCoins}
            </span>
          </div> */}
          <div className="total-price">
            {brand} Total:{' '}
            <span className="price">
              {/* <small>{currency}</small> */}
              {`${totalPrice}`}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

BrandProducts.propTypes = {
  setCart: PropTypes.func.isRequired,
  cart: PropTypes.objectOf(PropTypes.any).isRequired,
  influencer: PropTypes.objectOf(PropTypes.any).isRequired,
  setInfluencer: PropTypes.func.isRequired,
  idx: PropTypes.number.isRequired,
  shopCoins: PropTypes.string.isRequired,
  brand: PropTypes.objectOf(PropTypes.any).isRequired
};

export default BrandProducts;
