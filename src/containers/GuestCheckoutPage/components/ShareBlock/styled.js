import styled from 'styled-components';

export const ShareWrapper = styled.div`
  background-color: #ffffff;
  box-shadow: 0px 4px 20px rgba(0, 0, 0, 0.15);
  border-radius: 8px;
  width: 425px;
  margin: 40px auto;
  position: relative;

  i {
    position: absolute;
    left: -37px;
    top: 8px;
    cursor: pointer;
  }
`;

export const Title = styled.h3`
  font-weight: normal;
  font-size: 14px;
  line-height: 17px;
  color: #000000;
  margin: 12px -24px 16px;
  padding: 0 24px 24px;
  border-bottom: 1px solid #efefef;
`;

export const TopBlock = styled.div`
  padding: 12px 16px 22px;
`;

export const BottomBlock = styled.div`
  border-top: 1px solid #e4e4e4;
  padding: 9px 16px 24px;
  p {
    font-weight: normal;
    font-size: 12px;
    color: #464646;
  }
  input,
  input:hover,
  input:focus {
    border-color: #efefef !important;
  }
`;

export const ShareItemsBlock = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ShareItem = styled.div`
  text-align: center;
  p {
    font-weight: normal;
    font-size: 10px;
    line-height: 140%;
    text-align: center;
    color: #666666;
    margin: 0;
  }
`;

export const ShareIcon = styled.img`
  border-radius: 50%;
  width: 32px;
  height: 32px;
  margin-bottom: 10px;
  cursor: pointer;
`;
