import React, { useState } from 'react';
import {Input} from 'antd';
import { func } from 'prop-types';

import Icon from 'components/Icon';

import faceIcon from './imgs/face.png';
import linkIcon from './imgs/link.png';
import mailIcon from './imgs/mail.png';
import testSampleIcon from './imgs/testSample.png';
import twitterIcon from './imgs/twitter.png';
import whatsappIcon from './imgs/whatsapp.png';

import {followers as shareUserData} from 'constants/staticData';

import {ShareWrapper, Title, TopBlock, BottomBlock, ShareItemsBlock, ShareItem, ShareIcon} from './styled';
import {
  ActionsWrapper,
  AvatarStyled,
  Description,
  Follower,
  ShareButton, ShareUserList
} from "../../../MyProfile/components/List/styled";

const shareData = [
  {
    img: testSampleIcon,
    title: 'testSample'
  },
  {
    img: faceIcon,
    title: 'Facebook'
  },
  {
    img: whatsappIcon,
    title: 'WhatsApp'
  },
  {
    img: twitterIcon,
    title: 'Twitter'
  },
  {
    img: linkIcon,
    title: 'Copy Link'
  },
  {
    img: mailIcon,
    title: 'Email'
  }
];

const ShareBlock = ({setShare}) => {
  const [sharedUsers, setSharedUsers] = useState([]);
  const [users, setUsers] = useState([]);

  const shareWithUser = (username) => {
    if (!sharedUsers.includes(username)) {
      setSharedUsers((oldArray) => [...sharedUsers, username]);
    }
  };

  const handleSearch = ({target: {value}}) => {
    if (value.length) {
      const searchResult = shareUserData.filter(
        ({name}) => name.toLowerCase().indexOf(value.toLowerCase()) > -1 && name
      );
      setUsers(searchResult);
    } else {
      setUsers([]);
    }
  };

  return (
    <ShareWrapper>
      <TopBlock>
        <Title>Share Products</Title>
        <Icon type="back" svgStyle={{ fill: '#000' }} onClick={() => setShare(false)} />
        <ShareItemsBlock>
          {shareData.map((el, index) => (
            <ShareItem>
              <ShareIcon src={el.img} alt={el.title} />
              <p>{el.title}</p>
            </ShareItem>
          ))}
        </ShareItemsBlock>
      </TopBlock>
      <BottomBlock>
        <p>Or share with</p>
        <Input onChange={handleSearch} placeholder="Search for Username" />
      </BottomBlock>

      <ShareUserList>
        {users.map(({avatar, accountName, name}, i) => {
          const shared = sharedUsers.includes(accountName);
          return (
            <Follower key={i} styles={{marginRight: '16px'}}>
              <AvatarStyled icon="user" src={avatar} />
              <Description>
                <strong>{name}</strong>
                <small>{accountName}</small>
              </Description>
              <ActionsWrapper>
                <ShareButton isShared={shared} onClick={() => shareWithUser(accountName)}>
                  {shared ? 'Shared' : 'Share'}
                </ShareButton>
              </ActionsWrapper>
            </Follower>
          );
        })}
      </ShareUserList>
    </ShareWrapper>
  )
};

ShareBlock.propTypes = {
  setShare: func.isRequired
};

export default ShareBlock;
