import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';

export const Wrapper = styled.div`
  margin-top: 52px;
  text-align: center;
`;

export const FailMessage = styled.p`
  background: #f15960;
  border-radius: 4px;
  padding: 12px 35px;
  color: #fff;
  max-width: 600px;
  margin: 0 auto 63px;
`;

export const InfoMessage = styled.p`
  font-family: ${secondaryFont};
  max-width: 323px;
  color: #000000;
  margin: 0 auto 32px;
`;
