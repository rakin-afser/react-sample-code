import React, {useEffect} from 'react';
import Btn from 'components/Btn';
import {Wrapper, FailMessage, InfoMessage} from './styled';
import {useHistory} from 'react-router';

const Failed = () => {
  const {push, location} = useHistory();

  useEffect(() => {
    const timer = setTimeout(() => {
      push('/cart');
    }, 10000);

    return () => {
      clearTimeout(timer);
    };
  }, [push]);

  return (
    <Wrapper>
      <FailMessage>
        Sorry, but your card was {location?.state?.status || 'declined'} and order cancelled. Please try another debit
        or credit card to complete your purchase.
      </FailMessage>
      <InfoMessage>
        You will be redirected in 10 seconds to cart page automatically, if not please click on the button below.
      </InfoMessage>
      <Btn kind="back-lg" onClick={() => push('/cart')}>
        Back to Cart
      </Btn>
    </Wrapper>
  );
};

export default Failed;
