import React, {useState} from 'react';
import {Form, Input, Select, Button} from 'antd';
import {useMutation} from '@apollo/client';

import {StepTitleActive, LocationWrapper, StyledFormItem, MobileInput, InputNumber} from '../styled';
import {ReactComponent as LocationIcon} from '../assets/icon/location.svg';
import {ReactComponent as TargetIcon} from '../assets/icon/target.svg';
import {ReactComponent as SearchIcon} from '../assets/icon/search.svg';
import SubmitButton from './Button';
import StyledSelect from './Select';
import Map from 'components/Map';
import exampleData from '../assets/data/exampleData';
import cities from '../assets/data/cities';
import {ADD_SHIPPING_ADDRESS} from 'mutations';
import {getCountryFromCode} from 'containers/GuestCheckoutPage/utils';
import {useUser} from 'hooks/reactiveVars';
import {ShippingAddressFragment} from 'fragments';
import {mec} from 'constants/address';

const {Option} = Select;
const {TextArea} = Input;
const {country} = exampleData;

const numberFieldRules = {
  pattern: /^\d+$/,
  message: 'Numbers only'
};

const Location = ({form, setStep, shippingAddressChanged = () => {}, initialValues = {}}) => {
  const [user] = useUser();
  const {getFieldDecorator, validateFields, setFields, getFieldValue} = form;
  const [center, setCenter] = useState({lat: 26.0667, lng: 50.5577});
  const [position, setPosition] = useState({lat: 26.0667, lng: 50.5577});
  const [formValues, setFormValues] = useState({});
  const [addAddress] = useMutation(ADD_SHIPPING_ADDRESS, {
    onCompleted() {
      setStep('details');
    }
  });

  const onSubmitHandler = (e) => {
    e.preventDefault();
    validateFields((err, values) => {
      const address = {
        addDirections: formValues?.addrAddDirections,
        address1: formValues?.addrAddress1,
        addressNickname: formValues?.addrAddressNickname,
        apartment: parseInt(formValues?.addrApartment),
        block: parseInt(formValues?.addrBlock),
        city: formValues?.addrCity,
        country: formValues?.addrCountry,
        district: formValues?.addrDistrict,
        firstName: formValues?.addrFirstName,
        floor: parseInt(formValues?.addrFloor),
        map: formValues?.addrMap,
        phone: formValues?.addrPhone,
        road: parseInt(formValues?.addrRoad),
        searchRoad: formValues?.addrSearchRoad,
        type: formValues?.addrType,
        build: formValues?.addrBuild,
        postCode: parseInt(values?.addrPostCode)
      };

      if (!err) {
        if (user) {
          addAddress({
            variables: {input: {...formValues}},
            update(cache, {data}) {
              cache.modify({
                fields: {
                  testSampleAddresses(existing) {
                    const shipping = data?.addShippingAddress?.shippingAddresses?.map((addr) =>
                      cache.writeFragment({
                        id: cache.identify(addr),
                        data: addr,
                        fragment: ShippingAddressFragment
                      })
                    );
                    if (shipping?.length === 1) {
                      shippingAddressChanged(address);
                    }
                    return {...existing, shipping};
                  }
                }
              });
            }
          });
        } else {
          shippingAddressChanged(address);
          localStorage.setItem('gAddress', JSON.stringify(address));
          setStep('details');
        }
      }
    });
  };

  const prefixModilePhone = (
    <div className="mobile-code">
      {country[getCountryFromCode(formValues?.addrCountry)]
        ? country[getCountryFromCode(formValues?.addrCountry)].dialCode
        : '+'}
    </div>
  );

  const onChangeForm = (fieldName, value) => {
    setFormValues({...formValues, [fieldName]: value});
  };

  function getCurrentPostition() {
    const options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };

    function success(pos) {
      const {coords} = pos || {};
      setPosition({lat: coords.latitude, lng: coords.longitude});
      setCenter({lat: coords.latitude, lng: coords.longitude});
    }

    function error(err) {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    navigator.geolocation.getCurrentPosition(success, error, options);
  }

  function renderConditionalFields() {
    const countryField = getFieldValue('addrCountry');
    const countryCode = country[countryField]?.code;

    if (mec?.includes(countryField) || !countryField) {
      return (
        <>
          <StyledFormItem label="District">
            {getFieldDecorator('addrDistrict', {
              rules: [{required: true, message: 'Enter a valid District'}],
              initialValue: initialValues?.district
            })(<Input placeholder="" onChange={(e) => onChangeForm('addrDistrict', e.target.value)} />)}
          </StyledFormItem>
          <StyledFormItem label="City">
            {getFieldDecorator('addrCity', {
              rules: [{required: true, message: 'Enter a valid City'}],
              initialValue: initialValues?.city
            })(
              <StyledSelect
                placeholder="Select Your City"
                showSearch
                allowClear
                onChange={(value) => onChangeForm('addrCity', value)}
                filterOption={(input, {props: {value}}) => value.toLowerCase().startsWith(input.toLowerCase())}
              >
                {cities[getCountryFromCode(countryCode)] &&
                  cities[getCountryFromCode(countryCode)].map((city, key) => (
                    <Option value={city} key={key}>
                      {city}
                    </Option>
                  ))}
              </StyledSelect>
            )}
          </StyledFormItem>

          <StyledFormItem label="Street / Building Name">
            {getFieldDecorator('addrAddress1', {
              rules: [{required: true, message: 'Enter a valid street or building name'}],
              initialValue: initialValues?.address1
            })(<Input placeholder="" onChange={(e) => onChangeForm('addrAddress1', e.target.value)} />)}
          </StyledFormItem>

          {/* <div className="grid-2">
              <StyledFormItem label="Address Type">
                {getFieldDecorator('addrType', {
                  rules: [{required: true, message: 'Select Address Type'}],
                  initialValue: initialValues?.type
                })(
                  <StyledSelect allowClear onChange={(value) => onChangeForm('addrType', value)}>
                    <Option value="Apartment">Apartment</Option>
                    <Option value="Office">Office</Option>
                  </StyledSelect>
                )}
              </StyledFormItem>

              <StyledFormItem label="House/Building. No.">
                {getFieldDecorator('addrBuild', {
                  rules: [{required: true, message: 'Enter a valid No.'}],
                  initialValue: initialValues?.build
                })(<Input placeholder="" onChange={(e) => onChangeForm('addrBuild', e.target.value)} />)}
              </StyledFormItem>
            </div> */}
          <div className="grid-3">
            <StyledFormItem label="Block No.">
              {getFieldDecorator('addrBlock', {
                rules: [{required: true, message: 'Required'}, numberFieldRules],
                initialValue: initialValues?.block
              })(<InputNumber placeholder="" onChange={(e) => onChangeForm('addrBlock', parseInt(e.target.value))} />)}
            </StyledFormItem>

            <StyledFormItem label="Road No.">
              {getFieldDecorator('addrRoad', {
                rules: [{required: true, message: 'Required'}, numberFieldRules],
                initialValue: initialValues?.road
              })(<InputNumber placeholder="" onChange={(e) => onChangeForm('addrRoad', parseInt(e.target.value))} />)}
            </StyledFormItem>

            {/* <StyledFormItem label="Floor No.">
                {getFieldDecorator('addrFloor', {
                  rules: [numberFieldRules],
                  initialValue: initialValues?.floor
                })(
                  <InputNumber placeholder="" onChange={(e) => onChangeForm('addrFloor', parseInt(e.target.value))} />
                )}
              </StyledFormItem> */}

            <StyledFormItem label="Apt./House No.">
              {getFieldDecorator('addrApartment', {
                rules: [numberFieldRules],
                initialValue: initialValues?.apartment
              })(<InputNumber placeholder="" onChange={(e) => onChangeForm('addrApartment', e.target.value)} />)}
            </StyledFormItem>
          </div>
        </>
      );
    }
    return (
      <>
        <StyledFormItem label="Address Line 1">
          {getFieldDecorator('addrAddress1', {
            rules: [{required: true, message: 'Required'}],
            initialValue: initialValues?.addrAddress1
          })(<Input placeholder="" onChange={(e) => onChangeForm('addrAddress1', e.target.value)} />)}
        </StyledFormItem>

        <StyledFormItem label="Address Line 2">
          {getFieldDecorator('addrAddress2', {
            rules: [],
            initialValue: initialValues?.addrAddress2
          })(<Input placeholder="" onChange={(e) => onChangeForm('addrAddress2', e.target.value)} />)}
        </StyledFormItem>

        <div className="grid-2">
          <StyledFormItem label="City">
            {getFieldDecorator('addrCity', {
              rules: [{required: true, message: 'Enter a valid City'}],
              initialValue: initialValues?.city
            })(
              <StyledSelect
                placeholder="Select Your City"
                showSearch
                allowClear
                onChange={(value) => onChangeForm('addrCity', value)}
                filterOption={(input, {props: {value}}) => value.toLowerCase().startsWith(input.toLowerCase())}
              >
                {cities[getCountryFromCode(countryCode)] &&
                  cities[getCountryFromCode(countryCode)].map((city, key) => (
                    <Option value={city} key={key}>
                      {city}
                    </Option>
                  ))}
              </StyledSelect>
            )}
          </StyledFormItem>
          <StyledFormItem label="Zip Code">
            {getFieldDecorator('addrPostCode', {
              rules: [{required: true, message: 'Required'}],
              initialValue: initialValues?.addrPostCode
            })(<Input placeholder="" onChange={(e) => onChangeForm('addrPostCode', parseInt(e.target.value))} />)}
          </StyledFormItem>
        </div>
      </>
    );
  }

  return (
    <LocationWrapper>
      <StepTitleActive>
        <h2>Shipping Address</h2>
      </StepTitleActive>
      <Form onSubmit={onSubmitHandler}>
        <div className="grid-columns">
          <div>
            <StyledFormItem label="Country">
              {getFieldDecorator('addrCountry', {
                rules: [{required: true, message: 'Enter a valid country'}],
                initialValue: initialValues?.country
              })(
                <StyledSelect
                  placeholder="Select Your Country"
                  showSearch
                  allowClear
                  onChange={(value) => onChangeForm('addrCountry', country[value]?.code)}
                  filterOption={(input, {props: {value}}) =>
                    country[value]?.name.toLowerCase().startsWith(input.toLowerCase())
                  }
                >
                  {Object.keys(country).map((key) => (
                    <Option value={key} key={key}>
                      {key}
                    </Option>
                  ))}
                </StyledSelect>
              )}
            </StyledFormItem>

            <StyledFormItem label="Mobile Number">
              {getFieldDecorator('addrPhone', {
                rules: [{required: true, message: 'Enter a valid phone number'}],
                initialValue: initialValues?.mobileNumber
              })(
                <MobileInput
                  prefix={prefixModilePhone}
                  style={{width: '100%'}}
                  onChange={(e) => onChangeForm('addrPhone', e.target.value)}
                />
              )}
            </StyledFormItem>

            <StyledFormItem label="Full Name (First and Last name)">
              {getFieldDecorator('addrFirstName', {
                rules: [{required: true, message: 'Enter a valid Full Name'}],
                initialValue: initialValues?.firstName
              })(<Input placeholder="" onChange={(e) => onChangeForm('addrFirstName', e.target.value)} />)}
            </StyledFormItem>

            {renderConditionalFields()}
            <StyledFormItem label="Address Nickname">
              {getFieldDecorator('addrAddressNickname', {initialValue: initialValues?.addressNickname})(
                <Input placeholder="" onChange={(e) => onChangeForm('addrAddressNickname', e.target.value)} />
              )}
            </StyledFormItem>
          </div>
          <div className="column">
            {/* <StyledFormItem label="Last Name">
              {getFieldDecorator('addrLastName', {
                rules: [{required: true, message: 'Enter a valid Last Name'}],
                initialValue: initialValues?.lastName
              })(<Input placeholder="" onChange={(e) => onChangeForm('addrLastName', e.target.value)} />)}
            </StyledFormItem> */}

            <div className="select-location">
              <h3>
                <LocationIcon />
                Select Location
              </h3>
              <span className="text">Please pin your address on Map, this will help us serve you better.</span>
              {/* <StyledFormItem label="">
                {getFieldDecorator('addrSearchRoad', {
                  rules: [],
                  initialValue: ''
                })(
                  <div>
                    <Input
                      placeholder="Search Road or Landmark"
                      suffix={<SearchIcon />}
                      onChange={(e) => onChangeForm('addrSearchRoad', e.target.value)}
                    />
                    <Button onClick={() => getCurrentPostition()} shape="circle">
                      <TargetIcon />
                    </Button>
                  </div>
                )}
              </StyledFormItem> */}
              <Map
                setFields={setFields}
                center={center}
                position={position}
                setPosition={setPosition}
                setCenter={setCenter}
              />
            </div>
            <StyledFormItem label="Additional Directions" className="additional-directions">
              {getFieldDecorator('addrAddDirections', {
                rules: []
              })(
                <TextArea
                  placeholder="Behind Al Osra"
                  autoSize={{minRows: 4, maxRows: 4}}
                  onChange={(e) => onChangeForm('addrAddDirections', e.target.value)}
                />
              )}
            </StyledFormItem>
          </div>
        </div>
        <div className="actions">
          <SubmitButton htmlType="submit" ghost rightIcon styles="font-weight: normal;line-height: 140%;" fill="#000">
            Save & Continue
          </SubmitButton>
        </div>
      </Form>
    </LocationWrapper>
  );
};

const WrappedLocation = Form.create({
  name: 'location',
  onFieldsChange(props, changedFields) {
    const {name, value} = changedFields[Object.keys(changedFields)[0]];
    props.setLocation((prev) => ({...prev, [name]: value}));
  }
})(Location);

export default WrappedLocation;
