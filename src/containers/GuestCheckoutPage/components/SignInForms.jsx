import React from 'react';

import {Title} from '../styled';
import ExistingCustomers from './ExistingCustomers';
import GuestCheckout from './GuestCheckout';

const SignInForm = (props) => {
  return (
    <>
      <Title>Please Sign In</Title>
      <div className="sign-in">
        <ExistingCustomers {...props} />
        <GuestCheckout {...props} />
      </div>
    </>
  );
};

export default SignInForm;
