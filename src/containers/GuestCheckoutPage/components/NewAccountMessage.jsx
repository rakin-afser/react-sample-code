import React from 'react';
import {useHistory} from 'react-router-dom';

import {Submit} from '../styled';

const NewAccountMessage = () => {
  const {push} = useHistory();

  const continueShopping = () => {
    push('/');
  };

  return (
    <div className="create-account-message">
      <h3 className="title">Thank you for creating account with testSample</h3>
      <Submit type="danger" ghost onClick={continueShopping}>
        Continue Shopping
      </Submit>
    </div>
  );
};

export default NewAccountMessage;
