import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import { func } from 'prop-types';
import { Input } from 'antd';
import { GoogleLogin } from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

import FormTemplate from './FormTemplate';
import { SignForm, BtnList, BtnItem, Or } from '../styled';
import { Login, VALIDATE_EMAIL } from 'mutations';
import { useMutation } from '@apollo/client';
import { useUser } from 'hooks/reactiveVars';
import Btn from 'components/Btn';
import { FB_APP_ID, GOOGLE_APP_ID, localStorageKey } from 'constants/config';
import useGlobal from 'store';

const fields = [
  {
    label: 'Email / Username',
    name: 'emailUsername',
    type: 'email',
    renderField: <Input allowClear />,
    rules: [{ required: true, message: 'Please enter a valid username or email address' }]
  },
  {
    label: 'Password',
    name: 'password',
    type: 'password',
    renderField: <Input.Password allowClear />,
    rules: [{ required: true, message: 'Please enter a valid password' }]
  }
];

const ExistingCustomers = ({ setStep,setEmail }) => {
  const [,globalActions] = useGlobal();
  const [loading, setLoading] = useState(false);
  const [validateEmailMutation] = useMutation(VALIDATE_EMAIL);
  const [login] = useMutation(Login, {
    onCompleted(data) {
      localStorage.setItem('userId', data.login.user.databaseId);
      localStorage.setItem('token', data.login.authToken);
      setUser(data?.login?.user);
      setStep('details');
      setLoading(false);
    }, 
    onError() {
      setLoading(false);
    }
  });
  const [, setUser] = useUser();

  const signIn = async (values, err) => {
    if (!err) {
      setLoading(true);
      login({ variables: { input: { username: values.emailUsername, password: values.password } } });
    }
  };
  const signInWithSocial = async (username, password, social_login, fb) => {
    try {
      validateEmail(username).then(async (exist) => {
        if (exist) {
          const {data, loading, error} = await login({
            variables: {input: {username, password: '', social_login: true}}
          });
          if (!error && !loading) {
            setUser(fb ? Object.assign({}, data.login.user, {avatar: {url: fb.avatar}}) : data.login.user);
            localStorage.setItem('avatar', fb.avatar);
            localStorage.setItem('userId', data.login.user.databaseId);
            localStorage.setItem('token', data.login.authToken);
            localStorage.setItem(localStorageKey.socialLogin, false);
            setStep('details');
        
          }
        } else {
          if(username){
            setEmail(username);
            localStorage.setItem('gEmail',username);
            setStep('details');
          }
        }
      });


    } catch (error) {
     console.log(error)
    }
  };
  

  const validateEmail = (email) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await validateEmailMutation({
          variables: {
            input: {
              email: email
            }
          }
        });
        const {
          data: {
            validateEmail: {code}
          }
        } = response;
        if (code == 200) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (e) {
        reject(e);
      }
    });
  };

  return (
    <SignForm border>
      <h2>Existing Customers</h2>
      <FormTemplate fields={fields} submitBtn="Sign In" callback={signIn} loading={loading} />
      <Link to="/forgot">Forgot Password?</Link>
      <Or mr />
      <BtnList mr>
        <GoogleLogin
          clientId={GOOGLE_APP_ID}
          render={(renderProps) => (
            <BtnItem  onClick={renderProps.onClick} id="googleExiting">
              <Btn kind="googleAuth" />
            </BtnItem>
          )}
          onSuccess={(response) => {

                  const {email, familyName, givenName, imageUrl} = response.profileObj;
                  const formatedResponse = {
                    email: email,
                    firstName: givenName,
                    lastName: familyName,
                    avatar: imageUrl
                  };
                  globalActions.setSignUpInfo(formatedResponse);
                  signInWithSocial(email, '', true, formatedResponse).then();
            
          }}
          onFailure={(err) => {
            console.log(err) 
          }}
          cookiePolicy={'single_host_origin'}
          
          />
        <FacebookLogin
                appId={FB_APP_ID}
                autoLoad={false}
                key="existFb"
                fields="name,email,picture"
                callback={(response) => {
                  if (response && response.email) {
                    const name = response.name.split(' ');
                    const formattedResponse = {
                      email: response.email,
                      firstName: name[0],
                      lastName: name.length > 1 ? name[1] : ' ',
                      avatar: `https://graph.facebook.com/${response.id}/picture?width=160&height=160`
                    };
                    globalActions.setSignUpInfo(formattedResponse);
                    signInWithSocial(response.email, '', true, formattedResponse).then();
                  } else {
                    // setNotificationMessage({
                    //   text: 'Cancelled',
                    //   type: 'error'
                    // });
                    // setError(true);
                  }
                }}
                render={(renderProps) => (
                  <BtnItem onClick={renderProps.onClick} id="facebookExit">
                    <Btn kind="facebookAuth" />
                  </BtnItem>
                )}
                />
        
      </BtnList>
    </SignForm>
  );
};

ExistingCustomers.propTypes = {
  setStep: func.isRequired
};

export default ExistingCustomers;
