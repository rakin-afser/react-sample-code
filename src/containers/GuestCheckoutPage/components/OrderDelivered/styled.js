import styled, {css} from 'styled-components/macro';

export const Circle = styled.div`
  width: 25px;
  height: 25px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  border-radius: 50%;
  color: #a7a7a7;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #67dd99;
  color: #fff;
  margin-right: 11px;
`;

export const Text = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 140%;
  color: #000;

  ${({gray}) =>
    gray &&
    css`
      color: #a7a7a7;
    `}

  & span {
    color: #000;
  }
`;

export const OrderDeliveredWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 65px 0 15px;
`;

export const ReviewLink = styled(Text)`
  font-size: 12px;
  line-height: 140%;
  text-decoration-line: underline;
  color: #000;
  margin-top: 64px;
`;

export const IconWrapper = styled.div`
  display: flex;
  margin-bottom: 18px;
`;
