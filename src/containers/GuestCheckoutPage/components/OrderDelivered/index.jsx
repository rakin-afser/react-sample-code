import React from 'react';

import {Circle, OrderDeliveredWrapper, IconWrapper, Text, ReviewLink} from './styled';
import Icon from 'components/Icon';

const OrderDelivered = () => {
  return (
    <OrderDeliveredWrapper>
      <IconWrapper>
        <Circle>
          <Icon type="checkmark" color="#fff" />
        </Circle>
        <Text>Your order has been delivered</Text>
      </IconWrapper>
      <Text gray>
        Thank you for shopping with <span>testSample</span>
      </Text>
      <ReviewLink>Review & Unlock Coins</ReviewLink>
    </OrderDeliveredWrapper>
  );
};

export default OrderDelivered;
