import React, {useState} from 'react';

// import {CheckoutMobileWrapper} from './styled';

// import Icon from 'components/Icon';
// import Button from 'components/Button';
// import Divider from 'components/Divider';

import {
  Authorization,
  Header,
  Verify
  // Navigation,
  // ShippingAddress,
  // SelectLocation,
  // PreviewOrder,
  // Payment,
  // ThankYou
} from './components/mobile';

// import {Step, StepHeader, StepFooter, OrderItem} from './styled';
import {useHistory, useParams} from 'react-router';
import SecureCheckout from './components/mobile/SecureCheckout';
import {gEmail} from 'util/heplers';

const CheckoutMobile = () => {
  const {step} = useParams();
  const history = useHistory();
  const [email, setEmail] = useState(gEmail);
  // const [data, setData] = useState({
  //   guestEmail: 'null'
  // });
  // const [options, setOptions] = useState({
  //   showOrderSummary: true
  // });

  const setStep = (step, state) => {
    history.push(`/checkout/${step}`, state);
  };

  function renderCheckoutMobile() {
    switch (step) {
      case 'start':
        return (
          <>
            <Header />
            <Authorization setStep={setStep} setEmail={setEmail}/>
          </>
        );
      case 'verify':
        return (
          <>
            <Header />
            <Verify setStep={setStep} setEmail={setEmail} />
          </>
        );
      case 'details':
      case 'address':
      case 'review':
      case 'payment':
      case 'payment-handler':
      case 'failed':
      case 'success':
        return <SecureCheckout email={email} step={step} setStep={setStep} />;
      default:
        return null;
    }
  }

  return renderCheckoutMobile();
};

export default CheckoutMobile;
