import React, { useState } from 'react';

import { CheckoutMobileWrapper } from './styled';

import Icon from 'components/Icon';
import Button from 'components/Button';
import Divider from 'components/Divider';

import {
  ShippingAddress,
  Authorization,
  Header,
  Navigation,
  Verify,
  SelectLocation,
  PreviewOrder,
  Payment,
  ThankYou
} from './components/mobile';

import {
  Step,
  StepHeader,
  StepFooter,
  OrderItem
} from './styled';

const CheckoutMobile = () => {
  const [step, setStep] = useState('authorization'); // authorization, verify, shippingAddress, selectLocation, previewOrder, payment, thankYou
  const [data, setData] = useState({
    guestEmail: 'null'
  });
  const [options, setOptions] = useState({
    showOrderSummary: true
  });

  return <CheckoutMobileWrapper>
    {
      step === 'authorization' &&
        <>
          <Header/>
          <Authorization
            setStep={setStep}
            setData={setData}
          />
        </>
    }
    {
      step === 'verify' &&
        <>
          <Header/>
          <Verify
            data={data}
            setStep={setStep}
          />
        </>
    }
    {
      step !== 'authorization' && step !== 'verify' && <Navigation step={step} setStep={setStep}/>
    }
    {
      step === 'shippingAddress' &&
        <ShippingAddress
          step={step}
          setStep={setStep}
        />
    }
    {
      step === 'selectLocation' &&
        <SelectLocation
          step={step}
          setStep={setStep}
        />
    }
    {
      step === 'previewOrder' &&
        <PreviewOrder
          step={step}
          setStep={setStep}
        />
    }
    {
      step === 'payment' &&
        <Payment
          step={step}
          setStep={setStep}
        />
    }
    {
      step === 'thankYou' &&
        <ThankYou
          step={step}
          setStep={setStep}
        />
    }

    {
      (step === 'shippingAddress' || step === 'previewOrder' || step === 'payment') &&
        <>
          <Divider
            style={{
              height: 8,
              background: '#FAFAFA'
            }}
          />

          <Step mini>
            <StepHeader opened={options.showOrderSummary} mini onClick={() => setOptions({...options, showOrderSummary: !options.showOrderSummary})}>
              Order Summary
              <Icon
                type="arrow"
                color={'#000'}
              />
            </StepHeader>

            {
              options.showOrderSummary
                &&
                <>
                  <OrderItem>
                    <span>Items Total</span>
                    <span>
                      <small>BD</small>
                      <strong> 678.000</strong>
                    </span>
                  </OrderItem>

                  <OrderItem>
                    <span>Estimated Shipping</span>
                    <span>
                      <small>BD</small>
                      <strong> 56.000</strong>
                    </span>
                  </OrderItem>

                  <OrderItem total>
                    <span>Total To Pay</span>
                    <span>
                      <small>BD</small>
                      <strong> 448.000</strong>
                    </span>
                  </OrderItem>

                  <OrderItem coins>
                    <span>MidCoins </span>
                    <span>
                      <span>+98</span>
                      <Icon type="coins" />
                    </span>
                  </OrderItem>

                  <OrderItem coins>
                    <span>Shop Coins</span>
                    <span>
                      <span>+98</span>
                      <Icon type="coins" />
                    </span>
                  </OrderItem>
                </>
            }

            <StepFooter>
              <Button
                disabled={step !== 'payment'}
                onClick={() => step === 'payment' ? setStep('thankYou') : null}
              >
                <span>Confirm & Pay</span>
              </Button>
            </StepFooter>
          </Step>
        </>
    }
  </CheckoutMobileWrapper>
};

export default CheckoutMobile;