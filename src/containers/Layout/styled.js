import styled from 'styled-components';
import media from 'constants/media';
import {mobileHeaderHeight} from 'components/Header/constants';

export const Wrapper = styled.div`
  max-width: 100vw;
  /* overflow: hidden; */
  @media (max-width: ${media.mobileMax}) {
    ${({styles}) => `padding-bottom: ${styles.paddingBottom ? styles.paddingBottom + 'px' : 0}`}
    ${({showBottomNav}) => showBottomNav && `padding-bottom: 54px;`}
    ${({styles, hideHeader}) =>
      `padding-top: ${
        styles.paddingTop || styles.paddingTop === 0 ? styles.paddingTop + 'px' : hideHeader ? '0' : mobileHeaderHeight
      }
        `}
    min-height: 100%;
    width: 100%;
    display: block;
  }
`;
