import React, {useEffect} from 'react';
import {bool, string, object} from 'prop-types';
import {useWindowSize} from '@reach/window-size';
import {Helmet} from 'react-helmet';
import qs from 'qs';
import {useLocation} from 'react-router-dom';

import Header from 'components/Header';
import Footer from 'components/Footer';
import QuickView from 'components/QuickView';

import {Wrapper} from './styled';
import BottomNavigation from 'components/BottomNav';

// Popups
import ActionPopup from 'components/Actions/popup';
import AuthRequest from 'components/AuthRequest';
import BestFeatures from 'components/BestFeatures';
import Report from 'containers/Report/mobile';
import OptionsPopup from 'components/Modals/mobile/OptionsPopup';
import AddNewPostPopup from 'components/Modals/mobile/AddNewPostPopup';
import ProductVariations from 'components/Modals/mobile/ProductVariations';
import SizeGuide from 'components/Modals/mobile/SizeGuide';
import ProductAdded from 'components/Modals/mobile/ProductAdded';
import FilterModal from 'components/Modals/mobile/Filters';

import {product, followers as followersData} from 'constants/staticData';
import OrderDetails from 'components/Modals/mobile/OrderDetails';
import ReturnRequest from 'components/Modals/mobile/ReturnRequest';
import SellerContact from 'components/Modals/mobile/SellerContact';
import LeaveFeedback from 'components/Modals/mobile/LeaveFeedback';
import Feedback from 'components/Modals/mobile/Feedback';
import FollowersModal from 'containers/MyProfile/components/List/components/FollowersModal';
import ReportModal from 'components/Modals/Report';
import PostActions from 'components/Modals/PostActions';
import PostShare from 'components/Modals/PostShare';
import useGlobal from 'store';
// import ToTop from 'components/ToTop';
import FiltersDesktop from 'components/Modals/Filters';
import PostViewMobile from 'components/Modals/mobile/PostViewMobile';
import PostSingle from 'components/Modals/PostSingle';
import ogMetaImage from 'images/og-image.png';
import CookiesConsent from 'components/CookiesConsent';
import ProductPopup from 'components/Trends/mobile/components/ProductPopup';

const Layout = ({
  children,
  hideHeader,
  hideFooter,
  hideHeaderLinks,
  isFooterShort,
  showBurgerButton,
  showRingButton,
  showSearchButton,
  showBasketButton,
  showBackButton,
  hideCopyright,
  title,
  layOutStyles,
  stickyHeader,
  headerShadow,
  showBottomNav,
  sellerHeader,
  customRightIcon
}) => {
  const [globalState, setGlobalState] = useGlobal();
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const {search} = useLocation();
  const {modal} = qs.parse(search, {ignoreQueryPrefix: true}) || {};

  useEffect(() => {
    if (modal?.type === 'product' && !globalState.quickView) {
      setGlobalState.setQuickView({quickViewType: 'product', ...modal?.data});
    }
  }, []);

  return (
    <Wrapper showBottomNav={isMobile && showBottomNav} hideHeader={hideHeader} styles={layOutStyles}>
      <Helmet>
        <meta charSet="utf-8" />
        <title>testSample - Shop and Earn</title>
        <meta
          name="description"
          content="Now turn your social posts into shoppable feed and earn real money. Get started now!"
        />

        {/* Facebook Meta Tags */}
        <meta property="og:type" content="website" />
        <meta property="og:title" content="testSample - Shop and Earn" />
        <meta
          property="og:description"
          content="Now turn your social posts into shoppable feed and earn real money. Get started now!"
        />
        <meta property="og:image" content={ogMetaImage} />

        {/* Twitter Meta Tags */}
        <meta property="twitter:type" content="website" />
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:domain" content="" />
        <meta property="twitter:title" content="testSample - Shop and Earn" />
        <meta
          property="twitter:description"
          content="Now turn your social posts into shoppable feed and earn real money. Get started now!"
        />
        <meta property="twitter:image" content={ogMetaImage} />
      </Helmet>

      {!hideHeader && (
        <Header
          isMobile={isMobile}
          hideLinks={hideHeaderLinks}
          showBurgerButton={showBurgerButton}
          showRingButton={showRingButton}
          showSearchButton={showSearchButton}
          showBasketButton={showBasketButton}
          showBackButton={showBackButton}
          sticky={stickyHeader}
          title={title}
          shadow={headerShadow}
          sellerHeader={sellerHeader}
          customRightIcon={customRightIcon}
        />
      )}
      {showBottomNav && <BottomNavigation />}

      {children}

      {!hideFooter && <Footer isMobile={isMobile} isFooterShort={isFooterShort} hideCopyright={hideCopyright} />}

      <QuickView />
      <BestFeatures />
      {/** CookiesConsent should be disabled for now */}
      {/* <CookiesConsent /> */}
      {!isMobile && (
        <>
          <FollowersModal isOpen={globalState.followersModal} onClose={() => setGlobalState.setFollowersModal(false)} />
          <PostActions
            isOpen={globalState.postActionsModal}
            onClose={() => setGlobalState.setPostActions(false)}
            {...globalState.postActionsProps}
          />
          <PostShare
            isOpen={globalState.postShareModal}
            onClose={() => setGlobalState.setPostShare(false)}
            {...globalState.postShareProps}
          />
          <ReportModal
            isOpen={globalState.reportModal}
            onClose={() => setGlobalState.setReportModal(false)}
            {...globalState.reportModalProps}
          />
          <FiltersDesktop />
          {modal?.type === 'postView' && <PostSingle data={modal?.data} />}
        </>
      )}

      {/* Mobile Popups */}
      {isMobile && (
        <>
          <ProductPopup />
          <FilterModal
            isOpen={globalState.filterModal}
            onClose={() => setGlobalState.setFilterModal(false)}
            {...globalState.filterModalProps}
          />
          <OrderDetails />
          <AuthRequest />
          <Report />
          <ActionPopup />
          <OptionsPopup />
          <ProductVariations sizes={product.sizes} colors={product.colors} />
          <SizeGuide />
          <ProductAdded />
          <ReturnRequest />
          <SellerContact />
          <LeaveFeedback />
          <Feedback />
          {/* <ToTop /> */}
          {modal?.type === 'postView' && <PostViewMobile data={modal?.data} />}
        </>
      )}
    </Wrapper>
  );
};

Layout.defaultProps = {
  hideHeader: false,
  hideFooter: false,
  isFooterShort: false,
  hideHeaderLinks: false,
  showBurgerButton: true,
  showRingButton: false,
  showSearchButton: true,
  showBackButton: false,
  showBasketButton: true,
  title: '',
  hideCopyright: false,
  layOutStyles: {},
  stickyHeader: true,
  headerShadow: true
};

Layout.propTypes = {
  hideHeader: bool,
  hideFooter: bool,
  isFooterShort: bool,
  hideHeaderLinks: bool,
  showBurgerButton: bool,
  showRingButton: bool,
  showSearchButton: bool,
  showBackButton: bool,
  showBasketButton: bool,
  title: string,
  hideCopyright: bool,
  layOutStyles: object,
  stickyHeader: bool,
  headerShadow: bool
};

export default Layout;
