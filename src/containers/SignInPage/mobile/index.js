import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import Logo from 'assets/LogoIcon';
import Layout from '../../Layout';
import AuthHeading from 'components/UserAuth/components/Heading';
import AuthTint from 'components/UserAuth/components/Tint';
import CreateFields from 'components/UserAuth/CreateFields';
import {signInData, fakeBackendSignInErrors} from '../data';
import ForgotBox from 'components/UserAuth/components/ForgotBox';
import ButtonCustom from 'components/ButtonCustom';
import SeparatorLineAndText from 'components/UserAuth/components/SeparatorLineAndText';
import Social from 'components/UserAuth/Social';
import FormMessage from 'components/FormMessage';
import {SignForm, Content, LogoWrapper} from './styled';

const SignInPageMobile = ({form}) => {
  const {push} = useHistory();
  const {validateFields} = form;
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const [locked, setLocked] = useState(false);

  const onSubmitHandler = (e) => {
    e.preventDefault();
    setLocked(false);
    validateFields((err, values) => {
      if (err) {
        setError(true);
      } else {
        setIsLoading(true);
        setError(false);
        setErrorMessage(null);

        setTimeout(() => {
          setIsLoading(false);
          const response = {
            error: fakeBackendSignInErrors.invalidCredentials
          };
          // setLocked(true);
          setErrorMessage(response.error);
          // push('/');
        }, 1500);
      }
    });
  };

  return (
    <Layout
      hideFooter
      hideHeader
      layOutStyles={{
        paddingTop: 25,
        paddingBottom: 20
      }}
    >
      <LogoWrapper>
        <Logo />
      </LogoWrapper>
      <AuthHeading title="Sign in to testSample" onClickClose={() => push('/')} />
      <Content>{errorMessage && <FormMessage styles={{marginBottom: 27}} message={errorMessage} />}</Content>
      <AuthTint isSingIn={true} />

      <SignForm onSubmit={onSubmitHandler}>
        <Content>
          {CreateFields(form, signInData)}
          <ForgotBox />
          <ButtonCustom
            isLoading={isLoading}
            title="Sign in"
            variant="contained"
            color="mainWhiteColor"
            iconRight="chevronRight"
            disabled={locked}
            styles={{margin: '24px auto 0 auto'}}
          />
        </Content>
        <SeparatorLineAndText />
        <Content>
          <Social disabled={locked} />
        </Content>
      </SignForm>
    </Layout>
  );
};

const WrappedFormTemplate = SignForm.create({name: 'auth'})(SignInPageMobile);

export default WrappedFormTemplate;
