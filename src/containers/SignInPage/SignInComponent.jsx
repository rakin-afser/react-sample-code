import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {Formik} from 'formik';
import {Helmet} from 'react-helmet';
import AuthHeading from 'components/UserAuth/components/Heading';
import AuthTint from 'components/UserAuth/components/Tint';
import {SignForm, Content, Wrapper, FullWidthFormField} from './styled';
import ForgotBox from 'components/UserAuth/components/ForgotBox';
import ButtonCustom from 'components/ButtonCustom';
import SeparatorLineAndText from 'components/UserAuth/components/SeparatorLineAndText';
import FormMessage from 'components/FormMessage';
import {getErrorByCode} from 'util/responseDecoding';
import LogoIcon from 'assets/LogoIcon';
import SocialButton from 'components/UserAuth/components/SocialButton';
import useGlobal from 'store';
import Input from 'components/Input';
import {useApolloClient, useMutation} from '@apollo/client';
import {Login, VALIDATE_EMAIL} from 'mutations';
import {GoogleApiProvider} from 'react-gapi';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import {FB_APP_ID, GOOGLE_APP_ID, localStorageKey} from 'constants/config';
import {useUser} from 'hooks/reactiveVars';
import {GET_INTERESTS} from 'queries';
import {GoogleLogin} from 'react-google-login';
import {getSelectedInterests} from 'util/heplers';
const SignInComponent = ({isMobile, onClickClose}) => {
  const [global, globalActions] = useGlobal();
  const {push} = useHistory();
  const client = useApolloClient();
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(false);
  const [notificationMessage, setNotificationMessage] = useState();
  const [locked, setLocked] = useState(false);
  const [formValid, setFormValid] = useState(false);
  const [login] = useMutation(Login);
  const [validateEmailMutation] = useMutation(VALIDATE_EMAIL);
  const [user, setUser] = useUser();
  const [fbResponse, setFbResponse] = useState(null);

  const signIn = async (username, password, social_login, fb) => {
    setLocked(false);
    setIsLoading(true);
    if (!social_login) {
      try {
        const {data, error, loading} = await login({variables: {input: {username, password}}});
        if (!error && !loading) {
          const {wooSessionToken} = data?.login || {};
          if (wooSessionToken) localStorage.setItem('woo-session', wooSessionToken);
          setUser(data.login.user);
          localStorage.removeItem('gEmail');
          localStorage.removeItem('gAddress');
          localStorage.setItem('userId', data.login.user.databaseId);
          localStorage.setItem('token', data.login.authToken);
          localStorage.setItem(localStorageKey.socialLogin, false);
          const isSeller = data?.login?.user?.capabilities?.includes('seller');
          await userInterestCheck(isSeller);
          client.resetStore();
        }
        return;
      } catch (e) {
        setNotificationMessage({
          text: 'Invalid Email OR Password',
          type: 'error'
        });
        setIsLoading(false);
        return;
      }
    }
    try {
      validateEmail(username).then(async (exist) => {
        if (exist) {
          const {data, loading, error} = await login({
            variables: {input: {username, password: '', social_login: true}}
          });
          if (!error && !loading) {
            setUser(fb ? Object.assign({}, data.login.user, {avatar: {url: fb.avatar}}) : data.login.user);
            localStorage.setItem('avatar', fb.avatar);
            localStorage.setItem('userId', data.login.user.databaseId);
            localStorage.setItem('token', data.login.authToken);
            localStorage.setItem(localStorageKey.socialLogin, false);
            const isSeller = data?.login?.user?.capabilities?.includes('seller');
            await userInterestCheck(isSeller);
            client.resetStore();
            // setUser(data.login.user);
          }
        } else {
          push('/auth/sign-up');
        }
      });
    } catch (error) {
      setNotificationMessage(getErrorByCode(error.code));
      setIsLoading(false);
    }
  };
  const userInterestCheck = (isSeller) => {
    return new Promise((resolve, reject) => {
      client
        .query({
          query: GET_INTERESTS
        })
        .then((res) => {
          setIsLoading(false);
          setError(false);
          if (!isSeller && getSelectedInterests(res.data) < 3) {
            push('/user-onboarding');
          } else {
            push('/');
          }

          return resolve();
        })
        .catch((error) => {
          console.log('Error interest query ', error);
          push('/');
        });
    });
  };

  const validateEmail = (email) => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await validateEmailMutation({
          variables: {
            input: {
              email: email
            }
          }
        });
        const {
          data: {
            validateEmail: {code}
          }
        } = response;
        if (code == 200) {
          resolve(true);
        } else {
          resolve(false);
        }
      } catch (e) {
        reject(e);
      }
    });
  };

  return (
    <Wrapper>
      <Helmet>
        <title>testSample - Sign in</title>
        <meta
          name="description"
          content="Signing in is a pain, testSample makes it easy. We are making signing as easy as possible with our simple UI that allows you to login with Facebook or Google"
        />
      </Helmet>
      {isMobile && (
        <Content>
          <LogoIcon
            styles={{
              width: 119,
              height: 33,
              marginTop: 24
            }}
          />
        </Content>
      )}

      <AuthHeading isMobile={isMobile} title="Sign in to testSample" onClickClose={onClickClose} />
      <Content>
        {notificationMessage && <FormMessage styles={{marginBottom: 27}} message={notificationMessage} />}
      </Content>
      <AuthTint isSingIn={true} />

      <Formik
        initialValues={{
          EmailorUsername: '',
          Password: ''
        }}
        validate={(values) => {
          const errors = {};
          if (!values.Password) errors.Password = 'Please enter a valid password';
          if (!values.EmailorUsername) errors.EmailorUsername = 'Please enter a valid username or email address';
          setFormValid(!(!values.Password || !values.EmailorUsername));
          return errors;
        }}
        onSubmit={(values) => {
          signIn(values.EmailorUsername, values.Password).then();
        }}
      >
        {({values, errors, handleChange, handleSubmit, setFieldValue, touched, handleBlur}) => (
          <SignForm onSubmit={handleSubmit} noValidate>
            <Content>
              <FullWidthFormField>
                <Input
                  required
                  key="EmailorUsername"
                  type="text"
                  name="EmailorUsername"
                  id="email"
                  label="Email or Username"
                  placeholder="Enter your email or username"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  error={errors.EmailorUsername && touched.EmailorUsername ? errors.EmailorUsername : ''}
                  value={values.EmailorUsername}
                  reset={values.EmailorUsername.length > 0}
                  resetCallback={() => setFieldValue('EmailorUsername', '')}
                />
              </FullWidthFormField>

              <FullWidthFormField>
                <Input
                  required
                  key="Password"
                  type="password"
                  name="Password"
                  id="password"
                  label="Password"
                  placeholder="Enter your password"
                  onChange={handleChange}
                  autoComplete="on"
                  onBlur={handleBlur}
                  error={errors.Password && touched.Password && values.Password.length < 1 ? errors.Password : ''}
                  value={values.Password}
                  showPassword={true}
                />
              </FullWidthFormField>
              <ForgotBox />
              <ButtonCustom
                type="submit"
                isLoading={isLoading}
                title="Sign in"
                variant="contained"
                color="mainWhiteColor"
                iconRight="chevronRight"
                disabled={!formValid}
                styles={{margin: isMobile ? '25px auto 0 auto' : '28px auto 0 auto'}}
              />
            </Content>
            <SeparatorLineAndText />
            <Content>
              {/* <GoogleApiProvider clientId={GOOGLE_APP_ID}> */}
              <GoogleLogin
                clientId={GOOGLE_APP_ID}
                render={(renderProps) => (
                  <SocialButton
                    setLocked={setLocked}
                    setIsLoading={setIsLoading}
                    client={client}
                    login={login}
                    checkEmail={validateEmail}
                    setError={setError}
                    setNotificationMessage={setNotificationMessage}
                    getErrorByCode={getErrorByCode}
                    socialName="google"
                    // disabled={locked}
                    styles={{
                      marginBottom: 16
                    }}
                    signIn={signIn}
                    onClick={renderProps.onClick}
                    disabled={renderProps.disabled}
                  />
                )}
                onSuccess={(response) => {
                  console.log(response.profileObj);
                  const {email, familyName, givenName, imageUrl} = response.profileObj;
                  const formatedResponse = {
                    email: email,
                    firstName: givenName,
                    lastName: familyName,
                    avatar: imageUrl
                  };
                  globalActions.setSignUpInfo(formatedResponse);
                  signIn(email, '', true, formatedResponse).then();
                }}
                onFailure={(e) => {
                  if (e?.error !== 'idpiframe_initialization_failed') {
                    // setNotificationMessage({
                    //   text: 'Please try again',
                    //   type: 'error'
                    // });
                  }
                }}
                cookiePolicy={'single_host_origin'}
              />

              {/* </GoogleApiProvider> */}
              <FacebookLogin
                appId={FB_APP_ID}
                autoLoad={false}
                fields="name,email,picture"
                callback={(response) => {
                  // if (response?.status == 'unknown') {
                  //   setNotificationMessage({
                  //     text: 'Cancelled',
                  //     type: 'error'
                  //   });
                  //   setError(true);
                  //   return;
                  // }
                  if (response && response.email) {
                    const name = response.name.split(' ');
                    const formattedResponse = {
                      email: response.email,
                      firstName: name[0],
                      lastName: name.length > 1 ? name[1] : ' ',
                      avatar: `https://graph.facebook.com/${response.id}/picture?width=160&height=160`
                    };
                    globalActions.setSignUpInfo(formattedResponse);
                    signIn(response.email, '', true, formattedResponse).then();
                  } else {
                    // setNotificationMessage({
                    //   text: 'Cancelled',
                    //   type: 'error'
                    // });
                    // setError(true);
                  }
                }}
                render={(renderProps) => (
                  <SocialButton onClick={renderProps.onClick} socialName="facebook" disabled={locked} />
                )}
              />
            </Content>
          </SignForm>
        )}
      </Formik>
    </Wrapper>
  );
};

export default SignInComponent;
