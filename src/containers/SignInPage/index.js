import React from 'react';
import {useWindowSize} from '@reach/window-size';
import SignInComponent from './SignInComponent';

const SignInPage = ({onClickClose}) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return <SignInComponent isMobile={isMobile} onClickClose={onClickClose} />;
};

export default SignInPage;
