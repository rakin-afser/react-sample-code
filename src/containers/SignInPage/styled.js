import styled from 'styled-components';
import media from '../../constants/media';

export const Wrapper = styled.div`
  padding-bottom: 64px;
  width: 100%;
  
  // @media (min-width: ${media.mobileMax}){
  //   & .custom-form-field {
  //     margin-bottom: 28px;
  //   }
  // }
`;

export const Content = styled.div`
  &&& {
    padding: 0 24px;
    display: flex;
    align-items: center;
    flex-direction: column;
    @media (min-width: ${media.mobileMax}) {
      padding: 0 22px;
    }
  }
`;

export const SignForm = styled.form.attrs(() => ({noValidate: true}))`
  margin-top: 31px;
  @media (min-width: ${media.mobileMax}) {
    margin-top: 18px;
  }
`;

export const FullWidthFormField = styled.div`
  width: 100%;
`;
