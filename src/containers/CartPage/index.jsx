import React from 'react';
import {useWindowSize} from '@reach/window-size';

import CartPageDesktop from 'components/Cart/desktop';
import CartPageMobile from 'components/Cart/mobile';

import Layout from '../Layout';

function CartPage() {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  return (
    <Layout hideFooter={isMobile} hideHeader={isMobile} isFooterShort>
      {isMobile ? <CartPageMobile /> : <CartPageDesktop />}
    </Layout>
  );
}

export default CartPage;
