import React, {useEffect} from 'react';
import {PageWrap} from './styled';
import PostMobile from '../../components/PostMobile';
import {feedBacks} from './data';
import Report from 'containers/Report/mobile';
import OptionsPopup from 'components/Modals/mobile/OptionsPopup';

const ShopPageFeedbackMobile = () => {
  const buttons = [
    {
      title: 'Edit'
    },
    {
      title: 'Delete'
    }
  ];

  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, []);

  return (
    <>
      <PageWrap>
        {feedBacks.map((feedBack, i) => (
          <PostMobile
            key={i}
            avatar={feedBack.avatar}
            name={feedBack.name}
            title={feedBack.title}
            message={feedBack.message}
            time={feedBack.when}
            rating={feedBack.rating}
            tags={feedBack.tags}
            slides={feedBack.photos}
            sliderCount={feedBack.shoppingCount}
            productDelivery={feedBack.delivery}
            productName={feedBack.productTitle}
            productPhoto={feedBack.productPhoto}
            productPrice={feedBack.price}
            showFollowBtn
            comments={feedBack.comments}
            likes={feedBack.likes}
            headerBorderBottom
            bookmarks={feedBack.bookmarks}
            shared={feedBack.shared}
          />
        ))}
      </PageWrap>
      <Report />
      <OptionsPopup buttons={buttons} />
    </>
  );
};

export default ShopPageFeedbackMobile;
