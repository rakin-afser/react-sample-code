import Avatar from './mock/avatar.png';
import PostPhoto from './mock/post2.png';
import PostPhoto2 from './mock/post.png';
import Product from './mock/product.png';
import Avatar2 from './mock/avatar2.png';

export const feedBacks = [
  {
    name: 'gigihadid',
    when: '20m ago',
    rating: 5,
    avatar: Avatar,
    photos: [PostPhoto, PostPhoto2],
    title: 'I just love it <3 <3',
    message:
      'New collection is coming in spring 2020 and we created so many things which are really awesome and new, i love it so much and i recommend it!',
    tags: ['topproducts', 'chanel', 'levernis'],
    productTitle: 'Nail Polish Le Vernis 08',
    productPhoto: Product,
    price: '490.00',
    delivery: 'Free Shipping',
    likes: 130,
    commentsCount: 324,
    bookmarks: 100,
    shared: 251,
    shoppingCount: 1,
    comments: [
      {
        avatar: Avatar2,
        name: 'trishalists',
        message: 'Colors will depend heavily on the setting the character who wears.',
        replies: 3,
        time: '2 days ago',
        likes: 5
      },
      {
        avatar: Avatar,
        name: 'kate',
        message: 'Colors will depend heavily on the setting the character who wears.',
        replies: 3,
        time: '2 days ago',
        likes: 20
      },
      {
        avatar: Avatar2,
        name: 'liza',
        message: 'Colors will depend heavily on the setting the character who wears.',
        replies: 3,
        time: '2 days ago',
        likes: 2
      }
    ]
  },
  {
    name: 'sara',
    when: '20m ago',
    rating: 5,
    avatar: Avatar2,
    photos: [PostPhoto2, PostPhoto],
    title: 'I just love it <3 <3',
    message:
      'Last month I bought these Makeup Pallette into my new collections and everybody is asking me where I get!',
    tags: ['topproducts', 'chanel', 'levernis'],
    productTitle: 'Nail Polish Le Vernis 08',
    productPhoto: Product,
    price: '490.00',
    delivery: 'Free Shipping',
    likes: 350,
    shoppingCount: 2,
    commentsCount: 324,
    bookmarks: 100,
    shared: 251,
    comments: [
      {
        avatar: Avatar,
        name: 'trishalists',
        message: 'Colors will depend heavily on the setting the character who wears.',
        replies: 3,
        time: '2 days ago',
        likes: 5
      }
    ]
  }
];
