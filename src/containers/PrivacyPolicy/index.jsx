import React from 'react';
import {useTranslation} from 'react-i18next';
import { useQuery } from '@apollo/client';
import {Helmet} from "react-helmet";
import {PRIVACY_POLICY_CONTENT} from "../../queries";
import Loader from "../../components/Loader";
import Wrapper from "./styled";
import Layout from "../Layout";
const PrivacyPolicy = () => {
    const {t} = useTranslation();
    const {data, loading} = useQuery(PRIVACY_POLICY_CONTENT);

    if(loading != true){
        console.log('loading ', loading)
        const {page: {content, title}}=data;
        return (
           <Layout>
               <Wrapper>
                   <Helmet>
                       <title>{title || 'Privacy Policy'}</title>
                       <meta name={'description'} content={'testSample privacy policy and data protection policy'} />
                   </Helmet>
                   <div dangerouslySetInnerHTML={{__html: content}} />
               </Wrapper>
           </Layout>
        );
    }
    return <Loader />

};

export default PrivacyPolicy;
