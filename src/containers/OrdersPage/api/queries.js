import {gql} from '@apollo/client';

export const REVIEW_CHECK = gql`
  query ReviewCheck($productId: ID, $userId: [ID]) {
    comments(where: {commentType: "review", contentId: $productId, authorIn: $userId}) {
      nodes {
        databaseId
        id
      }
    }
  }
`;

export const WRITE_REVIEW = gql`
  mutation WriteReview($input: WriteReviewInput!) {
    __typename
    writeReview(input: $input) {
      clientMutationId
      rating
    }
  }
`;
