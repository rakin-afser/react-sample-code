import styled from 'styled-components';

import {secondaryTextColor} from '../../constants/colors';

export const ModalTitle = styled.span`
  display: block;
  font-family: 'SF Pro Display', sans-serif;
  font-weight: 600;
  font-size: 18px;
  letter-spacing: 0.019em;
  color: #000;
`;

export const FieldHolder = styled.div`
  &:not(:last-child) {
    margin-bottom: 16px;
  }
`;

export const FieldLabel = styled.label`
  display: block;
  color: ${secondaryTextColor};
  margin-bottom: 4px;
`;
