import React, {useState, useEffect} from 'react';
import {useQuery} from '@apollo/client';
import {GET_ORDERS} from 'queries';
import moment from 'moment';
import {Input} from 'antd';
import Grid from 'components/Grid';
import FadeOnMount from 'components/Transitions';
import OrderDetails from 'components/Modals/OrderDetails';
import useGlobal from 'store';
import OrdersList from './components/OrdersList';
import Modal from './components/Modal';
import {userId} from 'util/heplers';
import {TabList, TabListItem, TabListLink} from './components/TabsMenu/styled';
import {ModalTitle, FieldHolder, FieldLabel} from './styled';

const {TextArea} = Input;

const links = ['All Orders', 'Processing', 'Delivered', 'Returned', 'Cancelled'];

const OrderPageDesktop = () => {
  const [global, setGlobal] = useGlobal();
  const [activeLink, setActiveLink] = useState('All Orders');
  const [activeDate, setActiveDate] = useState('Last year');

  const getStatus = () => {
    switch (activeLink) {
      case 'All Orders':
        return null;
      case 'Processing':
        return 'PROCESSING';
      case 'Delivered':
        return 'COMPLETED';
      case 'Returned':
        return 'REFUNDED';
      case 'Cancelled':
        return 'CANCELLED';
      default:
        return null;
    }
  };

  const getByDate = () => {
    const year = parseInt(moment().format('YYYY'));
    const month = moment().format('M') - 3;
    const week = moment().format('w') - 1;

    switch (activeDate) {
      case 'Last Year':
        return {year};
      case 'Last 3 Months':
        return {after: {month}};
      case 'Last Week':
        return {week};
      default:
        return {year};
    }
  };

  const {data, loading, error} = useQuery(GET_ORDERS, {
    variables: {
      customerId: userId,
      statuses: getStatus(),
      dateQuery: getByDate('activeDate')
    }
  });

  // useEffect(() => {
  //   if (userId) {
  //     getOrders();
  //   }
  // }, [getOrders]);

  function getLinks(activeLink, setActiveLink) {
    return links.map((item) => (
      <TabListItem key={item}>
        <TabListLink name={item} onClick={(ev) => changeContent(ev, setActiveLink)} active={item === activeLink}>
          {item}
        </TabListLink>
      </TabListItem>
    ));
  }

  function changeContent(ev, setActiveLink) {
    const activeLink = ev.target.getAttribute('name');
    setActiveLink(activeLink);
    // getOrders();
  }

  const feedbackHeaderContent = () => {
    return <ModalTitle>Send Message</ModalTitle>;
  };

  const feedbackModalContent = () => {
    return (
      <form>
        <FieldHolder>
          <FieldLabel htmlFor="orderId">Order No:</FieldLabel>
          <Input size="large" id="orderId" />
        </FieldHolder>
        <FieldHolder>
          <FieldLabel htmlFor="feedbackMessage">Add Description</FieldLabel>
          <TextArea id="feedbackMessage" rows={4} />
        </FieldHolder>
      </form>
    );
  };

  const onFeedbackFormSend = () => {};

  return (
    <FadeOnMount>
      <Grid>
        <Grid column padding="0 0 0 24px" width="100%">
          <TabList>{getLinks(activeLink, setActiveLink)}</TabList>
          <OrdersList
            activeDate={activeDate}
            setActiveDate={setActiveDate}
            loading={loading}
            error={error}
            /** right data will needed after Alex fix query */
            // orders={data?.orders?.nodes}
            orders={data?.orders?.nodes?.filter((el) => el.stores?.nodes?.length === 1)}
          />
        </Grid>
      </Grid>

      <Modal
        headerContent={feedbackHeaderContent()}
        mainContent={feedbackModalContent()}
        footerBtnText="Send"
        onFooterBtnClick={onFeedbackFormSend()}
      />

      <OrderDetails orderDetailsModal={global.orderDetails.open} setOrderDetailsModal={setGlobal.setOrderDetails} />
    </FadeOnMount>
  );
};

export default OrderPageDesktop;
