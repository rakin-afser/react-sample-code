import styled from 'styled-components';
import {textGray} from 'constants/colors';

export const List = styled.ul`
  color: ${textGray};
  font-size: 12px;
  margin-bottom: 10px;

  &.active {
    color: #398287;

    svg {
      stroke: #398287;
    }
  }

  li {
    display: inline-block;
    vertical-align: top;

    &:not(:last-child) {
      margin-right: 20px;
    }
  }

  strong {
    font-weight: 500;
    margin-right: 2px;
    font-size: 14px;
  }

  i {
    vertical-align: middle;
    margin-top: -5px;
  }

  svg {
    stroke: ${textGray};
  }
`;
