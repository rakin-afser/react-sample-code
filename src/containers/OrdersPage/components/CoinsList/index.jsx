import React from 'react';

import Icons from 'components/Icon';

import {List} from './styled';

const CoinsList = ({status, shopCoins, midCoins}) => {
  return (
    <List className={status === 'COMPLETED' ? 'active' : null}>
      <li>
        Mid Coins <strong>+{midCoins}</strong> <Icons type="coins" />
      </li>
      <li>
        Shop Coins <strong>+{shopCoins}</strong> <Icons type="coins" />
      </li>
    </List>
  );
};

export default CoinsList;
