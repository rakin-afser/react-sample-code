import React from 'react';
import {List, Item, Link} from './styled';

const ProfileMenu = ({items = [], selected, select = (f) => f}) => (
  <List>
    {items.map((item, i) => (
      <Item key={i}>
        <Link active={i === selected} onClick={() => select(i)}>
          {item}
        </Link>
      </Item>
    ))}
  </List>
);

export default ProfileMenu;
