import React from 'react';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';
import Btn from 'components/Btn';

const FollowBtn = ({shopId, followed}) => {
  const [followUnfollowStore, {loading}] = useFollowUnFollowStoreMutation({followed});

  return (
    <Btn
      kind={followed ? 'following-md' : 'follow-md'}
      loading={loading}
      onClick={() => followUnfollowStore({variables: {input: {id: parseInt(shopId)}}})}
    />
  );
};

FollowBtn.propTypes = {};

export default FollowBtn;
