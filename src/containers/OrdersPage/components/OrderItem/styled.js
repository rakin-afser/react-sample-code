import styled from 'styled-components/macro';
import {blue, blueColor} from 'constants/colors';

export const ItemWrapper = styled.div`
  background: #fff;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  overflow: hidden;

  &:not(:last-child) {
    margin-bottom: 24px;
  }

  @media (hover: hover) {
    &:hover {
      .more-btn {
        opacity: 1;
      }
    }
  }
`;

export const AvatarWrapper = styled.div`
  margin-bottom: 24px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Avatar = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  overflow: hidden;
  margin-right: 17px;
  flex-shrink: 0;
  transition: ease 0.4s;
  box-shadow: 0 0 6px rgba(000, 000, 000, 0);

  &:hover {
    cursor: pointer;
    box-shadow: 0 0 6px rgba(000, 000, 000, 0.3);
  }
`;

export const AvatarPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const UserInfoWrapper = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: space-between;
`;

export const UserInfo = styled.div`
  & + * {
    margin-left: 20px;
  }
`;

export const UserName = styled.span`
  display: block;
  font-weight: 500;
  font-size: 18px;
  color: #000;
  transition: ease 0.4s;

  &:hover {
    cursor: pointer;
    color: ${blue};
  }
`;

export const OrderLink = styled.a`
  color: ${blueColor};
  margin-left: 30px;
`;

export const ProgressId = styled.span`
  color: ${blueColor};
  margin-left: 15px;
  font-weight: 500;
  font-size: 12px;
`;

export const FollowBtnWrapper = styled.button`
  width: 92px;
`;
