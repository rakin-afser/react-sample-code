import React from 'react';
import PropTypes from 'prop-types';
import {useHistory} from 'react-router-dom';
import useGlobal from 'store';
import Grid from 'components/Grid';
import {bgSuperLightGray, headerShadowColor} from 'constants/colors';
import OrderStatus from '../OrderStatus';
import UserRating from '../UserRating';
import OrderInfoList from '../OrderInfoList';
import PriceInfo from '../PriceInfo';
import OrderCartItem from '../OrderCartItem';
import FollowBtn from 'containers/OrdersPage/components/OrderItem/components/FollowBtn';
import avatarPlaceholder from 'images/avatarPlaceholder.png';

import {
  ItemWrapper,
  AvatarWrapper,
  Avatar,
  AvatarPic,
  UserInfoWrapper,
  UserInfo,
  UserName,
  OrderLink,
  ProgressId,
  FollowBtnWrapper
} from './styled';

const OrderItem = ({orderInformation}) => {
  const history = useHistory();
  const [, setGlobalState] = useGlobal();
  const {status, orderInfo, databaseId, total, date, stores, lineItems, transactionId} = orderInformation;

  const onOrderDetailsModal = () => {
    setGlobalState.setOrderDetails({open: true, data: orderInformation});
  };

  return (
    <ItemWrapper>
      <Grid
        sb
        wrap
        padding="16px 24px"
        bg={bgSuperLightGray}
        customStyles={`border-bottom: 1px solid ${headerShadowColor};`}
      >
        <div>
          {stores?.nodes?.map((el, key) => (
            <AvatarWrapper key={key}>
              <Avatar onClick={() => history.push(`/shop/${el?.id}/products`)}>
                <AvatarPic src={el?.gravatar || avatarPlaceholder} />
              </Avatar>
              <UserInfoWrapper>
                <UserInfo>
                  <UserName onClick={() => history.push(`/shop/${el?.id}/products`)}>{el?.name}</UserName>
                  <UserRating rating={el?.rating} />
                </UserInfo>
                <FollowBtnWrapper>
                  <FollowBtn followed={el?.followed} shopId={el?.id} />
                </FollowBtnWrapper>
              </UserInfoWrapper>
            </AvatarWrapper>
          ))}
        </div>

        <Grid wrap jcfe aic padding="0 0 0 10px">
          <OrderStatus status={status} />
          {status === 'PROCESSING' && <ProgressId>{transactionId}</ProgressId>}
          <OrderLink onClick={onOrderDetailsModal}>View Order</OrderLink>
        </Grid>

        <Grid column customStyles={'flex-grow: 1;'}>
          <Grid sb width="100%">
            <Grid wrap padding="0 10px 0 0" width="54%">
              <OrderInfoList
                orderId={databaseId}
                date={date}
                items={lineItems?.nodes?.length}
                orderDataset={orderInfo}
              />
            </Grid>
            <Grid wrap jcfe padding="0 0 0 10px" width="46%">
              <PriceInfo price={total} />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      {lineItems?.nodes?.map((orderCartItem, index) => (
        <OrderCartItem status={status} key={index} cartItemInfo={orderCartItem} date={date} />
      ))}
    </ItemWrapper>
  );
};

OrderItem.defaultProps = {
  orderInformation: {}
};

OrderItem.propTypes = {
  orderInformation: PropTypes.shape()
};

export default OrderItem;
