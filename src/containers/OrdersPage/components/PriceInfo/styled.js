import styled from 'styled-components';
import {grayTextColor} from '../../../../constants/colors';

export const Price = styled.div`
  color: ${grayTextColor};

  small {
    margin-left: 8px;
    font-size: 0.86em;
  }

  strong {
    font-weight: 500;
    color: #000;
    font-size: 16px;
  }

  ${({styles}) => styles || null};
`;
