import React from 'react';
import parse from 'html-react-parser';
import {Price} from './styled';

const PriceInfo = ({styles, price, currencySymbol}) => {
  return (
    <Price styles={styles}>
      Total:{' '}
      <strong>
        {currencySymbol && parse(currencySymbol.toString())}
        {price}
      </strong>
    </Price>
  );
};

export default PriceInfo;
