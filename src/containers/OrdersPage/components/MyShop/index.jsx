import React from 'react';
import {Container, Logo, About, Name, Title, Date} from './styled';
import Grid from 'components/Grid';
import Icons from 'components/Icon';
import shopIcon from './img/shop_icon.png';

export default function() {
  return (
    <Container>
      <Grid aic>
        <Logo>
          <img src={shopIcon} alt="shop" />
        </Logo>
        <About>
          <Name>valentino</Name>
          <Title>To your shop</Title>
        </About>
        <Icons type="arrow" />
      </Grid>
      <Date>Joined 12 Sep, 2019</Date>
    </Container>
  );
}
