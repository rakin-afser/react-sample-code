import styled from 'styled-components';
import {headerShadowColor} from 'constants/colors';

export const Overlay = styled.div`
  position: fixed;
  z-index: 1111;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.5);
`;

export const Container = styled.div`
  position: relative;
  width: 425px;
  margin: 80px 0;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
  background: #fff;
  border-radius: 4px;
`;

export const CloseBtn = styled.div`
  cursor: pointer;
  transition: opacity 0.3s;
  flex-shrink: 0;
  margin-right: -10px;

  &:hover {
    opacity: 0.7;
  }

  svg {
    vertical-align: top;
  }
`;

export const Header = styled.header`
  display: flex;
  align-items: center;
  padding: 14px 24px;
  border-bottom: 1px solid ${headerShadowColor};
`;

export const HeaderContent = styled.header`
  flex-grow: 1;
`;

export const Main = styled.div`
  padding: 24px;
`;

export const Footer = styled.footer`
  padding: 16px 24px;
  border-top: 1px solid ${headerShadowColor};
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

export const FooterCloseBtn = styled.button`
  color: #000;
  background: transparent;
  border: none;
  padding: 0;
  cursor: pointer;
  transition: opacity 0.3s;

  &:hover {
    opacity: 0.7;
  }
`;

export const FooterBtn = styled.button`
  color: #000;
  background: transparent;
  cursor: pointer;
  border: 1px solid #000;
  font-weight: 500;
  line-height: 1;
  min-width: 100px;
  text-align: center;
  padding: 6px 20px;
  border-radius: 24px;
  margin-left: 28px;
  transition: 0.3s;

  &:hover {
    background: #000;
    color: #fff;
  }
`;
