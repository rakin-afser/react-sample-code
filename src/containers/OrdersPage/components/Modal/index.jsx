import React, {useEffect, useRef} from 'react';
import useGlobal from 'store';
import {element, bool, string, func} from 'prop-types';

import {CSSTransition} from 'react-transition-group';

import CloseIcon from '../../../../assets/CloseIcon';

import {Overlay, CloseBtn, Header, HeaderContent, Container, Main, Footer, FooterCloseBtn, FooterBtn} from './styled';

const Modal = ({headerContent, mainContent, isFooterVisible, footerBtnText, onFooterBtnClick}) => {
  const [globalState, globalActions] = useGlobal();

  useEffect(() => {
    if (globalState.isModalOpened) {
      document.querySelector('html').classList.add('overflow-hidden');
    } else {
      document.querySelector('html').classList.remove('overflow-hidden');
    }
  }, [globalState.isModalOpened]);

  const onModalClose = () => {
    globalActions.setIsModalOpened(false);
  };

  const useOutsideClickHandler = (ref) => {
    const handleClickOutside = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        onModalClose();
      }
    };

    useEffect(() => {
      document.addEventListener('mousedown', handleClickOutside);
      return () => {
        document.removeEventListener('mousedown', handleClickOutside);
      };
    });
  };

  const modalContainer = useRef();
  useOutsideClickHandler(modalContainer);

  return (
    <CSSTransition in={globalState.isModalOpened} timeout={100} unmountOnExit className="modal-animation">
      <Overlay>
        <Container ref={modalContainer}>
          {headerContent && (
            <Header>
              <HeaderContent>{headerContent}</HeaderContent>
              <CloseBtn onClick={onModalClose}>
                <CloseIcon width={24} height={24} color="#000" />
              </CloseBtn>
            </Header>
          )}
          <Main>{mainContent}</Main>
          {isFooterVisible && (
            <Footer>
              <FooterCloseBtn onClick={onModalClose}>Cancel</FooterCloseBtn>
              {footerBtnText && <FooterBtn onClick={onFooterBtnClick}>{footerBtnText}</FooterBtn>}
            </Footer>
          )}
        </Container>
      </Overlay>
    </CSSTransition>
  );
};

Modal.defaultProps = {
  isModalOpened: false,
  mainContent: '<div>Modal Content</div>',
  isFooterVisible: true
};

Modal.propTypes = {
  isModalOpened: bool.isRequired,
  headerContent: element,
  mainContent: element.isRequired,
  isFooterVisible: bool,
  footerContent: element,
  footerBtnText: string,
  onFooterBtnClick: func
};

export default Modal;
