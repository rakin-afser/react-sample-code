import styled from 'styled-components';

export const Wrap = styled.div`
  background: #fff;
  width: 100%;
  min-height: 120px;
  padding: 24px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-weight: 500;
  color: #000;
  text-align: center;
  box-shadow: 0 4px 15px rgba(0, 0, 0, 0.1);
  border-radius: 4px;
  ${({styles}) => styles || null};
`;
