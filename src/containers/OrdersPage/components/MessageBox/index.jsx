import React from 'react';
import {
    Wrap
} from "./styled";

const MessageBox = ({children, styles}) => {
    return (
        <Wrap styles={styles}>
            {children}
        </Wrap>
    )
};

export default MessageBox;
