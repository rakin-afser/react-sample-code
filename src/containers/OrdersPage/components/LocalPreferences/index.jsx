import React from 'react';
import {
	Field,
	Label,
    FormGroup,
    LocalPreferencesArea,
} from './styled';
import Grid from "../../../../components/Grid";
import AccountContent from "components/AccountContent";

import {Select} from 'antd';
const { Option } = Select;

const local = [
	{
      label: 'Country',
      value: 'Bahrain',
      options: [
        {
          label: 'Ukraine',
          value: 'Ukraine',
        },
        {
          label: 'United Kingdom',
          value: 'United Kingdom',
        },
        {
          label: 'United States of America',
          value: 'United States of America',
        },
        {
          label: 'United Arab Emirates',
          value: 'United Arab Emirates',
        },
        {
          label: 'Uruguay',
          value: 'Uruguay',
        },
      ],
	},
	{
      label: 'Timezone',
      value: '(GTM + 3:00) Manama',
      options: [
        {
          label: '(GMT + 3:00) Manama',
          value: '(GMT + 3:00) Manama',
        },
        {
          label: '(GMT + 3:00) Manama',
          value: '(GMT + 3:00) Manama',
        },
        {
          label: '(GMT + 3:00) Manama',
          value: '(GMT + 3:00) Manama',
        },
        {
          label: '(GMT + 3:00) Manama',
          value: '(GMT + 3:00) Manama',
        },
      ],
	},
	{
      label: 'Currency',
      value: 'BHD (Bahraini dinar)',
      options: [
        {
          label: 'BHD (Bahrain Dinar)',
          value: 'BHD (Bahrain Dinar)',
        },
        {
          label: 'USD (US Dollar)',
          value: 'USD (US Dollar)',
        },
        {
          label: 'GBP (Great Britain Pound)',
          value: 'GBP (Great Britain Pound)',
        },
        {
          label: 'EUR (Euro)',
          value: 'EUR (Euro)',
        },
        {
          label: 'CAD (Canadian Dollar)',
          value: 'CAD (Canadian Dollar)',
        },
      ],
	},
	{
      label: 'Language',
      value: 'English',
      options: [
        {
          label: 'English',
          value: 'English',
        },
        {
          label: 'Estonian',
          value: 'Estonian',
        },
        {
          label: 'Ethiopian',
          value: 'Ethiopian',
        }
      ],
	},
];

function handleChange(value) {
  console.log(`selected ${value}`);
}

function getFields() {
	return(
		local.map( item =>
			<FormGroup>
				<Label>{item.label}</Label>
				<div>
                  <Field defaultValue={item.options[0].label} style={{ width: 360 }} onChange={handleChange}>
                    {item.options.map(option => (
                        <Option value={option.label}>
                          {option.value}
                        </Option>
                    ))}
                  </Field>
                </div>
			</FormGroup>
		)
	)
}

function LocalPreferences() {
	return (
		<AccountContent heading='Local Preferences'>
			<Grid column padding='24px 58px 24px 44px'>
              <LocalPreferencesArea>
                { getFields() }
              </LocalPreferencesArea>
			</Grid>
		</AccountContent>
	);
}

export default LocalPreferences;