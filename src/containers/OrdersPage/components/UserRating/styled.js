import styled from 'styled-components';
import {disabledLinkColor} from '../../../../constants/colors';

export const Rating = styled.div`
  display: flex;
  align-items: center;
  font-size: 12px;
  line-height: 1.3;
  color: ${disabledLinkColor};
  margin-top: 2px;

  i {
    margin: 0 1px;
  }
`;
