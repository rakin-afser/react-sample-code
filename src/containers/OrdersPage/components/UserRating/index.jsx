import React from 'react';
import {Rating} from './styled';

import Icon from 'components/Icon';

const UserRating = ({rating}) => {
  return (
    <Rating>
      <span style={{marginRight: '6px'}}>{rating}</span>
      <Icon type="star" svgStyle={{width: 9, height: 9, fill: '#FFC131'}} />
      <Icon type="star" svgStyle={{width: 9, height: 9, fill: '#333333'}} />
      <Icon type="star" svgStyle={{width: 9, height: 9, fill: '#333333'}} />
      <Icon type="star" svgStyle={{width: 9, height: 9, fill: '#333333'}} />
      <Icon type="star" svgStyle={{width: 9, height: 9, fill: '#333333'}} />
      {rating ? <span style={{marginLeft: '8px'}}>({rating})</span> : null}
    </Rating>
  );
};

export default UserRating;
