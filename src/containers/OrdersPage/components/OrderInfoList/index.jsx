import React from 'react';
import moment from 'moment';
import {List} from './styled';

const OrderInfoList = ({orderId, date, items}) => {
  return (
    <>
      <List>
        <li>
          Items: <strong>{items}</strong>
        </li>
        <li>
          Order date: <strong>{moment(date).format('M/DD/YY')}</strong>
        </li>
        <li>
          Order ID: <strong>{orderId}</strong>
        </li>
      </List>
      <List>
        <li>
          Shipping/Delivery: <strong>!!!WE NEED THIS FIELD!!!</strong>
        </li>
      </List>
    </>
  );
};

export default OrderInfoList;
