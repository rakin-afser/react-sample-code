import styled from 'styled-components';
import {grayTextColor} from '../../../../constants/colors';

export const List = styled.ul`
  color: ${grayTextColor};

  li {
    display: inline-block;
    margin-bottom: 9px;

    &:not(:last-child) {
      margin-right: 34px;
    }
  }

  strong {
    font-weight: 500;
    color: #000;
    margin-left: 12px;
  }
`;
