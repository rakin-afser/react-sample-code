import React from 'react';
import {Wrap, IconWrapper} from './styled';

import Icon from 'components/Icon';
import {primaryColor} from 'constants/colors';

const statuses = {
  COMPLETED: {
    icon: 'checkCircle',
    iconColor: '#2ECC71',
    text: 'Completed'
  },
  REFUNDED: {
    icon: 'closeCircle',
    iconColor: primaryColor,
    text: 'Refunded'
  },
  PROCESSING: {
    text: 'Processing',
    icon: '',
    iconColor: ''
  },
  CANCELLED: {
    icon: '',
    iconColor: '',
    text: 'Cancelled'
  },
  PENDING: {
    icon: '',
    iconColor: '',
    text: 'Pending'
  }
};

const OrderStatus = ({status = ''}) => {
  return (
    <Wrap>
      {statuses[status]?.icon && (
        <IconWrapper>
          <Icon type={statuses[status]?.icon} svgStyle={{width: 17, height: 17, color: statuses[status]?.iconColor}} />
        </IconWrapper>
      )}
      {statuses[status]?.text}
    </Wrap>
  );
};

export default OrderStatus;
