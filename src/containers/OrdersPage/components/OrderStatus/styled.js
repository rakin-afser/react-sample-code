import styled from 'styled-components';

export const Wrap = styled.span`
  display: inline-flex;
  align-items: center;
  font-weight: 500;
  font-size: 12px;
`;

export const IconWrapper = styled.span`
  margin-right: 9px;
  display: inline-flex;
`;
