import styled from 'styled-components';

export const TabList = styled.ul`
  display: flex;
  justify-content: space-around;
  box-shadow: rgba(0, 0, 0, 0.1) 0 0 5px;
  background: rgb(255, 255, 255);
  border-radius: 4px;
  width: 100%;
`;

export const TabListItem = styled.li`
  text-align: center;
  cursor: pointer;
  font-size: 16px;
  line-height: 20px;
  color: rgb(122, 122, 122);
  box-sizing: border-box;
  outline: none;
  width: 50%;
  transition: ease 0.3s;
`;

export const TabListLink = styled.a`
  font-size: 16px;
  color: ${({active}) => (active ? '#000' : '#656565')};
  font-weight: ${({active}) => (active ? '700' : '400')};
  display: inline-block;
  padding: 10px 0;
  text-align: center;
  ${({active}) => (active ? 'box-shadow: inset 0 -2px 0 0 #ED484F;' : 'box-shadow: inset 0 -2px 0 0 transparent')};
  transition: ease 0.3s;

  &:hover {
    color: #000;
  }
`;
