import React from 'react';
import Skeleton, {SkeletonTheme} from 'react-loading-skeleton';

const OrdersListSkeleton = () => {
  const renderSkeleton = (quantity) => {
    return Array.from(Array(quantity).keys()).map((elm, idx) => (
      <SkeletonTheme>
        <div style={{display: 'flex', alignItems: 'center', marginBottom: 20, marginTop: 20}}>
          <div style={{marginRight: '15px'}}>
            <Skeleton width={80} circle height={80} />
          </div>
          <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
            <Skeleton width={800} height={20} />
            <Skeleton width={800} height={20} />
            <Skeleton width={800} height={20} />
          </div>
        </div>
        <div style={{display: 'flex', alignItems: 'center'}}>
          <div style={{marginRight: '15px'}}>
            <Skeleton width={80} height={80} />
          </div>
          <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
            <Skeleton width={800} height={20} />
            <Skeleton width={800} height={20} />
            <Skeleton width={800} height={20} />
          </div>
        </div>
      </SkeletonTheme>
    ));
  };

  return <>{renderSkeleton(5)}</>;
};

export default OrdersListSkeleton;
