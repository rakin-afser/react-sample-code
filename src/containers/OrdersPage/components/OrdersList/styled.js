import styled from 'styled-components';

export const Orders = styled.div`
  width: 100%;
`;

export const SelectHolder = styled.div`
  width: 100%;
  margin: 24px 0;
  text-align: right;

  .ant-select {
    min-width: 160px;
    font-family: Helvetica Neue, sans-serif;
    font-size: 12px;
    color: #000;

    &-open {
      .ant-select-arrow {
        transform: rotate(180deg);
      }
    }

    &.ant-select-focused {
      .ant-select-selection {
        border-right-width: 0 !important;
        box-shadow: none;
      }
    }

    &-selection {
      border: solid #c3c3c3;
      border-width: 0 0 1px;
      border-radius: 0;

      &:focus,
      &:hover,
      &:active {
        border-right-width: 0 !important;
        box-shadow: none;
      }

      &__rendered {
        margin-left: 20px;
      }
    }

    &-arrow {
      font-size: 22px;
      margin-top: -11px;
      right: 0;
      transition: transform 0.3s;

      svg {
        width: 22px;
        height: 22px;
        vertical-align: top;
      }
    }
  }
`;
