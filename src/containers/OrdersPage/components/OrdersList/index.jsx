import React from 'react';
import {Select} from 'antd';

import OrderItem from '../OrderItem';
import MessageBox from '../MessageBox';
import ArrowDown from '../../../../assets/ArrowDown';

import {Orders, SelectHolder} from './styled';
import FadeOnMount from 'components/Transitions';
import Error from 'components/Error';
import OrdersListSkeleton from 'containers/OrdersPage/components/OrdersList/components/OrdersListSkeleton';

const OrdersList = ({orders = [], loading, error, activeDate, setActiveDate}) => {
  const {Option} = Select;
  const handleChange = (value) => {
    setActiveDate(value);
  };

  const renderSelectHolder = () => {
    return (
      <SelectHolder>
        <Select defaultValue={activeDate} onChange={handleChange} suffixIcon={<ArrowDown />}>
          <Option key="Last Week" value="Last Week">
            Last Week
          </Option>
          <Option key="Last 3 Months" value="Last 3 Months">
            Last 3 Months
          </Option>
          <Option key="Last Year" value="Last Year">
            Last Year
          </Option>
        </Select>
      </SelectHolder>
    );
  };

  if (error) {
    return <Error />;
  }

  if (loading) {
    return <OrdersListSkeleton />;
  }

  return orders.length ? (
    <FadeOnMount>
      {renderSelectHolder()}
      <Orders>
        {orders.map((order, index) => (
          <OrderItem orderInformation={order} key={index} />
        ))}
      </Orders>
    </FadeOnMount>
  ) : (
    <>
      {renderSelectHolder()}
      <MessageBox styles="margin-top: 24px">
        <span>You do not have any orders to display in this view.</span>
      </MessageBox>
    </>
  );
};

export default OrdersList;
