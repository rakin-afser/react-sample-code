import styled from 'styled-components';
import {headerShadowColor, bookmarkFillColor, primaryColor, bgSuperLightGray} from '../../../../constants/colors';
import {Link} from 'react-router-dom';

export const ItemWrapper = styled.div`
  background: #fff;
  padding: 22px 24px;

  &:not(:last-child) {
    border-bottom: 1px solid ${headerShadowColor};
  }
`;

export const Product = styled(Link)`
  width: 96px;
  height: 96px;
  border-radius: 4px;
  overflow: hidden;
  margin-right: 20px;
  flex-shrink: 0;
`;

export const ProductPic = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Title = styled.span`
  display: block;
  margin-bottom: 14px;
  color: #000;
`;

export const OrderDate = styled.span`
  font-size: 12px;
  color: ${bookmarkFillColor};
`;

export const UnlockBtn = styled.button`
  color: ${primaryColor};
  border: 1px solid ${primaryColor};
  border-radius: 34px;
  height: 28px;
  display: inline-block;
  vertical-align: top;
  text-align: center;
  transition: 0.3s;
  padding: 0 16px;
  background-color: transparent;
  cursor: pointer;

  &:hover {
    background-color: ${primaryColor};
    color: #fff;

    svg {
      stroke: #fff;
    }
  }

  i {
    vertical-align: middle;
    margin: -2px 0 0 4px;
  }

  svg {
    stroke: ${primaryColor};
    transition: stroke 0.3s;
  }
`;

export const MoreInfo = styled.ul`
  position: absolute;
  top: 0;
  right: 100%;
  background: #fff;
  max-width: 200px;
  box-shadow: 0 2px 9px rgba(0, 0, 0, 0.28);
  border-radius: 8px;
  white-space: nowrap;
  font-family: Helvetica, sans-serif;

  li {
    padding: 8px 24px;

    &:not(:last-child) {
      border-bottom: 1px solid ${bgSuperLightGray};
    }
  }

  span,
  a {
    color: ${bookmarkFillColor};
    cursor: pointer;

    &:hover {
      color: #000;
    }
  }
`;

export const MoreBtn = styled.button`
  color: #7c7e82;
  border: none;
  padding: 0;
  line-height: 1;
  text-align: center;
  cursor: pointer;
  transition: 0.3s;
  background-color: transparent;

  @media (hover: hover) {
    opacity: 0;
  }

  &.ant-dropdown-open {
    opacity: 1;
  }

  &:hover {
    color: #000;
  }

  svg {
    vertical-align: top;
  }
`;
