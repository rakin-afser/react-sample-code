import React, {useState} from 'react';
import useGlobal from 'store';
import PropTypes from 'prop-types';
import moment from 'moment';
import ClampLines from 'react-clamp-lines';
import {Dropdown} from 'antd';
import {useQuery} from '@apollo/client';
import _ from 'lodash';

import Grid from 'components/Grid';
import Icons from 'components/Icon';
import RequestAReturn from 'components/Modals/RequestAReturn';
import PriceInfo from '../PriceInfo';
import CoinsList from '../CoinsList';
import MoreIcon from '../../../../assets/More';
import {ItemWrapper, Product, ProductPic, Title, OrderDate, UnlockBtn, MoreInfo, MoreBtn} from './styled';
import {renderCoinsByVariation} from 'util/heplers';
import {useUser} from 'hooks/reactiveVars';
import {REVIEW_CHECK} from 'containers/OrdersPage/api/queries';

const OrderCartItem = ({cartItemInfo, date, status}) => {
  const [, globalActions] = useGlobal();
  const [returnModal, showReturnModal] = useState(false);
  const {variationId, product, leftfeedback} = cartItemInfo;
  const variationToPass = {
    databaseId: variationId
  };
  const [user] = useUser();
  // const {data, loading, error} = useQuery(REVIEW_CHECK, {
  //   variables: {productId: product?.databaseId, userId: user?.databaseId}
  // });

  const openModal = () => {
    globalActions.setIsModalOpened(true);
  };

  const onReturnItemModal = () => {
    showReturnModal(true);
  };

  const MoreInfoMenu = (
    <MoreInfo>
      {status === 'COMPLETED' && (
        <li>
          <span>Leave Feedback</span>
        </li>
      )}
      <li>
        <a onClick={openModal} role="menuitem" href="#">
          Contact Seller
        </a>
      </li>
      {status === 'COMPLETED' && (
        <li>
          <a onClick={onReturnItemModal} href="#">
            Return Item
          </a>
        </li>
      )}
    </MoreInfo>
  );

  const midcoins = renderCoinsByVariation(product?.midCoins, variationToPass);
  const shopcoins = renderCoinsByVariation(product?.shopCoins, variationToPass);

  const unlockMidCoins = () => {
    globalActions.setLeaveFeedback({
      open: true,
      data: {
        databaseId: product?.databaseId,
        name: product?.name,
        image: product?.image?.sourceUrl,
        price: cartItemInfo?.total,
        currencySymbol: product?.currencySymbol,
        orderId: cartItemInfo?.orderId,
        storeId: product?.seller?.id,
        midcoins,
        shopcoins
      }
    });
  };

  function renderCoinButtons() {
    if (status === 'COMPLETED' && (shopcoins || midcoins) && !leftfeedback) {
      return <UnlockBtn onClick={unlockMidCoins}>Unlock Coins</UnlockBtn>;
    }

    if (status === 'COMPLETED' && (shopcoins || midcoins) && leftfeedback) {
      return <span>Coins Unlocked</span>;
    }

    return null;
  }
  return (
    <>
      <ItemWrapper>
        <Grid wrap width="100%">
          <Product to={`/product/${product?.slug}`}>
            <ProductPic src={product?.image?.sourceUrl} />
          </Product>
          <Grid sb customStyles="align-items: stretch; flex: 1;">
            <Grid column padding="0 10px 0 0" customStyles="flex-grow: 1;">
              <Title>
                <ClampLines text={product?.name} lines={1} buttons={false} />
              </Title>
              <CoinsList shopCoins={shopcoins} midCoins={midcoins} status={status} />
              <Grid aic width="100%">
                <PriceInfo
                  currencySymbol={product?.currencySymbol}
                  price={cartItemInfo?.total}
                  styles="margin-right: 9px;"
                />
                <OrderDate>Ordered on {moment(date).format('MMM D, YYYY')}</OrderDate>
              </Grid>
            </Grid>
            <Grid column sb aife padding="0 0 0 10px" customStyles="flex-shrink: 0;">
              {status !== 'CANCELLED' && (
                <Dropdown placement="topLeft" overlay={MoreInfoMenu} trigger={['click']}>
                  <MoreBtn className="more-btn">
                    <MoreIcon />
                  </MoreBtn>
                </Dropdown>
              )}
              {renderCoinButtons()}
            </Grid>
          </Grid>
        </Grid>
      </ItemWrapper>
      <RequestAReturn reqReturnModal={returnModal} setReqReturnModal={showReturnModal} />
    </>
  );
};

OrderCartItem.defaultProps = {
  cartItemInfo: {}
};

OrderCartItem.propTypes = {
  cartItemInfo: PropTypes.shape()
};

export default OrderCartItem;
