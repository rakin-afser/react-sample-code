import React from 'react';
import OrderPageDesktop from './OrderPageDesktop';

const OrdersPage = () => {
  return <OrderPageDesktop />;
};

export default OrdersPage;
