import preview1 from './img/preview1.png';
import preview2 from './img/preview2.png';
import preview3 from './img/preview3.png';
import preview4 from './img/preview4.png';
import preview5 from './img/preview5.png';
import preview6 from './img/preview6.png';
import preview7 from './img/preview7.png';

export default [
  {
    userInfo: {
      avatar: preview1,
      name: 'Shop Bright'
    },
    orderStatus: 'delivered',
    orderInfo: {
      Items: 2,
      'Order Id': '3490751',
      'Order Date': '12/13/19',
      'Shipping/Delivery': 'Aramex'
    },
    orderCartItems: [
      {
        image: preview4,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: '',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Ordered on Feb 17, 2018'
      },
      {
        image: preview5,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: 'active',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Ordered on Feb 17, 2018'
      }
    ]
  },
  {
    userInfo: {
      avatar: preview2,
      name: 'Shop Bright'
    },
    orderStatus: 'inProgress',
    orderProgressId: '2837-9382-4374-3853-4343-4334',
    orderInfo: {
      Items: 3,
      'Order Id': '3490751',
      'Order Date': '12/13/19',
      'Shipping/Delivery': 'Aramex'
    },
    orderCartItems: [
      {
        image: preview6,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: '',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Estimate on Feb 17, 2018'
      },
      {
        image: preview7,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: '',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Estimate on Feb 17, 2018'
      },
      {
        image: preview4,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: '',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Estimate on Feb 17, 2018'
      }
    ]
  },
  {
    userInfo: {
      avatar: preview3,
      name: 'Shop Bright'
    },
    orderStatus: 'returned',
    orderInfo: {
      Items: 3,
      'Order Id': '3490751',
      'Order Date': '12/13/19',
      'Shipping/Delivery': 'Aramex'
    },
    orderCartItems: [
      {
        image: preview6,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: '',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Estimate on Feb 17, 2018'
      },
      {
        image: preview7,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: '',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Estimate on Feb 17, 2018'
      },
      {
        image: preview4,
        title: '2PCS Full Coverage For iPhone..',
        coins: [
          {
            midCoins: 'Mid Coins:',
            price: '+16'
          },
          {
            midCoins: 'Shop Coins:',
            price: '+11'
          }
        ],
        coinsStatus: '',
        total: {
          type: 'BD',
          price: '756.870'
        },
        date: 'Estimate on Feb 17, 2018'
      }
    ]
  }
];
