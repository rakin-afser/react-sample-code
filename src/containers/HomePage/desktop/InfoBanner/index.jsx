import {useLazyQuery} from '@apollo/client';
import React from 'react';
import {INFO_BANNER} from '../../queries';
import Loader from 'components/Loader';
import ContentWithBackground from '../../components/ContentWithBackground';
import MakeEastAdvert from '../../components/MakeEasyAdvert';
import {ImageContainer} from './styled';
import _ from 'lodash';
import {useOnScreen} from 'hooks';

const InfoBanner = () => {
  const [get, {data, loading, error, called}] = useLazyQuery(INFO_BANNER, {
    variables: {id: 'secondary-banner-1'}
  });
  const ref = useOnScreen({doAction: get});

  if (!called) return <div ref={ref} />;

  if (loading) return <Loader isPartialHeight />;

  if (_.isEmpty(data?.banner) || error) return null;

  return (
    <ContentWithBackground
      banner
      style={{
        marginTop: 43,
        padding: '59px 0',
        backgroundSize: 'contain',
        backgroundPosition: 'right',
        backgroundColor: '#EAEAEC'
      }}
    >
      <MakeEastAdvert banner={data?.banner} />
      <ImageContainer src={data?.banner?.featuredImage?.node?.sourceUrl} />
    </ContentWithBackground>
  );
};

export default InfoBanner;
