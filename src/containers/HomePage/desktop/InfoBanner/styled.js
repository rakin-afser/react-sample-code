import styled from 'styled-components';

export const ImageContainer = styled.img`
  height: 243px;
  width: 413px;
  margin-right: 22px;
  position: relative;
  top: -2px;
  object-fit: cover;
`;
