import styled from 'styled-components/macro';
import {FlexContainer} from '../../../../globalStyles';
import {mainBlackColor, grayTextColor} from '../../../../constants/colors';

export const Wrapper = styled.div`
  background: linear-gradient(84.41deg, #f9f9f9 24.07%, #f0f0f0 89.21%);
  border-radius: 10px;
  width: calc(100% - 40px);
  max-width: 1300px;
  height: 360px;
  margin: 40px auto auto;
`;

export const Container = styled(FlexContainer)`
  max-width: 1170px;
`;

export const MainImage = styled.img`
  display: block;
  width: 460px;
  height: 360px;
  object-fit: cover;
  border-radius: 10px 0px 0px 10px;
`;

export const ContentBlock = styled(FlexContainer)`
  flex-direction: column;
  align-items: flex-start;
  margin-left: auto;
  max-width: 462px;
  margin: 0 20px 0;
`;

export const Title = styled.div`
  font-weight: bold;
  font-size: 32px;
  font-family: Helvetica Neue;
  display: flex;
  line-height: 180%;
  align-items: center;
  text-transform: uppercase;
  margin-top: 18px;
  color: ${mainBlackColor};
`;

export const Description = styled.div`
  color: ${grayTextColor};
  font-family: SF Pro Display;
  margin-bottom: 27px;
  font-size: 16px;
  line-height: 22.4px;
  letter-spacing: 0.013em;

  & p {
    &:first-child {
      font-family: Helvetica Neue;
      color: ${mainBlackColor};
      font-size: 32px;
      padding: 0;
      line-height: 180%;
      font-weight: 500;
      margin-bottom: 21px;
    }
    &:not(:first-child) {
      max-width: 310px;
    }
  }
`;
