import React from 'react';
import {MainImage, Wrapper, Container, ContentBlock, Title, Description} from './styled';
import parse from 'html-react-parser';
import LazyLoad from 'react-lazyload';
import {useLazyQuery} from '@apollo/client';
import {INFO_BANNER} from 'containers/HomePage/queries';
import {useOnScreen} from 'hooks';
import Loader from 'components/Loader';
import _ from 'lodash';

const SingleImageBanner = () => {
  const [get, {data, loading, error, called}] = useLazyQuery(INFO_BANNER, {
    variables: {id: 'secondary-banner-2'}
  });

  const {featuredImage, title, content} = data?.banner || {};

  const ref = useOnScreen({doAction: get});

  if (!called) return <div ref={ref} />;

  if (loading) return <Loader isPartialHeight />;

  if (_.isEmpty(data?.banner) || error) return null;

  return (
    <Wrapper>
      <Container>
        <LazyLoad once>
          <MainImage src={featuredImage?.node?.sourceUrl} alt="Product" />
        </LazyLoad>
        <ContentBlock>
          <Title>{title}</Title>
          <Description>{parse(content || '')}</Description>
        </ContentBlock>
      </Container>
    </Wrapper>
  );
};

export default SingleImageBanner;
