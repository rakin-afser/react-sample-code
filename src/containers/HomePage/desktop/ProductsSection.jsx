import {useLazyQuery} from '@apollo/client';
import {SPECIFIC_PRODUCTS} from 'queries';
import React from 'react';
import WithSlider from 'components/WithSlider';
import ProductCard from 'components/Cards/Product';
import Skeleton from '../components/Skeleton';
import _ from 'lodash';
import {useOnScreen} from 'hooks';

const ProductsSection = ({title, path, variables, marginTop}) => {
  const [get, {data, loading, error, called}] = useLazyQuery(SPECIFIC_PRODUCTS, {variables});
  const ref = useOnScreen({called, doAction: get});

  if (!called) return <Skeleton ref={ref} />;

  if (loading) return <Skeleton />;

  if (_.isEmpty(data?.products?.nodes) || error) return null;

  return (
    <WithSlider
      marginTop={marginTop}
      title={title}
      withSeeMore={data?.products?.pageInfo?.total >= 7}
      hideViewAll={path !== '/deals'} // temporary show view all only for deals
      slidesToScroll={4}
      infinite={false}
      slidesToShow={4}
      to={path}
      seeMoreNumber={data?.products?.pageInfo?.total}
    >
      {data?.products?.nodes?.map((product, index) => (
        <ProductCard hideLens key={index} inQuickView={false} index={index} maxWidth="228px" content={product} />
      ))}
    </WithSlider>
  );
};

export default ProductsSection;
