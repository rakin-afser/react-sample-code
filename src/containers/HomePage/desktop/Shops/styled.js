import {mainWhiteColor} from 'constants/colors';
import styled from 'styled-components';

export const ShopsWrapper = styled.div`
  margin-top: 89px;
  padding-bottom: 50px;
  background: ${mainWhiteColor};
`;
