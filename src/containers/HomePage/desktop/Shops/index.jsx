import React from 'react';
import {useLazyQuery} from '@apollo/client';
import {POPULAR_STORES} from 'containers/HomePage/queries';
import Skeleton from '../../components/Skeleton';
import {ShopsWrapper} from './styled';
import _ from 'lodash';
import WithSlider from 'components/WithSlider';
import CardShop from 'components/CardShop';
import {useHistory} from 'react-router';
import {useOnScreen} from 'hooks';

const Shops = () => {
  const history = useHistory();
  const [get, {data, loading, error, called}] = useLazyQuery(POPULAR_STORES);
  const ref = useOnScreen({called, doAction: get});

  if (!called) return <Skeleton isSpecial ref={ref} />;

  if (loading) return <Skeleton isSpecial />;

  if (_.isEmpty(data?.stores?.nodes) || error) return null;

  return (
    <ShopsWrapper>
      <WithSlider
        dots
        title="Popular Shops"
        type="shops"
        slidesToScroll={5}
        marginTop={0}
        infinite={false}
        slidesToShow={5}
        padding="23px 45px 30px"
        to="/search/stores"
      >
        {data?.stores?.nodes?.map((shop) =>
          shop?.name ? (
            <CardShop
              onClick={() => {
                history.push(`/shop/${shop.storeUrl}/products`);
              }}
              key={shop?.id}
              {...{
                id: shop.id,
                title: shop.name,
                imgSrc: shop.banner,
                rating: shop.rating,
                isFollowing: shop.followed,
                location: shop.location,
                totalFollowers: shop.totalFollowers
              }}
            />
          ) : null
        )}
      </WithSlider>
    </ShopsWrapper>
  );
};

export default Shops;
