import React from 'react';
import Skeleton from '../components/Skeleton';
import CardCategory from 'components/CardCategory';
import {useLazyQuery} from '@apollo/client';
import {POPULAR_CATEGORIES} from 'queries';
import CardsContainer from 'components/CardsContainer';
import _ from 'lodash';
import {useOnScreen} from 'hooks';

const CategoriesSection = () => {
  const [get, {data, loading, error, called}] = useLazyQuery(POPULAR_CATEGORIES);
  const ref = useOnScreen({called, doAction: get});

  if (!called) return <Skeleton ref={ref} />;

  if (loading) return <Skeleton />;

  if (_.isEmpty(data?.productCategories?.nodes) || error) return null;

  return (
    <CardsContainer title="Popular Categories" paddingTop={89}>
      {data?.productCategories?.nodes?.slice(0, 4).map((card) => (
        <CardCategory key={card.id} {...card} />
      ))}
    </CardsContainer>
  );
};

export default CategoriesSection;
