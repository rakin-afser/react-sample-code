import React from 'react';
import {useLazyQuery} from '@apollo/client';
import {SPECIFIC_PRODUCTS} from 'queries';
import Skeleton from '../../components/Skeleton';
import {SliderContainerList, SliderContainer} from './styled';
import WithSlider from 'components/WithSlider';
import WithScroll from 'components/WithScroll';
import CardNewArrival from 'components/CardNewArrival';
import _ from 'lodash';
import {useOnScreen} from 'hooks';

const Products = ({title, path, variables, withSlider, ...props}) => {
  const [get, {data, loading, error, called}] = useLazyQuery(SPECIFIC_PRODUCTS, {variables});
  const ref = useOnScreen({doAction: get, called});

  if (!called) return <Skeleton ref={ref} />;

  if (loading) return <Skeleton />;

  if (_.isEmpty(data?.products?.nodes) || error) return null;

  return withSlider ? (
    <SliderContainerList>
      <WithSlider hideViewAll seeMoreCounter={data?.products?.pageInfo?.total} to={path} title={title} {...props}>
        {data?.products?.nodes?.map((product, index) => (
          <CardNewArrival alwaysImage key={index} node={product} inline />
        ))}
      </WithSlider>
    </SliderContainerList>
  ) : (
    <SliderContainer>
      <WithScroll
        seeMoreCounter={data?.products?.pageInfo?.total}
        to={path}
        title={title}
        hideViewAll={path !== '/deals'} // temporary show view all only for deals
        withSeeMore={data?.products?.pageInfo?.total >= 7}
        height={275}
        {...props}
      >
        {data?.products?.nodes?.map((product, index) => (
          <CardNewArrival key={index} node={product} alwaysImage />
        ))}
      </WithScroll>
    </SliderContainer>
  );
};

export default Products;
