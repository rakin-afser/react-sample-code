import React, {useEffect} from 'react';

import {shopNow} from 'constants/staticData';
import {primaryColor} from 'constants/colors';
import {StartSellingLink} from './styled';

import MainSlider from './components/MainSlider';
import Text from './components/Text';
import CategoriesMobile from './components/CategoriesMobile';
import Divider from './components/Divider';
import BannerMobile from './components/BannerMobile';
import Explore from './components/Explore';
import PopularStores from './components/PopularStores';
import ShopNow from './components/ShopNow';
import Sell from './components/Sell';
import PopularCategories from './components/PopularCategories';
import {useQuery} from '@apollo/client';
import {MAIN_BANNER} from './queries';
import {POPULAR_CATEGORIES} from 'queries';
import {orderByValues} from 'constants/sorting';
import Products from './mobile/Products';

const HomePageMobile = () => {
  const {data} = useQuery(POPULAR_CATEGORIES, {variables: {first: 8}});
  const {data: mainBanner, loading: loadingMainBanner} = useQuery(MAIN_BANNER);

  useEffect(() => {
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }, []);

  const mainSlides = [
    {
      id: 1,
      image: mainBanner?.hero?.featuredImage?.node?.sourceUrl,
      title: mainBanner?.hero?.title || '',
      description: mainBanner?.hero?.content || ''
    }
  ];

  return (
    <>
      <MainSlider slides={mainSlides} touchThreshold={8} speed={100} waitForAnimate={false} />
      <Text>
        Do you have products to Sell?
        <br />
        <StartSellingLink href="https://seller.testSample.com/seller-landing/">
          <strong>
            Start Selling on <span style={{color: primaryColor}}>testSample!</span>
          </strong>
        </StartSellingLink>
      </Text>
      <CategoriesMobile list={data?.productCategories?.nodes} />
      <Divider />
      <Products marginTop={0} title="Deals" path="/deals" variables={{first: 7, where: {is_a_deal_product: true}}} />
      <Divider height={40} />
      <Products
        marginTop={0}
        title="Most Liked"
        path="/most-liked"
        variables={{first: 7, where: {orderby: [{field: 'MOST_LIKED', order: 'DESC'}]}}}
      />
      <Divider height={40} />
      <BannerMobile />
      <Divider height={40} />
      <Products
        marginTop={0}
        withSlider
        title="Featured Products"
        path="/featured"
        variables={{first: 7, where: {featured: true}}}
        slidesToScroll={1}
        infinite={false}
        slidesToShow={1}
        arrows={false}
        dots
        rows={3}
        swipeToSlide
        touchThreshold={8}
      />
      <Divider height={40} />
      <Products
        marginTop={0}
        title="New Arrivals"
        path="/new-arrivals"
        variables={{first: 7, where: {orderby: [{field: 'DATE', order: 'DESC'}]}}}
      />
      <Explore />
      <PopularStores />
      <ShopNow list={shopNow} />
      <Products
        marginTop={0}
        title="Beauty"
        path="/categories/beauty"
        variables={{first: 7, where: {categoryIn: ['beauty']}}}
        showFollow
        followed
      />
      <Divider height={40} />
      <Products
        marginTop={0}
        title="Fragrance"
        path="/categories/fragrance"
        variables={{first: 7, where: {categoryIn: ['fragrance']}}}
        showFollow
        followed
      />
      <Divider height={40} />
      <Products
        marginTop={0}
        withSlider
        title="Recently Viewed"
        path="/recently-viewed"
        variables={{first: 7, where: {recently_viewed: true}}}
        slidesToScroll={1}
        infinite={false}
        slidesToShow={1}
        arrows={false}
        dots
        rows={3}
        touchThreshold={8}
      />
      <Divider />
      <Sell />
      <Divider />
      <Divider />
      <PopularCategories list={data?.productCategories?.nodes} />
    </>
  );
};

export default HomePageMobile;
