import React from 'react';
import Scrollbar from 'react-scrollbars-custom';
import _ from 'lodash';
import {Wrapper, Header, Title, View, List} from './styled';
import Icon from 'components/Icon';
import Store from './components/Store';
import {useOnScreen} from 'hooks';
import {useLazyQuery} from '@apollo/client';
import {POPULAR_STORES} from 'containers/HomePage/queries';
import Skeleton from '../Skeleton';

const PopularStores = () => {
  const [get, {data, loading, error, called}] = useLazyQuery(POPULAR_STORES);
  const ref = useOnScreen({doAction: get, called});

  if (!called) return <Skeleton ref={ref} />;

  if (loading) return <Skeleton />;

  if (_.isEmpty(data?.stores?.nodes) || error) return null;

  return (
    <Wrapper>
      <Header>
        <Title>Popular Stores</Title>
        <View href="search/stores">
          <span>View All</span>
          <Icon width={7} height={11} color="#8F8F8F" type="arrow" />
        </View>
      </Header>
      {data?.stores?.nodes?.length ? (
        <Scrollbar
          disableTracksWidthCompensation
          style={{height: 182}}
          trackXProps={{
            renderer: (props) => {
              const {elementRef, ...restProps} = props;
              return <span {...restProps} ref={elementRef} className="TrackX" />;
            }
          }}
        >
          <List>
            {data?.stores?.nodes?.map((store) => (!store?.name ? null : <Store {...store} key={store?.id} />))}
          </List>
        </Scrollbar>
      ) : null}
    </Wrapper>
  );
};

export default PopularStores;
