import React from 'react';
import {
  Wrapper,
  StoreHeader,
  StoreAvatar,
  StoreName,
  StoreCountry,
  StoreGallery,
  StoreGalleryImage,
  Follow
} from './styled';
import userPlaceholder from 'images/placeholders/user.jpg';
import productPlaceholder from 'images/placeholders/product.jpg';
import {useHistory} from 'react-router';
import {useFollowUnFollowStoreMutation} from 'hooks/mutations';
import parser from 'html-react-parser';

const Store = (props) => {
  const history = useHistory();
  const {id, storeUrl, gravatar, name, address, banner, followed} = props || {};
  const [followUnfollowStore] = useFollowUnFollowStoreMutation({followed});

  const shopClick = (shopUrl) => {
    history.push(`/shop/${shopUrl}/products`);
  };

  return (
    <Wrapper>
      <StoreHeader>
        <StoreAvatar onClick={() => shopClick(storeUrl)} src={gravatar || userPlaceholder} />
        <StoreName>
          {parser(name || '')}
          <StoreCountry>
            {address?.city}, {address?.countryName}
          </StoreCountry>
        </StoreName>
        <Follow
          onClick={() => followUnfollowStore({variables: {input: {id: parseInt(id)}}})}
          kind={followed ? 'following' : 'follow'}
        />
      </StoreHeader>

      <StoreGallery onClick={() => shopClick(storeUrl)}>
        <StoreGalleryImage src={banner || productPlaceholder} />
      </StoreGallery>
    </Wrapper>
  );
};

export default Store;
