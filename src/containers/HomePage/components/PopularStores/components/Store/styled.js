import styled from 'styled-components';
import Btn from 'components/Btn';

export const Wrapper = styled.div`
  min-width: 307px;
  margin: 0 5px;
  display: flex;
  flex-wrap: wrap;

  padding: 10px;
  align-items: flex-start;
  min-height: 182px;
  background: #ffffff;
  border: 1px solid #efefef;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.04);
  border-radius: 4px;
`;

export const StoreHeader = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
`;

export const StoreAvatar = styled.img`
  width: 35px;
  height: 35px;
  border-radius: 50%;
  border: 1px solid #f0f0f0;
  flex-shrink: 0;
`;

export const StoreName = styled.div`
  padding-left: 8px;
  font-weight: 500;
  font-size: 14px;
  line-height: 1.4;
  color: #000000;
  max-width: 55%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

export const StoreCountry = styled.span`
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #666666;
  display: block;
  max-width: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
`;

export const StoreGallery = styled.div`
  display: flex;
  margin: 10px -3px auto;
`;

export const StoreGalleryImage = styled.img`
  display: inline-flex;
  height: 90px;
  border-radius: 4px;
  object-fit: cover;
  margin: 0 3px;
  width: 100%;
`;

export const Follow = styled(Btn)`
  margin-left: auto;
`;
