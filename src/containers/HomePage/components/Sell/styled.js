import styled from 'styled-components/macro';

export const Image = styled.img`
  max-width: 30%;
  object-fit: cover;
`;

export const Wrapper = styled.div`
  height: 225px;
  position: relative;
  z-index: 1;
  display: flex;
  background: linear-gradient(325.09deg, #e8e3dd 25.02%, #f6f4f1 68.72%);
`;

export const Content = styled.div`
  position: relative;
  z-index: 5;
  max-width: 220px;
  padding-left: 20px;
  padding-right: 20px;
  margin-left: auto;
  margin-right: auto;
  align-self: center;
`;

export const Title = styled.p`
  margin: 0 0 8px 0;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 17px;
  display: flex;
  align-items: center;
  color: #000000;
`;

export const Description = styled.p`
  margin: 0 0 16px 0;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 1.4;
  letter-spacing: 0.013em;
  color: #545454;

  & p {
    &:first-child {
      font-family: Helvetica Neue;
      font-style: normal;
      font-weight: 500;
      font-size: 16px;
      color: #000000;
      padding: 0;
      margin-bottom: 16px;
    }
  }
`;

export const Links = styled.div`
  display: flex;

  a {
    &:not(:last-child) {
      margin-right: 8px;
    }
  }

  img {
    max-width: 78px;
  }
`;
