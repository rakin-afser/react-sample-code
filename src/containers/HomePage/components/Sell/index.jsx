import React from 'react';
import PropTypes from 'prop-types';
import parse from 'html-react-parser';

import {Wrapper, Content, Title, Description, Image} from './styled';
import sell from 'images/sell.png';
import {useLazyQuery} from '@apollo/client';
import {INFO_BANNER} from 'containers/HomePage/queries';
import {useOnScreen} from 'hooks';
import Loader from 'components/Loader';

const Sell = ({background}) => {
  const [get, {data, loading, error, called}] = useLazyQuery(INFO_BANNER, {variables: {id: 'secondary-banner-2'}});
  const ref = useOnScreen({doAction: get, called});
  const {title, content, featuredImage} = data?.banner || {};
  const {sourceUrl, altText} = featuredImage?.node || {};

  if (!called) return <div ref={ref} />;

  if (loading) return <Loader isPartialHeight />;

  if (!data?.banner || error) return null;

  return (
    <Wrapper>
      <Image src={sourceUrl} alt={altText} />
      <Content>
        <Title>{title}</Title>
        <Description>{parse(content || '')}</Description>
      </Content>
    </Wrapper>
  );
};

Sell.defaultProps = {
  background: sell
};

Sell.propTypes = {
  background: PropTypes.string
};

export default Sell;
