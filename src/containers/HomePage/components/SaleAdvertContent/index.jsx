import React from 'react';
import {Description, Title, Wrapper} from './styled';
import parse from 'html-react-parser';

const SaleAdvertContent = ({hero}) => {
  if (!hero) {
    return null;
  }
  const {content = '', title = ''} = hero;

  return (
    <Wrapper>
      <Title>{title}</Title>
      <Description>{parse(content)}</Description>
    </Wrapper>
  );
};

export default SaleAdvertContent;
