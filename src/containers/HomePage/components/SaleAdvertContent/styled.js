import styled from 'styled-components/macro';
import {Button} from 'antd';
import {secondaryColor, mainWhiteColor, mainBlackColor} from '../../../../constants/colors';

export const Wrapper = styled.div`
  width: 40%;
`;

export const Title = styled.div`
  color: #303030;
  font-size: 42px;
  font-family: Helvetica Neue;
  letter-spacing: 0.011em;
  font-weight: 700;
  margin-bottom: 38px;
  line-height: 120%;
`;

export const Description = styled.div`
  max-width: 310px;
  font-family: Helvetica Neue;
  font-size: 14px;
  font-size: 14px;
  line-height: 150%;
  margin-bottom: 20px;
  font-weight: 500;
  color: ${mainBlackColor};
`;

export const BuyButton = styled(Button)`
  background: ${secondaryColor}!important;
  padding: 0 !important;
  color: ${mainWhiteColor}!important;
  text-transform: uppercase;
  border: none !important;
  line-height: 32px;
  height: 32px !important;
  width: 142px;
  border-radius: 24px !important;

  span {
    font-size: 14px;
    font-weight: 700;
    letter-spacing: 0px;
  }
`;
