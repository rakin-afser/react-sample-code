import styled from 'styled-components';
import {mainBlackColor} from 'constants/colors';

export const Wrapper = styled.div`
  max-width: 1170px;
  margin: 0 auto;
`;

export const Title = styled.div`
  font-size: 32px;
  letter-spacing: 0.5px;
  color: ${mainBlackColor};
  font-weight: bold;
  position: relative;
  padding-bottom: 9px;
`;

export const Cards = styled.div`
  display: flex;
  flex-direction: ${({isVertical}) => (isVertical ? 'column' : 'row')};
  padding: 32px 65px;
`;

export const Card = styled.div`
  width: ${({isVertical}) => (isVertical ? '100%' : '228px')};
  height: 356px;
  margin-bottom: ${({isVertical}) => (isVertical ? '40px' : '0')};

  & + & {
    margin-left: ${({isVertical}) => (isVertical ? '0' : '42px')};
  }

  @media (max-width: 1024px) {
    height: 86px;
  }
`;
