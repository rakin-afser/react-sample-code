import React, {forwardRef} from 'react';
import SkeletonItem, {SkeletonTheme} from 'react-loading-skeleton';
import {useWindowSize} from '@reach/window-size';
import {Wrapper, Title, Cards, Card} from './styled';

const Skeleton = forwardRef(({title, marginTop, isSpecial, isVertical}, ref) => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  const renderSkeleton = (count) => {
    return (
      <>
        {Array.from(Array(count).keys()).map((elm, idx) => (
          <SkeletonTheme key={idx}>
            <div>
              <SkeletonItem height={isMobile ? 50 : 250} />
              <p>
                <SkeletonItem height={isMobile ? 10 : 20} count={3} />
              </p>
            </div>
          </SkeletonTheme>
        ))}
      </>
    );
  };

  return (
    <Wrapper ref={ref} marginTop={marginTop}>
      <Title>{title}</Title>
      <Cards isVertical={isVertical}>
        <Card isVertical={isVertical}>{renderSkeleton(1)}</Card>
        <Card isVertical={isVertical}>{renderSkeleton(1)}</Card>
        <Card isVertical={isVertical}>{renderSkeleton(1)}</Card>
        <Card isVertical={isVertical}>{renderSkeleton(1)}</Card>
        {isSpecial && <Card isVertical={isVertical}>{renderSkeleton(1)}</Card>}
      </Cards>
    </Wrapper>
  );
});

export default Skeleton;
