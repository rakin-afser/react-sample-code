import React from 'react';
import {Link} from 'react-router-dom';
import {Container, Column, Heading, Item, MainHeading, Row} from './styled';

export const templateData = [
  {
    heading: 'Women’s Clothing',
    list: ['Dresses', 'Activewear', 'Tops', 'Jeans', 'Shorts & Capris', 'Skirts', 'Pants & Leggins', 'Pajamas & Robes']
  },
  {
    heading: 'Lingerie',
    list: [
      'Thongs & Knickerssexy',
      'Lingeriebackless',
      'Bras',
      'Pyjamas',
      'Dressing Gowns',
      'Shapewear',
      'Bodies',
      'Lingerie Sets',
      'Fuller Bust Lingerie DD +'
    ]
  },
  {
    heading: 'Regional Wear',
    subList: [
      {
        heading: 'Arabian Wear',
        list: ['Dresses', 'Abayas', 'Kaftans']
      },
      {
        heading: 'Indian Wear',
        list: ['Sarees', 'Salwar Kameez', 'Lehengas', 'Indo Western']
      },
      {
        heading: 'Pakistani Wear',
        list: [
          'Kurtas & Shalwar Kameez',
          'Unstitched Fabric',
          'Pants, Palazzos & Capris',
          'Dupattas, Stoles & Shawls',
          'Formal Wear'
        ]
      }
    ]
  },
  {
    heading: 'Swimwear',
    list: ['Bikini Sets', 'One-Piece Suits', 'Two-Piece Suits', 'Cover-Ups', 'Men`s Swimwear', 'Children Swimwear']
  },
  {
    heading: 'Maternity',
    list: ['Maternity Wear', 'Maternity Underwear', 'Pajamas & Nightgowns']
  },
  {
    heading: 'Maternity2',
    list: ['Maternity Wear', 'Maternity Underwear', 'Pajamas & Nightgowns']
  },
  {
    heading: 'Men’s Clothing',
    list: [
      'Shirts',
      'Shorts',
      'Jeans',
      'Casual',
      'Pants',
      'Suits & Dresswear',
      'ActiveWear',
      'Swimwear',
      'Underwear',
      'Pajamas & Robes',
      'Workwear'
    ]
  }
];

const getColumn = (content, key, onItemClick, sub) => {
  return (
    <div key={key}>
      {/* <Heading isSub={sub}>{content.name}</Heading> */}
      <Item onClick={onItemClick}>{content.name}</Item>
      {/* {content?.children?.nodes?.map((item) => (
        <Item key={item.id}>{item.name}</Item>
      ))} */}
    </div>
  );
};

const SubcategoryPopup = ({mainHeading, aside, mainCategory}) => {
  return (
    <Container aside={aside}>
      {mainHeading ? <MainHeading>{mainHeading}</MainHeading> : null}
      <Row>
        {mainCategory?.children?.nodes?.map((content) => (
          <Column key={content.id}>
            {content?.children?.nodes ? (
              <div>
                <Heading style={{marginBottom: '6px'}}>
                  <Link to={`/categories/${mainCategory?.slug}/${content?.slug || ''}`}>
                    {content.name}
                  </Link>
                </Heading>
                {content?.children?.nodes?.map((el) =>(
                  <Item>
                    <Link to={`/categories/${mainCategory?.slug}/${content?.slug}/${el?.slug || ''}`}>
                      {el.name}
                    </Link>
                  </Item>
                ))}
              </div>
            ) : (
              getColumn(content)
            )}
          </Column>
        ))}
      </Row>
    </Container>
  );
};

export default SubcategoryPopup;
