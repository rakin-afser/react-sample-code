import styled from 'styled-components/macro';

export const Container = styled.div`
  position: absolute;
  top: ${({aside}) => (aside ? '0px' : 'calc(100% + 2px)')};
  left: ${({aside}) => (aside ? '100%' : '0')};
  right: ${({aside}) => (aside ? 'auto' : '0')};
  background: #fff;
  box-shadow: ${({aside}) => (aside ? 'none' : '0px 4px 15px rgba(0, 0, 0, 0.1)')};
  border-radius: ${({aside}) => (aside ? 'none' : '8px')};
  z-index: 10;
  padding: 36px 37px;
  transition: 0.3s;
  opacity: 1;

  @media (max-width: 1300px) {
    width: 980px;
  }

  &.subcategory-enter-active {
    opacity: 1;
  }

  &.subcategory-enter-done {
    opacity: 1;
  }
`;

export const Column = styled.div`
  margin: 0 19px;

  @media (max-width: 1300px) {
    margin-right: 45px;
  }
`;

export const MainHeading = styled.span`
  display: block;
  font-size: 18px;
  line-height: 25px;
  font-weight: 700;
  color: #000;
  margin: 0 0 21px 4px;
`;

export const Heading = styled.h3`
  font-weight: 500;
  line-height: 17px;
  cursor: pointer;
  color: #000000;
  ${({isSub}) => (isSub ? {fontSize: '13px', marginBottom: '6px'} : {fontSize: '14px', marginBottom: '16px'})};
  a {
    color: #000000;
  }
`;

export const Item = styled.span`
  display: block;
  font-weight: normal;
  font-size: 12px;
  line-height: 140%;
  margin-bottom: 8px;
  color: #414141;
  text-decoration: none;
  &:hover {
    color: #000;
  }
  a {
    color: #414141;
    &:hover {
      color: #000;
    }
  }
`;

export const Row = styled.div`
  display: flex;
`;
