import React from 'react';
import Scrollbar from 'react-scrollbars-custom';

import {Wrapper, Title, List, Card, Thumb, Name} from './styled';
import placehoder from 'images/placeholder.png';

const backgrounds = ['#D5D9D2', '#CDD8D8', '#DED4D3', '#DAD3DA', '#dacdd2', '#e0dfcc'];

const CategoriesMobile = ({title, list}) => {
  const renderCategories = () => {
    const categoriesCards = [];

    for (let i = 0; i <= list.length; i += 2) {
      const {id, name, image, slug} = list[i] || {};
      const {id: idSecond, name: nameSecond, image: imageSecond, slug: slugSecond} = list[i + 1] || {};

      categoriesCards.push(
        <div>
          {name && (
            <Card background={backgrounds[i % backgrounds?.length]} to={`/categories/${slug}`} key={id}>
              <Thumb backgroundImage={image?.sourceUrl || placehoder} />
              <Name>{name}</Name>
            </Card>
          )}
          {nameSecond && (
            <Card background={backgrounds[(i + 1) % backgrounds?.length]} to={`/categories/${slugSecond}`} key={idSecond}>
              <Thumb backgroundImage={imageSecond?.sourceUrl || placehoder} />
              <Name>{nameSecond}</Name>
            </Card>
          )}
        </div>
      );
    }

    return categoriesCards;
  };

  return (
    <Wrapper>
      {title ? <Title>{title}</Title> : null}
      {list && list.length ? (
        <Scrollbar
          disableTracksWidthCompensation
          style={{height: 180}}
          trackXProps={{
            renderer: (props) => {
              const {elementRef, ...restProps} = props;
              return <span {...restProps} ref={elementRef} className="TrackX" />;
            }
          }}
        >
          <List>{renderCategories()}</List>
        </Scrollbar>
      ) : null}
    </Wrapper>
  );
};

CategoriesMobile.defaultProps = {
  title: 'Categories'
};

export default CategoriesMobile;
