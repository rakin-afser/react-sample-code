import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {CSSTransition} from 'react-transition-group';

import {CategoryItem, CategoriesWrapper} from './styled';
import {ContentWrapper} from '../../../../globalStyles';
import SubcategoryPopup from '../SubcategoryPopup';

const CategoriesList = ({list}) => {
  const [isSubcategoryOpen, toggleSubcategoryOpen] = useState(false);
  const [mainCategory, setMainCategory] = useState(null);

  function onMouseEnter(category) {
    toggleSubcategoryOpen(true);
    setMainCategory(category);
  }

  return (
    <ContentWrapper style={{borderBottom: '1px solid #EEEEEE', maxWidth: '1200px', display: 'flex', alignItems: 'flex-start'}}>
      <CategoriesWrapper
        onMouseEnter={() => toggleSubcategoryOpen(true)}
        onMouseLeave={() => toggleSubcategoryOpen(false)}
        justifyContent="flex-start"
      >
        {list?.map((category) => (
          <CategoryItem onMouseEnter={() => onMouseEnter(category)} key={category.id}>
            <Link to={`/categories/${category?.slug}`}>
              {category.name}
            </Link>
          </CategoryItem>
        ))}
        <CSSTransition in={isSubcategoryOpen} timeout={300} classNames="subcategory" unmountOnExit>
          <SubcategoryPopup mainCategory={mainCategory} />
        </CSSTransition>
      </CategoriesWrapper>
    </ContentWrapper>
  );
};

export default CategoriesList;
