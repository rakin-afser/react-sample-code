import styled from 'styled-components/macro';
import {FlexContainer} from '../../../../globalStyles';
import {primaryColor} from '../../../../constants/colors';

export const CategoriesWrapper = styled(FlexContainer)`
  position: relative;
  margin: 13px 0 0 -19px;

  @media (max-width: 1260px) {
    margin-left: 25px;
  }
`;

export const CategoryItem = styled.div`
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: #000;
  cursor: pointer;
  transition: all 0.3s ease-in-out;
  letter-spacing: 0;
  padding: 0 0 6px;
  margin: 0 18px;
  &:hover {
    color: #000;
    box-shadow: 0 2px 0 0 #000;
  }
  a {
    color: #000;
    &:hover {
      color: #000;
    }
  }
`;
