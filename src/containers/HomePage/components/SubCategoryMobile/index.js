import React from 'react';
import PropTypes from 'prop-types';
import Scrollbar from 'react-scrollbars-custom';
import {Wrapper, List, Category, CategoryAvatar, CategoryName} from './styled';

const SubCategoryMobile = ({categories, onClick}) => {
  return (
    <Wrapper>
      {categories && categories.length ? (
        <Scrollbar
          disableTracksWidthCompensation
          trackXProps={{
            renderer: (props) => {
              const {elementRef, ...restProps} = props;
              return <span {...restProps} ref={elementRef} className="TrackX" />;
            }
          }}
        >
          <List>
            {categories.map((item, key) => {
              return (
                <Category key={key} onClick={() => onClick(item)}>
                  <CategoryAvatar src={item.imgSrc} />
                  <CategoryName>{item.name}</CategoryName>
                </Category>
              );
            })}
          </List>
        </Scrollbar>
      ) : null}
    </Wrapper>
  );
};

SubCategoryMobile.defaultProps = {
  categories: []
};

SubCategoryMobile.propTypes = {
  categories: PropTypes.array
};

export default SubCategoryMobile;
