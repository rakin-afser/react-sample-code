import styled from 'styled-components';

import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  .TrackX {
    display: none;
  }

  .ScrollbarsCustom {
    height: 92px !important;
  }
`;

export const List = styled.div`
  display: flex;
`;

export const Category = styled.div`
  margin-left: 16px;
`;

export const CategoryAvatar = styled.img`
  width: 66px;
  height: 54px;
  border-radius: 4px;
  object-fit: cover;
`;

export const CategoryName = styled.div`
  font-weight: 400;
  font-size: 12px;
  line-height: 14.3px;
  color: #000000;
  margin-bottom: 9px;
  width: 100%;
  text-align: center;
  margin-top: 8px;
`;
