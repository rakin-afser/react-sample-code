import React from 'react';
import Scrollbar from 'react-scrollbars-custom';

import {
  Wrapper,
  Header,
  Title,
  List,
  Category,
  CategoryAvatar,
  CategoryContent,
  CategoryName,
  CategoryFollowers,
  CategoryFollow
} from './styled';
import Plus from 'assets/Plus';
import {Link} from 'react-router-dom';
import productPlaceholder from 'images/placeholders/product.jpg';

const PopularCategories = ({title, list}) => {
  return (
    <Wrapper>
      <Header>
        <Title>{title}</Title>
      </Header>
      {list && list?.length ? (
        <Scrollbar
          disableTracksWidthCompensation
          style={{height: 74}}
          trackXProps={{
            renderer: (props) => {
              const {elementRef, ...restProps} = props;
              return <span {...restProps} ref={elementRef} className="TrackX" />;
            }
          }}
        >
          <List>
            {list.map((item) => (
              <Category key={item?.id}>
                <Link to={`/categories/${item?.slug}`}>
                  <CategoryAvatar src={item?.image?.sourceUrl || productPlaceholder} />
                </Link>
                <CategoryContent>
                  <CategoryName to="categories">{item?.name}</CategoryName>
                  <CategoryFollowers>{item?.totalFollowers || '0'} followers</CategoryFollowers>
                  <CategoryFollow>
                    <Plus />
                  </CategoryFollow>
                </CategoryContent>
              </Category>
            ))}
          </List>
        </Scrollbar>
      ) : null}
    </Wrapper>
  );
};

export default PopularCategories;
