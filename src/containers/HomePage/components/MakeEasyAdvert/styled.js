import styled from 'styled-components';
import {FlexContainer} from '../../../../globalStyles';
import {mainWhiteColor, primaryColor, mainBlackColor} from '../../../../constants/colors';

export const Box = styled(FlexContainer)`
  justify-content: center;
  align-items: flex-start;
  flex-direction: column;
  width: 512px;
  height: 254px;
  padding: 50px;
  background: ${mainWhiteColor};
  border-radius: 8px;
`;

export const Signup = styled.div`
  border-color: #999 !important;
  color: #666 !important;
  max-width: 186px;
  width: 100%;
  margin-top: 13px;
  height: 40px !important;
  display: inline-flex !important;
  align-items: center;
  justify-content: center;
`;

export const WideTitle = styled.div`
  font-weight: 500;
  font-size: 24px;
  line-height: 31.61px;
  color: ${mainBlackColor};
  margin-bottom: 30px;
  letter-spacing: -0.55px;
`;

export const SecondaryText = styled.div`
  font-weight: normal;
  font-size: 16px;
  line-height: 19.2px;
  color: ${mainBlackColor};
  & button {
    margin-top: 14px;
    width: 142px;
    height: 35px;
    font-size: 14px;
  }
`;

export const Link = styled.span`
  color: ${primaryColor};
  cursor: pointer;
  text-decoration: underline;
`;
