import React from 'react';
import parse from 'html-react-parser';

import {SecondaryText, WideTitle, Box} from './styled';
import Btn from 'components/Btn';

const MakeEastAdvert = ({banner}) => {
  if (!banner) {
    return null;
  }

  const {content = '', title = ''} = banner;

  return (
    <Box>
      <WideTitle>{title}</WideTitle>
      <SecondaryText>
        {parse(content)}
        <Btn kind="primary-bold">Sign Up Now</Btn>
      </SecondaryText>
    </Box>
  );
};

export default MakeEastAdvert;
