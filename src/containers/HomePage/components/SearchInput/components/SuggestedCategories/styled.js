import {Tabs} from 'antd';
import {primaryColor} from 'constants/colors';
import styled from 'styled-components';

export const StyledTabs = styled(Tabs)`
  .ant-tabs {
    &-bar {
      border-bottom: 2px solid #f5f5f5;
    }

    &-ink-bar {
      bottom: 9px;
    }

    &-nav {
      width: 100%;
      color: #999999;

      > div {
        display: flex;
      }

      .ant-tabs {
        &-tab {
          margin-right: 0;
          padding-left: 0;
          padding-right: 0;
          &:not(:first-child):not(:last-child) {
            margin-left: auto;
            margin-right: auto;
          }

          &:first-child {
            margin-right: auto;
          }

          &:last-child {
            margin-left: auto;
          }

          &:hover {
            color: ${primaryColor};
          }
        }

        &-tab-active {
          color: #000;
          font-weight: 400;
        }
      }
    }

    &-ink-bar {
      background-color: ${primaryColor};
    }
  }
`;
