import React from 'react';
import Btn from 'components/Btn';
import {
  useFollowUnfollowListMutation,
  useFollowUnFollowStoreMutation,
  useFollowUnfollowUserMutation
} from 'hooks/mutations';

const FollowBtnUser = (props) => {
  const {databaseId, id, isFollowed} = props;
  const [followUnfollow] = useFollowUnfollowUserMutation({id, isFollowed});

  return (
    <Btn
      kind={isFollowed ? 'msg-square' : 'follow-text'}
      mr={isFollowed ? '14px' : null}
      ml="auto"
      onClick={isFollowed ? null : () => followUnfollow({variables: {input: {id: databaseId}}})}
    />
  );
};

const FollowBtnStore = (props) => {
  const {id, followed} = props;
  const [followUnfollow] = useFollowUnFollowStoreMutation({followed});

  return (
    <Btn
      kind={followed ? 'following-text' : 'follow-text'}
      ml="auto"
      onClick={() => followUnfollow({variables: {input: {id: parseInt(id)}}})}
    />
  );
};

const FollowBtnList = (props) => {
  const {id, isFollowed, list_id: listId} = props;
  const [followUnfollow] = useFollowUnfollowListMutation({id, isFollowed});

  return (
    <Btn
      kind={isFollowed ? 'following-text' : 'follow-text'}
      ml="auto"
      onClick={() => followUnfollow({variables: {input: {id: listId}}})}
    />
  );
};

export {FollowBtnUser, FollowBtnStore, FollowBtnList};
