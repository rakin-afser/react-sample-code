import {primaryColor} from 'constants/colors';
import {Link} from 'react-router-dom';
import styled from 'styled-components';

export const ResultCategory = styled.div`
  margin: 0 0 24px 0;
`;

export const CategoryHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #c3c3c3;
  margin: 0;
  padding: 0 0 8px 0;
`;

export const CategoryHeading = styled.span`
  font-size: 14px;
  line-height: 20px;
  font-weight: 700;
  margin: 0;
  color: #000;
`;

export const ViewAll = styled(Link)`
  display: flex;
  align-items: flex-start;
  flex-grow: 1;
  justify-content: flex-end;
  font-size: 14px;
  color: #8f8f8f;
  transition: color ease 0.4s;
  cursor: pointer;

  &:hover {
    color: ${primaryColor};
    & svg {
      margin-left: 10px;
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const Empty = styled.p`
  font-size: 12px;
  margin-bottom: 0;
  color: #999;
  text-align: center;
`;
