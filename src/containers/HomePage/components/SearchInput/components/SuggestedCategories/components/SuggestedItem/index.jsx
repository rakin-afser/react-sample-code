import React from 'react';
import {
  BlockContent,
  CategoryItem,
  ItemDescription,
  ItemName,
  ItemPic,
  ItemText,
  SmallItem,
  StyledLink,
  ViewButton
} from './styled';
import productPlaceholder from 'images/placeholders/product.jpg';
import userPlaceholder from 'images/placeholders/user.jpg';
import {Link, useHistory} from 'react-router-dom';
import {FollowBtnList, FollowBtnStore, FollowBtnUser} from './components/FollowButtons';

const SuggestedItem = ({page, data}) => {
  const {push} = useHistory();
  function renderProdShortInfo(p) {
    const {shippingType, totalLikes} = p || {};
    if (shippingType) return <ItemText product>{shippingType}</ItemText>;
    if (totalLikes) {
      return <ItemText product>{`${totalLikes} Like${totalLikes?.toString()?.endsWith('1') ? '' : 's'}`}</ItemText>;
    }
    return null;
  }

  switch (page) {
    case 'lists':
      return data?.map((item) => (
        <CategoryItem key={item?.id}>
          <Link to={`/profile/${item?.user_id}/lists/${item?.list_id}`}>
            <ItemPic src={item?.image || productPlaceholder} />
          </Link>
          <ItemDescription>
            <StyledLink to={`/profile/${item?.user_id}/lists/${item?.list_id}`}>
              <ItemName>{item.list_name}</ItemName>
            </StyledLink>
            <ItemText>{item.totalFollowers} followers</ItemText>
          </ItemDescription>
          <FollowBtnList {...item} />
        </CategoryItem>
      ));
    case 'posts':
      return data?.map((item) => (
        <CategoryItem key={item?.id}>
          <Link to={`/post/${item?.databaseId}`}>
            <ItemPic
              src={
                item?.galleryImages?.nodes?.[0]?.sourceUrl ||
                item?.galleryImages?.nodes?.[0]?.poster?.sourceUrl ||
                productPlaceholder
              }
              alt={item?.slug}
            />
          </Link>
          <ItemDescription>
            <StyledLink to={`/post/${item?.databaseId}`}>
              <ItemName>{item?.title}</ItemName>
            </StyledLink>
            {item?.totalLikes ? <ItemText>{item?.totalLikes} Likes</ItemText> : null}
          </ItemDescription>
          <ViewButton to={`/post/${item?.databaseId}`}>View</ViewButton>
        </CategoryItem>
      ));
    case 'stores':
      return data?.map((item) => (
        <CategoryItem key={item?.id}>
          <Link to={`/shop/${item?.storeUrl}/products`}>
            <ItemPic src={item?.gravatar || userPlaceholder} />
          </Link>
          <ItemDescription>
            <StyledLink to={`/shop/${item?.storeUrl}/products`}>
              <ItemName>{item.name}</ItemName>
            </StyledLink>
            <ItemText>{item.totalFollowers} followers</ItemText>
          </ItemDescription>
          <FollowBtnStore {...item} />
        </CategoryItem>
      ));
    case 'users':
      return data?.map((item) => (
        <CategoryItem key={item?.id}>
          <Link to={`/profile/${item?.databaseId}/posts`}>
            <ItemPic src={item?.avatar?.url || userPlaceholder} alt={item?.nickname} />
          </Link>
          <ItemDescription>
            <StyledLink to={`/profile/${item?.databaseId}/posts`}>
              <ItemName>{item?.username}</ItemName>
            </StyledLink>
            <ItemText>
              {item?.totalFollowers} followers {item?.isFollowed ? '| Following' : null}
            </ItemText>
          </ItemDescription>
          <FollowBtnUser {...item} />
        </CategoryItem>
      ));
    case 'products':
      return (
        <BlockContent>
          {data?.map((item) => (
            <SmallItem key={item.id} onClick={() => push(`/product/${item?.slug}`)}>
              <Link to={`/product/${item?.slug}`}>
                <ItemPic src={item?.image?.sourceUrl || productPlaceholder} />
              </Link>
              <ItemDescription>
                <StyledLink to={`/product/${item?.slug}`}>
                  <ItemName product>{item?.name}</ItemName>
                </StyledLink>
                {renderProdShortInfo(item)}
              </ItemDescription>
            </SmallItem>
          ))}
        </BlockContent>
      );
    default:
      return null;
  }
};

export default SuggestedItem;
