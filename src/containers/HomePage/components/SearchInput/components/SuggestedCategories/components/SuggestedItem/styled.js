import {Link} from 'react-router-dom';
import styled, {css} from 'styled-components';

export const CategoryItem = styled.div`
  display: flex;
  align-items: center;
  margin: 16px 0 0 0;
`;

export const ItemPic = styled.img`
  width: 42px;
  height: 42px;
  border-radius: 50%;
  object-fit: cover;
`;

export const ItemText = styled.div`
  font-size: 12px;
  color: #7a7a7a;

  ${({product}) =>
    product &&
    css`
      white-space: nowrap;
      max-width: 140px;
      overflow: hidden;
      text-overflow: ellipsis;
    `}
`;

export const SmallItem = styled.div`
  display: flex;
  align-items: center;
  width: calc(50% - 15px);
  min-width: 165px;
  margin: 15px 10px 0 0;

  &:nth-child(2n) {
    margin-right: 0;
  }
`;

export const BlockContent = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export const StyledLink = styled(Link)`
  display: inline-block;
`;

export const ViewButton = styled(Link)`
  font-size: 14px;
  font-weight: 700;
  padding: 3px 11px 3px 11px;
  color: #545454;
  margin-left: auto;
`;

export const ItemDescription = styled.div`
  margin: 0 0 0 16px;
  flex-grow: 1;
  max-width: 68%;
`;

export const ItemName = styled.span`
  font-size: 14px;
  line-height: 20px;
  color: #000000;
  overflow: hidden;
  white-space: nowrap;
  max-width: 260px;
  text-overflow: ellipsis;
  display: inherit;

  ${({product}) =>
    product &&
    css`
      max-width: 140px;
    `}
`;
