import Loader from 'components/Loader';
import {
  useListsQuery,
  usePostsSearchInputQuery,
  useSearchInputProductsQuery,
  useSearchInputQuery,
  useStoresSearchQuery,
  useUsersSearchQuery
} from 'hooks/queries';
import React from 'react';
import SuggestedItem from '../SuggestedItem';
import _ from 'lodash';
import {ResultCategory, CategoryHeading, CategoryHeader, ViewAll, Empty} from './styled';

const All = ({search}) => {
  const {data, loading} = useSearchInputQuery({variables: {search}});

  const s = search ? `?search=${search}` : '';

  const dataSearch = [
    {slug: 'products', name: 'Products', children: data?.products?.nodes || []},
    {slug: 'stores', name: 'Stores', children: data?.stores?.nodes || []},
    {slug: 'posts', name: 'Posts', children: data?.posts?.nodes || []},
    {slug: 'lists', name: 'Lists', children: data?.lists?.nodes || []},
    {slug: 'users', name: 'Users', children: data?.users?.nodes || []}
  ];

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

  if (!dataSearch?.filter((d) => d?.children?.length)?.length) return <Empty>No results found!</Empty>;

  return dataSearch?.map((page) =>
    _.isEmpty(page?.children) ? null : (
      <ResultCategory key={page?.slug}>
        <CategoryHeader>
          <CategoryHeading>{page.name}</CategoryHeading>
          <ViewAll to={`/search/${page.slug}${s}`}>View All</ViewAll>
        </CategoryHeader>
        <SuggestedItem page={page?.slug} data={page?.children} />
      </ResultCategory>
    )
  );
};

const Products = ({search}) => {
  const {data, loading} = useSearchInputProductsQuery({variables: {where: {search}}});

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

  if (!data?.products?.nodes?.length) return <Empty>No results found!</Empty>;

  return <SuggestedItem data={data?.products?.nodes} page="products" />;
};

const Users = ({search}) => {
  const {data, loading} = useUsersSearchQuery({variables: {where: {search}}});

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

  if (!data?.users?.nodes?.length) return <Empty>No results found!</Empty>;

  return <SuggestedItem data={data?.users?.nodes} page="users" />;
};

const Stores = ({search}) => {
  const {data, loading} = useStoresSearchQuery({variables: {where: {search}}});

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

  if (!data?.stores?.nodes?.length) return <Empty>No results found!</Empty>;

  return <SuggestedItem data={data?.stores?.nodes} page="stores" />;
};

const Posts = ({search}) => {
  const {data, loading} = usePostsSearchInputQuery({variables: {where: {search}}});

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

  if (!data?.posts?.nodes?.length) return <Empty>No results found!</Empty>;

  return <SuggestedItem data={data?.posts?.nodes} page="posts" />;
};

const Lists = ({search}) => {
  const {data, loading} = useListsQuery({variables: {where: {list_name: search}}});

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="100%" />;

  if (!data?.lists?.nodes?.length) return <Empty>No results found!</Empty>;

  return <SuggestedItem data={data?.lists?.nodes} page="lists" />;
};

export {All, Products, Users, Stores, Posts, Lists};
