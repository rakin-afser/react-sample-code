import React from 'react';
import {StyledTabs} from './styled';
import {All, Products, Users, Posts, Stores, Lists} from './components/SearchLists';

const SuggestedCategories = ({search, dropdownValue, setDropdownValue}) => {
  return (
    <StyledTabs
      onChange={setDropdownValue}
      defaultActiveKey={dropdownValue}
      animated={{tabPane: false}}
      destroyInactiveTabPane
    >
      <StyledTabs.TabPane tab="All" key="all">
        <All search={search} />
      </StyledTabs.TabPane>
      <StyledTabs.TabPane tab="Users" key="users">
        <Users search={search} />
      </StyledTabs.TabPane>
      <StyledTabs.TabPane tab="Products" key="products">
        <Products search={search} />
      </StyledTabs.TabPane>
      <StyledTabs.TabPane tab="Stores" key="stores">
        <Stores search={search} />
      </StyledTabs.TabPane>
      <StyledTabs.TabPane tab="Posts" key="posts">
        <Posts search={search} />
      </StyledTabs.TabPane>
      <StyledTabs.TabPane tab="Lists" key="lists">
        <Lists search={search} />
      </StyledTabs.TabPane>
    </StyledTabs>
  );
};

export default SuggestedCategories;
