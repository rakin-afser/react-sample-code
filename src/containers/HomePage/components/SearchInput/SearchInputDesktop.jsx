import React, {useState, useRef} from 'react';
import {CSSTransition} from 'react-transition-group';
import _ from 'lodash';
import Scrollbar from 'components/Scrollbar';

import SearchIcon from 'assets/SearchIcon';
import RecentlyLink from 'assets/ArrowLeft';
import {
  Search,
  SearchButton,
  SearchContainer,
  RecentlySearch,
  RecentlyList,
  ClearRecently,
  RecentlyHeader,
  RecentTitle,
  RecentlyItem,
  SearchPopup,
  CloseOverlay,
  SearchContent
} from 'containers/HomePage/components/SearchInput/styled';
import useRecentSearches from 'util/useRecentSearches';
import PopularTags from 'components/PopularTags';
// import {GET_CATEGORIES_BY_TEXT} from 'containers/SearchResult/api/queries';
import useDebounce from 'hooks/useDebounce';
import SuggestedCategories from './components/SuggestedCategories';

const Item = ({key, onClick, text, type, category}) => (
  <RecentlyItem key={key} onClick={onClick}>
    {text}&nbsp;
    <span>
      in&nbsp;
      {type === 'products' && category ? category : _.capitalize(type)}
    </span>
    <RecentlyLink />
  </RecentlyItem>
);

const SearchInputDesktop = ({isShow, open, isFocus, hint, onSubmit: onSearch}) => {
  const [inputValue, setInputValue] = useState(null);
  const {recentSearches, saveSearch, clearRecentSearches} = useRecentSearches();
  // const [searchList, setSearchList] = useState([]);
  const [dropdownValue, setDropdownValue] = useState('all');
  const searchInput = useRef(null);
  const debouncedSearchTerm = useDebounce(inputValue, 300);

  // const [getCategoriesByText] = useLazyQuery(GET_CATEGORIES_BY_TEXT, {
  //   onCompleted(data) {
  //     const filteredCategories = data?.productCategories?.nodes?.filter((c) => c?.products?.nodes?.length > 0);
  //     if (inputValue) {
  //       setSearchList(filteredCategories || []);
  //     }
  //   }
  // });

  function setValue(ev) {
    if (ev.type === 'focus') {
      hint(true);
      return;
    }
    const value = ev.target.innerText || ev.target.value;
    setInputValue(value);
  }

  function closeHint(ev) {
    const t = ev.target;
    setTimeout(() => {
      if (!t.value.length) {
        hint(false);
      }
    }, 100);
  }

  const handleKeyDown = (e) => {
    if (e.key === 'Enter') {
      onSubmit(
        inputValue,
        dropdownValue,
        false
        //  {name: searchList?.[0]?.name, slug: searchList?.[0]?.slug}
      );
    }
  };

  const onSubmit = (searchString, searchType, doNotSave = false, categoryObj) => {
    if (!doNotSave) {
      saveSearch(searchString, searchType, categoryObj?.slug, categoryObj?.name);
    }

    const st = searchType === 'all' ? 'products' : dropdownValue;

    onSearch(searchString, st, {categories: [categoryObj?.slug]});
    setInputValue('');
    searchInput.current.blur();
  };

  function renderContent() {
    function renderRecentSearches() {
      if (_.isEmpty(recentSearches))
        return (
          <RecentlyList>
            <RecentlyItem empty>You don't have any search history yet</RecentlyItem>
          </RecentlyList>
        );

      if (recentSearches?.slice(-5)?.length)
        return (
          <RecentlyList>
            {recentSearches?.slice(-5)?.map((item, i) => (
              <Item
                key={i}
                onClick={() =>
                  onSubmit(item?.text, item?.type, true, {name: item?.categoryName, slug: item?.categorySlug})
                }
                text={item?.text}
                type={item?.type}
                category={item?.categoryName}
              />
            ))}
            {/* {searchList?.map((item) => (
              <Item
                key={item?.id}
                onClick={() => onSubmit(inputValue, 'products', false, {name: item?.name, slug: item?.slug})}
                // text={item?.keyword}
                // type={item?.type}
                text={inputValue}
                type="products"
                category={item?.name}
              />
            ))} */}
          </RecentlyList>
        );

      return null;
    }

    if (_.isEmpty(inputValue))
      return (
        <>
          <RecentlyHeader>
            <RecentTitle>Recent</RecentTitle>
            <ClearRecently onClick={clearRecentSearches}>Clear All</ClearRecently>
          </RecentlyHeader>
          {renderRecentSearches()}
          <RecentlyHeader>
            <RecentTitle>Trending Now</RecentTitle>
          </RecentlyHeader>
          <RecentlyList>
            <PopularTags filled />
          </RecentlyList>
        </>
      );

    if (!_.isEmpty(debouncedSearchTerm))
      return (
        <SuggestedCategories
          search={debouncedSearchTerm}
          dropdownValue={dropdownValue}
          setDropdownValue={setDropdownValue}
        />
      );

    return null;
  }

  return (
    <SearchContainer isFocus={isFocus}>
      <CloseOverlay className="close-search-popup" overlay={isShow || isFocus} />
      <Search
        onKeyDown={handleKeyDown}
        value={inputValue}
        placeholder="Search"
        onInput={(ev) => setValue(ev)}
        onFocus={(ev) => setValue(ev)}
        onBlur={(ev) => closeHint(ev)}
        ref={searchInput}
      />
      <SearchButton onClick={() => onSubmit(inputValue, dropdownValue)}>
        <SearchIcon />
      </SearchButton>
      <SearchPopup>
        <CSSTransition in={isFocus} timeout={300} classNames="autocomplete" unmountOnExit>
          <RecentlySearch>
            {_.isEmpty(inputValue) ? (
              <SearchContent>{renderContent()}</SearchContent>
            ) : (
              <Scrollbar autoHeight autoHeightMax={400} autoHide>
                <SearchContent>{renderContent()}</SearchContent>
              </Scrollbar>
            )}
          </RecentlySearch>
        </CSSTransition>
      </SearchPopup>
    </SearchContainer>
  );
};

export default SearchInputDesktop;
