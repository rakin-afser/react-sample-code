import React from 'react';
import Badges from '../../../../components/Badges';

const category = ['Products', 'Stores', 'Posts', 'Lists'];

const SearchInputMobile = () => {
  return <Badges action={() => console.log('search')} name="search" />;
};

export default SearchInputMobile;
