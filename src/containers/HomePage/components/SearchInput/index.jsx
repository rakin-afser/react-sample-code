import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import qs from 'qs';

import SearchInputDesktop from './SearchInputDesktop';
import SearchInputMobile from './SearchInputMobile';

const SearchInput = ({isMobile}) => {
  const history = useHistory();
  const [showResult, setShowResult] = useState(false);
  const [showRecent, setShowRecent] = useState(false);

  document.addEventListener('click', (ev) => {
    const t = ev.target;
    const close = t.classList.contains('close-search-popup');
    if (close) {
      closeHandler();
    }
  });

  const closeHandler = () => {
    setShowResult(false);
    setShowRecent(false);
  };

  const onSubmit = (search, page, filters) => {
    const queryString = qs.stringify({filters, search});
    history.push(`/search${page ? `/${page}` : ''}?${queryString}`);
    closeHandler();
  };

  return isMobile ? (
    <SearchInputMobile />
  ) : (
    <SearchInputDesktop
      closeHandler={closeHandler}
      isShow={showResult}
      open={setShowResult}
      isFocus={showRecent}
      hint={setShowRecent}
      onSubmit={onSubmit}
    />
  );
};

export default SearchInput;
