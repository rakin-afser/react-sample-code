import React, {useEffect, useRef} from 'react';
import Slider from 'react-slick';
import parse from 'html-react-parser';

import {
  MainSliderWrapper,
  SlideWrapper,
  SlideContent,
  SlideTitle,
  SlideDescription,
  SlideButton,
  SlideImage,
  TextWrapper
} from './styled';

const MainSlider = ({slides}) => {
  const settings = {
    arrows: false
  };

  let firstClientX;
  let clientX;

  const preventTouch = (e) => {
    const minValue = 5;

    clientX = e.touches[0].clientX - firstClientX;

    if (Math.abs(clientX) > minValue) {
      e.preventDefault();
      e.returnValue = false;

      return false;
    }
  };

  const touchStart = (e) => {
    firstClientX = e.touches[0].clientX;
  };

  const containerRef = useRef();

  useEffect(() => {
    if (containerRef.current) {
      containerRef.current.addEventListener('touchstart', touchStart);
      containerRef.current.addEventListener('touchmove', preventTouch, {
        passive: false
      });
    }

    return () => {
      if (containerRef.current) {
        containerRef.current.removeEventListener('touchstart', touchStart);
        containerRef.current.removeEventListener('touchmove', preventTouch, {
          passive: false
        });
      }
    };
  });

  return (
    <MainSliderWrapper ref={containerRef}>
      <Slider {...settings}>
        {slides
          ? slides.map((slide) => {
              return (
                <SlideWrapper key={slide.id}>
                  <SlideContent>
                    <TextWrapper isRTLBanner={slide?.isRTL}>
                      {slide.title ? <SlideTitle>{parse(slide?.title)}</SlideTitle> : null}
                      {slide.description ? <SlideDescription>{parse(slide?.description)}</SlideDescription> : null}
                      {slide.url ? <SlideButton to={slide.url}>Get started</SlideButton> : null}
                    </TextWrapper>
                    {slide?.image && <SlideImage src={slide.image} />}
                  </SlideContent>
                </SlideWrapper>
              );
            })
          : null}
      </Slider>
    </MainSliderWrapper>
  );
};

export default MainSlider;
