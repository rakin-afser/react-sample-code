import styled from 'styled-components/macro';

export const Wrapper = styled.div`
  background: #f0f0f0;
  max-height: 380px;
`;

export const WrapperContent = styled.div`
  background: #ffffff;
  border-radius: 4px;
  padding: 13px;
  margin: 0 18px 0 auto;
  max-width: 50%;
`;

export const Background = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 33px 0;
  min-height: 225px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: right;
  background-image: ${({background}) => `url(${background})` || 'none'};
`;

export const Title = styled.p`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  line-height: 16px;
  color: #000000;
`;

export const Description = styled.div`
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 14px;
  color: #666666;
`;

export const ButtonLink = styled.button`
  display: inline-block;
  margin-top: 10px;
  padding: 8px 20px;
  line-height: normal;
  background: #ed484f;
  border-radius: 20px;
  font-family: Helvetica Neue;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #ffffff;
`;
