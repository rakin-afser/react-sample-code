import React from 'react';
import PropTypes from 'prop-types';
import parse from 'html-react-parser';

import bannerMobile from 'images/bannerMobile.jpg';
import {Wrapper, WrapperContent, Background, Title, Description, ButtonLink} from './styled';
import {useLazyQuery} from '@apollo/client';
import {INFO_BANNER} from 'containers/HomePage/queries';
import {useOnScreen} from 'hooks';
import Loader from 'components/Loader';

const BannerMobile = ({background}) => {
  const [get, {data, loading, error, called}] = useLazyQuery(INFO_BANNER, {variables: {id: 'secondary-banner-1'}});
  const ref = useOnScreen({doAction: get, called});

  const {content, title} = data?.banner || {};

  if (!called) return <div ref={ref} />;

  if (loading) return <Loader isPartialHeight />;

  if (!data?.banner || error) return null;

  return (
    <Wrapper>
      <Background background={background}>
        <WrapperContent>
          <Title>{title}</Title>
          <Description>{parse(content || '')}</Description>
          <ButtonLink>Sign Up Now</ButtonLink>
        </WrapperContent>
      </Background>
    </Wrapper>
  );
};

BannerMobile.defaultProps = {
  background: bannerMobile
};

BannerMobile.propTypes = {
  background: PropTypes.string
};

export default BannerMobile;
