import styled from 'styled-components';

export const Wrapper = styled.div`
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: #000000;
  padding-top: 24px;
  padding-bottom: 24px;
  background: #fafafa;
`;
