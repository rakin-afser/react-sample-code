import React from 'react';
import {useQuery} from '@apollo/client';

import CategoriesList from './components/CategoriesList';
import ContentWithBackground from './components/ContentWithBackground';
import SaleAdvertContent from './components/SaleAdvertContent';
import {BoldText, MainBannerWrapper, MainColor, SellTitle} from './styled';
import {NAV_CATEGORIES} from 'queries';
import {MAIN_BANNER} from './queries';
import ProductsSection from './desktop/ProductsSection';
import CategoriesSection from './desktop/CategoriesSection';
import Shops from './desktop/Shops';
import InfoBanner from './desktop/InfoBanner';
import SingleImageBanner from './desktop/SingleImageBanner';
import Skeleton from 'react-loading-skeleton';

const HomePageDesktop = () => {
  const {data} = useQuery(NAV_CATEGORIES);
  const {data: mainBanner, loading: loadingMainBanner} = useQuery(MAIN_BANNER);

  function renderMainBanner() {
    if (loadingMainBanner)
      return (
        <MainBannerWrapper>
          <Skeleton
            style={{
              borderRadius: '10px'
            }}
            height={320}
          />
        </MainBannerWrapper>
      );

    return (
      <ContentWithBackground
        background={mainBanner?.hero?.featuredImage?.node?.sourceUrl}
        style={{
          backgroundColor: mainBanner?.hero ? '#F0F0F0' : 'transparent',
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'contain',
          backgroundPosition: 'right center',
          borderRadius: '10px',
          margin: '10px auto 12px',
          maxWidth: '1300px',
          width: 'calc(100% - 40px)',
          paddingLeft: '20px',
          paddingRight: '20px'
        }}
      >
        <SaleAdvertContent hero={mainBanner?.hero} />
      </ContentWithBackground>
    );
  }

  return (
    <>
      <CategoriesList list={data?.productCategories?.nodes} />
      {renderMainBanner()}
      <SellTitle>
        <span>Do you have products to Sell? </span>
        <BoldText href="https://seller.testSample.com/seller-landing/">
          <strong>
            Start Selling on <MainColor>testSample!</MainColor>
          </strong>
        </BoldText>
      </SellTitle>

      <ProductsSection
        marginTop={31}
        title="Deals"
        path="/deals"
        variables={{first: 7, where: {is_a_deal_product: true}}}
      />

      <InfoBanner />

      <ProductsSection
        marginTop={74}
        title="Featured Products"
        path="/featured"
        variables={{first: 7, where: {featured: true}}}
      />

      <ProductsSection
        marginTop={88}
        title="New Arrivals"
        path="/new-arrivals"
        variables={{first: 7, where: {orderby: [{field: 'DATE', order: 'DESC'}]}}}
      />

      <Shops />

      <ProductsSection
        marginTop={31}
        title="Beauty"
        path="/categories/beauty"
        variables={{first: 7, where: {categoryIn: ['beauty']}}}
      />

      <ProductsSection
        marginTop={89}
        title="Fragrance"
        path="/categories/fragrance"
        variables={{first: 7, where: {categoryIn: ['fragrance']}}}
      />

      <SingleImageBanner />

      <ProductsSection
        marginTop={89}
        title="Recently Viewed"
        path="/recently-viewed"
        variables={{first: 7, where: {recently_viewed: true}}}
      />

      <CategoriesSection />
    </>
  );
};

export default HomePageDesktop;
