import {gql} from '@apollo/client';

export const BannerFragment = gql`
  fragment BannerFragment on Banner {
    id
    title
    content
    featuredImage {
      node {
        id
        sourceUrl(size: LARGE)
        altText
      }
    }
  }
`;

export const MAIN_BANNER = gql`
  ${BannerFragment}
  query getMainBanner {
    hero: banner(id: "main-banner-1", idType: SLUG) {
      ...BannerFragment
    }
    heroArabian: banner(id: "main-banner-2", idType: SLUG) {
      ...BannerFragment
    }
  }
`;

export const BANNERS = gql`
  ${BannerFragment}
  query getMainBanner {
    bannerFirst: banner(id: "secondary-banner-1", idType: SLUG) {
      ...BannerFragment
    }
    bannerSecond: banner(id: "secondary-banner-2", idType: SLUG) {
      ...BannerFragment
    }
  }
`;

export const INFO_BANNER = gql`
  ${BannerFragment}
  query InfoBanner($id: ID!) {
    banner(id: $id, idType: SLUG) {
      ...BannerFragment
    }
  }
`;

export const POPULAR_STORES = gql`
  query PopularStores {
    stores(where: {sortBy: mostPopular}) {
      nodes {
        id
        name
        storeUrl
        banner
        rating
        gravatar
        followed
        address {
          city
          countryName
        }
        totalFollowers
      }
    }
  }
`;
