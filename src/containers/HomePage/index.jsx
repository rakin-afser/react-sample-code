import React, {useState} from 'react';
import {useWindowSize} from '@reach/window-size';

import {FlexContainer} from 'globalStyles';
import {Helmet} from "react-helmet";
import Layout from 'containers/Layout';
import HomePageDesktop from 'containers/HomePage/HomePageDesktop';
import HomePageMobile from 'containers/HomePage/HomePageMobile';
import { useMutation } from '@apollo/client';
import { Subscribe } from 'mutations';
import SubscribeForm from 'containers/BuyerLanding/components/SubscribeForm';

const HomePage = () => {
  const {width} = useWindowSize();
  const isMobile = width <= 767;
  const [subscribeEmail,setSubscribeEmail] = useState()
  const [isLoading, setIsLoading] = useState(false);
  const [success,setSuccess] = useState(false)
  const [subscribe] = useMutation(Subscribe);

      const submitSubscribe= async (e) => {
          setIsLoading(true)
          e.preventDefault();
        try {
            let response = await subscribe({
                variables: {
                    input: {email: subscribeEmail}
                }
            });
            const {data} = response
            setSuccess(true)
            setSubscribeEmail('')
        }catch (error) {
            console.log("...........error",error)
        }
          setIsLoading(false)
      }

  return (
    <Layout showBottomNav={isMobile}>
      <Helmet>
        <title>testSample - Shop and Earn</title>
        <meta name="description" content="Now turn your social posts into shoppable feed and earn real money. Get started now!"></meta>
      </Helmet>
      {isMobile ? <HomePageMobile /> : <HomePageDesktop />}
      <SubscribeForm />
    </Layout>
  );
};

export default HomePage;
