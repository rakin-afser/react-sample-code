import React from 'react';
import {useParams} from 'react-router';
import SinglePost from 'containers/SinglePost';
import {Container} from './styled';
import Layout from 'containers/Layout';

const Post = () => {
  const {postId} = useParams();

  return (
    <Layout isFooterShort>
      <Container>
        <SinglePost postId={postId} />
      </Container>
    </Layout>
  );
};

export default Post;
