import styled from 'styled-components';

export const Container = styled.div`
  padding-top: 20px;
  max-width: 1010px;
  margin: 0 auto;
`;
