import React, {useState} from 'react';
import Icon from 'components/Icon';
import {SeeComments, Wrapper, Container, ShowMoreButton} from './styled';
import Comments from 'components/Comments/CommentsDesktop';
import {CSSTransition} from 'react-transition-group';

const CommentsSection = ({commentCount, postId, commentOn}) => {
  const [active, setActive] = useState(false);

  function onEnter(node) {
    node.style.setProperty('margin-top', `-${node.clientHeight}px`);
  }

  function onEntering(node) {
    node.style.setProperty('margin-top', '');
  }

  function onEntered(node) {
    node.style.setProperty('margin-top', '');
  }

  function onExiting(node) {
    node.style.setProperty('margin-top', `-${node.clientHeight}px`);
  }

  return (
    <>
      <SeeComments>
        See Comments <span>{commentCount && `(${commentCount})`}</span>
        <ShowMoreButton expanded={active} onClick={() => setActive(!active)}>
          {active ? 'Hide' : 'Show'}
          <Icon type="arrowDown" color="#000" />
        </ShowMoreButton>
      </SeeComments>
      <Container>
        <CSSTransition
          onEnter={onEnter}
          onEntering={onEntering}
          onExiting={onExiting}
          onEntered={onEntered}
          in={active}
          timeout={300}
          unmountOnExit
        >
          <Wrapper>
            <Comments
              postId={postId}
              commentOn={commentOn}
              // showReportPopup={showReportPopup}
            />
          </Wrapper>
        </CSSTransition>
      </Container>
    </>
  );
};

export default CommentsSection;
