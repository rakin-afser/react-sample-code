import styled, {css} from 'styled-components';
import {secondaryFont} from 'constants/fonts';
import {mainBlackColor} from 'constants/colors';

export const Wrapper = styled.div`
  &.enter {
    opacity: 0;
  }

  &.enter-active {
    opacity: 1;
    margin-top: 0;
    transition: opacity ease 300ms, margin-top 300ms;
  }

  &.exit-active {
    opacity: 1;
  }

  &.exit {
    opacity: 0;
    transition: opacity ease 300ms, margin-top 300ms;
  }
`;

export const Container = styled.div`
  overflow: hidden;
`;

export const SeeComments = styled.div`
  position: relative;
  z-index: 1;
  font-size: 14px;
  font-weight: 600;
  font-family: ${secondaryFont};
  margin: 30px 0;
  width: 100%;
  transition: 0.5s margin;
  display: flex;
  align-items: center;
  color: ${mainBlackColor};
  & span {
    font-weight: normal;
    margin: 0 10px 0 4px;
  }
`;

export const ShowMoreButton = styled.div`
  display: flex;
  font-family: ${secondaryFont};
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  color: #8f8f8f;
  cursor: pointer;

  & svg {
    transition: 0.5s transform;
    margin-left: 5px;
    ${({expanded}) =>
      expanded &&
      css`
        transform: rotate(180deg);
      `}
  }
`;
