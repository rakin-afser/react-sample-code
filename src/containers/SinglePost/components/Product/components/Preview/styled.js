import styled, {css} from 'styled-components';
import {primaryColor} from 'constants/colors';
import Icon from 'components/Icon';

export const Wrapper = styled.div`
  display: flex;
  position: absolute;
  top: 0;
  right: 14px;
  flex-direction: column;
`;

export const Preview = styled.div`
  position: relative;
  cursor: pointer;
  width: 40px;
  height: 40px;
  background-color: #f4f4f4;

  &::before {
    content: '';
    position: absolute;
    top: 100%;
    left: 0;
    right: 0;
    border-bottom: 2px solid transparent;
    transition: border-color 0.3s ease;
  }

  ${({active}) =>
    active
      ? css`
          &::before {
            border-bottom-color: ${primaryColor};
          }
        `
      : null}

  & + & {
    margin-top: 5px;
  }
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Play = styled(Icon)`
  width: 19px;
  height: 19px;
  border-radius: 50%;
  background-color: rgba(0, 0, 0, 0.4);
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const PlayWrap = styled.div``;
