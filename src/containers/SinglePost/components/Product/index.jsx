import {Col, Row} from 'antd';
import React, {useEffect, useState} from 'react';
import OrderInfo from 'containers/ProductPage/Components/OrderInfo';
import {useQuery} from '@apollo/client';
import {FEED_POST_SHORT, PRODUCT_POST_QUICKVIEW} from 'queries';
import Tags from 'components/Tags';
import {MediaWrapper, UserInfoContainer, Wrapper, Content, Title, ActionRowWrap} from './styled';
import CommentsSection from '../CommentsSection';
import UserInfo from 'components/UserInfo/GuestView/Small';
import userPlaceholder from 'images/placeholders/user.jpg';
import {useHistory} from 'react-router';
import Slider from '../Slider';
import Loader from 'components/Loader';
import Preview from './components/Preview';
import ItemInfo from 'components/ItemInfo';
import parse from 'html-react-parser';
import SeoMeta from 'components/SeoMeta';
import Scrollbar from 'components/Scrollbar';

const Product = (props) => {
  const history = useHistory();
  const {productId, postId, setView} = props;
  const {data, error, loading} = useQuery(PRODUCT_POST_QUICKVIEW, {variables: {id: productId}});
  const {image, galleryImages, description, product_video_url: productVideoUrl} = data?.product || {};
  const [mainImage, setMainImage] = useState(null);

  // Apply temporary rerender fix
  useEffect(() => {
    if (productId) setMainImage();
  }, [productId]);

  let gallery = [];
  gallery = productVideoUrl ? [{id: 'prod-video', mimeType: 'video', mediaItemUrl: productVideoUrl}] : [];
  gallery = mainImage || image ? [...gallery, mainImage || image] : [...gallery];
  gallery = galleryImages ? [...gallery, ...galleryImages?.nodes] : [...gallery];

  const {data: dataPost, loading: loadingPost, error: errorPost} = useQuery(FEED_POST_SHORT, {variables: {id: postId}});
  const {post} = dataPost || {};
  const {id, author, commentCount} = post || {};

  const {productTags} = data?.product || {};

  const getLinkByUserRole = (userRoles) => {
    if (userRoles.some((el) => el.name === 'seller')) {
      return `/shop/${author?.node?.nicename}/products`;
    }

    return `/profile/${author?.node?.databaseId}/posts`;
  };

  if (loading) return <Loader wrapperWidth="100%" wrapperHeight="697px" />;

  return (
    <Wrapper>
      <SeoMeta title={data?.product?.name} image={data?.product?.image?.sourceUrl} />
      <Row gutter={16}>
        <Col md={14}>
          <MediaWrapper>
            <Slider gallery={gallery} small={1} />
          </MediaWrapper>
          <Preview post={post} productId={productId} setView={setView} />
          <ActionRowWrap>
            <Row align="middle" type="flex">
              <Col md={12}>
                <Tags items={productTags?.nodes} />
              </Col>
              <Col md={12}>
                <ItemInfo isModal data={data} />
              </Col>
            </Row>
          </ActionRowWrap>
          <UserInfoContainer>
            <UserInfo
              id={author?.node?.id}
              databaseId={author?.node?.databaseId}
              src={author?.node?.avatar?.url || userPlaceholder}
              firstName={author?.node?.name}
              followers={author?.node?.totalFollowers}
              isFollowed={author?.node?.isFollowed}
              onClick={() => history.push(`${getLinkByUserRole(author?.node?.roles?.nodes)}`)}
            />
          </UserInfoContainer>
          <CommentsSection commentCount={commentCount} postId={id} commentOn={postId} />
        </Col>
        <Col md={10}>
          <Scrollbar autoHeight autoHeightMax={680}>
            <OrderInfo
              quickView
              isModal
              postId={postId}
              setMainImage={setMainImage}
              productData={data}
              returnsInfoDisplaying={false}
              paymentsInfoDisplaying={false}
              displayPostInfo={false}
            />
            {description ? (
              <>
                <Title>Description</Title>
                <Content>{parse(description)}</Content>
              </>
            ) : null}
          </Scrollbar>
        </Col>
      </Row>
    </Wrapper>
  );
};

export default Product;
