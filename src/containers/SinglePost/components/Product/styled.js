import {secondaryFont} from 'constants/fonts';
import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 62px 32px 42px;
`;

export const MediaWrapper = styled.div`
  position: relative;
`;

export const UserInfoContainer = styled.div`
  margin: 40px 0 30px;
`;

export const Content = styled.div`
  font-family: ${secondaryFont};
  font-size: 14px;
  color: #464646;
`;

export const Title = styled.h3`
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 16px;
  line-height: 1.24;
  letter-spacing: 0.019em;
`;

export const ActionRowWrap = styled.div`
  margin-top: 20px;
  margin-right: 54px;
`;
