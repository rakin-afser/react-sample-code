import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';

import {useUser} from 'hooks/reactiveVars';

import Comments from 'components/CommentsCounter';
import Likes from 'components/Likes';
import Share from 'assets/Share';
import {PostActionsWrapper} from 'components/Modals/PostView/components/PostActions/styled';

const PostActions = ({likesCount = 0, isLiked, commentsCount = 0, onLikeClick = () => {}, onShareClick = () => {}}) => {
  const [user] = useUser();
  const history = useHistory();
  const [liked, setLiked] = useState(isLiked);
  const [likesCountCurrent, setLikesCountCurrent] = useState(likesCount);

  const onLike = () => {
    if (!user?.databaseId) {
      history.push('/auth/sign-in');
      return;
    }

    onLikeClick();
    setLikesCountCurrent(liked ? likesCountCurrent - 1 : likesCountCurrent + 1);
    setLiked(!liked);
  };

  return (
    <PostActionsWrapper>
      <div onClick={onLike}>
        <Likes isLiked={liked} width={20} height={18} likesCount={likesCountCurrent} showCount />
      </div>
      <Comments commentsCount={commentsCount} showCount />
      <div onClick={onShareClick}>
        <Share fill="#8F8F8F" width={20} height={18} />
      </div>
    </PostActionsWrapper>
  );
};

export default PostActions;
