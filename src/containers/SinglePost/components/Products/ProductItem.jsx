import React, {useEffect, useRef, useState} from 'react';
import useGlobal from 'store';
import {
  Description,
  ProductBrand,
  ProductDelivery,
  ProductImageMobile,
  ProductMobile,
  ProductNameMobile,
  Sale,
  SellButton,
  ViewButton
} from './styled.js';
import Icon from 'components/Icon';
import {useHistory} from 'react-router';
import {useMutation} from '@apollo/client';
import {ADD_TO_CART} from 'mutations.js';
import {getDiscountPercent} from 'util/heplers.js';

const ProductItem = ({isAlone, product, isUserLogeIn, isExpanded, openQuickView, author}) => {
  const [, globalActions] = useGlobal();
  const targetRef = useRef(null);
  const [height, setHeight] = useState(0);
  const {push} = useHistory();

  const [addToCart] = useMutation(ADD_TO_CART, {
    update(cache, {data}) {
      cache.modify({
        fields: {
          cart() {
            return data.addToCart.cart;
          }
        }
      });
    },
    onCompleted() {
      push('/cart');
    }
  });

  useEffect(() => {
    if (targetRef?.current) {
      setHeight(targetRef.current.offsetHeight);
    }
  }, [isExpanded]);

  // ToDo: fix height value issue

  const {id, databaseId, name, shippingType, productBrands, variations} = product || {};
  const {image, onSale, regularPrice, salePrice} = variations?.nodes?.[0] || product || {};
  const purchaseSource = author ? {source_type: 'FEED_POST', author} : {};

  function onSellButton() {
    if (variations?.nodes?.length > 0) {
      globalActions.setShowVariationsPopup({active: true, productId: id, purchaseSource});
      return null;
    }
    addToCart({
      variables: {
        input: {
          productId: databaseId,
          quantity: 1,
          purchase_source: purchaseSource
        }
      }
    });
    return null;
  }

  return (
    <ProductMobile ref={targetRef} height={64} isAlone={isAlone && isUserLogeIn}>
      {image?.sourceUrl ? (
        <ProductImageMobile onClick={() => openQuickView()} src={image?.sourceUrl} alt={name} />
      ) : null}
      <Description onClick={() => openQuickView()}>
        {name ? (
          <ProductNameMobile>
            <span>{name}</span>
          </ProductNameMobile>
        ) : null}
        {productBrands?.nodes[0]?.name ? <ProductBrand>Brand: {productBrands?.nodes[0]?.name}</ProductBrand> : null}
        {shippingType ? <ProductDelivery>{shippingType}</ProductDelivery> : null}
      </Description>
      {onSale ? (
        <Sale>
          -<b>{getDiscountPercent(regularPrice, salePrice)}</b>%
        </Sale>
      ) : null}
      {regularPrice ? (
        <SellButton onClick={() => onSellButton()} isAlone={isAlone && isUserLogeIn}>
          <Icon type="cart" color="currentColor" width="15" height="16" />
          <span>{regularPrice}</span>
          {/* <small>.00</small> */}
        </SellButton>
      ) : null}
    </ProductMobile>
  );
};

export {ProductItem};
