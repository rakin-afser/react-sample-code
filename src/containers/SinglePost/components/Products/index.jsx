import React, {useState} from 'react';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import {Divider, ExpandProductsButtonMobile, ProductsBlock} from './styled.js';
import {ProductItem} from './ProductItem';

const Products = ({products, isUserLogeIn, openQuickView, author}) => {
  const [hideButton, setHideButton] = useState(false);
  const [shownProduct, setShownProduct] = useState(2);

  const getProducts = (count = null) => {
    const isAlone = products.length === 1;
    if (count !== null) {
      return products.map((product, i) => {
        return i < count ? (
          <CSSTransition key={i} timeout={300} classNames="item">
            <ProductItem
              author={author}
              openQuickView={openQuickView}
              isExpanded={hideButton}
              isAlone={isAlone}
              product={product}
              isUserLogeIn={isUserLogeIn}
            />
          </CSSTransition>
        ) : null;
      });
    }

    return products.map((product, i) => {
      return (
        <CSSTransition key={i} timeout={300} classNames="item">
          <ProductItem
            author={author}
            openQuickView={openQuickView}
            key={i}
            isExpanded={hideButton}
            isAlone={isAlone}
            product={product}
            isUserLogeIn={isUserLogeIn}
          />
        </CSSTransition>
      );
    });
  };

  const getMoreProducts = (e) => {
    e.currentTarget.style.display = 'none';
    setShownProduct(products.length);
    setHideButton(true);
  };

  const renderExpand = () => {
    return (
      !hideButton && (
        <Divider>
          <ExpandProductsButtonMobile type="button" onClick={(e) => getMoreProducts(e)}>
            See More
          </ExpandProductsButtonMobile>
        </Divider>
      )
    );
  };

  return (
    <ProductsBlock>
      {products?.length > 2 ? (
        <>
          <TransitionGroup className="product-list">{getProducts(shownProduct)}</TransitionGroup>
          {renderExpand()}
        </>
      ) : null}
      {products?.length <= 2 && products?.length > 0 ? getProducts() : null}
    </ProductsBlock>
  );
};

export default Products;
