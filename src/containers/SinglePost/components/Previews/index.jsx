import React from 'react';
import {Wrapper, Preview, Image, Play, PlayWrap} from './styled';

const Previews = (props) => {
  const {post, setView} = props;
  const {relatedProducts, ...rest} = post || {};
  let previews = rest ? [rest] : [];
  previews = relatedProducts?.nodes?.length ? [...previews, ...relatedProducts?.nodes] : previews;

  function renderMedia(item) {
    const {image, galleryImages} = item || {};
    const media = image || galleryImages?.nodes?.[0];

    if (media?.mimeType?.startsWith('image')) return <Image src={media?.sourceUrl} alt={media?.altText} />;
    if (media?.mimeType?.startsWith('video')) {
      return (
        <PlayWrap>
          {media?.poster ? <Image src={media?.poster?.sourceUrl} alt={media?.poster?.altText} /> : null}
          <Play type="play" width={8} height={8} fill="#fff" />
        </PlayWrap>
      );
    }
    return null;
  }

  return (
    <Wrapper>
      {previews?.map((item) => (
        <Preview
          key={item?.id}
          active={item?.databaseId === post?.databaseId ? 1 : 0}
          onClick={() => setView(item?.databaseId)}
        >
          {renderMedia(item)}
        </Preview>
      ))}
    </Wrapper>
  );
};

export default Previews;
