import styled, {css} from 'styled-components';
import {primaryColor} from 'constants/colors';
import Icon from 'components/Icon';

export const Wrapper = styled.div`
  display: flex;
`;

export const Preview = styled.div`
  position: relative;
  cursor: pointer;
  width: 47px;
  height: 47px;
  transition: border-bottom 0.3s ease;
  border-radius: 8px;
  border: 1px solid #efefef;
  overflow: hidden;
  background-color: #f4f4f4;

  & + & {
    margin-left: 14px;
  }

  ${({active}) =>
    active
      ? css`
          border-color: ${primaryColor};
        `
      : null}
`;

export const Image = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Play = styled(Icon)`
  width: 19px;
  height: 19px;
  border-radius: 50%;
  background-color: rgba(0, 0, 0, 0.4);
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

export const PlayWrap = styled.div``;
