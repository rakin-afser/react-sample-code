import styled, {css} from 'styled-components/macro';
import {Swiper} from 'swiper/react';

export const SliderContent = styled(Swiper)`
  width: 100%;
  height: 100%;
`;

export const ImageSlide = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
  flex-shrink: 0;
`;

export const FullscreenButton = styled.div`
  width: 40px !important;
  height: 30px !important;
  position: absolute;
  bottom: 0;
  right: 24px;
  z-index: 5;
  background-color: transparent;
  cursor: pointer;
  transition: ease 0.4s;

  &:hover {
    transform: scale(1.1);
  }
`;

export const VideoSlide = styled.div`
  width: 100%;
  height: 100%;

  & > div {
    width: 100%;
    height: 100%;
  }

  & .video-js {
    width: 100% !important;
    height: 100% !important;
  }
`;

export const SliderWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding-bottom: 30px;
  position: relative;

  & .swiper-slide-active {
    width: 100% !important;
    display: flex;
  }

  & .swiper-pagination {
    bottom: 0px;
    width: 100%;
    padding: 12px;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
  }

  & .swiper-pagination-bullet {
    width: 8px;
    height: 8px;
    background: #e4e4e4;
    margin-right: 12px;
    opacity: 1;
    transition: background 0.5s;
    display: inline-block;
    cursor: pointer;
    border-radius: 50%;
  }

  & .swiper-pagination-bullet-active {
    background: #ea2c34;
  }

  & .custom-swiper-button-next,
  & .custom-swiper-button-prev {
    background: #00000030;
    border-radius: 3px;
    width: 37px;
    height: 37px;
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
    cursor: pointer;
    z-index: 5;
    transition: background 0.5s;

    &:hover {
      background: #00000060;
    }
  }

  & .custom-swiper-button-prev {
    left: 8px;
    & svg {
      margin: 9px 11px;
      transform: scale(-1, 1);
    }
  }

  & .custom-swiper-button-next {
    right: 8px;
    & svg {
      margin: 9px 14px;
    }
  }
`;
