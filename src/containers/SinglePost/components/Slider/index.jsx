import React from 'react';
import SwiperCore, {Navigation, Pagination} from 'swiper';
import {SwiperSlide} from 'swiper/react';
import VideoPlayer from 'components/VideoPlayer';

import Icon from 'components/Icon';
import {
  SliderWrapper,
  SliderContent,
  ImageSlide,
  VideoSlide
} from 'components/Modals/PostView/components/Slider/styled';
import productPlaceholder from 'images/placeholders/product.jpg';

// Swiper modules
SwiperCore.use([Pagination, Navigation]);

const Slider = ({gallery = [], small, style = {}}) => {
  const settings = {
    initialSlide: 0,
    slidesPerView: 1,
    loop: false,
    spaceBetween: 20,
    pagination: {
      clickable: true
    },
    navigation: {
      nextEl: '.custom-swiper-button-next',
      prevEl: '.custom-swiper-button-prev'
    }
  };

  function renderMedia(item) {
    const {mimeType, sourceUrl, altText, mediaItemUrl} = item || {};
    switch (mimeType?.split('/')?.[0]) {
      case 'image':
        return <ImageSlide src={sourceUrl} alt={altText} />;
      case 'video':
        return (
          <VideoSlide>
            <VideoPlayer src={mediaItemUrl} />
          </VideoSlide>
        );
      default:
        return <div />;
    }
  }

  function renderControls() {
    if (!gallery || gallery?.length <= 1) return null;

    return (
      <>
        <div className="custom-swiper-button-prev">
          <Icon type="arrow" width={11} height={18} color="#fff" />
        </div>
        <div className="custom-swiper-button-next">
          <Icon type="arrow" width={11} height={18} color="#fff" />
        </div>
      </>
    );
  }

  return (
    <SliderWrapper small={small} style={style}>
      {gallery?.length ? (
        <SliderContent {...settings}>
          <div className="swiper-wrapper">
            {gallery?.map((item) => (
              <SwiperSlide key={item?.id}>{renderMedia(item)}</SwiperSlide>
            ))}
          </div>
          {renderControls()}
        </SliderContent>
      ) : null}
    </SliderWrapper>
  );
};

export default Slider;
