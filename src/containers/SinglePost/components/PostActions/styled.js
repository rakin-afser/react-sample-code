import styled from 'styled-components/macro';

export const Wrapper = styled.ul`
  position: absolute;
  z-index: 1;
  right: 20px;
  bottom: 85px;
  display: flex;
  flex-direction: column;
`;

export const ActionItem = styled.li`
  & + & {
    margin-top: 15px;
  }
`;

export const ActionButton = styled.button`
  width: 45px;
  height: 45px;
  color: #fff;
  background-color: rgba(97, 95, 95, 0.2);
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.13);
  border-radius: 50%;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  font-size: 11px;
  line-height: 1.5;
`;
