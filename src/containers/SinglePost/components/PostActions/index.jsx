import React from 'react';
import {Wrapper, ActionItem, ActionButton} from './styled';
import Icon from 'components/Icon';
import {useAddUpdateLikedPostMutation} from 'hooks/mutations';
import {stripTags} from 'util/heplers';

const PostActions = (props) => {
  const {post} = props;
  const {databaseId, title, content, totalLikes, isLiked, commentCount} = post || {};
  const [triggerLike] = useAddUpdateLikedPostMutation({post});

  const onShare = async () => {
    const shareData = {
      title: `testSample - ${title}`,
      text: stripTags(content),
      url: `${window.location.origin}/post/${databaseId}`
    };

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Wrapper>
      <ActionItem>
        <ActionButton>
          <Icon type="message" color="currentColor" />
          {commentCount || ''}
        </ActionButton>
      </ActionItem>
      <ActionItem>
        <ActionButton onClick={() => triggerLike({variables: {input: {id: databaseId}}})}>
          <Icon type={isLiked ? 'liked' : 'like'} color="#fff" />
          {totalLikes || ''}
        </ActionButton>
      </ActionItem>
      <ActionItem>
        <ActionButton onClick={onShare}>
          <Icon type="share" />
        </ActionButton>
      </ActionItem>
    </Wrapper>
  );
};

export default PostActions;
