import React, {useState} from 'react';
import parse from 'html-react-parser';
import {useQuery} from '@apollo/client';

import useGlobal from 'store';
import {useUser} from 'hooks/reactiveVars';
import {useAddUpdateLikedPostMutation} from 'hooks/mutations';

import {
  MediaWrapper,
  Content,
  PostTitle,
  TagsContainer,
  BackToFeed,
  ActionsWrap,
  Item,
  LikeWrapper,
  Likes,
  CommentsCount,
  PostData
} from './styled';

import CommentsIcon from 'assets/Comments';
import Share from 'assets/Share';
import BookmarkFilled from 'assets/BookmarkFilled';
import Bookmark from 'assets/Bookmark';

import {stripTags} from 'util/heplers';

import CommentsSection from './components/CommentsSection';
import Tags from 'components/Tags';
import Icon from 'components/Icon';
import Slider from './components/Slider';
import Products from './components/Products';
import Comments from 'components/Comments';
import UserInfo from 'components/UserInfo/GuestView/Post';
import userPlaceholder from 'images/placeholders/user.jpg';
import {FEED_POST} from 'queries';
import Previews from './components/Previews';
import {Row, Col} from 'antd';
import {useHistory} from 'react-router';
import Loader from 'components/Loader';
import SeoMeta from 'components/SeoMeta';

const Post = ({postId, setView}) => {
  const [showCommentsPopup, setShowCommentsPopup] = useState(false);
  const [addedToList, setAddedToList] = useState(false);
  const history = useHistory();
  const [globalState, setGlobalState] = useGlobal();
  const [user] = useUser();
  const {data: dataPost, loading: loadingPost, error: errorPost} = useQuery(FEED_POST, {variables: {id: postId}});
  const {post} = dataPost || {};
  const [triggerLike] = useAddUpdateLikedPostMutation({post});
  const {
    id,
    author,
    title,
    tags,
    content,
    commentCount,
    relatedProducts,
    galleryImages,
    databaseId,
    totalLikes,
    isLiked,
    seller,
    slug,
    comments
  } = post || {};
  const {id: authorId, databaseId: authorDatabaseId, isFollowed, nicename} = author?.node || {};

  const getLinkByUserRole = (userRoles) => {
    if (userRoles.some((el) => el.name === 'seller')) {
      return `/shop/${nicename}/products`;
    }

    return `/profile/${authorDatabaseId}/posts`;
  };

  const addToList = (value = false) => {
    setAddedToList(value);
  };

  function onLike() {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    triggerLike({variables: {input: {id: databaseId}}});
  }

  function onComment() {
    if (!user?.databaseId) {
      setGlobalState.setIsRequireAuth(true);
      return;
    }
    setShowCommentsPopup(true);
  }

  const onSaveToList = () => {
    if (user?.databaseId) {
      setGlobalState.setActionsPopup(true, {productId: databaseId, seller, slug, title, callback: addToList});
    } else {
      setGlobalState.setIsRequireAuth(true);
    }
  };

  const onShare = async () => {
    const shareData = {
      title: `testSample - ${title}`,
      text: stripTags(content),
      url: `${window.location.origin}/post/${databaseId}`
    };

    try {
      if (navigator.share) {
        await navigator.share(shareData);
      }
    } catch (err) {
      console.log(err);
    }
  };

  if (loadingPost) return <Loader wrapperWidth="100%" wrapperHeight="697px" />;

  return (
    <>
      <BackToFeed onClick={() => history.goBack()}>
        <Icon type="arrowBack" color="#000" />
        <span>Back to Feed</span>
      </BackToFeed>
      <PostData>
        <UserInfo
          id={author?.node?.id}
          authorDatabaseId={authorDatabaseId}
          databaseId={author?.node?.databaseId}
          src={author?.node?.avatar?.url || userPlaceholder}
          firstName={author?.node?.name}
          followers={author?.node?.totalFollowers}
          isFollowed={author?.node?.isFollowed}
          onClick={() => history.push(`${getLinkByUserRole(author?.node?.roles?.nodes)}`)}
        />
        <PostTitle style={{textTransform: 'initial', fontSize: 16, marginTop: 8, marginBottom: 4, lineHeight: 1.1}}>
          {title}
        </PostTitle>
        {content ? <Content style={{marginBottom: 0}}>{parse(content)}</Content> : null}
        {tags?.nodes?.length ? (
          <TagsContainer>
            <Tags items={tags?.nodes} />
          </TagsContainer>
        ) : null}
      </PostData>
      <SeoMeta title={title} image={galleryImages?.nodes[0]?.sourceUrl} />
      <MediaWrapper>
        <Slider gallery={galleryImages?.nodes} style={{height: '100vw', paddingBottom: 0}} />
        {/* <PostActions post={post} /> */}
      </MediaWrapper>

      {relatedProducts?.nodes?.length ? <Products products={relatedProducts?.nodes} author={authorDatabaseId} /> : null}

      <ActionsWrap>
        <Item>
          <LikeWrapper onClick={onLike} isLiked={isLiked}>
            <Icon type={isLiked ? 'liked' : 'like'} color={isLiked ? '#ED484F' : '#999999'} width="20" height="18" />
            {totalLikes ? <Likes>{totalLikes}</Likes> : null}
          </LikeWrapper>
          {/* {data.likes ? (
              <Likes>
                {data.likes < 1000
                  ? isLiked
                    ? data.likes + 1
                    : data.likes
                  : data.likes < 1000000
                  ? changeLikesFormat(data.likes)
                  : changeLikesFormat(data.likes, 2)}
              </Likes>
            ) : null} */}
        </Item>
        <Item onClick={onComment}>
          <CommentsIcon fill="#999999" />
          {commentCount ? <CommentsCount>{commentCount}</CommentsCount> : null}
        </Item>

        <Item>
          <Share fill="#999999" onClick={onShare} />
        </Item>

        {relatedProducts?.nodes?.length ? (
          <Item
            marginAuto
            onClick={() => {
              onSaveToList();
            }}
          >
            {addedToList ? <BookmarkFilled fill="#999999" /> : <Bookmark fill="#999999" />}
          </Item>
        ) : null}
      </ActionsWrap>
      <Comments
        postId={id}
        commentOn={databaseId}
        data={comments?.nodes}
        isUserLogeIn={user?.databaseId?.length}
        isModal={false}
        showCommentsPopup={showCommentsPopup}
        setShowCommentsPopup={setShowCommentsPopup}
        commentCount={commentCount}
      />
      {/* <Container>
        <Row gutter={16}>
          <Col md={{span: 12, push: 12}}>
            <Previews post={post} setView={setView} />
          </Col>
          <Col md={{span: 12, pull: 12}}>
            <CommentsSection commentCount={commentCount} postId={id} commentOn={postId} />
          </Col>
        </Row>
      </Container> */}
    </>
  );
};

export default Post;
