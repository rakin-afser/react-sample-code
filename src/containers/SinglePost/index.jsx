import React, {useEffect, useState} from 'react';
import {useLocation} from 'react-router-dom';
import {useWindowSize} from '@reach/window-size';
import {Wrapper} from './styled';
import PostMobile from './PostMobile';
import PostDesktop from './PostDesktop';
import Product from './components/Product';

const SinglePost = ({postId}) => {
  const {pathname} = useLocation();
  const [view, setView] = useState(postId);
  const {width} = useWindowSize();
  const isMobile = width <= 767;

  useEffect(() => {
    if (pathname !== `/post/${postId}`) {
      window.history.pushState(null, null, `/post/${postId}`);
    }
  }, [postId, pathname]);

  const renderPost = () => {
    if (isMobile) {
      return <PostMobile postId={postId} setView={setView} />;
    }
    
    return <PostDesktop postId={postId} setView={setView} />;
  };

  const isPostID = postId?.toString() === view?.toString();

  return (
    <Wrapper style={isPostID && isMobile ? { marginBottom: -24 } : {}}>
      {isPostID ? (
        renderPost()
      ) : (
        <Product productId={view} postId={postId} setView={setView} />
      )}
    </Wrapper>
  );
};

export default SinglePost;
