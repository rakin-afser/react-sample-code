import styled, {css} from 'styled-components/macro';
import {Link} from 'react-router-dom';
import media from 'constants/media';
import {mainFont, secondaryFont} from 'constants/fonts';
import {primaryColor} from 'constants/colors';

export const Wrapper = styled.div`
  position: relative;
  overflow: hidden;
`;

export const Container = styled.div`
  padding: 12px 28px;
`;

export const Row = styled.div`
  display: flex;
  align-items: flex-start;
  padding: ${({padding}) => padding || '0'};
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  width: ${({width}) => width || '50%'};
  padding: ${({padding}) => padding || '0'};
`;

export const MediaWrapper = styled.div`
  position: relative;
  max-height: 100vw;

  .custom-swiper-button-prev,
  .custom-swiper-button-next {
    display: none !important;
  }

  .swiper-pagination-bullet {
    width: 6px !important;
    height: 6px !important;
    margin-right: 0 !important;

    &:not(:last-child) {
      margin-right: 6px !important;
    }
  }

  .video-js .vjs-tech {
    position: static !important;
  }
`;

export const UserInfoContainer = styled.div`
  margin: 0 0 30px;
`;

export const ShopInfoContainer = styled.div`
  padding: 13px 0 17px;
  overflow: hidden;
`;

export const PostTitle = styled.h3`
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 20px;
  text-transform: uppercase;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  margin-bottom: 8px;
`;

export const Title = styled.div`
  font-family: ${secondaryFont};
  font-weight: 600;
  font-size: 16px;
  letter-spacing: 0.019em;
  color: #000000;
  padding: 11px 0;
  margin-top: 40px;
`;

export const PostInfoWrapper = styled.div`
  max-width: 100%;
  margin-bottom: 24px;
  flex-grow: 1;

  ${({withoutMargin}) =>
    withoutMargin &&
    css`
      margin: 0;
    `}
`;

export const Content = styled.div`
  font-family: ${secondaryFont};
  font-size: 14px;
  color: #464646;

  p {
    margin-bottom: 0;
  }
`;

export const TagsContainer = styled.div`
  margin-top: 12px;
`;

export const PostData = styled.div`
  padding: 12px 16px 16px;
`;

export const BackToFeed = styled.button`
  margin: -2px 16px 10px;
  cursor: pointer;
  font-style: normal;
  font-weight: bold;
  font-size: 14px;
  line-height: 140%;
  display: inline-flex;
  align-items: center;
  letter-spacing: -0.016em;
  color: #000000;

  span {
    padding: 0 0 3px 5px;
  }
`;

export const ActionsWrap = styled.div`
  display: flex;
  width: 100%;
  padding-top: 15px;
  padding-bottom: 15px;
  position: relative;

  @media (max-width: ${media.mobileMax}) {
    padding: 15px 16px;

    &::before {
      content: '';
      position: absolute;
      left: -16px;
      right: -16px;
    }

    &::before {
      border-top: 0.5px solid #d8d8d8;
      top: 0px;
    }
  }
`;

export const Item = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  margin-left: ${({marginAuto}) => (marginAuto ? 'auto' : 'unset')};

  height: 26px;
  background: #f5f5f5;
  border: 0.5px solid #cccccc;
  box-sizing: border-box;
  border-radius: 22px;
  padding: 0 11px 0 10px;
  cursor: pointer;

  i,
  svg {
    max-width: 15px;
    max-height: 15px;
  }

  &:not(:last-child) {
    margin-right: 10px;
  }

  &:hover {
    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }

  span {
    font-size: 10px;
    line-height: 1;
    color: #999999;
    margin-right: 0;
    line-height: 13px;
  }
`;

export const Right = styled.div`
  margin-left: auto;
`;

export const Likes = styled.span`
  margin-top: 0;
  display: inline-block;
  font-family: ${mainFont};
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 140%;
  color: #8f8f8f;
  align-self: center;
  margin-left: 6px;

  @media (max-width: 767px) {
    font-size: 10px;
    position: relative;
    top: -1px;
    line-height: 1;
    color: #999999;
    margin-right: 0;
  }
`;

export const CommentsCount = styled(Likes)`
  font-family: Helvetica, sans-serif;
  margin-left: 0;
  color: ${({showComments}) => (showComments ? primaryColor : '#8f8f8f')};
  margin-right: 0;
  display: flex;
  align-items: center;
  cursor: pointer;
  transition: ease 0.4s;

  span {
    margin-left: 6px;
  }

  svg {
    path {
      transition: ease 0.4s;
      fill: ${({showComments}) => (showComments ? primaryColor : '#8f8f8f')};
    }
  }

  &:hover {
    color: ${primaryColor};

    svg {
      path {
        fill: ${primaryColor};
      }
    }
  }

  @media (max-width: 767px) {
    transition: ease 0.3s;
    font-size: 10px;
    position: relative;
    top: -1px;
    line-height: 1;
    color: #999999;
    margin-left: 6px;
  }
`;

export const BookmarkWrap = styled.div`
  display: flex;
  align-items: center;
  margin-right: 0;
  max-height: 20px;
  overflow: hidden;

  svg.iconBookmark {
    display: ${({isWished}) => (isWished ? 'none' : 'block')};
  }

  svg.iconFilledBookmark {
    display: ${({isWished}) => (isWished ? 'block' : 'none')};
    margin-right: -4px;
  }

  & > svg {
    cursor: pointer;

    path {
      transition: ease 0.4s;
    }
  }

  &:hover {
    & > svg {
      path {
        fill: ${primaryColor};
      }
    }
  }
`;

export const ShareWrapper = styled.div`
  position: absolute;
  right: 0;
  //top: -238px;
  bottom: 35px;
  z-index: 200;
  width: 384px;
`;

export const LikeWrapper = styled.div`
  display: flex;
  align-items: center;

  ${({isLiked}) => (isLiked ? `span { color: #ED494F; }` : '')}
`;
