import React from 'react';
import parse from 'html-react-parser';
import {useQuery} from '@apollo/client';
import FadeOnMount from 'components/Transitions';
import {
  MediaWrapper,
  UserInfoContainer,
  PostInfoWrapper,
  Content,
  Container,
  PostTitle,
  TagsContainer,
  Title
} from './styled';
import CommentsSection from './components/CommentsSection';
import Tags from 'components/Tags';
import Slider from './components/Slider';
import PostActions from './components/PostActions';
import UserInfo from 'components/UserInfo/GuestView/Small';
import userPlaceholder from 'images/placeholders/user.jpg';
import {FEED_POST} from 'queries';
import Previews from './components/Previews';
import {Row, Col} from 'antd';
import {useHistory} from 'react-router';
import Loader from 'components/Loader';
import SeoMeta from 'components/SeoMeta';

const Post = ({postId, setView}) => {
  const history = useHistory();
  const {data: dataPost, loading: loadingPost, error: errorPost} = useQuery(FEED_POST, {variables: {id: postId}});
  const {post} = dataPost || {};
  const {id, author, title, tags, content, commentCount, relatedProducts, galleryImages, databaseId} = post || {};

  const getLinkByUserRole = (userRoles) => {
    if (userRoles.some((el) => el.name === 'seller')) {
      return `/shop/${author?.node?.nicename}/products`;
    }

    return `/profile/${author?.node?.databaseId}/posts`;
  };

  if (loadingPost) return <Loader wrapperWidth="100%" wrapperHeight="697px" />;

  return (
    <>
      <SeoMeta title={title} image={galleryImages?.nodes?.[0]?.sourceUrl} />
      <MediaWrapper>
        <Slider gallery={galleryImages?.nodes} />
        <PostActions post={post} />
      </MediaWrapper>
      <Container>
        <Row gutter={16}>
          <Col md={{span: 12, push: 12}}>
            <Previews post={post} setView={setView} />
            {content ? (
              <>
                <Title>Description</Title>
                <Content>{parse(content)}</Content>
              </>
            ) : null}
          </Col>
          <Col md={{span: 12, pull: 12}}>
            <PostTitle>{title}</PostTitle>
            <TagsContainer>
              <Tags items={tags?.nodes} />
            </TagsContainer>
            <FadeOnMount>
              <PostInfoWrapper withoutMargin>
                <UserInfoContainer>
                  <UserInfo
                    id={author?.node?.id}
                    databaseId={author?.node?.databaseId}
                    src={author?.node?.avatar?.url || userPlaceholder}
                    firstName={author?.node?.name}
                    followers={author?.node?.totalFollowers}
                    isFollowed={author?.node?.isFollowed}
                    onClick={() => history.push(`${getLinkByUserRole(author?.node?.roles?.nodes)}`)}
                  />
                </UserInfoContainer>
              </PostInfoWrapper>
            </FadeOnMount>
            <CommentsSection commentCount={commentCount} postId={id} commentOn={postId} />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Post;
